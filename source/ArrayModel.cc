#include "Model.h"

ArrayModel::ArrayModel(const Exposure &exposure, const CountsMap &counts, const Psf &psf, const Sources &sources, const Parameters &pars, const Skymap<char> &filter, unsigned int configure) :
	BaseModel(counts,exposure,psf,sources,pars, filter, configure), fmodels(0)
{
}

void ArrayModel::setModels( const std::vector<BaseModel*> & models) {
	//Check for null pointers in the array
	for (int i = 0; i < models.size(); ++i){
		if (models[i] == 0) {
			throw(ModelError("Null pointer in array model, aborting"));
		}
	}
	//Clear up the variables
	fvariables.clear();
	
	//Set the model array to the new models
	fmodels = models;

	//Join the variables
	for (int i = 0; i < fmodels.size(); ++i){
		fvariables.add(fmodels[i]->getVariables());
	}
}

void ArrayModel::getMap(const Variables & vars, Skymap<double> &map) {
	for (int i = 0; i < fmodels.size(); ++i){
		fmodels[i]->getMap(vars, map);
	}
}

BaseModel::gradMap ArrayModel::getComponents(const Variables & vars, const std::string &prefix){
	gradMap output;
	for (int i = 0; i < fmodels.size(); ++i){
		gradMap tmp(fmodels[i]->getComponents(vars, prefix));
		for (gradMap::iterator it = tmp.begin(); it != tmp.end(); ++it){
		   //Check if something exists, if it does, add the new map
		   //rather than replace it
		   gradMap::iterator it2 = output.find((*it).first);
		   if (it2 == output.end()) {
			output[(*it).first] = (*it).second;
		   } else {
			output[(*it).first] += (*it).second;
		   }
		}
	}
	return output;
}

void ArrayModel::getGrads(const Variables & vars, const std::string & varName, Skymap<double> &map) {
	for (int i = 0; i < fmodels.size(); ++i){
		fmodels[i]->getGrads(vars,varName, map);
	}
}

BaseModel::gradMap ArrayModel::getSecDer(const Variables & vars, const std::string & varName1, const std::string & varName2) {
	gradMap output;
	//Loop over the models and add to output
	for (int i = 0; i < fmodels.size(); ++i){
		gradMap tmp(fmodels[i]->getSecDer(vars,varName1,varName2));
		for (gradMap::iterator it = tmp.begin(); it != tmp.end(); ++it){
		   //Check if something exists, if it does, add the new map
		   //rather than replace it
		   gradMap::iterator it2 = output.find((*it).first);
		   if (it2 == output.end()) {
			output[(*it).first] = (*it).second;
		   } else {
			output[(*it).first] += (*it).second;
		   }
		}
	}
	return output;
}

void ArrayModel::modifySources(const Variables & vars, Sources &sources) const {
	for (int i = 0; i < fmodels.size(); ++i) {
		fmodels[i]->modifySources(vars,sources);
	}
}
