
#include"Galplot.h"                    //AWS20050920
#include"galprop_classes.h"
#include"galprop.h"
#include"constants.h"
#include"fitsio.h"

static Distribution gcr_modulated; // used by gcr_modulate
static Distribution gcr_modulated_total;
//static void gcr_modulate(int i_species,double phi,int key);
static double average_value(double rmin, double rmax, double r1, double r2, double r3, double v1, double v2, double v3, int debug );// AWS20090316

int Galplot::plot_gcr_spectra()
{
   cout<<" >>>> plot_gcr_spectra    "<<endl;
   int status=0;


int i_species;            // index of species in nuclei file
int ir,ix,iy,iz,ip;
int i_ZA;                 // index of species in galplotdef parameters
double r;
int i_mod_phi;

char name[100],canvastitle[100], workstring1[100],workstring2[100];
char psfile[400];


TCanvas *c1;
TGraph *spectrum;
TText *text;
TH1F *h;

int index_mult = 3; // spectrum * E^index_mult. only 2 or 3 allowed. Not yet controlled by galplotdef

if(gcr[0].n_spatial_dimensions==2)
{
 gcr_modulated      .init(gcr[0].n_rgrid,gcr[0].n_zgrid,               gcr[0].n_pgrid);
 gcr_modulated_total.init(gcr[0].n_rgrid,gcr[0].n_zgrid,               gcr[0].n_pgrid);

}

if(gcr[0].n_spatial_dimensions==3)
 { 
gcr_modulated      .init(gcr[0].n_xgrid,gcr[0].n_ygrid,gcr[0].n_zgrid,gcr[0].n_pgrid);
gcr_modulated_total.init(gcr[0].n_xgrid,gcr[0].n_ygrid,gcr[0].n_zgrid,gcr[0].n_pgrid);
 }
  

 i_mod_phi=0; // only first modulation phi: later use all


int ncolors=1; int *colors=0;
gStyle->SetPalette(ncolors,colors);

 // names must be different or canvas disappears

  
  strcpy(canvastitle," galdef ID ");
  strcat(canvastitle,galdef.galdef_ID);
 

  //====== see HowTo Style: and do before creating canvas
 
 
   TStyle *plain  = new TStyle("Plain","Plain Style (no colors/fill areas)"); 

   
   plain->SetCanvasBorderMode(0);
   plain->SetPadBorderMode(0);
   plain->SetPadColor(0);
   plain->SetCanvasColor(0);
   plain->SetTitleColor(1,"X"); //AWS20080125  was (0), changed for root 5.08
   plain->SetTitleColor(1,"Y"); //AWS20080125
   plain->SetStatColor(0);
   gROOT->SetStyle("Plain");
  


  strcpy(name,"gcr_spectra");// to identify it: name is not plotted

  for(i_ZA=0;i_ZA<galplotdef.gcr_spectra_n_ZA;i_ZA++)
  {

    for(int Ek_or_momentum = 1;  Ek_or_momentum <= 2;  Ek_or_momentum++) //AWS20130131  1=Ek 2=momentum
    {


 
      sprintf(name,"gcr_spectra%d%d",i_ZA,Ek_or_momentum);//AWS20130131 canvas names must be unique

  c1=new TCanvas(name,name       ,300+i_ZA*20 ,150+i_ZA*20,600,600);
  c1->SetLogx(); c1->SetLogy();
  
 
  
  if(galplotdef.gcr_spectra_Emin              ==0.0)
    h=c1->DrawFrame( gcr[0].Ekin[0],                galplotdef.gcr_spectra_Imin,
                     gcr[0].Ekin[gcr[0].n_pgrid-1] ,galplotdef.gcr_spectra_Imax);

  if( galplotdef.gcr_spectra_Emin              >0)
    h=c1->DrawFrame( galplotdef.gcr_spectra_Emin ,galplotdef.gcr_spectra_Imin,
                     galplotdef.gcr_spectra_Emax ,galplotdef.gcr_spectra_Imax);


  //AWS20050930
  if(Ek_or_momentum==1) //AWS20130131
  {
                                           h->SetXTitle("energy, MeV");                   // electrons, positrons
   if(galplotdef.gcr_spectra_A[i_ZA]>0)    h->SetXTitle("kinetic energy, MeV/nucleon");   // nuclei

   if(index_mult==2)    //AWS20091009
   h->SetYTitle("E^{2} I(E), cm^{-2} sr^{-1} s^{-1} MeV   "); // needs {} to work
   if(index_mult==3)    //AWS20091009
   h->SetYTitle("E^{3} I(E), cm^{-2} sr^{-1} s^{-1} MeV^{2}   "); // needs {} to work
  }

  if(Ek_or_momentum==2) //AWS20130131
  {
   h->SetXTitle("momentum, MeV                 ");  
   if(index_mult==2)    //AWS20091009
   h->SetYTitle("p^{2} n(p), cm^{-2} sr^{-1} s^{-1} MeV   "); // needs {} to work
   if(index_mult==3)    //AWS20091009
   h->SetYTitle("p^{3} n(p), cm^{-2} sr^{-1} s^{-1} MeV^{2}   "); // needs {} to work
  }



  h->SetLabelOffset(+0.00,"X"); //  +ve down
  h->SetLabelSize  ( 0.03,"Y"); // labels, not titles
  h->SetTitleOffset(+1.1,"X");  //  +ve down
  h->SetTitleOffset(+1.2,"Y");  //  +ve to left
  h->SetTitleSize   (0.04 ,"Y");// titles, not labels



  text=new TText();
  text->SetTextFont(62);
  text->SetTextColor(36);
  text->SetTextSize(0.035 );
  text->SetTextAlign(12);

  text->DrawTextNDC(.12 ,.93 ,canvastitle);// NDC=normalized coord system
  text->SetTextSize(0.025 );
  sprintf(workstring1,"Z = %d  A = %d",galplotdef.gcr_spectra_Z[i_ZA],galplotdef.gcr_spectra_A[i_ZA]);
  text->DrawTextNDC(.75 ,.88 ,workstring1);// NDC=normalized coord system
  //  text->DrawTextNDC(.58 ,.85 ,workstring2);// NDC=normalized coord system
 

   

 // -------

  int i_species_gcr   =-1; //AWS20130201 for use with gcr for momentum

  for(i_mod_phi=0;i_mod_phi< galplotdef.gcr_spectra_n_mod_phi;i_mod_phi++)  //AWS20060316
  {

   gcr_modulated_total=0.;

   for(i_species=0;i_species<n_species;i_species++)
   {

    if(  gcr[i_species].Z==galplotdef.gcr_spectra_Z[i_ZA]
       &&gcr[i_species].A==galplotdef.gcr_spectra_A[i_ZA]
      || gcr[i_species].Z==galplotdef.gcr_spectra_Z[i_ZA]
	 &&            -1==galplotdef.gcr_spectra_A[i_ZA]) // include all A for this Z
    {

      cout<<"plot_gcr_spectra: Z,A:"<<gcr[i_species].Z<<","<<gcr[i_species].A<<" mod_phi="<<galplotdef.gcr_spectra_mod_phi[i_mod_phi] <<endl;

     gcr_modulate(i_species,galplotdef.gcr_spectra_mod_phi[i_mod_phi],     0);

     gcr_modulated_total+=gcr_modulated;

     i_species_gcr = i_species;  //AWS20130201

     }//if species
    }//i_species

   if( i_species_gcr==-1){cout<<"plot_gcr_spectra: particle not found, exiting this routine !"<<endl; return -1;}


    if(gcr[0].n_spatial_dimensions==2)
    {
    cout<<"plot_gcr_spectra: 2D"<<endl;
    for(iz=0;iz<gcr[0].n_zgrid;iz++)
      {
    for(ir=0;ir<gcr[0].n_rgrid;ir++)
      {
       if(  gcr[0].z[iz]>=galplotdef.gcr_spectra_z_min && gcr[0].z[iz]<=galplotdef.gcr_spectra_z_max
	   &&gcr[0].r[ir]>=galplotdef.gcr_spectra_r_min && gcr[0].r[ir]<=galplotdef.gcr_spectra_r_max)
	 {
	    cout<<"    plot_gcr_spectra: Z,A:"<<galplotdef.gcr_spectra_Z[i_ZA]<<","<<galplotdef.gcr_spectra_A[i_ZA]<<"  r ="<<gcr[0].r[ir]<<" z="<<gcr[0].z[iz]<<endl;

         spectrum=new TGraph(gcr[0].n_pgrid);

 
  


  for  (ip    =0;  ip    <gcr[0].n_pgrid;ip    ++)
  {
    //   spectrum->SetPoint(ip,gcr[0].Ekin[ip],gcr_modulated_total       .d2[ir][iz].s[ip]);

                             int debug=0;      
   if(galplotdef.verbose==-1501) debug=1; // selectable debug                                  //AWS20090316
   double value=average_value                                                                  //AWS20090316

                            (galplotdef.gcr_spectra_r_min,galplotdef.gcr_spectra_r_max, 
                             gcr[0]              .r[ir-1],                         gcr[0].r[ir],                           gcr[0].r[ir+1],
                             gcr_modulated_total.d2[ir-1][iz].s[ip], gcr_modulated_total.d2[ir][iz].s[ip], gcr_modulated_total  .d2[ir+1][iz].s[ip],
                             debug);


  if(Ek_or_momentum==1)                                     //AWS20130131 Ekin
  {
   if(index_mult==3) value *= gcr[0].Ekin[ip]; // AWS20091009

   spectrum->SetPoint(ip,gcr[0].Ekin[ip],value);                                               //AWS20090316

    double x,y; spectrum->GetPoint(ip,x,y);

    cout<<"Ekin     spectrum i_ZA="<<i_ZA<<   " Z="<<galplotdef.gcr_spectra_Z[i_ZA]<<" A="<< galplotdef.gcr_spectra_A[i_ZA] <<" gcr[0].Ekin[ip] ="<< gcr[0].Ekin[ip]
   	<<" i_species_gcr="<<i_species_gcr              <<" gcr[i_species_gcr].p[ip] ="<< gcr[i_species_gcr].p[ip] 
        << " value="<<value<<" ip="<<ip<<" x="<<x<<" y="<<y <<endl;

    // shorter output for export
    cout<<"export spectrum:  Z="<<galplotdef.gcr_spectra_Z[i_ZA]<<" A="<< galplotdef.gcr_spectra_A[i_ZA] <<" Ekin ="<< gcr[0].Ekin[ip] << " spectrum="<<value<<endl;

  }


  if(Ek_or_momentum==2)                                     //AWS20130131 momentum
   {
     if(gcr[i_species_gcr].A >0 ) // nuclei only, not electrons or positrons
                          value *= pow(gcr[i_species_gcr].p[ip]/gcr[0].Ekin[ip],2) / gcr[i_species_gcr].A   ;  // I(Ekin)*Ekin^2 -> n(p)*p^2
    if(index_mult==3)     value *=     gcr[i_species_gcr].p[ip];          
    spectrum->         SetPoint(ip,    gcr[i_species_gcr].p[ip],value);   

    double x,y; spectrum->GetPoint(ip,x,y);
    cout<<"momentum spectrum i_ZA="<<i_ZA<<   " Z="<<galplotdef.gcr_spectra_Z[i_ZA]<<" A="<< galplotdef.gcr_spectra_A[i_ZA] <<" gcr[0].Ekin[ip] ="<< gcr[0].Ekin[ip]
	<<" i_species_gcr="<<i_species_gcr     <<" gcr[i_species_gcr].p[ip] ="<< gcr[i_species_gcr].p[ip] 
        << " value="<<value<<" ip="<<ip<<" x="<<x<<" y="<<y <<endl;
   }




  }

  /* do this with DrawFrame to get both required range and axis labels
  spectrum->GetHistogram()->SetXTitle("energy, MeV                 ");  
  spectrum->GetHistogram()->SetYTitle("E^{2} I(E), cm^{-2} sr^{-1} s^{-1} MeV   "); // needs {} to work
  spectrum->GetHistogram()->SetLabelOffset(+0.00,"X"); //  +ve down
  spectrum->GetHistogram()->SetLabelSize  ( 0.03,"Y"); // labels, not titles
  spectrum->GetHistogram()->SetTitleOffset(+1.1,"X");  //  +ve down
  spectrum->GetHistogram()->SetTitleOffset(+1.2,"Y");  //  +ve to left
  spectrum->GetHistogram()->SetTitleSize   (0.04 ,"Y");// titles, not labels
  */
  //Gtypes.h:enum EColor { kWhite, kBlack, kRed, kGreen, kBlue, kYellow, kMagenta, kCyan };
  spectrum->SetMarkerColor(kBlack); //AWS20090220
  spectrum->SetMarkerStyle(21); 
  spectrum->SetMarkerStyle(22); // triangles
  spectrum->SetMarkerSize (0.5);// 
  spectrum->SetLineColor(kBlack);  //AWS20090220
  spectrum->SetLineWidth(3     );  //AWS20090220
  spectrum->SetLineStyle(1      ); //AWS20090220
  spectrum->SetMarkerStyle(22); // triangles
  spectrum->SetMarkerSize (0.5);// 
  //   spectrum->Draw();       // don't use "same" for TGraph
        spectrum->Draw("L");  // lines  axes           
  
 
	}//if z,r
       }//ir
      }//iz

  }//2D


    /////////// 3D

   if(gcr[0].n_spatial_dimensions==3)
    {
    cout<<"3D"<<endl;
    for(iz=0;iz<gcr[0].n_zgrid;iz++)
      {
    for(ix=0;ix<gcr[0].n_xgrid;ix++)
      {
    for(iy=0;iy<gcr[0].n_ygrid;iy++)
      {
	r=sqrt(gcr[0].x[ix]*gcr[0].x[ix]+ gcr[0].y[iy]*gcr[0].y[iy]);

       if(  gcr[0].z[iz]>=galplotdef.gcr_spectra_z_min && gcr[0].z[iz]<=galplotdef.gcr_spectra_z_max
	  &&           r>=galplotdef.gcr_spectra_r_min &&            r<=galplotdef.gcr_spectra_r_max)
	 {
	    cout<<"    plot_gcr_spectra:Z,A:"<<galplotdef.gcr_spectra_Z[i_ZA]<<","<<galplotdef.gcr_spectra_A[i_ZA]
                 <<" x="<<gcr[0].x[ix]
                 <<" y="<<gcr[0].y[iy]
                 <<" r="<<       r         
                 <<" z="<<gcr[0].z[iz]<<endl;

         spectrum=new TGraph(gcr[0].n_pgrid);

 
  


  for  (ip    =0;  ip    <gcr[0].n_pgrid;ip    ++)
  {

    //   spectrum->SetPoint(ip,gcr[0].Ekin[ip],gcr_modulated_total       .d3[ix][iy][iz].s[ip]);// AWS20120508

   double value;                                            // AWS20120508
   value=gcr_modulated_total.d3[ix][iy][iz].s[ip];          // AWS20120508

   if(Ek_or_momentum==1)                                    //AWS20130131 Ek 
   {
    if(index_mult==3)     value *= gcr[0].Ekin[ip];          // AWS20120508
    spectrum->         SetPoint(ip,gcr[0].Ekin[ip],value);   // AWS20120508

    double x,y; spectrum->GetPoint(ip,x,y);
    cout<<"Ekin     spectrum i_ZA="<<i_ZA<<   " Z="<<galplotdef.gcr_spectra_Z[i_ZA]<<" A="<< galplotdef.gcr_spectra_A[i_ZA] <<" gcr[0].Ekin[ip] ="<< gcr[0].Ekin[ip]
   	<<" i_species_gcr="<<i_species_gcr              <<" gcr[i_species_gcr].p[ip] ="<< gcr[i_species_gcr].p[ip] 
        << " value="<<value<<" ip="<<ip<<" x="<<x<<" y="<<y <<endl;
   }

   if(Ek_or_momentum==2)                                     //AWS20130131 momentum
   {
      if(gcr[i_species_gcr].A >0 ) // nuclei only, not electrons or positrons
                          value *= pow(gcr[i_species_gcr].p[ip]/gcr[0].Ekin[ip],2) / gcr[i_species_gcr].A   ;  // I(Ekin)*Ekin^2 -> n(p)*p^2
                         
    if(index_mult==3)     value *=     gcr[i_species_gcr].p[ip];          
    spectrum->         SetPoint(ip,    gcr[i_species_gcr].p[ip],value);   

    double x,y; spectrum->GetPoint(ip,x,y);
    cout<<"momentum spectrum i_ZA="<<i_ZA<<   " Z="<<galplotdef.gcr_spectra_Z[i_ZA]<<" A="<< galplotdef.gcr_spectra_A[i_ZA] <<" gcr[0].Ekin[ip] ="<< gcr[0].Ekin[ip]
	<<" i_species_gcr="<<i_species_gcr     <<" gcr[i_species_gcr].p[ip] ="<< gcr[i_species_gcr].p[ip] 
        << " value="<<value<<" ip="<<ip<<" x="<<x<<" y="<<y <<endl;
   }

  } // ip

  /*
  spectrum->GetHistogram()->SetXTitle("energy, MeV                 ");  
  spectrum->GetHistogram()->SetYTitle("E^{2} I(E), cm^{-2} sr^{-1} s^{-1} MeV   "); // needs {} to work
  spectrum->GetHistogram()->SetLabelOffset(+0.00,"X"); //  +ve down
  spectrum->GetHistogram()->SetLabelSize  ( 0.03,"Y"); // labels, not titles
  spectrum->GetHistogram()->SetTitleOffset(+1.1,"X");  //  +ve down
  spectrum->GetHistogram()->SetTitleOffset(+1.2,"Y");  //  +ve to left
  spectrum->GetHistogram()->SetTitleSize   (0.04 ,"Y");// titles, not labels
*/
  //Gtypes.h:enum EColor { kWhite, kBlack, kRed, kGreen, kBlue, kYellow, kMagenta, kCyan };
  spectrum->SetMarkerColor(kBlack); //AWS20090220
  spectrum->SetMarkerStyle(21); 
  spectrum->SetMarkerStyle(22); // triangles
  spectrum->SetMarkerSize (0.5);// 
  spectrum->SetLineColor(kBlack);  //AWS20090220
  spectrum->SetLineWidth(3     );  //AWS20090220
  spectrum->SetLineStyle(1      ); //AWS20090220
  spectrum->SetMarkerStyle(22); // triangles
  spectrum->SetMarkerSize (0.5);// 
  
  spectrum->Draw("L");  // line 
  
 
	 }//if z,x,y
        }//iy
       }//ix
      }//iz

  }//3D


   // see User Manual 3.02b Ch 9 p 166: `:goto Greek, #: end of Greek. Symbols only in ps file
   //   sprintf(workstring1,"`F# = %.0f MV",galplotdef.gcr_spectra_mod_phi[i_mod_phi]   );              //AWS20060316
   // Greek does not work now so remove phi until do it in Latex
   sprintf(workstring1,"%.0f MV",galplotdef.gcr_spectra_mod_phi[i_mod_phi]   );              //AWS20090316
   text->DrawTextNDC(.15 ,.88-i_mod_phi*.022,workstring1);// NDC=normalized coord system           //AWS20060316

  }//i_mod_phi                                                                AWS20060316

  //============= plot data

  plot_gcr_spectra_data(galplotdef.gcr_spectra_Z[i_ZA],galplotdef.gcr_spectra_A[i_ZA], index_mult, Ek_or_momentum); //AWS20130201


  //============= spectrum from emissivities
  
  if( gcr[i_species_gcr].Z==1 && gcr[i_species_gcr].A==1)
    {

  /* emissivity_analysis .cc :
  cr_spectrum_parameters[4]=1.0041e5 ;// reference energy, MeV/nucleon: PAMELA           p
  cr_spectrum_parameters[5]=4.59e-9;  // flux at reference energy :       PAMELA
  cr_spectrum_parameters[4]=1.0493e5 ;  // reference energy, MeV/nucleon: PAMELA        He
  cr_spectrum_parameters[5]=2.42e-10;   // flux at reference energy :     PAMELA
  */
  double p_ref=1e5;// reference momentum MeV
  double p_break=5e3;// break momentum MeV
  double flux_ref=4.59e-9;

  double flux_factor=1.3;
  double g1=2.5;
  double g2=2.8;

  spectrum=new TGraph(gcr[0].n_pgrid);

 for  (ip    =0;  ip    <gcr[0].n_pgrid;ip    ++)
  {

   

   double value;                                          
             
   /*
   if(Ek_or_momentum==1)                                    //AWS20130131 Ek 
   {
    if(index_mult==3)     value *= gcr[0].Ekin[ip];          // AWS20120508
    spectrum->         SetPoint(ip,gcr[0].Ekin[ip],value);   // AWS20120508

    double x,y; spectrum->GetPoint(ip,x,y);
    cout<<"Ekin     spectrum i_ZA="<<i_ZA<<   " Z="<<galplotdef.gcr_spectra_Z[i_ZA]<<" A="<< galplotdef.gcr_spectra_A[i_ZA] <<" gcr[0].Ekin[ip] ="<< gcr[0].Ekin[ip]
   	<<" i_species_gcr="<<i_species_gcr              <<" gcr[i_species_gcr].p[ip] ="<< gcr[i_species_gcr].p[ip] 
        << " value="<<value<<" ip="<<ip<<" x="<<x<<" y="<<y <<endl;
   }
   */


   if(Ek_or_momentum==1)                                     //AWS20130131 Ek
   {
      
     value = flux_ref *  flux_factor * pow(gcr[i_species_gcr].p[ip]/p_ref,  -g2)  ;  
     
     if (  gcr[i_species_gcr].p[ip]<p_break) value*= pow(gcr[i_species_gcr].p[ip]/p_break,  -(g1-g2));

     if(index_mult==2)     value *= pow(    gcr[i_species_gcr].Ekin[ip],2);  
     if(index_mult==3)     value *= pow(    gcr[i_species_gcr].Ekin[ip],3); 
         
     spectrum->         SetPoint(ip,    gcr[i_species_gcr].Ekin[ip], value);   

    double x,y; spectrum->GetPoint(ip,x,y);
    cout<<"kinetic energy spectrum from emissivities="
        << " value="<<value<<" ip="<<ip<<" x="<<x<<" y="<<y <<endl;
   }


   if(Ek_or_momentum==2)                                     //AWS20130131 momentum
   {
      
     value = flux_ref *  flux_factor * pow(gcr[i_species_gcr].p[ip]/p_ref,  -g2)  ;  
     
     if (  gcr[i_species_gcr].p[ip]<p_break) value*= pow(gcr[i_species_gcr].p[ip]/p_break,  -(g1-g2));

     if(index_mult==2)     value *= pow(    gcr[i_species_gcr].p[ip],2);  
     if(index_mult==3)     value *= pow(    gcr[i_species_gcr].p[ip],3); 
         
     spectrum->         SetPoint(ip,    gcr[i_species_gcr].p[ip], value);   

    double x,y; spectrum->GetPoint(ip,x,y);
    cout<<"momentum spectrum from emissivities="
        << " value="<<value<<" ip="<<ip<<" x="<<x<<" y="<<y <<endl;
   }

  } // ip



  spectrum->SetMarkerColor(kRed);
  spectrum->SetMarkerStyle(21); 
  spectrum->SetMarkerStyle(22); // triangles
  spectrum->SetMarkerSize (0.5);// 
  spectrum->SetLineColor(kRed);  
  spectrum->SetLineWidth(3     );  
  spectrum->SetLineStyle(1      ); 
  spectrum->SetMarkerStyle(22); // triangles
  spectrum->SetMarkerSize (0.5);// 
  
  spectrum->Draw("L");  // line 

    } // Z=1 and A=1 protons


  //============== postscript output

  sprintf(workstring1,"Z%d_A%d%",galplotdef.gcr_spectra_Z[i_ZA],galplotdef.gcr_spectra_A[i_ZA]);                         
                                                                          

  sprintf(workstring2,"b_%.1f_%.1f_%.1f_%.1f",
                          galplotdef.lat_min1,galplotdef.lat_max1,
                          galplotdef.lat_min2,galplotdef.lat_max2);

  strcpy(psfile,"plots/");
  if(Ek_or_momentum==1) strcat(psfile,"gcr_kinetic_energy_spectrum_");       //AWS20130131
  if(Ek_or_momentum==2) strcat(psfile,"gcr_momentum_spectrum_");              //AWS20130131
  strcat(psfile,galdef.galdef_ID);
  strcat(psfile,"_"        );
  strcat(psfile,workstring1);
  // strcat(psfile,"_"        );
  // strcat(psfile,workstring2);
  strcat(psfile,".eps");

  cout<<"postscript file="<<psfile<<endl;
  c1->Print(psfile       );


   } // for Ek or momentum AWS20130131

  
  }// for i_ZA



  plot_gcr_spectra_data_legend();//AWS20060621


  //==============


   cout<<" <<<< plot_gcr_spectra   "<<endl;
   return status;
}


 
// declare static to avoid conflict with function in plot_gcr_spectra_ratios.cc

//static

 void Galplot::gcr_modulate(int i_species,double phi,int key)
{

  int iz,ir,ix,iy,ip;

  gcr_modulated=gcr[i_species].cr_density;

  if(phi<1.)return;

  if(gcr[0].n_spatial_dimensions==2)
    {

    for(iz=0;iz<gcr[0].n_zgrid;iz++)
    {
    for(ir=0;ir<gcr[0].n_rgrid;ir++)
     {

       
	//   cout<<"modulate before: ir iz "<<ir<<" "<<iz<<endl;
	//  for(ip=0;ip<gcr[0].n_pgrid;ip++)cout<<gcr_modulated.d2[ir][iz].s[ip]<<endl;

        modulate(gcr[0].Ekin,gcr_modulated.d2[ir][iz].s,
                 gcr[0].n_pgrid,gcr[i_species].Z,gcr[i_species].A,phi,key);

        //  cout<<"modulate after : ir iz "<<ir<<" "<<iz<<endl;
	// for(ip=0;ip<gcr[0].n_pgrid;ip++)cout<<gcr_modulated.d2[ir][iz].s[ip]<<endl;
        
      }//ir
    }//iz


    }//if 2D


  if(gcr[0].n_spatial_dimensions==3)
    {

    for(iz=0;iz<gcr[0].n_zgrid;iz++)
    {
     for(ix=0;ix<gcr[0].n_xgrid;ix++)
     {
     for(iy=0;iy<gcr[0].n_ygrid;iy++)
      {
       


      modulate(gcr[0].Ekin,gcr_modulated.d3[ix][iy][iz].s,
               gcr[0].n_pgrid,gcr[i_species].Z,gcr[i_species].A,phi,key);

        

      }//iy
     }//ix
    }//iz


    }//if 3D


if(galplotdef.verbose==-1100) // selective debug     AWS20060316
      gcr_modulated.print(1.0);


 cout<<" gcr_modulate: phi="<<phi<<" A="<<gcr[i_species].A<<" Z="<<gcr[i_species].Z <<endl;


  return;
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
// function to compute  average value from rmin to rmax
double average_value(double rmin, double rmax, double r1, double r2, double r3, double v1, double v2, double v3, int debug )
{

/*

r1          r2         r3   input grid points
v1          v2         v3   input values 
      rmin      rmax        required range for average
      vmin      vmax        values at rmin, rmax
   vavmin      vavmax       average in ranges left and right of grid point
            
           v                average weighted by range

*/


  double v,vmin,vmax,vavmin,vavmax;

  vmin = v1 + (rmin-r1)/(r2-r1) * (v2-v1); // value at rmin
  vmax = v2 + (rmax-r2)/(r3-r2) * (v3-v2); // value at rmax

  vavmin = (vmin + v2    )/2.0;  // average in rmin, r2
  vavmax = (v2   + vmax  )/2.0;  // average in r2  , rmax

  v = (vavmin * (r2-rmin) + vavmax * (rmax-r2) ) / (rmax-rmin) ; //weighted average of averages

  if(debug==1)
  cout<< "plot_gcr_spectra: average_value: rmin ="<<rmin <<" rmax="<<rmax<<" r1  ="<<r1  <<" r2  ="<<r2  <<" r3  ="<<r3 
       <<" v1  ="<<v1  <<" v2  ="<<v2  <<" v3  ="<<v3  <<" vmin="<<vmin<<" vmax="<<vmax<<" vavmin ="<<vavmin <<" vavmax ="<<vavmax 
       <<" v = "<<v<<endl;

return v;
}
