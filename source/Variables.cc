#include "Variables.h"
#include "Parameters.h"
#include <string>
#include <vector>
#include <iostream>
#include <iomanip>
#include <algorithm>

//Adding variables from parameters 
void Variables::add(const std::vector<std::string> & names, const Parameters &pars){
	//Set up the variables from the parameters
	for (int i = 0; i < names.size(); ++i){
		std::vector<double> parInput;
		pars.getParameter(names[i], parInput);
		if (parInput.size() == 2){
			add(names[i], parInput[0], parInput[1]);
		}else if (parInput.size() == 4){
			add(names[i], parInput[0], parInput[1], parInput[2], parInput[3]);
		}else{
			std::cout<<"Number of values for variable: "<<parInput.size()<<std::endl;
			for (int j = 0; j < parInput.size(); ++j){
				std::cout<<parInput[j]<<" ";
			}
			std::cout<<std::endl;
			throw(VariableError("Variable \""+names[i]+"\" has incorrect number of values.  Only 2 or 4 is accepted"));
		}
	}
}

//Adding variables from another variables object
void Variables::add(const Variables & vars) {
	//Use the insert operator of the map to insert all the element of vars
	fvars.insert(vars.fvars.begin(), vars.fvars.end());
	//Find the maximum length
	fmaxLength = std::max(fmaxLength,vars.fmaxLength);
}

//Adding a variable to the ensemble
void Variables::add(const std::string &name, double value, double error){
	//Check for existance of the variable and throw an error if it already exists
#pragma omp critical
	{
	if (fvars.find(name) != fvars.end()){
		throw(VariableAlreadyCreated("Variable \""+name+"\" already exists; cannot add a variable with the same name"));
	}else{
		//Create the variable struct 
		variable var;
		var.value = value;
		var.error = error;
		var.uSet = false;
		var.lSet = false;
		fvars[name] = var;
		fmaxLength = std::max(fmaxLength,int(name.size()));
	}
	}
}

void Variables::add(const std::string &name, double value, double error, double lowerLimit, double upperLimit){
	//Check for existance of the variable and throw an error if it already exists
	if (fvars.find(name) != fvars.end()){
		throw(VariableAlreadyCreated("Variable \""+name+"\" already exists; cannot add a variable with the same name"));
	}else{
		//Create the variable struct
		variable var;
		var.value = value;
		var.error = error;
		var.uSet = true;
		var.lSet = true;
		var.uLimit = upperLimit;
		var.lLimit = lowerLimit;
		fvars[name] = var;
		fmaxLength = std::max(fmaxLength,int(name.size()));
	}
}

//Set the variable limits
void Variables::setUpperLimit(const std::string &name, double limit){
	//Find the variable and return an error if not found
	varIt it = fvars.find(name);
	if(it == fvars.end()){
		throw(VariableNotFound("Variable \""+name+"\" does not exist; cannot set upper limit"));
	}else{
		(*it).second.uSet = true;
		(*it).second.uLimit = limit;
	}
}

void Variables::setLowerLimit(const std::string &name, double limit){
	//Find the variable and return an error if not found
	varIt it = fvars.find(name);
	if(it == fvars.end()){
		throw(VariableNotFound("Variable \""+name+"\" does not exist; cannot set lower limit"));
	}else{
		(*it).second.lSet = true;
		(*it).second.lLimit = limit;
	}
}

//Get the values of the variables as an array
std::vector<double> Variables::getValueVector() const{
	std::vector<double> values;
	//values.reserve(fvars.size());
	//Loop the fvars map
	for (cvarIt it=fvars.begin(); it != fvars.end(); ++it){
		values.push_back((*it).second.value);
	}
	return values;
}
//Set the values of the variables from an array
void Variables::setValueVector(const std::vector<double> &values){
	//Check the sizes of the vector and fvars
	if (values.size() != fvars.size()){
		throw(VariableError("The size of the value array is not the same as the number of variables"));
	}else{
		//Loop the array and set the values
		varIt vit = fvars.begin();
		for (std::vector<double>::const_iterator it = values.begin(); it != values.end(); ++it, ++vit){
			(*vit).second.value = *it;
		}
	}
}

//Return the value of the variable
double & Variables::value(const std::string &name){
	varIt it = fvars.find(name);
	if (it == fvars.end()){
		throw(VariableNotFound("Variable \""+name+"\" does not exist; cannot access value"));
	}else{
		return (*it).second.value;
	}
}
const double & Variables::value(const std::string &name) const{
	cvarIt it = fvars.find(name);
	if (it == fvars.end()){
		throw(VariableNotFound("Variable \""+name+"\" does not exist; cannot access value"));
	}else{
		return (*it).second.value;
	}
}
double & Variables::operator [] (const std::string &name){
	return value(name);
}
const double & Variables::operator [] (const std::string &name) const{
	return value(name);
}

//Return the error of the variable
double & Variables::error(const std::string &name){
	varIt it = fvars.find(name);
	if (it == fvars.end()){
		throw(VariableNotFound("Variable \""+name+"\" does not exist; cannot access error"));
	}else{
		return (*it).second.error;
	}
}
const double & Variables::error(const std::string &name) const{
	cvarIt it = fvars.find(name);
	if (it == fvars.end()){
		throw(VariableNotFound("Variable \""+name+"\" does not exist; cannot access error"));
	}else{
		return (*it).second.error;
	}
}

//Return the names of the variables in a vector
std::vector<std::string> Variables::getNames() const{
	std::vector<std::string> names;
	names.reserve(fvars.size());
	for (cvarIt it=fvars.begin(); it != fvars.end(); ++it){
		names.push_back((*it).first);
	}
	return names;
}

//Return the upper and lower limits
const double & Variables::upperLimit(const std::string &name) const{
	cvarIt it = fvars.find(name);
	if (it == fvars.end()){
		throw(VariableNotFound("Variable \""+name+"\" does not exist; cannot access upper limit"));
	}else{
		if (! (*it).second.uSet){
			throw(LimitNotSet("Upper limit not set for variable \""+name+"\""));
		}else{
			return (*it).second.uLimit;
		}
	}
}
const double & Variables::lowerLimit(const std::string &name) const{
	cvarIt it = fvars.find(name);
	if (it == fvars.end()){
		throw(VariableNotFound("Variable \""+name+"\" does not exist; cannot access lower limit"));
	}else{
		if (! (*it).second.lSet){
			throw(LimitNotSet("Lower limit not set for variable \""+name+"\""));
		}else{
			return (*it).second.lLimit;
		}
	}
}

//Output operator
std::ostream & operator << (std::ostream &os, const Variables &vars){
	//Print the header
	os << std::setw(vars.fmaxLength) << "Variable Name";
	os << " |    Value    |    Error    | Lower Limit | Upper Limit\n";
	//Loop the variables
	for (Variables::cvarIt it = vars.fvars.begin(); it != vars.fvars.end(); ++it){
		//The name
		os << std::left << std::setw(vars.fmaxLength) << (*it).first << std::setw(3) << " | ";
		//The value and error
		os << std::setw(11) << (*it).second.value << std::setw(3) << " | " << std::setw(11) << (*it).second.error << std::setw(3) << " | ";
		//Check for the lower limit and print
		if ((*it).second.lSet){
			os << std::setw(11) << (*it).second.lLimit << std::setw(3) << " | ";
		}else{
			os << std::setw(11) << "N/A" << std::setw(3) << " | ";
		}
		//Check for the upper limit and print
		if ((*it).second.uSet){
			os << std::setw(11) << (*it).second.uLimit << std::setw(1) << "\n";
		}else{
			os << std::setw(11) << "N/A" << std::setw(1) << "\n";
		}
	}
	return os;
}

void Variables::clear() {
	fvars.clear();
	fmaxLength = 0;
}
