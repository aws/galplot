class RMF
{
 public:

  long n_E;
  double *E_in, *E_lo,*E_hi;
  
  double *matrix1, *matrix2 ,*matrix3;

  double f(double E_in, double E_out);

  int read (char *filename);

};
