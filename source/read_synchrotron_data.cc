
using namespace std;
#include"Galplot.h"                    //AWS20050920
#include"galprop_classes.h"
#include"galprop.h"

#include "fitsio.h" 
#include "slalib.h"

#include "healpix_map.h"                //AWS20110413
#include "healpix_map_fitsio.h"         //AWS20110413

int Galplot::read_synchrotron_data()
{
  int stat;
  int status, ii, jj;
  FITS work;   
  double factor;
  double nu;

  double k=1.38e-16;                // Boltzmann's constant, erg/K
  double I_to_Tb=C*C/ (2.* k);      // intensity to brighness temp: Tb= I_to_Tb * I/nu^2;

  int iWMAP;
  int WMAP_data; // choice of WMAP data

  cout<<" >>>> read_synchrotron_data"<<endl;

  stat=0;


    char  infile[100], outfile[100];

    //--------------------------------------------------------------------------------------------

    // 10 MHz DRAO survey (Caswell et al. 1976  MNRAS 177,601
    // from Tom Landecker 20050729
    // survey is in RA,dec. Northern Hemisphere: -5 <dec<+75
    // NB contains NULL for some regions
    // dimensions 320*45, 0.75*1.8 deg bins
    // here expanded to 480 bins (360 deg) in longitude for convenience
 
    strcpy( infile,configure.fits_directory);
 
    strcat( infile,"drao_10mhz.fits");  

   
   
    cout<<"  reading  10 Mhz data  from file "<<infile<<endl;

    status = 0;         
    work.read(infile); 

    if(work.NAXIS==0)galplotdef.sync_data____10MHz=0; // no data found so don't try to use it AWS20071221

    if(galplotdef.verbose== -806)// selectable debug
    work.print();

// data.synchrotron____10MHz.init(work.NAXES[0],work.NAXES[1],1);
   // expand in ra: 480*0.75=360 deg for convenience
   data.synchrotron____10MHz.init(480,          work.NAXES[1],1);
  
   for (int ib       =0;        ib<work.NAXES[1];       ib++)
   for (int il       =0;        il<work.NAXES[0];       il++)
   {             
           // cout<<work(ib,il)<<endl;
    data.synchrotron____10MHz.d2[il][ib].s[0]=work(il,ib);
                       
   }   
   

   if(galplotdef.verbose== -807)// selectable debug
   {
    cout<<"data.synchrotron____10MHz:"<<endl;
    data.synchrotron____10MHz.print();
   }
    //--------------------------------------------------------------------------------------------

    // 22 MHz DRAO survey (Roger et al. 1999, A&AS 137,7)
    // from Tom Landecker 20050729
    // survey is in RA,dec. Northern Hemisphere: -28<dec<+80

    strcpy( infile,configure.fits_directory);
 
    strcat( infile,"drao_22mhz.fits");  

   
   
    cout<<"  reading  22 Mhz data  from file "<<infile<<endl;

    status = 0;         
    work.read(infile); 

    if(work.NAXIS==0)galplotdef.sync_data____22MHz=0; // no data found so don't try to use it AWS20071221

    if(galplotdef.verbose== -804)// selectable debug
    work.print();

   data.synchrotron____22MHz.init(work.NAXES[0],work.NAXES[1],1);
 
  
   for (int ib       =0;        ib<work.NAXES[1];       ib++)
   for (int il       =0;        il<work.NAXES[0];       il++)
   {             
           // cout<<work(ib,il)<<endl;
    data.synchrotron____22MHz.d2[il][ib].s[0]=work(il,ib);
                       
   }   
   

   if(galplotdef.verbose== -805)// selectable debug
   {
    cout<<"data.synchrotron____22MHz:"<<endl;
    data.synchrotron____22MHz.print();
   }


    //--------------------------------------------------------------------------------------------

    // 45 MHz  allsky survey

    strcpy( infile,configure.fits_directory);
 
    strcat( infile,"lb45.sm5d.g30m.fits");  

   
   
    cout<<"  reading  45 Mhz data  from file "<<infile<<endl;

    status = 0;         
    work.read(infile); 

    if(work.NAXIS==0)galplotdef.sync_data____45MHz=0; // no data found so don't try to use it AWS20071221

    if(galplotdef.verbose== -808)// selectable debug
    work.print();

  


   data.synchrotron____45MHz.init(work.NAXES[0],work.NAXES[1],1);
 
  
   for (int ib       =0;        ib<work.NAXES[1];       ib++)
   for (int il       =0;        il<work.NAXES[0];       il++)
   {             
           // cout<<work(ib,il)<<endl;
    data.synchrotron____45MHz.d2[il][ib].s[0]=work(il,ib);
                       
   }   
   

   if(galplotdef.verbose== -809)// selectable debug
   {
    cout<<"data.synchrotron____45MHz:"<<endl;
    data.synchrotron____45MHz.print();
   }


   //--------------------------------------------------------------------------------------------
   // 150 MHz Parkes-Jodrell Bank allsky survey
   // from P & W Reich
   // from Table 8 of T.L. Landecker and R. Wielebinski, 1970, Australian J. Phys. Supp. 16, 1
   // 'strong sources removed by hand, data delta>25 extrapolated'

    strcpy( infile,configure.fits_directory);
 
    strcat( infile,"mhz150.g30m.fits");  

   
   
    cout<<"  reading  150 MHz data  from file "<<infile<<endl;

    status = 0;         
    work.read(infile); 

    if(work.NAXIS==0)galplotdef.sync_data___150MHz=0; // no data found so don't try to use it AWS20071221


    if(galplotdef.verbose== -819)// selectable debug
    work.print();

  


   data.synchrotron___150MHz.init(work.NAXES[0],work.NAXES[1],1);
 
  
   for (int ib       =0;        ib<work.NAXES[1];       ib++)
   for (int il       =0;        il<work.NAXES[0];       il++)
   {             
           // cout<<work(ib,il)<<endl;
    data.synchrotron___150MHz.d2[il][ib].s[0]=work(il,ib);
                       
   }   
   

   if(galplotdef.verbose== -819)// selectable debug
   {
    cout<<"data.synchrotron___150MHz:"<<endl;
    data.synchrotron___150MHz.print();
   }
   //  work.delete();


     //--------------------------------------------------------------------------------------------

    // 408 MHz Bonn allsky survey

    strcpy( infile,configure.fits_directory);
 
    strcat( infile,"408MHz.fits");  

   
   
    cout<<"  reading  408 Mhz data  from file "<<infile<<endl;

    status = 0;         
    work.read(infile); 

    if(work.NAXIS==0)galplotdef.sync_data___408MHz=0; // no data found so don't try to use it AWS20071221

    if(galplotdef.verbose== -800)// selectable debug
    work.print();

  


   data.synchrotron___408MHz.init(work.NAXES[0],work.NAXES[1],1);
 
  
   for (int ib       =0;        ib<work.NAXES[1];       ib++)
   for (int il       =0;        il<work.NAXES[0];       il++)
   {             
           // cout<<work(ib,il)<<endl;
    data.synchrotron___408MHz.d2[il][ib].s[0]=work(il,ib);
                       
   }   
   

   if(galplotdef.verbose== -801)// selectable debug
   {
    cout<<"data.synchrotron___408MHz:"<<endl;
    data.synchrotron___408MHz.print();
   }
   //  work.delete();




   //-------------------------------------------------------------------------------------------
   // 820 MHz Dwingeloo
   // obtained from http://www.mpifr-bonn.mpg.de/survey.html

    strcpy( infile,configure.fits_directory);
 
    strcat( infile,"Dwingeloo_820MHz.fits");  
 
    cout<<"  reading  Dwingeloo  MHz data  from file "<<infile<<endl;

    status = 0;         
    work.read(infile); 

    if(work.NAXIS==0)galplotdef.sync_data___820MHz=0; // no data found so don't try to use it AWS20071221

    if(galplotdef.verbose== -813)// selectable debug
    work.print();

  


   data.synchrotron___820MHz.init(work.NAXES[0],work.NAXES[1],1);
 
  
   for (int ib       =0;        ib<work.NAXES[1];       ib++)
   for (int il       =0;        il<work.NAXES[0];       il++)
   {             
           // cout<<work(ib,il)<<endl;
    data.synchrotron___820MHz.d2[il][ib].s[0]=work(il,ib);
                       
   }   
   

   if(galplotdef.verbose== -814)// selectable debug
   {
    cout<<"data.synchrotron___820MHz:"<<endl;
    data.synchrotron___820MHz.print();
   }


   //--------------------------------------------------------------------------------------------
   // 1420 MHz Stockert 25m telescope 21 cm
   // obtained from http://www.mpifr-bonn.mpg.de/survey.html
   // replaced by
   // 1420 MHz Stockert + Villa Elisa, from Patricia Reich

    strcpy( infile,configure.fits_directory);

    /*
    strcat( infile,"Stockert_1420MHz.fits");  
 
    cout<<"  reading  Stockert 1420 MHz data  from file "<<infile<<endl;
    */

    strcat( infile,"lb21.sm2d.fits");                                     //AWS20070305
 
    cout<<"  reading  whole-sky 1420 MHz data  from file "<<infile<<endl; //AWS20070305


    status = 0;         
    work.read(infile); 

    if(work.NAXIS==0)galplotdef.sync_data__1420MHz=0; // no data found so don't try to use it AWS20071221

    if(galplotdef.verbose== -811)// selectable debug
    work.print();

  


   data.synchrotron__1420MHz.init(work.NAXES[0],work.NAXES[1],1);
 
  
   for (int ib       =0;        ib<work.NAXES[1];       ib++)
   for (int il       =0;        il<work.NAXES[0];       il++)
   {             
           // cout<<work(ib,il)<<endl;
    data.synchrotron__1420MHz.d2[il][ib].s[0]=work(il,ib);
                       
   }   
   

   if(galplotdef.verbose== -812)// selectable debug
   {
    cout<<"data.synchrotron__1420MHz:"<<endl;
    data.synchrotron__1420MHz.print();
   }
   //  work.delete();

   //--------------------------------------------------------------------------------------------
   // 2326 MHz Rhodes HartRAO 26m telescope, Jonas et al. 1998 MNRAS 297, 977
   // obtained from J. Jonas in HEALPix and converted to l,b.

    strcpy( infile,configure.fits_directory);
 
    strcat( infile,"Rhodes2326_lbmap.fits");  
 
    cout<<"  reading  Rhodes   2326 MHz data  from file "<<infile<<endl;

    status = 0;         
    work.read(infile); 

    if(work.NAXIS==0)galplotdef.sync_data__2326MHz=0; // no data found so don't try to use it AWS20071221

    if(galplotdef.verbose== -815)// selectable debug
    work.print();

  


   data.synchrotron__2326MHz.init(work.NAXES[0],work.NAXES[1],1);
 
  
   for (int ib       =0;        ib<work.NAXES[1];       ib++)
   for (int il       =0;        il<work.NAXES[0];       il++)
   {             
           // cout<<work(ib,il)<<endl;
    data.synchrotron__2326MHz.d2[il][ib].s[0]=work(il,ib);
                       
   }   
   

   if(galplotdef.verbose== -816)// selectable debug
   {
    cout<<"data.synchrotron__2326MHz:"<<endl;
    data.synchrotron__2326MHz.print();
   }
   //  work.delete();


   //--------------------------------------------------------------------------------------------
   //                                     WMAP
   //-------------------------------------------------------------------------------------------

    // WMAP k band 22800 MHz

   WMAP_data=galplotdef.sync_data_WMAP;// 0 = WMAP team 3yr MEM  AWS20071221
                                       // 1-4= Miville-Deschenes models spectral-index-scaled 408 MHz (for 23 GHz but here used for all WMAP bands)

   // Miville-Deschenes models:
   // model 1: no spinning dust,                    mean beta=2.84
   // model 2: spinning dust template = E(B-V) mean sync beta=2.97
   // model 3: constant                             sync beta=3.0
   // model 4: based on polarized maps         mean sync beta=?

 


   
   
    
    // mK


    strcpy( infile,configure.fits_directory);

    if(WMAP_data==0)   strcat( infile,"wmap_k_mem_synch_3yr_v2_lbmap.fits"); // converted to l,b from 3yr data using HEALPix/aws/WMAP_convert.cc 
    if(WMAP_data==1)   strcat( infile,"wmap_miville_deschenes_Haslam_408MHz_scaled_to_23GHz_1.fits");    //AWS20071220
    if(WMAP_data==2)   strcat( infile,"wmap_miville_deschenes_Haslam_408MHz_scaled_to_23GHz_2.fits");  
    if(WMAP_data==3)   strcat( infile,"wmap_miville_deschenes_Haslam_408MHz_scaled_to_23GHz_3.fits");  
    if(WMAP_data==4)   strcat( infile,"wmap_miville_deschenes_Haslam_408MHz_scaled_to_23GHz_4.fits");  

    if(WMAP_data==0)                   cout<<"  reading  22800 Mhz WMAP 3yr data  from file "     <<infile<<endl;
    if(WMAP_data>=1&&WMAP_data<=4)     cout<<"  reading  22800 Mhz scaled from 408MHz  from file "<<infile<<endl;

    status = 0;         
    work.read(infile); // no extension

    if(work.NAXIS==0)galplotdef.sync_data_22800MHz=0; // no data found so don't try to use it AWS20071221

    if(galplotdef.verbose== -802)// selectable debug
    work.print();

 

    data.synchrotron_22800MHz.init(work.NAXES[0],work.NAXES[1],1);
 
  
   for (int ib       =0;        ib<work.NAXES[1];       ib++)
   for (int il       =0;        il<work.NAXES[0];       il++)
   {             
           // cout<<work(ib,il)<<endl;
    data.synchrotron_22800MHz.d2[il][ib].s[0]=work(il,ib);
                       
   }   
   

   if(galplotdef.verbose== -803)// selectable debug
   {
    cout<<"data.synchrotron_22800MHz:"<<endl;
           data.synchrotron_22800MHz.print();
   }
  




  //--------------------------------------------------------------------------------------------
  // WMAP ka band 33000 MHz

   // converted to l,b from 3yr data using HEALPix/aws/WMAP_convert.cc
    // mK

     strcpy( infile,configure.fits_directory);
    if(WMAP_data==0)   strcat( infile,"wmap_ka_mem_synch_3yr_v2_lbmap.fits");  
    if(WMAP_data==1)   strcat( infile,"wmap_miville_deschenes_Haslam_408MHz_scaled_to_33GHz_1.fits");   //AWS20071220 
    if(WMAP_data==2)   strcat( infile,"wmap_miville_deschenes_Haslam_408MHz_scaled_to_33GHz_2.fits");  
    if(WMAP_data==3)   strcat( infile,"wmap_miville_deschenes_Haslam_408MHz_scaled_to_33GHz_3.fits");  
    if(WMAP_data==4)   strcat( infile,"wmap_miville_deschenes_Haslam_408MHz_scaled_to_33GHz_4.fits");  

    if(WMAP_data==0)                   cout<<"  reading  33000 Mhz WMAP 3yr data  from file "     <<infile<<endl;
    if(WMAP_data>=1&&WMAP_data<=4)     cout<<"  reading  33000 Mhz scaled from 408MHz  from file "<<infile<<endl;
    //  cout<<"  reading  33000 Mhz WMAP 3yr data  from file "<<infile<<endl;

    status = 0;         
    work.read(infile); // no extension

    if(work.NAXIS==0)galplotdef.sync_data_33000MHz=0; // no data found so don't try to use it AWS20071221

    if(galplotdef.verbose== -802)// selectable debug
    work.print();

    data.synchrotron_33000MHz.init(work.NAXES[0],work.NAXES[1],1);
    
   for (int ib       =0;        ib<work.NAXES[1];       ib++)
   for (int il       =0;        il<work.NAXES[0];       il++)
   {             
           // cout<<work(ib,il)<<endl;
    data.synchrotron_33000MHz.d2[il][ib].s[0]=work(il,ib);                        
   }   
   
   if(galplotdef.verbose== -803)// selectable debug
   {
    cout<<"data.synchrotron_33000MHz:"<<endl;
           data.synchrotron_33000MHz.print();
   }

  //--------------------------------------------------------------------------------------------
  // WMAP ka band 41000 MHz

   // converted to l,b from 3yr data using HEALPix/aws/WMAP_convert.cc
    // mK

     strcpy( infile,configure.fits_directory);
    if(WMAP_data==0)   strcat( infile,"wmap_q_mem_synch_3yr_v2_lbmap.fits");  
    if(WMAP_data==1)   strcat( infile,"wmap_miville_deschenes_Haslam_408MHz_scaled_to_41GHz_1.fits");  //AWS20071220 
    if(WMAP_data==2)   strcat( infile,"wmap_miville_deschenes_Haslam_408MHz_scaled_to_41GHz_2.fits");  
    if(WMAP_data==3)   strcat( infile,"wmap_miville_deschenes_Haslam_408MHz_scaled_to_41GHz_3.fits");  
    if(WMAP_data==4)   strcat( infile,"wmap_miville_deschenes_Haslam_408MHz_scaled_to_41GHz_4.fits");  

    if(WMAP_data==0)                   cout<<"  reading  41000 Mhz WMAP 3yr data  from file "     <<infile<<endl;
    if(WMAP_data>=1&&WMAP_data<=4)     cout<<"  reading  41000 Mhz scaled from 408MHz  from file "<<infile<<endl;
    //  cout<<"  reading  41000 Mhz WMAP 3yr data  from file "<<infile<<endl;

    status = 0;         
    work.read(infile); // no extension

    if(work.NAXIS==0)galplotdef.sync_data_41000MHz=0; // no data found so don't try to use it AWS20071221

    if(galplotdef.verbose== -802)// selectable debug
    work.print();

    data.synchrotron_41000MHz.init(work.NAXES[0],work.NAXES[1],1);
    
   for (int ib       =0;        ib<work.NAXES[1];       ib++)
   for (int il       =0;        il<work.NAXES[0];       il++)
   {             
           // cout<<work(ib,il)<<endl;
    data.synchrotron_41000MHz.d2[il][ib].s[0]=work(il,ib);                        
   }   
   
   if(galplotdef.verbose== -803)// selectable debug
   {
    cout<<"data.synchrotron_41000MHz:"<<endl;
           data.synchrotron_41000MHz.print();
   }

 //--------------------------------------------------------------------------------------------
  // WMAP ka band 61000 MHz

   // converted to l,b from 3yr data using HEALPix/aws/WMAP_convert.cc
    // mK

     strcpy( infile,configure.fits_directory);
    if(WMAP_data==0)   strcat( infile,"wmap_v_mem_synch_3yr_v2_lbmap.fits");  
    if(WMAP_data==1)   strcat( infile,"wmap_miville_deschenes_Haslam_408MHz_scaled_to_61GHz_1.fits");  //AWS20071220
    if(WMAP_data==2)   strcat( infile,"wmap_miville_deschenes_Haslam_408MHz_scaled_to_61GHz_2.fits");  
    if(WMAP_data==3)   strcat( infile,"wmap_miville_deschenes_Haslam_408MHz_scaled_to_61GHz_3.fits");  
    if(WMAP_data==4)   strcat( infile,"wmap_miville_deschenes_Haslam_408MHz_scaled_to_61GHz_4.fits");  

    if(WMAP_data==0)                   cout<<"  reading  61000 Mhz WMAP 3yr data  from file "     <<infile<<endl;
    if(WMAP_data>=1&&WMAP_data<=4)     cout<<"  reading  61000 Mhz scaled from 408MHz  from file "<<infile<<endl;
    //  cout<<"  reading  61000 Mhz WMAP 3yr data  from file "<<infile<<endl;

    status = 0;         
    work.read(infile); // no extension

    if(work.NAXIS==0)galplotdef.sync_data_61000MHz=0; // no data found so don't try to use it AWS20071221

    if(galplotdef.verbose== -802)// selectable debug
    work.print();

    data.synchrotron_61000MHz.init(work.NAXES[0],work.NAXES[1],1);
    
   for (int ib       =0;        ib<work.NAXES[1];       ib++)
   for (int il       =0;        il<work.NAXES[0];       il++)
   {             
           // cout<<work(ib,il)<<endl;
    data.synchrotron_61000MHz.d2[il][ib].s[0]=work(il,ib);                        
   }   
   
   if(galplotdef.verbose== -803)// selectable debug
   {
    cout<<"data.synchrotron_61000MHz:"<<endl;
           data.synchrotron_61000MHz.print();
   }

 //--------------------------------------------------------------------------------------------
  // WMAP ka band 94000 MHz

   // converted to l,b from 3yr data using HEALPix/aws/WMAP_convert.cc
    // mK

     strcpy( infile,configure.fits_directory);
    if(WMAP_data==0)   strcat( infile,"wmap_w_mem_synch_3yr_v2_lbmap.fits");  
    if(WMAP_data==1)   strcat( infile,"wmap_miville_deschenes_Haslam_408MHz_scaled_to_94GHz_1.fits");  //AWS20071220
    if(WMAP_data==2)   strcat( infile,"wmap_miville_deschenes_Haslam_408MHz_scaled_to_94GHz_2.fits");  
    if(WMAP_data==3)   strcat( infile,"wmap_miville_deschenes_Haslam_408MHz_scaled_to_94GHz_3.fits");  
    if(WMAP_data==4)   strcat( infile,"wmap_miville_deschenes_Haslam_408MHz_scaled_to_94GHz_4.fits");  

    if(WMAP_data==0)                   cout<<"  reading  94000 Mhz WMAP 3yr data  from file "     <<infile<<endl;
    if(WMAP_data>=1&&WMAP_data<=4)     cout<<"  reading  94000 Mhz scaled from 408MHz  from file "<<infile<<endl;
    //  cout<<"  reading  94000 Mhz WMAP 3yr data  from file "<<infile<<endl;

    status = 0;         
    work.read(infile); // no extension

    if(work.NAXIS==0)galplotdef.sync_data_94000MHz=0; // no data found so don't try to use it AWS20071221

    if(galplotdef.verbose== -802)// selectable debug
    work.print();

    data.synchrotron_94000MHz.init(work.NAXES[0],work.NAXES[1],1);
    
   for (int ib       =0;        ib<work.NAXES[1];       ib++)
   for (int il       =0;        il<work.NAXES[0];       il++)
   {             
           // cout<<work(ib,il)<<endl;
    data.synchrotron_94000MHz.d2[il][ib].s[0]=work(il,ib);                        
   }   
   
   if(galplotdef.verbose== -803)// selectable debug
   {
    cout<<"data.synchrotron_94000MHz:"<<endl;
           data.synchrotron_94000MHz.print();
   }



   //////////////////////////////////////////////////////////
   // put surveys on galprop grid
   //////////////////////////////////////////////////////////
   // code base on plot_synchrotron_skymaps.cc

 double CRVAL1,CRPIX1,CDELT1,CRVAL2,CRPIX2,CDELT2; 
 int n_rgrid,n_zgrid;
 double l,b;
 int il,ib;
 int ill,ibb;
 double ra,dec;
 int ira,idec;
 double dtr=acos(-1.0)/180.;
 Distribution n_use;

  data.synchrotron_skymap____10MHz.init(galaxy.n_long,galaxy.n_lat,1);//AWS20070112
  data.synchrotron_skymap____22MHz.init(galaxy.n_long,galaxy.n_lat,1);//AWS20070112
  data.synchrotron_skymap____45MHz.init(galaxy.n_long,galaxy.n_lat,1);//AWS20070112
  data.synchrotron_skymap___150MHz.init(galaxy.n_long,galaxy.n_lat,1);//AWS20070315
  data.synchrotron_skymap___408MHz.init(galaxy.n_long,galaxy.n_lat,1);//AWS20070112
  data.synchrotron_skymap___820MHz.init(galaxy.n_long,galaxy.n_lat,1);//AWS20070221
  data.synchrotron_skymap__1420MHz.init(galaxy.n_long,galaxy.n_lat,1);//AWS20070112
  data.synchrotron_skymap__2326MHz.init(galaxy.n_long,galaxy.n_lat,1);//AWS20070219
  data.synchrotron_skymap_22800MHz.init(galaxy.n_long,galaxy.n_lat,1);//AWS20070112
  data.synchrotron_skymap_33000MHz.init(galaxy.n_long,galaxy.n_lat,1);//AWS20070606
  data.synchrotron_skymap_41000MHz.init(galaxy.n_long,galaxy.n_lat,1);//AWS20070606
  data.synchrotron_skymap_61000MHz.init(galaxy.n_long,galaxy.n_lat,1);//AWS20070606
  data.synchrotron_skymap_94000MHz.init(galaxy.n_long,galaxy.n_lat,1);//AWS20070606

  n_use.init(galaxy.n_long,galaxy.n_lat,1);//AWS20070112

  // -----------------------------------  10 MHz
  
  //  10 MHz map:  320 * 45   RA,dec 
  
  CRVAL1  =        .23900443E+03;          // REF COORD VALUE (DEG)
  CRPIX1  =        .10000000E+01;          // REF POINT PIXEL
  CDELT1  =       -.75000000E+00;          // COORD VALUE INCREMENT (DEG)
  CRVAL2  =       -.53772001E+01;          // REF COORD VALUE (DEG)
  CRPIX2  =        .10000000E+01;          // REF POINT PIXEL
  CDELT2  =        .17979165E+01;          // COORD VALUE INCREMENT (DEG)

  n_rgrid = data.synchrotron____10MHz.n_rgrid;
  n_zgrid = data.synchrotron____10MHz.n_zgrid;

  n_use=0.;

  for( ira=0; ira <n_rgrid;  ira++)
  for(idec=0; idec<n_zgrid; idec++)
  {
    ra=CRVAL1+CDELT1*(ira -CRPIX1+1);
   dec=CRVAL2+CDELT2*(idec-CRPIX2+1);
   slaEqgal(ra*dtr,dec*dtr,&l,&b); l/=dtr; b/=dtr;

   if(l<0.0)l+=360.0;    //AWS20070209
   //cout<<"10 MHz ira="<<ira<<" idec="<<idec<<" ra="<<ra<<" dec="<<dec<<" l="<<l<<" b="<<b<<endl;
     ill=(l - galaxy.long_min)/galaxy.d_long;
     ibb=(b - galaxy.lat_min )/galaxy.d_lat ;

     if(ill>=0 && ill<galaxy.n_long && ibb>=0 && ibb<galaxy.n_lat)
       {
         if(galplotdef.verbose== -817)// selectable debug
         cout<<" 10 MHz ira="<<ira<<" idec="<<idec<<" l="<<l<<" b="<<b<<" ill="<<ill<<" ibb="<<ibb<<" " 
             <<" 10 MHz:selected point l = "<<l<<" b = "<<b<<" intensity="<< data.synchrotron____10MHz.d2[ira][idec].s[0]<< endl;

       if (data.synchrotron____10MHz.d2[ira][idec].s[0]>0.) // excludes also nan's 
       {
        data.synchrotron_skymap____10MHz.d2[ill][ibb].s[0]   += data.synchrotron____10MHz.d2[ira][idec].s[0];
        n_use                           .d2[ill][ibb].s[0]   +=1; // number of times this pixel sampled
       }//if
     }//if
  }// for


   if(galplotdef.verbose== -817)// selectable debug
     {
      cout<<"data.synchrotron_skymap____10MHz before dividing by n_use:"<<endl;
      data.synchrotron_skymap____10MHz.print();
      cout<<"                                                      n_use:"<<endl;
      n_use.print();
     }




  for(  ill=0;  ill <galaxy.n_long;  ill++)
  for(  ibb=0;  ibb <galaxy.n_lat;   ibb++)
  {
    if  ( n_use.d2[ill][ibb].s[0]>0.) data.synchrotron_skymap____10MHz.d2[ill][ibb].s[0] /= n_use .d2[ill][ibb].s[0];
    if  ( n_use.d2[ill][ibb].s[0]==0. && galplotdef.verbose== -818  )cout<<"read_synchrotron data: warning: no skymap sample for ill,ibb="<<ill<<", "<<ibb<<endl;
  }

  // convert from brightness temp to intensity
  nu= 10.e6; //  10 MHz
  data.synchrotron_skymap____10MHz  /=   ( I_to_Tb / pow(nu,2) )  ;

  
   if(galplotdef.verbose== -818)// selectable debug
     {
      cout<<"data.synchrotron_skymap____10MHz after dividing by n_use and converting to intensity:"<<endl;
      data.synchrotron_skymap____10MHz.print();
     }

   // output as FITS
   work.init(galaxy.n_long,galaxy.n_lat);

   // adjust assuming full sky map 
  work.CRPIX[0]= galaxy.n_long/2;  // centre at longitude near GC
  work.CRPIX[1]= galaxy.n_lat /2;  // centre at latitude  near GC
  work.CDELT[0]=-galaxy.d_long;    // decreasing longitude 
  work.CDELT[1]= galaxy.d_lat;
  work.CRVAL[0]= galaxy.long_min;
  work.CRVAL[1]= galaxy.lat_min +  (work.CRPIX[1] -1 ) * galaxy.d_lat ;



  for(  ill=0;  ill <galaxy.n_long;  ill++)
  for(  ibb=0;  ibb <galaxy.n_lat;   ibb++)
  {
    il=work.CRPIX[0] -ill;
    if(il <  0            ) il+=galaxy.n_long;
    if(il >= galaxy.n_long) il-=galaxy.n_long;
    work(il ,ibb)=data.synchrotron_skymap____10MHz.d2[ill][ibb].s[0] ;
  }
  


  work.smooth(7); // to avoid sampling problem to give nice image, but gives wrong scaling !

  //work.print();
  strcpy(outfile,"plots/synchrotron_skymap_survey____10MHz.fits");
  work.write(outfile);
  work.write_keyword_double(outfile,1,"FREQ", 10.,"frequency (MHz)");
  work.write_keyword_double(outfile,1,"UNITS",1.0,"erg cm-2 sr-1 s-1 Hz-1"); // should use proper keyword
  work.write_keyword_double(outfile,1,"PROGRAM",0,"survey data written by galplot");
  work.write_keyword_double(outfile,1,"NOTE   ",0,"smoothing gives wrong scaling!");

   // -----------------------------------  22 MHz

  //  22 MHz map: 1440*433, RA,dec 
  
CRVAL1  =        .00000000E+00 ;         // REF COORD VALUE (DEG)                 
CRPIX1  =        .14400000E+04 ;         // REF POINT PIXEL                       
CDELT1  =       -.25000000E+00 ;         // COORD VALUE INCREMENT (DEG)                                 
CRVAL2  =       -.28000000E+02 ;         // REF COORD VALUE (DEG)                 
CRPIX2  =        .10000000E+01 ;         // REF POINT PIXEL                       
CDELT2  =        .24999997E+00 ;         // COORD VALUE INCREMENT (DEG) 

  n_rgrid = data.synchrotron____22MHz.n_rgrid;
  n_zgrid = data.synchrotron____22MHz.n_zgrid;

  n_use=0.;

  for( ira=0; ira <n_rgrid;  ira++)
  for(idec=0; idec<n_zgrid; idec++)
  {
    ra=CRVAL1+CDELT1*(ira -CRPIX1+1);
   dec=CRVAL2+CDELT2*(idec-CRPIX2+1);
   slaEqgal(ra*dtr,dec*dtr,&l,&b); l/=dtr; b/=dtr;

   if(l<0.0)l+=360.0;    //AWS20070209
   //cout<<"22 MHz ira="<<ira<<" idec="<<idec<<" ra="<<ra<<" dec="<<dec<<" l="<<l<<" b="<<b<<endl;
     ill=(l - galaxy.long_min)/galaxy.d_long;
     ibb=(b - galaxy.lat_min )/galaxy.d_lat ;

     if(ill>=0 && ill<galaxy.n_long && ibb>=0 && ibb<galaxy.n_lat)
       {
         if(galplotdef.verbose== -817)// selectable debug
         cout<<" 22 MHz ira="<<ira<<" idec="<<idec<<" l="<<l<<" b="<<b<<" ill="<<ill<<" ibb="<<ibb<<" " 
             <<" 22 MHz:selected point l = "<<l<<" b = "<<b<<" intensity="<< data.synchrotron____22MHz.d2[ira][idec].s[0]<< endl;

       if (data.synchrotron____22MHz.d2[ira][idec].s[0]>0.) // excludes also nan's 
       {
        data.synchrotron_skymap____22MHz.d2[ill][ibb].s[0]   += data.synchrotron____22MHz.d2[ira][idec].s[0];
        n_use                           .d2[ill][ibb].s[0]   +=1; // number of times this pixel sampled
       }//if
     }//if
  }// for


   if(galplotdef.verbose== -817)// selectable debug
     {
      cout<<"data.synchrotron_skymap____22MHz before dividing by n_use:"<<endl;
      data.synchrotron_skymap____22MHz.print();
      cout<<"                                                      n_use:"<<endl;
      n_use.print();
     }




  for(  ill=0;  ill <galaxy.n_long;  ill++)
  for(  ibb=0;  ibb <galaxy.n_lat;   ibb++)
  {
    if  ( n_use.d2[ill][ibb].s[0]>0.) data.synchrotron_skymap____22MHz.d2[ill][ibb].s[0] /= n_use .d2[ill][ibb].s[0];
    if  ( n_use.d2[ill][ibb].s[0]==0. && galplotdef.verbose== -818  )cout<<"read_synchrotron data: warning: no skymap sample for ill,ibb="<<ill<<", "<<ibb<<endl;
  }

  // convert from brightness temp to intensity
  nu= 22.e6; //  22 MHz
  data.synchrotron_skymap____22MHz  /=   ( I_to_Tb / pow(nu,2) )  ;

  
   if(galplotdef.verbose== -818)// selectable debug
     {
      cout<<"data.synchrotron_skymap____22MHz after dividing by n_use and converting to intensity:"<<endl;
      data.synchrotron_skymap____22MHz.print();
     }

   // output as FITS
   work.init(galaxy.n_long,galaxy.n_lat);

   // adjust assuming full sky map 
  work.CRPIX[0]= galaxy.n_long/2;  // centre at longitude near GC
  work.CRPIX[1]= galaxy.n_lat /2;  // centre at latitude  near GC
  work.CDELT[0]=-galaxy.d_long;    // decreasing longitude 
  work.CDELT[1]= galaxy.d_lat;
  work.CRVAL[0]= galaxy.long_min;
  work.CRVAL[1]= galaxy.lat_min +  (work.CRPIX[1] -1 ) * galaxy.d_lat ;



  for(  ill=0;  ill <galaxy.n_long;  ill++)
  for(  ibb=0;  ibb <galaxy.n_lat;   ibb++)
  {
    il=work.CRPIX[0] -ill;
    if(il <  0            ) il+=galaxy.n_long;
    if(il >= galaxy.n_long) il-=galaxy.n_long;
    work(il ,ibb)=data.synchrotron_skymap____22MHz.d2[ill][ibb].s[0] ;
  }
  


  //work.print();
  strcpy(outfile,"plots/synchrotron_skymap_survey____22MHz.fits");
  work.write(outfile);
  work.write_keyword_double(outfile,1,"FREQ", 22.,"frequency (MHz)");
  work.write_keyword_double(outfile,1,"UNITS",1.0,"erg cm-2 sr-1 s-1 Hz-1"); // should use proper keyword
  work.write_keyword_double(outfile,1,"PROGRAM",0,"survey data written by galplot");



  // -----------------------------------  45 MHz

  // Galactic coordinates
  
  

  CRVAL1  =    180.0             ;
  CDELT1  =     -0.5             ;
  CRPIX1  =      1.0             ;
  
  CRVAL2  =   -9.00000000000E+01 ;
  CDELT2  =      0.5             ;
  CRPIX2  =      1.0             ;

  n_rgrid = data.synchrotron____45MHz.n_rgrid;
  n_zgrid = data.synchrotron____45MHz.n_zgrid;


 n_use=0.;

  for(  il=0;  il <n_rgrid;   il++)
  for(  ib=0;  ib <n_zgrid;   ib++)
  {
     l=CRVAL1+CDELT1*(il -CRPIX1+1);
     b=CRVAL2+CDELT2*(ib- CRPIX2+1);
   
     if(l<0.0) l += 360.0;


     ill=(l - galaxy.long_min)/galaxy.d_long;
     ibb=(b - galaxy.lat_min )/galaxy.d_lat ;

     if(ill>=0 && ill<galaxy.n_long && ibb>=0 && ibb<galaxy.n_lat)
       {
     //  cout<<" 45 MHz il="<<il<<" ib="<<" l="<<l<<" b="<<b<<" ill="<<ill<<" ibb="<<ibb<<endl;

       //  cout<<" 45 MHz:selected point l = "<<l<<" b = "<<b<<" intensity="<< data.synchrotron____45MHz.d2[il][ib].s[0]<< endl;
       if (data.synchrotron____45MHz.d2[il][ib].s[0]>0.) // excludes also nan's 
       {
     
        data.synchrotron_skymap____45MHz.d2[ill][ibb].s[0]   += data.synchrotron____45MHz.d2[il][ib].s[0];
        n_use                           .d2[ill][ibb].s[0]   +=1; // number of times this pixel sampled
       }//if
     }//if
  }//for


   if(galplotdef.verbose== -810)// selectable debug
     {
      cout<<"data.synchrotron_skymap____45MHz before dividing by n_use:"<<endl;
      data.synchrotron_skymap____45MHz.print();
      cout<<"                                                      n_use:"<<endl;
      n_use.print();
     }

  for(  ill=0;  ill <galaxy.n_long;  ill++)
  for(  ibb=0;  ibb <galaxy.n_lat;   ibb++)
  {
    if  ( n_use.d2[ill][ibb].s[0]>0.) data.synchrotron_skymap____45MHz.d2[ill][ibb].s[0] /= n_use .d2[ill][ibb].s[0];
    if  ( n_use.d2[ill][ibb].s[0]==0. && galplotdef.verbose== -810  )cout<<"read_synchrotron data: warning: no skymap sample for ill,ibb="<<ill<<", "<<ibb<<endl;
  }

  // convert from brightness temp to intensity
  nu= 45.e6; //  45 MHz
  data.synchrotron_skymap____45MHz  /=   ( I_to_Tb / pow(nu,2) )  ;

  
   if(galplotdef.verbose== -810)// selectable debug
     {
      cout<<"data.synchrotron_skymap____45MHz after dividing by n_use and converting to intensity:"<<endl;
      data.synchrotron_skymap____45MHz.print();
     }

   // output as FITS
   work.init(galaxy.n_long,galaxy.n_lat);

   // adjust assuming full sky map 
  work.CRPIX[0]= galaxy.n_long/2;  // centre at longitude near GC
  work.CRPIX[1]= galaxy.n_lat /2;  // centre at latitude  near GC
  work.CDELT[0]=-galaxy.d_long;    // decreasing longitude 
  work.CDELT[1]= galaxy.d_lat;
  work.CRVAL[0]= galaxy.long_min;
  work.CRVAL[1]= galaxy.lat_min +  (work.CRPIX[1] -1 ) * galaxy.d_lat ;



  for(  ill=0;  ill <galaxy.n_long;  ill++)
  for(  ibb=0;  ibb <galaxy.n_lat;   ibb++)
  {
    il=work.CRPIX[0] -ill;
    if(il <  0            ) il+=galaxy.n_long;
    if(il >= galaxy.n_long) il-=galaxy.n_long;
    work(il ,ibb)=data.synchrotron_skymap____45MHz.d2[ill][ibb].s[0] ;
  }
  


  //work.print();
  strcpy(outfile,"plots/synchrotron_skymap_survey____45MHz.fits");
  work.write(outfile);
  work.write_keyword_double(outfile,1,"FREQ", 45.,"frequency (MHz)");
  work.write_keyword_double(outfile,1,"UNITS",1.0,"erg cm-2 sr-1 s-1 Hz-1"); // should use proper keyword
  work.write_keyword_double(outfile,1,"PROGRAM",0,"survey data written by galplot");


  // -----------------------------------   150 MHz
  // units are  K 
  // Parkes - Jodrell Bank all-sky 



 CRVAL1  =          180.0000000 ;
 CDELT1  =        -0.5000000000 ;
 CRPIX1  =          1.000000000 ;


 CRVAL2  =         -90.00000000 ;
 CDELT2  =         0.5000000000 ;
 CRPIX2  =          1.000000000 ;


 //CRVAL3  =          150000000.0 / frequency (Hz)
 //CDELT3  =          2000.000000 / bandwidth of observation (Hz)

                                                                                                                                                                                                                                            

  n_rgrid = data.synchrotron___150MHz.n_rgrid;
  n_zgrid = data.synchrotron___150MHz.n_zgrid;


 n_use=0.;

  for(  il=0;  il <n_rgrid;   il++)
  for(  ib=0;  ib <n_zgrid;   ib++)
  {
     l=CRVAL1+CDELT1*(il -CRPIX1+1);
     b=CRVAL2+CDELT2*(ib- CRPIX2+1);
   
     if(l<0.0) l += 360.0;


     ill=(l - galaxy.long_min)/galaxy.d_long;
     ibb=(b - galaxy.lat_min )/galaxy.d_lat ;

     if(ill>=0 && ill<galaxy.n_long && ibb>=0 && ibb<galaxy.n_lat)
       {
     //  cout<<" 150 MHz il="<<il<<" ib="<<" l="<<l<<" b="<<b<<" ill="<<ill<<" ibb="<<ibb<<endl;

       //  cout<<" 150 MHz:selected point l = "<<l<<" b = "<<b<<" intensity="<< data.synchrotron___150MHz.d2[il][ib].s[0]<< endl;
       if (data.synchrotron___150MHz.d2[il][ib].s[0]>0.) // excludes also nan's 
       {
     
        data.synchrotron_skymap___150MHz.d2[ill][ibb].s[0]   += data.synchrotron___150MHz.d2[il][ib].s[0];
        n_use                           .d2[ill][ibb].s[0]   +=1; // number of times this pixel sampled
       }//if
     }//if
  }//for


   if(galplotdef.verbose== -810)// selectable debug
     {
      cout<<"data.synchrotron_skymap___150MHz before dividing by n_use:"<<endl;
      data.synchrotron_skymap___150MHz.print();
      cout<<"                                                      n_use:"<<endl;
      n_use.print();
     }

  for(  ill=0;  ill <galaxy.n_long;  ill++)
  for(  ibb=0;  ibb <galaxy.n_lat;   ibb++)
  {
    if  ( n_use.d2[ill][ibb].s[0]>0.) data.synchrotron_skymap___150MHz.d2[ill][ibb].s[0] /= n_use .d2[ill][ibb].s[0];
    if  ( n_use.d2[ill][ibb].s[0]==0. && galplotdef.verbose== -810  )cout<<"read_synchrotron data: warning: no skymap sample for ill,ibb="<<ill<<", "<<ibb<<endl;
  }


  //              (original units are    K)
  

  // convert from brightness temp to intensity
  nu= 150.e6; //  150 MHz
  data.synchrotron_skymap___150MHz  /=   ( I_to_Tb / pow(nu,2) )  ;

  
   if(galplotdef.verbose== -810)// selectable debug
     {
      cout<<"data.synchrotron_skymap___150MHz after dividing by n_use and converting to intensity:"<<endl;
      data.synchrotron_skymap___150MHz.print();
     }

   // output as FITS
   work.init(galaxy.n_long,galaxy.n_lat);

   // adjust assuming full sky map 
  work.CRPIX[0]= galaxy.n_long/2;  // centre at longitude near GC
  work.CRPIX[1]= galaxy.n_lat /2;  // centre at latitude  near GC
  work.CDELT[0]=-galaxy.d_long;    // decreasing longitude 
  work.CDELT[1]= galaxy.d_lat;
  work.CRVAL[0]= galaxy.long_min;
  work.CRVAL[1]= galaxy.lat_min +  (work.CRPIX[1] -1 ) * galaxy.d_lat ;



  for(  ill=0;  ill <galaxy.n_long;  ill++)
  for(  ibb=0;  ibb <galaxy.n_lat;   ibb++)
  {
    il=work.CRPIX[0] -ill;
    if(il <  0            ) il+=galaxy.n_long;
    if(il >= galaxy.n_long) il-=galaxy.n_long;
    work(il ,ibb)=data.synchrotron_skymap___150MHz.d2[ill][ibb].s[0] ;
  }
  


  //work.print();
  strcpy(outfile,"plots/synchrotron_skymap_survey___150MHz.fits");
  work.write(outfile);
  work.write_keyword_double(outfile,1,"FREQ", 150.,"frequency (MHz)");
  work.write_keyword_double(outfile,1,"UNITS",1.0,"erg cm-2 sr-1 s-1 Hz-1"); // should use proper keyword
  work.write_keyword_double(outfile,1,"PROGRAM",0,"survey data written by galplot");




  // ----------------------------------- 408 MHz

  // Galactic coordinates
  // the FITS header is WRONG ! :
  /*
  CRVAL1  =    2.65610840000E+02 ;
  CDELT1  =     -3.515625000E-01 ;
  CRPIX1  =      5.125000000E+02 ;
  
  CRVAL2  =   -9.00000000000E+01 ;
  CDELT2  =      3.515625000E-01 ;
  CRPIX2  =      2.565000000E+02 ;
  */
  // the following is better:
  CRVAL1  =      0.0             ;
  CDELT1  =     -3.515625000E-01 ;
  CRPIX1  =      5.125000000E+02 ;
  
  CRVAL2  =   -9.00000000000E+01 ;
  CDELT2  =      3.515625000E-01 ;
  CRPIX2  =      1.0             ;

  n_rgrid = data.synchrotron___408MHz.n_rgrid;
  n_zgrid = data.synchrotron___408MHz.n_zgrid;

  n_use=0.;

  for(  il=0;  il <n_rgrid;   il++)
  for(  ib=0;  ib <n_zgrid;   ib++)
  {
     l=CRVAL1+CDELT1*(il -CRPIX1+1);
     b=CRVAL2+CDELT2*(ib- CRPIX2+1);
   
     if(l<0.0) l += 360.0;


     ill=(l - galaxy.long_min)/galaxy.d_long;
     ibb=(b - galaxy.lat_min )/galaxy.d_lat ;

     if(ill>=0 && ill<galaxy.n_long && ibb>=0 && ibb<galaxy.n_lat)
       {
     //  cout<<"408 MHz il="<<il<<" ib="<<" l="<<l<<" b="<<b<<" ill="<<ill<<" ibb="<<ibb<<endl;

       //  cout<<"408 MHz:selected point l = "<<l<<" b = "<<b<<" intensity="<< data.synchrotron___408MHz.d2[il][ib].s[0]<< endl;
       if (data.synchrotron___408MHz.d2[il][ib].s[0]>0.) // excludes also nan's 
       {
     
        data.synchrotron_skymap___408MHz.d2[ill][ibb].s[0]   += data.synchrotron___408MHz.d2[il][ib].s[0];
        n_use                           .d2[ill][ibb].s[0]   +=1; // number of times this pixel sampled
       }//if
     }//if
  }//for


   if(galplotdef.verbose== -810)// selectable debug
     {
      cout<<"data.synchrotron_skymap___408MHz before dividing by n_use:"<<endl;
      data.synchrotron_skymap___408MHz.print();
      cout<<"                                                      n_use:"<<endl;
      n_use.print();
     }

  for(  ill=0;  ill <galaxy.n_long;  ill++)
  for(  ibb=0;  ibb <galaxy.n_lat;   ibb++)
  {
    if  ( n_use.d2[ill][ibb].s[0]>0.) data.synchrotron_skymap___408MHz.d2[ill][ibb].s[0] /= n_use .d2[ill][ibb].s[0];
    if  ( n_use.d2[ill][ibb].s[0]==0. && galplotdef.verbose== -810  )cout<<"read_synchrotron data: warning: no skymap sample for ill,ibb="<<ill<<", "<<ibb<<endl;
  }

  // convert from brightness temp to intensity
  nu=408.e6; // 408 MHz
  data.synchrotron_skymap___408MHz  /=   ( I_to_Tb / pow(nu,2) )  ;

  
   if(galplotdef.verbose== -810)// selectable debug
     {
      cout<<"data.synchrotron_skymap___408MHz after dividing by n_use and converting to intensity:"<<endl;
      data.synchrotron_skymap___408MHz.print();
     }

   // output as FITS
   work.init(galaxy.n_long,galaxy.n_lat);

   // adjust assuming full sky map 
  work.CRPIX[0]= galaxy.n_long/2;  // centre at longitude near GC
  work.CRPIX[1]= galaxy.n_lat /2;  // centre at latitude  near GC
  work.CDELT[0]=-galaxy.d_long;    // decreasing longitude 
  work.CDELT[1]= galaxy.d_lat;
  work.CRVAL[0]= galaxy.long_min;
  work.CRVAL[1]= galaxy.lat_min +  (work.CRPIX[1] -1 ) * galaxy.d_lat ;



  for(  ill=0;  ill <galaxy.n_long;  ill++)
  for(  ibb=0;  ibb <galaxy.n_lat;   ibb++)
  {
    il=work.CRPIX[0] -ill;
    if(il <  0            ) il+=galaxy.n_long;
    if(il >= galaxy.n_long) il-=galaxy.n_long;
    work(il ,ibb)=data.synchrotron_skymap___408MHz.d2[ill][ibb].s[0] ;
  }
  


  //work.print();
  strcpy(outfile,"plots/synchrotron_skymap_survey___408MHz.fits");
  work.write(outfile);
  work.write_keyword_double(outfile,1,"FREQ",408.,"frequency (MHz)");
  work.write_keyword_double(outfile,1,"UNITS",1.0,"erg cm-2 sr-1 s-1 Hz-1"); // should use proper keyword
  work.write_keyword_double(outfile,1,"PROGRAM",0,"survey data written by galplot");





  // ------------------------------------  820 MHz

  // Galactic

CRVAL1  =                    0.;        
CRPIX1  =                  361.;      
CDELT1  =                 -0.5 ;      
CRVAL2  =                    0.;     
CRPIX2  =                  181.;        
CDELT2  =                  0.5 ;    


  n_rgrid = data.synchrotron___820MHz.n_rgrid;
  n_zgrid = data.synchrotron___820MHz.n_zgrid;


  n_use=0.;

  for(  il=0;  il <n_rgrid;   il++)
  for(  ib=0;  ib <n_zgrid;   ib++)
  {
     l=CRVAL1+CDELT1*(il -CRPIX1+1);
     b=CRVAL2+CDELT2*(ib- CRPIX2+1);
   
     if(l<0.0) l += 360.0;


     ill=(l - galaxy.long_min)/galaxy.d_long;
     ibb=(b - galaxy.lat_min )/galaxy.d_lat ;

     if(ill>=0 && ill<galaxy.n_long && ibb>=0 && ibb<galaxy.n_lat)
       {
     //  cout<<"820 MHz il="<<il<<" ib="<<" l="<<l<<" b="<<b<<" ill="<<ill<<" ibb="<<ibb<<endl;

     //    cout<<"820 MHz:selected point l = "<<l<<" b = "<<b<<" intensity="<< data.synchrotron___820MHz.d2[il][ib].s[0]<< endl;
       if (data.synchrotron___820MHz.d2[il][ib].s[0]>0.) // excludes also nan's 
       {
     
        data.synchrotron_skymap___820MHz.d2[ill][ibb].s[0]   += data.synchrotron___820MHz.d2[il][ib].s[0];
        n_use                           .d2[ill][ibb].s[0]   +=1; // number of times this pixel sampled
       }//if
     }//if
  }//for


   if(galplotdef.verbose== -810)// selectable debug
     {
      cout<<"data.synchrotron_skymap___820MHz before dividing by n_use:"<<endl;
      data.synchrotron_skymap___820MHz.print();
      cout<<"                                                      n_use:"<<endl;
      n_use.print();
     }

  for(  ill=0;  ill <galaxy.n_long;  ill++)
  for(  ibb=0;  ibb <galaxy.n_lat;   ibb++)
  {
    if  ( n_use.d2[ill][ibb].s[0]>0.) data.synchrotron_skymap___820MHz.d2[ill][ibb].s[0] /= n_use .d2[ill][ibb].s[0];
    if  ( n_use.d2[ill][ibb].s[0]==0. && galplotdef.verbose== -810  )cout<<"read_synchrotron data: warning: no skymap sample for ill,ibb="<<ill<<", "<<ibb<<endl;
  }


  // convert to K (original units are 0.1K)
  data.synchrotron_skymap___820MHz *= 0.1;

  // convert from brightness temp to intensity
  nu=820.e6; // 820 MHz
  data.synchrotron_skymap___820MHz  /=   ( I_to_Tb / pow(nu,2) )  ;

  
   if(galplotdef.verbose== -810)// selectable debug
     {
      cout<<"data.synchrotron_skymap___820MHz after dividing by n_use and converting to intensity:"<<endl;
      data.synchrotron_skymap___820MHz.print();
     }

   // output as FITS
   work.init(galaxy.n_long,galaxy.n_lat);

   // adjust assuming full sky map 
  work.CRPIX[0]= galaxy.n_long/2;  // centre at longitude near GC
  work.CRPIX[1]= galaxy.n_lat /2;  // centre at latitude  near GC
  work.CDELT[0]=-galaxy.d_long;    // decreasing longitude 
  work.CDELT[1]= galaxy.d_lat;
  work.CRVAL[0]= galaxy.long_min;
  work.CRVAL[1]= galaxy.lat_min +  (work.CRPIX[1] -1 ) * galaxy.d_lat ;



  for(  ill=0;  ill <galaxy.n_long;  ill++)
  for(  ibb=0;  ibb <galaxy.n_lat;   ibb++)
  {
    il=work.CRPIX[0] -ill;
    if(il <  0            ) il+=galaxy.n_long;
    if(il >= galaxy.n_long) il-=galaxy.n_long;
    work(il ,ibb)=data.synchrotron_skymap___820MHz.d2[ill][ibb].s[0] ;
  }
  


  //work.print();
  strcpy(outfile,"plots/synchrotron_skymap_survey___820MHz.fits");
  work.write(outfile);
  work.write_keyword_double(outfile,1,"FREQ",820.,"frequency (MHz)");
  work.write_keyword_double(outfile,1,"UNITS",1.0,"erg cm-2 sr-1 s-1 Hz-1"); // should use proper keyword
  work.write_keyword_double(outfile,1,"PROGRAM",0,"survey data written by galplot");






  // -----------------------------------  1420 MHz
  // units are mK 
  // all-sky Stockert-Villa Eliza has  coordinate keywords equivalent to Stockert northern survey

CRVAL1  =                    0.;         
CDELT1  =                -0.25 ;        
CRPIX1  =                  721.;  
CRVAL2  =                    0.;          
CDELT2  =                 0.25 ;        
CRPIX2  =                  361.;  

  n_rgrid = data.synchrotron__1420MHz.n_rgrid;
  n_zgrid = data.synchrotron__1420MHz.n_zgrid;


 n_use=0.;

  for(  il=0;  il <n_rgrid;   il++)
  for(  ib=0;  ib <n_zgrid;   ib++)
  {
     l=CRVAL1+CDELT1*(il -CRPIX1+1);
     b=CRVAL2+CDELT2*(ib- CRPIX2+1);
   
     if(l<0.0) l += 360.0;


     ill=(l - galaxy.long_min)/galaxy.d_long;
     ibb=(b - galaxy.lat_min )/galaxy.d_lat ;

     if(ill>=0 && ill<galaxy.n_long && ibb>=0 && ibb<galaxy.n_lat)
       {
     //  cout<<"1420 MHz il="<<il<<" ib="<<" l="<<l<<" b="<<b<<" ill="<<ill<<" ibb="<<ibb<<endl;

       //  cout<<"1420 MHz:selected point l = "<<l<<" b = "<<b<<" intensity="<< data.synchrotron__1420MHz.d2[il][ib].s[0]<< endl;
       if (data.synchrotron__1420MHz.d2[il][ib].s[0]>0.) // excludes also nan's 
       {
     
        data.synchrotron_skymap__1420MHz.d2[ill][ibb].s[0]   += data.synchrotron__1420MHz.d2[il][ib].s[0];
        n_use                           .d2[ill][ibb].s[0]   +=1; // number of times this pixel sampled
       }//if
     }//if
  }//for


   if(galplotdef.verbose== -810)// selectable debug
     {
      cout<<"data.synchrotron_skymap__1420MHz before dividing by n_use:"<<endl;
      data.synchrotron_skymap__1420MHz.print();
      cout<<"                                                      n_use:"<<endl;
      n_use.print();
     }

  for(  ill=0;  ill <galaxy.n_long;  ill++)
  for(  ibb=0;  ibb <galaxy.n_lat;   ibb++)
  {
    if  ( n_use.d2[ill][ibb].s[0]>0.) data.synchrotron_skymap__1420MHz.d2[ill][ibb].s[0] /= n_use .d2[ill][ibb].s[0];
    if  ( n_use.d2[ill][ibb].s[0]==0. && galplotdef.verbose== -810  )cout<<"read_synchrotron data: warning: no skymap sample for ill,ibb="<<ill<<", "<<ibb<<endl;
  }


  // convert to K (original units are   mK)
  data.synchrotron_skymap__1420MHz *= 1.e-3;

  // convert from brightness temp to intensity
  nu=1420.e6; // 1420 MHz
  data.synchrotron_skymap__1420MHz  /=   ( I_to_Tb / pow(nu,2) )  ;

  
   if(galplotdef.verbose== -810)// selectable debug
     {
      cout<<"data.synchrotron_skymap__1420MHz after dividing by n_use and converting to intensity:"<<endl;
      data.synchrotron_skymap__1420MHz.print();
     }

   // output as FITS
   work.init(galaxy.n_long,galaxy.n_lat);

   // adjust assuming full sky map 
  work.CRPIX[0]= galaxy.n_long/2;  // centre at longitude near GC
  work.CRPIX[1]= galaxy.n_lat /2;  // centre at latitude  near GC
  work.CDELT[0]=-galaxy.d_long;    // decreasing longitude 
  work.CDELT[1]= galaxy.d_lat;
  work.CRVAL[0]= galaxy.long_min;
  work.CRVAL[1]= galaxy.lat_min +  (work.CRPIX[1] -1 ) * galaxy.d_lat ;



  for(  ill=0;  ill <galaxy.n_long;  ill++)
  for(  ibb=0;  ibb <galaxy.n_lat;   ibb++)
  {
    il=work.CRPIX[0] -ill;
    if(il <  0            ) il+=galaxy.n_long;
    if(il >= galaxy.n_long) il-=galaxy.n_long;
    work(il ,ibb)=data.synchrotron_skymap__1420MHz.d2[ill][ibb].s[0] ;
  }
  


  //work.print();
  strcpy(outfile,"plots/synchrotron_skymap_survey__1420MHz.fits");
  work.write(outfile);
  work.write_keyword_double(outfile,1,"FREQ",1420.,"frequency (MHz)");
  work.write_keyword_double(outfile,1,"UNITS",1.0,"erg cm-2 sr-1 s-1 Hz-1"); // should use proper keyword
  work.write_keyword_double(outfile,1,"PROGRAM",0,"survey data written by galplot");


  // -----------------------------------  2326 MHz

CRVAL1  =                 0.25 ;         
CDELT1  =                -0.50 ;        
CRPIX1  =                  360.;  
CRVAL2  =               -89.75 ;          
CDELT2  =                 0.50 ;        
CRPIX2  =                    1.;  
  

  n_rgrid = data.synchrotron__2326MHz.n_rgrid;
  n_zgrid = data.synchrotron__2326MHz.n_zgrid;

 n_use=0.;

  for(  il=0;  il <n_rgrid;   il++)
  for(  ib=0;  ib <n_zgrid;   ib++)
  {
     l=CRVAL1+CDELT1*(il -CRPIX1+1);
     b=CRVAL2+CDELT2*(ib- CRPIX2+1);
   
     if(l<0.0) l += 360.0;


     ill=(l - galaxy.long_min)/galaxy.d_long;
     ibb=(b - galaxy.lat_min )/galaxy.d_lat ;

     if(ill>=0 && ill<galaxy.n_long && ibb>=0 && ibb<galaxy.n_lat)
       {
     //  cout<<"2326 MHz il="<<il<<" ib="<<" l="<<l<<" b="<<b<<" ill="<<ill<<" ibb="<<ibb<<endl;

       //  cout<<"2326 MHz:selected point l = "<<l<<" b = "<<b<<" intensity="<< data.synchrotron__2326MHz.d2[il][ib].s[0]<< endl;
       if (data.synchrotron__2326MHz.d2[il][ib].s[0]>0.) // excludes also nan's 
       {
     
        data.synchrotron_skymap__2326MHz.d2[ill][ibb].s[0]   += data.synchrotron__2326MHz.d2[il][ib].s[0];
        n_use                           .d2[ill][ibb].s[0]   +=1; // number of times this pixel sampled
       }//if
     }//if
  }//for


   if(galplotdef.verbose== -810)// selectable debug
     {
      cout<<"data.synchrotron_skymap__2326MHz before dividing by n_use:"<<endl;
      data.synchrotron_skymap__2326MHz.print();
      cout<<"                                                      n_use:"<<endl;
      n_use.print();
     }

  for(  ill=0;  ill <galaxy.n_long;  ill++)
  for(  ibb=0;  ibb <galaxy.n_lat;   ibb++)
  {
    if  ( n_use.d2[ill][ibb].s[0]>0.) data.synchrotron_skymap__2326MHz.d2[ill][ibb].s[0] /= n_use .d2[ill][ibb].s[0];
    if  ( n_use.d2[ill][ibb].s[0]==0. && galplotdef.verbose== -810  )cout<<"read_synchrotron data: warning: no skymap sample for ill,ibb="<<ill<<", "<<ibb<<endl;
  }

  // original units are K
  // convert to K (original units are   mK)          NO AWS20070525
  //  data.synchrotron_skymap__2326MHz *= 1.e-3;

  // convert from brightness temp to intensity
  nu=2326.e6; // 2326 MHz
  data.synchrotron_skymap__2326MHz  /=   ( I_to_Tb / pow(nu,2) )  ;

  
   if(galplotdef.verbose== -810)// selectable debug
     {
      cout<<"data.synchrotron_skymap__2326MHz after dividing by n_use and converting to intensity:"<<endl;
      data.synchrotron_skymap__2326MHz.print();
     }

   // output as FITS
   work.init(galaxy.n_long,galaxy.n_lat);

   // adjust assuming full sky map 
  work.CRPIX[0]= galaxy.n_long/2;  // centre at longitude near GC
  work.CRPIX[1]= galaxy.n_lat /2;  // centre at latitude  near GC
  work.CDELT[0]=-galaxy.d_long;    // decreasing longitude 
  work.CDELT[1]= galaxy.d_lat;
  work.CRVAL[0]= galaxy.long_min;
  work.CRVAL[1]= galaxy.lat_min +  (work.CRPIX[1] -1 ) * galaxy.d_lat ;



  for(  ill=0;  ill <galaxy.n_long;  ill++)
  for(  ibb=0;  ibb <galaxy.n_lat;   ibb++)
  {
    il=work.CRPIX[0] -ill;
    if(il <  0            ) il+=galaxy.n_long;
    if(il >= galaxy.n_long) il-=galaxy.n_long;
    work(il ,ibb)=data.synchrotron_skymap__2326MHz.d2[ill][ibb].s[0] ;
  }
  


  //work.print();
  strcpy(outfile,"plots/synchrotron_skymap_survey__2326MHz.fits");
  work.write(outfile);
  work.write_keyword_double(outfile,1,"FREQ",2326.,"frequency (MHz)");
  work.write_keyword_double(outfile,1,"UNITS",1.0,"erg cm-2 sr-1 s-1 Hz-1"); // should use proper keyword
  work.write_keyword_double(outfile,1,"PROGRAM",0,"survey data written by galplot");


  // --------------------------------------------------------------------------
  //                                    WMAP
  //---------------------------------------------------------------------------



 // ----------------------------------- 22800 MHz
  /*
  // 22800 MHz WMAP map: 719*360, GC centred


  CRVAL1  =                   0.;
  CDELT1  =                  -0.5 ;
  CRPIX1  =                 360.;
  CRVAL2  =                   0. ;
  CDELT2  =                  0.5 ;
  CRPIX2  =                180.5 ;
  */

  // 22800 MHz 3yr WMAP map: 720*360, GC centred    AWS20070223

  CRVAL1  =                 0.25; 
  CRVAL2  =               -89.75; 
  CRPIX1  =                 360.; 
  CRPIX2  =                   1.; 
  CDELT1  =                 -0.5; 
  CDELT2  =                  0.5; 
 

  n_rgrid = data.synchrotron_22800MHz.n_rgrid;
  n_zgrid = data.synchrotron_22800MHz.n_zgrid;

 n_use=0.;

  for(  il=0;  il <n_rgrid;   il++)
  for(  ib=0;  ib <n_zgrid;   ib++)
  {
     l=CRVAL1+CDELT1*(il -CRPIX1+1);
     b=CRVAL2+CDELT2*(ib- CRPIX2+1);
   
     if(l<0.0) l += 360.0;


     ill=(l - galaxy.long_min)/galaxy.d_long;
     ibb=(b - galaxy.lat_min )/galaxy.d_lat ;

     if(ill>=0 && ill<galaxy.n_long && ibb>=0 && ibb<galaxy.n_lat)
       {
     //  cout<<"22800 MHz il="<<il<<" ib="<<" l="<<l<<" b="<<b<<" ill="<<ill<<" ibb="<<ibb<<endl;

       //  cout<<"22800 MHz:selected point l = "<<l<<" b = "<<b<<" intensity="<< data.synchrotron_22800MHz.d2[il][ib].s[0]<< endl;
       if (data.synchrotron_22800MHz.d2[il][ib].s[0]>0.) // excludes also nan's 
       {
     
        data.synchrotron_skymap_22800MHz.d2[ill][ibb].s[0]   += data.synchrotron_22800MHz.d2[il][ib].s[0];
        data.synchrotron_skymap_33000MHz.d2[ill][ibb].s[0]   += data.synchrotron_33000MHz.d2[il][ib].s[0]; //AWS20070606
        data.synchrotron_skymap_41000MHz.d2[ill][ibb].s[0]   += data.synchrotron_41000MHz.d2[il][ib].s[0]; //AWS20070606
        data.synchrotron_skymap_61000MHz.d2[ill][ibb].s[0]   += data.synchrotron_61000MHz.d2[il][ib].s[0]; //AWS20070606
        data.synchrotron_skymap_94000MHz.d2[ill][ibb].s[0]   += data.synchrotron_94000MHz.d2[il][ib].s[0]; //AWS20070606

        n_use                           .d2[ill][ibb].s[0]   +=1; // number of times this pixel sampled
       }//if
     }//if
  }//for


   if(galplotdef.verbose== -810)// selectable debug
     {
      cout<<"data.synchrotron_skymap_22800MHz before dividing by n_use:"<<endl;
      data.synchrotron_skymap_22800MHz.print();
      cout<<"                                                      n_use:"<<endl;
      n_use.print();
     }

  for(  ill=0;  ill <galaxy.n_long;  ill++)
  for(  ibb=0;  ibb <galaxy.n_lat;   ibb++)
  {
    if  ( n_use.d2[ill][ibb].s[0]>0.) data.synchrotron_skymap_22800MHz.d2[ill][ibb].s[0] /= n_use .d2[ill][ibb].s[0];
    if  ( n_use.d2[ill][ibb].s[0]>0.) data.synchrotron_skymap_33000MHz.d2[ill][ibb].s[0] /= n_use .d2[ill][ibb].s[0]; //AWS20070606
    if  ( n_use.d2[ill][ibb].s[0]>0.) data.synchrotron_skymap_41000MHz.d2[ill][ibb].s[0] /= n_use .d2[ill][ibb].s[0]; //AWS20070606
    if  ( n_use.d2[ill][ibb].s[0]>0.) data.synchrotron_skymap_61000MHz.d2[ill][ibb].s[0] /= n_use .d2[ill][ibb].s[0]; //AWS20070606
    if  ( n_use.d2[ill][ibb].s[0]>0.) data.synchrotron_skymap_94000MHz.d2[ill][ibb].s[0] /= n_use .d2[ill][ibb].s[0]; //AWS20070606


    if  ( n_use.d2[ill][ibb].s[0]==0. && galplotdef.verbose== -810  )cout<<"read_synchrotron data: warning: no skymap sample for ill,ibb="<<ill<<", "<<ibb<<endl;
  }


  // convert to K ( units are   mK)
  data.synchrotron_skymap_22800MHz *= 1.e-3;
  data.synchrotron_skymap_33000MHz *= 1.e-3;
  data.synchrotron_skymap_41000MHz *= 1.e-3;
  data.synchrotron_skymap_61000MHz *= 1.e-3;
  data.synchrotron_skymap_94000MHz *= 1.e-3;



  // convert from brightness temp to intensity
  nu=22800.e6; //22800 MHz
  data.synchrotron_skymap_22800MHz  /=   ( I_to_Tb / pow(nu,2) )  ;

  nu=33000.e6; //33000 MHz
  data.synchrotron_skymap_33000MHz  /=   ( I_to_Tb / pow(nu,2) )  ;

  nu=41000.e6; //41000 MHz
  data.synchrotron_skymap_41000MHz  /=   ( I_to_Tb / pow(nu,2) )  ;

  nu=61000.e6; //61000 MHz
  data.synchrotron_skymap_61000MHz  /=   ( I_to_Tb / pow(nu,2) )  ;

  nu=94000.e6; //94000 MHz
  data.synchrotron_skymap_94000MHz  /=   ( I_to_Tb / pow(nu,2) )  ;


 

   if(galplotdef.verbose== -810)// selectable debug
     {
      cout<<"data.synchrotron_skymap_22800MHz after dividing by n_use and converting to intensity:"<<endl;
      data.synchrotron_skymap_22800MHz.print();
     }

   // output 5 WMAPs  as FITS

 for (iWMAP=1; iWMAP<=5;iWMAP++) //AWS20070608
 {
   work.init(galaxy.n_long,galaxy.n_lat);

   work=0.0;   //AWS20070608

   // adjust assuming full sky map 
  work.CRPIX[0]= galaxy.n_long/2;  // centre at longitude near GC
  work.CRPIX[1]= galaxy.n_lat /2;  // centre at latitude  near GC
  work.CDELT[0]=-galaxy.d_long;    // decreasing longitude 
  work.CDELT[1]= galaxy.d_lat;
  work.CRVAL[0]= galaxy.long_min;
  work.CRVAL[1]= galaxy.lat_min +  (work.CRPIX[1] -1 ) * galaxy.d_lat ;



  for(  ill=0;  ill <galaxy.n_long;  ill++)
  for(  ibb=0;  ibb <galaxy.n_lat;   ibb++)
  {
    il=work.CRPIX[0] -ill;
    if(il <  0            ) il+=galaxy.n_long;
    if(il >= galaxy.n_long) il-=galaxy.n_long;

    if(iWMAP==1)work(il ,ibb)=data.synchrotron_skymap_22800MHz.d2[ill][ibb].s[0] ; //AWS20070608
    if(iWMAP==2)work(il ,ibb)=data.synchrotron_skymap_33000MHz.d2[ill][ibb].s[0] ; //AWS20070608
    if(iWMAP==3)work(il ,ibb)=data.synchrotron_skymap_41000MHz.d2[ill][ibb].s[0] ; //AWS20070608
    if(iWMAP==4)work(il ,ibb)=data.synchrotron_skymap_61000MHz.d2[ill][ibb].s[0] ; //AWS20070608
    if(iWMAP==5)work(il ,ibb)=data.synchrotron_skymap_94000MHz.d2[ill][ibb].s[0] ; //AWS20070608
  }
  


  //work.print();
  if(iWMAP==1)strcpy(outfile,"plots/synchrotron_skymap_Miville_survey_22800MHz.fits");      //AWS20070608
  if(iWMAP==2)strcpy(outfile,"plots/synchrotron_skymap_Miville_survey_33000MHz.fits");      //AWS20070608
  if(iWMAP==3)strcpy(outfile,"plots/synchrotron_skymap_Miville_survey_41000MHz.fits");      //AWS20070608
  if(iWMAP==4)strcpy(outfile,"plots/synchrotron_skymap_Miville_survey_61000MHz.fits");      //AWS20070608
  if(iWMAP==5)strcpy(outfile,"plots/synchrotron_skymap_Miville_survey_94000MHz.fits");      //AWS20070608


 if(WMAP_data==0)
 {
  if(iWMAP==1)strcpy(outfile,"plots/synchrotron_skymap_WMAP_3yr_template_survey_22800MHz.fits");      //AWS20120105
  if(iWMAP==2)strcpy(outfile,"plots/synchrotron_skymap_WMAP_3yr_template_survey_33000MHz.fits");      
  if(iWMAP==3)strcpy(outfile,"plots/synchrotron_skymap_WMAP_3yr_template_survey_41000MHz.fits");   
  if(iWMAP==4)strcpy(outfile,"plots/synchrotron_skymap_WMAP_3yr_template_survey_61000MHz.fits");      
  if(iWMAP==5)strcpy(outfile,"plots/synchrotron_skymap_WMAP_3yr_template_survey_94000MHz.fits");     
 }

  work.write(outfile);
  if(iWMAP==1)work.write_keyword_double(outfile,1,"FREQ",22800.,"frequency (MHz)"); //AWS20070608
  if(iWMAP==2)work.write_keyword_double(outfile,1,"FREQ",33000.,"frequency (MHz)"); //AWS20070608
  if(iWMAP==3)work.write_keyword_double(outfile,1,"FREQ",41000.,"frequency (MHz)"); //AWS20070608
  if(iWMAP==4)work.write_keyword_double(outfile,1,"FREQ",61000.,"frequency (MHz)"); //AWS20070608
  if(iWMAP==5)work.write_keyword_double(outfile,1,"FREQ",94000.,"frequency (MHz)"); //AWS20070608
  work.write_keyword_double(outfile,1,"UNITS",1.0,"erg cm-2 sr-1 s-1 Hz-1"); // should use proper keyword
  work.write_keyword_double(outfile,1,"PROGRAM",0,"survey data written by galplot");



} //for iWMAP





 // WMAP 7-year polarization data
 // keep original units mK 
 // use smoothed maps instead of raw maps 
  
    string configure_fits_directory = configure.fits_directory;

    string mapfile,inputfile;


  for (iWMAP=1;iWMAP<=5;iWMAP++)
  {
    if(iWMAP==1)mapfile  = "wmap_band_smth_iqumap_r9_7yr_K_v4.fits";// gz not working. smth instead of raw, AWS20110426
    if(iWMAP==2)mapfile  = "wmap_band_smth_iqumap_r9_7yr_Ka_v4.fits";
    if(iWMAP==3)mapfile  = "wmap_band_smth_iqumap_r9_7yr_Q_v4.fits";
    if(iWMAP==4)mapfile  = "wmap_band_smth_iqumap_r9_7yr_V_v4.fits";
    if(iWMAP==5)mapfile  = "wmap_band_smth_iqumap_r9_7yr_W_v4.fits";

    inputfile  = configure_fits_directory + mapfile;


    
    int colnum;// 1=I 2=Q 3=U 4=Number of points 

    for(colnum=1;colnum<=3;colnum++)
    {
     cout<<"reading WMAP from "<<inputfile<<" column "<<colnum<<endl;
    
     if(iWMAP==1 && colnum==1) read_Healpix_map_from_fits(inputfile,data.synchrotron_WMAP_I_22800MHz,colnum,2);// HDU 2 (1=primary)
     if(iWMAP==1 && colnum==2) read_Healpix_map_from_fits(inputfile,data.synchrotron_WMAP_Q_22800MHz,colnum,2);
     if(iWMAP==1 && colnum==3) read_Healpix_map_from_fits(inputfile,data.synchrotron_WMAP_U_22800MHz,colnum,2); 
    
     if(iWMAP==2 && colnum==1) read_Healpix_map_from_fits(inputfile,data.synchrotron_WMAP_I_33000MHz,colnum,2);
     if(iWMAP==2 && colnum==2) read_Healpix_map_from_fits(inputfile,data.synchrotron_WMAP_Q_33000MHz,colnum,2);
     if(iWMAP==2 && colnum==3) read_Healpix_map_from_fits(inputfile,data.synchrotron_WMAP_U_33000MHz,colnum,2); 

     if(iWMAP==3 && colnum==1) read_Healpix_map_from_fits(inputfile,data.synchrotron_WMAP_I_41000MHz,colnum,2);
     if(iWMAP==3 && colnum==2) read_Healpix_map_from_fits(inputfile,data.synchrotron_WMAP_Q_41000MHz,colnum,2);
     if(iWMAP==3 && colnum==3) read_Healpix_map_from_fits(inputfile,data.synchrotron_WMAP_U_41000MHz,colnum,2); 

     if(iWMAP==4 && colnum==1) read_Healpix_map_from_fits(inputfile,data.synchrotron_WMAP_I_61000MHz,colnum,2);
     if(iWMAP==4 && colnum==2) read_Healpix_map_from_fits(inputfile,data.synchrotron_WMAP_Q_61000MHz,colnum,2);
     if(iWMAP==4 && colnum==3) read_Healpix_map_from_fits(inputfile,data.synchrotron_WMAP_U_61000MHz,colnum,2); 

     if(iWMAP==5 && colnum==1) read_Healpix_map_from_fits(inputfile,data.synchrotron_WMAP_I_94000MHz,colnum,2);
     if(iWMAP==5 && colnum==2) read_Healpix_map_from_fits(inputfile,data.synchrotron_WMAP_Q_94000MHz,colnum,2);
     if(iWMAP==5 && colnum==3) read_Healpix_map_from_fits(inputfile,data.synchrotron_WMAP_U_94000MHz,colnum,2); 

     } // colnum

    }//iWMAP



  data.synchrotron_WMAP_P_22800MHz=data.synchrotron_WMAP_Q_22800MHz; // just to create object with same attributes
  data.synchrotron_WMAP_P_33000MHz=data.synchrotron_WMAP_Q_22800MHz; 
  data.synchrotron_WMAP_P_41000MHz=data.synchrotron_WMAP_Q_22800MHz;
  data.synchrotron_WMAP_P_61000MHz=data.synchrotron_WMAP_Q_22800MHz; 
  data.synchrotron_WMAP_P_94000MHz=data.synchrotron_WMAP_Q_22800MHz; 
  int pix;

  // polarized intensity = sqrt(Q^2 + U^2)
  for (pix=0;pix<data.synchrotron_WMAP_I_22800MHz.Npix(); pix++)
  {
   data.synchrotron_WMAP_P_22800MHz[pix]=sqrt( pow(data.synchrotron_WMAP_Q_22800MHz[pix],2) +pow(data.synchrotron_WMAP_U_22800MHz[pix],2) )  ;
   data.synchrotron_WMAP_P_33000MHz[pix]=sqrt( pow(data.synchrotron_WMAP_Q_33000MHz[pix],2) +pow(data.synchrotron_WMAP_U_33000MHz[pix],2) )  ;
   data.synchrotron_WMAP_P_41000MHz[pix]=sqrt( pow(data.synchrotron_WMAP_Q_41000MHz[pix],2) +pow(data.synchrotron_WMAP_U_41000MHz[pix],2) )  ;
   data.synchrotron_WMAP_P_61000MHz[pix]=sqrt( pow(data.synchrotron_WMAP_Q_61000MHz[pix],2) +pow(data.synchrotron_WMAP_U_61000MHz[pix],2) )  ;
   data.synchrotron_WMAP_P_94000MHz[pix]=sqrt( pow(data.synchrotron_WMAP_Q_94000MHz[pix],2) +pow(data.synchrotron_WMAP_U_94000MHz[pix],2) )  ;
  }

  // test reading

  pointing pointing_;

    for (iWMAP=1;iWMAP<=5;iWMAP++)
  {


   if(iWMAP==1)cout<<" I Npix  = "<<data.synchrotron_WMAP_I_22800MHz.Npix(); 
   if(iWMAP==1)cout<<" I Nside = "<<data.synchrotron_WMAP_I_22800MHz.Nside();
   if(iWMAP==1)cout<<" I Order = "<<data.synchrotron_WMAP_I_22800MHz.Order()<<endl;

   if(iWMAP==1)cout<<" Q Npix  = "<<data.synchrotron_WMAP_Q_22800MHz.Npix(); 
   if(iWMAP==1)cout<<" Q Nside = "<<data.synchrotron_WMAP_Q_22800MHz.Nside();
   if(iWMAP==1)cout<<" Q Order = "<<data.synchrotron_WMAP_Q_22800MHz.Order()<<endl;

   if(iWMAP==1)cout<<" U Npix  = "<<data.synchrotron_WMAP_U_22800MHz.Npix(); 
   if(iWMAP==1)cout<<" U Nside = "<<data.synchrotron_WMAP_U_22800MHz.Nside();
   if(iWMAP==1)cout<<" U Order = "<<data.synchrotron_WMAP_U_22800MHz.Order()<<endl;


   pix=1;
   pix=data.synchrotron_WMAP_I_22800MHz.Npix()/2;

  if(iWMAP==1) 
  {

   
   pointing_=data.synchrotron_WMAP_I_22800MHz.pix2ang(pix);   
   cout<<"I: pixel : pointing theta,phi = "<<pix<<" : "<<pointing_.theta<<" ,  "<<pointing_.phi;
   cout<<" ang2pix: pixel="<<data.synchrotron_WMAP_I_22800MHz.ang2pix(pointing_);
   cout<<endl;

   pointing_=data.synchrotron_WMAP_Q_22800MHz.pix2ang(pix);   
   cout<<"Q: pixel : pointing theta,phi = "<<pix<<" : "<<pointing_.theta<<" ,  "<<pointing_.phi;
   cout<<" ang2pix: pixel="<<data.synchrotron_WMAP_Q_22800MHz.ang2pix(pointing_);
   cout<<endl;

   pointing_=data.synchrotron_WMAP_U_22800MHz.pix2ang(pix);   
   cout<<"U: pixel : pointing theta,phi = "<<pix<<" : "<<pointing_.theta<<" ,  "<<pointing_.phi;
   cout<<" ang2pix: pixel="<<data.synchrotron_WMAP_U_22800MHz.ang2pix(pointing_);
   cout<<endl;
   
   }


   if(iWMAP==1)cout<<"K  band 22800 MHz: ";
   if(iWMAP==2)cout<<"Ka band 33000 MHz: ";
   if(iWMAP==3)cout<<"Q  band 41000 MHz: ";
   if(iWMAP==4)cout<<"V  band 61000 MHz: ";
   if(iWMAP==5)cout<<"W  band 94000 MHz: ";



  
  
  if(iWMAP==1)   pointing_=data.synchrotron_WMAP_I_22800MHz.pix2ang(pix);   
  
  //  cout<<"I: pixel : pointing theta,phi = "<<pix<<" : "<<pointing.theta<<" ,  "<<pointing.phi;
  
  //  if(iWMAP==1) cout<<" ang2pix: pixel="<<data.synchrotron_WMAP_I_22800MHz.ang2pix(pointing)<<endl;




  if(iWMAP==1) cout<<"Q ="         <<data.synchrotron_WMAP_Q_22800MHz[pix]<<"  "; 
  if(iWMAP==2) cout<<"Q ="         <<data.synchrotron_WMAP_Q_33000MHz[pix]<<"  ";  
  if(iWMAP==3) cout<<"Q ="         <<data.synchrotron_WMAP_Q_41000MHz[pix]<<"  ";
  if(iWMAP==4) cout<<"Q ="         <<data.synchrotron_WMAP_Q_61000MHz[pix]<<"  ";
  if(iWMAP==5) cout<<"Q ="         <<data.synchrotron_WMAP_Q_94000MHz[pix]<<"  ";

  if(iWMAP==1) cout<<"U ="         <<data.synchrotron_WMAP_U_22800MHz[pix]<<"  "; 
  if(iWMAP==2) cout<<"U ="         <<data.synchrotron_WMAP_U_33000MHz[pix]<<"  ";  
  if(iWMAP==3) cout<<"U ="         <<data.synchrotron_WMAP_U_41000MHz[pix]<<"  ";
  if(iWMAP==4) cout<<"U ="         <<data.synchrotron_WMAP_U_61000MHz[pix]<<"  ";
  if(iWMAP==5) cout<<"U ="         <<data.synchrotron_WMAP_U_94000MHz[pix]<<"  ";

  if(iWMAP==1) cout<<"P ="         <<data.synchrotron_WMAP_P_22800MHz[pix]<<endl; 
  if(iWMAP==2) cout<<"P ="         <<data.synchrotron_WMAP_P_33000MHz[pix]<<endl;  
  if(iWMAP==3) cout<<"P ="         <<data.synchrotron_WMAP_P_41000MHz[pix]<<endl;
  if(iWMAP==4) cout<<"P ="         <<data.synchrotron_WMAP_P_61000MHz[pix]<<endl;
  if(iWMAP==5) cout<<"P ="         <<data.synchrotron_WMAP_P_94000MHz[pix]<<endl;

  if(iWMAP==1) cout<<"I ="         <<data.synchrotron_WMAP_I_22800MHz[pix]<<"  "; 
  if(iWMAP==2) cout<<"I ="         <<data.synchrotron_WMAP_I_33000MHz[pix]<<"  ";  
  if(iWMAP==3) cout<<"I ="         <<data.synchrotron_WMAP_I_41000MHz[pix]<<"  ";
  if(iWMAP==4) cout<<"I ="         <<data.synchrotron_WMAP_I_61000MHz[pix]<<"  ";
  if(iWMAP==5) cout<<"I ="         <<data.synchrotron_WMAP_I_94000MHz[pix]<<"  ";



  } // iWMAP



   //////////////////////////////////////////////////////////
   // put surveys on galprop grid
   //////////////////////////////////////////////////////////

  data.synchrotron_Q_skymap_22800MHz.init(galaxy.n_long,galaxy.n_lat,1);//AWS20110426
  data.synchrotron_Q_skymap_33000MHz.init(galaxy.n_long,galaxy.n_lat,1);//AWS20110426
  data.synchrotron_Q_skymap_41000MHz.init(galaxy.n_long,galaxy.n_lat,1);//AWS20110426
  data.synchrotron_Q_skymap_61000MHz.init(galaxy.n_long,galaxy.n_lat,1);//AWS20110426
  data.synchrotron_Q_skymap_94000MHz.init(galaxy.n_long,galaxy.n_lat,1);//AWS20110426

  data.synchrotron_U_skymap_22800MHz.init(galaxy.n_long,galaxy.n_lat,1);//AWS20110426
  data.synchrotron_U_skymap_33000MHz.init(galaxy.n_long,galaxy.n_lat,1);//AWS20110426
  data.synchrotron_U_skymap_41000MHz.init(galaxy.n_long,galaxy.n_lat,1);//AWS20110426
  data.synchrotron_U_skymap_61000MHz.init(galaxy.n_long,galaxy.n_lat,1);//AWS20110426
  data.synchrotron_U_skymap_94000MHz.init(galaxy.n_long,galaxy.n_lat,1);//AWS20110426

  data.synchrotron_P_skymap_22800MHz.init(galaxy.n_long,galaxy.n_lat,1);//AWS20110426
  data.synchrotron_P_skymap_33000MHz.init(galaxy.n_long,galaxy.n_lat,1);//AWS20110426
  data.synchrotron_P_skymap_41000MHz.init(galaxy.n_long,galaxy.n_lat,1);//AWS20110426
  data.synchrotron_P_skymap_61000MHz.init(galaxy.n_long,galaxy.n_lat,1);//AWS20110426
  data.synchrotron_P_skymap_94000MHz.init(galaxy.n_long,galaxy.n_lat,1);//AWS20110426


  data.synchrotron_I_skymap_22800MHz.init(galaxy.n_long,galaxy.n_lat,1);//AWS20110426
  data.synchrotron_I_skymap_33000MHz.init(galaxy.n_long,galaxy.n_lat,1);//AWS20110426
  data.synchrotron_I_skymap_41000MHz.init(galaxy.n_long,galaxy.n_lat,1);//AWS20110426
  data.synchrotron_I_skymap_61000MHz.init(galaxy.n_long,galaxy.n_lat,1);//AWS20110426
  data.synchrotron_I_skymap_94000MHz.init(galaxy.n_long,galaxy.n_lat,1);//AWS20110426




 
  
  for(  il=0;  il <galaxy.n_long;   il++)
  for(  ib=0;  ib <galaxy.n_lat ;   ib++)
  {
    l=galaxy.long_min + il * galaxy.d_long;
    b=galaxy. lat_min + ib * galaxy.d_lat;
   
     if(l<0.0) l += 360.0;

     double theta = (90.-b) * dtr;
     double   phi =      l  * dtr;
 
     pointing_ = pointing(theta,phi);
     pix=data.synchrotron_WMAP_Q_22800MHz.ang2pix(pointing_);

     double value = data.synchrotron_WMAP_Q_22800MHz[pix];

     /*
     cout<<" converting WMAP to Distribution: l="<<l<<" b="<<b<<" theta="<<theta<<" phi="<<phi<<" ang2pix: pixel="<<pix;
     cout<<" pointing theta,phi = "<<pix<<" : "<<pointing_.theta<<" ,  "<<pointing_.phi<<" value="<<value <<endl;
     */

     data.synchrotron_Q_skymap_22800MHz.d2[il][ib].s[0]= data.synchrotron_WMAP_Q_22800MHz[pix];
     data.synchrotron_Q_skymap_33000MHz.d2[il][ib].s[0]= data.synchrotron_WMAP_Q_33000MHz[pix];
     data.synchrotron_Q_skymap_41000MHz.d2[il][ib].s[0]= data.synchrotron_WMAP_Q_41000MHz[pix];
     data.synchrotron_Q_skymap_61000MHz.d2[il][ib].s[0]= data.synchrotron_WMAP_Q_61000MHz[pix];
     data.synchrotron_Q_skymap_94000MHz.d2[il][ib].s[0]= data.synchrotron_WMAP_Q_94000MHz[pix];

     data.synchrotron_U_skymap_22800MHz.d2[il][ib].s[0]= data.synchrotron_WMAP_U_22800MHz[pix];
     data.synchrotron_U_skymap_33000MHz.d2[il][ib].s[0]= data.synchrotron_WMAP_U_33000MHz[pix];
     data.synchrotron_U_skymap_41000MHz.d2[il][ib].s[0]= data.synchrotron_WMAP_U_41000MHz[pix];
     data.synchrotron_U_skymap_61000MHz.d2[il][ib].s[0]= data.synchrotron_WMAP_U_61000MHz[pix];
     data.synchrotron_U_skymap_94000MHz.d2[il][ib].s[0]= data.synchrotron_WMAP_U_94000MHz[pix];


     data.synchrotron_P_skymap_22800MHz.d2[il][ib].s[0]= data.synchrotron_WMAP_P_22800MHz[pix];
     data.synchrotron_P_skymap_33000MHz.d2[il][ib].s[0]= data.synchrotron_WMAP_P_33000MHz[pix];
     data.synchrotron_P_skymap_41000MHz.d2[il][ib].s[0]= data.synchrotron_WMAP_P_41000MHz[pix];
     data.synchrotron_P_skymap_61000MHz.d2[il][ib].s[0]= data.synchrotron_WMAP_P_61000MHz[pix];
     data.synchrotron_P_skymap_94000MHz.d2[il][ib].s[0]= data.synchrotron_WMAP_P_94000MHz[pix];

     data.synchrotron_I_skymap_22800MHz.d2[il][ib].s[0]= data.synchrotron_WMAP_I_22800MHz[pix];
     data.synchrotron_I_skymap_33000MHz.d2[il][ib].s[0]= data.synchrotron_WMAP_I_33000MHz[pix];
     data.synchrotron_I_skymap_41000MHz.d2[il][ib].s[0]= data.synchrotron_WMAP_I_41000MHz[pix];
     data.synchrotron_I_skymap_61000MHz.d2[il][ib].s[0]= data.synchrotron_WMAP_I_61000MHz[pix];
     data.synchrotron_I_skymap_94000MHz.d2[il][ib].s[0]= data.synchrotron_WMAP_I_94000MHz[pix];


 

     //   pointing_=data.synchrotron_WMAP_Q_22800MHz.pix2ang(pix);   


   

     

  } // for
  
  // convert from mK to erg cm-2 sr-1 Hz-1

  nu=22800.e6; //22800 MHz
  data.synchrotron_Q_skymap_22800MHz  /=   (1.0e3* I_to_Tb / pow(nu,2) )  ;
  data.synchrotron_U_skymap_22800MHz  /=   (1.0e3* I_to_Tb / pow(nu,2) )  ;
  data.synchrotron_P_skymap_22800MHz  /=   (1.0e3* I_to_Tb / pow(nu,2) )  ;
  data.synchrotron_I_skymap_22800MHz  /=   (1.0e3* I_to_Tb / pow(nu,2) )  ;

  nu=33000.e6; //33000 MHz
  data.synchrotron_Q_skymap_33000MHz  /=   (1.0e3* I_to_Tb / pow(nu,2) )  ;
  data.synchrotron_U_skymap_33000MHz  /=   (1.0e3* I_to_Tb / pow(nu,2) )  ;
  data.synchrotron_P_skymap_33000MHz  /=   (1.0e3* I_to_Tb / pow(nu,2) )  ;
  data.synchrotron_I_skymap_33000MHz  /=   (1.0e3* I_to_Tb / pow(nu,2) )  ;

  nu=41000.e6; //41000 MHz
  data.synchrotron_Q_skymap_41000MHz  /=   (1.0e3* I_to_Tb / pow(nu,2) )  ;
  data.synchrotron_U_skymap_41000MHz  /=   (1.0e3* I_to_Tb / pow(nu,2) )  ;
  data.synchrotron_P_skymap_41000MHz  /=   (1.0e3* I_to_Tb / pow(nu,2) )  ;
  data.synchrotron_I_skymap_41000MHz  /=   (1.0e3* I_to_Tb / pow(nu,2) )  ;


  nu=61000.e6; //61000 MHz
  data.synchrotron_Q_skymap_61000MHz  /=   (1.0e3* I_to_Tb / pow(nu,2) )  ;
  data.synchrotron_U_skymap_61000MHz  /=   (1.0e3* I_to_Tb / pow(nu,2) )  ;
  data.synchrotron_P_skymap_61000MHz  /=   (1.0e3* I_to_Tb / pow(nu,2) )  ;
  data.synchrotron_I_skymap_61000MHz  /=   (1.0e3* I_to_Tb / pow(nu,2) )  ;

  nu=94000.e6; //94000 MHz
  data.synchrotron_Q_skymap_94000MHz  /=   (1.0e3* I_to_Tb / pow(nu,2) )  ;
  data.synchrotron_U_skymap_94000MHz  /=   (1.0e3* I_to_Tb / pow(nu,2) )  ;
  data.synchrotron_P_skymap_94000MHz  /=   (1.0e3* I_to_Tb / pow(nu,2) )  ;
  data.synchrotron_I_skymap_94000MHz  /=   (1.0e3* I_to_Tb / pow(nu,2) )  ;


  //data.synchrotron_Q_skymap_22800MHz.print();

  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  // WMAP 7-year free-free MEM templates
  // For data: keep original units mK  for consistency with other  WMAP (healpix) cases. Conversion done when plotting.
  // For derived  model: convert to erg sr-1 sr-1 Hz-1 for consistency with galprop models

  cout<<endl;

  for (iWMAP=1;iWMAP<=5;iWMAP++)
  {
    if(iWMAP==1)mapfile  = "wmap_k_mem_freefree_7yr_v4.fits";        // gz not working. 
    if(iWMAP==2)mapfile  = "wmap_ka_mem_freefree_7yr_v4.fits";
    if(iWMAP==3)mapfile  = "wmap_q_mem_freefree_7yr_v4.fits";
    if(iWMAP==4)mapfile  = "wmap_v_mem_freefree_7yr_v4.fits";
    if(iWMAP==5)mapfile  = "wmap_w_mem_freefree_7yr_v4.fits";

    inputfile  = configure_fits_directory + mapfile;


    
    int colnum;// 1=I 
    colnum=1;

    
    cout<<"reading WMAP free-free MEM template from "<<inputfile<<" column "<<colnum<<endl;
    
     if(iWMAP==1 ) read_Healpix_map_from_fits(inputfile,data.free_free_WMAP_MEM_22800MHz,colnum,2);// HDU 2 (1=primary)
     if(iWMAP==2 ) read_Healpix_map_from_fits(inputfile,data.free_free_WMAP_MEM_33000MHz,colnum,2);
     if(iWMAP==3 ) read_Healpix_map_from_fits(inputfile,data.free_free_WMAP_MEM_41000MHz,colnum,2);
     if(iWMAP==4 ) read_Healpix_map_from_fits(inputfile,data.free_free_WMAP_MEM_61000MHz,colnum,2);
     if(iWMAP==5 ) read_Healpix_map_from_fits(inputfile,data.free_free_WMAP_MEM_94000MHz,colnum,2);


   

    }//iWMAP






  // test reading free-free template

  

    for (iWMAP=1;iWMAP<=5;iWMAP++)
  {

  if(iWMAP==1) 
  {
   cout<<"free_free_WMAP_MEM_22800MHz  Npix  = "<<data.free_free_WMAP_MEM_22800MHz.Npix(); 
   cout<<"                             Nside = "<<data.free_free_WMAP_MEM_22800MHz.Nside();
   cout<<"                             Order = "<<data.free_free_WMAP_MEM_22800MHz.Order()<<endl;

   pix=data.free_free_WMAP_MEM_22800MHz.Npix()/2;


   pointing_=data.free_free_WMAP_MEM_22800MHz.pix2ang(pix);   
   cout<<" pixel : pointing theta,phi = "<<pix<<" : "<<pointing_.theta<<" ,  "<<pointing_.phi;
   cout<<" ang2pix: pixel="<<data.free_free_WMAP_MEM_22800MHz.ang2pix(pointing_);
   cout<<endl;
  }


   if(iWMAP==1)cout<<"K  band 22800 MHz: ";
   if(iWMAP==2)cout<<"Ka band 33000 MHz: ";
   if(iWMAP==3)cout<<"Q  band 41000 MHz: ";
   if(iWMAP==4)cout<<"V  band 61000 MHz: ";
   if(iWMAP==5)cout<<"W  band 94000 MHz: ";


  if(iWMAP==1) cout<<"free_free ="         <<data.free_free_WMAP_MEM_22800MHz[pix]<<"  "; 
  if(iWMAP==2) cout<<"free_free ="         <<data.free_free_WMAP_MEM_33000MHz[pix]<<"  ";  
  if(iWMAP==3) cout<<"free_free ="         <<data.free_free_WMAP_MEM_41000MHz[pix]<<"  ";
  if(iWMAP==4) cout<<"free_free ="         <<data.free_free_WMAP_MEM_61000MHz[pix]<<"  ";
  if(iWMAP==5) cout<<"free_free ="         <<data.free_free_WMAP_MEM_94000MHz[pix]<<"  ";

  double T1,T2,nu1,nu2;
  if(iWMAP==1){ T1=data.free_free_WMAP_MEM_22800MHz[pix];  T2=data.free_free_WMAP_MEM_33000MHz[pix]; nu1=22800.;nu2=33000.;}
  if(iWMAP==2){ T1=data.free_free_WMAP_MEM_33000MHz[pix];  T2=data.free_free_WMAP_MEM_41000MHz[pix]; nu1=33000.;nu2=41000.;}
  if(iWMAP==3){ T1=data.free_free_WMAP_MEM_41000MHz[pix];  T2=data.free_free_WMAP_MEM_61000MHz[pix]; nu1=41000.;nu2=61000.;}
  if(iWMAP==4){ T1=data.free_free_WMAP_MEM_61000MHz[pix];  T2=data.free_free_WMAP_MEM_94000MHz[pix]; nu1=61000.;nu2=94000.;}
 
  if(iWMAP<5)  cout<< "free_free index (" <<nu1<<","<<nu2<<") = "<<log(T1/T2)/log(nu1/nu2);

  cout<<endl;


  } // iWMAP


    // generate free-free skymaps on galprop frequency grid;

     data.free_free_WMAP_MEM_hp_skymap = galaxy.synchrotron_hp_skymap; // initialize with galprop grid
  
   cout<<"free_free_WMAP_MEM_hp_skymap Npix  = "<<data.free_free_WMAP_MEM_hp_skymap.Npix(); 
   cout<<"                             Nside = "<<data.free_free_WMAP_MEM_hp_skymap.Nside();
   cout<<"                             Order = "<<data.free_free_WMAP_MEM_hp_skymap.Order()<<endl;

   pix=data.free_free_WMAP_MEM_hp_skymap.Npix()/2;


   pointing_=data.free_free_WMAP_MEM_hp_skymap.pix2ang(pix);   
   cout<<" pixel : pointing theta,phi = "<<pix<<" : "<<pointing_.theta<<" ,  "<<pointing_.phi;
   cout<<" ang2pix: pixel="<<data.free_free_WMAP_MEM_hp_skymap.ang2pix(pointing_);
   cout<<endl;
  
   int ipix, ip;
   int debug=0;

   for (ipix=0;ipix<data.free_free_WMAP_MEM_hp_skymap.Npix();ipix++)
   {
    pointing_=data.free_free_WMAP_MEM_hp_skymap.pix2ang(ipix);   
   // use pointing to get interpolated value on WMAP grid
    pix=data.free_free_WMAP_MEM_22800MHz.ang2pix(pointing_);

   if(debug)
   {
    cout<<" galprop pixel : pointing theta,phi = "<<ipix<<" : "<<pointing_.theta<<" ,  "<<pointing_.phi;
    cout<<" ang2pix: check pixel on galprop grid="<<data.free_free_WMAP_MEM_hp_skymap.ang2pix(pointing_);
    cout<<" pixel on WMAP grid="<<pix;
    cout<<endl;
   }


  for(ip=0;ip<galaxy.n_nu_synchgrid;ip++)
  {
     
   double T1,T2,nu1,nu2,T,index;
                                  iWMAP=1;
   if(galaxy.nu_synch[ip]> 33.0e9)iWMAP=2;
   if(galaxy.nu_synch[ip]> 41.0e9)iWMAP=3;
   if(galaxy.nu_synch[ip]> 61.0e9)iWMAP=4;
   if(galaxy.nu_synch[ip]> 94.0e9)iWMAP=5;
  


   if(iWMAP==1){ T1=data.free_free_WMAP_MEM_22800MHz[pix];  T2=data.free_free_WMAP_MEM_33000MHz[pix]; nu1=22.8e9;nu2=33.0e9;}// Hz
   if(iWMAP==2){ T1=data.free_free_WMAP_MEM_33000MHz[pix];  T2=data.free_free_WMAP_MEM_41000MHz[pix]; nu1=33.0e9;nu2=41.0e9;}
   if(iWMAP==3){ T1=data.free_free_WMAP_MEM_41000MHz[pix];  T2=data.free_free_WMAP_MEM_61000MHz[pix]; nu1=41.0e9;nu2=61.0e9;}
   if(iWMAP==4){ T1=data.free_free_WMAP_MEM_61000MHz[pix];  T2=data.free_free_WMAP_MEM_94000MHz[pix]; nu1=61.0e9;nu2=94.0e9;}
   if(iWMAP==5){ T1=data.free_free_WMAP_MEM_61000MHz[pix];  T2=data.free_free_WMAP_MEM_94000MHz[pix]; nu1=61.0e9;nu2=94.0e9;}

 
   index=log(T1/T2)/log(nu1/nu2);

   T = T1 * pow(galaxy.nu_synch[ip]/nu1, index);

   

   data.free_free_WMAP_MEM_hp_skymap[ipix][ip] = T  /   (1.0e3* I_to_Tb / pow(galaxy.nu_synch[ip]  ,2) ); //mK -> erg cm-2 sr-1 Hz-1

  
   if(debug)
   cout<< "free_free index (" <<nu1<<","<<nu2<<") = "<<index<<" nu="<< galaxy.nu_synch[ip] <<" T="<<T<<" mK"
         <<" I= "<<data.free_free_WMAP_MEM_hp_skymap[ipix][ip]<<" erg cm-2 sr-1 Hz-1 " <<endl;

   }//ip
  }//pix

  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  // WMAP 7-year      dust MEM templates AWS20111222
  // For data: keep original units mK  for consistency with other  WMAP (healpix) cases. Conversion done when plotting.
  // For derived  model: convert to erg sr-1 sr-1 Hz-1 for consistency with galprop models

  cout<<endl;

  for (iWMAP=1;iWMAP<=5;iWMAP++)
  {
    if(iWMAP==1)mapfile  = "wmap_k_mem_dust_7yr_v4.fits";        // gz not working. 
    if(iWMAP==2)mapfile  = "wmap_ka_mem_dust_7yr_v4.fits";
    if(iWMAP==3)mapfile  = "wmap_q_mem_dust_7yr_v4.fits";
    if(iWMAP==4)mapfile  = "wmap_v_mem_dust_7yr_v4.fits";
    if(iWMAP==5)mapfile  = "wmap_w_mem_dust_7yr_v4.fits";

    inputfile  = configure_fits_directory + mapfile;


    
    int colnum;// 1=I 
    colnum=1;

    
    cout<<"reading WMAP      dust MEM template from "<<inputfile<<" column "<<colnum<<endl;
    
     if(iWMAP==1 ) read_Healpix_map_from_fits(inputfile,data.dust_WMAP_MEM_22800MHz,colnum,2);// HDU 2 (1=primary)
     if(iWMAP==2 ) read_Healpix_map_from_fits(inputfile,data.dust_WMAP_MEM_33000MHz,colnum,2);
     if(iWMAP==3 ) read_Healpix_map_from_fits(inputfile,data.dust_WMAP_MEM_41000MHz,colnum,2);
     if(iWMAP==4 ) read_Healpix_map_from_fits(inputfile,data.dust_WMAP_MEM_61000MHz,colnum,2);
     if(iWMAP==5 ) read_Healpix_map_from_fits(inputfile,data.dust_WMAP_MEM_94000MHz,colnum,2);


   

    }//iWMAP






  // test reading      dust template

  

    for (iWMAP=1;iWMAP<=5;iWMAP++)
  {

  if(iWMAP==1) 
  {
   cout<<"     dust_WMAP_MEM_22800MHz  Npix  = "<<data.     dust_WMAP_MEM_22800MHz.Npix(); 
   cout<<"                             Nside = "<<data.     dust_WMAP_MEM_22800MHz.Nside();
   cout<<"                             Order = "<<data.     dust_WMAP_MEM_22800MHz.Order()<<endl;

   pix=data.     dust_WMAP_MEM_22800MHz.Npix()/2;


   pointing_=data.     dust_WMAP_MEM_22800MHz.pix2ang(pix);   
   cout<<" pixel : pointing theta,phi = "<<pix<<" : "<<pointing_.theta<<" ,  "<<pointing_.phi;
   cout<<" ang2pix: pixel="<<data.     dust_WMAP_MEM_22800MHz.ang2pix(pointing_);
   cout<<endl;
  }


   if(iWMAP==1)cout<<"K  band 22800 MHz: ";
   if(iWMAP==2)cout<<"Ka band 33000 MHz: ";
   if(iWMAP==3)cout<<"Q  band 41000 MHz: ";
   if(iWMAP==4)cout<<"V  band 61000 MHz: ";
   if(iWMAP==5)cout<<"W  band 94000 MHz: ";


  if(iWMAP==1) cout<<"dust      ="         <<data.     dust_WMAP_MEM_22800MHz[pix]<<"  "; 
  if(iWMAP==2) cout<<"dust      ="         <<data.     dust_WMAP_MEM_33000MHz[pix]<<"  ";  
  if(iWMAP==3) cout<<"dust      ="         <<data.     dust_WMAP_MEM_41000MHz[pix]<<"  ";
  if(iWMAP==4) cout<<"dust      ="         <<data.     dust_WMAP_MEM_61000MHz[pix]<<"  ";
  if(iWMAP==5) cout<<"dust      ="         <<data.     dust_WMAP_MEM_94000MHz[pix]<<"  ";

  double T1,T2,nu1,nu2;
  if(iWMAP==1){ T1=data.     dust_WMAP_MEM_22800MHz[pix];  T2=data.     dust_WMAP_MEM_33000MHz[pix]; nu1=22800.;nu2=33000.;}
  if(iWMAP==2){ T1=data.     dust_WMAP_MEM_33000MHz[pix];  T2=data.     dust_WMAP_MEM_41000MHz[pix]; nu1=33000.;nu2=41000.;}
  if(iWMAP==3){ T1=data.     dust_WMAP_MEM_41000MHz[pix];  T2=data.     dust_WMAP_MEM_61000MHz[pix]; nu1=41000.;nu2=61000.;}
  if(iWMAP==4){ T1=data.     dust_WMAP_MEM_61000MHz[pix];  T2=data.     dust_WMAP_MEM_94000MHz[pix]; nu1=61000.;nu2=94000.;}
 
  if(iWMAP<5)  cout<< "     dust index (" <<nu1<<","<<nu2<<") = "<<log(T1/T2)/log(nu1/nu2);

  cout<<endl;


  } // iWMAP


    // generate      dust skymaps on galprop frequency grid;

     data.     dust_WMAP_MEM_hp_skymap = galaxy.synchrotron_hp_skymap; // initialize with galprop grid
  
   cout<<"     dust_WMAP_MEM_hp_skymap Npix  = "<<data.     dust_WMAP_MEM_hp_skymap.Npix(); 
   cout<<"                             Nside = "<<data.     dust_WMAP_MEM_hp_skymap.Nside();
   cout<<"                             Order = "<<data.     dust_WMAP_MEM_hp_skymap.Order()<<endl;

   pix=data.     dust_WMAP_MEM_hp_skymap.Npix()/2;


   pointing_=data.     dust_WMAP_MEM_hp_skymap.pix2ang(pix);   
   cout<<" pixel : pointing theta,phi = "<<pix<<" : "<<pointing_.theta<<" ,  "<<pointing_.phi;
   cout<<" ang2pix: pixel="<<data.     dust_WMAP_MEM_hp_skymap.ang2pix(pointing_);
   cout<<endl;
  
  

   for (ipix=0;ipix<data.     dust_WMAP_MEM_hp_skymap.Npix();ipix++)
   {
    pointing_=data.     dust_WMAP_MEM_hp_skymap.pix2ang(ipix);   
   // use pointing to get interpolated value on WMAP grid
    pix=data.     dust_WMAP_MEM_22800MHz.ang2pix(pointing_);

   if(debug)
   {
    cout<<" galprop pixel : pointing theta,phi = "<<ipix<<" : "<<pointing_.theta<<" ,  "<<pointing_.phi;
    cout<<" ang2pix: check pixel on galprop grid="<<data.     dust_WMAP_MEM_hp_skymap.ang2pix(pointing_);
    cout<<" pixel on WMAP grid="<<pix;
    cout<<endl;
   }


  for(ip=0;ip<galaxy.n_nu_synchgrid;ip++)
  {
     
   double T1,T2,nu1,nu2,T,index;
                                  iWMAP=1;
   if(galaxy.nu_synch[ip]> 33.0e9)iWMAP=2;
   if(galaxy.nu_synch[ip]> 41.0e9)iWMAP=3;
   if(galaxy.nu_synch[ip]> 61.0e9)iWMAP=4;
   if(galaxy.nu_synch[ip]> 94.0e9)iWMAP=5;
  


   if(iWMAP==1){ T1=data.     dust_WMAP_MEM_22800MHz[pix];  T2=data.     dust_WMAP_MEM_33000MHz[pix]; nu1=22.8e9;nu2=33.0e9;}// Hz
   if(iWMAP==2){ T1=data.     dust_WMAP_MEM_33000MHz[pix];  T2=data.     dust_WMAP_MEM_41000MHz[pix]; nu1=33.0e9;nu2=41.0e9;}
   if(iWMAP==3){ T1=data.     dust_WMAP_MEM_41000MHz[pix];  T2=data.     dust_WMAP_MEM_61000MHz[pix]; nu1=41.0e9;nu2=61.0e9;}
   if(iWMAP==4){ T1=data.     dust_WMAP_MEM_61000MHz[pix];  T2=data.     dust_WMAP_MEM_94000MHz[pix]; nu1=61.0e9;nu2=94.0e9;}
   if(iWMAP==5){ T1=data.     dust_WMAP_MEM_61000MHz[pix];  T2=data.     dust_WMAP_MEM_94000MHz[pix]; nu1=61.0e9;nu2=94.0e9;}

 
   index=log(T1/T2)/log(nu1/nu2);

   T = T1 * pow(galaxy.nu_synch[ip]/nu1, index);

   

   data.     dust_WMAP_MEM_hp_skymap[ipix][ip] = T  /   (1.0e3* I_to_Tb / pow(galaxy.nu_synch[ip]  ,2) ); //mK -> erg cm-2 sr-1 Hz-1

  
   if(debug)
   cout<< "     dust index (" <<nu1<<","<<nu2<<") = "<<index<<" nu="<< galaxy.nu_synch[ip] <<" T="<<T<<" mK"
         <<" I= "<<data.     dust_WMAP_MEM_hp_skymap[ipix][ip]<<" erg cm-2 sr-1 Hz-1 " <<endl;

   }//ip
  }//pix


 





 //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  // WMAP 7-year synchrotron MEM templates AWS20111222
  // For data: keep original units mK  for consistency with other  WMAP (healpix) cases. Conversion done when plotting.
  // For derived  model: convert to erg sr-1 sr-1 Hz-1 for consistency with galprop models

  cout<<endl;

  for (iWMAP=1;iWMAP<=5;iWMAP++)
  {
    if(iWMAP==1)mapfile  = "wmap_k_mem_synch_7yr_v4.fits";        // gz not working. 
    if(iWMAP==2)mapfile  = "wmap_ka_mem_synch_7yr_v4.fits";
    if(iWMAP==3)mapfile  = "wmap_q_mem_synch_7yr_v4.fits";
    if(iWMAP==4)mapfile  = "wmap_v_mem_synch_7yr_v4.fits";
    if(iWMAP==5)mapfile  = "wmap_w_mem_synch_7yr_v4.fits";

    inputfile  = configure_fits_directory + mapfile;


    
    int colnum;// 1=I 
    colnum=1;

    
    cout<<"reading WMAP synchrotron MEM template from "<<inputfile<<" column "<<colnum<<endl;
    
     if(iWMAP==1 ) read_Healpix_map_from_fits(inputfile,data.synchrotron_WMAP_MEM_22800MHz,colnum,2);// HDU 2 (1=primary)
     if(iWMAP==2 ) read_Healpix_map_from_fits(inputfile,data.synchrotron_WMAP_MEM_33000MHz,colnum,2);
     if(iWMAP==3 ) read_Healpix_map_from_fits(inputfile,data.synchrotron_WMAP_MEM_41000MHz,colnum,2);
     if(iWMAP==4 ) read_Healpix_map_from_fits(inputfile,data.synchrotron_WMAP_MEM_61000MHz,colnum,2);
     if(iWMAP==5 ) read_Healpix_map_from_fits(inputfile,data.synchrotron_WMAP_MEM_94000MHz,colnum,2);


   

    }//iWMAP






  // test reading synchrotron template

  

    for (iWMAP=1;iWMAP<=5;iWMAP++)
  {

  if(iWMAP==1) 
  {
   cout<<"synchrotron_WMAP_MEM_22800MHz  Npix  = "<<data.synchrotron_WMAP_MEM_22800MHz.Npix(); 
   cout<<"                               Nside = "<<data.synchrotron_WMAP_MEM_22800MHz.Nside();
   cout<<"                               Order = "<<data.synchrotron_WMAP_MEM_22800MHz.Order()<<endl;

   pix=data.synchrotron_WMAP_MEM_22800MHz.Npix()/2;


   pointing_=data.synchrotron_WMAP_MEM_22800MHz.pix2ang(pix);   
   cout<<" pixel : pointing theta,phi = "<<pix<<" : "<<pointing_.theta<<" ,  "<<pointing_.phi;
   cout<<" ang2pix: pixel="<<data.synchrotron_WMAP_MEM_22800MHz.ang2pix(pointing_);
   cout<<endl;
  }


   if(iWMAP==1)cout<<"K  band 22800 MHz: ";
   if(iWMAP==2)cout<<"Ka band 33000 MHz: ";
   if(iWMAP==3)cout<<"Q  band 41000 MHz: ";
   if(iWMAP==4)cout<<"V  band 61000 MHz: ";
   if(iWMAP==5)cout<<"W  band 94000 MHz: ";


  if(iWMAP==1) cout<<"synchrotron ="         <<data.synchrotron_WMAP_MEM_22800MHz[pix]<<"  "; 
  if(iWMAP==2) cout<<"synchrotron ="         <<data.synchrotron_WMAP_MEM_33000MHz[pix]<<"  ";  
  if(iWMAP==3) cout<<"synchrotron ="         <<data.synchrotron_WMAP_MEM_41000MHz[pix]<<"  ";
  if(iWMAP==4) cout<<"synchrotron ="         <<data.synchrotron_WMAP_MEM_61000MHz[pix]<<"  ";
  if(iWMAP==5) cout<<"synchrotron ="         <<data.synchrotron_WMAP_MEM_94000MHz[pix]<<"  ";

  double T1,T2,nu1,nu2;
  if(iWMAP==1){ T1=data.synchrotron_WMAP_MEM_22800MHz[pix];  T2=data.synchrotron_WMAP_MEM_33000MHz[pix]; nu1=22800.;nu2=33000.;}
  if(iWMAP==2){ T1=data.synchrotron_WMAP_MEM_33000MHz[pix];  T2=data.synchrotron_WMAP_MEM_41000MHz[pix]; nu1=33000.;nu2=41000.;}
  if(iWMAP==3){ T1=data.synchrotron_WMAP_MEM_41000MHz[pix];  T2=data.synchrotron_WMAP_MEM_61000MHz[pix]; nu1=41000.;nu2=61000.;}
  if(iWMAP==4){ T1=data.synchrotron_WMAP_MEM_61000MHz[pix];  T2=data.synchrotron_WMAP_MEM_94000MHz[pix]; nu1=61000.;nu2=94000.;}
 
  if(iWMAP<5)  cout<< "synchrotron index (" <<nu1<<","<<nu2<<") = "<<log(T1/T2)/log(nu1/nu2);

  cout<<endl;


  } // iWMAP


    // generate synchrotron  skymaps on galprop frequency grid;

     data.synchrotron_WMAP_MEM_hp_skymap = galaxy.synchrotron_hp_skymap; // initialize with galprop grid
  
   cout<<"synchrotron_WMAP_MEM_hp_skymap Npix  = "<<data.synchrotron_WMAP_MEM_hp_skymap.Npix(); 
   cout<<"                               Nside = "<<data.synchrotron_WMAP_MEM_hp_skymap.Nside();
   cout<<"                               Order = "<<data.synchrotron_WMAP_MEM_hp_skymap.Order()<<endl;

   pix=data.synchrotron_WMAP_MEM_hp_skymap.Npix()/2;


   pointing_=data.synchrotron_WMAP_MEM_hp_skymap.pix2ang(pix);   
   cout<<" pixel : pointing theta,phi = "<<pix<<" : "<<pointing_.theta<<" ,  "<<pointing_.phi;
   cout<<" ang2pix: pixel="<<data.synchrotron_WMAP_MEM_hp_skymap.ang2pix(pointing_);
   cout<<endl;
  
   

   for (ipix=0;ipix<data.synchrotron_WMAP_MEM_hp_skymap.Npix();ipix++)
   {
    pointing_=data.synchrotron_WMAP_MEM_hp_skymap.pix2ang(ipix);   
   // use pointing to get interpolated value on WMAP grid
    pix=data.synchrotron_WMAP_MEM_22800MHz.ang2pix(pointing_);

   if(debug)
   {
    cout<<" galprop pixel : pointing theta,phi = "<<ipix<<" : "<<pointing_.theta<<" ,  "<<pointing_.phi;
    cout<<" ang2pix: check pixel on galprop grid="<<data.synchrotron_WMAP_MEM_hp_skymap.ang2pix(pointing_);
    cout<<" pixel on WMAP grid="<<pix;
    cout<<endl;
   }


  for(ip=0;ip<galaxy.n_nu_synchgrid;ip++)
  {
     
   double T1,T2,nu1,nu2,T,index;
                                  iWMAP=1;
   if(galaxy.nu_synch[ip]> 33.0e9)iWMAP=2;
   if(galaxy.nu_synch[ip]> 41.0e9)iWMAP=3;
   if(galaxy.nu_synch[ip]> 61.0e9)iWMAP=4;
   if(galaxy.nu_synch[ip]> 94.0e9)iWMAP=5;
  


   if(iWMAP==1){ T1=data.synchrotron_WMAP_MEM_22800MHz[pix];  T2=data.synchrotron_WMAP_MEM_33000MHz[pix]; nu1=22.8e9;nu2=33.0e9;}// Hz
   if(iWMAP==2){ T1=data.synchrotron_WMAP_MEM_33000MHz[pix];  T2=data.synchrotron_WMAP_MEM_41000MHz[pix]; nu1=33.0e9;nu2=41.0e9;}
   if(iWMAP==3){ T1=data.synchrotron_WMAP_MEM_41000MHz[pix];  T2=data.synchrotron_WMAP_MEM_61000MHz[pix]; nu1=41.0e9;nu2=61.0e9;}
   if(iWMAP==4){ T1=data.synchrotron_WMAP_MEM_61000MHz[pix];  T2=data.synchrotron_WMAP_MEM_94000MHz[pix]; nu1=61.0e9;nu2=94.0e9;}
   if(iWMAP==5){ T1=data.synchrotron_WMAP_MEM_61000MHz[pix];  T2=data.synchrotron_WMAP_MEM_94000MHz[pix]; nu1=61.0e9;nu2=94.0e9;}

 
   index=log(T1/T2)/log(nu1/nu2);

   T = T1 * pow(galaxy.nu_synch[ip]/nu1, index);

   

   data.synchrotron_WMAP_MEM_hp_skymap[ipix][ip] = T  /   (1.0e3* I_to_Tb / pow(galaxy.nu_synch[ip]  ,2) ); //mK -> erg cm-2 sr-1 Hz-1

  
   if(debug)
   cout<< "synchrotron index (" <<nu1<<","<<nu2<<") = "<<index<<" nu="<< galaxy.nu_synch[ip] <<" T="<<T<<" mK"
         <<" I= "<<data.synchrotron_WMAP_MEM_hp_skymap[ipix][ip]<<" erg cm-2 sr-1 Hz-1 " <<endl;

   }//ip
  }//pix

   cout<<endl;


   ////////////////////////////////////////////////

   int                synchrotron_WMAP_template=0;
   if (WMAP_data==0)  synchrotron_WMAP_template=1; // replaces 3yr WMAP template with 7 year template. Only used for profiles, spectra use healpix already read

   if ( synchrotron_WMAP_template==1)
   {
   cout<<" WMAP 7-yr synchrotron template to l,b grid"<<endl;

   // put on l,b grid like other WMAP 7-yr data for profiles AWS20120104

    // WMAP 7-yr synchrotron template instead of Miville-Deschenes based 

  for(  il=0;  il <galaxy.n_long;   il++)
  for(  ib=0;  ib <galaxy.n_lat ;   ib++)
  {
    l=galaxy.long_min + il * galaxy.d_long;
    b=galaxy. lat_min + ib * galaxy.d_lat;
   
     if(l<0.0) l += 360.0;

     double theta = (90.-b) * dtr;
     double   phi =      l  * dtr;
 
     pointing_ = pointing(theta,phi);
     pix=data.synchrotron_WMAP_MEM_22800MHz.ang2pix(pointing_);

     double value = data.synchrotron_WMAP_MEM_22800MHz[pix];

     /*
     cout<<" converting WMAP 7-yr synchrotron template to Distribution: l="<<l<<" b="<<b<<" theta="<<theta<<" phi="<<phi<<" ang2pix: pixel="<<pix;
     cout<<" pointing theta,phi = "<<pix<<" : "<<pointing_.theta<<" ,  "<<pointing_.phi<<" value="<<value <<endl;
     */

     data.synchrotron_skymap_22800MHz.d2[il][ib].s[0]   = data.synchrotron_WMAP_MEM_22800MHz[pix]; //AWS20120104
     data.synchrotron_skymap_33000MHz.d2[il][ib].s[0]   = data.synchrotron_WMAP_MEM_33000MHz[pix]; //AWS20120104
     data.synchrotron_skymap_41000MHz.d2[il][ib].s[0]   = data.synchrotron_WMAP_MEM_41000MHz[pix]; //AWS20120104
     data.synchrotron_skymap_61000MHz.d2[il][ib].s[0]   = data.synchrotron_WMAP_MEM_61000MHz[pix]; //AWS20120104
     data.synchrotron_skymap_94000MHz.d2[il][ib].s[0]   = data.synchrotron_WMAP_MEM_94000MHz[pix]; //AWS20120104


  } // for

 // convert to K ( units are   mK)
  data.synchrotron_skymap_22800MHz *= 1.e-3;
  data.synchrotron_skymap_33000MHz *= 1.e-3;
  data.synchrotron_skymap_41000MHz *= 1.e-3;
  data.synchrotron_skymap_61000MHz *= 1.e-3;
  data.synchrotron_skymap_94000MHz *= 1.e-3;



  // convert from brightness temp to intensity
  nu=22800.e6; //22800 MHz
  data.synchrotron_skymap_22800MHz  /=   ( I_to_Tb / pow(nu,2) )  ;

  nu=33000.e6; //33000 MHz
  data.synchrotron_skymap_33000MHz  /=   ( I_to_Tb / pow(nu,2) )  ;

  nu=41000.e6; //41000 MHz
  data.synchrotron_skymap_41000MHz  /=   ( I_to_Tb / pow(nu,2) )  ;

  nu=61000.e6; //61000 MHz
  data.synchrotron_skymap_61000MHz  /=   ( I_to_Tb / pow(nu,2) )  ;

  nu=94000.e6; //94000 MHz
  data.synchrotron_skymap_94000MHz  /=   ( I_to_Tb / pow(nu,2) )  ;


   // output 5 WMAPs  as FITS

 for (iWMAP=1; iWMAP<=5;iWMAP++) //AWS20070608
 {
   work.init(galaxy.n_long,galaxy.n_lat);

   work=0.0;   //AWS20070608

   // adjust assuming full sky map 
  work.CRPIX[0]= galaxy.n_long/2;  // centre at longitude near GC
  work.CRPIX[1]= galaxy.n_lat /2;  // centre at latitude  near GC
  work.CDELT[0]=-galaxy.d_long;    // decreasing longitude 
  work.CDELT[1]= galaxy.d_lat;
  work.CRVAL[0]= galaxy.long_min;
  work.CRVAL[1]= galaxy.lat_min +  (work.CRPIX[1] -1 ) * galaxy.d_lat ;



  for(  ill=0;  ill <galaxy.n_long;  ill++)
  for(  ibb=0;  ibb <galaxy.n_lat;   ibb++)
  {
    il=work.CRPIX[0] -ill;
    if(il <  0            ) il+=galaxy.n_long;
    if(il >= galaxy.n_long) il-=galaxy.n_long;

    if(iWMAP==1)work(il ,ibb)=data.synchrotron_skymap_22800MHz.d2[ill][ibb].s[0] ; //AWS20070608
    if(iWMAP==2)work(il ,ibb)=data.synchrotron_skymap_33000MHz.d2[ill][ibb].s[0] ; //AWS20070608
    if(iWMAP==3)work(il ,ibb)=data.synchrotron_skymap_41000MHz.d2[ill][ibb].s[0] ; //AWS20070608
    if(iWMAP==4)work(il ,ibb)=data.synchrotron_skymap_61000MHz.d2[ill][ibb].s[0] ; //AWS20070608
    if(iWMAP==5)work(il ,ibb)=data.synchrotron_skymap_94000MHz.d2[ill][ibb].s[0] ; //AWS20070608
  }


 //work.print();
  if(iWMAP==1)strcpy(outfile,"plots/synchrotron_skymap_WMAP_7yr_template_survey_22800MHz.fits");      //AWS20070608
  if(iWMAP==2)strcpy(outfile,"plots/synchrotron_skymap_WMAP_7yr_template_survey_33000MHz.fits");      //AWS20070608
  if(iWMAP==3)strcpy(outfile,"plots/synchrotron_skymap_WMAP_7yr_template_survey_41000MHz.fits");      //AWS20070608
  if(iWMAP==4)strcpy(outfile,"plots/synchrotron_skymap_WMAP_7yr_template_survey_61000MHz.fits");      //AWS20070608
  if(iWMAP==5)strcpy(outfile,"plots/synchrotron_skymap_WMAP_7yr_template_survey_94000MHz.fits");      //AWS20070608

  work.write(outfile);
  if(iWMAP==1)work.write_keyword_double(outfile,1,"FREQ",22800.,"frequency (MHz)"); //AWS20070608
  if(iWMAP==2)work.write_keyword_double(outfile,1,"FREQ",33000.,"frequency (MHz)"); //AWS20070608
  if(iWMAP==3)work.write_keyword_double(outfile,1,"FREQ",41000.,"frequency (MHz)"); //AWS20070608
  if(iWMAP==4)work.write_keyword_double(outfile,1,"FREQ",61000.,"frequency (MHz)"); //AWS20070608
  if(iWMAP==5)work.write_keyword_double(outfile,1,"FREQ",94000.,"frequency (MHz)"); //AWS20070608
  work.write_keyword_double(outfile,1,"UNITS",1.0,"erg cm-2 sr-1 s-1 Hz-1"); // should use proper keyword
  work.write_keyword_double(outfile,1,"PROGRAM",0,"survey data written by galplot");



} //for iWMAP


 // fill GC-centred grid with original mK values for compatibility (used in plot_synchrotron_skymap.cc for spectra)

  CRVAL1  =                 0.25; 
  CRVAL2  =               -89.75; 
  CRPIX1  =                 360.; 
  CRPIX2  =                   1.; 
  CDELT1  =                 -0.5; 
  CDELT2  =                  0.5; 
 

  n_rgrid = data.synchrotron_22800MHz.n_rgrid;
  n_zgrid = data.synchrotron_22800MHz.n_zgrid;

 

  for(  il=0;  il <n_rgrid;   il++)
  for(  ib=0;  ib <n_zgrid;   ib++)
  {
     l=CRVAL1+CDELT1*(il -CRPIX1+1);
     b=CRVAL2+CDELT2*(ib- CRPIX2+1);

    if(l<0.0) l += 360.0;

     double theta = (90.-b) * dtr;
     double   phi =      l  * dtr;
 
     pointing_ = pointing(theta,phi);
     pix=data.synchrotron_WMAP_MEM_22800MHz.ang2pix(pointing_);

     double value = data.synchrotron_WMAP_MEM_22800MHz[pix];

     /*
     cout<<" converting WMAP 7-yr synchrotron template to GC-centred Distribution: l="<<l<<" b="<<b<<" theta="<<theta<<" phi="<<phi<<" ang2pix: pixel="<<pix;
     cout<<" pointing theta,phi = "<<pix<<" : "<<pointing_.theta<<" ,  "<<pointing_.phi<<" value="<<value <<endl;
     */

     data.synchrotron_22800MHz.d2[il][ib].s[0]   = data.synchrotron_WMAP_MEM_22800MHz[pix];
     data.synchrotron_33000MHz.d2[il][ib].s[0]   = data.synchrotron_WMAP_MEM_33000MHz[pix];
     data.synchrotron_41000MHz.d2[il][ib].s[0]   = data.synchrotron_WMAP_MEM_41000MHz[pix];
     data.synchrotron_61000MHz.d2[il][ib].s[0]   = data.synchrotron_WMAP_MEM_61000MHz[pix];
     data.synchrotron_94000MHz.d2[il][ib].s[0]   = data.synchrotron_WMAP_MEM_94000MHz[pix];

  } //for


   }// if synchrotron_WMAP_template==1


   /////////////////////////////////////////////////////////////

  // WMAP 7-year   MCMC spinning dust model templates AWS20120116
  // For data: keep original units mK  for consistency with other  WMAP (healpix) cases. Conversion done when plotting.
  // For derived  model: convert to erg sr-1 sr-1 Hz-1 for consistency with galprop models

  // these maps are provided for one frequency only, with a fixed spectral index
  // Gold etal. 2011 ApJSupp 192,15.
  // these are provided at one frequency, with power-law fixed at +2.0 for dust
  // and beta_d=+2.0, nu_sd=4.2 GHz in equation(10) for spinning dust.

  // ORDERING = 'NESTED' was missing from the table extension header (present in Primary header), and was added using fv
  // the other HealPix parameters are also absent but correctly deduced by the read routine. (HealPix 2.01).
  // AWS20131129 HealPix 2.20a gave error "keyword NSIDE not found"; added NSIDE=64 to table extension header to solve this.

  cout<<endl;


    mapfile    = "wmap_mcmc_sd_w_dust_temp_7yr_v4.fits";        
    inputfile  = configure_fits_directory + mapfile;


    
    int colnum;// 1=I 
    colnum=1;

    
    cout<<"reading WMAP      dust from MCMC spinning dust model template from "<<inputfile<<" column "<<colnum<<endl;
    read_Healpix_map_from_fits(inputfile,data.dust_WMAP_MCMC_SD_94000MHz,colnum,2);

   cout<<"   dust_WMAP_MCMC_SD_94000MHz    Npix  = "<<data.dust_WMAP_MCMC_SD_94000MHz     .Npix(); 
   cout<<"                                 Nside = "<<data.dust_WMAP_MCMC_SD_94000MHz.Nside();
   cout<<"                                 Order = "<<data.dust_WMAP_MCMC_SD_94000MHz.Order()<<endl;

   pix=      data.dust_WMAP_MCMC_SD_94000MHz.Npix()/2;
   pointing_=data.dust_WMAP_MCMC_SD_94000MHz.pix2ang(pix);   
   cout<<" pixel : pointing theta,phi = "<<pix<<" : "<<pointing_.theta<<" ,  "<<pointing_.phi;
   cout<<" ang2pix: pixel="<<data. dust_WMAP_MCMC_SD_94000MHz .ang2pix(pointing_);
   cout<<"  value="<<data.dust_WMAP_MCMC_SD_94000MHz[pix];
   cout<<endl;


    mapfile    = "wmap_mcmc_sd_w_dust_stk_q_7yr_v4.fits";        
    inputfile  = configure_fits_directory + mapfile;

    cout<<"reading WMAP      dust Q from MCMC spinning dust model template from "<<inputfile<<" column "<<colnum<<endl;
    read_Healpix_map_from_fits(inputfile,data.dust_Q_WMAP_MCMC_SD_94000MHz,colnum,2);

    mapfile    = "wmap_mcmc_sd_w_dust_stk_u_7yr_v4.fits";        
    inputfile  = configure_fits_directory + mapfile;

    cout<<"reading WMAP      dust U from MCMC spinning dust model template from "<<inputfile<<" column "<<colnum<<endl;
    read_Healpix_map_from_fits(inputfile,data.dust_U_WMAP_MCMC_SD_94000MHz,colnum,2);





   mapfile    = "wmap_mcmc_sd_k_spin_dust_temp_7yr_v4.fits";        
   inputfile  = configure_fits_directory + mapfile;

   cout<<"reading WMAP   spinning dust from MCMC spinning dust model template from "<<inputfile<<" column "<<colnum<<endl;
   read_Healpix_map_from_fits(inputfile,data.spin_dust_WMAP_MCMC_SD_22800MHz,colnum,2);// HDU 2 (1=primary)
   
   cout<<"   spin_dust_WMAP_MCMC_SD_22800MHz    Npix  = "<<data.spin_dust_WMAP_MCMC_SD_22800MHz     .Npix(); 
   cout<<"                                      Nside = "<<data.spin_dust_WMAP_MCMC_SD_22800MHz.Nside();
   cout<<"                                      Order = "<<data.spin_dust_WMAP_MCMC_SD_22800MHz.Order()<<endl;

   pix=      data.spin_dust_WMAP_MCMC_SD_22800MHz.Npix()/2;
   pointing_=data.spin_dust_WMAP_MCMC_SD_22800MHz.pix2ang(pix);   
   cout<<" pixel : pointing theta,phi = "<<pix<<" : "<<pointing_.theta<<" ,  "<<pointing_.phi;
   cout<<" ang2pix: pixel="<<data. spin_dust_WMAP_MCMC_SD_22800MHz .ang2pix(pointing_);
   cout<<"  value="<<data.spin_dust_WMAP_MCMC_SD_22800MHz[pix];
   cout<<endl;
  
   // generate dust and spinning dust  skymaps on galprop frequency grid;

    data.     dust_WMAP_MCMC_SD_hp_skymap = galaxy.synchrotron_hp_skymap; // initialize with galprop grid
    data.   dust_Q_WMAP_MCMC_SD_hp_skymap = galaxy.synchrotron_hp_skymap;
    data.   dust_U_WMAP_MCMC_SD_hp_skymap = galaxy.synchrotron_hp_skymap;
    data.   dust_P_WMAP_MCMC_SD_hp_skymap = galaxy.synchrotron_hp_skymap;

    data.spin_dust_WMAP_MCMC_SD_hp_skymap = galaxy.synchrotron_hp_skymap;




   for (ipix=0;ipix<data.synchrotron_WMAP_MEM_hp_skymap.Npix();ipix++)
   {
    pointing_=data.dust_WMAP_MCMC_SD_hp_skymap.pix2ang(ipix);   
   // use pointing to get interpolated value on WMAP grid
    pix=data.dust_WMAP_MCMC_SD_94000MHz.ang2pix(pointing_);

   if(debug)
   {
    cout<<" galprop pixel : pointing theta,phi = "<<ipix<<" : "<<pointing_.theta<<" ,  "<<pointing_.phi;
    cout<<" ang2pix: check pixel on galprop grid="<<data.dust_WMAP_MCMC_SD_hp_skymap.ang2pix(pointing_);
    cout<<" pixel on WMAP grid="<<pix;
    cout<<endl;
   }


  for(ip=0;ip<galaxy.n_nu_synchgrid;ip++)
  {
     
   double T1,nu1,T,index;
                             
  
   double beta_d = +2.0; // dust spectral index. Gold etal. 2011 MCMC SD model

   nu1=94.0e9;// Hz,  for reference template
 
   index=beta_d;
  
   T1=data.dust_WMAP_MCMC_SD_94000MHz[pix];
   T = T1 * pow(galaxy.nu_synch[ip]/nu1, index);

   data.dust_WMAP_MCMC_SD_hp_skymap[ipix][ip] = T  /   (1.0e3* I_to_Tb / pow(galaxy.nu_synch[ip]  ,2) ); //mK -> erg cm-2 sr-1 Hz-1

    debug=0;

   if(debug)
   cout<< "WMAP MCMC_SD model:          dust index  = "<<index<<" nu="<< galaxy.nu_synch[ip] <<" T="<<T<<" mK"
       <<" dust I= "<<data.dust_WMAP_MCMC_SD_hp_skymap[ipix][ip]<<" erg cm-2 sr-1 Hz-1 " <<endl;



   T1=data.dust_Q_WMAP_MCMC_SD_94000MHz[pix];
   T = T1 * pow(galaxy.nu_synch[ip]/nu1, index);

   data.dust_Q_WMAP_MCMC_SD_hp_skymap[ipix][ip] = T  /   (1.0e3* I_to_Tb / pow(galaxy.nu_synch[ip]  ,2) ); //mK -> erg cm-2 sr-1 Hz-1

   if(debug)
   cout<< "WMAP MCMC_SD model:          dust index  = "<<index<<" nu="<< galaxy.nu_synch[ip] <<" T="<<T<<" mK"
       <<" dust Q= "<<data.dust_Q_WMAP_MCMC_SD_hp_skymap[ipix][ip]<<" erg cm-2 sr-1 Hz-1 " <<endl;

   T1=data.dust_U_WMAP_MCMC_SD_94000MHz[pix];
   T = T1 * pow(galaxy.nu_synch[ip]/nu1, index);

   data.dust_U_WMAP_MCMC_SD_hp_skymap[ipix][ip] = T  /   (1.0e3* I_to_Tb / pow(galaxy.nu_synch[ip]  ,2) ); //mK -> erg cm-2 sr-1 Hz-1


   if(debug)
   cout<< "WMAP MCMC_SD model:          dust index  = "<<index<<" nu="<< galaxy.nu_synch[ip] <<" T="<<T<<" mK"
       <<" dust U= "<<data.dust_U_WMAP_MCMC_SD_hp_skymap[ipix][ip]<<" erg cm-2 sr-1 Hz-1 " <<endl;


 
   data.dust_P_WMAP_MCMC_SD_hp_skymap[ipix][ip] =
     sqrt( pow(data.dust_Q_WMAP_MCMC_SD_hp_skymap[ipix][ip], 2)
	  +pow(data.dust_U_WMAP_MCMC_SD_hp_skymap[ipix][ip], 2) );

   if(debug)
   cout<< "WMAP MCMC_SD model:          dust index  = "<<index<<" nu="<< galaxy.nu_synch[ip] 
       <<" dust P= "<<data.dust_P_WMAP_MCMC_SD_hp_skymap[ipix][ip]<<" erg cm-2 sr-1 Hz-1 " <<endl;






   double nu_sd =  4.2e9;// Gold et al. 2011 
          nu1   = 22.8e9;// Hz, for reference template
   

   T1=data.spin_dust_WMAP_MCMC_SD_22800MHz[pix];
   index=beta_d + 1;

   T = T1 * pow(galaxy.nu_synch[ip]/nu_sd, index )/(exp(galaxy.nu_synch[ip]/nu_sd ) -1.0)  // Gold et al. 2011 equation (10)
          /(pow(       nu1         /nu_sd, index )/(exp(       nu1         /nu_sd ) -1.0));

   data.spin_dust_WMAP_MCMC_SD_hp_skymap[ipix][ip] = T  /   (1.0e3* I_to_Tb / pow(galaxy.nu_synch[ip]  ,2) ); //mK -> erg cm-2 sr-1 Hz-1

   if(debug)
   cout<< "WMAP MCMC_SD model: spinning dust index = "<<index<<" nu="<< galaxy.nu_synch[ip] <<" T="<<T<<" mK"
         <<" spinning dust I= "<<data.spin_dust_WMAP_MCMC_SD_hp_skymap[ipix][ip]<<" erg cm-2 sr-1 Hz-1 " <<endl;

   }//ip
  }//pix




   // HealPix versions of non-WMAP data from lambda website   AWS20130121
    mapfile    = "lambda_haslam408_dsds.fits";        
    inputfile  = configure_fits_directory + mapfile;

    colnum=1;
    cout<<"reading 408 MHz HealPix map from "<<inputfile<<" column "<<colnum<<endl;
    read_Healpix_map_from_fits(inputfile, data. synchrotron_Healpix__408MHz   ,colnum,2);
   


   //////////////////////////////////////////////////////////////////////////

    cout<<" <<<< read_synchrotron_data"<<endl;

   
 


    return status;
}
