
//#include"FITS.h"
#include"galprop_classes.h" // since FITS already here and included in galplot

class IRF
{
 public:

  FITS matrix;

  long n_E;
  double *E_lo,*E_hi,*dE; // not yet assigned, taken from RMF
  
  double *sum,*sum1,*sum2,*sum3;// sums for 3 IRF types as function of energy;

 

  int read (char *filename,int idet1,int idet2);
  void print();
};
