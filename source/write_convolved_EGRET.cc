#include"Galplot.h"                 
#include"galprop_classes.h"
#include"galprop.h"





int Galplot::write_convolved_EGRET()
{
    
   cout<<">>>>write_convolved_EGRET "<<endl;


   FITS work;
   char  outfile[200];

   int i_long,i_lat,ip,i_E_EGRET,i_comp;
   
   work.init(galaxy.n_long,galaxy.n_lat,data.n_E_EGRET);

   // pi0 decay skymap

   for (i_long   =0;  i_long<galaxy.n_long; i_long++)  
   for (i_lat    =0;  i_lat <galaxy.n_lat ; i_lat++)
   for (i_E_EGRET=0;  i_E_EGRET  <data.n_E_EGRET;i_E_EGRET++)
       work(i_long,i_lat,i_E_EGRET) = convolved.EGRET_pi0_decay_skymap   .d2[i_long][i_lat].s[i_E_EGRET];

   //   work.print();

    strcpy( outfile, configure.fits_directory);
    strcat( outfile, "pion_decay_skymap_EGRET_convolved_"); 
    strcat( outfile, galdef.galdef_ID);

    cout<<"  writing  pi0_decay skymap to file "<<outfile<<endl;
    work.write(outfile);

  // bremss    skymap

   for (i_long   =0;  i_long<galaxy.n_long; i_long++)  
   for (i_lat    =0;  i_lat <galaxy.n_lat ; i_lat++)
   for (i_E_EGRET=0;  i_E_EGRET  <data.n_E_EGRET;i_E_EGRET++)
       work(i_long,i_lat,i_E_EGRET) = convolved.EGRET_bremss_skymap      .d2[i_long][i_lat].s[i_E_EGRET];

   //   work.print();

    strcpy( outfile, configure.fits_directory);
    strcat( outfile, "bremss_skymap_EGRET_convolved_"); 
    strcat( outfile, galdef.galdef_ID);

    cout<<"  writing  bremss    skymap to file "<<outfile<<endl;
    work.write(outfile);



  // iso IC    skymap

   for (i_long   =0;  i_long<galaxy.n_long; i_long++)  
   for (i_lat    =0;  i_lat <galaxy.n_lat ; i_lat++)
   for (i_E_EGRET=0;  i_E_EGRET  <data.n_E_EGRET;i_E_EGRET++)
       work(i_long,i_lat,i_E_EGRET) =      convolved.EGRET_IC_iso_skymap[0]   .d2[i_long][i_lat].s[i_E_EGRET] 
                                         + convolved.EGRET_IC_iso_skymap[1]   .d2[i_long][i_lat].s[i_E_EGRET] 
                                	 + convolved.EGRET_IC_iso_skymap[2]   .d2[i_long][i_lat].s[i_E_EGRET] ;

   //   work.print();

    strcpy( outfile, configure.fits_directory);
    strcat( outfile, "ics_isotropic_skymap_EGRET_convolved_"); 
    strcat( outfile, galdef.galdef_ID);

    cout<<"  writing  iso IC    skymap to file "<<outfile<<endl;
    work.write(outfile);

 
  // isotropic background  skymap

   for (i_long   =0;  i_long<galaxy.n_long; i_long++)  
   for (i_lat    =0;  i_lat <galaxy.n_lat ; i_lat++)
   for (i_E_EGRET=0;  i_E_EGRET  <data.n_E_EGRET;i_E_EGRET++)
     work(i_long,i_lat,i_E_EGRET) =      convolved.EGRET_isotropic_skymap       .d2[i_long][i_lat].s[i_E_EGRET]; 
              

   //   work.print();

    strcpy( outfile, configure.fits_directory);
    strcat( outfile, "isotropic_skymap_EGRET_convolved_"); 
    strcat( outfile, galdef.galdef_ID);

    cout<<"  writing  isotropic    skymap to file "<<outfile<<endl;
    work.write(outfile);   




  // total    skymap

   for (i_long   =0;  i_long<galaxy.n_long; i_long++)  
   for (i_lat    =0;  i_lat <galaxy.n_lat ; i_lat++)
   for (i_E_EGRET=0;  i_E_EGRET  <data.n_E_EGRET;i_E_EGRET++)
     work(i_long,i_lat,i_E_EGRET) =      convolved.EGRET_total_skymap       .d2[i_long][i_lat].s[i_E_EGRET]; 
              

   //   work.print();

    strcpy( outfile, configure.fits_directory);
    strcat( outfile, "total_skymap_EGRET_convolved_"); 
    strcat( outfile, galdef.galdef_ID);

    cout<<"  writing  total    skymap to file "<<outfile<<endl;
    work.write(outfile);   


 


  cout<<"<<<<write_convolved_EGRET "<<endl;

   return 0;

}
