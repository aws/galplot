#ifndef HIH2IC_h
#define HIH2IC_h

#include "Model.h"
#include "Exposure.h"
#include "Counts.h"
#include "Psf.h"
#include "Sources.h"
#include "Parameters.h"
#include "Variables.h"

/**\brief A sample model class for people to modify.
 *
 * This model is not intended to be used unmodified.
 */
class HIH2IC : public BaseModel {
	public:
		/** \brief The constructor takes the same parameters as the BaseModel constructor.
		 *
		 * The constructor should load and prepare the data used in the model.  It is advised to cache
		 * the results between computation to speed up model fitting, but please keep memory consumption minimal.
		 *
		 * This model loads a skymap from a fits file and scales it.  It also adds a Gaussian skymap profile with
		 * a power law energy dependance.
		 */

                                                                                                                               // use bool to avoid ambiguity between cases

                HIH2IC(const Exposure &exposure, const CountsMap &counts, const Psf &psf, const Sources &sources, const Parameters &pars, const Skymap<char> &filter,                unsigned int configure = 3); //AWS20080717
		HIH2IC(const Exposure &exposure, const CountsMap &counts, const Psf &psf, const Sources &sources, const Parameters &pars, const Skymap<char> &filter, int nrings_in, unsigned int configure = 3); //AWS20080623 AWS20080717

		void getMap(const Variables &vars, Skymap<double> &map);
		gradMap getComponents(const Variables & vars, const std::string &prefix);
		void getGrads(const Variables &vars, const std::string & varName, Skymap<double> &map);
		gradMap getSecDer(const Variables & vars, const std::string & varName1, const std::string & varName2);
	private:
       
		Skymap<double> fpi0Map,fbremssMap,ficsMap;    //AWS20080417
                Skymap<double> fHIMap, fH2Map;                //AWS20080421
                Skymap<double> fHIIMap;                       //AWS20091217
		double flastPrefactor, flastIndex;
};

#endif
