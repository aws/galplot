// sample generator from power-law between limits 
// A. Strong 4 Nov  2005 

using namespace std;
#include<iostream>
#include<cmath>
#include <cstdlib>  //AWS20111010 for sles11 gcc 4.3.4 rand




//////////////////////////////
double power_law_sampler(double  x_min, double x_max, double g)
{
  // samples power law p(x) = x^g between x_min and x_max
  // see notes 20051104

  double x,y;

 // using uniform distribution of integral probability 

 y= double(rand())/RAND_MAX; // uniform in (0,1)

 x = pow(x_min  , g+1.) - y * (pow(x_min, g+1) - pow(x_max, g+1)) ;
 x = pow(x,  1./( g+1.));

 //  cout<<y<<" "<<x<<endl;


return x;

}

///////////////////////////////
int power_law_sampler(unsigned int initialize_seed)               //AWS20030402
{
  // use to re-initialize sequence
 
 srand(initialize_seed);
 return 0;
}
/////////////////////////////// test program
/*
int main()
{

  double x_min,x_max,g,x;
  long  nrept=long(1e7)  ;
  x_min=1.e35;
  x_max=1.e40;  
  g=-1.99;

 double   total=0.;
 double   totalsq=0.;

 double x_sample_min;
 double x_sample_max;
 x_sample_min=1e80;
 x_sample_max=0.;


 unsigned seed;
 seed=12345;
 power_law_sampler(seed); // initialize

 for(long irept=0;irept<nrept;irept++)
 {
  x=power_law_sampler(x_min,x_max,g);
  //  cout<<x<<" ";
  total  += x;
  totalsq+= x*x;
  if(x<x_sample_min) x_sample_min=x;
  if(x>x_sample_max) x_sample_max=x;
 }


 double xbar=       total /nrept;
 double rms  =sqrt(totalsq/nrept - xbar*xbar);

 // analytical form for average
 double xbar_analytical= (g+1)/(g+2)
                       * (pow(x_max,g+2)-pow(x_min,g+2))
                       / (pow(x_max,g+1)-pow(x_min,g+1));

 cout<<endl<<"x_min="<<x_min<<" x_max="<<x_max<<" g="<<g<<" nrept="<<nrept<<endl;
 cout<<endl<<" average= "<<xbar<< " average analytical="<<xbar_analytical<<   " rms dev="<<rms<<                       endl;
 cout<<endl<<" sample min="<<x_sample_min<< " sample max="<<x_sample_max<<                       endl;


return 0;
}


*/
