
//**.****|****.****|****.****|****.****|****.****|****.****|****.****|****.****|
// * read_gcr.cc *                           galprop package * 4/14/2000 
//**"****!****"****!****"****!****"****!****"****!****"****!****"****!****"****|
#include"Galplot.h"                    //AWS20050920
#include"galprop_classes.h"
#include"galprop.h"

#include "fitsio.h" 
int Galplot::read_gcr()                //AWS20050920
{
int stat;

double NUCNORM,ELENORM;
char keyword[20];
char   comment[100];

  cout<<" >>>> read_gcr"<<endl;
stat=0;
   fitsfile *fptr;       /* pointer to the FITS file; defined in fitsio.h */
    int status, ii, jj;
    long  fpixel = 1, naxis = 4, nelements, exposure;
    long naxes[5]  ; 



    char  infile[100];
  
    strcpy( infile,configure.fits_directory);
 
    strcat( infile,"nuclei_full_");
    strcat( infile,galdef.galdef_ID);
    cout<<"  reading  nuclei array from file "<<infile<<endl;

    status = 0;         /* initialize status before calling fitsio routines */
    fits_open_file(&fptr,  infile,READONLY, &status);   
    cout<<"  fits open status = "<<status<<endl;

    if (galdef.n_spatial_dimensions==2)strcpy(keyword,"NAXIS4");
    if (galdef.n_spatial_dimensions==3)strcpy(keyword,"NAXIS5");
    fits_read_key(fptr, TINT   , keyword , &n_species,comment, &status);
    cout<<"  n_species = "<<n_species<<endl;

    create_gcr(0); // AWS20130131


    if(gcr[0].n_spatial_dimensions==2){
     naxis=4;
     naxes[0]=gcr[0].n_rgrid;
     naxes[1]=gcr[0].n_zgrid;
     naxes[2]=gcr[0].n_pgrid;
     naxes[3]=n_species;
     nelements=naxes[0]*naxes[1]*naxes[2]*naxes[3];
    }

    if(gcr[0].n_spatial_dimensions==3){
     naxis=5;
     naxes[0]=gcr[0].n_xgrid;
     naxes[1]=gcr[0].n_ygrid;
     naxes[2]=gcr[0].n_zgrid;
     naxes[3]=gcr[0].n_pgrid;
     naxes[4]=n_species;
     nelements=naxes[0]*naxes[1]*naxes[2]*naxes[3]*naxes[4];
    }

    
    float *array;          
    array=new float[nelements];
    /* Read the array of floats  */
    float nulval=0;
    int anynul;
    fits_read_img(fptr, TFLOAT, fpixel, nelements, &nulval,array, &anynul,&status);
 


    if(gcr[0].n_spatial_dimensions==2){
   int i=0;
   for (int i_species=0;i_species< naxes[3];i_species++){ 
   for (int ip       =0;        ip<naxes[2];       ip++){
   for (int iz       =0;        iz<naxes[1];       iz++){
   for (int ir       =0;        ir<naxes[0];       ir++){


      /*this is how the arrays are output, for reference
     if(gcr[i_species].A!=0)  
     array[i]=    gcr[i_species].cr_density.d2[ir]    [iz].s[ip] 
             *    gcr[i_species].A
             *pow(gcr[i_species].Ekin[ip],2);

     if(gcr[i_species].A==0)  // electrons, positrons
     array[i]=    gcr[i_species].cr_density.d2[ir]    [iz].s[ip] 
             *pow(gcr[i_species].Ekin[ip],2);
	     */


     //    if(gcr[i_species].A!=0)
       gcr[i_species].cr_density.d2[ir]    [iz].s[ip]=array[i];

     /*
             /    gcr[i_species].A
             /pow(gcr[i_species].Ekin[ip],2);

     if(gcr[i_species].A==0)  // electrons, positrons
       gcr[i_species].cr_density.d2[ir]    [iz].s[ip]= array[i];
             /pow(gcr[i_species].Ekin[ip],2);
     */
           i++;
   }//ir
   }//iz
   }//ip
     if(galdef.verbose       ==1)gcr[i_species].cr_density.print();
     if(galplotdef.verbose==-901)gcr[i_species].cr_density.print();//selectable debug

   }//i_species
    }//n_spatial_dimensions==2






    if(gcr[0].n_spatial_dimensions==3){
 
   int i=0;
   for (int i_species=0;i_species< naxes[4];i_species++){ 
   for (int ip       =0;        ip<naxes[3];       ip++){
   for (int iz       =0;        iz<naxes[2];       iz++){
   for (int iy       =0;        iy<naxes[1];       iy++){
   for (int ix       =0;        ix<naxes[0];       ix++){

     /* this is how the arrays are output, for reference
     if(gcr[i_species].A!=0)
     array[i]=    gcr[i_species].cr_density.d3[ix][iy][iz].s[ip] 
             *    gcr[i_species].A
             *pow(gcr[i_species].Ekin[ip],2);


     if(gcr[i_species].A==0)     // electrons, positrons
     array[i]=    gcr[i_species].cr_density.d3[ix][iy][iz].s[ip]
             *pow(gcr[i_species].Ekin[ip],2);
	     */


     //     if(gcr[i_species].A!=0)
       gcr[i_species].cr_density.d3[ix][iy][iz].s[ip]=array[i];
       /*
             /    gcr[i_species].A
             /pow(gcr[i_species].Ekin[ip],2);


     if(gcr[i_species].A==0)     // electrons, positrons
       gcr[i_species].cr_density.d3[ix][iy][iz].s[ip]=array[i];
             /pow(gcr[i_species].Ekin[ip],2);
       */
           i++;
   }//ix
   }//iy
   }//iz
   }//ip
     if(galdef.verbose       ==1)gcr[i_species].cr_density.print();
     if(galplotdef.verbose==-901)gcr[i_species].cr_density.print();//selectable debug

   }//i_species


    }//n_spatial_dimensions==3

  for (int i_species=0;i_species< n_species;i_species++)
  { 
   sprintf(keyword,"NUCZ%03d",       i_species+1 ); // e.g. NUCZ012
   fits_read_key(fptr, TINT   , keyword , &gcr[i_species].Z,comment, &status);
   cout<<keyword<<" "<<gcr[i_species].Z<<" ";

   sprintf(keyword,"NUCA%03d",       i_species+1 ); // e.g. NUCA012
   fits_read_key(fptr, TINT   , keyword , &gcr[i_species].A,comment, &status);
   cout<<keyword<<" "<<gcr[i_species].A<<" " ;

   sprintf(keyword,"NUCK%03d",       i_species+1 ); // e.g. NUCK012
   fits_read_key(fptr, TINT   , keyword , &gcr[i_species].K_electron,comment, &status);
   cout<<keyword<<" "<<gcr[i_species].K_electron<<endl;

  }


  // assign names according to GALPROP convention //AWS20100312

  //int primary_electrons=1;// AWS20100512
  int secondary_electrons=1;//AWS20100512
  
  for (int i_species=0;i_species< n_species;i_species++)
  {
    if(gcr[i_species].Z==+1 && gcr[i_species].A==  1) strcpy(gcr[i_species].name,"Hydrogen_1"         ); 
    if(gcr[i_species].Z==+1 && gcr[i_species].A==  2) strcpy(gcr[i_species].name,"Hydrogen_2"         );
    if(gcr[i_species].Z==+2 && gcr[i_species].A==  3) strcpy(gcr[i_species].name,"Helium_3"           );
    if(gcr[i_species].Z==+2 && gcr[i_species].A==  4) strcpy(gcr[i_species].name,"Helium_4"           );
    if(gcr[i_species].Z==+6 && gcr[i_species].A== 11) strcpy(gcr[i_species].name,"Carbon_11"          );
    if(gcr[i_species].Z==+6 && gcr[i_species].A== 12) strcpy(gcr[i_species].name,"Carbon_12"          );

   
    /* AWS20100512 wrong, secondary electrons come first
    if(gcr[i_species].Z==-1 && gcr[i_species].A==  0 && primary_electrons==0)                             //primary electrons come before secondary electrons in file            
                                                      strcpy(gcr[i_species].name,"secondary_electrons"  );//note the order of this logic to get desired effect

    if(gcr[i_species].Z==-1 && gcr[i_species].A==  0 && primary_electrons==1)                         
                                                     { strcpy(gcr[i_species].name,"primary_electrons"  ); primary_electrons=0;}
    */
 
       

    if(gcr[i_species].Z==-1 && gcr[i_species].A==  0 && secondary_electrons==0)    //secondary electrons come before primary electrons in file   AWS20100512
                                                                 strcpy(gcr[i_species].name,"primary_electrons"  );//note the order of this logic to get desired effect

    if(gcr[i_species].Z==-1 && gcr[i_species].A==  0 && secondary_electrons==1)                         
                                                               { strcpy(gcr[i_species].name,"secondary_electrons"  ); secondary_electrons=0;} // AWS20100512


    if(gcr[i_species].Z==+1 && gcr[i_species].A==  0) strcpy(gcr[i_species].name,"secondary_positrons");


    cout<<"read_gcr: i_species="<<i_species<<" Z="<<gcr[i_species].Z<<" A="<<gcr[i_species].A<<"  gcr[i_species].name="<< gcr[i_species].name<<endl;

  }


  create_gcr(1);//  AWS20130131


  for (int i_species=0;i_species< n_species;i_species++)gcr[i_species].print(); //AWS20100312




    /* AWS20020722: not required for galplot

    // remove normalization factor to allow further processing
    // for a warm start

 

   for (int i_species=0;i_species< n_species;i_species++){ 
     //nuclei 
    if(gcr[i_species].A!=0){
     fits_read_key(fptr,TDOUBLE,"NUCNORM" ,&NUCNORM ,comment,&status);  
     cout<<"read NUCNORM status= "<<status<<"  NUCNORM="<<NUCNORM<<endl;
     gcr[i_species].cr_density/=NUCNORM;
    }
    // positrons
    if(gcr[i_species].A==0&&gcr[i_species].Z==+1){
     fits_read_key(fptr,TDOUBLE,"NUCNORM" ,&NUCNORM ,comment,&status);  
     cout<<"read NUCNORM status= "<<status<<"  NUCNORM="<<NUCNORM<<endl;
     gcr[i_species].cr_density/=NUCNORM;
    }    
    // electrons
    if(gcr[i_species].A==0&&gcr[i_species].Z==-1){
     fits_read_key(fptr,TDOUBLE,"ELENORM" ,&ELENORM ,comment,&status);  
     cout<<"read ELENORM status= "<<status<<" ELENORM="<<ELENORM<<endl;
     gcr[i_species].cr_density/=ELENORM;
    }
    }//i_species
    AWS20020722: not required for galplot */

    fits_close_file(fptr, &status);            /* close the file */

    fits_report_error(stderr, status);  /* print out any error messages */


    delete[] array; //AWS20010216

  cout<<" <<<< read_gcr"<<endl;

    return( status );
return stat;
}
