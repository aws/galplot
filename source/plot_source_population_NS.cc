
using namespace std;
#include"Galplot.h"                  
#include"galprop_classes.h"
#include"galprop.h"
#include"constants.h"

#include "fitsio.h" 
#include "slalib.h"

int  Galplot::plot_source_population_NS()
{
 int stat;
 

 TCanvas *c1;
 TH1F *h;
 TGraph *spectrum;
 TGraph *datapoint;

TText *text;
TLatex *latex;
char name[100],canvastitle[100], workstring1[100],workstring2[100];;
char  psfile[400];
char giffile[400];
char txtfile[400];

 int ii;
 double xmin,xmax; //AWS20060109
 double ymin,ymax; //AWS20132604

  cout<<" >>>> plot_source_population_NS"<<endl;

   stat=0;
   

 //====== see HowTo Style: and do before creating canvas


   TStyle *plain  = new TStyle("Plain","Plain Style (no colors/fill areas)");


   plain->SetCanvasBorderMode(0);
   plain->SetPadBorderMode(0);
   plain->SetPadColor(0);
   plain->SetCanvasColor(0);
   //  plain->SetTitleColor(0);
   plain->SetTitleColor(1,"X"); //AWS20081217  was (0), changed for root 5.08. Otherwise X title does not appear !
   plain->SetTitleColor(1,"Y"); //AWS20081217

   plain->SetStatColor(0);
   gROOT->SetStyle("Plain");


  int ncolors=1; int *colors=0;
  gStyle->SetPalette(ncolors,colors);
 
  // define line styles for SetLineStyle (only 1-4 predefined at root 5.08)       AWS20071005 AWS20081217
  gStyle->SetLineStyleString(1," ");
  gStyle->SetLineStyleString(2,"12 12");
  gStyle->SetLineStyleString(3,"4 8");
  gStyle->SetLineStyleString(4,"12 16 4 16");
  gStyle->SetLineStyleString(5,"20 12 4 12");
  gStyle->SetLineStyleString(6,"20 12 4 12 4 12 4 12");
  gStyle->SetLineStyleString(7,"20 20");
  gStyle->SetLineStyleString(8,"20 12 4 12 4 12");
  gStyle->SetLineStyleString(9,"80 20");
  gStyle->SetLineStyleString(10,"80 40 4 40");

  strcpy(name,"source_population_NS");// to identify it: name is not plotted
  c1=new TCanvas(name,name,200,100,650,650);

  c1  ->SetLogx(1);
  c1  ->SetLogy(1);

  xmin=sourcepop5.flux_detection_limit*1e-5;                   //AWS20110819 was sourcepop1
  xmax=sourcepop5.flux_detection_limit*1e+3;                   //AWS20110819 was sourcepop1

  xmin=1e-14;  //AWS20130329 
  xmin=5e-13;  //AWS20141020 for 3FGL, includes peak but not minimum
  xmax=1e-06;  //AWS20140121 for 2FGL (was 1e-07 for LAT hard source paper)
     

  ymin=1e-1;
  ymax=2e+3;   //AWS20130426 was 1e5
  ymax=5e+3;   //AWS20141020 for 3FGL

  //               xmin,xmax                ymin,ymax   
  //  h=c1->DrawFrame( 1e-10                      ,1e-1,
  //                   1e-4                       ,1e3);

  h=c1->DrawFrame( xmin,                       ymin,  //AWS20130426
                   xmax,                       ymax); //AWS20130426 


  h ->SetXTitle("Flux, photons   cm^{-2}  s^{-1}              "); //needs {} to work
  h ->SetYTitle("Source number counts"); 

  h ->SetLabelOffset(+0.00,"X"); //  +ve down
  h ->SetLabelSize  ( 0.03,"Y"); // labels, not titles


  h ->SetTitleOffset(+1.1,"X");  //  +ve down
  h ->SetTitleOffset(+1.2,"Y");  //  +ve to left

  h ->SetTitleSize   (0.04 ,"X");// titles, not labels  was default
  h ->SetTitleSize   (0.04 ,"Y");// titles, not labels  was 0.04

spectrum=new TGraph(sourcepop5.n_dlnN_dlnS); //AWS20110819 was sourcepop1






 for  (ii    =0;  ii    < sourcepop5.n_dlnN_dlnS ;ii++  ) //AWS20110819 was sourcepop1
      spectrum->SetPoint(ii, 
                         pow(10,sourcepop5.lnS_min +(ii+0.5)*sourcepop5.dlnS), //AWS20110819 was sourcepop1
	  		        sourcepop5.dlnN_dlnS[ii] );                    //AWS20110819 was sourcepop1
  

//Gtypes.h:enum EColor { kWhite, kBlack, kRed, kGreen, kBlue, kYellow, kMagenta, kCyan };
//enum EColor { kWhite =0,   kBlack =1,   kGray=920
//             ,kRed   =632, kGreen =416, kBlue=600, kYellow=400, kMagenta=616, kCyan=432
//             ,kOrange=800, kSpring=820, kTeal=840, kAzure =860, kViolet =880, kPink=900};

  spectrum->SetMarkerColor(kBlack  );
  spectrum->SetMarkerStyle(21);
  spectrum->SetMarkerStyle(22); // triangles
  spectrum->SetMarkerSize (1.5);//
  spectrum->SetLineColor(kBlack  );
  spectrum->SetLineWidth(2     );
  spectrum->SetLineStyle(1      );//1=solid 2=dash 3=dot 4=dash-dot
  spectrum->SetTitle(""         ); // removes text written in box on plot




  spectrum->Draw(" L ");       // no axes L= lines P=points 


  //spectrum->Draw("ALPF");  // axes; points as markers;line  ; fill area

  // above detection limit  AWS20170123

  spectrum=new TGraph(sourcepop5.n_dlnN_dlnS);

  spectrum->SetMarkerColor(kBlack  );
  spectrum->SetMarkerStyle(21);
  spectrum->SetMarkerStyle(22); // triangles
  spectrum->SetMarkerSize (1.5);//
  spectrum->SetLineColor(kMagenta  );
  spectrum->SetLineWidth(2     );
  spectrum->SetLineStyle(1      );//1=solid 2=dash 3=dot 4=dash-dot
  spectrum->SetTitle(""         ); // removes text written in box on plot

  for  (ii    =0;  ii    < sourcepop5.n_dlnN_dlnS ;ii++  ) 
      spectrum->SetPoint(ii, 
                         pow(10,sourcepop5.lnS_min +(ii+0.5)*sourcepop5.dlnS), 
	  		        sourcepop5.dlnN_dlnS_soplimit[ii] );                    

  spectrum->Draw(" L ");       // no axes L= lines P=points 


  // below detection limit  AWS20170123

  spectrum=new TGraph(sourcepop5.n_dlnN_dlnS);

  spectrum->SetMarkerColor(kBlack  );
  spectrum->SetMarkerStyle(21);
  spectrum->SetMarkerStyle(22); // triangles
  spectrum->SetMarkerSize (1.5);//
  spectrum->SetLineColor(kCyan  );
  spectrum->SetLineWidth(2     );
  spectrum->SetLineStyle(1      );//1=solid 2=dash 3=dot 4=dash-dot
  spectrum->SetTitle(""         ); // removes text written in box on plot

  for  (ii    =0;  ii    < sourcepop5.n_dlnN_dlnS ;ii++  ) 
      spectrum->SetPoint(ii, 
                         pow(10,sourcepop5.lnS_min +(ii+0.5)*sourcepop5.dlnS), 
	  		        sourcepop5.dlnN_dlnS_sublimit[ii] );                    

  spectrum->Draw(" L ");       // no axes L= lines P=points 





// MSPs

  if(galplotdef.sourcepop_ext_model==2)//AWS20120522
  {
  
   spectrum=new TGraph(sourcepop2.n_dlnN_dlnS);

   for  (ii    =0;  ii    < sourcepop5.n_dlnN_dlnS ;ii++  ) //AWS20110819 was sourcepop1
      spectrum->SetPoint(ii,  
                         pow(10,sourcepop2.lnS_min +(ii+0.5)*sourcepop2.dlnS),            
                                sourcepop2.dlnN_dlnS[ii] );

  spectrum->SetMarkerColor(kRed  );
  spectrum->SetMarkerStyle(21);
  spectrum->SetMarkerStyle(22); // triangles
  spectrum->SetMarkerSize (1.5);//
  spectrum->SetLineColor(kRed  );
  spectrum->SetLineWidth(2     );
  spectrum->SetLineStyle(1      );//1=solid 2=dash 3=dot 4=dash-dot

  spectrum->Draw("L "); 


  } //AWS20140114 (before it included EGRET catalogue in if block)

//----------  3rd EGRET Catalogue

 if(0) //AWS20140114
 {  
  spectrum=new TGraph(sourcepop2.n_dlnN_dlnS);

 for  (ii    =0;  ii    < sourcepop5.n_dlnN_dlnS ;ii++  ) //AWS20110819 was sourcepop1
      spectrum->SetPoint(ii,  
                         pow(10,sourcepop2.lnS_min +(ii+0.5)*sourcepop2.dlnS),            
                                sourcepop2.dlnN_dlnS[ii] );

  spectrum->SetMarkerColor(kRed  );
  spectrum->SetMarkerStyle(21);
  spectrum->SetMarkerStyle(22); // triangles 
  spectrum->SetMarkerSize (1.5);//
  spectrum->SetLineColor(kRed  );
  spectrum->SetLineWidth(2     );
  spectrum->SetLineStyle(1      );//1=solid 2=dash 3=dot 4=dash-dot

  spectrum->Draw("P "); 

    }

//----------      Fermi Catalogue  1    all sources                         //AWS20081216
  
spectrum=new TGraph(sourcepop4.n_dlnN_dlnS);

 for  (ii    =0;  ii    < sourcepop4.n_dlnN_dlnS ;ii++  )
      spectrum->SetPoint(ii,  
                         pow(10,sourcepop4.lnS_min +(ii+0.5)*sourcepop4.dlnS),            
                                sourcepop4.dlnN_dlnS[ii] );

  spectrum->SetMarkerColor(kBlack );//AWS20140121
  spectrum->SetMarkerStyle(21);
  spectrum->SetMarkerStyle(21); // (20=circles, 21=squares, 22= triangles)
  spectrum->SetMarkerSize (1.3);//
  spectrum->SetLineColor(kBlack );  //AWS20140121
  spectrum->SetLineWidth(2     );
  spectrum->SetLineStyle(1      );//1=solid 2=dash 3=dot 4=dash-dot

  spectrum->Draw("P "); 

  
//----------      Fermi Catalogue  2    Galactic assocs                         //AWS20140114
  
spectrum=new TGraph(sourcepop6.n_dlnN_dlnS);

 for  (ii    =0;  ii    < sourcepop6.n_dlnN_dlnS ;ii++  )
      spectrum->SetPoint(ii,  
                         pow(10,sourcepop6.lnS_min +(ii+0.5)*sourcepop6.dlnS),            
                                sourcepop6.dlnN_dlnS[ii] );

  spectrum->SetMarkerColor(kBlue ); //AWS20140121
  spectrum->SetMarkerStyle(21);
  spectrum->SetMarkerStyle(22); //  (20=circles, 21=squares, 22= triangles)
  spectrum->SetMarkerSize (1.5);//
  spectrum->SetLineColor(kBlue );   //AWS20140121
  spectrum->SetLineWidth(2     );
  spectrum->SetLineStyle(1      );//1=solid 2=dash 3=dot 4=dash-dot

  spectrum->Draw("P "); 


//----------      Fermi Catalogue  3    Galactic assocs + unids                         //AWS20140115
  
spectrum=new TGraph(sourcepop7.n_dlnN_dlnS);

 for  (ii    =0;  ii    < sourcepop7.n_dlnN_dlnS ;ii++  )
      spectrum->SetPoint(ii,  
                         pow(10,sourcepop7.lnS_min +(ii+0.5)*sourcepop7.dlnS),            
                                sourcepop7.dlnN_dlnS[ii] );

  spectrum->SetMarkerColor(kRed); //AWS20140121
  spectrum->SetMarkerStyle(21);
  spectrum->SetMarkerStyle(20); // (20=circles, 21=squares, 22= triangles)
  spectrum->SetMarkerSize (1.0);//AWS20140126 smaller to avoid overwriting Catalogue 2 if equal
  spectrum->SetLineColor(kRed );   //AWS20140121
  spectrum->SetLineWidth(2     );
  spectrum->SetLineStyle(1      );//1=solid 2=dash 3=dot 4=dash-dot

  spectrum->Draw("P "); 



//----------------------------------------- text


  
  strcpy(canvastitle," galdef ID ");
  strcat(canvastitle,galdef.galdef_ID);


  sprintf(workstring1,"  %5.2f<l<%5.2f , %5.2f<l<%5.2f",               
                          galplotdef.long_min1,galplotdef.long_max1,
                          galplotdef.long_min2,galplotdef.long_max2);

  sprintf(workstring2,"  %5.2f<b<%5.2f , %5.2f<b<%5.2f",                 
                         galplotdef.lat_min1,galplotdef.lat_max1,
                         galplotdef.lat_min2,galplotdef.lat_max2);




  text=new TText();
  text->SetTextFont(62);
  text->SetTextColor(kBlack);
  text->SetTextSize(0.034 );                                         
  text->SetTextAlign(12);

  if(galplotdef.verbose!=-4001 &&galplotdef.verbose!=-4002  )  //AWS20130426
  text->DrawTextNDC(.10 ,.93 ,canvastitle);// NDC=normalized coord system  

 
  if(                            galplotdef.verbose!=-4002  )  //AWS20130426
  {
  text->SetTextSize(0.022 );                                            
  text->DrawTextNDC(.58 ,.88 ,workstring1);// NDC=normalized coord system  
  text->DrawTextNDC(.58 ,.86 ,workstring2);// NDC=normalized coord system   
  }

 //============== postscript, gif and txt files

  sprintf(workstring1,"l_%.2f_%.2f_%.2f_%.2f",                          
                          galplotdef.long_min1,galplotdef.long_max1,
                          galplotdef.long_min2,galplotdef.long_max2);

  sprintf(workstring2,"b_%.2f_%.2f_%.2f_%.2f",                          
                          galplotdef.lat_min1,galplotdef.lat_max1,
                          galplotdef.lat_min2,galplotdef.lat_max2);

  strcpy(psfile,"plots/");
  strcat(psfile,"source_population_NS_");
  strcat(psfile,galdef.galdef_ID);
  strcat(psfile,"_"        );
  strcat(psfile,workstring1);
  strcat(psfile,"_"        );
  strcat(psfile,workstring2);
  strcat(psfile,"_"        ); //AWS20081217

  strcat(psfile,galplotdef.psfile_tag);
  strcpy(giffile,psfile);

 if(galplotdef.output_format==1 || galplotdef.output_format==3)
   {
   strcat(psfile,".eps");
   cout<<"postscript file="<<psfile<<endl;
   c1->Print(psfile,"eps" );
  }

  if(galplotdef.output_format==2 || galplotdef.output_format==3)
   {
   strcat(giffile,".gif");
   cout<<"       gif file="<<giffile<<endl;
   c1->Print(giffile,"gif" );
  }


    cout<<" <<<< plot_source_population_NS"<<endl;

   
    return stat;
}
