/*

1. power-law integration
2. sum over model energy intervals for uniform algorithm:
   I(Emin,Emin)=sum_model_bins(contribution from model bin E1,E2 to Emin,Emax)
                               =f(E11,E12,I1,I2)
E1  E11        E12     E2
      Emin        Emax

this is general, holds for any relation of ranges.


3. intensity must be given in form E^2 I(E)
 */
using namespace std;  //AWS20050919
#include<iostream>    //AWS20050919
#include<cmath>       //AWS20050919

double energy_integral
(double emin,double emax,double e1,double e2,double intensity_esq1,double intensity_esq2)
{

  double result;
  double intensity1,intensity2;
  double e11,e22;
  double A,gamma;


  if(emin>e2 || emax <e1){result=0.;return result;} // lies completely outside interval
  
  e11=e1;
  e22=e2;

  if(emin>e1)e11=emin;
  if(emax<e2)e22=emax;

  //cout<<" e11="<<e11<<" e22="<<e22<<") "      ;

  if(e11>=e22){result=0.;return result;}
  if(intensity_esq1<=0.0){result=0.;return result;}
  if(intensity_esq2<=0.0){result=0.;return result;}

  gamma=2-log(intensity_esq1/intensity_esq2)/log(e1/e2);
  A    =intensity_esq1/pow(e1,2-gamma);

  // cout<<" (A="<<A<<"  gamma="<<gamma<<" e11="<<e11<<" e22="<<e22<<") "      ;

  if(fabs(gamma-1.)<.0001)cout<<"energy_integral: warning gamma="<<gamma<<endl;

  //  if(gamma==1.)
  if(fabs(gamma-1.)<.0001)
    {
      result=A*log(e22/e11);
      return result;
    }

  result=A/(1-gamma)*(pow(e22,-gamma+1)-pow(e11,-gamma+1));
  return result;
}

int test_energy_integral    ()
{

  double emin, emax, e1,e2, intensity1, intensity2;


  cout<<"testing energy_integral"<<endl;

  // emin e1 emax e2

  emin=70;
  emax=150;
  e1=100;
  e2=200;
  intensity1=10  *pow(e1,2);
  intensity2=1.25*pow(e2,2);

  cout<<"emin emax e1 e2 intensity1 intensity2"<<" "<<emin<<" "<<emax<<" "<<e1<<" "<<e2<<" "
      <<intensity1<<" "<<intensity2
      <<" integral=" <<energy_integral( emin, emax, e1, e2, intensity1,intensity2)<<endl;

  // e1 emin emax e2
  emin=110;
  emax=120;
  


  cout<<"emin emax e1 e2 intensity1 intensity2"<<" "<<emin<<" "<<emax<<" "<<e1<<" "<<e2<<" "
      <<intensity1<<" "<<intensity2
      <<" integral=" <<energy_integral( emin, emax, e1, e2, intensity1,intensity2)<<endl;

  // e1 emin  e2  emax
  emin=110;
  emax=300;
  e1=100;
  e2=200;
 

  cout<<"emin emax e1 e2 intensity1 intensity2"<<" "<<emin<<" "<<emax<<" "<<e1<<" "<<e2<<" "
      <<intensity1<<" "<<intensity2
      <<" integral=" <<energy_integral( emin, emax, e1, e2, intensity1,intensity2)<<endl;
  // emin emax e1 e2
  emin=70;
  emax=90 ;
  e1=100;
  e2=200;
 
  cout<<"emin emax e1 e2 intensity1 intensity2"<<" "<<emin<<" "<<emax<<" "<<e1<<" "<<e2<<" "
      <<intensity1<<" "<<intensity2
      <<" integral=" <<energy_integral( emin, emax, e1, e2, intensity1,intensity2)<<endl;

  // e1 e2 emin emax
  emin=200;
  emax=300;
  e1=100;
  e2=200;

  cout<<"emin emax e1 e2 intensity1 intensity2"<<" "<<emin<<" "<<emax<<" "<<e1<<" "<<e2<<" "
      <<intensity1<<" "<<intensity2
      <<" integral=" <<energy_integral( emin, emax, e1, e2, intensity1,intensity2)<<endl;


  // gamma~1
  emin=100;
  emax=200;
  e1=100;
  e2=200;
  intensity1=   10*pow(e1,2);
  intensity2= 5.01*pow(e2,2);
  cout<<"emin emax e1 e2 intensity1 intensity2"<<" "<<emin<<" "<<emax<<" "<<e1<<" "<<e2<<" "
      <<intensity1<<" "<<intensity2
      <<" integral=" <<energy_integral( emin, emax, e1, e2, intensity1,intensity2)<<endl;

 // gamma~1
  emin=100;
  emax=200;
  e1=100;
  e2=200;
  intensity1=   10*pow(e1,2);
  intensity2= 4.99*pow(e2,2);
  cout<<"emin emax e1 e2 intensity1 intensity2"<<" "<<emin<<" "<<emax<<" "<<e1<<" "<<e2<<" "
      <<intensity1<<" "<<intensity2
      <<" integral=" <<energy_integral( emin, emax, e1, e2, intensity1,intensity2)<<endl;

  // gamma=1
  emin=100;
  emax=200;
  e1=100;
  e2=200;
  intensity1=   10*pow(e1,2);
  intensity2=    5*pow(e2,2);
  cout<<"emin emax e1 e2 intensity1 intensity2"<<" "<<emin<<" "<<emax<<" "<<e1<<" "<<e2<<" "
      <<intensity1<<" "<<intensity2
      <<" integral=" <<energy_integral( emin, emax, e1, e2, intensity1,intensity2)<<endl;

  return 0;
}
