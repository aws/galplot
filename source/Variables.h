#ifndef Variables_h
#define Variables_h

#include "Parameters.h"
#include <string>
#include <map>
#include <exception>
#include <iostream>
#include <vector>

/** \brief Handles communication of free variables between models and fitters.
 *
 * Stores the names, value and limits of variables.
 */
class Variables {
	public:
		//! Default constructor to initialize the variables
		Variables() : fmaxLength(0) {}
		/** \brief Add variables from a Parameters object.
		 *
		 * \param names is the variable names to look up in the Parameters object
		 * \param pars is the Parameters object containing the parameters and their value.  
		 * The format for the variables in the parameter object has to be
		 * \code
		 * name = value errorEstimate <lowerLimit upperLimit>
		 * \endcode
		 * The limits are optional but both have to be present if one is.
		 */
		void add(const std::vector<std::string> & names, const Parameters &pars);
		/** \brief Add variables from another Variables object */
		void add(const Variables & vars);
		/** \brief Add a variable without limits 
		 *
		 * \param name is the variable name
		 * \param value is its value
		 * \param error is its error boundary
		 */
		void add(const std::string &name, double value, double error);
		/** \brief Add a variable with limits 
		 *
		 * \param name is the variable name
		 * \param value is its value
		 * \param error is its error boundary
		 * \param lowerLimit is its lower limit
		 * \param upperLimit is its upper limit
		 */
		void add(const std::string &name, double value, double error, double lowerLimit, double upperLimit);

		/** \brief Return a reference to the variable value */
		double & operator [] (const std::string &name);
		/** \brief Return a constant reference to the variable value */
		const double & operator [] (const std::string &name) const;
		/** \brief Return a reference to the variable value */
		double & value(const std::string &name);
		/** \brief Return a constant reference to the variable value */
		const double & value(const std::string &name) const;
		
		/** \brief Return a reference to the variable error */
		double & error(const std::string &name);
		/** \brief Return a constant reference to the variable error */
		const double & error(const std::string &name) const;
		
		/** \brief Return a vector with all the variable names */
		std::vector<std::string> getNames() const;

		/** \brief Set the upper limit of a variable
		 * \param name is the variable name
		 * \param limit is the new upper limit.
		 */
		void setUpperLimit(const std::string &name, double limit);
		/** \brief Set the lower limit of a variable
		 * \param name is the variable name
		 * \param limit is the new lower limit.
		 */
		void setLowerLimit(const std::string &name, double limit);
		
		/** \brief Return a constant reference to the variable upper limit, if it exists */
		const double & upperLimit(const std::string &name) const;
		/** \brief Return a constant reference to the variable lower limit, if it exists */
		const double & lowerLimit(const std::string &name) const;

		/** \brief Return the values of the variables as a vector.
		 *
		 * Uses the alphabetical order of the parameters
		 */
		std::vector<double> getValueVector() const;
		/** \brief Sets the values of the variables from a vector.
		 *
		 * Uses the alphabetical order of the parameters and throws an error if the sizes do not match.
		 */
		void setValueVector(const std::vector<double> & values);

		//! Clear all set variables
		void clear();
		
		/** \brief Default error class */
		class VariableError : public std::exception {
			private:
				std::string eMessage;
			public:
				/** \brief Sets a default error string "Error in Variables class" */
				VariableError() : eMessage("Error in Variables class"){}
				/** \brief Sets a specialized error string */
				VariableError(const std::string & message) : eMessage(message){}
				~VariableError() throw(){}
				/** \brief Returns the error string */
				virtual const char* what () const throw(){
					return(eMessage.c_str());
				}
		};
		/** \brief Thrown when the variable name does not exist */
		class VariableNotFound : public std::exception {
			private:
				std::string eMessage;
			public:
				/** \brief Sets a default error string "Variable not found" */
				VariableNotFound() : eMessage("Variable not found"){}
				/** \brief Sets a specialized error string */
				VariableNotFound(const std::string & message) : eMessage(message){}
				~VariableNotFound() throw(){}
				/** \brief Returns the error string */
				virtual const char* what () const throw(){
					return(eMessage.c_str());
				}
		};
		/** \brief Thrown when trying to access a limit that is not there */
		class LimitNotSet : public std::exception {
			private:
				std::string eMessage;
			public:
				/** \brief Sets a default error string "Limit not set" */
				LimitNotSet() : eMessage("Limit not set"){}
				/** \brief Sets a specialized error string */
				LimitNotSet(const std::string & message) : eMessage(message){}
				~LimitNotSet() throw(){}
				/** \brief Returns the error string */
				virtual const char* what () const throw(){
					return(eMessage.c_str());
				}
		};
		/** \brief Thrown when trying to create a variable with a name that already exists */
		class VariableAlreadyCreated : public std::exception {
			private:
				std::string eMessage;
			public:
				/** \brief Sets a default error string "Variable already created" */
				VariableAlreadyCreated() : eMessage("Variable already created"){}
				/** \brief Sets a specialized error string */
				VariableAlreadyCreated(const std::string & message) : eMessage(message){}
				~VariableAlreadyCreated() throw(){}
				/** \brief Returns the error string */
				virtual const char* what () const throw(){
					return(eMessage.c_str());
				}
		};
		/** \brief Output operator for the Variables class */
		friend std::ostream & operator << (std::ostream &os, const Variables &vars);

	private:
		/** \brief A structure to store each variable */
		struct variable {
			double value, error, uLimit, lLimit;
			bool uSet, lSet;
		};
		std::map<std::string, variable> fvars;
		typedef std::map<std::string, variable>::iterator varIt;
		typedef std::map<std::string, variable>::const_iterator cvarIt;
		int fmaxLength; //!< The maximum lenght of a variable name
};

#endif
