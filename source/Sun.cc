#include "Sun.h"
#include "Model.h"
#include "Exposure.h"
#include "Counts.h"
#include "Psf.h"
#include "Sources.h"
#include "Parameters.h"
#include "Variables.h"
#include "Skymap.h"
//#include "SkymapFitsio.h"  AWS20200601 SkymapFitsio.cc outdated, not used
#include <string>
#include <vector>

// developed by Andy Strong, 20080901
// a 2 component model consisting of 1. solar disk 2. solar inverse-Compton


Sun::Sun(const Exposure &exposure, const CountsMap &counts, const Psf &psf, const Sources &sources, const Parameters &pars, const Skymap<char> & filter, unsigned int configure):  
         BaseModel(counts,exposure,psf,sources,pars,filter,configure)
        {
         


	std::cout<<"Model Sun"; 
        std::cout<<std::endl;

	//Get some parameters from the Parameters object.  These might point to skymaps or mapcubes.
	//Note that the Parameters object throws an exception if the parameter name is not in the object.
      
        std::string dirName;                                            //AWS20080613
	std::string mapName;
      

	//The Parameters object can handle most data types
        fparameters.getParameter("galpropFitsDirectory", dirName);      //AWS20080613
	std::cout<<"dirName="<<dirName<<std::endl;                      //AWS20080613
      
	

	//The Parameters object should contain initial values for the variables we modify in the model
	//The variables object has a method to add variables from parameters objects.  We only 
	//need to supply a vector of variable names to look for
	std::vector<std::string> variableNames;
       
       	variableNames.push_back("Sun_disk_MapScale");
	variableNames.push_back("Sun_IC_MapScale"); 



	//Set up the variables from the parameters
	//Note that the fvariables is inherited from BaseModel
	fvariables.add(variableNames, fparameters);

     

	// Sun disk counts  skymap

	 fparameters.getParameter("Sun_disk_ModelMapName", mapName); 
     

	 //	 mapName=dirName+"/" + mapName; // use full pathname AWS20080905                 
      	std::cout<<"mapName="<<mapName<<std::endl;                               

	//Read in a skymap from a fits file
	fSun_disk_Map.load(mapName);


	//        fSun_disk_Map.print(std::cout);
	fSun_disk_Map.write("Sun_disk_healpix_out.fits");

                  
	//Convert flux to counts, use the fluxToCounts method of BaseModel
	//  it's already supposed to be a counts map (sun track * exposure) 
	// but it's needed to make the energy bins....
	// hence input instead (sun track * exposure)/(total exposure) = exposure-weighted intensity
      
      
	//	 fSun_disk_Map *= 1.e-2; //  numerical problem when predicted counts too large! current disk model is just E^-g so need factor anyway
        
	 fSun_disk_Map = fluxToCounts(fSun_disk_Map); 


	//Convolve the map, using the convolve method of BaseModel
	fSun_disk_Map = convolve(fSun_disk_Map);

 

	fSun_disk_Map.write("Sun_disk_healpix_convolved_out.fits");

	// Sun IC counts  skymap
	 fparameters.getParameter("Sun_IC_ModelMapName", mapName); 
     

	 //	 mapName=dirName+"/" + mapName;  // use full pathname AWS20080905        
      	std::cout<<"mapName="<<mapName<<std::endl;                               

	//Read in a skymap from a fits file
	fSun_IC_Map.load(mapName);

	//	fSun_IC_Map *= 1.e-2; //  numerical problem when predicted counts too large!

	//Convert flux to counts, use the fluxToCounts method of BaseModel            
        
       	fSun_IC_Map = fluxToCounts(fSun_IC_Map); // test 
     

	//Convolve the map, using the convolve method of BaseModel
	fSun_IC_Map = convolve(fSun_IC_Map);

	fSun_IC_Map.write("Sun_IC_healpix_convolved_out.fits");//AWS20100217

}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////////////////////////////
void Sun::getMap(const Variables &variables, Skymap<double> &map){
	//Create the output map with the same size as the counts map, or in our case
	//we simply multiply the loadedMap with the scale factor and assign it to the output map
	//Here we could change the stored floadedMap, but that would make calculations of the gradient 
	//more expensive.  In this case, the multiplication should not be too cpu intensive.

 

	// Sun disk  map
 double diskScale, icScale;
  diskScale = variables["Sun_disk_MapScale"];
    icScale = variables["Sun_IC_MapScale"];


	for (int co = 0; co < map.Npix(); ++co){
		for (int i = 0; i < map.nSpectra(); ++i){
       //	     map[co][i] = fSun_disk_Map[co][i]*diskScale + diskScale*fSun_IC_Map[co][i]*icScale; //AWS20081127  error!
                     map[co][i] = fSun_disk_Map[co][i]*diskScale +           fSun_IC_Map[co][i]*icScale; //AWS20100217

		}
	}
      
	

	std::cout<<"Sun_disk_MapScale="<<variables["Sun_disk_MapScale"]<<" Sun_IC_MapScale="<<variables["Sun_IC_MapScale"]<<std::endl;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
BaseModel::gradMap Sun::getComponents(const Variables & variables, const std::string &prefix){
   gradMap output;
   
   
   output["Sun_disk"] =  fSun_disk_Map;
   output["Sun_disk"] *= variables["Sun_disk_MapScale"];
   output["Sun_IC"  ] =  fSun_IC_Map;
   output["Sun_IC"  ] *= variables["Sun_IC_MapScale"];

   return output;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
void Sun::getGrads(const Variables & variables, const std::string & varName, Skymap<double> &map){
	//If we are calculating	the gradient for the scaled map, then it is easy




	if (varName == "Sun_disk_MapScale") { 
		map += fSun_disk_Map;
	}


	if (varName == "Sun_IC_MapScale") { 
		map += fSun_IC_Map;
	}

	//	std::cout<<"error in Sun::getGrads:"<<varName<< "not found!"<<std::endl;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
BaseModel::gradMap Sun::getSecDer(const Variables & vars, const std::string & varName1, const std::string & varName2){
	gradMap output;
	//There is nothing to be done, since it is a linear model

	return output;
}
