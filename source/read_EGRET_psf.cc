

#include"Galplot.h"                    //AWS20050920
#include"galprop_classes.h"
#include"galprop.h"


 


int Galplot::read_EGRET_psf()          //AWS20050920
{
int status=0;


FITS work;

char  infile[100];

char **psf_files;
 
int il,ip;

double    pi=acos(-1.0);
double   dtr=pi/180.;

double theta,dtheta,domega;

  cout<<" >>>> read_EGRET_psf"<<endl;

  ///////////////////////////// counts
  psf_files = new char *[data.n_E_EGRET];
    for (int ip       =0;        ip<data.n_E_EGRET;       ip++)
      {
	psf_files[ip]=new char[50];
        strcpy(psf_files[ip],"COMPASS.IAQ.M00");
        if(ip==0)strcat(psf_files[ip],"15802");
        if(ip==1)strcat(psf_files[ip],"15803");
        if(ip==2)strcat(psf_files[ip],"15804");
        if(ip==3)strcat(psf_files[ip],"15805");
        if(ip==4)strcat(psf_files[ip],"15806");
        if(ip==5)strcat(psf_files[ip],"15807");
        if(ip==6)strcat(psf_files[ip],"15808");
        if(ip==7)strcat(psf_files[ip],"15809");
        if(ip==8)strcat(psf_files[ip],"15810");
        if(ip==9)strcat(psf_files[ip],"15811");
        cout<<psf_files[ip]<<endl;
      }



   

    // first first psf to get size
     ip=0;
     strcpy( infile,configure.fits_directory);
     strcat( infile,psf_files[ip]                       ); 

    cout<<"  reading  EGRET counts  from file "<<infile<<endl;

    work.read(infile);

    cout<<"work NAXES: "<<work.NAXES[0]<<" "<<endl;

    // data.EGRET_psf.init(work.NAXES[0],1,data.n_E_EGRET); // second index required since 1D distribution not available
    // 1D init not yet in Distribution so do explicitly, but should put it in Distribution


    data.EGRET_psf.n_spatial_dimensions=1;
    data.EGRET_psf.n_rgrid            =             work.NAXES[0]; 
    data.EGRET_psf.n_pgrid            =             data.n_E_EGRET;            
    data.EGRET_psf.d1                 =new Spectrum[work.NAXES[0]]; 
    data.EGRET_psf_dtheta             =             work.CDELT[0];

   for (    il       =0;        il<work.NAXES[0];       il++)
               data.EGRET_psf.d1[il].s=new float[data.n_E_EGRET];

// for (    ip       =0;        ip<data.n_E_EGRET;       ip++)//AWS20031021
   for (    ip       =0;        ip<10;       ip++)            //AWS20031021

   {
     strcpy( infile,configure.fits_directory);
     strcat( infile,psf_files[ip]                       ); 

    cout<<"  reading  EGRET counts  from file "<<infile<<endl;

    work.read(infile);

    cout<<"work NAXES: "<<work.NAXES[0]<<" "<<endl;
    
    if(galplotdef.verbose>0) work.print();

     for (int il       =0;        il<work.NAXES[0];       il++)
     {              
       // cout<< work(il,0)<<endl;
            data.EGRET_psf.d1[il].s[ip]=work(il,0);            
     }//il

 
   }//ip

  if(galplotdef.verbose>0)
  {
   for (    ip       =0;        ip<data.EGRET_psf.n_pgrid     ;       ip++)
   {
     cout<<endl<<"   EGRET PSF as IAQ for "<<data.E_EGRET[ip]<<"-"<<data.E_EGRET[ip+1]<<" MeV"<<endl;
     for (  il       =0;        il<data.EGRET_psf.n_rgrid     ;       il++)cout<<data.EGRET_psf.d1[il].s[ip]<<" ";
   }//ip

   data.EGRET_psf.print(); // not defined for 1D so no output
  }//if

//      Method from ~aws/sky/skymos/c/psi/v11: 
//      convert from probability per phigeo bin to
//      probability per steradian
//        solid angle =  2pi integral[sin(theta)dtheta]=2pi delta(cos(theta))       
//        solid angle of phigeo bin in steradian -- use of addition
//        theorem: cos(x)-cos(y) = 2*sin(0.5*(x+y))*sin(0.5*(y-x))
//        (higher numerical precision)
   //     solid angle = 4pi sin (theta) sin (0.5*dtheta)
   dtheta=data.EGRET_psf_dtheta;

   for (   ip       =0;        ip<data.EGRET_psf.n_pgrid ; ip++)
   {
    for (  il       =0;        il<data.EGRET_psf.n_rgrid ; il++)
    {

          theta = (il+0.5)* dtheta;//bin centre
          domega = 4.0 * pi * sin(theta*dtr) * sin(0.5*dtheta*dtr);
          data.EGRET_psf.d1[il].s[ip]/=domega;

    }//il
   }//ip


if(galplotdef.verbose>0)
  {
   for (    ip       =0;        ip<data.EGRET_psf.n_pgrid     ;       ip++)
   {
     cout<<endl<<"   EGRET PSF per steradian for "<<data.E_EGRET[ip]<<"-"<<data.E_EGRET[ip+1]<<" MeV"<<endl;
     for (  il       =0;        il<data.EGRET_psf.n_rgrid     ;       il++)cout<<data.EGRET_psf.d1[il].s[ip]<<" ";
   }//ip

 }//if

  cout<<endl;
  cout<<" <<<< read_EGRET_psf"<<endl;

  
return status;
}
