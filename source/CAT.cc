using namespace std;
// for slalib version which uses: if defined (__cplusplus) extern "C"
// but this does not work, use version without this 
// #define __cplusplus

#include<iostream>
#include"fitsio.h"
#include"slalib.h"

#include"CAT.h"



//////////////////////////////////////////////////////
  
  
///////////////////////////////////////////////////////
  int CAT:: read (char *directory)
{
  char infile[500];

  char err_text[100];
  char comment[100];

  fitsfile *fptr;
  int status;
  int hdunum,hdutype,total_hdus,colnum;
  int n_columns,n_rows;
  double nulval=0.;
  int anynul   =0;
  char nulstr[100];strcpy(nulstr,"null");
  double dtr=acos(-1.0)/180.;
  int i,j,k;


  //char catalogue_file[]="gnrl_refr_cat_0020_reformatted.fits";
  //char catalogue_file[]="isgriCat_2nd_Tony.fits";
    char catalogue_file[]="isgri_2nd_cat_revised.fits";
 



  cout<< "CAT::read  (" << directory<<")" <<endl;

  
  strcpy(infile,directory);
  strcat(infile,catalogue_file);

  
  cout<<"   infile "<<infile<<endl;

  status=0; // needed
  fits_open_file(&fptr,infile,READONLY,&status) ;
  cout<<"FITS read open status= "<<status<<" "<<infile<<endl;

  fits_get_errstatus(status,err_text);
  cout<<err_text<<endl;

fits_get_num_hdus(fptr,&total_hdus,&status);
  cout<<"total number of header units="<<total_hdus<<endl;

  hdunum=1;

  fits_movabs_hdu(fptr,hdunum+1,&hdutype,&status );
  cout<<"FITS movabs_hdu hdunum="<<hdunum
      <<" status= "<<status<<" hdutype="<<hdutype<<endl  ;

   fits_read_key(fptr,TLONG,"TFIELDS",&n_columns,comment,&status);
   cout<<" n_columns ="<<n_columns ;
   fits_read_key(fptr,TLONG,"NAXIS2",&n_rows  ,comment,&status);
   cout<<" n_rows    ="<<n_rows    <<endl;


   n_sources=n_rows;

   ra      =new double[n_rows];
   dec     =new double[n_rows];
   flux    =new double[n_rows];//AWS20060110
   flux_err=new double[n_rows];//AWS20060110

  fits_get_colnum(fptr,CASEINSEN,"RA_OBJ",&colnum,&status);
  cout<<"RA_OBJ column ="<< colnum<<endl;
  fits_read_col(fptr, TDOUBLE, colnum, 1,1,n_rows,&nulval,ra, &anynul,&status);

  fits_get_colnum(fptr,CASEINSEN,"DEC_OBJ",&colnum,&status);
  cout<<"DEC_OBJ column ="<< colnum<<endl;
  fits_read_col(fptr, TDOUBLE, colnum, 1,1,n_rows,&nulval,dec, &anynul,&status);


  // cout<<"ra :";for (i=0;i<n_rows;i++)cout<<" "<<ra [i];cout<<endl;
  //cout<<"dec:";for (i=0;i<n_rows;i++)cout<<" "<<dec[i];cout<<endl;


 fits_get_colnum(fptr,CASEINSEN,"NAME",&colnum,&status);
 cout<<"NAME column ="<< colnum<<endl;
 name=new char*[n_rows];
 for (i=0;i<n_rows;i++)     name[i]=new char[100];
 fits_read_col_str(fptr,  colnum, 1,1, n_rows,nulstr,  name, &anynul,&status);

 // for (i=0;i<n_rows;i++)  cout<<   name[i] <<" ra="<<ra[i]<<" dec="<<dec[i]<<endl;

  longitude =new double[n_rows];
  latitude  =new double[n_rows];

 for (i=0;i<n_rows;i++) 
   {
     // NB had to compile sla_c since undefined symbol using lib from osa4.2

  slaEqgal(ra[i]*dtr, dec[i]*dtr,  &longitude[i],  &latitude[i]);
  longitude[i]/=dtr;
   latitude[i]/=dtr;
   }



  return 0;
}
///////////////////////////////////////////////////////
  int CAT:: read (char *directory,char *filename) //AWS20060110
{
  char infile[500];

  char err_text[100];
  char comment[100];

  fitsfile *fptr;
  int status;
  int hdunum,hdutype,total_hdus,colnum;
  int n_columns,n_rows;
  double nulval=0.;
  int anynul   =0;
  char nulstr[100];strcpy(nulstr,"null");
  double dtr=acos(-1.0)/180.;
  int i,j,k;


  


  cout<< "CAT::read  (" << directory<<"/"<<filename<<")" <<endl;

  
  strcpy(infile,directory);
  strcat(infile,filename);

  
  cout<<"CAT::read   infile= "<<infile<<endl;

  status=0; // needed
  fits_open_file(&fptr,infile,READONLY,&status) ;
  cout<<"FITS read open status= "<<status<<" "<<infile<<endl;

  fits_get_errstatus(status,err_text);
  cout<<err_text<<endl;

  fits_get_num_hdus(fptr,&total_hdus,&status);
  cout<<"total number of header units="<<total_hdus<<endl;

  hdunum=total_hdus-1; // because spimodfit output has also grouping extension, make it thus more general assuming catalogue is last extension

  fits_movabs_hdu(fptr,hdunum+1,&hdutype,&status );
  cout<<"FITS movabs_hdu hdunum="<<hdunum
      <<" status= "<<status<<" hdutype="<<hdutype<<endl  ;

   fits_read_key(fptr,TLONG,"TFIELDS",&n_columns,comment,&status);
   cout<<" n_columns ="<<n_columns ;
   fits_read_key(fptr,TLONG,"NAXIS2",&n_rows  ,comment,&status);
   cout<<" n_rows    ="<<n_rows    <<endl;


   n_sources=n_rows;

  ra      =new double[n_rows];
  dec     =new double[n_rows];
  flux    =new double[n_rows];
  flux_err=new double[n_rows];

  fits_get_colnum(fptr,CASEINSEN,"RA_OBJ",&colnum,&status);
  cout<<"RA_OBJ column ="<< colnum<<endl;
  fits_read_col(fptr, TDOUBLE, colnum, 1,1,n_rows,&nulval,ra, &anynul,&status);

  fits_get_colnum(fptr,CASEINSEN,"DEC_OBJ",&colnum,&status);
  cout<<"DEC_OBJ column ="<< colnum<<endl;
  fits_read_col(fptr, TDOUBLE, colnum, 1,1,n_rows,&nulval,dec, &anynul,&status);

  fits_get_colnum(fptr,CASEINSEN,"FLUX  ",&colnum,&status);
  cout<<"FLUX    column ="<< colnum<<endl;
  // read only first value of spectrum vector
  for (i=0;i<n_rows;i++)
  fits_read_col(fptr, TDOUBLE, colnum,i+ 1,1,1,&nulval,&flux[i],   &anynul,&status);

  fits_get_colnum(fptr,CASEINSEN,"FLUX_ERR",&colnum,&status);
  cout<<"FLUX_ERR column ="<< colnum<<endl;
  // read only first value of spectrum vector
  for (i=0;i<n_rows;i++)
  fits_read_col(fptr, TDOUBLE, colnum,i+ 1,1,1,&nulval,&flux_err[i],&anynul,&status);

  // cout<<"ra :";for (i=0;i<n_rows;i++)cout<<" "<<ra [i];cout<<endl;
  //cout<<"dec:";for (i=0;i<n_rows;i++)cout<<" "<<dec[i];cout<<endl;
  //cout<<"flux:    ";for (i=0;i<n_rows;i++)cout<<" "<<flux[i]    ;cout<<endl;
  //cout<<"flux_err:";for (i=0;i<n_rows;i++)cout<<" "<<flux_err[i];cout<<endl;

 fits_get_colnum(fptr,CASEINSEN,"NAME",&colnum,&status);
 cout<<"NAME column ="<< colnum<<endl;
 name=new char*[n_rows];
 for (i=0;i<n_rows;i++)     name[i]=new char[100];
 fits_read_col_str(fptr,  colnum, 1,1, n_rows,nulstr,  name, &anynul,&status);

 // for (i=0;i<n_rows;i++)  cout<<   name[i] <<" ra="<<ra[i]<<" dec="<<dec[i]<<endl;

  longitude =new double[n_rows];
  latitude  =new double[n_rows];

 for (i=0;i<n_rows;i++) 
   {
     // NB had to compile sla_c since undefined symbol using lib from osa4.2

  slaEqgal(ra[i]*dtr, dec[i]*dtr,  &longitude[i],  &latitude[i]);
  longitude[i]/=dtr;
   latitude[i]/=dtr;
   }



  return 0;
}

////////////////////////////////////////////////////////////////
void CAT:: print ()
{

  int i;
  cout<<"CAT::print"<<endl;

  for (i=0;i<n_sources;i++)  cout<<"source #"<<i<<"   "<<   name[i]
                                 <<"    ra = "<<ra[i]<<" dec = "<<dec[i]
				 <<"     l = "<<longitude[i]<<"  b = "<<latitude[i]
				 <<"  flux = "<<flux     [i]<<" +-   "<<flux_err[i]
                                 <<endl;

  cout<<"end CAT::print"<<endl;
}
////////////////////////////////////////////////////////////////

void CAT:: find (char *name_, double *ra_, double *dec_, double *longitude_, double *latitude_)
{

  int i;
  int found;

  cout<<"CAT::find"<<endl;

  found=0;

  for (i=0;i<n_sources;i++) 
  {
    /*
     cout<<"source #"<<i<<"   "<<   name[i]
                                     <<"    ra = "<<ra[i]<<" dec = "<<dec[i]
	       			 <<"     l = "<<longitude[i]<<"  b = "<<latitude[i]
                                 <<endl;
    */
 if(strcmp(name[i],name_)==0) found=1;
 if(strcmp(name[i],name_)==0) break;

    }

  if(found==0)
  {
   *ra_              =-999.;
   *dec_             =-999.;
   *longitude_       =-999.;
   *latitude_        =-999.;

   cout<<"source "<<name_<< "   not found in catalogue !"<<endl;
  }

  if(found==1)
  {
   cout<<"found source #"<<i<<"   "<<   name[i]
                                 <<"    ra = "<<ra[i]<<" dec = "<<dec[i]
				 <<"     l = "<<longitude[i]<<"  b = "<<latitude[i]
                                 <<endl;

   *ra_              =ra[i];
   *dec_            =dec[i];
   *longitude_=longitude[i];
   *latitude_ = latitude[i];

   }

  cout<<"end CAT::find"<<endl;
}

