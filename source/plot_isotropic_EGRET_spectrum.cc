
 
#include"Galplot.h"                    //AWS20050920
#include"galprop_classes.h"
#include"galprop.h"
#include"constants.h"
#include"fitsio.h"


//static void   differential_spectrum(double e1,double e2, double intensity, double g,double &intensity_esq1,double &intensity_esq2,double &Ebar, double &Ibar);
 

int Galplot::plot_isotropic_EGRET_spectrum()
{

  

   cout<<" >>>> plot_isotropic_EGRET_spectrum    "<<endl;
   int status=0;


 TGraph *spectrum;
 double A=galplotdef.isotropic_const;
 double g=galplotdef.isotropic_g;
 double e1,e2;


   
   spectrum=new TGraph(2);
   e1=   30.;
   e2=10000.;
   spectrum->SetPoint(0,e1,A*pow(e1,2.0-g));
   spectrum->SetPoint(1,e2,A*pow(e2,2.0-g));

  spectrum->SetMarkerColor(kMagenta);
  spectrum->SetMarkerStyle(21); 
  spectrum->SetMarkerStyle(22); // triangles
  spectrum->SetMarkerSize (0.5);// 
  spectrum->SetLineColor(kMagenta);
  spectrum->SetLineWidth(5     );
  spectrum->SetLineStyle(1      );//1=solid 2=dash 3=dash-dot 4=dot-dot
  spectrum->SetMarkerStyle(22); // triangles
  spectrum->SetMarkerSize (0.5);// 
  spectrum->Draw();
  spectrum->Draw(" P ");  // points as markers  
  spectrum->Draw();




  // energy ranges


int i_E_EGRET;
double  *intensity_esq1,*intensity_esq2;

double *Ebar,*Ibar;
double segment[2],Ebar_segment[2];
int errorbar; double errorbarfactor;

 
 
  intensity_esq1    = new double[data.n_E_EGRET];
  intensity_esq2    = new double[data.n_E_EGRET];

 


  Ebar=new double[data.n_E_EGRET];
  Ibar=new double[data.n_E_EGRET];

         for(i_E_EGRET=0; i_E_EGRET<data.n_E_EGRET                    ; i_E_EGRET++)
        {
  
          
          differential_spectrum(data.E_EGRET[i_E_EGRET],data.E_EGRET[i_E_EGRET+1] ,
                                galplotdef.isotropic_EGRET[i_E_EGRET], g,
                                intensity_esq1[i_E_EGRET],intensity_esq2[i_E_EGRET],Ebar[i_E_EGRET] ,Ibar[i_E_EGRET] );

         
          cout<<"plot_isotropic_EGRET_spectrum: e1,e2="<<data.E_EGRET[i_E_EGRET]<<" "<<data.E_EGRET[i_E_EGRET+1]
              <<" intensity="<<galplotdef.isotropic_EGRET[i_E_EGRET]<<" intensity_esq1, 2 ="<<intensity_esq1[i_E_EGRET]<<" "<<intensity_esq2[i_E_EGRET]<<endl;


        }//i_E_EGRET




for(i_E_EGRET=0; i_E_EGRET<data.n_E_EGRET                    ; i_E_EGRET++)
{

 if(galplotdef.spectrum_style_EGRET==1 ||  galplotdef.spectrum_style_EGRET==3)
 {
  for (errorbar=1;errorbar<=2; errorbar++)
    {

         
          if(errorbar==1)errorbarfactor=     1.0+galplotdef.error_bar_EGRET; 
          if(errorbar==2)errorbarfactor= 1./(1.0+galplotdef.error_bar_EGRET); 

          segment[0]=intensity_esq1[i_E_EGRET]*errorbarfactor;
          segment[1]=intensity_esq2[i_E_EGRET]*errorbarfactor;

          

          spectrum=new TGraph(2,&data.E_EGRET[i_E_EGRET],segment);

          spectrum->SetMarkerColor(kMagenta  );
          spectrum->SetMarkerStyle(21); 
          spectrum->SetMarkerStyle(22); // triangles
          spectrum->SetMarkerSize (0.5);// 
          spectrum->SetLineColor(kMagenta );
          spectrum->SetLineWidth(3     );
          spectrum->SetLineStyle(1      );
          spectrum->SetMarkerStyle(22); // triangles
          spectrum->SetMarkerSize (0.5);// 
          spectrum->Draw();       // don't use "same" for TGraph
          spectrum->Draw(" P ");  // points as markers  

          spectrum->Draw();
     }//errorbar
    }//if


if(galplotdef.spectrum_style_EGRET==2 ||  galplotdef.spectrum_style_EGRET==3)
 {
         segment[0]=Ibar[i_E_EGRET]*(1.0+galplotdef.error_bar_EGRET);
         segment[1]=Ibar[i_E_EGRET]/(1.0+galplotdef.error_bar_EGRET);
    Ebar_segment[0]=Ebar[i_E_EGRET];
    Ebar_segment[1]=Ebar[i_E_EGRET];
    spectrum=new TGraph(2,Ebar_segment,segment);

          spectrum->SetMarkerColor(kMagenta  );
          spectrum->SetMarkerStyle(21); 
          spectrum->SetMarkerStyle(22); // triangles
          spectrum->SetMarkerSize (0.5);// 
          spectrum->SetLineColor(kMagenta );
          spectrum->SetLineWidth(3     );
          spectrum->SetLineStyle(1      );
          spectrum->SetMarkerStyle(22); // triangles
          spectrum->SetMarkerSize (0.5);// 
          spectrum->Draw();       // don't use "same" for TGraph
          spectrum->Draw(" P ");  // points as markers  

          spectrum->Draw();

  }//if

 }//i_EGRET




// power-law fit

 cout<<" fitting power law to isotropic EGRET spectrum:all points"<<endl;
 TGraphErrors *spectrum_to_fit;
 spectrum_to_fit=new TGraphErrors(data.n_E_EGRET );


 //using log-log plot
 cout<<" ========================================        log-log plot                         "<<endl;
 for(i_E_EGRET=0; i_E_EGRET<data.n_E_EGRET ; i_E_EGRET++)
 {
  spectrum_to_fit->SetPoint     (i_E_EGRET, log10(Ebar[i_E_EGRET]),log10(Ibar[i_E_EGRET]));
  spectrum_to_fit->SetPointError(i_E_EGRET, 0.0,     log10(1.0+galplotdef.error_bar_EGRET));
 }

 TF1 *fitfunction=new TF1("f1","[0] + [1]*x"); // scaling and      background free, parameters 0, 1
 // spectrum_to_fit->Fit("pol1","V");
 spectrum_to_fit->Fit(fitfunction,"V");
 cout<<"chi^2/N="<<fitfunction->GetChisquare()<<endl;


 // explicit power-law
 cout<<" ==========================================        explicit power law                   "<<endl;
 spectrum_to_fit=new TGraphErrors(data.n_E_EGRET );

 for(i_E_EGRET=0; i_E_EGRET<data.n_E_EGRET ; i_E_EGRET++)
 {
  spectrum_to_fit->SetPoint     (i_E_EGRET,       Ebar[i_E_EGRET] ,      Ibar[i_E_EGRET] );
  spectrum_to_fit->SetPointError(i_E_EGRET, 0.0,  Ibar[i_E_EGRET]*galplotdef.error_bar_EGRET );
 }

      fitfunction=new TF1("f2","[0] * pow(x,[1])"); // [0]*10^[1]
 
 spectrum_to_fit->Fit(fitfunction,"V");
 cout<<"chi^2/N="<<fitfunction->GetChisquare()<<endl;


 cout<<" fitting power law to isotropic EGRET spectrum:2 end points removed"<<endl;
 // explicit power-law
 cout<<" ==========================================        explicit power law                   "<<endl;
 spectrum_to_fit=new TGraphErrors(data.n_E_EGRET-2 );

 for(i_E_EGRET=1; i_E_EGRET<data.n_E_EGRET-1 ; i_E_EGRET++)
 {
  spectrum_to_fit->SetPoint     (i_E_EGRET-1,       Ebar[i_E_EGRET] ,      Ibar[i_E_EGRET] );
  spectrum_to_fit->SetPointError(i_E_EGRET-1, 0.0,  Ibar[i_E_EGRET]*galplotdef.error_bar_EGRET );
 }

      fitfunction=new TF1("f2","[0] * pow(x,[1])"); // [0]*10^[1]
 
 spectrum_to_fit->Fit(fitfunction,"V");
 cout<<"chi^2/N="<<fitfunction->GetChisquare()<<endl;




   cout<<" <<<< plot_isotropic_EGRET_spectrum   "<<endl;
   return status;
}

 
//------------------------------------------------------------------------------------------

//static
 void Galplot::differential_spectrum
(double e1,double e2,double intensity, double g,double &intensity_esq1,double &intensity_esq2,double &Ebar, double &Ibar)
{
  // converts integral intensity between e1 and e2 to two differential values at e1, e2
  // for a given spectral index g
  // result in form E^2 I(E)
  double A;
  
  A=(-g+1.)/(pow(e2,-g+1.)-pow(e1,-g+1)) * intensity;

  intensity_esq1=A*pow(e1,-g+2);
  intensity_esq2=A*pow(e2,-g+2);


  Ebar  =pow(e1*e2,0.5);
  Ibar=A*pow(Ebar,-g+2.0);

 cout<<"A="<<A<<endl;
} 
