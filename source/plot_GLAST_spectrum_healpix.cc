#include"Galplot.h"                    
#include"galprop_classes.h"
#include"galprop.h"
#include"constants.h"
#include"fitsio.h"

#include"Fermi_systematic_errors.h"    //AWS20090408

#include<vector>
#include<valarray>

int Galplot::plot_GLAST_spectrum_healpix(int mode)
{
   cout<<" >>>> plot_GLAST_spectrum_healpix    "<<endl;
   int status=0;

   int i,j, ipix, i_E_GLAST;
   pointing point;
   double l,b;

   double pi= acos(-1.0);
   double rtd=180./pi;        // rad to deg
   double dtr=pi / 180.;      // deg to rad

  int intensity_method=1; //   1=sum(counts/exposure)(preferred)  2=(sum counts)/(sum exposure) 

  cout<<"GLAST counts energy bounds:"<<endl;
  for( i=0;i<  data.GLAST_counts_healpix .getEMin().size();i++)   
    cout<< data.GLAST_counts_healpix.getEMin()[i]<<" -  " << data.GLAST_counts_healpix.getEMax()[i] <<endl; // returns a valarray<double>

//Skymap<long> counts=data.GLAST_counts_healpix.getCountsMap();   AWS20091210
  Skymap<int>  counts=data.GLAST_counts_healpix.getCountsMap(); //AWS20091210

  // if(galplotdef.verbose==-1801)  { cout<<"GLAST counts:"<<endl;  counts.print(cout);} AWS20091204

 

  cout<<"GLAST exposure energies :"<<endl;
  
  for( i=0;i< data.GLAST_exposure_healpix.getEnergies().size();i++)    cout<<data.GLAST_exposure_healpix.getEnergies()[i]<<endl;

  Skymap<double> exposure=   data.GLAST_exposure_healpix .getExposureMap();
  //  if(galplotdef.verbose==-1801) {  cout<<"GLAST exposure:"<<endl;  exposure.print(cout);} AWS20091204


  cout<<"GLAST counts    healpix #pixels = "<<  counts.Npix()<<endl;
  cout<<"GLAST exposure  healpix #pixels = "<<exposure.Npix()<<endl;

  cout<<"GLAST counts    healpix solid angle (sr) = "<< 4.*pi/ counts  .Npix()<<endl;
  cout<<"GLAST exposure  healpix solid angle (sr) = "<< 4.*pi/ exposure.Npix()<<endl;


  cout<<"GLAST counts    Skymap solid angle (sr)  = "<<counts  .solidAngle()<<endl; //NB erroneously stated deg^2 in Skymap.h
  cout<<"GLAST exposure  Skymap solid angle (sr)  = "<<exposure.solidAngle()<<endl; //NB erroneously stated deg^2 in Skymap.h

  if(galplotdef.verbose==-1801)
  for (ipix=0;ipix<counts.Npix();ipix++) // Npix: total pixels: healpix_base.h
  {

    cout<<"theta,phi= "<<counts.pix2ang(ipix)<<endl; // (theta, phi)  in radian

    //  cout<<counts.coord[ipix]<<endl; not sure how this works
    l =        counts.pix2ang(ipix).phi   * rtd;
    b = 90.0 - counts.pix2ang(ipix).theta * rtd;
    cout<<"l = "<<l<<" b ="<<b<<endl; // l,b in deg

   for( i_E_GLAST=0;i_E_GLAST< data.GLAST_exposure_healpix.getEnergies().size();i_E_GLAST++)
     cout<<counts  [ipix][i_E_GLAST]<<" ";   cout<<endl;

   for( i_E_GLAST=0;i_E_GLAST< data.GLAST_counts_healpix.  getEMin()    .size();i_E_GLAST++)
     cout<<exposure[ipix][i_E_GLAST]<<" ";   cout<<endl;

   
}

  //  integrate exposure over energy
  /*-----------
  // integrated exposure has same size as counts
  Skymap<double>exposure_integrated (counts.Order() ,  data.GLAST_counts_healpix.  getEMin()    .size()  ) ;  // Order() in healpix_base.h
  cout  <<"exposure_integrated.Npix="<<counts.Npix()   <<"   exposure_integrated.Npix="<<exposure_integrated.Npix()<<endl; // check for equality

  

   for (ipix=0;ipix<exposure_integrated.Npix() ;ipix++)
   {
    SM::Coordinate coordinate=counts.pix2coord( ipix);
//    if(galplotdef.verbose==-1902)      {cout<<"  pixel coordinate from pix2coord:";coordinate.print(cout);cout<<endl;} AWS20091204

    for( i_E_GLAST=0;i_E_GLAST< data.GLAST_counts_healpix.getEMin().size();i_E_GLAST++)
    {
     double index=-2.1;// 2.0 gave nan !!
     double exposure_integrated_;
 
     exposure_integrated_=
      data.GLAST_exposure_healpix .getWeightedExposure
        (coordinate,  data.GLAST_counts_healpix.getEMin()[i_E_GLAST], data.GLAST_counts_healpix.getEMax()[i_E_GLAST],  index) ;

     exposure_integrated[ipix][i_E_GLAST]=exposure_integrated_;

     if(galplotdef.verbose==-1902) 
     cout<<"plot_GLAST_profile_healpix: Emin="<<  data.GLAST_counts_healpix.getEMin()[i_E_GLAST]<<" Emax="<<  data.GLAST_counts_healpix.getEMax()[i_E_GLAST]
         <<" exposure_integrated_="<<exposure_integrated_<<endl;


    }
   }

 //  if(galplotdef.verbose==-1903) {   cout<<"exposure_integrated:"<<endl; exposure_integrated.print(cout);} //AWS20091204
     -------------*/


  
if (data.GLAST_exposure_integrated_init==0) // only compute once, for all energies
      
{
  cout<<"    integrating GLAST exposure for all data energy bands"<<endl;
  data.GLAST_exposure_integrated_init=1;

  // integrated exposure has same size as counts
  //  Skymap<double>exposure_integrated (counts.Order() ,  data.GLAST_counts_healpix.  getEMin()    .size()  ) ;  // Order() in healpix_base.h
  //  cout  <<"counts.Npix="<<counts.Npix()   <<"   exposure_integrated.Npix="<<exposure_integrated.Npix()<<endl; // check for equality

  data.GLAST_exposure_integrated =Skymap<double>(counts.Order() ,  data.GLAST_counts_healpix.  getEMin()    .size()  ) ;  // Order() in healpix_base.h
  cout  <<"count.Npix="<<counts.Npix()   <<"   data.GLAST_exposure_integrated.Npix="<<data.GLAST_exposure_integrated.Npix()<<endl; // check for equality

   for (ipix=0;ipix<data.GLAST_exposure_integrated.Npix() ;ipix++)
   {
    SM::Coordinate coordinate=counts.pix2coord( ipix);
    //    if(galplotdef.verbose==-1902)      {cout<<"  pixel coordinate from pix2coord:";coordinate.print(cout);cout<<endl;} AWS20091204

    for( i_E_GLAST=0;i_E_GLAST< data.GLAST_counts_healpix.getEMin().size();i_E_GLAST++)
    {
     double index=-2.1;// 2.0 gave nan !!
     double exposure_integrated_;
 
     exposure_integrated_=
      data.GLAST_exposure_healpix .getWeightedExposure
        (coordinate,  data.GLAST_counts_healpix.getEMin()[i_E_GLAST], data.GLAST_counts_healpix.getEMax()[i_E_GLAST],  index) ;

     //              exposure_integrated[ipix][i_E_GLAST]=exposure_integrated_;
     data.GLAST_exposure_integrated[ipix][i_E_GLAST]=exposure_integrated_;

     if(galplotdef.verbose==-1902) 
     cout<<"plot_GLAST_profile_healpix: Emin="<<  data.GLAST_counts_healpix.getEMin()[i_E_GLAST]<<" Emax="<<  data.GLAST_counts_healpix.getEMax()[i_E_GLAST]
         <<" exposure_integrated_="<<exposure_integrated_<<endl;


    }
   }

   //  if(galplotdef.verbose==-1903) {   cout<<"data.GLAST_exposure_integrated:"<<endl; data.GLAST_exposure_integrated.print(cout);} AWS20091204
    } // if




  //---------------------------------------------------------------------------------------------
 
  // do it this way since copy of counts will give long int, but want double
  Skymap<double>flux (counts.Order() ,  data.GLAST_counts_healpix.  getEMin()    .size()  ) ;  // Order() in healpix_base.h
  cout  <<"counts.Npix="<<counts.Npix()   <<"   flux.Npix="<<flux.Npix()<<endl; // check for equality


  // flux skymap 
   for (ipix=0;ipix<flux.Npix() ;ipix++)
        for( i_E_GLAST=0;i_E_GLAST< data.GLAST_counts_healpix.getEMin().size();i_E_GLAST++)
     {
		  flux[ipix][i_E_GLAST] = counts[ipix][i_E_GLAST] / data.GLAST_exposure_integrated[ipix][i_E_GLAST];
     } //i_E_GLAST

   flux/=counts.solidAngle();

   //   cout<<"flux=";flux.print(cout);

   //---------------------------------------------------------------------------------------------

  // accumulate pixels in chosen area

   int ilong,nlong;
  
   
   // data arrays
   valarray<double>            counts_total( data.GLAST_counts_healpix.getEMin().size() );  
   valarray<double>          exposure_total( data.GLAST_counts_healpix.getEMin().size() );
   valarray<double>         intensity_total( data.GLAST_counts_healpix.getEMin().size() );
   valarray<double>   intensity_error_total( data.GLAST_counts_healpix.getEMin().size() );
   valarray<double>   intensity_sys_error  ( data.GLAST_counts_healpix.getEMin().size() );    //AWS20090408
   valarray<double>   intensity_combined_error( data.GLAST_counts_healpix.getEMin().size() ); //AWS20090408
   valarray<double>        number_of_pixels( data.GLAST_counts_healpix.getEMin().size() );

   // model arrays for spatial (de)convolution correction
   valarray<double>      model_unconv_counts_total( data.GLAST_counts_healpix.getEMin().size() ); // model predicted unconvolved counts //AWS20080717
   valarray<double>      model___conv_counts_total( data.GLAST_counts_healpix.getEMin().size() ); // model predicted   convolved counts //AWS20080717
   valarray<double>      model_unconv_over_conv   ( data.GLAST_counts_healpix.getEMin().size() ); // model predicted unconv/conv counts //AWS20080717

   counts_total          = 0.;
   exposure_total        = 0.;
   intensity_total       = 0.;
   intensity_error_total = 0.;
   number_of_pixels      = 0.;

   model_unconv_counts_total = 0.;
   model___conv_counts_total = 0.;

   for (ipix=0;ipix<flux.Npix() ;ipix++)
   {
    l =        counts.pix2ang(ipix).phi   * rtd;
    b = 90.0 - counts.pix2ang(ipix).theta * rtd;
    //  cout<<"ipix="<<ipix<<" l = "<<l<<" b ="<<b<<endl; // l,b in deg

    int select=0;
    // select if pixel lies in one of the 4 regions
    if(l>=galplotdef.long_min1 && l<=galplotdef.long_max1 && b>=galplotdef.lat_min1 && b<=galplotdef.lat_max1 ) select=1;
    if(l>=galplotdef.long_min2 && l<=galplotdef.long_max2 && b>=galplotdef.lat_min1 && b<=galplotdef.lat_max1 ) select=1;
    if(l>=galplotdef.long_min1 && l<=galplotdef.long_max1 && b>=galplotdef.lat_min2 && b<=galplotdef.lat_max2 ) select=1;
    if(l>=galplotdef.long_min2 && l<=galplotdef.long_max2 && b>=galplotdef.lat_min2 && b<=galplotdef.lat_max2 ) select=1;

    if (select==1)
    {    
     for( i_E_GLAST=0;i_E_GLAST< data.GLAST_counts_healpix.getEMin().size();i_E_GLAST++)
     {
           counts_total[i_E_GLAST] +=   counts[ipix][i_E_GLAST];
         exposure_total[i_E_GLAST] +=                             data.GLAST_exposure_integrated[ipix][i_E_GLAST]*counts.solidAngle(); 
	intensity_total[i_E_GLAST] +=   counts[ipix][i_E_GLAST]/( data.GLAST_exposure_integrated[ipix][i_E_GLAST]*counts.solidAngle() );
       number_of_pixels[i_E_GLAST]++;

       model_unconv_counts_total[i_E_GLAST] += unconvolved.GLAST_unconvolved_counts[ipix][i_E_GLAST];// AWS20080718
                                          

       model___conv_counts_total[i_E_GLAST] +=   convolved.  GLAST_convolved_counts[ipix][i_E_GLAST];// AWS20080717

     } //i_E_GLAST
    } // if select

   } // ipix



   
   for( i_E_GLAST=0;i_E_GLAST< data.GLAST_counts_healpix.getEMin().size();i_E_GLAST++)
   {

    if(intensity_method==1)
      {
      //   average intensity over bin, error estimate from total counts
             intensity_total[i_E_GLAST]/= number_of_pixels[i_E_GLAST];
       intensity_error_total[i_E_GLAST]=  intensity_total[i_E_GLAST]/ sqrt(counts_total[i_E_GLAST]);
      }

      if(intensity_method==2)
      {
     //  method ignoring variations of intensity and exposure
             intensity_total         [i_E_GLAST]=      counts_total[i_E_GLAST] /exposure_total[i_E_GLAST] ;
       intensity_error_total         [i_E_GLAST]= sqrt(counts_total[i_E_GLAST])/exposure_total[i_E_GLAST] ;
      }


    }


   // systematic errors
   for( i_E_GLAST=0;i_E_GLAST< data.GLAST_counts_healpix.getEMin().size();i_E_GLAST++)
   {
    
     double Emean=(data.GLAST_counts_healpix.getEMin()[i_E_GLAST]+ data.GLAST_counts_healpix.getEMax()[ i_E_GLAST])/2.0;
     int mode=1;
     double sysfracerror= Fermi_systematic_errors( Emean,   mode);
     intensity_sys_error[i_E_GLAST]=intensity_total[i_E_GLAST] * sysfracerror;

     cout<<"plot_GLAST_spectrum_healpix: Emean="<<Emean<<" sysfracerror="<<sysfracerror<< " intensity_sys_error="<< intensity_sys_error[i_E_GLAST]
         <<" intensity stat error="<< intensity_error_total[i_E_GLAST] ;

     // combine systematic and statistical errors in quadrature
     intensity_combined_error[i_E_GLAST] = sqrt(  pow(    intensity_error_total[i_E_GLAST],2.0) + pow(intensity_sys_error[i_E_GLAST],2.0));

     cout<<" intensity combined error="<< intensity_combined_error[i_E_GLAST]<<endl ;
   }


      model_unconv_over_conv = 0.0;

      for( i_E_GLAST=0;i_E_GLAST< data.GLAST_counts_healpix.getEMin().size();i_E_GLAST++)
	if ( model___conv_counts_total[i_E_GLAST] >0.0   )
	 model_unconv_over_conv[i_E_GLAST] =  model_unconv_counts_total[i_E_GLAST] /  model___conv_counts_total[i_E_GLAST] ;
      
      // convolution correction if required
      if(galplotdef.convolve_GLAST==0)cout<<" NOT applying correction for spatial convolution"<<endl;
      if(galplotdef.convolve_GLAST==1)cout<<"     applying correction for spatial convolution"<<endl;

      if(galplotdef.convolve_GLAST==1)
      for( i_E_GLAST=0;i_E_GLAST< data.GLAST_counts_healpix.getEMin().size();i_E_GLAST++)
      {
       intensity_total         [i_E_GLAST]  *=  model_unconv_over_conv[i_E_GLAST];
       intensity_error_total   [i_E_GLAST]  *=  model_unconv_over_conv[i_E_GLAST];
      }
  
       cout<<"counts = " ;
       for( i_E_GLAST=0;i_E_GLAST< data.GLAST_counts_healpix.getEMin().size();i_E_GLAST++)
	 cout<<  counts_total[i_E_GLAST]<< " "      ;
       cout<<endl;

       cout<<"  intensity: ";
       for( i_E_GLAST=0;i_E_GLAST< data.GLAST_counts_healpix.getEMin().size();i_E_GLAST++)
	 cout<<  intensity_total[i_E_GLAST]<< " "      ;
       cout<<endl;

       cout<<"  intensity error: ";
       for( i_E_GLAST=0;i_E_GLAST< data.GLAST_counts_healpix.getEMin().size();i_E_GLAST++)
	 cout<<  intensity_error_total[i_E_GLAST]<< " "      ;
       cout<<endl;

       cout<<"  model_unconv_counts_total: ";
       for( i_E_GLAST=0;i_E_GLAST< data.GLAST_counts_healpix.getEMin().size();i_E_GLAST++)
	 cout<<  model_unconv_counts_total[i_E_GLAST]<< " "      ;
       cout<<endl;

       cout<<"  model___conv_counts_total: ";
       for( i_E_GLAST=0;i_E_GLAST< data.GLAST_counts_healpix.getEMin().size();i_E_GLAST++)
	 cout<< model___conv_counts_total[i_E_GLAST]<< " "      ;
       cout<<endl;

      cout<<"  model_unconv_over_conv: ";
       for( i_E_GLAST=0;i_E_GLAST< data.GLAST_counts_healpix.getEMin().size();i_E_GLAST++)
	 cout<< model_unconv_over_conv[i_E_GLAST]<< " "      ;
       cout<<endl;

       //--------------------------------------------------------------------------

       // definitions as required by differential_spectrum automatic
       double *E_GLAST       =new double[data.GLAST_counts_healpix.getEMin().size()+1 ];
       double *intensity_    =new double[data.GLAST_counts_healpix.getEMin().size()   ];
       double *intensity_esq1=new double[data.GLAST_counts_healpix.getEMin().size()   ];
       double *intensity_esq2=new double[data.GLAST_counts_healpix.getEMin().size()   ];
       double *Ebar          =new double[data.GLAST_counts_healpix.getEMin().size()   ];
       double *Ibar          =new double[data.GLAST_counts_healpix.getEMin().size()   ];

       double g=2.0;

       for( i_E_GLAST=0;i_E_GLAST< data.GLAST_counts_healpix.getEMin().size();i_E_GLAST++)
        {
	
          
          differential_spectrum(data.GLAST_counts_healpix.getEMin()[i_E_GLAST], data.GLAST_counts_healpix.getEMax()[ i_E_GLAST] ,
                                intensity_total[i_E_GLAST], g,
                                intensity_esq1 [i_E_GLAST],intensity_esq2[i_E_GLAST]);

	 
	  cout<<"e1,e2="<< data.GLAST_counts_healpix.getEMin() [i_E_GLAST]<<" "<<data.GLAST_counts_healpix.getEMax() [i_E_GLAST]
              <<" intensity="<<     intensity_total[i_E_GLAST]  <<"intensity_esq1, 2 ="<<intensity_esq1[i_E_GLAST]<<" "<<intensity_esq2[i_E_GLAST]
	      <<" counts="<<counts_total[i_E_GLAST]<<endl;

	  // E_GLAST is a list of energies including the first and last
	  E_GLAST[i_E_GLAST  ]=data.GLAST_counts_healpix.getEMin()[i_E_GLAST];
	  E_GLAST[i_E_GLAST+1]=data.GLAST_counts_healpix.getEMax()[i_E_GLAST];

          intensity_[i_E_GLAST  ]= intensity_total[i_E_GLAST];

	}

 

	 // automatic determination of spectral index
	 differential_spectrum(E_GLAST, data.GLAST_counts_healpix.getEMin().size()  , intensity_, intensity_esq1, intensity_esq2, Ebar,Ibar);

         cout<<"plot_GLAST_spectrum_healpix: E^2*spectrum"<<endl;                        
	 for(i_E_GLAST=0; i_E_GLAST<           data.GLAST_counts_healpix.getEMin().size(); i_E_GLAST++)
	   cout<<"GLAST spectrum *E^2: E1 E2 Ebar Ibar "                                            
               <<E_GLAST[i_E_GLAST]<<" "<<E_GLAST[i_E_GLAST+1]           
               <<" "<<Ebar[i_E_GLAST]<<" "<<Ibar[i_E_GLAST]<<endl;                

// tex output  
// txtFILE in  assigned in plot_spectrum

fprintf(txtFILE,"======================================================\n");
fprintf(txtFILE,"GLAST spectrum from healpix                                        \n");   
 fprintf(txtFILE,"     E(MeV)      E^2 * intensity (MeV cm^-2 sr^-1 s^-1)    counts    \n");
	 for(i_E_GLAST=0; i_E_GLAST<           data.GLAST_counts_healpix.getEMin().size(); i_E_GLAST++)
  {
  fprintf(txtFILE,"%6.0f-%6.0f & %9.5f &                             %9.1f \\\\ \n",E_GLAST[i_E_GLAST],E_GLAST[i_E_GLAST+1],Ibar[i_E_GLAST],counts_total[i_E_GLAST]   );  
                                   // "\\\\" yields "\\"
  }
fprintf(txtFILE,"======================================================\n");



//-------------------------------------------------------------------------------------
// plot the spectrum
// same method as for plot_EGRET_spectrum.cc

 double segment[2],Ebar_segment[2];
 double delta_err,stat_err,errorbar,errorbarfactor;
 TGraph  *spectrum;

for( i_E_GLAST=0;i_E_GLAST< data.GLAST_counts_healpix.getEMin().size();i_E_GLAST++)
{




 if(galplotdef.spectrum_style_EGRET==1 ||  galplotdef.spectrum_style_EGRET==3)
 {
  for (errorbar=1;errorbar<=2; errorbar++)
    {

          delta_err=0;                                                                

          if(errorbar==1)errorbarfactor=    1.0+galplotdef.error_bar_EGRET + delta_err; 
          if(errorbar==2)errorbarfactor=1./(1.0+galplotdef.error_bar_EGRET + delta_err);

	  // for GLAST we do not yet have an estimate of systematic error
	  errorbarfactor=    1.0; // AWS20080714

	  // now we do
          if(errorbar==1)errorbarfactor =       1.0 + intensity_sys_error[i_E_GLAST]/intensity_total[i_E_GLAST] ; //AWS20090408
	  if(errorbar==2)errorbarfactor =  1.0/(1.0 + intensity_sys_error[i_E_GLAST]/intensity_total[i_E_GLAST]); //AWS20090408

       

          stat_err=1.0/sqrt(1.0+counts_total[i_E_GLAST]);                                 
          if(errorbar==1)errorbarfactor*= 1.0+stat_err;                              
          if(errorbar==2)errorbarfactor/= 1.0+stat_err;                             

          cout<<     E_GLAST[i_E_GLAST]<<" MeV counts="<<counts_total[i_E_GLAST] <<" stat_err="<<stat_err<<" errorbarfactor="<<errorbarfactor<<endl;

	  segment[0]=intensity_esq1[i_E_GLAST]*errorbarfactor;
 	  segment[1]=intensity_esq2[i_E_GLAST]*errorbarfactor;

          

          spectrum=new TGraph(2,     &E_GLAST[i_E_GLAST],segment);

	  spectrum->SetMarkerColor(kBlack); //AWS20080714 AWS20091120
	  spectrum->SetMarkerStyle(21); 
	  spectrum->SetMarkerStyle(22); // triangles
	  spectrum->SetMarkerSize (0.5);// 
	  spectrum->SetLineColor(kBlack ); //AWS20080714  AWS20091120
	  spectrum->SetLineWidth(6     ); //AWS20080714
	  spectrum->SetLineStyle(1      );
	  spectrum->SetMarkerStyle(22); // triangles
	  spectrum->SetMarkerSize (0.5);// 
	  spectrum->Draw();       // don't use "same" for TGraph
	  spectrum->Draw(" P ");  // points as markers  

          spectrum->Draw();
     }//errorbar
    }//if







 if(galplotdef.spectrum_style_EGRET==2 ||  galplotdef.spectrum_style_EGRET==3)
 {
   /*
          delta_err=0;                                                                  //AWS20040312



         segment[0]=Ibar[i_E_GLAST]*(1.0+galplotdef.error_bar_EGRET + delta_err);       //AWS20040312
         segment[1]=Ibar[i_E_GLAST]/(1.0+galplotdef.error_bar_EGRET + delta_err);       //AWS20040312
   */
   // for GLAST we do not yet have estimate of systematic error

         segment[0]=Ibar[i_E_GLAST];       //AWS20080714
         segment[1]=Ibar[i_E_GLAST];       //AWS20080714


  
	

         stat_err=1.0/sqrt(1.0+counts_total[i_E_GLAST]);                                    //AWS20060322
         segment[0]      *=         (1.0+stat_err);                                    //AWS20060322
         segment[1]      /=         (1.0+stat_err);                                    //AWS20060322

	 // systematic errors
         errorbarfactor=1.0+ intensity_sys_error[i_E_GLAST]/intensity_total[i_E_GLAST] ; //AWS20090408
	 segment[0] *= errorbarfactor;                                                   //AWS20090408
	 segment[1] /= errorbarfactor;                                                   //AWS20090408

         cout<<E_GLAST[i_E_GLAST]<<" MeV counts="<<counts_total[i_E_GLAST] <<" stat_err="<<stat_err<<endl;

    Ebar_segment[0]=Ebar[i_E_GLAST];
    Ebar_segment[1]=Ebar[i_E_GLAST];
    spectrum=new TGraph(2,Ebar_segment,segment);

	  spectrum->SetMarkerColor(kBlack); //AWS20080714  AWS20091120
	  spectrum->SetMarkerStyle(21); 
	  spectrum->SetMarkerStyle(22); // triangles
	  spectrum->SetMarkerSize (0.5);// 
	  spectrum->SetLineColor(kBlack ); //AWS20080714  AWS20091120
	  spectrum->SetLineWidth(6     ); //AWS20080714
	  spectrum->SetLineStyle(1      );
	  spectrum->SetMarkerStyle(22); // triangles
	  spectrum->SetMarkerSize (0.5);// 
	  spectrum->Draw();       // don't use "same" for TGraph
	  spectrum->Draw(" P ");  // points as markers  

          spectrum->Draw();

  }//if
 }// i_E_GLAST

   cout<<" <<<< plot_GLAST_spectrum_healpix    "<<endl;
   return status;


}


