
using namespace std;
#include"Galplot.h"                    //AWS20050920
#include"galprop_classes.h"
#include"galprop.h"
#include"constants.h"

#include "fitsio.h" 
#include "slalib.h"

int  Galplot::plot_synchrotron_skymap() 
{
 int stat;
 int il,ib,ip;
 int ira,idec;
 int n_rgrid,n_zgrid;
 int select;
 double l,b,ra,dec,loff,boff,raoff,decoff;

 double CRVAL1,CRPIX1,CDELT1,CRVAL2,CRPIX2,CDELT2;          

 double pi=acos(-1.0); //AWS20110425 before was from healpix constants.h

 double k=1.38e-16;                // Boltzmann's constant, erg/K
 double I_to_Tb=C*C/ (2.* k);      // intensity to brighness temp: Tb= I_to_Tb * I/nu^2;
 double dtr=acos(-1.0)/180.;

 double intensity,nu;
 double *intensity_mean,*intensity_Q_mean,*intensity_U_mean,*intensity_P_mean; //AWS20110412
 double *intensity_free_free_mean;                                             //AWS20110622
 double *intensity_dust_mean,*intensity_dust_Q_mean,*intensity_dust_U_mean,*intensity_dust_P_mean;    //AWS20120117
 double *intensity_sync_dust_P_mean;                                                                  //AWS20120727
 double *intensity_synchrotron_template_mean;                                  //AWS20111222
 

 double intensity_mean____10MHz=0;  //AWS20110622
 double intensity_mean____22MHz=0;
 double intensity_mean____45MHz=0;
 double intensity_mean___150MHz=0;
 double intensity_mean___408MHz=0;
 double intensity_mean___820MHz=0;
 double intensity_mean__1420MHz=0;
 double intensity_mean__2326MHz=0;

 double *spectral_index_mean;
 int    n_mean,n_select;
 double     survey_fraction;          //         fraction of region with valid data in survey
 double     survey_fraction_min=0.80; // minimum fraction of region with valid data to plot survey point

 Distribution synchrotron_index_skymap;
 Distribution synchrotron_Tb_skymap;   // brightness temperature (K) = I c^2/(2k nu^2) 
 TCanvas *c1;
 TGraph *spectrum;
 TGraph *datapoint;
 TGraph *spectral_index;
 TGraph *spectral_index_data; //AWS20090123
 TGraph *spectrum_free_free,*spectrum_sync_free_free;      //AWS20110622
 TGraph *spectrum_dust,     *spectrum_sync_free_free_dust; //AWS20111222
 TGraph *spectrum_dust_Q,*spectrum_dust_U,*spectrum_dust_P;//AWS20120117
 TGraph *spectrum_sync_dust_Q,*spectrum_sync_dust_U,*spectrum_sync_dust_P;//AWS20120119
 TGraph *spectrum_synchrotron_template;                    //AWS20111222

TText *text;
TLatex *latex;
char name[100],canvastitle[100], workstring1[100],workstring2[100];;
char  psfile[400];
char giffile[400];
char txtfile[400];


  cout<<" >>>> plot_synchrotron_skymap"<<endl;

   stat=0;
   
  //-------------------- coordinates of point to be plotted -------------------------------
  l=180;   b= 0;
  l= 90;   b= 0;
  l= 60;   b=10;
  //  l= 60;   b=20;
  //---------------------------------------------------------------------------------------
   
   synchrotron_index_skymap.init(galaxy.n_long,galaxy.n_lat,galaxy.n_nu_synchgrid);
   synchrotron_Tb_skymap   .init(galaxy.n_long,galaxy.n_lat,galaxy.n_nu_synchgrid);

   intensity_mean     =new double[galaxy.n_nu_synchgrid];
   intensity_Q_mean   =new double[galaxy.n_nu_synchgrid];//AWS20110412
   intensity_U_mean   =new double[galaxy.n_nu_synchgrid];//AWS20110412
   intensity_P_mean   =new double[galaxy.n_nu_synchgrid];//AWS20110412

   intensity_free_free_mean            =new double[galaxy.n_nu_synchgrid];//AWS20110622
   intensity_dust_mean                 =new double[galaxy.n_nu_synchgrid];//AWS20111222
   intensity_dust_Q_mean               =new double[galaxy.n_nu_synchgrid];//AWS20120117
   intensity_dust_U_mean               =new double[galaxy.n_nu_synchgrid];//AWS20120117
   intensity_dust_P_mean               =new double[galaxy.n_nu_synchgrid];//AWS20120117

   intensity_sync_dust_P_mean          =new double[galaxy.n_nu_synchgrid];//AWS20120727

   intensity_synchrotron_template_mean =new double[galaxy.n_nu_synchgrid];//AWS20111222


   spectral_index_mean=new double[galaxy.n_nu_synchgrid];


   if(galdef.skymap_format==0) //AWS20100615
   {

   for ( ip       =0;        ip<galaxy.n_nu_synchgrid;       ip++)      intensity_mean  [ip]=0.;
   for ( ip       =0;        ip<galaxy.n_nu_synchgrid;       ip++)      intensity_Q_mean[ip]=0.;//AWS20110519
   for ( ip       =0;        ip<galaxy.n_nu_synchgrid;       ip++)      intensity_U_mean[ip]=0.;//AWS20110519
   for ( ip       =0;        ip<galaxy.n_nu_synchgrid;       ip++)      intensity_P_mean[ip]=0.;//AWS20110519

   for ( ip       =0;        ip<galaxy.n_nu_synchgrid;       ip++) spectral_index_mean[ip]=0.;
   n_mean=0;

   for ( ib       =0;        ib<galaxy.n_lat;                ib++)
   for ( il       =0;        il<galaxy.n_long;               il++)
   {
    for (ip       =0;        ip<galaxy.n_nu_synchgrid;       ip++)
    {             
        synchrotron_Tb_skymap     .d2[il][ib].s[ip]
      =  galaxy.synchrotron_skymap.d2[il][ib].s[ip]
	     *I_to_Tb / pow(galaxy.nu_synch[ip],2)  ;
     
     if(ip> 0 && ip<galaxy.n_nu_synchgrid-1)
     {
       //   brightness temperature index, hence add 2

            synchrotron_index_skymap.d2[il][ib].s[ip]=
       2.0 -
       log(galaxy.synchrotron_skymap.d2[il][ib].s[ip-1]/
           galaxy.synchrotron_skymap.d2[il][ib].s[ip+1])
         /
       log (                      galaxy.nu_synch[ip-1]/
                                  galaxy.nu_synch[ip+1]);          
     }//if

     if(ip==0)                                  
     {
            synchrotron_index_skymap.d2[il][ib].s[ip]=
       2.0 -
       log(galaxy.synchrotron_skymap.d2[il][ib].s[ip  ]/
           galaxy.synchrotron_skymap.d2[il][ib].s[ip+1])
         /
       log (                      galaxy.nu_synch[ip  ]/
                                  galaxy.nu_synch[ip+1]);          
     }//if

     if(ip==galaxy.n_nu_synchgrid-1 )                                  
     {
            synchrotron_index_skymap.d2[il][ib].s[ip]=
       2.0 -
       log(galaxy.synchrotron_skymap.d2[il][ib].s[ip  ]/
           galaxy.synchrotron_skymap.d2[il][ib].s[ip-1])
         /
       log (                      galaxy.nu_synch[ip  ]/
                                  galaxy.nu_synch[ip-1]);          
     }//if

    }//ip

     l=galaxy.long_min + galaxy.d_long *il;
     b=galaxy. lat_min + galaxy.d_lat  *ib;
     select=0;
     // check whether point is in any of the 4 regions
     if(l>=galplotdef.long_min1&&l<=galplotdef.long_max1 && b>=galplotdef.lat_min1 && b<=galplotdef.lat_max1) select=1;
     if(l>=galplotdef.long_min2&&l<=galplotdef.long_max2 && b>=galplotdef.lat_min1 && b<=galplotdef.lat_max1) select=1;
     if(l>=galplotdef.long_min1&&l<=galplotdef.long_max1 && b>=galplotdef.lat_min2 && b<=galplotdef.lat_max2) select=1;
     if(l>=galplotdef.long_min2&&l<=galplotdef.long_max2 && b>=galplotdef.lat_min2 && b<=galplotdef.lat_max2) select=1;

     if(select==1)
     {
       //cout<<"selected point l = "<<l<<" b = "<<b<<endl;
       n_mean++;
       for (ip       =0;        ip<galaxy.n_nu_synchgrid;       ip++)      intensity_mean  [ip]+= galaxy.synchrotron_skymap      .d2[il][ib].s[ip];
       for (ip       =0;        ip<galaxy.n_nu_synchgrid;       ip++)      intensity_Q_mean[ip]+= galaxy.synchrotron_Q_skymap    .d2[il][ib].s[ip];//AWS20110519
       for (ip       =0;        ip<galaxy.n_nu_synchgrid;       ip++)      intensity_U_mean[ip]+= galaxy.synchrotron_U_skymap    .d2[il][ib].s[ip];//AWS20110519
       for (ip       =0;        ip<galaxy.n_nu_synchgrid;       ip++)      intensity_P_mean[ip]+= galaxy.synchrotron_P_skymap    .d2[il][ib].s[ip];//AWS20110519


       for (ip       =0;        ip<galaxy.n_nu_synchgrid;       ip++) spectral_index_mean[ip]+=        synchrotron_index_skymap.d2[il][ib].s[ip];
     }

   }// ib il

   }// skymap_format==0


   ////////////////  healpix format    AWS20100615

   Skymap<double> synchrotron_index_hp_skymap= galaxy.synchrotron_hp_skymap;

 if(galdef.skymap_format==3)
 {
  cout<<"plot_synchrotron_skymap: starting loop over healpix pixels"<<endl; 
  double rtd=180./pi;        // rad to deg

  for( ip=0;ip<  galaxy.synchrotron_hp_skymap.nSpectra() ;ip++)
  {
        intensity_mean   [ip] = 0.;
        intensity_Q_mean [ip] = 0.;//AWS20110412
        intensity_U_mean [ip] = 0.;//AWS20110412
        intensity_P_mean [ip] = 0.;//AWS20110412

        intensity_free_free_mean            [ip] = 0.;//AWS20110622
        intensity_dust_mean                 [ip] = 0.;//AWS20111222
        intensity_dust_Q_mean               [ip] = 0.;//AWS20120117
        intensity_dust_U_mean               [ip] = 0.;//AWS20120117
        intensity_dust_P_mean               [ip] = 0.;//AWS20120117

        intensity_sync_dust_P_mean          [ip] = 0.;//AWS20120727

        intensity_synchrotron_template_mean [ip] = 0.;//AWS20111222

   spectral_index_mean [ip] = 0.;
  }

  for (int ipix=0;ipix< galaxy.synchrotron_hp_skymap.Npix() ;ipix++)
  {
   for( ip=0;ip<  galaxy.synchrotron_hp_skymap.nSpectra() ;ip++)
   {
    if(ip> 0 && ip<galaxy.n_nu_synchgrid-1)
     {
       //   brightness temperature index, hence add 2

            synchrotron_index_hp_skymap[ipix][ip]=
       2.0 -
       log(galaxy.synchrotron_hp_skymap[ipix][ip-1]/
           galaxy.synchrotron_hp_skymap[ipix][ip+1])
         /
       log (                      galaxy.nu_synch[ip-1]/
                                  galaxy.nu_synch[ip+1]);          
     }//if

     if(ip==0)                                  
     {
            synchrotron_index_hp_skymap[ipix][ip]=
       2.0 -
       log(galaxy.synchrotron_hp_skymap[ipix][ip  ]/
           galaxy.synchrotron_hp_skymap[ipix][ip+1])
         /
       log (                      galaxy.nu_synch[ip  ]/
                                  galaxy.nu_synch[ip+1]);          
     }//if

     if(ip==galaxy.n_nu_synchgrid-1 )                                  
     {
            synchrotron_index_hp_skymap[ipix][ip]=
       2.0 -
       log(galaxy.synchrotron_hp_skymap[ipix][ip  ]/
           galaxy.synchrotron_hp_skymap[ipix][ip-1])
         /
       log (                      galaxy.nu_synch[ip  ]/
                                  galaxy.nu_synch[ip-1]);          
     }//if

    }//ip

 
} //ipix


  n_mean=0;

 for (int ipix=0;ipix< galaxy.synchrotron_hp_skymap.Npix() ;ipix++)
   {
    l =        galaxy.synchrotron_hp_skymap.pix2ang(ipix).phi   * rtd;
    b = 90.0 - galaxy.synchrotron_hp_skymap.pix2ang(ipix).theta * rtd;

     if(galplotdef.verbose==-2000)
     {
      cout<<"ipix="<<ipix<<" l = "<<l<<" b ="<<b<<" galaxy.synchrotron_hp_skymap[ipix][ip]  = ";
      for( ip=0;ip<  galaxy.pi0_decay_hp_skymap.nSpectra() ;ip++)
        cout<<galaxy.synchrotron_hp_skymap[ipix][ip] <<endl; 
     }

     select=0;
    // select if pixel lies in one of the 4 regions
    if(l>=galplotdef.long_min1 && l<=galplotdef.long_max1 && b>=galplotdef.lat_min1 && b<=galplotdef.lat_max1 ) select=1;
    if(l>=galplotdef.long_min2 && l<=galplotdef.long_max2 && b>=galplotdef.lat_min1 && b<=galplotdef.lat_max1 ) select=1;
    if(l>=galplotdef.long_min1 && l<=galplotdef.long_max1 && b>=galplotdef.lat_min2 && b<=galplotdef.lat_max2 ) select=1;
    if(l>=galplotdef.long_min2 && l<=galplotdef.long_max2 && b>=galplotdef.lat_min2 && b<=galplotdef.lat_max2 ) select=1;

    if (select==1)
    {
      n_mean++;
     for( ip=0;ip<  galaxy.synchrotron_hp_skymap.nSpectra() ;ip++)
     {
       intensity_mean           [ip] +=  galaxy.synchrotron_hp_skymap  [ipix][ip];
       intensity_Q_mean         [ip] +=  galaxy.synchrotron_Q_hp_skymap[ipix][ip];//AWS20110412 
       intensity_U_mean         [ip] +=  galaxy.synchrotron_U_hp_skymap[ipix][ip];//AWS20110412 
       intensity_P_mean         [ip] +=  galaxy.synchrotron_P_hp_skymap[ipix][ip];//AWS20110412 


       
       if(galplotdef.free_free_options==1)                                           //AWS20111222
       intensity_free_free_mean [ip] +=  galaxy.free_free_hp_skymap    [ipix][ip];   //AWS20110906



       if(galplotdef.free_free_options==2)                                           //AWS20111222
       intensity_free_free_mean            [ip] +=  data  .free_free_WMAP_MEM_hp_skymap[ipix][ip];//AWS20110622

      



       if(galplotdef.spin_dust_options==1)                                                        //AWS20120117
       {
        intensity_dust_mean                [ip] +=  data.       dust_WMAP_MCMC_SD_hp_skymap[ipix][ip];
        intensity_dust_mean                [ip] +=  data.  spin_dust_WMAP_MCMC_SD_hp_skymap[ipix][ip];

        intensity_dust_Q_mean              [ip] +=  data.     dust_Q_WMAP_MCMC_SD_hp_skymap[ipix][ip];
        intensity_dust_U_mean              [ip] +=  data.     dust_U_WMAP_MCMC_SD_hp_skymap[ipix][ip];
        intensity_dust_P_mean              [ip] +=  data.     dust_P_WMAP_MCMC_SD_hp_skymap[ipix][ip];

        intensity_sync_dust_P_mean         [ip] +=  sqrt(                                                      //AWS20120727
		pow(galaxy.synchrotron_Q_hp_skymap[ipix][ip] + data.dust_Q_WMAP_MCMC_SD_hp_skymap[ipix][ip],2) //AWS20120727
              + pow(galaxy.synchrotron_U_hp_skymap[ipix][ip] + data.dust_U_WMAP_MCMC_SD_hp_skymap[ipix][ip],2) //AWS20120727
							);                                                     //AWS20120727

       }

       if(galplotdef.spin_dust_options==2)                                                        //AWS20120117
       intensity_dust_mean                 [ip] +=  data.       dust_WMAP_MEM_hp_skymap[ipix][ip];//AWS20111222


       intensity_synchrotron_template_mean [ip] +=  data.synchrotron_WMAP_MEM_hp_skymap[ipix][ip];//AWS20111222

       spectral_index_mean      [ip] +=     synchrotron_index_hp_skymap[ipix][ip];



      } //ip
     } //if select
    } //ipix
   } // if skymap_format==3

     ///////////////////////////////////////////////////////////////////////////////////////////////

   for ( ip       =0;        ip<galaxy.n_nu_synchgrid;       ip++)      intensity_mean  [ip]/=n_mean;
   for ( ip       =0;        ip<galaxy.n_nu_synchgrid;       ip++)      intensity_Q_mean[ip]/=n_mean;//AWS20110412
   for ( ip       =0;        ip<galaxy.n_nu_synchgrid;       ip++)      intensity_U_mean[ip]/=n_mean;//AWS20110412
   for ( ip       =0;        ip<galaxy.n_nu_synchgrid;       ip++)      intensity_P_mean[ip]/=n_mean;//AWS20110412

   for ( ip       =0;        ip<galaxy.n_nu_synchgrid;       ip++)      intensity_free_free_mean           [ip]/=n_mean;//AWS20110622
   for ( ip       =0;        ip<galaxy.n_nu_synchgrid;       ip++)      intensity_dust_mean                [ip]/=n_mean;//AWS20111222
   for ( ip       =0;        ip<galaxy.n_nu_synchgrid;       ip++)      intensity_dust_Q_mean              [ip]/=n_mean;//AWS20120117
   for ( ip       =0;        ip<galaxy.n_nu_synchgrid;       ip++)      intensity_dust_U_mean              [ip]/=n_mean;//AWS20120117
   for ( ip       =0;        ip<galaxy.n_nu_synchgrid;       ip++)      intensity_dust_P_mean              [ip]/=n_mean;//AWS20120117
   for ( ip       =0;        ip<galaxy.n_nu_synchgrid;       ip++)      intensity_sync_dust_P_mean         [ip]/=n_mean;//AWS20120727

   for ( ip       =0;        ip<galaxy.n_nu_synchgrid;       ip++)      intensity_synchrotron_template_mean[ip]/=n_mean;//AWS20111222
   for ( ip       =0;        ip<galaxy.n_nu_synchgrid;       ip++) spectral_index_mean[ip]/=n_mean;


   //galaxy.synchrotron_skymap .print();
   /*
    cout<<" ======= synchrotron_index_skymap"<<endl;
     synchrotron_index_skymap .print();
   */
     il=galaxy.n_long/2;
     ib=galaxy. n_lat/2;

   for (int ip       =0;        ip<galaxy.n_nu_synchgrid;       ip++)

     cout<<"frequency = "<<galaxy.nu_synch[ip]/1.e6<<" MHz   index="

         <<synchrotron_index_skymap.d2[ 0][ib].s[ip]<<" (GC)    "
         <<synchrotron_index_skymap.d2[il][ib].s[ip]<<" (AC)    "
         <<synchrotron_index_skymap.d2[ 0][0 ].s[ip]<<" (pole)  "
	 <<" Tb = "
         <<synchrotron_Tb_skymap.   d2[ 0][ib].s[ip]<<" (GC)    "
         <<synchrotron_Tb_skymap   .d2[il][ib].s[ip]<<" (AC)    "
         <<synchrotron_Tb_skymap   .d2[ 0][0 ].s[ip]<<" (pole)  "
         <<endl;

 //====== see HowTo Style: and do before creating canvas


   TStyle *plain  = new TStyle("Plain","Plain Style (no colors/fill areas)");


   plain->SetCanvasBorderMode(0);
   plain->SetPadBorderMode(0);
   plain->SetPadColor(0);
   plain->SetCanvasColor(0);
   plain->SetTitleColor(1,"X"); //AWS20080125
   plain->SetTitleColor(1,"Y"); //AWS20080125
   plain->SetStatColor(0);
   gROOT->SetStyle("Plain");


  strcpy(name,"synch_spectrum");// to identify it: name is not plotted
  c1=new TCanvas(name,name,200,100,650,650);

  c1  ->SetLogx(1);
  c1  ->SetLogy(1);


spectrum      =new TGraph(galaxy.n_nu_synchgrid);
spectral_index=new TGraph(galaxy.n_nu_synchgrid);


  il=int((l-galaxy.long_min)/galaxy.d_long);
  ib=int((b-galaxy. lat_min)/galaxy.d_lat );
  if(il<0)il+=galaxy.n_long;  if(il>galaxy.n_long-1)il+=galaxy.n_long;
  if(ib<0)ib=0;               if(ib>galaxy.n_lat -1)ib+=galaxy.n_lat ;

  cout<<"galprop synchrotron skymap: l="<<l<<" b="<<b<<" il="<<il<<" ib="<<ib<<endl;



  for  (ip    =0;  ip    <galaxy.n_nu_synchgrid; ip++)
     spectrum->      SetPoint(ip,galaxy.nu_synch[ip]/1e6,     intensity_mean[ip]);

  for  (ip    =0;  ip    <galaxy.n_nu_synchgrid; ip++)
     spectral_index->SetPoint(ip,galaxy.nu_synch[ip]/1e6,spectral_index_mean[ip]);

  

//Gtypes.h:enum EColor { kWhite, kBlack, kRed, kGreen, kBlue, kYellow, kMagenta, kCyan };
  spectrum->SetMarkerColor(kBlue );
  spectrum->SetMarkerStyle(21);
  spectrum->SetMarkerStyle(22); // triangles
  spectrum->SetMarkerSize (0.5);//
  spectrum->SetLineColor(kBlue );
  spectrum->SetLineWidth(2     );
  spectrum->SetLineStyle(1      );//1=solid 2=dash 3=dot 4=dash-dot

  spectrum->SetTitle(""         ); // removes text written in box on plot
  spectrum->GetHistogram()->SetXTitle("frequency, MHz                 ");

  spectrum->GetHistogram()->SetYTitle("intensity / erg cm^{-2} sr^{-1} s^{-1} Hz^{-1}       "); //needs {} to work AWS20121130 added spaces at end to avoid conflict with labels

  spectrum->GetHistogram()->SetLabelOffset(+0.00,"X"); //  +ve down
  spectrum->GetHistogram()->SetLabelSize  ( 0.03,"Y"); // labels, not titles


  spectrum->GetHistogram()->SetTitleOffset(+1.15,"X");  //  +ve down    //AWS20121130 was +1.20
  spectrum->GetHistogram()->SetTitleOffset(+1.20,"Y");  //  +ve to left //AWS20121130 was +1.46
  spectrum->GetHistogram()->SetTitleSize   (0.040,"X");//AWS20121130 was 0.035
  spectrum->GetHistogram()->SetTitleSize   (0.040,"Y");//AWS20121130 was 0.035  

  spectrum->SetMinimum(1.0e-21);
  spectrum->SetMaximum(1.0e-16);

  spectrum->Draw("AL");       // axes  lines


  //spectrum->Draw("PL");  // axes; points as markers;line  AWS20040408





  ///////////////////////////////////////////////////////////////////
  // plot surveys
  ///////////////////////////////////////////////////////////////////





  slaGaleq(l*dtr,b*dtr,&ra,&dec); ra/=dtr; dec/=dtr;
  cout<<"l="<<l<<" b="<<b<<" ra="<<ra<<" dec="<<dec<<endl;

  if(galplotdef.sync_data____10MHz==1) //AWS20071221
  {
  // -----------------------------------  10 MHz

  //  10 MHz map:  320 * 45   RA,dec 
  
  CRVAL1  =        .23900443E+03;          // REF COORD VALUE (DEG)
  CRPIX1  =        .10000000E+01;          // REF POINT PIXEL
  CDELT1  =       -.75000000E+00;          // COORD VALUE INCREMENT (DEG)
  CRVAL2  =       -.53772001E+01;          // REF COORD VALUE (DEG)
  CRPIX2  =        .10000000E+01;          // REF POINT PIXEL
  CDELT2  =        .17979165E+01;          // COORD VALUE INCREMENT (DEG)
  n_rgrid = data.synchrotron____10MHz.n_rgrid;
  n_zgrid = data.synchrotron____10MHz.n_zgrid;
  //data.synchrotron____10MHz.print();

  /*
   raoff =    ra- CRVAL1;
  decoff =   dec- CRVAL2;

  ira =int( raoff/CDELT1 + CRPIX1 - 1);
  if(ira<0)        ira+=n_rgrid;
  if(ira>n_rgrid-1)ira-=n_rgrid;

  idec=int(decoff/CDELT2 + CRPIX2 - 1);
  cout<<"10 MHz map: l="<<l<<" b="<<b<<" ra="<<ra<<" dec="<<dec<<" ira="<<ira<<" idec="<<idec<<endl;

  if(idec>=0 && idec<=n_zgrid-1)
  {

  ira =260   ;// test point in anticentre (since dec coverage does not reach inner Galaxy north)
  idec= 40;

  datapoint=new TGraph(1);
  nu= 10e6;//  10 MHz
  // convert Tb (K) to intensity
  intensity=data.synchrotron____10MHz.d2[ira][idec].s[0] / I_to_Tb  *pow(nu,2);

  cout<<"datapoint nu="<<nu<<" intensity="<<intensity<<endl;

  datapoint->SetPoint(0, nu/1.e6, intensity);

  datapoint->SetMarkerColor(kRed );
  datapoint->SetMarkerStyle(22); // triangles
  datapoint->SetMarkerSize (2.5);//


  datapoint->Draw("P");
  }//if

  */

  intensity=0;
  n_mean   =0; // selected according to coordinates  and checking for      valid data
  n_select =0; // selected according to coordinates independent of whether valid data

  for( ira=0; ira <n_rgrid;  ira++)
  for(idec=0; idec<n_zgrid; idec++)
  {
    ra=CRVAL1+CDELT1*(ira -CRPIX1+1);
   dec=CRVAL2+CDELT2*(idec-CRPIX2+1);
   slaEqgal(ra*dtr,dec*dtr,&l,&b); l/=dtr; b/=dtr;

   if(l<0.0)l+=360.0;    //AWS20070209
   //cout<<"10 MHz ira="<<ira<<" idec="<<idec<<" ra="<<ra<<" dec="<<dec<<" l="<<l<<" b="<<b<<endl;



    select=0;
     // check whether point is in any of the 4 regions
     if(l>=galplotdef.long_min1&&l<=galplotdef.long_max1 && b>=galplotdef.lat_min1 && b<=galplotdef.lat_max1) select=1;
     if(l>=galplotdef.long_min2&&l<=galplotdef.long_max2 && b>=galplotdef.lat_min1 && b<=galplotdef.lat_max1) select=1;
     if(l>=galplotdef.long_min1&&l<=galplotdef.long_max1 && b>=galplotdef.lat_min2 && b<=galplotdef.lat_max2) select=1;
     if(l>=galplotdef.long_min2&&l<=galplotdef.long_max2 && b>=galplotdef.lat_min2 && b<=galplotdef.lat_max2) select=1;


     if(select==1)
     {
       n_select++;
       // cout<<"10 MHz:selected point l = "<<l<<" b = "<<b<<" intensity="<< data.synchrotron____10MHz.d2[ira][idec].s[0]<< endl;
       if (data.synchrotron____10MHz.d2[ira][idec].s[0]>0.) // excludes also nan's
       {
       n_mean++;
       intensity+= data.synchrotron____10MHz.d2[ira][idec].s[0];
       }
     }
  }
  intensity/=n_mean;
  survey_fraction=double(n_mean)/double(n_select);
  cout<<"10 MHz number of points in region  n_select="<<n_select<<" of which valid data chosen  n_mean="<<n_mean<<" fraction="<<survey_fraction;
  if (survey_fraction <survey_fraction_min)cout<<" coverage < "<<survey_fraction_min<<" : not plotted !"<<endl;
  if (survey_fraction>=survey_fraction_min)cout<<" coverage > "<<survey_fraction_min<<" :     plotted !"<<endl; 

  nu= 10e6;//  10 MHz
  // convert Tb (K) to intensity
  intensity=intensity/ I_to_Tb  *pow(nu,2);
  if (survey_fraction>=survey_fraction_min)intensity_mean____10MHz=intensity;  //AWS20110622

  cout<<"datapoint nu="<<nu<<" intensity="<<intensity<<endl;
  datapoint=new TGraph(1);
  datapoint->SetPoint(0, nu/1.e6, intensity);

  datapoint->SetMarkerColor(kRed );
  datapoint->SetMarkerStyle(22); // triangles
  datapoint->SetMarkerSize (1.5);//

  if (survey_fraction>=survey_fraction_min)
  datapoint->Draw("P");

  }// if



  // -----------------------------------  22 MHz
  if(galplotdef.sync_data____22MHz==1) //AWS20071221
  {
  //  22 MHz map: 1440*433, RA,dec 
  
CRVAL1  =        .00000000E+00 ;         // REF COORD VALUE (DEG)                 
CRPIX1  =        .14400000E+04 ;         // REF POINT PIXEL                       
CDELT1  =       -.25000000E+00 ;         // COORD VALUE INCREMENT (DEG)                                 
CRVAL2  =       -.28000000E+02 ;         // REF COORD VALUE (DEG)                 
CRPIX2  =        .10000000E+01 ;         // REF POINT PIXEL                       
CDELT2  =        .24999997E+00 ;         // COORD VALUE INCREMENT (DEG) 
  n_rgrid = data.synchrotron____22MHz.n_rgrid;
  n_zgrid = data.synchrotron____22MHz.n_zgrid;

  /*
   raoff =    ra- CRVAL1;
  decoff =   dec- CRVAL2;

  ira =int( raoff/CDELT1 + CRPIX1 - 1);
  if(ira<0)        ira+=n_rgrid;
  if(ira>n_rgrid-1)ira-=n_rgrid;

  idec=int(decoff/CDELT2 + CRPIX2 - 1);
  cout<<"22 MHz map: l="<<l<<" b="<<b<<" ra="<<ra<<" dec="<<dec<<" ira="<<ira<<" idec="<<idec<<endl;

  if(idec>=0 && idec<=n_zgrid-1)
  {  
    
  il=370   ;// test point in positive longitudes avoiding bright points
  ib= 50;

  datapoint=new TGraph(1);
  nu= 22e6;//  22 MHz
  // convert Tb (K) to intensity
  intensity=data.synchrotron____22MHz.d2[ira][idec ].s[0] / I_to_Tb  *pow(nu,2);

  cout<<"datapoint nu="<<nu<<" intensity="<<intensity<<endl;

  datapoint->SetPoint(0, nu/1.e6, intensity);

  datapoint->SetMarkerColor(kRed );
  datapoint->SetMarkerStyle(22); // triangles
  datapoint->SetMarkerSize (2.5);//


  datapoint->Draw("P");

  }//if

  */

  intensity=0;
  n_mean   =0; // selected according to coordinates  and checking for      valid data
  n_select =0; // selected according to coordinates independent of whether valid data

  for( ira=0; ira <n_rgrid;  ira++)
  for(idec=0; idec<n_zgrid; idec++)
  {
    ra=CRVAL1+CDELT1*(ira -CRPIX1+1);
   dec=CRVAL2+CDELT2*(idec-CRPIX2+1);
   slaEqgal(ra*dtr,dec*dtr,&l,&b); l/=dtr; b/=dtr;

   if(l<0.0)l+=360.0;    //AWS20070209
     //   cout<<"22 MHz ira="<<ira<<" idec="<<idec<<" ra="<<ra<<" dec="<<dec<<" l="<<l<<" b="<<b<<endl;



    select=0;
     // check whether point is in any of the 4 regions
     if(l>=galplotdef.long_min1&&l<=galplotdef.long_max1 && b>=galplotdef.lat_min1 && b<=galplotdef.lat_max1) select=1;
     if(l>=galplotdef.long_min2&&l<=galplotdef.long_max2 && b>=galplotdef.lat_min1 && b<=galplotdef.lat_max1) select=1;
     if(l>=galplotdef.long_min1&&l<=galplotdef.long_max1 && b>=galplotdef.lat_min2 && b<=galplotdef.lat_max2) select=1;
     if(l>=galplotdef.long_min2&&l<=galplotdef.long_max2 && b>=galplotdef.lat_min2 && b<=galplotdef.lat_max2) select=1;


     if(select==1)
     {
       n_select++;
       //  cout<<"22 MHz:selected point l = "<<l<<" b = "<<b<<" intensity="<< data.synchrotron____22MHz.d2[ira][idec].s[0]<< endl;
       if (data.synchrotron____22MHz.d2[ira][idec].s[0]>0.) // excludes also nan's 
       {
       n_mean++;
       intensity+= data.synchrotron____22MHz.d2[ira][idec].s[0];
       }
     }
  }
  intensity/=n_mean;
  survey_fraction=double(n_mean)/double(n_select);
  cout<<"22 MHz number of points in region  n_select="<<n_select<<" of which valid data chosen  n_mean="<<n_mean<<" fraction="<<survey_fraction;
  if (survey_fraction <survey_fraction_min)cout<<" coverage < "<<survey_fraction_min<<" : not plotted !"<<endl; 
  if (survey_fraction>=survey_fraction_min)cout<<" coverage > "<<survey_fraction_min<<" :     plotted !"<<endl; 

  nu= 22e6;//  22 MHz
  // convert Tb (K) to intensity
  intensity=intensity/ I_to_Tb  *pow(nu,2);
  if (survey_fraction>=survey_fraction_min)intensity_mean____22MHz=intensity;  //AWS20110622
  cout<<"datapoint nu="<<nu<<" intensity="<<intensity<<endl;

  datapoint=new TGraph(1);
  datapoint->SetPoint(0, nu/1.e6, intensity);

  datapoint->SetMarkerColor(kRed );
  datapoint->SetMarkerStyle(22); // triangles
  datapoint->SetMarkerSize (1.5);//

  if (survey_fraction>=survey_fraction_min)
  datapoint->Draw("P");


  }// if

  // -----------------------------------  45 MHz
  cout<<" 45 MHz  galplotdef.sync_data____45MHz="<<galplotdef.sync_data____45MHz<<endl; 

  if(galplotdef.sync_data____45MHz==1) //AWS20071221
  {
  // Galactic coordinates
  
  

  CRVAL1  =    180.0             ;
  CDELT1  =     -0.5             ;
  CRPIX1  =      1.0             ;
  
  CRVAL2  =   -9.00000000000E+01 ;
  CDELT2  =      0.5             ;
  CRPIX2  =      1.0             ;

  n_rgrid = data.synchrotron____45MHz.n_rgrid;
  n_zgrid = data.synchrotron____45MHz.n_zgrid;


  intensity=0;
  n_mean   =0; // selected according to coordinates  and checking for      valid data
  n_select =0; // selected according to coordinates independent of whether valid data

  for(  il=0;  il <n_rgrid;   il++)
  for(  ib=0;  ib <n_zgrid;   ib++)
  {
     l=CRVAL1+CDELT1*(il -CRPIX1+1);
     b=CRVAL2+CDELT2*(ib- CRPIX2+1);
   
   //cout<<" 45 MHz il="<<il<<" ib="<<" l="<<l<<" b="<<b<<endl;



    select=0;
     // check whether point is in any of the 4 regions
     if(l>=galplotdef.long_min1&&l<=galplotdef.long_max1 && b>=galplotdef.lat_min1 && b<=galplotdef.lat_max1) select=1;
     if(l>=galplotdef.long_min2&&l<=galplotdef.long_max2 && b>=galplotdef.lat_min1 && b<=galplotdef.lat_max1) select=1;
     if(l>=galplotdef.long_min1&&l<=galplotdef.long_max1 && b>=galplotdef.lat_min2 && b<=galplotdef.lat_max2) select=1;
     if(l>=galplotdef.long_min2&&l<=galplotdef.long_max2 && b>=galplotdef.lat_min2 && b<=galplotdef.lat_max2) select=1;


     if(select==1)
     {
       n_select++;
       //  cout<<" 45 MHz:selected point l = "<<l<<" b = "<<b<<" intensity="<< data.synchrotron____45MHz.d2[il][ib].s[0]<< endl;
       if (data.synchrotron____45MHz.d2[il][ib].s[0]>0.) // excludes also nan's 
       {
       n_mean++;
       intensity+= data.synchrotron____45MHz.d2[il][ib].s[0];
       }
     }
  }
  intensity/=n_mean;
  survey_fraction=double(n_mean)/double(n_select);
  cout<<"45 MHz number of points in region  n_select="<<n_select<<" of which valid data chosen  n_mean="<<n_mean<<" fraction="<<survey_fraction;
  if (survey_fraction< survey_fraction_min)cout<<" coverage < "<<survey_fraction_min<<" : not plotted !"<<endl;
  if (survey_fraction>=survey_fraction_min)cout<<" coverage > "<<survey_fraction_min<<" :     plotted !"<<endl; 

  nu= 45e6;//  45 MHz

  intensity -= 550.; // Guzman etal 2011 A&A 525 A138 //AWS20110223

  // convert Tb (K) to intensity
  intensity=intensity/ I_to_Tb  *pow(nu,2);
  if (survey_fraction>=survey_fraction_min)intensity_mean____45MHz=intensity;  //AWS20110622
  cout<<"datapoint nu="<<nu<<" intensity="<<intensity<<endl;

  datapoint=new TGraph(1);
  datapoint->SetPoint(0, nu/1.e6, intensity);

  datapoint->SetMarkerColor(kRed );
  datapoint->SetMarkerStyle(22); // triangles
  datapoint->SetMarkerSize (1.5);//

  if (survey_fraction>=survey_fraction_min)
  datapoint->Draw("P");


  }// if
  
  // ---------------------------------------------------------- 150 MHz
  // all-sky Bonn-Parkes, Landecker and Wielebinksi 1970
  if(galplotdef.sync_data___150MHz==1) //AWS20071221                                              
  {
 CRVAL1  =          180.0000000 ;
 CDELT1  =        -0.5000000000 ;
 CRPIX1  =          1.000000000 ;


 CRVAL2  =         -90.00000000 ;
 CDELT2  =         0.5000000000 ;
 CRPIX2  =          1.000000000 ;


 //CRVAL3  =          150000000.0 / frequency (Hz)
 //CDELT3  =          2000.000000 / bandwidth of observation (Hz)


  n_rgrid = data.synchrotron___150MHz.n_rgrid;
  n_zgrid = data.synchrotron___150MHz.n_zgrid;


  intensity=0;
  n_mean   =0; // selected according to coordinates  and checking for      valid data
  n_select =0; // selected according to coordinates independent of whether valid data

  for(  il=0;  il <n_rgrid;   il++)
  for(  ib=0;  ib <n_zgrid;   ib++)
  {
     l=CRVAL1+CDELT1*(il -CRPIX1+1);
     b=CRVAL2+CDELT2*(ib- CRPIX2+1);

     if(l<0.0)l+=360.0;    //AWS20070209
     //   cout<<"  150 MHz il="<<il<<" ib="<<" l="<<l<<" b="<<b<<endl;



    select=0;
     // check whether point is in any of the 4 regions
     if(l>=galplotdef.long_min1&&l<=galplotdef.long_max1 && b>=galplotdef.lat_min1 && b<=galplotdef.lat_max1) select=1;
     if(l>=galplotdef.long_min2&&l<=galplotdef.long_max2 && b>=galplotdef.lat_min1 && b<=galplotdef.lat_max1) select=1;
     if(l>=galplotdef.long_min1&&l<=galplotdef.long_max1 && b>=galplotdef.lat_min2 && b<=galplotdef.lat_max2) select=1;
     if(l>=galplotdef.long_min2&&l<=galplotdef.long_max2 && b>=galplotdef.lat_min2 && b<=galplotdef.lat_max2) select=1;


     if(select==1)
     {
       n_select++;
       //  cout<<"  150 MHz:selected point l = "<<l<<" b = "<<b<<" intensity="<< data.synchrotron___150MHz.d2[il][ib].s[0]<< endl;
       if (data.synchrotron___150MHz.d2[il][ib].s[0]>0.) // excludes also nan's 
       {
       n_mean++;
       intensity+= data.synchrotron___150MHz.d2[il][ib].s[0];
       }
     }
  }
  intensity/=n_mean;
  survey_fraction=double(n_mean)/double(n_select);
  cout<<" 150 MHz number of points in region  n_select="<<n_select<<" of which valid data chosen  n_mean="<<n_mean<<" fraction="<<survey_fraction;
  if (survey_fraction< survey_fraction_min)cout<<" coverage < "<<survey_fraction_min<<" : not plotted !"<<endl;
  if (survey_fraction>=survey_fraction_min)cout<<" coverage > "<<survey_fraction_min<<" :     plotted !"<<endl; 

  nu=  150e6;//   150 MHz
  // units are K
  
  // no zero level correction applied - should check
  
  intensity=intensity/ I_to_Tb  *pow(nu,2);
  if (survey_fraction>=survey_fraction_min)intensity_mean___150MHz=intensity;  //AWS20110622

  cout<<"datapoint nu="<<nu<<" intensity="<<intensity<<endl;

  datapoint=new TGraph(1);
  datapoint->SetPoint(0, nu/1.e6, intensity);

  datapoint->SetMarkerColor(kRed );
  datapoint->SetMarkerStyle(22); // triangles
  datapoint->SetMarkerSize (1.5);//

  if (survey_fraction>=survey_fraction_min)
  datapoint->Draw("P");

  }// if

  // ----------------------------------- 408 MHz
  if(galplotdef.sync_data___408MHz==1) //AWS20071221
  {
  // Galactic coordinates
  // the FITS header is WRONG ! :
  /*
  CRVAL1  =    2.65610840000E+02 ;
  CDELT1  =     -3.515625000E-01 ;
  CRPIX1  =      5.125000000E+02 ;
  
  CRVAL2  =   -9.00000000000E+01 ;
  CDELT2  =      3.515625000E-01 ;
  CRPIX2  =      2.565000000E+02 ;
  */
  // the following is better:
  CRVAL1  =      0.0             ;
  CDELT1  =     -3.515625000E-01 ;
  CRPIX1  =      5.125000000E+02 ;
  
  CRVAL2  =   -9.00000000000E+01 ;
  CDELT2  =      3.515625000E-01 ;
  CRPIX2  =      1.0             ;

  n_rgrid = data.synchrotron___408MHz.n_rgrid;
  n_zgrid = data.synchrotron___408MHz.n_zgrid;

  /*
    loff =    l - CRVAL1;
    boff =    b - CRVAL2;

  il  =int(  loff/CDELT1 + CRPIX1 - 1);
  if( il<0)        il +=n_rgrid;
  if( il>n_rgrid-1)il -=n_rgrid;

  ib  =int(  boff/CDELT2 + CRPIX2 - 1);
  cout<<"408 MHz map: l="<<l<<" b="<<b<<" ra="<<ra<<" dec="<<dec<<" il ="<<il <<" ib  ="<<ib  <<endl;

  if(ib  >=0 && ib  <=n_zgrid-1)
  {  
  // 408 MHz map: 1024*512, GC centred
  //il=512+20;//avoid exact GC peak
  //ib=256;
  datapoint=new TGraph(1);
  nu=408e6;// 408 MHz
  // convert Tb (K) to intensity
  intensity=data.synchrotron___408MHz.d2[il][ib].s[0] / I_to_Tb  *pow(nu,2);

  cout<<"datapoint nu="<<nu<<" intensity="<<intensity<<endl;

  datapoint->SetPoint(0, nu/1.e6, intensity);

  datapoint->SetMarkerColor(kRed );
  datapoint->SetMarkerStyle(22); // triangles
  datapoint->SetMarkerSize (1.5);//


  datapoint->Draw("P");
  }//if

  */

  intensity=0;
  n_mean   =0; // selected according to coordinates  and checking for      valid data
  n_select =0; // selected according to coordinates independent of whether valid data

  for(  il=0;  il <n_rgrid;   il++)
  for(  ib=0;  ib <n_zgrid;   ib++)
  {
     l=CRVAL1+CDELT1*(il -CRPIX1+1);
     b=CRVAL2+CDELT2*(ib- CRPIX2+1);
   

     if(l<0.0)l+=360.0;    //AWS20070209
     //   cout<<"408 MHz il="<<il<<" ib="<<" l="<<l<<" b="<<b<<endl;



    select=0;
     // check whether point is in any of the 4 regions
     if(l>=galplotdef.long_min1&&l<=galplotdef.long_max1 && b>=galplotdef.lat_min1 && b<=galplotdef.lat_max1) select=1;
     if(l>=galplotdef.long_min2&&l<=galplotdef.long_max2 && b>=galplotdef.lat_min1 && b<=galplotdef.lat_max1) select=1;
     if(l>=galplotdef.long_min1&&l<=galplotdef.long_max1 && b>=galplotdef.lat_min2 && b<=galplotdef.lat_max2) select=1;
     if(l>=galplotdef.long_min2&&l<=galplotdef.long_max2 && b>=galplotdef.lat_min2 && b<=galplotdef.lat_max2) select=1;


     if(select==1)
     {
       n_select++;
       //        cout<<"408 MHz:selected point l = "<<l<<" b = "<<b<<" intensity="<< data.synchrotron___408MHz.d2[il][ib].s[0]<< endl;
       if (data.synchrotron___408MHz.d2[il][ib].s[0]>0.) // excludes also nan's 
       {
       n_mean++;
       intensity+= data.synchrotron___408MHz.d2[il][ib].s[0];
       }
     }
  }
  intensity/=n_mean;
  survey_fraction=double(n_mean)/double(n_select);
  cout<<"408 MHz number of points in region  n_select="<<n_select<<" of which valid data chosen  n_mean="<<n_mean<<" fraction="<<survey_fraction;
  if (survey_fraction< survey_fraction_min)cout<<" coverage < "<<survey_fraction_min<<" : not plotted !"<<endl;
  if (survey_fraction>=survey_fraction_min)cout<<" coverage > "<<survey_fraction_min<<" :     plotted !"<<endl; 

  nu=408e6;// 408 MHz
  // Reich&Reich 1988 A&A Supp 74, 7 Table VII: Toff=3.7+-0.85K
  intensity-=3.7;
  // convert Tb (K) to intensity
  intensity=intensity/ I_to_Tb  *pow(nu,2);
  if (survey_fraction>=survey_fraction_min)intensity_mean___408MHz=intensity;  //AWS20110622

  cout<<"datapoint nu="<<nu<<" intensity="<<intensity<<endl;

  datapoint=new TGraph(1);
  datapoint->SetPoint(0, nu/1.e6, intensity);

  datapoint->SetMarkerColor(kRed );
  datapoint->SetMarkerStyle(22); // triangles
  datapoint->SetMarkerSize (1.5);//

  if (survey_fraction>=survey_fraction_min)
  datapoint->Draw("P");

  }// if

  // ------------------------------------  820 MHz
  if(galplotdef.sync_data___820MHz==1) //AWS20071221
  {
  /*
Dwingeloo 25m 820 MHz         Berkhuijsen, E.M.,
                              1972, A&A, Suppl.5, 263-312
Dwingeloo 25m 820 MHz         Tabular Units = KTB(fB) HPBW = 1.2D      >>>>>>>>>>> in fact  in units of 0.1K  comparing with publication!
                              0H<RA<24H,-7D<DEC<+85D  RMS = 1.4 KTB +6%
                                          FullBeam = 3.64E-4 sr

see Reich&Reich 1988 A&A Supp 74, 7,  Table VII: Toff=3.6+-0.15K (used here), cf Lawson et al. 1987 MNRAS 225,307: Toff=4.14K

There may be error in scale for declinations <20 deg = l<60 deg, see Reich&Reich 1988 Section 6.

SIMPLE  =                    T          / STANDARD FITS-FORMAT
BITPIX  =                   16          / BITS PER PIXEL
NAXIS   =                    3          / NUMBER OF AXIS
NAXIS1  =                  721          / X AXIS DIMENSION
NAXIS2  =                  361          / Y AXIS DIMENSION
NAXIS3  =                    1          / # WAVELENGTH
BSCALE  =     -0.0299323549155          / TRUE = [TAPE*BSCALE]+BZERO
BZERO   =                  539          / OFFSET TO THE TRUE PIXEL
BLANK   =               -32000          / DUMMY VALUE FOR EMPTY CELL
CRVAL1  =                    0          / GLON     REF POINT VALUE (DEG)
CRPIX1  =                  361          / GLON     REF POINT PIXEL LOCATION
CDELT1  =                 -0.5          / GLON     INCREMENT ALONG AXIS
CTYPE1  = 'GLON    '                    / GLON     PROJECTION TYPE
IRESOL1 =        1.19999999998          / INITIAL 1/2 POWER LONGITUDE BEAMWIDTH
RESOL1  =        1.19999999998          / CURRENT 1/2 POWER LONGITUDE BEAMWIDTH
CRVAL2  =                    0          / GLAT     REF POINT VALUE (DEG)
CRPIX2  =                  181          / GLAT     REF POINT PIXEL LOCATION
CDELT2  =                  0.5          / GLAT     INCREMENT ALONG AXIS
CTYPE2  = 'GLAT    '                    / GLAT     PROJECTION TYPE
IRESOL2 =        1.19999999998          / INITIAL 1/2 POWER LATITUDE BEAMWIDTH
RESOL2  =        1.19999999998          / CURRENT 1/2 POWER LATITUDE BEAMWIDTH
CRVAL3  =       0.365853658537          / WAVELENGTH IN METERS
CRPIX3  =                    1          /
CDELT3  =                    0          /
CTYPE3  = 'LAMBDA  '                    /
OBSEPOCH=                 1966          / EPOCH OF OBSERVATION
OBSLAT  =        52.8130000001          / LATITUDE OF OBSERVATORY
FREQUENZ=                  820          / FREQUENCY OF OBSERVATION IN MHZ
MATRIX11=     -0.0669887394152          / MATRIX ELEMENT (1,1)
MATRIX12=       0.492728466075          / MATRIX ELEMENT (1,2)
MATRIX13=      -0.867600811151          / MATRIX ELEMENT (1,3)
MATRIX21=      -0.872755765852          / MATRIX ELEMENT (2,1)
MATRIX22=       -0.45034695802          / MATRIX ELEMENT (2,2)
MATRIX23=      -0.188374601723          / MATRIX ELEMENT (2,3)
MATRIX31=      -0.483538914632          / MATRIX ELEMENT (3,1)
MATRIX32=       0.744584633283          / MATRIX ELEMENT (3,2)
MATRIX33=       0.460199784784          / MATRIX ELEMENT (3,3)
COMMENT                                   9 ELEMENT TRANSFORMATION MATRIX
COMMENT                                   FROM THE COORDINATE SYSTEM OF THE MAP
COMMENT                                   TO RA,DEC EPOCH 1950
IDATA   =                    1          / TYPE NUMBER OF DATA
COMMENT                                   SEE NOD2 MANUAL (PAGE A 1.1)
ICODE   =                    0          / COORDINATE CODE
COMMENT                                   SEE NOD2 MANUAL (PAGE B 9.1)
OBJECT  = ' SURVEY.820                                                        '
COMMENT                                   IDENTIFYING TITLE
DATAMAX =             1030.016          / MAX VALUE OF ARRAY
DATAMIN =        49.0130000006          / MIN VALUE OF ARRAY
DATE    = '02/09/07'                    / DATE FITS DATA WRITTEN  (MM/DD/YY)
USER    = 'WWWRUN                                                             '
ORIGIN  = 'MAX-PLANCK-INSTITUT FUER RADIOASTRONOMIE, BONN                     '
END


  */


CRVAL1  =                    0.;        
CRPIX1  =                  361.;      
CDELT1  =                 -0.5 ;      
CRVAL2  =                    0.;     
CRPIX2  =                  181.;        
CDELT2  =                  0.5 ;       


  n_rgrid = data.synchrotron___820MHz.n_rgrid;
  n_zgrid = data.synchrotron___820MHz.n_zgrid;


  intensity=0;
  n_mean   =0; // selected according to coordinates  and checking for      valid data
  n_select =0; // selected according to coordinates independent of whether valid data

  for(  il=0;  il <n_rgrid;   il++)
  for(  ib=0;  ib <n_zgrid;   ib++)
  {
     l=CRVAL1+CDELT1*(il -CRPIX1+1);
     b=CRVAL2+CDELT2*(ib- CRPIX2+1);

     if(l<0.0)l+=360.0;    //AWS20070209
     //   cout<<"  820 MHz il="<<il<<" ib="<<" l="<<l<<" b="<<b<<endl;



    select=0;
     // check whether point is in any of the 4 regions
     if(l>=galplotdef.long_min1&&l<=galplotdef.long_max1 && b>=galplotdef.lat_min1 && b<=galplotdef.lat_max1) select=1;
     if(l>=galplotdef.long_min2&&l<=galplotdef.long_max2 && b>=galplotdef.lat_min1 && b<=galplotdef.lat_max1) select=1;
     if(l>=galplotdef.long_min1&&l<=galplotdef.long_max1 && b>=galplotdef.lat_min2 && b<=galplotdef.lat_max2) select=1;
     if(l>=galplotdef.long_min2&&l<=galplotdef.long_max2 && b>=galplotdef.lat_min2 && b<=galplotdef.lat_max2) select=1;


     if(select==1)
     {
       n_select++;
       //     cout<<"  820 MHz:selected point l = "<<l<<" b = "<<b<<" intensity="<< data.synchrotron___820MHz.d2[il][ib].s[0]<< endl;
       if (data.synchrotron___820MHz.d2[il][ib].s[0]>0.) // excludes also nan's 
       {
       n_mean++;
       intensity+= data.synchrotron___820MHz.d2[il][ib].s[0];
       }
     }
  }
  intensity/=n_mean;
  survey_fraction=double(n_mean)/double(n_select);
  cout<<"820 MHz number of points in region  n_select="<<n_select<<" of which valid data chosen  n_mean="<<n_mean<<" fraction="<<survey_fraction;
  if (survey_fraction< survey_fraction_min)cout<<" coverage < "<<survey_fraction_min<<" : not plotted !"<<endl;
  if (survey_fraction>=survey_fraction_min)cout<<" coverage > "<<survey_fraction_min<<" :     plotted !"<<endl; 

  nu=  820e6;//   820 MHz
  // convert Tb ( K) to intensity
  intensity *= 0.1; // factor from comparison with publication (see above)
  // Reich&Reich 1988 A&A Supp 74, 7 Table VII: Toff=3.6+-0.15K 
  intensity -= 3.6;
  intensity=intensity/ I_to_Tb  *pow(nu,2);
  if (survey_fraction>=survey_fraction_min)intensity_mean___820MHz=intensity;  //AWS20110622
  cout<<"datapoint nu="<<nu<<" intensity="<<intensity<<endl;

  datapoint=new TGraph(1);
  datapoint->SetPoint(0, nu/1.e6, intensity);

  datapoint->SetMarkerColor(kRed );
  datapoint->SetMarkerStyle(22); // triangles
  datapoint->SetMarkerSize (1.5);//

  if (survey_fraction>=survey_fraction_min)
  datapoint->Draw("P");


  }// if


  // -----------------------------------  1420 MHz
  if(galplotdef.sync_data__1420MHz==1) //AWS20071221
  {
  /*


Stockert 25m 21cm             Reich, W.,
                              1981, A&A, Suppl.48, 219-297
                              Reich, P., Reich, W.,
                              1986, A&A.63, 205-
Stockert 25m 21cm             Tabular Units = mKTB(fB)            S/T(fB) = 11.25
                              0H<RA<24H, DEC>-19D     RMS  = 20 mKTB(fB)
                              MainBeam = 1.27E-4 sr,  FullBeam = 1.80E-4 sr
                              HPBW=35.4'
see Reich&Reich 1988 A&A Supp 74, 7 Table VII: Toff=2.8+-0.03K ( big correction)

SIMPLE  =                    T          / STANDARD FITS-FORMAT
BITPIX  =                   16          / BITS PER PIXEL
NAXIS   =                    3          / NUMBER OF AXIS
NAXIS1  =                 1441          / X AXIS DIMENSION
NAXIS2  =                  721          / Y AXIS DIMENSION
NAXIS3  =                    1          / # WAVELENGTH
BSCALE  =       -1.32379325075          / TRUE = [TAPE*BSCALE]+BZERO
BZERO   =                24861          / OFFSET TO THE TRUE PIXEL
BLANK   =               -32000          / DUMMY VALUE FOR EMPTY CELL
CRVAL1  =                    0          / GLON     REF POINT VALUE (DEG)
CRPIX1  =                  721          / GLON     REF POINT PIXEL LOCATION
CDELT1  =                -0.25          / GLON     INCREMENT ALONG AXIS
CTYPE1  = 'GLON    '                    / GLON     PROJECTION TYPE
IRESOL1 =                 0.56          / INITIAL 1/2 POWER LONGITUDE BEAMWIDTH
RESOL1  =                 0.59          / CURRENT 1/2 POWER LONGITUDE BEAMWIDTH
CRVAL2  =                    0          / GLAT     REF POINT VALUE (DEG)
CRPIX2  =                  361          / GLAT     REF POINT PIXEL LOCATION
CDELT2  =                 0.25          / GLAT     INCREMENT ALONG AXIS
CTYPE2  = 'GLAT    '                    / GLAT     PROJECTION TYPE
IRESOL2 =                 0.56          / INITIAL 1/2 POWER LATITUDE BEAMWIDTH
RESOL2  =                 0.59          / CURRENT 1/2 POWER LATITUDE BEAMWIDTH
CRVAL3  =       0.211267605634          / WAVELENGTH IN METERS
CRPIX3  =                    1          /
CDELT3  =                    0          /
CTYPE3  = 'LAMBDA  '                    /
OBSEPOCH=                 1950          / EPOCH OF OBSERVATION
OBSLAT  =               50.571          / LATITUDE OF OBSERVATORY
FREQUENZ=                 1420          / FREQUENCY OF OBSERVATION IN MHZ
MATRIX11=     -0.0669887394152          / MATRIX ELEMENT (1,1)
MATRIX12=       0.492728466075          / MATRIX ELEMENT (1,2)
MATRIX13=      -0.867600811151          / MATRIX ELEMENT (1,3)
MATRIX21=      -0.872755765852          / MATRIX ELEMENT (2,1)
MATRIX22=       -0.45034695802          / MATRIX ELEMENT (2,2)
MATRIX23=      -0.188374601723          / MATRIX ELEMENT (2,3)
MATRIX31=      -0.483538914632          / MATRIX ELEMENT (3,1)
MATRIX32=       0.744584633283          / MATRIX ELEMENT (3,2)
MATRIX33=       0.460199784784          / MATRIX ELEMENT (3,3)
COMMENT                                   9 ELEMENT TRANSFORMATION MATRIX
COMMENT                                   FROM THE COORDINATE SYSTEM OF THE MAP
COMMENT                                   TO RA,DEC EPOCH 1950
IDATA   =                    1          / TYPE NUMBER OF DATA
COMMENT                                   SEE NOD2 MANUAL (PAGE A 1.1)
ICODE   =                    0          / COORDINATE CODE
COMMENT                                   SEE NOD2 MANUAL (PAGE B 9.1)
OBJECT  = ' STOCKERT.21CM                                                     '
COMMENT                                   IDENTIFYING TITLE
DATAMAX =            46554.016          / MAX VALUE OF ARRAY
DATAMIN =             3168.016          / MIN VALUE OF ARRAY
DATE    = '02/08/07'                    / DATE FITS DATA WRITTEN  (MM/DD/YY)
USER    = 'WWWRUN                                                             '
ORIGIN  = 'MAX-PLANCK-INSTITUT FUER RADIOASTRONOMIE, BONN                     '
END
  */
  // units are mK 

  // all-sky Stockert-Villa Eliza has  coordinate keywords equivalent to Stockert northern survey

CRVAL1  =                    0.;         
CDELT1  =                -0.25 ;        
CRPIX1  =                  721.;  
CRVAL2  =                    0.;          
CDELT2  =                 0.25 ;        
CRPIX2  =                  361.;  

  n_rgrid = data.synchrotron__1420MHz.n_rgrid;
  n_zgrid = data.synchrotron__1420MHz.n_zgrid;


  intensity=0;
  n_mean   =0; // selected according to coordinates  and checking for      valid data
  n_select =0; // selected according to coordinates independent of whether valid data

  for(  il=0;  il <n_rgrid;   il++)
  for(  ib=0;  ib <n_zgrid;   ib++)
  {
     l=CRVAL1+CDELT1*(il -CRPIX1+1);
     b=CRVAL2+CDELT2*(ib- CRPIX2+1);

     if(l<0.0)l+=360.0;    //AWS20070209
     //   cout<<" 1420 MHz il="<<il<<" ib="<<" l="<<l<<" b="<<b<<endl;



    select=0;
     // check whether point is in any of the 4 regions
     if(l>=galplotdef.long_min1&&l<=galplotdef.long_max1 && b>=galplotdef.lat_min1 && b<=galplotdef.lat_max1) select=1;
     if(l>=galplotdef.long_min2&&l<=galplotdef.long_max2 && b>=galplotdef.lat_min1 && b<=galplotdef.lat_max1) select=1;
     if(l>=galplotdef.long_min1&&l<=galplotdef.long_max1 && b>=galplotdef.lat_min2 && b<=galplotdef.lat_max2) select=1;
     if(l>=galplotdef.long_min2&&l<=galplotdef.long_max2 && b>=galplotdef.lat_min2 && b<=galplotdef.lat_max2) select=1;


     if(select==1)
     {
       n_select++;
       //  cout<<" 1420 MHz:selected point l = "<<l<<" b = "<<b<<" intensity="<< data.synchrotron__1420MHz.d2[il][ib].s[0]<< endl;
       if (data.synchrotron__1420MHz.d2[il][ib].s[0]>0.) // excludes also nan's 
       {
       n_mean++;
       intensity+= data.synchrotron__1420MHz.d2[il][ib].s[0];
       }
     }
  }
  intensity/=n_mean;
  survey_fraction=double(n_mean)/double(n_select);
  cout<<"1420 MHz number of points in region  n_select="<<n_select<<" of which valid data chosen  n_mean="<<n_mean<<" fraction="<<survey_fraction;
  if (survey_fraction< survey_fraction_min)cout<<" coverage < "<<survey_fraction_min<<" : not plotted !"<<endl;
  if (survey_fraction>=survey_fraction_min)cout<<" coverage > "<<survey_fraction_min<<" :     plotted !"<<endl; 

  nu= 1420e6;//  1420 MHz
  // convert Tb (mK) to intensity
  intensity     *=1.0e-3;
  // Reich&Reich 1988 A&A Supp 74, 7 Table VII: Toff=2.8+-0.03K 
  intensity -= 2.8;
  intensity=intensity/ I_to_Tb  *pow(nu,2);
  if (survey_fraction>=survey_fraction_min)intensity_mean__1420MHz=intensity;  //AWS20110622

  cout<<"datapoint nu="<<nu<<" intensity="<<intensity<<endl;

  datapoint=new TGraph(1);
  datapoint->SetPoint(0, nu/1.e6, intensity);

  datapoint->SetMarkerColor(kRed );
  datapoint->SetMarkerStyle(22); // triangles
  datapoint->SetMarkerSize (1.5);//

  if (survey_fraction>=survey_fraction_min)
  datapoint->Draw("P");

  }// if

  // -----------------------------------  2326 MHz
if(galplotdef.sync_data__2326MHz==1) //AWS20071221
{
  // Rhodes HartRAO 2326 MHz survey. Jonas et al. 1998 MNRAS 297, 977
  // see Giardino et al. 2001 A&A   371, 708  for detailed analysis of spectral index and other useful information
  /*
NAXIS   =                    2 / number of data axes
NAXIS1  =                  720 / length of data axis 1
NAXIS2  =                  360 / length of data axis 2
EXTEND  =                    T / FITS dataset may contain extensions
COMMENT   FITS (Flexible Image Transport System) format is defined in 'Astronomy
COMMENT   and Astrophysics', volume 376, page 359; bibcode: 2001A&A...376..359H
CRVAL1  =                 0.25 / Start of axis 1
CRVAL2  =               -89.75 / Start of axis 2
CRPIX1  =                 360. / Reference pixel of axis 1
CRPIX2  =                   1. / Reference pixel of axis 1
CDELT1  =                 -0.5 / Increment of axis 1
CDELT2  =                  0.5 / Increment of axis 2
COMMENT Rhodes HartRAO 2326 MHz survey. Jonas et al. 1998 MNRAS 297, 977
COMMENT   Created by A. Strong on 19 Feb 2007 from HEALPix Rhodes2326.fits
COMMENT using Rhodes2326_convert.cc
COMMENT HEALPix version kindly provided by J. Jonas
  */
CRVAL1  =                 0.25 ;         
CDELT1  =                -0.50 ;        
CRPIX1  =                  360.;  
CRVAL2  =               -89.75 ;          
CDELT2  =                 0.50 ;        
CRPIX2  =                    1.;  
  

  n_rgrid = data.synchrotron__2326MHz.n_rgrid;
  n_zgrid = data.synchrotron__2326MHz.n_zgrid;


  intensity=0;
  n_mean   =0; // selected according to coordinates  and checking for      valid data
  n_select =0; // selected according to coordinates independent of whether valid data

  for(  il=0;  il <n_rgrid;   il++)
  for(  ib=0;  ib <n_zgrid;   ib++)
  {
     l=CRVAL1+CDELT1*(il -CRPIX1+1);
     b=CRVAL2+CDELT2*(ib- CRPIX2+1);

     if(l<0.0)l+=360.0;    //AWS20070209
     //   cout<<" 2326 MHz il="<<il<<" ib="<<" l="<<l<<" b="<<b<<endl;



    select=0;
     // check whether point is in any of the 4 regions
     if(l>=galplotdef.long_min1&&l<=galplotdef.long_max1 && b>=galplotdef.lat_min1 && b<=galplotdef.lat_max1) select=1;
     if(l>=galplotdef.long_min2&&l<=galplotdef.long_max2 && b>=galplotdef.lat_min1 && b<=galplotdef.lat_max1) select=1;
     if(l>=galplotdef.long_min1&&l<=galplotdef.long_max1 && b>=galplotdef.lat_min2 && b<=galplotdef.lat_max2) select=1;
     if(l>=galplotdef.long_min2&&l<=galplotdef.long_max2 && b>=galplotdef.lat_min2 && b<=galplotdef.lat_max2) select=1;


     if(select==1)
     {
       n_select++;
       //  cout<<" 2326 MHz:selected point l = "<<l<<" b = "<<b<<" intensity="<< data.synchrotron__2326MHz.d2[il][ib].s[0]<< endl;
       if (data.synchrotron__2326MHz.d2[il][ib].s[0]>0.) // excludes also nan's 
       {
       n_mean++;
       intensity+= data.synchrotron__2326MHz.d2[il][ib].s[0];
       }
     }
  }
  intensity/=n_mean;
  survey_fraction=double(n_mean)/double(n_select);
  cout<<"2326 MHz number of points in region  n_select="<<n_select<<" of which valid data chosen  n_mean="<<n_mean<<" fraction="<<survey_fraction;
  if (survey_fraction< survey_fraction_min)cout<<" coverage < "<<survey_fraction_min<<" : not plotted !"<<endl;
  if (survey_fraction>=survey_fraction_min)cout<<" coverage > "<<survey_fraction_min<<" :     plotted !"<<endl; 
  
  nu= 2326e6;//  2326 MHz
  
  // 
  intensity -= 0; // have to find offset. CMB is 2.725K:  must have been subtracted since values start around 0.1K.   EGB~0.03K 
  intensity=intensity/ I_to_Tb  *pow(nu,2);
  if (survey_fraction>=survey_fraction_min)intensity_mean__2326MHz=intensity;  //AWS20110622
  cout<<"datapoint nu="<<nu<<" intensity="<<intensity<<endl;

  datapoint=new TGraph(1);
  datapoint->SetPoint(0, nu/1.e6, intensity);

  datapoint->SetMarkerColor(kRed );
  datapoint->SetMarkerStyle(22); // triangles
  datapoint->SetMarkerSize (1.5);//

  if (survey_fraction>=survey_fraction_min)
  datapoint->Draw("P");

}//if

  // ----------------------------------- 22800 MHz



 // 22800 MHz 3yr WMAP map: 720*360, GC centred    AWS20070223

 // Data selected in read_synchrotron_data.cc galplotdef.sync_data_WMAP: 0=WMAP 3yr synch template, 1-4=Miville-Deschenes based.
 
  CRVAL1  =                 0.25; 
  CRVAL2  =               -89.75; 
  CRPIX1  =                 360.; 
  CRPIX2  =                   1.; 
  CDELT1  =                 -0.5; 
  CDELT2  =                  0.5; 

  n_rgrid = data.synchrotron_22800MHz.n_rgrid;
  n_zgrid = data.synchrotron_22800MHz.n_zgrid;



  int   nnu_WMAP=5, inu_WMAP;
  double nu_WMAP[nnu_WMAP];
  nu_WMAP[0]=22800.e6;
  nu_WMAP[1]=31000.e6;
  nu_WMAP[2]=41000.e6;
  nu_WMAP[3]=61000.e6;
  nu_WMAP[4]=94000.e6;

 for (inu_WMAP=0;inu_WMAP<nnu_WMAP;inu_WMAP++)
  {
   if(   (inu_WMAP==0 && galplotdef.sync_data_22800MHz==1) //AWS20071221
      || (inu_WMAP==1 && galplotdef.sync_data_33000MHz==1)
      || (inu_WMAP==2 && galplotdef.sync_data_41000MHz==1)
      || (inu_WMAP==3 && galplotdef.sync_data_61000MHz==1)
      || (inu_WMAP==4 && galplotdef.sync_data_94000MHz==1))

{
  intensity=0;
  n_mean   =0; // selected according to coordinates  and checking for      valid data
  n_select =0; // selected according to coordinates independent of whether valid data

  for(  il=0;  il <n_rgrid;   il++)
  for(  ib=0;  ib <n_zgrid;   ib++)
  {
     l=CRVAL1+CDELT1*(il -CRPIX1+1);
     b=CRVAL2+CDELT2*(ib- CRPIX2+1);
   
     if(l<0.0)l+=360.0;    //AWS20070209
   //cout<<"22800 MHz il="<<il<<" ib="<<" l="<<l<<" b="<<b<<endl;



    select=0;
     // check whether point is in any of the 4 regions
     if(l>=galplotdef.long_min1&&l<=galplotdef.long_max1 && b>=galplotdef.lat_min1 && b<=galplotdef.lat_max1) select=1;
     if(l>=galplotdef.long_min2&&l<=galplotdef.long_max2 && b>=galplotdef.lat_min1 && b<=galplotdef.lat_max1) select=1;
     if(l>=galplotdef.long_min1&&l<=galplotdef.long_max1 && b>=galplotdef.lat_min2 && b<=galplotdef.lat_max2) select=1;
     if(l>=galplotdef.long_min2&&l<=galplotdef.long_max2 && b>=galplotdef.lat_min2 && b<=galplotdef.lat_max2) select=1;


     if(select==1)
     {
       n_select++;
       //  cout<<"22800 MHz:selected point l = "<<l<<" b = "<<b<<" intensity="<< data.synchrotron_22800MHz.d2[il][ib].s[0]<< endl;
       if (data.synchrotron_22800MHz.d2[il][ib].s[0]>0.) // excludes also nan's 
       {
       n_mean++;
       if(inu_WMAP==0)intensity+= data.synchrotron_22800MHz.d2[il][ib].s[0];
       if(inu_WMAP==1)intensity+= data.synchrotron_33000MHz.d2[il][ib].s[0];
       if(inu_WMAP==2)intensity+= data.synchrotron_41000MHz.d2[il][ib].s[0];
       if(inu_WMAP==3)intensity+= data.synchrotron_61000MHz.d2[il][ib].s[0];
       if(inu_WMAP==4)intensity+= data.synchrotron_94000MHz.d2[il][ib].s[0];
       }
     }
  }
  intensity/=n_mean;
  survey_fraction=double(n_mean)/double(n_select);
  cout<<nu_WMAP[inu_WMAP]<<" MHz number of points in region  n_select="<<n_select<<" of which valid data chosen  n_mean="<<n_mean<<" fraction="<<survey_fraction;
  if (survey_fraction< survey_fraction_min)cout<<" coverage < "<<survey_fraction_min<<" : not plotted !"<<endl;
  if (survey_fraction>=survey_fraction_min)cout<<" coverage > "<<survey_fraction_min<<" :     plotted !"<<endl; 
 
 
  nu=nu_WMAP[inu_WMAP];
  // convert Tb (mK) to intensity
  intensity     *=1.0e-3;
  intensity=intensity/ I_to_Tb  *pow(nu,2);

  cout<<"WMAP datapoint from l,b (Miville or 7yr template): nu="<<nu<<" intensity="<<intensity<<endl;

  datapoint=new TGraph(1);
  datapoint->SetPoint(0, nu/1.e6, intensity);

  datapoint->SetMarkerColor(kRed );
  datapoint->SetMarkerStyle(22); // triangles
  datapoint->SetMarkerSize (1.5);//

  if (survey_fraction>=survey_fraction_min)
  datapoint->Draw("P");

}// if

  }// for inu_WMAP




// WMAP 7-year templates using original Healpix AWS20111223

  double intensity_mean_22800MHz = 0.;
  double intensity_mean_33000MHz = 0.;
  double intensity_mean_41000MHz = 0.;
  double intensity_mean_61000MHz = 0.;
  double intensity_mean_94000MHz = 0.;

  double rtd=180./pi;        // rad to deg

  n_mean=0;

cout<<" data.synchrotron_WMAP_MEM_22800MHz.Npix()="<< data.synchrotron_WMAP_MEM_22800MHz.Npix() <<endl;

 for (int ipix=0;ipix<  data.synchrotron_WMAP_MEM_22800MHz.Npix() ;ipix++)
   {
    l =        data.synchrotron_WMAP_MEM_22800MHz.pix2ang(ipix).phi   * rtd;
    b = 90.0 - data.synchrotron_WMAP_MEM_22800MHz.pix2ang(ipix).theta * rtd;

    if(galplotdef.verbose==-2000) //AWS20140110
     {
       cout<<"ipix="<<ipix<<" l = "<<l<<" b ="<<b
           <<"  data.synchrotron_WMAP_MEM_22800MHz  [ipix]  = "<< data.synchrotron_WMAP_MEM_22800MHz  [ipix]<<endl;
     }

     select=0;
    // select if pixel lies in one of the 4 regions
    if(l>=galplotdef.long_min1 && l<=galplotdef.long_max1 && b>=galplotdef.lat_min1 && b<=galplotdef.lat_max1 ) select=1;
    if(l>=galplotdef.long_min2 && l<=galplotdef.long_max2 && b>=galplotdef.lat_min1 && b<=galplotdef.lat_max1 ) select=1;
    if(l>=galplotdef.long_min1 && l<=galplotdef.long_max1 && b>=galplotdef.lat_min2 && b<=galplotdef.lat_max2 ) select=1;
    if(l>=galplotdef.long_min2 && l<=galplotdef.long_max2 && b>=galplotdef.lat_min2 && b<=galplotdef.lat_max2 ) select=1;

    if (select==1)
    {
      n_mean++;
     
     


       intensity_mean_22800MHz     +=  data.synchrotron_WMAP_MEM_22800MHz  [ipix];
       intensity_mean_33000MHz     +=  data.synchrotron_WMAP_MEM_33000MHz  [ipix];
       intensity_mean_41000MHz     +=  data.synchrotron_WMAP_MEM_41000MHz  [ipix];
       intensity_mean_61000MHz     +=  data.synchrotron_WMAP_MEM_61000MHz  [ipix];
       intensity_mean_94000MHz     +=  data.synchrotron_WMAP_MEM_94000MHz  [ipix];

    
     } //if select
    } //ipix

  
  cout<<"WMAP 7 year synchrotron template data: n_mean="<<n_mean<<endl;

   intensity_mean_22800MHz /= n_mean;
   intensity_mean_33000MHz /= n_mean;
   intensity_mean_41000MHz /= n_mean;
   intensity_mean_61000MHz /= n_mean;
   intensity_mean_94000MHz /= n_mean;

 if(galplotdef.sync_data_options==1)
 {

 for (inu_WMAP=0;inu_WMAP<nnu_WMAP;inu_WMAP++)
  {
  nu=nu_WMAP[inu_WMAP];

  if(inu_WMAP==0)intensity= intensity_mean_22800MHz;
  if(inu_WMAP==1)intensity= intensity_mean_33000MHz;
  if(inu_WMAP==2)intensity= intensity_mean_41000MHz;
  if(inu_WMAP==3)intensity= intensity_mean_61000MHz;
  if(inu_WMAP==4)intensity= intensity_mean_94000MHz;

  // convert Tb (mK) to intensity
  intensity     *=1.0e-3;
  intensity=intensity/ I_to_Tb  *pow(nu,2);

  cout<<"WMAP datapoint from healpix: nu="<<nu<<" WMAP 7 year synchrotron template intensity="<<intensity<<endl;

  datapoint=new TGraph(1);
  datapoint->SetPoint(0, nu/1.e6, intensity);

  datapoint->SetMarkerColor(kBlue );
  datapoint->SetMarkerStyle(22); // triangles
  datapoint->SetMarkerSize (1.0);//from 1.5 to distinguish at same frequency AWS20120105

 
  datapoint->Draw("P");
  }


 } // if synch_data_options==1


// finish this plot

//----------------------------------------- text

  strcpy(canvastitle," galdef ID ");
  strcat(canvastitle,galdef.galdef_ID);


  sprintf(workstring1,"  %5.2f<l<%5.2f , %5.2f<l<%5.2f",               
                          galplotdef.long_min1,galplotdef.long_max1,
                          galplotdef.long_min2,galplotdef.long_max2);

  sprintf(workstring2,"  %5.2f<b<%5.2f , %5.2f<b<%5.2f",                 
                         galplotdef.lat_min1,galplotdef.lat_max1,
                         galplotdef.lat_min2,galplotdef.lat_max2);




  text=new TText();
  text->SetTextFont(62);
  text->SetTextColor(kBlack);
  text->SetTextSize(0.034 );                                         
  text->SetTextAlign(12);

  text->DrawTextNDC(.10 ,.93 ,canvastitle);// NDC=normalized coord system   
  text->SetTextSize(0.022 );                                            
  text->DrawTextNDC(.58 ,.88 ,workstring1);// NDC=normalized coord system  
  text->DrawTextNDC(.58 ,.86 ,workstring2);// NDC=normalized coord system 
  
  text->SetTextSize(0.040 ); //AWS20121130
  text->DrawTextNDC(.13 ,.88 ,"synchrotron" );// NDC=normalized coord system AWS20121130 was .20,.86

 //============== postscript, gif and txt files

  sprintf(workstring1,"l_%.2f_%.2f_%.2f_%.2f",                          
                          galplotdef.long_min1,galplotdef.long_max1,
                          galplotdef.long_min2,galplotdef.long_max2);

  sprintf(workstring2,"b_%.2f_%.2f_%.2f_%.2f",                          
                          galplotdef.lat_min1,galplotdef.lat_max1,
                          galplotdef.lat_min2,galplotdef.lat_max2);

  strcpy(psfile,"plots/");
  strcat(psfile,"synchrotron_spectrum_");
  strcat(psfile,galdef.galdef_ID);
  strcat(psfile,"_"        );
  strcat(psfile,workstring1);
  strcat(psfile,"_"        );
  strcat(psfile,workstring2);
  strcat(psfile,"_"        );
  strcat(psfile,galplotdef.psfile_tag);
  strcpy(giffile,psfile);

 if(galplotdef.output_format==1 || galplotdef.output_format==3)
   {
   strcat(psfile,".eps");
   cout<<"postscript file="<<psfile<<endl;
   c1->Print(psfile,"eps" );
  }

  if(galplotdef.output_format==2 || galplotdef.output_format==3)
   {
   strcat(giffile,".gif");
   cout<<"       gif file="<<giffile<<endl;
   c1->Print(giffile,"gif" );
  }


  ///////////////////////////////////////
  // plot polarized spectra              AWS20110412
  /////////////////////////////////////

// WMAP 7 year data
  double intensity_Q_mean_22800MHz = 0.;
  double intensity_Q_mean_33000MHz = 0.;
  double intensity_Q_mean_41000MHz = 0.;
  double intensity_Q_mean_61000MHz = 0.;
  double intensity_Q_mean_94000MHz = 0.;

  double intensity_U_mean_22800MHz = 0.;
  double intensity_U_mean_33000MHz = 0.;
  double intensity_U_mean_41000MHz = 0.;
  double intensity_U_mean_61000MHz = 0.;
  double intensity_U_mean_94000MHz = 0.;

  double intensity_P_mean_22800MHz = 0.;
  double intensity_P_mean_33000MHz = 0.;
  double intensity_P_mean_41000MHz = 0.;
  double intensity_P_mean_61000MHz = 0.;
  double intensity_P_mean_94000MHz = 0.;

  double intensity_I_mean_22800MHz = 0.;
  double intensity_I_mean_33000MHz = 0.;
  double intensity_I_mean_41000MHz = 0.;
  double intensity_I_mean_61000MHz = 0.;
  double intensity_I_mean_94000MHz = 0.;

         rtd=180./pi;        // rad to deg

  n_mean=0;

cout<<" data.synchrotron_WMAP_P_22800MHz.Npix()="<< data.synchrotron_WMAP_P_22800MHz.Npix() <<endl;

 for (int ipix=0;ipix<  data.synchrotron_WMAP_P_22800MHz.Npix() ;ipix++)
   {
    l =        data.synchrotron_WMAP_P_22800MHz.pix2ang(ipix).phi   * rtd;
    b = 90.0 - data.synchrotron_WMAP_P_22800MHz.pix2ang(ipix).theta * rtd;

    if(galplotdef.verbose==-2000)//AWS20140110
     {
       cout<<"ipix="<<ipix<<" l = "<<l<<" b ="<<b
           <<"  data.synchrotron_WMAP_P_22800MHz  [ipix]  = "<< data.synchrotron_WMAP_P_22800MHz  [ipix]<<endl;
     }

     select=0;
    // select if pixel lies in one of the 4 regions
    if(l>=galplotdef.long_min1 && l<=galplotdef.long_max1 && b>=galplotdef.lat_min1 && b<=galplotdef.lat_max1 ) select=1;
    if(l>=galplotdef.long_min2 && l<=galplotdef.long_max2 && b>=galplotdef.lat_min1 && b<=galplotdef.lat_max1 ) select=1;
    if(l>=galplotdef.long_min1 && l<=galplotdef.long_max1 && b>=galplotdef.lat_min2 && b<=galplotdef.lat_max2 ) select=1;
    if(l>=galplotdef.long_min2 && l<=galplotdef.long_max2 && b>=galplotdef.lat_min2 && b<=galplotdef.lat_max2 ) select=1;

    if (select==1)
    {
      n_mean++;
     
     
       intensity_Q_mean_22800MHz     +=  data.synchrotron_WMAP_Q_22800MHz  [ipix];
       intensity_Q_mean_33000MHz     +=  data.synchrotron_WMAP_Q_33000MHz  [ipix];
       intensity_Q_mean_41000MHz     +=  data.synchrotron_WMAP_Q_41000MHz  [ipix];
       intensity_Q_mean_61000MHz     +=  data.synchrotron_WMAP_Q_61000MHz  [ipix];
       intensity_Q_mean_94000MHz     +=  data.synchrotron_WMAP_Q_94000MHz  [ipix];

     
       intensity_U_mean_22800MHz     +=  data.synchrotron_WMAP_U_22800MHz  [ipix];
       intensity_U_mean_33000MHz     +=  data.synchrotron_WMAP_U_33000MHz  [ipix];
       intensity_U_mean_41000MHz     +=  data.synchrotron_WMAP_U_41000MHz  [ipix];
       intensity_U_mean_61000MHz     +=  data.synchrotron_WMAP_U_61000MHz  [ipix];
       intensity_U_mean_94000MHz     +=  data.synchrotron_WMAP_U_94000MHz  [ipix];

       intensity_P_mean_22800MHz     +=  data.synchrotron_WMAP_P_22800MHz  [ipix];
       intensity_P_mean_33000MHz     +=  data.synchrotron_WMAP_P_33000MHz  [ipix];
       intensity_P_mean_41000MHz     +=  data.synchrotron_WMAP_P_41000MHz  [ipix];
       intensity_P_mean_61000MHz     +=  data.synchrotron_WMAP_P_61000MHz  [ipix];
       intensity_P_mean_94000MHz     +=  data.synchrotron_WMAP_P_94000MHz  [ipix];

       intensity_I_mean_22800MHz     +=  data.synchrotron_WMAP_I_22800MHz  [ipix];
       intensity_I_mean_33000MHz     +=  data.synchrotron_WMAP_I_33000MHz  [ipix];
       intensity_I_mean_41000MHz     +=  data.synchrotron_WMAP_I_41000MHz  [ipix];
       intensity_I_mean_61000MHz     +=  data.synchrotron_WMAP_I_61000MHz  [ipix];
       intensity_I_mean_94000MHz     +=  data.synchrotron_WMAP_I_94000MHz  [ipix];

    
     } //if select
    } //ipix

  
  cout<<"WMAP 7 year data: n_mean="<<n_mean<<endl;

   intensity_Q_mean_22800MHz /= n_mean;
   intensity_Q_mean_33000MHz /= n_mean;
   intensity_Q_mean_41000MHz /= n_mean;
   intensity_Q_mean_61000MHz /= n_mean;
   intensity_Q_mean_94000MHz /= n_mean;

   intensity_U_mean_22800MHz /= n_mean;
   intensity_U_mean_33000MHz /= n_mean;
   intensity_U_mean_41000MHz /= n_mean;
   intensity_U_mean_61000MHz /= n_mean;
   intensity_U_mean_94000MHz /= n_mean;

   intensity_P_mean_22800MHz /= n_mean;
   intensity_P_mean_33000MHz /= n_mean;
   intensity_P_mean_41000MHz /= n_mean;
   intensity_P_mean_61000MHz /= n_mean;
   intensity_P_mean_94000MHz /= n_mean;

   intensity_I_mean_22800MHz /= n_mean;
   intensity_I_mean_33000MHz /= n_mean;
   intensity_I_mean_41000MHz /= n_mean;
   intensity_I_mean_61000MHz /= n_mean;
   intensity_I_mean_94000MHz /= n_mean;


  for (int icase=1;icase<=4;icase++) // 1=Q 2=U 3=P 4=I
  {
 
 //====== see HowTo Style: and do before creating canvas


   
   plain->SetCanvasBorderMode(0);
   plain->SetPadBorderMode(0);
   plain->SetPadColor(0);
   plain->SetCanvasColor(0);
   plain->SetTitleColor(1,"X"); //AWS20080125
   plain->SetTitleColor(1,"Y"); //AWS20080125
   plain->SetStatColor(0);
   gROOT->SetStyle("Plain");


   if(icase==1)strcpy(name,"synch_Q_spectrum");// to identify it: name is not plotted
   if(icase==2)strcpy(name,"synch_U_spectrum");// to identify it: name is not plotted
   if(icase==3)strcpy(name,"synch_P_spectrum");// to identify it: name is not plotted
   if(icase==4)strcpy(name,"synch_I_spectrum");// to identify it: name is not plotted


  c1=new TCanvas(name,name,200,100,650,650);

  c1  ->SetLogx(1);
  c1  ->SetLogy(1);


spectrum      =new TGraph(galaxy.n_nu_synchgrid);
spectral_index=new TGraph(galaxy.n_nu_synchgrid);

spectrum_free_free                 =new TGraph(galaxy.n_nu_synchgrid);//AWS20110622
spectrum_sync_free_free            =new TGraph(galaxy.n_nu_synchgrid);//AWS20110622
spectrum_dust                      =new TGraph(galaxy.n_nu_synchgrid);//AWS20111222
spectrum_dust_Q                    =new TGraph(galaxy.n_nu_synchgrid);//AWS20120117
spectrum_dust_U                    =new TGraph(galaxy.n_nu_synchgrid);//AWS20120117
spectrum_dust_P                    =new TGraph(galaxy.n_nu_synchgrid);//AWS20120117
spectrum_sync_dust_Q               =new TGraph(galaxy.n_nu_synchgrid);//AWS20120219
spectrum_sync_dust_U               =new TGraph(galaxy.n_nu_synchgrid);//AWS20120219
spectrum_sync_dust_P               =new TGraph(galaxy.n_nu_synchgrid);//AWS20120219

spectrum_sync_free_free_dust       =new TGraph(galaxy.n_nu_synchgrid);//AWS20111222
spectrum_synchrotron_template      =new TGraph(galaxy.n_nu_synchgrid);//AWS20111222



  for  (ip    =0;  ip    <galaxy.n_nu_synchgrid; ip++)
  {
    if(icase==1) spectrum->      SetPoint(ip,galaxy.nu_synch[ip]/1e6,     abs(intensity_Q_mean[ip])); // to avoid -ve values on spectra
    if(icase==2) spectrum->      SetPoint(ip,galaxy.nu_synch[ip]/1e6,     abs(intensity_U_mean[ip])); // to avoid -ve values on spectra
    if(icase==3) spectrum->      SetPoint(ip,galaxy.nu_synch[ip]/1e6,         intensity_P_mean[ip]);
    if(icase==4) spectrum->      SetPoint(ip,galaxy.nu_synch[ip]/1e6,           intensity_mean[ip]);

       spectrum_free_free->      SetPoint(ip,galaxy.nu_synch[ip]/1e6,                             intensity_free_free_mean[ip]); //AWS20110622
       spectrum_sync_free_free-> SetPoint(ip,galaxy.nu_synch[ip]/1e6,      intensity_mean[ip] +   intensity_free_free_mean[ip]); //AWS20110622

       spectrum_dust          -> SetPoint(ip,galaxy.nu_synch[ip]/1e6,     intensity_dust_mean  [ip] ); //AWS20111222
       spectrum_dust_Q        -> SetPoint(ip,galaxy.nu_synch[ip]/1e6, abs(intensity_dust_Q_mean[ip])); //AWS20120117
       spectrum_dust_U        -> SetPoint(ip,galaxy.nu_synch[ip]/1e6, abs(intensity_dust_U_mean[ip])); //AWS20120117
       spectrum_dust_P        -> SetPoint(ip,galaxy.nu_synch[ip]/1e6,     intensity_dust_P_mean[ip] ); //AWS20120117

       spectrum_sync_dust_Q   -> SetPoint(ip,galaxy.nu_synch[ip]/1e6, abs(intensity_Q_mean[ip]+intensity_dust_Q_mean[ip])); //AWS20120117
       spectrum_sync_dust_U   -> SetPoint(ip,galaxy.nu_synch[ip]/1e6, abs(intensity_U_mean[ip]+intensity_dust_U_mean[ip])); //AWS20120117

       // replaced this by correct mean
       //       spectrum_sync_dust_P   -> SetPoint(ip,galaxy.nu_synch[ip]/1e6,
       //                                                          sqrt(pow(intensity_Q_mean[ip]+intensity_dust_Q_mean[ip],2)
       //                                                            +  pow(intensity_U_mean[ip]+intensity_dust_U_mean[ip],2)      )); //AWS20120117

       spectrum_sync_dust_P   -> SetPoint(ip,galaxy.nu_synch[ip]/1e6,  intensity_sync_dust_P_mean[ip] ); //AWS20120727

       spectrum_sync_free_free_dust-> 
                                 SetPoint(ip,galaxy.nu_synch[ip]/1e6,      intensity_mean[ip] +   intensity_free_free_mean[ip] +  intensity_dust_mean[ip]); //AWS20111222


       spectrum_synchrotron_template->   
                                 SetPoint(ip,galaxy.nu_synch[ip]/1e6, intensity_synchrotron_template_mean[ip]);//AWS20111222


  }
  


for  (ip    =0;  ip    <galaxy.n_nu_synchgrid; ip++)
     spectral_index->SetPoint(ip,galaxy.nu_synch[ip]/1e6,spectral_index_mean[ip]);

  

//Gtypes.h:enum EColor { kWhite, kBlack, kRed, kGreen, kBlue, kYellow, kMagenta, kCyan };
  spectrum->SetMarkerColor(kBlue );
  spectrum->SetMarkerStyle(21);
  spectrum->SetMarkerStyle(22); // triangles
  spectrum->SetMarkerSize (0.5);//
  spectrum->SetLineColor(kBlue );
  spectrum->SetLineWidth(2     );
  spectrum->SetLineStyle(1      );//1=solid 2=dash 3=dot 4=dash-dot

  spectrum->SetTitle(""         ); // removes text written in box on plot
  spectrum->GetHistogram()->SetXTitle("frequency, MHz                 ");

  spectrum->GetHistogram()->SetYTitle("intensity / erg cm^{-2} sr^{-1} s^{-1} Hz^{-1}       "); //needs {} to work AWS20121130 added spaces to not conflict with labels

  spectrum->GetHistogram()->SetLabelOffset(+0.00,"X"); //  +ve down
  spectrum->GetHistogram()->SetLabelSize  ( 0.03,"Y"); // labels, not titles


  spectrum->GetHistogram()->SetTitleOffset(+1.15,"X");  //  +ve down    //AWS20121130    was 1.20
  spectrum->GetHistogram()->SetTitleOffset(+1.22,"Y");  //  +ve to left //AWS20121130    was 1.46
  spectrum->GetHistogram()->SetTitleSize   (0.040,"X");//AWS20121130 was 0.035
  spectrum->GetHistogram()->SetTitleSize   (0.040,"Y");//AWS20121130 was 0.035  

  if(icase==1){spectrum->SetMinimum(1.0e-24);  spectrum->SetMaximum(1.0e-16);}// Q   AWS20110907
  if(icase==2){spectrum->SetMinimum(1.0e-25);  spectrum->SetMaximum(1.0e-16);}// U   AWS20110907
  if(icase==3){spectrum->SetMinimum(1.0e-23);  spectrum->SetMaximum(1.0e-16);}// P   AWS20110907
  if(icase==4){spectrum->SetMinimum(1.0e-22);  spectrum->SetMaximum(1.0e-16);}// I   AWS20110907

  spectrum->Draw("AL");       // axes  lines   synchrotron model, Q, U, P or I according to icase

  if(icase==1) //AWS20120117
  {
  // dust Q
   spectrum_dust_Q   ->SetLineColor(kMagenta); //AWS20111222
   spectrum_dust_Q   ->SetLineWidth(2     );   //AWS20111222
   spectrum_dust_Q   ->SetLineStyle(1      );  //1=solid 2=dash 3=dot 4=dash-dot  //AWS20111222
   spectrum_dust_Q   ->Draw("L");              //AWS20111222

   spectrum_sync_dust_Q   ->SetLineColor(kCyan   ); //AWS20111222
   spectrum_sync_dust_Q   ->SetLineWidth(2     );   //AWS20111222
   spectrum_sync_dust_Q   ->SetLineStyle(1      );  //1=solid 2=dash 3=dot 4=dash-dot  //AWS20111222
   spectrum_sync_dust_Q   ->Draw("L");              //AWS20111222


  }

  if(icase==2) //AWS20120117
  {
  // dust U
   spectrum_dust_U   ->SetLineColor(kMagenta); //AWS20111222
   spectrum_dust_U   ->SetLineWidth(2     );   //AWS20111222
   spectrum_dust_U   ->SetLineStyle(1      );  //1=solid 2=dash 3=dot 4=dash-dot  //AWS20111222
   spectrum_dust_U   ->Draw("L");              //AWS20111222

   spectrum_sync_dust_U   ->SetLineColor(kCyan   ); //AWS20111222
   spectrum_sync_dust_U   ->SetLineWidth(2     );   //AWS20111222
   spectrum_sync_dust_U   ->SetLineStyle(1      );  //1=solid 2=dash 3=dot 4=dash-dot  //AWS20111222
   spectrum_sync_dust_U   ->Draw("L");              //AWS20111222
  }

  if(icase==3) //AWS20120117
  {
  // dust P
   spectrum_dust_P   ->SetLineColor(kMagenta); //AWS20111222
   spectrum_dust_P   ->SetLineWidth(2     );   //AWS20111222
   spectrum_dust_P   ->SetLineStyle(1      );  //1=solid 2=dash 3=dot 4=dash-dot  //AWS20111222
   spectrum_dust_P   ->Draw("L");              //AWS20111222


   spectrum_sync_dust_P   ->SetLineColor(kCyan   ); //AWS20111222
   spectrum_sync_dust_P   ->SetLineWidth(2     );   //AWS20111222
   spectrum_sync_dust_P   ->SetLineStyle(1      );  //1=solid 2=dash 3=dot 4=dash-dot  //AWS20111222
   spectrum_sync_dust_P   ->Draw("L");              //AWS20111222

  }

 if(icase==4) // free-free and dust only for total intensity I
 {
  // free-free
  spectrum_free_free->SetLineColor(kGreen ); //AWS20110622
  spectrum_free_free->SetLineWidth(2     );  //AWS20110622
  spectrum_free_free->SetLineStyle(1      ); //1=solid 2=dash 3=dot 4=dash-dot  //AWS20110622
  spectrum_free_free->Draw("L");             //AWS20110622

  // sync+free-free
  spectrum_sync_free_free->SetLineColor(kRed  ); //AWS20110622
  spectrum_sync_free_free->SetLineWidth(2     ); //AWS20110622
  spectrum_sync_free_free->SetLineStyle(1      );//1=solid 2=dash 3=dot 4=dash-dot  //AWS20110622
  spectrum_sync_free_free->Draw("L");            //AWS20110622

 // dust
  spectrum_dust     ->SetLineColor(kMagenta); //AWS20111222
  spectrum_dust     ->SetLineWidth(2     );   //AWS20111222
  spectrum_dust     ->SetLineStyle(1      );  //1=solid 2=dash 3=dot 4=dash-dot  //AWS20111222
  spectrum_dust     ->Draw("L");              //AWS20111222

 // sync+free-free+dust
  spectrum_sync_free_free_dust->SetLineColor(kCyan);  //AWS20110622
  spectrum_sync_free_free_dust->SetLineWidth(2     ); //AWS20110622
  spectrum_sync_free_free_dust->SetLineStyle(1      );//1=solid 2=dash 3=dot 4=dash-dot  //AWS20110622
  spectrum_sync_free_free_dust->Draw("L");            //AWS20110622

 }

 if(icase==4) // plot other surveys only for total intensity
 {
  nu=22.0e6;
  intensity = intensity_mean____22MHz;
  datapoint=new TGraph(1);
  datapoint->SetPoint(0, nu/1.e6, intensity);
  datapoint->SetMarkerColor(kBlack );
  datapoint->SetMarkerStyle(22); // triangles
  datapoint->SetMarkerSize (1.5);//
  datapoint->Draw("P");

  nu=45.0e6;
  intensity = intensity_mean____45MHz;
  datapoint=new TGraph(1);
  datapoint->SetPoint(0, nu/1.e6, intensity);
  datapoint->SetMarkerColor(kBlack );
  datapoint->SetMarkerStyle(22); // triangles
  datapoint->SetMarkerSize (1.5);//
  datapoint->Draw("P");

  nu=150.e6;
  intensity = intensity_mean___150MHz;
  datapoint=new TGraph(1);
  datapoint->SetPoint(0, nu/1.e6, intensity);
  datapoint->SetMarkerColor(kBlack );
  datapoint->SetMarkerStyle(22); // triangles
  datapoint->SetMarkerSize (1.5);//
  datapoint->Draw("P");

  nu=408.e6;
  intensity = intensity_mean___408MHz;
  datapoint=new TGraph(1);
  datapoint->SetPoint(0, nu/1.e6, intensity);
  datapoint->SetMarkerColor(kBlack );
  datapoint->SetMarkerStyle(22); // triangles
  datapoint->SetMarkerSize (1.5);//
  datapoint->Draw("P");

  nu=820.e6;
  intensity = intensity_mean___820MHz;
  datapoint=new TGraph(1);
  datapoint->SetPoint(0, nu/1.e6, intensity);
  datapoint->SetMarkerColor(kBlack );
  datapoint->SetMarkerStyle(22); // triangles
  datapoint->SetMarkerSize (1.5);//
  datapoint->Draw("P");

  nu=1420e6;
  intensity = intensity_mean__1420MHz;
  datapoint=new TGraph(1);
  datapoint->SetPoint(0, nu/1.e6, intensity);
  datapoint->SetMarkerColor(kBlack );
  datapoint->SetMarkerStyle(22); // triangles
  datapoint->SetMarkerSize (1.5);//
  datapoint->Draw("P");

  nu=2326e6;
  intensity = intensity_mean__2326MHz;
  datapoint=new TGraph(1);
  datapoint->SetPoint(0, nu/1.e6, intensity);
  datapoint->SetMarkerColor(kBlack );
  datapoint->SetMarkerStyle(22); // triangles
  datapoint->SetMarkerSize (1.5);//
  datapoint->Draw("P");

 }

  // plot polarized WMAP data

  nu_WMAP[0]=22800.e6;
  nu_WMAP[1]=31000.e6;
  nu_WMAP[2]=41000.e6;
  nu_WMAP[3]=61000.e6;
  nu_WMAP[4]=94000.e6;

 for (inu_WMAP=0;inu_WMAP<nnu_WMAP;inu_WMAP++)
  {
   if(   (inu_WMAP==0 && galplotdef.sync_data_22800MHz==1) //AWS20071221
      || (inu_WMAP==1 && galplotdef.sync_data_33000MHz==1)
      || (inu_WMAP==2 && galplotdef.sync_data_41000MHz==1)
      || (inu_WMAP==3 && galplotdef.sync_data_61000MHz==1)
      || (inu_WMAP==4 && galplotdef.sync_data_94000MHz==1))

     {
  nu=nu_WMAP[inu_WMAP];

  intensity=0.;

  if(inu_WMAP==0 && icase==1) intensity =  intensity_Q_mean_22800MHz;
  if(inu_WMAP==1 && icase==1) intensity =  intensity_Q_mean_33000MHz;
  if(inu_WMAP==2 && icase==1) intensity =  intensity_Q_mean_41000MHz;
  if(inu_WMAP==3 && icase==1) intensity =  intensity_Q_mean_61000MHz;
  if(inu_WMAP==4 && icase==1) intensity =  intensity_Q_mean_94000MHz;


  if(inu_WMAP==0 && icase==2) intensity =  intensity_U_mean_22800MHz;
  if(inu_WMAP==1 && icase==2) intensity =  intensity_U_mean_33000MHz;
  if(inu_WMAP==2 && icase==2) intensity =  intensity_U_mean_41000MHz;
  if(inu_WMAP==3 && icase==2) intensity =  intensity_U_mean_61000MHz;
  if(inu_WMAP==4 && icase==2) intensity =  intensity_U_mean_94000MHz;


  if(inu_WMAP==0 && icase==3) intensity =  intensity_P_mean_22800MHz;
  if(inu_WMAP==1 && icase==3) intensity =  intensity_P_mean_33000MHz;
  if(inu_WMAP==2 && icase==3) intensity =  intensity_P_mean_41000MHz;
  if(inu_WMAP==3 && icase==3) intensity =  intensity_P_mean_61000MHz;
  if(inu_WMAP==4 && icase==3) intensity =  intensity_P_mean_94000MHz;

  if(inu_WMAP==0 && icase==4) intensity =  intensity_I_mean_22800MHz;
  if(inu_WMAP==1 && icase==4) intensity =  intensity_I_mean_33000MHz;
  if(inu_WMAP==2 && icase==4) intensity =  intensity_I_mean_41000MHz;
  if(inu_WMAP==3 && icase==4) intensity =  intensity_I_mean_61000MHz;
  if(inu_WMAP==4 && icase==4) intensity =  intensity_I_mean_94000MHz;


  // convert Tb (mK) to intensity
  intensity     *=1.0e-3;
  
  intensity=intensity/ I_to_Tb  *pow(nu,2);

  if(icase == 1 )cout<<"WMAP polarized Stokes Q datapoint nu="<<nu<<" intensity="<<intensity<<endl;
  if(icase == 2 )cout<<"WMAP polarized Stokes U datapoint nu="<<nu<<" intensity="<<intensity<<endl;
  if(icase == 3 )cout<<"WMAP polarized Stokes P datapoint nu="<<nu<<" intensity="<<intensity<<endl;
  if(icase == 4 )cout<<"WMAP polarized Stokes I datapoint nu="<<nu<<" intensity="<<intensity<<endl;

  intensity=abs(intensity); // to avoid negative values in the spectrum
  
  datapoint=new TGraph(1);
  datapoint->SetPoint(0, nu/1.e6, intensity);

  datapoint->SetMarkerColor(kRed );
  datapoint->SetMarkerStyle(22); // triangles
  datapoint->SetMarkerSize (1.5);//
  datapoint->Draw("P");


}
  }// inu_WMAP



//----------------------------------------- text

  strcpy(canvastitle," galdef ID ");
  strcat(canvastitle,galdef.galdef_ID);


  sprintf(workstring1,"  %5.2f<l<%5.2f , %5.2f<l<%5.2f",               
                          galplotdef.long_min1,galplotdef.long_max1,
                          galplotdef.long_min2,galplotdef.long_max2);

  sprintf(workstring2,"  %5.2f<b<%5.2f , %5.2f<b<%5.2f",                 
                         galplotdef.lat_min1,galplotdef.lat_max1,
                         galplotdef.lat_min2,galplotdef.lat_max2);




  text=new TText();
  text->SetTextFont(62);
  text->SetTextColor(kBlack);
  text->SetTextSize(0.034 );                                         
  text->SetTextAlign(12);

  text->DrawTextNDC(.10 ,.93 ,canvastitle);// NDC=normalized coord system   
  text->SetTextSize(0.022 );                                            
  text->DrawTextNDC(.58 ,.88 ,workstring1);// NDC=normalized coord system  
  text->DrawTextNDC(.58 ,.86 ,workstring2);// NDC=normalized coord system   

  text->SetTextSize(0.040 );//AWS20121130 was 0.22


  if(icase==1)text->DrawTextNDC(.12 ,.87 ,"Stokes Q" );// NDC=normalized coord system //AWS20121130 was 0.20,.86
  if(icase==2)text->DrawTextNDC(.12 ,.87 ,"Stokes U" );// NDC=normalized coord system
  if(icase==3)text->DrawTextNDC(.12 ,.87 ,"Stokes P" );// NDC=normalized coord system
  if(icase==4)text->DrawTextNDC(.12 ,.87 ,"Stokes I" );// NDC=normalized coord system

 //============== postscript, gif and txt files

  sprintf(workstring1,"l_%.2f_%.2f_%.2f_%.2f",                          
                          galplotdef.long_min1,galplotdef.long_max1,
                          galplotdef.long_min2,galplotdef.long_max2);

  sprintf(workstring2,"b_%.2f_%.2f_%.2f_%.2f",                          
                          galplotdef.lat_min1,galplotdef.lat_max1,
                          galplotdef.lat_min2,galplotdef.lat_max2);

  strcpy(psfile,"plots/");

  if(icase==1)  strcat(psfile,"synchrotron_Q_spectrum_");
  if(icase==2)  strcat(psfile,"synchrotron_U_spectrum_");
  if(icase==3)  strcat(psfile,"synchrotron_P_spectrum_");
  if(icase==4)  strcat(psfile,"synchrotron_I_spectrum_");

  strcat(psfile,galdef.galdef_ID);
  strcat(psfile,"_"        );
  strcat(psfile,workstring1);
  strcat(psfile,"_"        );
  strcat(psfile,workstring2);
  strcat(psfile,"_"        );
  strcat(psfile,galplotdef.psfile_tag);
  strcpy(giffile,psfile);

 if(galplotdef.output_format==1 || galplotdef.output_format==3)
   {
   strcat(psfile,".eps");
   cout<<"postscript file="<<psfile<<endl;
   c1->Print(psfile,"eps" );
  }

  if(galplotdef.output_format==2 || galplotdef.output_format==3)
   {
   strcat(giffile,".gif");
   cout<<"       gif file="<<giffile<<endl;
   c1->Print(giffile,"gif" );
  }




  }// icase






  ///////////////////////////////////////////////////////////////////////////////
  // plot spectral index
  ///////////////////////////////////////////////////////////////////////////////



  strcpy(name,"spectral_index");// to identify it: name is not plotted
  c1=new TCanvas(name,name,200,100,650,650);

  c1  ->SetLogx(1);
  c1  ->SetLogy(0);

//Gtypes.h:enum EColor { kWhite, kBlack, kRed, kGreen, kBlue, kYellow, kMagenta, kCyan };
  spectral_index->SetMarkerColor(kBlue );
  spectral_index->SetMarkerStyle(21);
  spectral_index->SetMarkerStyle(22); // triangles
  spectral_index->SetMarkerSize (0.5);//
  spectral_index->SetLineColor(kBlue );
  spectral_index->SetLineWidth(2     );
  spectral_index->SetLineStyle(1      );//1=solid 2=dash 3=dot 4=dash-dot

  spectral_index->SetTitle(""         ); // removes text written in box on plot
  spectral_index->GetHistogram()->SetXTitle("frequency, MHz                 ");

  spectral_index->GetHistogram()->SetYTitle("synchrotron spectral index"); //needs {} to work

  spectral_index->GetHistogram()->SetLabelOffset(+0.00,"X"); //  +ve down
  spectral_index->GetHistogram()->SetLabelSize  ( 0.03,"Y"); // labels, not titles


  spectral_index->GetHistogram()->SetTitleOffset(+1.1,"X");  //  +ve down
  spectral_index->GetHistogram()->SetTitleOffset(+1.2,"Y");  //  +ve to left
  spectral_index->GetHistogram()->SetTitleSize   (0.035,"X");//AWS20080125
  spectral_index->GetHistogram()->SetTitleSize   (0.035,"Y");//AWS20080125

  spectral_index->SetMinimum(2.0);

  spectral_index->Draw("AL");       // axes  lines


////////////////////////////////
// spectral index data        //AWS20090123
////////////////////////////////

// should eventually be moved to separate routine
  int n_spectral_index_data;

  n_spectral_index_data=4;
  valarray<double> nu1(n_spectral_index_data),nu2(n_spectral_index_data), spectral_index_value(n_spectral_index_data) ,spectral_index_value_err(n_spectral_index_data);
  valarray<double> spectral_index_value_min(n_spectral_index_data) ,spectral_index_value_max(n_spectral_index_data);


   // Tartari etal  ApJ 688, 32 (2008)  TRIS III  Table 8 (more frequency combinations are listed if required) 
  int n_region,i_region;

n_region=3; // use 4 if l=80 b=3 also required 
int i;

for (i_region=0;i_region<n_region;i_region++)
{


  if(i_region==0)
  {
  // RA=09h (l,b)=(179,41)
  i=0;nu1[i]=150.;nu2[i]= 408.; spectral_index_value[i]  =  2.1 ;spectral_index_value_err[i]=0.3;
  i=1;nu1[i]=408.;nu2[i]= 600.; spectral_index_value[i]  =  2.6 ;spectral_index_value_err[i]=0.1;
  i=2;nu1[i]=600.;nu2[i]= 820.; spectral_index_value[i]  =  2.91;spectral_index_value_err[i]=0.08;
  i=3;nu1[i]=820.;nu2[i]=1420.; spectral_index_value[i]  =  2.8 ;spectral_index_value_err[i]=0.1;
  }

  if(i_region==1)
  {
  // RA=10h (l,b)=(178,52)
  i=0;nu1[i]=150.;nu2[i]= 408.; spectral_index_value[i]  =  2.2 ;spectral_index_value_err[i]=0.3;
  i=1;nu1[i]=408.;nu2[i]= 600.; spectral_index_value[i]  =  2.5 ;spectral_index_value_err[i]=0.1;
  i=2;nu1[i]=600.;nu2[i]= 820.; spectral_index_value[i]  =  3.0 ;spectral_index_value_err[i]=0.1 ;
  i=3;nu1[i]=820.;nu2[i]=1420.; spectral_index_value[i]  =  2.7 ;spectral_index_value_err[i]=0.2;
  }

  if(i_region==2)
  {
  // RA=11h (l,b)=(172,63)
  i=0;nu1[i]=150.;nu2[i]= 408.; spectral_index_value[i]  =  2.2 ;spectral_index_value_err[i]=0.3;
  i=1;nu1[i]=408.;nu2[i]= 600.; spectral_index_value[i]  =  2.3 ;spectral_index_value_err[i]=0.2;
  i=2;nu1[i]=600.;nu2[i]= 820.; spectral_index_value[i]  =  3.0 ;spectral_index_value_err[i]=0.2 ;
  i=3;nu1[i]=820.;nu2[i]=1420.; spectral_index_value[i]  =  3.3 ;spectral_index_value_err[i]=0.6;
  }

  if(i_region==3)
  {
  // RA=20h 24m (l,b)=( 80, 3)
  i=0;nu1[i]=150.;nu2[i]= 408.; spectral_index_value[i]  =  0.0 ;spectral_index_value_err[i]=0.0;// no data
  i=1;nu1[i]=408.;nu2[i]= 600.; spectral_index_value[i]  =  2.82;spectral_index_value_err[i]=0.09;
  i=2;nu1[i]=600.;nu2[i]= 820.; spectral_index_value[i]  =  2.8 ;spectral_index_value_err[i]=0.1 ;
  i=3;nu1[i]=820.;nu2[i]=1420.; spectral_index_value[i]  =  2.4 ;spectral_index_value_err[i]=0.1;
  }


  spectral_index_value_min=spectral_index_value -spectral_index_value_err;
  spectral_index_value_max=spectral_index_value +spectral_index_value_err;

  for (i=0;i< n_spectral_index_data;i++)
  {
    double dnuf=1.0+i_region*.1; // offset to distingish regions
    spectral_index_data=new TGraph(5); //plot as boxes
    spectral_index_data->SetPoint(0,double(nu1[i])*dnuf,double(spectral_index_value_min[i]));
    spectral_index_data->SetPoint(1,double(nu2[i])*dnuf,double(spectral_index_value_min[i]));
    spectral_index_data->SetPoint(2,double(nu2[i])*dnuf,double(spectral_index_value_max[i]));
    spectral_index_data->SetPoint(3,double(nu1[i])*dnuf,double(spectral_index_value_max[i]));
    spectral_index_data->SetPoint(4,double(nu1[i])*dnuf,double(spectral_index_value_min[i]));
    spectral_index_data->SetLineColor(kRed);
    spectral_index_data->SetLineWidth(2);
    spectral_index_data->SetLineStyle(1);
    spectral_index_data->Draw("same");
  }//i
}// i_region




// Rogers & Bowman 2008 AJ 136, 641
// high latitudes, 100-200 MHz, and also 150-408MHz using Haslam 408MHz
  n_spectral_index_data=2; // arrays already defined above but not necessarily consistent! 
  i=0;nu1[i]=100.;nu2[i]= 200.; spectral_index_value[i]  =  2.5 ;spectral_index_value_err[i]=0.1;
  i=1;nu1[i]=150.;nu2[i]= 408.; spectral_index_value[i]  =  2.52;spectral_index_value_err[i]=0.04;

  spectral_index_value_min=spectral_index_value -spectral_index_value_err;
  spectral_index_value_max=spectral_index_value +spectral_index_value_err;
  i_region=0;

  for (i=0;i< n_spectral_index_data;i++)
  {
    double dnuf=1.0+i_region*.1; // offset to distingish regions
    spectral_index_data=new TGraph(5); //plot as boxes
    spectral_index_data->SetPoint(0,double(nu1[i])*dnuf,double(spectral_index_value_min[i]));
    spectral_index_data->SetPoint(1,double(nu2[i])*dnuf,double(spectral_index_value_min[i]));
    spectral_index_data->SetPoint(2,double(nu2[i])*dnuf,double(spectral_index_value_max[i]));
    spectral_index_data->SetPoint(3,double(nu1[i])*dnuf,double(spectral_index_value_max[i]));
    spectral_index_data->SetPoint(4,double(nu1[i])*dnuf,double(spectral_index_value_min[i]));
    spectral_index_data->SetLineColor(kBlue);
    spectral_index_data->SetLineWidth(2);
    spectral_index_data->SetLineStyle(1);
    spectral_index_data->Draw("same");
  }//i

// Roger et al. 1999 Astron. Astrophys. Suppl. Ser. 137, 7-19
// 22 MHz combined with Haslam 408 MHz
// average high latitude=2.47, variation about 0.05

  n_spectral_index_data=1; // arrays already defined above but not necessarily consistent! 
  i=0;nu1[i]=22. ;nu2[i]= 408.; spectral_index_value[i]  =  2.47;spectral_index_value_err[i]=0.05;

  spectral_index_value_min=spectral_index_value -spectral_index_value_err;
  spectral_index_value_max=spectral_index_value +spectral_index_value_err;
  i_region=0;

  for (i=0;i< n_spectral_index_data;i++)
  {
    double dnuf=1.0+i_region*.1; // offset to distingish regions
    spectral_index_data=new TGraph(5); //plot as boxes
    spectral_index_data->SetPoint(0,double(nu1[i])*dnuf,double(spectral_index_value_min[i]));
    spectral_index_data->SetPoint(1,double(nu2[i])*dnuf,double(spectral_index_value_min[i]));
    spectral_index_data->SetPoint(2,double(nu2[i])*dnuf,double(spectral_index_value_max[i]));
    spectral_index_data->SetPoint(3,double(nu1[i])*dnuf,double(spectral_index_value_max[i]));
    spectral_index_data->SetPoint(4,double(nu1[i])*dnuf,double(spectral_index_value_min[i]));
    spectral_index_data->SetLineColor(kCyan);
    spectral_index_data->SetLineWidth(2);
    spectral_index_data->SetLineStyle(1);
    spectral_index_data->Draw("same");
  }//i


// Giardino et al 2002 A&A 387, 82
// 408-1420-2326 MHz
// whole sky analysis
// value  abstract (and entry in Table 4)
  n_spectral_index_data=2; // arrays already defined above but not necessarily consistent! 
  i=0;nu1[i]=408.;nu2[i]=1420.; spectral_index_value[i]  =  2.78 ;spectral_index_value_err[i]=0.17;
  i=1;nu1[i]=408.;nu2[i]=2326.; spectral_index_value[i]  =  2.75 ;spectral_index_value_err[i]=0.12;

  spectral_index_value_min=spectral_index_value -spectral_index_value_err;
  spectral_index_value_max=spectral_index_value +spectral_index_value_err;
  i_region=0;

  for (i=0;i< n_spectral_index_data;i++)
  {
    double dnuf=1.0+i_region*.1; // offset to distingish regions
    spectral_index_data=new TGraph(5); //plot as boxes
    spectral_index_data->SetPoint(0,double(nu1[i])*dnuf,double(spectral_index_value_min[i]));
    spectral_index_data->SetPoint(1,double(nu2[i])*dnuf,double(spectral_index_value_min[i]));
    spectral_index_data->SetPoint(2,double(nu2[i])*dnuf,double(spectral_index_value_max[i]));
    spectral_index_data->SetPoint(3,double(nu1[i])*dnuf,double(spectral_index_value_max[i]));
    spectral_index_data->SetPoint(4,double(nu1[i])*dnuf,double(spectral_index_value_min[i]));
    spectral_index_data->SetLineColor(kBlack);
    spectral_index_data->SetLineWidth(2);
    spectral_index_data->SetLineStyle(2);
    spectral_index_data->Draw("same");
  }//i



// Platania et al. 1998 ApJ 505:473
// radiometers at high altitude 1400-7500 MHz
// whole sky analysis
// value  abstract (and entry in Table 4)
  n_spectral_index_data=1; // arrays already defined above but not necessarily consistent! 
  i=0;nu1[i]=1400;nu2[i]=7500.; spectral_index_value[i]  =  2.81 ;spectral_index_value_err[i]=0.16;

  spectral_index_value_min=spectral_index_value -spectral_index_value_err;
  spectral_index_value_max=spectral_index_value +spectral_index_value_err;
  i_region=0;

  for (i=0;i< n_spectral_index_data;i++)
  {
    double dnuf=1.0+i_region*.1; // offset to distingish regions
    spectral_index_data=new TGraph(5); //plot as boxes
    spectral_index_data->SetPoint(0,double(nu1[i])*dnuf,double(spectral_index_value_min[i]));
    spectral_index_data->SetPoint(1,double(nu2[i])*dnuf,double(spectral_index_value_min[i]));
    spectral_index_data->SetPoint(2,double(nu2[i])*dnuf,double(spectral_index_value_max[i]));
    spectral_index_data->SetPoint(3,double(nu1[i])*dnuf,double(spectral_index_value_max[i]));
    spectral_index_data->SetPoint(4,double(nu1[i])*dnuf,double(spectral_index_value_min[i]));
    spectral_index_data->SetLineColor(kBlack);
    spectral_index_data->SetLineWidth(2);
    spectral_index_data->SetLineStyle(3);
    spectral_index_data->Draw("same");
  }//i


// Platania et al. 2003  A&A 410, 847-863 
// 408-1420-2326 MHz
// whole sky analysis
// value  abstract (and entry in Table 4)
  n_spectral_index_data=1; // arrays already defined above but not necessarily consistent! 
  i=0;nu1[i]=408.;nu2[i]=2326.; spectral_index_value[i]  =  2.695;spectral_index_value_err[i]=0.12;

  spectral_index_value_min=spectral_index_value -spectral_index_value_err;
  spectral_index_value_max=spectral_index_value +spectral_index_value_err;
  i_region=0;

  for (i=0;i< n_spectral_index_data;i++)
  {
    double dnuf=1.0+i_region*.1; // offset to distingish regions
    spectral_index_data=new TGraph(5); //plot as boxes
    spectral_index_data->SetPoint(0,double(nu1[i])*dnuf,double(spectral_index_value_min[i]));
    spectral_index_data->SetPoint(1,double(nu2[i])*dnuf,double(spectral_index_value_min[i]));
    spectral_index_data->SetPoint(2,double(nu2[i])*dnuf,double(spectral_index_value_max[i]));
    spectral_index_data->SetPoint(3,double(nu1[i])*dnuf,double(spectral_index_value_max[i]));
    spectral_index_data->SetPoint(4,double(nu1[i])*dnuf,double(spectral_index_value_min[i]));
    spectral_index_data->SetLineColor(kGreen);
    spectral_index_data->SetLineWidth(2);
    spectral_index_data->SetLineStyle(1);
    spectral_index_data->Draw("same");
  }//i

// Kogut et al  The Astrophysical Journal, 665:355-362, 2007 
// 3 year WMAP polarization. whole sky average beta=3.2, increasing to 3.0 in plane

  n_spectral_index_data=1; // arrays already defined above but not necessarily consistent! 
  i=0;nu1[i]=23000.;nu2[i]=33000.; spectral_index_value[i]  =  3.2;spectral_index_value_err[i]=0.1;

  spectral_index_value_min=spectral_index_value -spectral_index_value_err;
  spectral_index_value_max=spectral_index_value +spectral_index_value_err;
  i_region=0;

  for (i=0;i< n_spectral_index_data;i++)
  {
    double dnuf=1.0+i_region*.1; // offset to distingish regions
    spectral_index_data=new TGraph(5); //plot as boxes
    spectral_index_data->SetPoint(0,double(nu1[i])*dnuf,double(spectral_index_value_min[i]));
    spectral_index_data->SetPoint(1,double(nu2[i])*dnuf,double(spectral_index_value_min[i]));
    spectral_index_data->SetPoint(2,double(nu2[i])*dnuf,double(spectral_index_value_max[i]));
    spectral_index_data->SetPoint(3,double(nu1[i])*dnuf,double(spectral_index_value_max[i]));
    spectral_index_data->SetPoint(4,double(nu1[i])*dnuf,double(spectral_index_value_min[i]));
    spectral_index_data->SetLineColor(kMagenta);
    spectral_index_data->SetLineWidth(2);
    spectral_index_data->SetLineStyle(1);
    spectral_index_data->Draw("same");
  }//i

// Miville-Deschenes et al 2008A&A...490.1093
// WMAP polarized, Haslam 408 MHz
// values from abstract: beta_sync = 3.00 with standard dev. 0.06
  n_spectral_index_data=1; // arrays already defined above but not necessarily consistent! 
i=0;nu1[i]=  408.;nu2[i]=23000.; spectral_index_value[i]  =  3.0;spectral_index_value_err[i]=0.06;

  spectral_index_value_min=spectral_index_value -spectral_index_value_err;
  spectral_index_value_max=spectral_index_value +spectral_index_value_err;
  i_region=0;

  for (i=0;i< n_spectral_index_data;i++)
  {
    double dnuf=1.0+i_region*.1; // offset to distingish regions
    spectral_index_data=new TGraph(5); //plot as boxes
    spectral_index_data->SetPoint(0,double(nu1[i])*dnuf,double(spectral_index_value_min[i]));
    spectral_index_data->SetPoint(1,double(nu2[i])*dnuf,double(spectral_index_value_min[i]));
    spectral_index_data->SetPoint(2,double(nu2[i])*dnuf,double(spectral_index_value_max[i]));
    spectral_index_data->SetPoint(3,double(nu1[i])*dnuf,double(spectral_index_value_max[i]));
    spectral_index_data->SetPoint(4,double(nu1[i])*dnuf,double(spectral_index_value_min[i]));
    spectral_index_data->SetLineColor(kOrange );
    spectral_index_data->SetLineWidth(2);
    spectral_index_data->SetLineStyle(1);
    spectral_index_data->Draw("same");
  }//i

// Gold et al 2008 ApJS http://arxiv.org/abs/0803.0715
// WMAP 5 years Fig 16 polarized, out-of-plane
// needs checking
  n_spectral_index_data=1; // arrays already defined above but not necessarily consistent! 
  i=0;nu1[i]=  408.;nu2[i]=23000.; spectral_index_value[i]  =  3.15;spectral_index_value_err[i]=0.10;

  spectral_index_value_min=spectral_index_value -spectral_index_value_err;
  spectral_index_value_max=spectral_index_value +spectral_index_value_err;
  i_region=0;

  for (i=0;i< n_spectral_index_data;i++)
  {
    double dnuf=1.0+i_region*.1; // offset to distingish regions
    spectral_index_data=new TGraph(5); //plot as boxes
    spectral_index_data->SetPoint(0,double(nu1[i])*dnuf,double(spectral_index_value_min[i]));
    spectral_index_data->SetPoint(1,double(nu2[i])*dnuf,double(spectral_index_value_min[i]));
    spectral_index_data->SetPoint(2,double(nu2[i])*dnuf,double(spectral_index_value_max[i]));
    spectral_index_data->SetPoint(3,double(nu1[i])*dnuf,double(spectral_index_value_max[i]));
    spectral_index_data->SetPoint(4,double(nu1[i])*dnuf,double(spectral_index_value_min[i]));
    spectral_index_data->SetLineColor(kOrange );
    spectral_index_data->SetLineWidth(2);
    spectral_index_data->SetLineStyle(2);
    spectral_index_data->Draw("same");
  }//i


// Dunkley et al 2008 http://arxiv.org/abs/0811.4280
// WMAP 5 year polarized
// do not confirm decrease in slope to plane found by Kogut etal 2007
// check what the frequency range really refers to here. Use 23-33 GHz here but can be 408MHz-23GHz

  n_spectral_index_data=1; // arrays already defined above but not necessarily consistent! 
  i=0;nu1[i]=23000.;nu2[i]=33000.; spectral_index_value[i]  =  3.02;spectral_index_value_err[i]=0.04;

  spectral_index_value_min=spectral_index_value -spectral_index_value_err;
  spectral_index_value_max=spectral_index_value +spectral_index_value_err;
  i_region=0;

  for (i=0;i< n_spectral_index_data;i++)
  {
    double dnuf=1.0+i_region*.1; // offset to distingish regions
    spectral_index_data=new TGraph(5); //plot as boxes
    spectral_index_data->SetPoint(0,double(nu1[i])*dnuf,double(spectral_index_value_min[i]));
    spectral_index_data->SetPoint(1,double(nu2[i])*dnuf,double(spectral_index_value_min[i]));
    spectral_index_data->SetPoint(2,double(nu2[i])*dnuf,double(spectral_index_value_max[i]));
    spectral_index_data->SetPoint(3,double(nu1[i])*dnuf,double(spectral_index_value_max[i]));
    spectral_index_data->SetPoint(4,double(nu1[i])*dnuf,double(spectral_index_value_min[i]));
    spectral_index_data->SetLineColor(kOrange );
    spectral_index_data->SetLineWidth(2);
    spectral_index_data->SetLineStyle(3);
    spectral_index_data->Draw("same");
  }//i

// Kogut et al. 2008 http://arxiv.org/abs/0901.0562
// ARCADE2 3, 8, 10 GHz.   Index quoted at reference 1 GHz
// average Galactic beta = 2.55 +-0.03, poles, coldest region=2.57
  n_spectral_index_data=1; // arrays already defined above but not necessarily consistent! 
  i=0;nu1[i]=  900.;nu2[i]= 1100.; spectral_index_value[i]  =  2.55;spectral_index_value_err[i]=0.03;

  spectral_index_value_min=spectral_index_value -spectral_index_value_err;
  spectral_index_value_max=spectral_index_value +spectral_index_value_err;
  i_region=0;

  for (i=0;i< n_spectral_index_data;i++)
  {
    double dnuf=1.0+i_region*.1; // offset to distingish regions
    spectral_index_data=new TGraph(5); //plot as boxes
    spectral_index_data->SetPoint(0,double(nu1[i])*dnuf,double(spectral_index_value_min[i]));
    spectral_index_data->SetPoint(1,double(nu2[i])*dnuf,double(spectral_index_value_min[i]));
    spectral_index_data->SetPoint(2,double(nu2[i])*dnuf,double(spectral_index_value_max[i]));
    spectral_index_data->SetPoint(3,double(nu1[i])*dnuf,double(spectral_index_value_max[i]));
    spectral_index_data->SetPoint(4,double(nu1[i])*dnuf,double(spectral_index_value_min[i]));
    spectral_index_data->SetLineColor(kBlack  );
    spectral_index_data->SetLineWidth(2);
    spectral_index_data->SetLineStyle(1);
    spectral_index_data->Draw("same");
  }//i

//----------------------------------------- text

  strcpy(canvastitle," galdef ID ");
  strcat(canvastitle,galdef.galdef_ID);


  sprintf(workstring1,"  %5.2f<l<%5.2f , %5.2f<l<%5.2f",               
                          galplotdef.long_min1,galplotdef.long_max1,
                          galplotdef.long_min2,galplotdef.long_max2);

  sprintf(workstring2,"  %5.2f<b<%5.2f , %5.2f<b<%5.2f",                 
                         galplotdef.lat_min1,galplotdef.lat_max1,
                         galplotdef.lat_min2,galplotdef.lat_max2);




  text=new TText();
  text->SetTextFont(62);
  text->SetTextColor(kBlack);
  text->SetTextSize(0.034 );                                         
  text->SetTextAlign(12);

  text->DrawTextNDC(.10 ,.93 ,canvastitle);// NDC=normalized coord system   
  text->SetTextSize(0.022 );                                            
  text->DrawTextNDC(.53 ,.88 ,workstring1);// NDC=normalized coord system  
  text->DrawTextNDC(.53 ,.86 ,workstring2);// NDC=normalized coord system   


 //============== postscript, gif and txt files

  sprintf(workstring1,"l_%.2f_%.2f_%.2f_%.2f",                          
                          galplotdef.long_min1,galplotdef.long_max1,
                          galplotdef.long_min2,galplotdef.long_max2);

  sprintf(workstring2,"b_%.2f_%.2f_%.2f_%.2f",                          
                          galplotdef.lat_min1,galplotdef.lat_max1,
                          galplotdef.lat_min2,galplotdef.lat_max2);

  strcpy(psfile,"plots/");
  strcat(psfile,"synchrotron_spectral_index_");
  strcat(psfile,galdef.galdef_ID);
  strcat(psfile,"_"        );
  strcat(psfile,workstring1);
  strcat(psfile,"_"        );
  strcat(psfile,workstring2);
  strcat(psfile,"_"        );
  strcat(psfile,galplotdef.psfile_tag);
  strcpy(giffile,psfile);

 if(galplotdef.output_format==1 || galplotdef.output_format==3)
   {
   strcat(psfile,".eps");
   cout<<"postscript file="<<psfile<<endl;
   c1->Print(psfile,"eps" );
  }

  if(galplotdef.output_format==2 || galplotdef.output_format==3)
   {
   strcat(giffile,".gif");
   cout<<"       gif file="<<giffile<<endl;
   c1->Print(giffile,"gif" );
  }


    cout<<" <<<< plot_synchrotron_skymap"<<endl;

   
    return stat;
}
