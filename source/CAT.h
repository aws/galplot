
//#include"FITS.h"
#include"galprop_classes.h" // since FITS already here and included in galplot

class CAT
{
 public:



  long n_sources;
  double *ra  ,*dec  ;
  double *longitude,*latitude;
  char **name;

  double *flux,*flux_err;                      //AWS20060110 

  int read (char *directory);     
  int read (char *directory,char*filename);    //AWS20060110     
  void print();

  void find (char *name_,
             double *ra_, double *dec_, double *longitude_, double *latitude_);
};
