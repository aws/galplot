using namespace std;
#include<iostream>
#include<fstream>
#include<cstring>
#include<cstdlib>
#include<vector>
#include<valarray>

#include"fitsio.h"

#include"EnergyDispersionFermiLAT.h"

 
//C linkage since GSL is C library, assumed to be present as library compiled with C. 

// error-handling versions are preferred

typedef struct{double val;double err;} gsl_sf_result; // structure used by GSL routines
extern "C" double     gsl_sf_gamma    (const double x);
extern "C" double     gsl_sf_gamma_e  (const double x, gsl_sf_result *result);
extern "C" void       gsl_set_error_handler_off();

//////////////////////////////////////////////////////////////////

void EnergyDispersionFermiLAT::read(int debug)
{
 

  cout<<">> EnergyDispersion::read"<<endl;

  fitsfile *fptr;
  int status=-1;
  char *Fermi_rsp_file=new char[100];
 
  strcpy (Fermi_rsp_file,"rsp_big.fits");
  cout<<"EmissivityData:: ReadEnergyDispersion"<<endl;
  cout<<"Fermi rsp file: "<<Fermi_rsp_file<<endl;

  fits_open_file(&fptr,Fermi_rsp_file,READONLY,&status);
  if(fptr==NULL||status!=0)
   {
      cout<<"no Fermi rsp file called "<<Fermi_rsp_file; return ;
   }

  fits_movnam_hdu(fptr,ANY_HDU,"MATRIX",0, &status);
  cout<<"Fermi rsp file: HDU MATRIX status="<<status<<endl;


  double *fitsdata; 
  long nrows=1;
  int ncolumns=1;
  fits_get_num_rows(fptr,&nrows,&status);
  cout<<"Fermi rsp file HDU MATRIX nrows="<<nrows<<endl;

  fits_get_num_cols(fptr,&ncolumns,&status);
  cout<<"Fermi rsp file HDU MATRIX top level ncolumns="<<ncolumns<<endl;

  int colnum=0;
  fits_get_colnum(fptr,CASESEN,"MATRIX",&colnum,&status);
  cout<<"Fermi rsp file MATRIX colnum="<<colnum<<endl;

  int  typecode=0;
  long repeat=0;
  long width =0;
  fits_get_coltype(fptr,colnum, &typecode,&repeat,&width,&status);
  cout<<" Fermi rsp file MATRIX fits_get_coltype     typecode="<<typecode<<endl;
  cout<<" Fermi rsp file MATRIX fits_get_coltype       repeat="<<repeat  <<endl;
  cout<<" Fermi rsp file MATRIX fits_get_coltype       width ="<<width   <<endl;
  cout<<" Fermi rsp file MATRIX fits_get_coltype       status="<<status  <<endl;

  // variable length rows require this routine
  long offset=0;
  ncolumns=0;
  for (LONGLONG irow=1;irow<=nrows;irow++)
  {
   fits_read_descript(fptr,colnum,irow, &repeat,&offset,&status);

   if(debug==1)
   {
    cout<< "irow="<<irow;
    cout<<" Fermi rsp file MATRIX  fits_read_descript       offset="<<offset  <<endl;
    cout<<" Fermi rsp file MATRIX  fits_read_descript       repeat="<<repeat  <<endl;
    cout<<" Fermi rsp file MATRIX  fits_read_descript       status="<<status  <<endl;
   }

   if(repeat>ncolumns)ncolumns=repeat; // get max length this way
  }

  cout<<"Fermi rsp file MATRIX ncolumns from max repeat="<<ncolumns<<endl;
  

  fitsdata=new double[nrows];
  LONGLONG nelements=nrows;
  LONGLONG first_row    =1;
  LONGLONG first_element=1;

  EnergyDispersionMatrix.resize(nrows);
  for(int i=0;i<nrows;i++)
  {
   EnergyDispersionMatrix[i].resize(nrows);
   for(int j=0;j<ncolumns;j++)
   {   
              EnergyDispersionMatrix[i][j]=0;
   }
  }



  int i,j;



  for (int irow=1;irow<=nrows;irow++)
  {

  double nulval=0;
  int    anynul=0;
  for(int k=0;k<nrows;k++) fitsdata[k]=0; // since may be undefined values

  fits_read_descript(fptr,colnum,irow, &repeat,&offset,&status);
  nelements=repeat;
  first_row=irow;

  fits_read_col(fptr,TDOUBLE,colnum,first_row,first_element,nelements,&nulval,fitsdata,&anynul,&status);


  if(debug==1)
  {
  cout<<"Fermi rsp file: MATRIX read status="<<status<<endl;
  cout<<"Fermi rsp file: MATRIX read anynul="<<anynul<<endl;


   cout<<"fitsdata irow= "<<irow<<" nelements= "<<nelements <<":";
   for(int k=0;k<nelements;k++)                                              cout<<fitsdata[k]<<" ";
   cout<<endl;
  }

  for(int k=0;k<nelements;k++){i=irow-1;j=k;         EnergyDispersionMatrix[i][j]=fitsdata[k];}


  } // irow


 if(debug==1)
 {
  cout<<"EnergyDispersionMatrix: filled to i="<<i<<",  j="<<j<<endl;


  for (int i=0;i<nrows;i++)
  {
    cout<<"EnergyDispersionMatrix as read: i="<<i<<": ";
   for(int j=0;j<ncolumns;j++)
   {
     cout<<  EnergyDispersionMatrix[i][j]  <<" ";
 
   }
   cout<<endl;
  }
}// debug
  

 

 // normalize each row. NB assumes the normalization is 1, i.e. the dispersion matrix is complete
  for (int i=0;i<nrows;i++) EnergyDispersionMatrix[i] /= EnergyDispersionMatrix[i].sum();


  // notation used in class
  nE_true=nrows;
  nE_meas=ncolumns;
 
 if(debug==1)
 {
  for (int i=0;i<nE_true;i++)
  {
    cout<<"normalized  EnergyDispersionMatrix: i="<<i<<": ";
   for(int j=0;j<nE_meas;j++)
   {
     cout<<  EnergyDispersionMatrix[i][j]  <<" ";

 
   }
    cout<< " sum="<< EnergyDispersionMatrix[i].sum();
    cout<<endl;
  }
 }// debug


 // following must be done using EBOUNDS and MATRIX
 E_true.resize(nE_true);
 E_meas.resize(nE_meas);

 E_true[0]        =20.;       // MeV until read from dataset
 E_true[nE_true-1]=2.029176e5;// MeV until read from dataset
 E_meas[0]        =20.;       // MeV until read from dataset
 E_meas[nE_meas-1]=2.029176e5;// MeV until read from dataset




 log_E_true_min=log(E_true[0]);
 log_E_meas_min=log(E_meas[0]);
 dlog_E_true=log( E_true[nE_true-1]/ E_true[0])/(nE_true-1);
 dlog_E_meas=log( E_meas[nE_meas-1]/ E_meas[0])/(nE_meas-1);

 for (int i=0;i<nE_true;i++) E_true[i]=exp(log_E_true_min+i* dlog_E_true);
 for (int j=0;j<nE_meas;j++) E_meas[j]=exp(log_E_meas_min+j* dlog_E_meas);

 dE_true.resize(nE_true);
 dE_meas.resize(nE_meas);
                            dE_true[0]=E_true[1]  -E_true[0];
 for (int i=1;i<nE_true;i++)dE_true[i]=E_true[i]  -E_true[i-1];

                            dE_meas[0]=E_meas[1]  -E_meas[0];
 for (int j=1;j<nE_meas;j++)dE_meas[j]=E_meas[j]  -E_meas[j-1];



 cout<<   "EnergyDispersion read complete, nE_true="<<nE_true<<" nE_meas="<<nE_meas<<endl;
 cout<<   "EnergyDispersion read complete,  log_E_true_min="<<log_E_true_min<<" log_E_meas_min="<<log_E_meas_min<<endl;
 cout<<   "EnergyDispersion read complete, dlog_E_true="<<dlog_E_true<<" dlog_E_meas="<<dlog_E_meas<<endl;
 
if(debug==1)
 {
  for (int i=1;i<nE_true;i++)cout<<"i="<<i<<" E_true[i]="<< E_true[i]<<" dE_true[i]="<< dE_true[i]<<endl;
  for (int j=1;j<nE_meas;j++)cout<<"j="<<j<<" E_meas[j]="<< E_meas[j]<<" dE_meas[j]="<< dE_meas[j]<<endl;
 }



 for (int i=0;i<nrows;i++) EnergyDispersionMatrix[i] /= dE_meas; // units MeV^-1




 if(debug==1)
 {
  for (int i=0;i<nE_true;i++)
  {
    cout<<"  EnergyDispersionMatrix MeV^-1: i="<<i<<": ";
    double sum=0;
   for(int j=0;j<nE_meas;j++)
   {
     cout<<  EnergyDispersionMatrix[i][j]  <<" ";
     sum +=  EnergyDispersionMatrix[i][j] * dE_meas[j];
 
   }
   cout<< " sum with MeV ="<< sum;
    cout<<endl;
  }
 }// debug


 initialized=1;

 cout<<"<< EnergyDispersion::read"<<endl;

  return;
};

////////////////////////////////////////////////////////////////////////////

double EnergyDispersionFermiLAT::value(double E_true,double E_meas, int debug)
{ 
  if(initialized!=1){cout<<" not initialized: initializing"<<endl; read(debug);}

  int iE_true=(log(E_true)-log_E_true_min)/dlog_E_true;
  int iE_meas=(log(E_meas)-log_E_meas_min)/dlog_E_meas;

  if(debug==1)cout<<"  E_true="<< E_true<<"  E_meas="<< E_meas;
  if(debug==1)cout<<" iE_true="<<iE_true<<" iE_meas="<<iE_meas;

  double value_=0.;

  if(iE_true<0||iE_true>nE_true-1  
   ||iE_meas<0||iE_meas>nE_meas-1) 
  {
   if(debug==1)cout<<"  EnergyDispersionMatrix outside range,  value=" << value_ <<endl;
   return value_;
  }

  value_ = EnergyDispersionMatrix[iE_true][iE_meas];

  if(debug==1)cout<<"  EnergyDispersionMatrix value=" << value_ <<endl;

  return value_;
}

////////////////////////////////////////////////////////////////////////////

int EnergyDispersionFermiLAT::read_parameters( int debug)
{ 

  cout<<">> EnergyDispersionFermiLAT::read_parameters"<<endl;

  int status=0;

 //set_parameter_file("default"); // disappeared when set in reset() !  try without and check output below, seems OK now

   cout<<"EnergyDispersionFermiLAT::read_parameters: parameter_file_name="<<parameter_file_name<<endl;
   cout<<"EnergyDispersionFermiLAT::read_parameters: parameter_file_type="<<parameter_file_type<<endl;


  if(parameter_file_type=="txt")
  {
   ifstream parameter_file;
   
  

   parameter_file.open(parameter_file_name.c_str());

   //  storage in file:
   //   F,  S1, K1,  BIAS,   S2, K2, PINDEX1, PINDEX2; 
 
 
    n_parameter_sets=1000; // large enough to read them in

    F      .resize(n_parameter_sets);
    S1     .resize(n_parameter_sets);
    K1     .resize(n_parameter_sets);
    BIAS   .resize(n_parameter_sets);
    BIAS1  .resize(n_parameter_sets); // for general formula, here set to BIAS
    BIAS2  .resize(n_parameter_sets); // for general formula, here set to BIAS
    S2     .resize(n_parameter_sets);
    K2     .resize(n_parameter_sets);
    PINDEX1.resize(n_parameter_sets);
    PINDEX2.resize(n_parameter_sets);



   double tmp1,tmp2,tmp3,tmp4,tmp5,tmp6,tmp7,tmp8,tmp9,tmp10;
   int i=0;

   while(!parameter_file.eof())
   {
     parameter_file>>F[i]>>tmp1>>S1[i]>>tmp2>>K1[i]>>tmp3>>BIAS[i]>>tmp4>>S2[i]>>tmp5>>K2[i]>>tmp6>>PINDEX1[i]>>tmp7>>PINDEX2[i]>>tmp8>>tmp9>>tmp10;

     cout<<"input: i="<<i<<"  F="<<F[i]<<" S1="<<S1[i]<<" K1="<<K1[i]<<" BIAS="<<BIAS[i]<<" S2="<<S2[i]<<" K2="<<K2[i]<<" PINDEX1="<<PINDEX1[i]<<" PINDEX2="<<PINDEX2[i]<<endl;
     i++;
   }

   n_parameter_sets = i-1;
   cout<<" EnergyDispersionFermiLAT::read_parameters: read "<<n_parameter_sets<<" parameter sets"<<endl;

   // the energies are not in the files so have to provide them
   if( parameter_file_name=="Edisp_P8V1_SOURCE.txt")
   {
    parameters_E_min   =10.0; // MeV
    parameters_E_factor=pow(10., 0.05);//1.05;
   }
   if( parameter_file_name=="Edisp_P8V1_SOURCE_extended.txt")
   {
    parameters_E_min   =3.2; // MeV
    parameters_E_factor=pow(10.,0.05);
   }

   cout<<"parameters_E_min   ="<<parameters_E_min   <<endl;
   cout<<"parameters_E_factor="<<parameters_E_factor<<endl;

   parameters_E_low .resize(n_parameter_sets);
   parameters_E_high.resize(n_parameter_sets);

   for(i=0;i<n_parameter_sets;i++)
     {
       parameters_E_low [i]=parameters_E_min * pow(parameters_E_factor,i);
       parameters_E_high[i]=parameters_E_min * pow(parameters_E_factor,i+1);
}

   BIAS1 = BIAS; // for general formula
   BIAS2 = BIAS; // for general formula

  for(i=0;i<n_parameter_sets;i++)
   cout<<"parameter file type txt: i="<<i<<" E= "<<parameters_E_low [i]<< " - "<<  parameters_E_high[i]
       << "  F="<<F[i]<<" S1="<<S1[i]<<" K1="<<K1[i]<<" BIAS="<<BIAS[i]<<" BIAS1="<<BIAS1[i]<<" BIAS2="<<BIAS2[i]
       <<" S2="<<S2[i]<<" K2="<<K2[i]<<" PINDEX1="<<PINDEX1[i]<<" PINDEX2="<<PINDEX2[i]<<endl;

 } //parameter_file_type=="txt"


  //////////////////////////////////////////////////////////

 if(parameter_file_type=="ST_FITS")
 {
  cout<<"<< EnergyDispersionFermiLAT::read_parameters: format ST_FITS"<<endl;
  char parameter_file_name_char[1000];
  strcpy (parameter_file_name_char, parameter_file_name.c_str());
  cout <<"parameter_file_name_char: "<<parameter_file_name_char<<endl;

  fitsfile *fptr;

   cout<<"opening  FITS file with fitsio: "<< parameter_file_name_char <<endl;

   int status=0; // need this on input
        
   if( fits_open_file(&fptr, parameter_file_name_char ,READONLY,&status) )   cout<<"FITS open status= "<<status<<endl;

   fits_report_error(stderr, status);  /* print out any error messages */

   int hdunum=2; // 1=primary, 2=1st extension, assumed to be ENERGY DISPERSION
   int hdutype;

   status=0; 
   fits_movabs_hdu (fptr, hdunum, &hdutype,&status);   cout<<"FITS movabs_hdu status= "<<status<<endl;

   int nvalues; 
   double *values;
   int datatype=TDOUBLE;
   int colnum=1;
   LONGLONG firstrow =1; 
   LONGLONG firstelem=1;
   LONGLONG nelements;
   double nulval=0;
   int    anynul=0;
   int    typecode;
   long    repeat;
   long    width;
   
   status=0;
   fits_get_coltype(fptr, colnum, &typecode, &repeat,&width, &status);

   nvalues  =repeat;
   nelements=repeat;
   cout<<"parameter file: number of values = "<<nvalues<<endl;   
   
   values = new double[nvalues];
 
   n_parameter_sets=nvalues; // for consistency with original format

   parameters_E_low .resize(n_parameter_sets);
   parameters_E_high.resize(n_parameter_sets);

    F      .resize(n_parameter_sets);

    S1     .resize(n_parameter_sets);
    K1     .resize(n_parameter_sets);
    BIAS1  .resize(n_parameter_sets);
    PINDEX1.resize(n_parameter_sets);
  
    S2     .resize(n_parameter_sets);
    K2     .resize(n_parameter_sets);
    BIAS2  .resize(n_parameter_sets);
    PINDEX2.resize(n_parameter_sets);

    BIAS   .resize(n_parameter_sets); // for compatibility with earlier version, later remove

    /*
    columns are:
    1 ENERGY_LO
    2 ENERGY_HI
    3 F
    4 S1
    5 K1
    6 BIAS   (=BIAS1)
    7 BIAS2
    8 S2
    9 K2
   10 PINDEX1
   11 PINDEX2
     */

   //  use column names for generality

   status=0; fits_get_colnum(fptr, CASEINSEN, "ENERGY_LO", &colnum,&status);  cout<<"    column name status="<<status<<endl;   fits_report_error(stderr, status);
                                cout<<"reading ENERGY_LO from column "<<colnum<<endl;
   fits_read_col       (fptr, datatype, colnum,  firstrow,  firstelem, nelements, &nulval, values, &anynul, &status);
   for (int k=0;k<nvalues;k++) cout<<"k="<<k<<" values="<< values[k]  <<endl;
   for (int k=0;k<nvalues;k++) parameters_E_low[k]= values[k];
   for (int k=0;k<nvalues;k++) cout<<"k="<<k<<" energy dispersion parameter parameters_E_low="<< parameters_E_low[k]  <<endl;

   status=0; fits_get_colnum(fptr, CASEINSEN, "ENERGY_HI", &colnum,&status);  cout<<"    column name status="<<status<<endl;  fits_report_error(stderr, status);
                                cout<<"reading ENERGY_HI from column "<<colnum<<endl;
   fits_read_col       (fptr, datatype, colnum,  firstrow,  firstelem, nelements, &nulval, values, &anynul, &status);
   for (int k=0;k<nvalues;k++) cout<<"k="<<k<<" values="<< values[k]  <<endl;
   for (int k=0;k<nvalues;k++) parameters_E_high[k]= values[k];
   for (int k=0;k<nvalues;k++) cout<<"k="<<k<<" energy dispersion parameter parameters_E_high="<< parameters_E_high[k]  <<endl;
  
   status=0; fits_get_colnum(fptr, CASEINSEN, "F", &colnum,&status);  cout<<"    column name status="<<status<<endl;  fits_report_error(stderr, status);
                                cout<<"reading F         from column "<<colnum<<endl;
   fits_read_col       (fptr, datatype, colnum,  firstrow,  firstelem, nelements, &nulval, values, &anynul, &status);
   for (int k=0;k<nvalues;k++) cout<<"k="<<k<<" values="<< values[k]  <<endl;
   for (int k=0;k<nvalues;k++) F[k]= values[k];
   for (int k=0;k<nvalues;k++) cout<<"k="<<k<<" energy dispersion parameter  F="<< F[k]  <<endl;
   
   status=0; fits_get_colnum(fptr, CASEINSEN, "S1", &colnum,&status);  cout<<"    column name status="<<status<<endl;  fits_report_error(stderr, status);
                                cout<<"reading S1        from column "<<colnum<<endl;
   fits_read_col       (fptr, datatype, colnum,  firstrow,  firstelem, nelements, &nulval, values, &anynul, &status);
   for (int k=0;k<nvalues;k++) cout<<"k="<<k<<" values="<< values[k]  <<endl;
   for (int k=0;k<nvalues;k++) S1[k]= values[k];
   for (int k=0;k<nvalues;k++) cout<<"k="<<k<<" energy dispersion parameter S1="<< S1[k]  <<endl;
  
   status=0; fits_get_colnum(fptr, CASEINSEN, "K1", &colnum,&status);  cout<<"    column name status="<<status<<endl;  fits_report_error(stderr, status);
                                cout<<"reading K1        from column "<<colnum<<endl;
   fits_read_col       (fptr, datatype, colnum,  firstrow,  firstelem, nelements, &nulval, values, &anynul, &status);
   for (int k=0;k<nvalues;k++) cout<<"k="<<k<<" values="<< values[k]  <<endl;
   for (int k=0;k<nvalues;k++) K1[k]= values[k];
   for (int k=0;k<nvalues;k++) cout<<"k="<<k<<" energy dispersion parameter K1="<< K1[k]  <<endl;

 
   status=0; fits_get_colnum(fptr, CASEINSEN, "BIAS1", &colnum,&status); cout<<"    column name BIAS1 status="<<status<<endl;   fits_report_error(stderr, status);
   if(status==COL_NOT_FOUND)
   {
   status=0; fits_get_colnum(fptr, CASEINSEN, "BIAS", &colnum,&status);  cout<<"    column name status="<<status<<endl;  fits_report_error(stderr, status);// NB column name BIAS not BIAS1, at present
   }
                                cout<<"reading BIAS1     from column "<<colnum<<" column name = BIAS"<<endl;
   fits_read_col       (fptr, datatype, colnum,  firstrow,  firstelem, nelements, &nulval, values, &anynul, &status);
   for (int k=0;k<nvalues;k++) cout<<"k="<<k<<" values="<< values[k]  <<endl;
   for (int k=0;k<nvalues;k++) BIAS1[k]= values[k];
   for (int k=0;k<nvalues;k++) cout<<"k="<<k<<" energy dispersion parameter BIAS1="<< BIAS1[k]  <<endl;

   status=0; fits_get_colnum(fptr, CASEINSEN, "BIAS2", &colnum,&status); cout<<"    column name status="<<status<<endl;  fits_report_error(stderr, status);
                                cout<<"reading BIAS2     from column "<<colnum<<endl;
   fits_read_col       (fptr, datatype, colnum,  firstrow,  firstelem, nelements, &nulval, values, &anynul, &status);
   for (int k=0;k<nvalues;k++) cout<<"k="<<k<<" values="<< values[k]  <<endl;
   for (int k=0;k<nvalues;k++) BIAS2[k]= values[k];
   for (int k=0;k<nvalues;k++) cout<<"k="<<k<<" energy dispersion parameter BIAS2="<< BIAS2[k]  <<endl;

   status=0; fits_get_colnum(fptr, CASEINSEN, "S2", &colnum,&status);  cout<<"    column name status="<<status<<endl;  fits_report_error(stderr, status);
                                cout<<"reading S2        from column "<<colnum<<endl;
   fits_read_col       (fptr, datatype, colnum,  firstrow,  firstelem, nelements, &nulval, values, &anynul, &status);
   for (int k=0;k<nvalues;k++) cout<<"k="<<k<<" values="<< values[k]  <<endl;
   for (int k=0;k<nvalues;k++) S2[k]= values[k];
   for (int k=0;k<nvalues;k++) cout<<"k="<<k<<" energy dispersion parameter S2="<< S2[k]  <<endl;
  
   status=0; fits_get_colnum(fptr, CASEINSEN, "K2", &colnum,&status);  cout<<"    column name status="<<status<<endl;  fits_report_error(stderr, status);
                                cout<<"reading K2        from column "<<colnum<<endl;
   fits_read_col       (fptr, datatype, colnum,  firstrow,  firstelem, nelements, &nulval, values, &anynul, &status);
   for (int k=0;k<nvalues;k++) cout<<"k="<<k<<" values="<< values[k]  <<endl;
   for (int k=0;k<nvalues;k++) K2[k]= values[k];
   for (int k=0;k<nvalues;k++) cout<<"k="<<k<<" energy dispersion parameter K2="<< K2[k]  <<endl;
 
   status=0; fits_get_colnum(fptr, CASEINSEN, "PINDEX1", &colnum,&status);  cout<<"    column name status="<<status<<endl;  fits_report_error(stderr, status);
                                cout<<"reading PINDEX1  from column "<<colnum<<endl;
   fits_read_col       (fptr, datatype, colnum,  firstrow,  firstelem, nelements, &nulval, values, &anynul, &status);
   for (int k=0;k<nvalues;k++) cout<<"k="<<k<<" values="<< values[k]  <<endl;
   for (int k=0;k<nvalues;k++) PINDEX1[k]= values[k];
   for (int k=0;k<nvalues;k++) cout<<"k="<<k<<" energy dispersion parameter PINDEX1="<< PINDEX1[k]  <<endl;

   status=0; fits_get_colnum(fptr, CASEINSEN, "PINDEX2", &colnum,&status);  cout<<"    column name status="<<status<<endl;  fits_report_error(stderr, status);
                                cout<<"reading PINDEX2  from column "<<colnum<<endl;
   fits_read_col       (fptr, datatype, colnum,  firstrow,  firstelem, nelements, &nulval, values, &anynul, &status);
   for (int k=0;k<nvalues;k++) cout<<"k="<<k<<" values="<< values[k]  <<endl;
   for (int k=0;k<nvalues;k++) PINDEX2[k]= values[k];
   for (int k=0;k<nvalues;k++) cout<<"k="<<k<<" energy dispersion parameter PINDEX2="<< PINDEX2[k]  <<endl;  fits_report_error(stderr, status);

   BIAS = (BIAS1+BIAS2)/2.; // estimate for compatibility with old formula with only BIAS, so it will work but is not correct 


  for(int i=0;i<n_parameter_sets;i++)
   cout<<" i="<<i<<" E= "<<parameters_E_low [i]<< " - "<<  parameters_E_high[i]
       << "  F="<<F[i]<<" S1="<<S1[i]<<" K1="<<K1[i]<<" BIAS1="<<BIAS1[i]<<" S2="<<S2[i]<<" K2="<<K2[i]<<" BIAS2="<<BIAS2[i]<<" PINDEX1="<<PINDEX1[i]<<" PINDEX2="<<PINDEX2[i]<<endl;
  
  for(int i=0;i<n_parameter_sets;i++) // to be eliminated eventually
   cout<<"for old parameters: i="<<i<<" E= "<<parameters_E_low [i]<< " - "<<  parameters_E_high[i]
       << "  F="<<F[i]<<" S1="<<S1[i]<<" K1="<<K1[i]<<" BIAS="<<BIAS[i]<<" S2="<<S2[i]<<" K2="<<K2[i]<<" PINDEX1="<<PINDEX1[i]<<" PINDEX2="<<PINDEX2[i]<<endl;



  parameters_E_min   =      parameters_E_low[0];                       // since need band within which E lies 
  parameters_E_factor=      parameters_E_low[1]/ parameters_E_low[0];  // assuming a log scale of E


  cout<<"parameters_E_min="<<parameters_E_min<<" parameters_E_factor="<<parameters_E_factor<<endl;

  //////// read scaling parameters for function Sd

  cout<<"reading scaling parameters for function Sd"<<endl;
  hdunum=3; // 1=primary, 3=2nd extension, assumed to be EDISP_SCALING_PARAMS
   
   status=0; 
   fits_movabs_hdu (fptr, hdunum, &hdutype,&status);   cout<<"FITS movabs_hdu status= "<<status<<endl;

   status=0;
   colnum=1;
   fits_get_coltype(fptr, colnum, &typecode, &repeat,&width, &status);

   nvalues  =repeat;
   nelements=repeat;
   cout<<"parameter file: number of scaling parameters for function Sd= "<<nvalues<<" (should be 7)" <<endl;   
   
   values = new double[nvalues];
   cout<<"reading scaling parameters for function Sd  from column "<<colnum<<endl;
   fits_read_col       (fptr, datatype, colnum,  firstrow,  firstelem, nelements, &nulval, values, &anynul, &status);
   for (int k=0;k<nvalues;k++) cout<<"scaling parameter for Sd,  k="<<k<<" values="<< values[k]  <<endl;
   Sd_parameters.resize(nvalues);
   for (int k=0;k<nvalues;k++) Sd_parameters[k]=values[k];
   for (int k=0;k<nvalues;k++) cout<<"scaling parameter for Sd,  k="<<k<<" Sd_parameters="<< Sd_parameters[k]  <<endl;

 } // parameter_file_type=="ST_FITS"

   ///////////////////////////////////////////////////////////

   parameters_initialized=1;

  cout<<"<< EnergyDispersionFermiLAT::read_parameters"<<endl;
  return status;
}

////////////////////////////////////////////////////////////////////////////

double EnergyDispersionFermiLAT::value_thibaut(double E_true,double E_meas, int debug)
{ 

  if(debug==1) cout<<">> value_thibaut "<<endl;

 
  if(debug==1) cout<<"parameters_initialized = "<<parameters_initialized<<endl;

  if(parameters_initialized!=1)
    {cout<<"parameters not initialized! parameters_initialized!=1!  initializing  now.."<<endl; read_parameters(debug); read_exposure(debug);} // required since initial read did not get values in class!

 double value_=0.;

  /*
  if(initialized!=1){cout<<" not initialized: initializing"<<endl; read(debug);}
  
  int iE_true=(log(E_true)-log_E_true_min)/dlog_E_true;
  int iE_meas=(log(E_meas)-log_E_meas_min)/dlog_E_meas;

  if(debug==1)cout<<"  E_true="<< E_true<<"  E_meas="<< E_meas;
  if(debug==1)cout<<" iE_true="<<iE_true<<" iE_meas="<<iE_meas;

  double value_=0.;

  if(iE_true<0||iE_true>nE_true-1  
   ||iE_meas<0||iE_meas>nE_meas-1) 
  {
   if(debug==1)cout<<" value_thibaut: EnergyDispersionMatrix outside range,  value=" << value_ <<endl;
   return value_;
  }

  value_ = EnergyDispersionMatrix[iE_true][iE_meas];

  if(debug==1)cout<<"  EnergyDispersionMatrix value_=" << value_ <<endl;

  */

 // check that  the parameters are available

   if(debug==1)
   {
        cout<<"parameters_initialized = "<<parameters_initialized<<endl;
        cout<<"F.size                 = "<<F.size()<<endl;
        cout<<"n_parameter_sets       = "<<n_parameter_sets<<endl;
        cout<<"parameters_E_min       = "<<parameters_E_min<<endl;
        cout<<"parameters_E_factor    = "<<parameters_E_factor<<endl;
   }

         int i=0;

	 i = log(E_true/parameters_E_min)/log(parameters_E_factor) + 1.0e-6; 


 
         if(i<0)                 {i=0;                   if(debug==1){cout<<" warning: E_true="<<E_true<< "  below parameter range, using minimum E_true available :"<<parameters_E_low[i] <<endl;}}
         if(i>n_parameter_sets-1){i=n_parameter_sets-1;  if(debug==1){cout<<" warning: E_true="<<E_true<< "  above parameter range, using maximum E_true available :"<<parameters_E_high[i]<<endl;}}

   if(debug==1)
   {
	 cout<<" value_thibaut: parameters for  E_true="<<E_true <<" i="<<i<<" E= "<<parameters_E_low [i]<< " - "<<  parameters_E_high[i]
       << "  F="<<F[i]<<" S1="<<S1[i]<<" K1="<<K1[i]<<" BIAS="<<BIAS[i] <<" BIAS1="<<BIAS1[i] <<" BIAS2="<<BIAS2[i] <<" S2="<<S2[i]<<" K2="<<K2[i]<<" PINDEX1="<<PINDEX1[i]<<" PINDEX2="<<PINDEX2[i]<<endl;
   }

         double x;

	 double Sd_=Sd(E_true,debug);

         x = (E_meas - E_true) / (E_true * Sd_);

	 //	 value_ = F * g(x,S1,K1,BIAS,PINDEX1) + (1. -F) * g(x,S2,K2,BIAS,PINDEX2) ;

       //value_ = F[i] * g(x,S1[i],K1[i],BIAS [i],PINDEX1[i],debug) + (1. -F[i]) * g(x,S2[i],K2[i],BIAS [i],PINDEX2[i],debug) ; //AWS20150220
	 value_ = F[i] * g(x,S1[i],K1[i],BIAS1[i],PINDEX1[i],debug) + (1. -F[i]) * g(x,S2[i],K2[i],BIAS2[i],PINDEX2[i],debug) ; //AWS20150220 new formula

	 if(debug==1)cout<<" value_thibaut test: E_true="<<E_true<<" E_meas="<<E_meas<<" x="<<x<<" F[i]="<<F[i]<< " g_proposed value_=" << value_ <<endl;


 // convert from P(x) to P(E_meas|E_true), MeV^-1. x = (E_meas-E_true)/(E_true * Sd)

         value_/=(E_true*Sd_);

         double Aeff_E_true = fermi_Aeff.Aeff_average_interpolated (E_true, debug) ;
         double Aeff_E_meas = fermi_Aeff.Aeff_average_interpolated (E_meas, debug) ;

         double Aeff_factor=1;
	 if(use_Aeff==1)
	 {
	  if(E_true >= E_true_threshold && E_meas >= E_meas_threshold) //AWS20150301 
               Aeff_factor = Aeff_E_true /  Aeff_E_meas;               //AWS20150223
	  else Aeff_factor = 0.;                                       //AWS20150223
         }

         value_ *= Aeff_factor;

	 if(debug==1)cout<<" value_thibaut: E_true="<<E_true<<" E_meas="<<E_meas<<" x="<<x<<" Sd_="<<Sd_
                         <<" use_Aeff="<< use_Aeff<<" Aeff_E_true="<< Aeff_E_true  <<" Aeff_E_meas="<< Aeff_E_meas  <<" Aeff_factor="<<Aeff_factor<< " value_ MeV-1 =" << value_ <<endl;

  return value_;
}

///////////////////////////////////////////////////////////////////////////
double  EnergyDispersionFermiLAT::g(double x, double sigma, double k, double b, double p,int debug)
{
 double value_=0.;

 double u,up,um;

 u = x-b; 
 if (u>=0.) up=  u; else up=0.;
 if (u<=0.) um= -u; else um=0.;

 double Gamma=1.; // Gamma function of 1/p. 

 gsl_sf_result result;
 gsl_set_error_handler_off(); // since we check the status and act on it
 int status;

 /* test gsl error handler by giving invalid argument <0
 status=0;
 status=gsl_sf_gamma_e(-2., &result);
 if(status!=0){ cout<<"g: gsl_sf_gamma_e status="<<status<<endl; return value_;}
 */

 status=0;
 status=gsl_sf_gamma_e(1./p, &result);

 // strangely it returns status=1 even though values look correct and the error handler does not report any problem. Hence ignore status for now.
 // if(status!=0){ cout<<"g: gsl_sf_gamma_e status="<<status<<endl; return value_;}

 Gamma=result.val;

 value_ = p/sigma/Gamma * k / (1. + k*k)
           * exp( -     pow(k,p)/pow(sigma,p)  * pow(up, p)
                  - 1./(pow(k,p)*pow(sigma,p)) * pow(um, p) )    ;

 if(debug==1) cout<<" EnergyDispersionFermiLAT::g : x="<<x<< " sigma="<<sigma<<" k="<<k<<" b="<<b<<" p="<<p<<" Gamma="<<Gamma<<"  u="<<u<<" up="<<up<<" um="<<um<<"   value_="<<value_<<endl;


 return value_;
}

//////////////////////////////////////////////////////////////////////////////
double  EnergyDispersionFermiLAT::Sd(double E, int debug)
{
  /* from Thibaut 
#Energy dependent only

pars=["4.69348e+04","-1.10280e+01","2.50445e+00","4.85432e-02","1.20464e+04","1.09158e+01","-9.43822e-01"]

g1 = pars[0]+"*TMath::Exp(-(X-"+pars[1]+")*(X-"+pars[1]+")/(2*"+pars[2]+"*"+pars[2]+"))+"+pars[3]

g2 = pars[4]+"*TMath::Exp(-(X-"+pars[5]+")*(X-"+pars[5]+")/(2*"+pars[6]+"*"+pars[6]+"))"

gg = g1+"+"+g2

tree.SetAlias('Sd',gg)
  */

 double value_=0.;

 double pars[]={4.69348e+04, -1.10280e+01, 2.50445e+00, 4.85432e-02, 1.20464e+04, 1.09158e+01, -9.43822e-01};

 // case where parameters are read from ST FITS file
 if(parameter_file_type=="ST_FITS")
 {
   for (int i=0; i<=6; i++) pars[i] = Sd_parameters[i];

   if (debug==1){ cout<<"Sd parameters from ST FITS file: "; for (int i=0; i<=6; i++)  cout<<pars[i]<<" "; cout<<endl;}
 }

 double X=log10(E);

 double g1 = pars[0] * exp(-(X-pars[1]) * (X-pars[1]) / (2*pars[2]*pars[2])) + pars[3];
 double g2 = pars[4] * exp(-(X-pars[5]) * (X-pars[5]) / (2*pars[6]*pars[6]))          ;

 value_= g1 + g2;

 if(debug==1) cout<<"Sd: g1="<<g1<<" g2="<<g2<<" value_="<<value_<<endl;

  return value_;
}

////////////////////////////////////////////////////////////////////////////
int  EnergyDispersionFermiLAT::read_exposure(int debug)
{
  int status=0;

  cout<<">> EnergyDispersionFermiLAT::read_exposure"<<endl;
  cout<<"reading exposure from file "<<exposure_file_name<<" exposure_file_type="<<exposure_file_type<<endl;

  //  fermi_Aeff.read(exposure_file_name, debug);
  fermi_Aeff.read(exposure_file_name, exposure_file_type,debug);


  cout<<"<< EnergyDispersionFermiLAT::read_exposure"<<endl;
  return status;
}

////////////////////////////////////////////////////////////////////////////
  void  EnergyDispersionFermiLAT::ApplyEnergyDispersion(valarray<double> E,  valarray<double> &spectrum, int debug)
{
  // in-place application of energy dispersion to spectrum

 int nE=spectrum.size();

  // generate matrix if not yet initialized

 if(EnergyDispersionMatrix2_initialized!=1)
 {
 cout<<"ApplyEnergyDispersion in-place: generating matrix"<<endl;

  EnergyDispersionMatrix2.resize(nE);
  for(int i=0;i<nE;i++)
  {
   EnergyDispersionMatrix2[i].resize(nE);

   for(int j=0;j<nE;j++)
   {   

     double E_true_=E[i];
     double E_meas_=E[j];
     double value_;
     if(method=="rsp"    ) value_=value        (E_true_,E_meas_,debug); // matrix element per MeV of E_meas
     if(method=="thibaut") value_=value_thibaut(E_true_,E_meas_,debug); // matrix element per MeV of E_meas

              EnergyDispersionMatrix2[i][j] = value_ * E_true_;  // E_true_: since log integration
   }
  }

  EnergyDispersionMatrix2_initialized=1;
  cout<<"ApplyEnergyDispersion in-place: generating matrix complete"<<endl;



 } //EnergyDispersionMatrix2_initialized!=1


  
  valarray<double> work;
  work.resize(nE);  
  work = 0.;




  if(use_matrix==1)

  // convolution using matrix
  for (int j=0;j<nE;j++)
  for (int i=0;i<nE;i++)
  {
    work[j] += spectrum[i] *   EnergyDispersionMatrix2[i][j] ;  

  } // use_matrix==1


  if(use_matrix==0)
    // convolution with function evaluation each time, use to check against fast matrix method
  for (int j=0;j<nE;j++)
  for (int i=0;i<nE;i++)
  {
     double E_true_=E[i];
     double E_meas_=E[j];
     double value_;
     if(method=="rsp"    ) value_=value        (E_true_,E_meas_,debug); // matrix element per MeV of E_meas
     if(method=="thibaut") value_=value_thibaut(E_true_,E_meas_,debug); // matrix element per MeV of E_meas

     work[j] += spectrum[i] * value_ * E_true_;  // E_true_: since log integration

     //    cout<<"ApplyEnergyDispersion : i="<<i<<" E_true_="<<E_true_<<" j="<<j<<"  E_meas_="<<E_meas_<< " spectrum input = "<<spectrum[i]<<" value_="<<value_<<" output =  "<<work[j]<<endl;

  } // use_matrix==0


  //////////////////////////// complete the calculation


  work *= log(E[1]/E[0]); // log integration

  double  input_sum=0;
  double output_sum=0;
  for (int i=0;i<nE;i++){input_sum+=spectrum[i]*E[i];  output_sum+=work[i]*E[i];}

   input_sum *= log(E[1]/E[0]); // log integration
  output_sum *= log(E[1]/E[0]); // log integration

  if(debug==1 || debug==2)
    for (int i=0;i<nE;i++) cout<<"ApplyEnergyDispersion, in-place : i="<<i<<"  E[i]="<<E[i]<<" spectrum input = "<<spectrum[i] <<" output = "<<work[i]<<" output/input="<<work[i]/spectrum[i]<<endl;

  if(debug==1 || debug==2) cout<<"ApplyEnergyDispersion, in-place : integrals:  input="<<input_sum<<" output="<<output_sum<<" output/input="<<output_sum/input_sum<<endl;
  
  spectrum = work; // in-place result

  return;
}


 

////////////////////////////////////////////////////////////////////////////
  
void  EnergyDispersionFermiLAT::ApplyEnergyDispersion(valarray<double> E,  valarray<double> &spectrum, double E_interp_factor,int debug)
{
  //  in-place application of energy dispersion to spectrum with interpolation of input spectrum on finer grid 
  if(debug==1||debug==2)      cout<<"ApplyEnergyDispersion, in-place with interpolation"<<endl;

  // follow notation of general case

  valarray<double> spectrum_true;
  valarray<double> spectrum_meas;
  valarray<double> E_true_;
  valarray<double> E_meas_;
  // int nE_true, nE_meas; in class header!

  // test case with no interpolation : use input spectrum directly, result should be same as normal in-place method
  int test_no_interp =0;
  if (test_no_interp==1)
  {
   nE_true=spectrum.size();
   E_true_      .resize(nE_true);
   spectrum_true.resize(nE_true);

   E_true_      = E;
   spectrum_true=spectrum;
  }

  else
  {
    // interpolate input spectrum
    if(debug==1||debug==2)      cout<<"ApplyEnergyDispersion, in-place with interpolation: E_interp_factor="<<E_interp_factor <<endl;
    double E_factor_input=E[1]/E[0];
    int n_interp=log(E_factor_input)/log(E_interp_factor) + 1.0e-6; 
    double E_interp_factor_adj; // adjusted to exactly n_interp points per input energy 
    E_interp_factor_adj=exp(log(E_factor_input)/n_interp);

  
 
  nE_true=(spectrum.size()-1) * n_interp + 1 ; //AWS20140803

  if(debug==1||debug==2)   cout<<"ApplyEnergyDispersion, in-place with interpolation: E_factor_input="<<E_factor_input<<"  E_interp_factor="<<E_interp_factor
                               <<" n_interp="<<n_interp<<"  E_interp_factor_adj="<<E_interp_factor_adj<<" no. points in input spectrum:"<<spectrum.size()<< ", in interpolated spectrum:"<<nE_true <<endl;

   E_true_      .resize(nE_true);
   spectrum_true.resize(nE_true);

   for(int i=0;i<nE_true;i++) E_true_[i]      = E[0] * pow(E_interp_factor_adj,i);

   // uniform log interpolation between input energies
   
   spectrum_true=0.;                                                                            //AWS20150212

   int j=0;
   for(int i=0;i<spectrum.size();i++) //AWS20140803
   {
     double delta =0.;
     if(spectrum[i]>0. && spectrum[i+1]>0.) delta = log(spectrum[i+1]/spectrum[i]) / n_interp;  //AWS20150212 avoid problems with zero

     for(int i_interp=0;i_interp<n_interp;i_interp++)
     {
      if(j<nE_true )
      {
       if(spectrum[i]>0. && spectrum[i+1]>0.) spectrum_true[j]=exp(log(spectrum[i]) + delta*i_interp); //AWS20150212 avoid problems with zero

       if(debug==1||debug==2) cout<<"ApplyEnergyDispersion, in-place with interpolation : i="<<i<<" E=      " <<E     [i]<<"        input  spectrum     [i]="<<spectrum     [i]<<endl;
       if(debug==1||debug==2) cout<<"ApplyEnergyDispersion, in-place with interpolation : j="<<j<<" E_true_="<<E_true_[j]<<" interpolated  spectrum_true[j]="<<spectrum_true[j]<<endl;

      }
       j++;

     }
   }

 if(debug==1||debug==2)  for(int i=0;i<spectrum.size();i++) cout<<"ApplyEnergyDispersion, in-place with interpolation : i="<<i<<" E=      " <<E     [i]<<       " input  spectrum     [i]="<<spectrum     [i]<<endl;
 if(debug==1||debug==2)  for(int j=0;j<nE_true;        j++) cout<<"ApplyEnergyDispersion, in-place with interpolation : j="<<j<<" E_true_="<<E_true_[j]<<" interpolated  spectrum_true[j]="<<spectrum_true[j]<<endl;

  

  }

  nE_meas=spectrum.size();
  E_meas_      .resize(nE_meas);
  spectrum_meas.resize(nE_meas);

  E_meas_= E;

 // generate matrix if not yet initialized

 if(EnergyDispersionMatrix2_initialized!=1)
 {
 cout<<"ApplyEnergyDispersion, in-place with interpolation: generating matrix"<<endl;

  EnergyDispersionMatrix2.resize(nE_true);

  for(int i=0;i<nE_true;i++)
  {
   EnergyDispersionMatrix2[i].resize(nE_meas);

   for(int j=0;j<nE_meas;j++)
   {   

     
     double value_;
     if(method=="rsp"    ) value_=value        (E_true_[i],E_meas_[j],debug); // matrix element per MeV of E_meas
     if(method=="thibaut") value_=value_thibaut(E_true_[i],E_meas_[j],debug); // matrix element per MeV of E_meas

              EnergyDispersionMatrix2[i][j] = value_ * E_true_[i];  // E_true_: since log integration

     if(debug==1) cout<<"ApplyEnergyDispersion, in-place with interpolation, matrix generation : i="<<i<<" E_true_="<<E_true_[i]<<" j="<<j<<"  E_meas_="<<E_meas_[j]<<" EnergyDispersionMatrix2[i][j]="<<EnergyDispersionMatrix2[i][j]<<endl;
   }
  }

  EnergyDispersionMatrix2_initialized=1;
  cout<<"ApplyEnergyDispersion, in-place with interpolation: generating matrix complete"<<endl;



 } //EnergyDispersionMatrix2_initialized!=1
    
  spectrum_meas = 0.;

  if(use_matrix==1)
  for (int j=0;j<nE_meas;j++)
  for (int i=0;i<nE_true;i++)
  {
    spectrum_meas[j] += spectrum_true[i] *  EnergyDispersionMatrix2[i][j];  

    if(debug==1)      cout<<"ApplyEnergyDispersion, in-place with interpolation : i="<<i<<" E_true_="<<E_true_[i]<<" j="<<j<<"  E_meas_="<<E_meas_[j]<< " spectrum input = "<<spectrum_true[i]<<" EnergyDispersionMatrix2[i][j]="<<EnergyDispersionMatrix2[i][j]<<" output =  "<<spectrum_meas[j]<<endl;
  }


  if(use_matrix==0)
  for (int j=0;j<nE_meas;j++)
  for (int i=0;i<nE_true;i++)
  {
     
    //    double value_     = value(E_true_[i], E_meas_[j], debug);    // matrix element per MeV of E_meas_
     double value_;

     if(method=="rsp")    value_     = value        (E_true_[i], E_meas_[j], debug);    // matrix element per MeV of E_meas_
     if(method=="thibaut")value_     = value_thibaut(E_true_[i], E_meas_[j], debug);    // matrix element per MeV of E_meas_

     spectrum_meas[j] += spectrum_true[i] * value_ * E_true_[i];  // E_true_: since log integration

     if(debug==1)      cout<<"ApplyEnergyDispersion, in-place with interpolation : i="<<i<<" E_true_="<<E_true_[i]<<" j="<<j<<"  E_meas_="<<E_meas_[j]<< " spectrum input = "<<spectrum_true[i]<<" value_="<<value_<<" output =  "<<spectrum_meas[j]<<endl;
  }


  // complete calculation

    spectrum_meas *= log(E_true_[1]/E_true_[0]); // log integration


    double  input_sum=0;      // interpolated spectrum
    double  input_sum_orig=0; //     original spectrum
    double output_sum=0;

  for (int i=0;i<nE_true;i++)         input_sum     += spectrum_true[i]*E_true_[i];
  for (int i=0;i<spectrum.size();i++) input_sum_orig+= spectrum     [i]*      E[i];
  for (int j=0;j<nE_meas;j++)        output_sum     += spectrum_meas[j]*E_meas_[j];



   input_sum      *= log(E_true_[1]/E_true_[0]); // log integration
   input_sum_orig *= log(      E[1]/      E[0]); // log integration
  output_sum      *= log(E_meas_[1]/E_meas_[0]); // log integration


 if(debug==1||debug==2)
 {
  for (int i=0;i<nE_true;i++)cout<<"ApplyEnergyDispersion, in-place with interpolation : i="<<i<<"  E_true_[i]="<<E_true_[i]<<" spectrum input  = "<<spectrum_true[i] <<endl;
  for (int j=0;j<nE_meas;j++)cout<<"ApplyEnergyDispersion, in-place with interpolation : j="<<j<<"  E_meas_[j]="<<E_meas_[j]<<" spectrum output = "<<spectrum_meas[j] <<endl;
    
  cout<<"ApplyEnergyDispersion, in-place with interpolation :  integration input interpolated="<<input_sum     <<" output="<<output_sum<<" output/input integrals="<<output_sum/input_sum     <<endl;
  cout<<"ApplyEnergyDispersion, in-place with interpolation :  integration input     original="<<input_sum_orig<<" output="<<output_sum<<" output/input integrals="<<output_sum/input_sum_orig<<endl;

  cout<<"ApplyEnergyDispersion, in-place with interpolation: true sum="<<spectrum_true.sum()<<" meas sum="<<spectrum_meas.sum()<<" dlogtrue="<<  log(E_true_[1]/E_true_[0])<<" dlogmeas="<<  log(E_meas_[1]/E_meas_[0])  <<endl;
 }



    spectrum = spectrum_meas; // in-place output spectrum

  return;
}

////////////////////////////////////////////////////////////////////////////
  void  EnergyDispersionFermiLAT::ApplyEnergyDispersion(valarray<double> E_true_,  valarray<double>  spectrum_true,valarray<double> E_meas_,  valarray<double> &spectrum_meas, int debug)
{
  //  application of energy dispersion to spectrum with independent E_true_, E_meas_
  // NB watch for confusion with E_true, E_meas used in the matrix !  and nE_true, nE_meas !




  nE_true = spectrum_true.size(); // in class header
  nE_meas = spectrum_meas.size(); // in class header


 // generate matrix if not yet initialized

 if(EnergyDispersionMatrix2_initialized!=1)
 {
 cout<<"ApplyEnergyDispersion general case: generating matrix"<<endl;

  EnergyDispersionMatrix2.resize(nE_true);

  for(int i=0;i<nE_true;i++)
  {
   EnergyDispersionMatrix2[i].resize(nE_meas);

   for(int j=0;j<nE_meas;j++)
   {   

     
     double value_;
     if(method=="rsp"    ) value_=value        (E_true_[i],E_meas_[j],debug); // matrix element per MeV of E_meas
     if(method=="thibaut") value_=value_thibaut(E_true_[i],E_meas_[j],debug); // matrix element per MeV of E_meas

              EnergyDispersionMatrix2[i][j] = value_ * E_true_[i];  // E_true_: since log integration
   }
  }

  EnergyDispersionMatrix2_initialized=1;
  cout<<"ApplyEnergyDispersion general case: generating matrix complete"<<endl;



 } //EnergyDispersionMatrix2_initialized!=1
    
  spectrum_meas = 0.;

  if(use_matrix==1)
  for (int j=0;j<nE_meas;j++)
  for (int i=0;i<nE_true;i++)
  {
    spectrum_meas[j] += spectrum_true[i] *  EnergyDispersionMatrix2[i][j];  

    if(debug==1)      cout<<"ApplyEnergyDispersion, general case : i="<<i<<" E_true_="<<E_true_[i]<<" j="<<j<<"  E_meas_="<<E_meas_[j]<< " spectrum input = "<<spectrum_true[i]<<" EnergyDispersionMatrix2[i][j]="<<EnergyDispersionMatrix2[i][j]<<" output =  "<<spectrum_meas[j]<<endl;
  }


  if(use_matrix==0)
  for (int j=0;j<nE_meas;j++)
  for (int i=0;i<nE_true;i++)
  {
     
    //    double value_     = value(E_true_[i], E_meas_[j], debug);    // matrix element per MeV of E_meas_
     double value_;

     if(method=="rsp")    value_     = value        (E_true_[i], E_meas_[j], debug);    // matrix element per MeV of E_meas_
     if(method=="thibaut")value_     = value_thibaut(E_true_[i], E_meas_[j], debug);    // matrix element per MeV of E_meas_

     spectrum_meas[j] += spectrum_true[i] * value_ * E_true_[i];  // E_true_: since log integration

     if(debug==1)      cout<<"ApplyEnergyDispersion, general case : i="<<i<<" E_true_="<<E_true_[i]<<" j="<<j<<"  E_meas_="<<E_meas_[j]<< " spectrum input = "<<spectrum_true[i]<<" value_="<<value_<<" output =  "<<spectrum_meas[j]<<endl;
  }


  // complete calculation

    spectrum_meas *= log(E_true_[1]/E_true_[0]); // log integration

 

  double  input_sum=0;
  double output_sum=0;

  for (int i=0;i<nE_true;i++) input_sum+=spectrum_true[i]*E_true_[i];
  for (int j=0;j<nE_meas;j++)output_sum+=spectrum_meas[j]*E_meas_[j];  

   input_sum *= log(E_true_[1]/E_true_[0]); // log integration
  output_sum *= log(E_meas_[1]/E_meas_[0]); // log integration


 if(debug==1||debug==2)
 {
  for (int i=0;i<nE_true;i++)cout<<"ApplyEnergyDispersion, general case : i="<<i<<"  E_true_[i]="<<E_true_[i]<<" spectrum input  = "<<spectrum_true[i] <<endl;
  for (int j=0;j<nE_meas;j++)cout<<"ApplyEnergyDispersion, general case : j="<<j<<"  E_meas_[j]="<<E_meas_[j]<<" spectrum output = "<<spectrum_meas[j] <<endl;
    
  cout<<"ApplyEnergyDispersion, general case :  integration input="<<input_sum<<" output="<<output_sum<<" output/input integrals="<<output_sum/input_sum<<endl;
  cout<<"ApplyEnergyDispersion, general case : true sum="<<spectrum_true.sum()<<" meas sum="<<spectrum_meas.sum()<<" dlogtrue="<<  log(E_true_[1]/E_true_[0])<<" dlogmeas="<<  log(E_meas_[1]/E_meas_[0])  <<endl;
 }

  return;
}


////////////////////////////////////////////////////////////////////////////
int EnergyDispersionFermiLAT::set_method(string method_)
{
  cout<<">>  EnergyDispersionFermiLAT::set_method"<<endl;
  int status=0;
  method=method_;
  cout<<"  EnergyDispersionFermiLAT::set_method: method="<<method<<endl;
  if(method!="rsp" && method!="thibaut") {status=1; cout<<"  EnergyDispersionFermiLAT::set_method: invalid method!"<<endl;}
  return status;
}

////////////////////////////////////////////////////////////////////////////
int EnergyDispersionFermiLAT::set_use_Aeff(int use_Aeff_)
{
  cout<<">>  EnergyDispersionFermiLAT::set_use_Aeff"<<endl;
  int status=0;
  use_Aeff=use_Aeff_;
  cout<<"  EnergyDispersionFermiLAT::set_use_Aeff: use_Aeff="<<use_Aeff<<endl;
  if(use_Aeff!=0 && use_Aeff!=1) {status=1; cout<<"  EnergyDispersionFermiLAT::set_use_Aeff: invalid value!"<<endl;}
  return status;
}
////////////////////////////////////////////////////////////////////////////
int EnergyDispersionFermiLAT::set_E_true_threshold(double E_true_threshold_)
{
  cout<<">>  EnergyDispersionFermiLAT::set_E_true_threshold"<<endl;
  int status=0;
  E_true_threshold=E_true_threshold_;
  cout<<"  EnergyDispersionFermiLAT::set_E_true_threshold:E_true_threshold = "<<E_true_threshold<<endl;
  
  return status;
}
////////////////////////////////////////////////////////////////////////////
int EnergyDispersionFermiLAT::set_E_meas_threshold(double E_meas_threshold_)
{
  cout<<">>  EnergyDispersionFermiLAT::set_E_meas_threshold"<<endl;
  int status=0;
  E_meas_threshold=E_meas_threshold_;
  cout<<"  EnergyDispersionFermiLAT::set_E_meas_threshold:E_meas_threshold = "<<E_meas_threshold<<endl;
  
  return status;
}
/////////////////////////////////////////////////////////////////////////////
int EnergyDispersionFermiLAT::set_parameter_file(string parameter_file_name_)
{
  cout<<">>  EnergyDispersionFermiLAT::set_parameter_file"<<endl;
  int status=0;
  if  (parameter_file_name_=="default") parameter_file_name= "Edisp_P8V1_SOURCE.txt";          // starts at 10.0 MeV
//if  (parameter_file_name_=="default") parameter_file_name= "Edisp_P8V1_SOURCE_extended.txt"; // starts at  3.2 MeV : use with caution!

  else                                  parameter_file_name=parameter_file_name_;

  if  (parameter_file_name_=="default")cout<<"  EnergyDispersionFermiLAT::set_parameter_file: using default parameter file!"<<endl;
  cout<<"  EnergyDispersionFermiLAT::set_parameter_file: parameter_file_name="<<parameter_file_name<<endl;
 
  return status;
}
/////////////////////////////////////////////////////////////////////////////
int EnergyDispersionFermiLAT::set_parameter_file_type(string parameter_file_type_)
{
  cout<<">>  EnergyDispersionFermiLAT::set_parameter_file_type"<<endl;
  int status=0;
  if  (parameter_file_type_=="default") parameter_file_type= "txt";       // later set to ST, FITS 


  else                                  parameter_file_type=parameter_file_type_;

  if  (parameter_file_type_=="default")cout<<"  EnergyDispersionFermiLAT::set_parameter_file_type: using default parameter file type!"<<endl;
  cout<<"  EnergyDispersionFermiLAT::set_parameter_file_type: parameter_file_type="<<parameter_file_type<<endl;
 
  return status;
}
/////////////////////////////////////////////////////////////////////////////
int EnergyDispersionFermiLAT::set_exposure_file(string exposure_file_name_)
{
  cout<<">>  EnergyDispersionFermiLAT::set_exposure_file"<<endl;
  int status=0;
  if  (exposure_file_name_=="default") exposure_file_name = "expo_20_204800_1000E.fits.gz";


  else                                  exposure_file_name = exposure_file_name_;

  if  (exposure_file_name_=="default")cout<<"  EnergyDispersionFermiLAT::set_exposure_file: using default exposure file!"<<endl;
  cout<<"  EnergyDispersionFermiLAT::set_exposure_file: exposure_file_name="<<exposure_file_name<<endl;
 
  return status;
}

/////////////////////////////////////////////////////////////////////////////
int EnergyDispersionFermiLAT::set_exposure_file_type(string exposure_file_type_)
{
  cout<<">>  EnergyDispersionFermiLAT::set_exposure_file_type"<<endl;
  int status=0;
  if  (exposure_file_type_=="default") exposure_file_type = "mapcube";


  else                                 exposure_file_type = exposure_file_type_;

  if  (exposure_file_type_=="default")cout<<"  EnergyDispersionFermiLAT::set_exposure_file_type: using default exposure file type!"<<endl;
  cout<<"  EnergyDispersionFermiLAT::set_exposure_file_type: exposure_file_type="<<exposure_file_type<<endl;
 
  return status;
}

//////////////////////////////////////////////////////////////////////////
int EnergyDispersionFermiLAT::set_use_matrix(int use_matrix_)
{
  cout<<">>  EnergyDispersionFermiLAT::set_use_matrix"<<endl;
  
 use_matrix=use_matrix_;

 if(use_matrix==0) cout<<">>  EnergyDispersionFermiLAT::set_use_matrix: matrix WILL NOT be used in  ApplyEnergyDispersion !"<<endl;
 if(use_matrix==1) cout<<">>  EnergyDispersionFermiLAT::set_use_matrix: matrix WILL     be used in  ApplyEnergyDispersion !"<<endl;
 
 
  int status=0;
  return status;
}
//////////////////////////////////////////////////////////////////////////
int EnergyDispersionFermiLAT::reset()
{
  cout<<">>  EnergyDispersionFermiLAT::reset"<<endl;
  cout<<">>  EnergyDispersionFermiLAT::reset: matrix will be re-initialized for new energies on next call  of ApplyEnergyDispersion !"<<endl;

  EnergyDispersionMatrix2_initialized = 0;

  cout<<">>  EnergyDispersionFermiLAT::reset: energy dispersion parameters will be read       on next call of ApplyEnergyDispersion !"<<endl;
  parameters_initialized = 0;

  cout<<">>  EnergyDispersionFermiLAT::reset: matrix will be used in                                          ApplyEnergyDispersion !"<<endl;
  set_use_matrix(1);

  set_parameter_file     ("default");
  set_parameter_file_type("default");
  set_exposure_file      ("default"); 
  set_exposure_file_type ("default"); 
  set_method             ("thibaut");
  set_use_Aeff           (1);
  set_E_true_threshold   (5.);

  int healpix_reset =1; // for testing
  if (healpix_reset==1)
  {
   set_exposure_file ("/afs/ipp-garching.mpg.de/home/a/aws/propagate/c/FITS/GLAST/p8_thibaut/exp2_healpix7_andy.fits");
   set_exposure_file_type("healpix");
  }

  // instead, test ST FITS format
  int ST_FITS_reset =1;
  if( ST_FITS_reset==1)
  {
   set_parameter_file("/afs/ipp-garching.mpg.de/home/a/aws/propagate/c/FITS/GLAST/p8_thibaut/edisp_P8V1_ULTRACLEAN_EDISP3.fits");
   set_parameter_file_type("ST_FITS");
  }


  int status=0;
  return status;
}
////////////////////////////////////////////////////////////////////////////
void EnergyDispersionFermiLAT::test()
{
  cout<<">>  EnergyDispersionFermiLAT::test"<<endl;
  
  int test_init_read       =0;
  int test_in_place        =0;
  int test_in_place_interp =0;


  EnergyDispersionFermiLAT energyDispersion;
  int debug=1;


  double E_true=100;
  double E_meas=200;
  double E_meas_factor=1.050;
  debug=0;
  double value;
  double value_thibaut_;
  
 

  //////////////////////////////////////////////////////////////////////
  // set initialization functions and reading

 if(test_init_read==1)
 {
  debug=1;
  set_method("test invalid method"); // test with invalid method
  set_method("rsp");
  set_method("thibaut");
  set_use_Aeff(1);

  cout<<endl<< "test read_parameters:"<<endl;
  set_parameter_file("Edisp_P8V1_SOURCE.txt");
  set_parameter_file_type("default");
  read_parameters(debug);

  set_parameter_file("/afs/ipp-garching.mpg.de/home/a/aws/propagate/c/FITS/GLAST/p8_thibaut/edisp_P8V1_ULTRACLEAN_EDISP3.fits");
  set_parameter_file_type("ST_FITS");
  read_parameters(debug);

  set_parameter_file_type("txt");
  set_parameter_file("Edisp_P8V1_SOURCE_extended.txt");
  read_parameters(debug);

  // mapcube exposure file
  set_exposure_file     ("default");
  set_exposure_file_type("default");
  read_exposure(debug);

  // healpix exposure file
  set_exposure_file ("/afs/ipp-garching.mpg.de/home/a/aws/propagate/c/FITS/GLAST/p8_thibaut/exp2_healpix7_andy.fits");
  set_exposure_file_type("healpix");
  read_exposure(debug);

  cout<<"fermi_Aeff test: "<<fermi_Aeff.Aeff_average_interpolated (100., debug)<<endl;

 cout<<F.size()<<endl;
 cout<<n_parameter_sets<<endl;
 cout<<parameters_initialized<<endl;
  for(int i=0;i<n_parameter_sets;i++)
   cout<<" i="<<i<<" E= "<<parameters_E_low [i]<< " - "<<  parameters_E_high[i]
       << "  F="<<F[i]<<" S1="<<S1[i]<<" K1="<<K1[i]<<" BIAS="<<BIAS[i]<<" S2="<<S2[i]<<" K2="<<K2[i]<<" PINDEX1="<<PINDEX1[i]<<" PINDEX2="<<PINDEX2[i]<<endl;
 } // test_init_read

  //////////////////////////////////////////////////////////////////

  cout<<"initializing for  test loop"<<endl;

  reset();
  set_method("rsp");
  set_method("thibaut");
  set_E_true_threshold(6.);
  set_E_true_threshold(7.);

  int choose_parameter_file_type=2;

  if(choose_parameter_file_type==1)
 {
  set_parameter_file("Edisp_P8V1_SOURCE.txt");
  set_parameter_file_type("txt");

 }
    if(choose_parameter_file_type==2)
 {
  set_parameter_file("/afs/ipp-garching.mpg.de/home/a/aws/propagate/c/FITS/GLAST/p8_thibaut/edisp_P8V1_ULTRACLEAN_EDISP3.fits");
  set_parameter_file_type("ST_FITS");
  }

  set_use_Aeff(0);

  cout<<"parameters_initialized="<<parameters_initialized<<endl;

  debug=1;
  cout<<"starting test loop without Aeff factor"<<endl;

  for(E_true=5;E_true<1e6;E_true*=1.5)
  {
  
   double sum=0;
   double sum1=0;
  for(E_meas=1 ;E_meas<1e6;E_meas*=E_meas_factor)
  {
   
    //   value_thibaut_=energyDispersion.value_thibaut(E_true,E_meas,debug); //energyDispersion not initialized! use *this

  value_thibaut_=value_thibaut(E_true,E_meas,debug);

   double dE_meas=E_meas*(sqrt(E_meas_factor) - 1./sqrt(E_meas_factor) );
   sum +=value_thibaut_ * dE_meas;
   sum1+=value_thibaut_ *  E_meas;
   cout<<" test  E_true="<< E_true<<"  E_meas="<< E_meas<<" value_thibaut="<<value_thibaut_<<endl;
  }
  sum1*=log(E_meas_factor); // log integration more accurate
  cout<<"without Aeff factor: E_true="<<E_true<<"  sum (value_thibaut_*dE_meas) ="<<sum<<"    log (E factor) * sum (value_thibaut_*E_meas) = "<<sum1<<endl;

 }


  set_use_Aeff(1);

  cout<<"parameters_initialized="<<parameters_initialized<<endl;

  debug=1;
  cout<<"starting test loop with    Aeff factor"<<endl;

  for(E_true=5;E_true<1e6;E_true*=1.5)
  {
  
   double sum=0;
   double sum1=0;
  for(E_meas=1 ;E_meas<1e6;E_meas*=E_meas_factor)
  {
   
    //   value_thibaut_=energyDispersion.value_thibaut(E_true,E_meas,debug); //energyDispersion not initialized! use *this

  value_thibaut_=value_thibaut(E_true,E_meas,debug);

   double dE_meas=E_meas*(sqrt(E_meas_factor) - 1./sqrt(E_meas_factor) );
   sum +=value_thibaut_ * dE_meas;
   sum1+=value_thibaut_ *  E_meas;
   cout<<" test  E_true="<< E_true<<"  E_meas="<< E_meas<<" value_thibaut="<<value_thibaut_<<endl;
  }
  sum1*=log(E_meas_factor); // log integration more accurate
  cout<<"with Aeff factor: E_true="<<E_true<<"  sum (value_thibaut_*dE_meas) ="<<sum<<"    log (E factor) * sum (value_thibaut_*E_meas) = "<<sum1<<endl;

 }
   return;

  /////////////////////////////////////////////////////////

  set_method("test invalid method"); // test with invalid method
  set_method("rsp");
  set_method("thibaut");

  set_parameter_file("default");
  set_parameter_file("Edisp_P8V1_SOURCE.txt");
  set_parameter_file_type("txt");


  ////////////////////////////////////////////////////////


  if(test_in_place==1)
  {
  // test in-place dispersion
  int nE=300;
  valarray<double> E,spectrum;
  E.resize(nE);
  spectrum.resize(nE);
  for(int i=0;i<nE;i++) E[i]=10*pow(10,i*.01); // start at 10 MeV
  for(int i=0;i<nE;i++) E[i]= 1*pow(10,i*.01); // start at  1 MeV
 
  debug=2; // 1= all details, 2=summary

  // inject line spectra
  spectrum=0;
  spectrum[  0]=1;
  spectrum[ 10]=1;
  spectrum[ 20]=0;
  spectrum[ 30]=0;
  spectrum[ 40]=0;
  spectrum[ 50]=1;
  spectrum[ 60]=0;
  spectrum[ 70]=0;
  spectrum[100]=0;
  spectrum[150]=1;
  spectrum[200]=1;
  spectrum[250]=1;
  spectrum[290]=1; // integral OK up to about here
  spectrum[298]=0; // last bins cause large discrepancy in integral
  spectrum[299]=0; // last bins cause large discrepancy in integral

  reset(); // to ensure everything will be initialized
  ApplyEnergyDispersion(E,spectrum,debug);

  spectrum=1;
  ApplyEnergyDispersion(E,spectrum,debug);

  // shows loss at low energies for steep power law
 for(int i=0;i<nE;i++) E[i]=10*pow(10,i*.01); // start at 10 MeV
 reset();
 for(int i=0;i<nE;i++) spectrum[i]=pow(E[i],-3);
 ApplyEnergyDispersion(E,spectrum,debug);

 for(int i=0;i<nE;i++) E[i]=1*pow(10,i*.01); // start at 1 MeV
 for(int i=0;i<nE;i++) spectrum[i]=pow(E[i],-3);
 reset();
 ApplyEnergyDispersion(E,spectrum,debug);


 // E^-2 powerlaw
for(int i=0;i<nE;i++) E[i]=10*pow(10,i*.01); // start at 10 MeV
 reset();
 for(int i=0;i<nE;i++) spectrum[i]=pow(E[i],-2);
 ApplyEnergyDispersion(E,spectrum,debug);

 for(int i=0;i<nE;i++) E[i]=1*pow(10,i*.01); // start at 1 MeV
 for(int i=0;i<nE;i++) spectrum[i]=pow(E[i],-2);
 reset();
 ApplyEnergyDispersion(E,spectrum,debug);

// E^-1 powerlaw
for(int i=0;i<nE;i++) E[i]=10*pow(10,i*.01); // start at 10 MeV
 reset();
 for(int i=0;i<nE;i++) spectrum[i]=pow(E[i],-1);
 ApplyEnergyDispersion(E,spectrum,debug);

 for(int i=0;i<nE;i++) E[i]=1*pow(10,i*.01); // start at 1 MeV
 for(int i=0;i<nE;i++) spectrum[i]=pow(E[i],-1);
 reset();
 ApplyEnergyDispersion(E,spectrum,debug);


 // start at higher energies, less loss
 for(int i=0;i<nE;i++) E[i]=200*pow(10,i*.01);
 for(int i=0;i<nE;i++) spectrum[i]=pow(E[i],-3);

 reset(); //since new energy grid
 ApplyEnergyDispersion(E,spectrum,debug);


// wide energies but spectrum starts higher so covered. integrals agree to 1%
 for(int i=0;i<nE;i++) E[i]=10*pow(10,i*.01);
 spectrum=0;
 for(int i=0;i<nE;i++)if(E[i]>50.) spectrum[i]=pow(E[i],-3);

 reset(); //since new energy grid
 ApplyEnergyDispersion(E,spectrum,debug);


 // speed test
 nE=100;
 spectrum.resize(nE);

 for(int i=0;i<nE;i++) E[i]=10*pow(10,i*.01);
 for(int i=0;i<nE;i++)if(E[i]>50.) spectrum[i]=pow(E[i],-3);
 reset(); //since new energy grid
 debug=0;

 int nrept=1.e4;
 cout<<"starting in-place speed test with matrix: nE="<<nE<< " nrept="<<nrept<<endl;
 
 for (int irept=0;irept<nrept;irept++) {if(irept%1000==0) cout<<" irept="<<irept<<" "<<endl; ApplyEnergyDispersion(E,spectrum,debug);}
 cout<<endl<<"completed in-place speed test"<<endl;


 set_use_matrix(0);
 nrept=1.e2;
 cout<<"starting in-place speed test without matrix: nE="<<nE<< " nrept="<<nrept<<endl;
 
 for (int irept=0;irept<nrept;irept++) {if(irept%10==0) cout<<" irept="<<irept<<" "<<endl; ApplyEnergyDispersion(E,spectrum,debug);}
 cout<<endl<<"completed in-place speed test"<<endl;

  }// test in-place

  //////////////////////////////////////////////////////////////

  // test in-place with interpolation

 if( test_in_place_interp==1)
 {
  cout<<endl<<"starting in-place with interpolation test"<<endl;
  int nE=30;
  valarray<double> E,spectrum;
  E.resize(nE);
  spectrum.resize(nE);
  double E_interp_factor;

// E^-2 powerlaw

 
 for(int i=0;i<nE;i++) E[i]=1*pow(10,i* 0.2); // start at 1 MeV
 for(int i=0;i<nE;i++) spectrum[i]=pow(E[i],-2);
 E_interp_factor=1.05;
 debug=2;

 reset();
 ApplyEnergyDispersion(E,spectrum,E_interp_factor,debug);

 E_interp_factor=1.01;
 debug=2;

 reset();
 ApplyEnergyDispersion(E,spectrum,E_interp_factor,debug);




// E^-3 powerlaw

  nE=30;
 
  E.resize(nE);
  spectrum.resize(nE);

 for(int i=0;i<nE;i++) E[i]=1*pow(10,i* 0.2); // start at 1 MeV
 for(int i=0;i<nE;i++) spectrum[i]=pow(E[i],-3);
 E_interp_factor=1.05;
 debug=2;

 reset();
 ApplyEnergyDispersion(E,spectrum,E_interp_factor,debug);

  nE=200;
 
  E.resize(nE);
  spectrum.resize(nE);

 for(int i=0;i<nE;i++) E[i]=1*pow(10,i* 0.05); // start at 1 MeV
 for(int i=0;i<nE;i++) spectrum[i]=pow(E[i],-3);
 E_interp_factor=1.001;
 debug=2;

 reset();
 ApplyEnergyDispersion(E,spectrum,E_interp_factor,debug);


 for(int i=0;i<nE;i++) E[i]=1*pow(10,i* 0.01); // start at 1 MeV
 for(int i=0;i<nE;i++) spectrum[i]=pow(E[i],-3);
 E_interp_factor=1.001;
 debug=2;

 reset();
 ApplyEnergyDispersion(E,spectrum,E_interp_factor,debug);


 // compare with non-interpolated version
 for(int i=0;i<nE;i++) spectrum[i]=pow(E[i],-3);

 debug=2;

 reset();
 ApplyEnergyDispersion(E,spectrum,debug);



 for(int i=0;i<nE;i++) spectrum[i]=pow(E[i],-3);
int nrept=1.e4;
E_interp_factor=1.01;

  cout<<"starting inplace interpolated  speed test with matrix: nE="<<nE<<" E_interp_factor="<<E_interp_factor<< " nrept="<<nrept<<endl;
 
  reset();
  
  debug=0;
  for (int irept=0;irept<nrept;irept++) {if(irept%100==0)cout<<"irept="<<irept<<" "<<endl;  ApplyEnergyDispersion(E,spectrum,E_interp_factor,debug);   }
  cout<<endl<<"completed in place interpolated  speed test"<<endl;


 /*
E_interp_factor=1.001;
 debug=2;

 reset();
 ApplyEnergyDispersion(E,spectrum,E_interp_factor,debug);
 */

  }//test_in_place_interp

  ///////////////////////////////////////////////////////////////////////////////////////


 // test general energies dispersion

  int test_general=1;
  if(test_general==1)
  {

    cout<<endl<<" ================  starting general energies case"<<endl;

 // case where energies equal
  debug=2;
  int nE_true=200;
  int nE_meas=200;
  valarray<double> E_true_,E_meas_,spectrum_true,spectrum_meas;
  E_true_.resize(nE_true);
  E_meas_.resize(nE_meas);
  spectrum_true.resize(nE_true);
  spectrum_meas.resize(nE_meas);
  for(int i=0;i<nE_true;i++) E_true_[i]=10*pow(10,i*.01);
  for(int j=0;j<nE_meas;j++) E_meas_[j]=10*pow(10,j*.01);
  spectrum_true=0;
  spectrum_true[100]=1;
  reset();
  set_use_matrix(1);
  ApplyEnergyDispersion(E_true_,spectrum_true,E_meas_,spectrum_meas,debug);

  set_use_matrix(0);
  ApplyEnergyDispersion(E_true_,spectrum_true,E_meas_,spectrum_meas,debug);
  
  // case where Emeas same start but different bins
  for(int i=0;i<nE_true;i++) E_true_[i]= 10*pow(10,i*.01 );
  for(int j=0;j<nE_meas;j++) E_meas_[j]= 10*pow(10,j*.007);
  spectrum_true=0;
  spectrum_true[100]=1;
  reset();
  set_use_matrix(1);
  ApplyEnergyDispersion(E_true_,spectrum_true,E_meas_,spectrum_meas,debug);
  set_use_matrix(0);
  ApplyEnergyDispersion(E_true_,spectrum_true,E_meas_,spectrum_meas,debug);
  
  // case where Emeas same start but different bins
  for(int i=0;i<nE_true;i++) E_true_[i]= 10*pow(10,i*.01 );
  for(int j=0;j<nE_meas;j++) E_meas_[j]= 10*pow(10,j*.020);
  spectrum_true=0;
  spectrum_true[100]=1;
  reset();
  set_use_matrix(1);
  ApplyEnergyDispersion(E_true_,spectrum_true,E_meas_,spectrum_meas,debug);

    // case constant spectrum, measured covers more
  for(int i=0;i<nE_true;i++) E_true_[i]= 50*pow(10,i*.01 );
  for(int j=0;j<nE_meas;j++) E_meas_[j]= 50*pow(10,j*.020);
  spectrum_true=1;
  
  reset();
  ApplyEnergyDispersion(E_true_,spectrum_true,E_meas_,spectrum_meas,debug);

  // steep power law covered by measured, might show loss due to response since starts at 20 MeV
  for(int i=0;i<nE_true;i++) E_true_[i]= 10*pow(10,i*.01 );
  for(int j=0;j<nE_meas;j++) E_meas_[j]= 10*pow(10,j*.020);
  spectrum_true=0;
  for(int i=0;i<nE_true;i++)if(E_true_[i]>20.) spectrum_true[i]=pow(E_true_[i],-3);
  reset();
  ApplyEnergyDispersion(E_true_,spectrum_true,E_meas_,spectrum_meas,debug);

  // steep power law covered by measured, shows no  loss due to response since starts at 300 MeV
  for(int i=0;i<nE_true;i++) E_true_[i]= 10*pow(10,i*.01 );
  for(int j=0;j<nE_meas;j++) E_meas_[j]= 10*pow(10,j*.020);
  spectrum_true=0;
  for(int i=0;i<nE_true;i++)if(E_true_[i]>300.) spectrum_true[i]=pow(E_true_[i],-3);
  reset();
  ApplyEnergyDispersion(E_true_,spectrum_true,E_meas_,spectrum_meas,debug);

  // speed test
 int nrept=1.e3;
  cout<<"starting general case speed test with matrix: nE_true="<<nE_true<<" nE_meas="<<nE_meas<< " nrept="<<nrept<<endl;
 
  reset();
  set_use_matrix(1);
  debug=0;
  for (int irept=0;irept<nrept;irept++) {if(irept%100==0)cout<<"irept="<<irept<<" "<<endl;   ApplyEnergyDispersion(E_true_,spectrum_true,E_meas_,spectrum_meas,debug); }
  cout<<endl<<"completed general case speed test"<<endl;


  nrept=1.e3;
  cout<<"starting general case speed test without matrix: nE_true="<<nE_true<<" nE_meas="<<nE_meas<< " nrept="<<nrept<<endl;
 
  reset();
  set_use_matrix(0);
  debug=0;
  for (int irept=0;irept<nrept;irept++) {if(irept%100==0)cout<<"irept="<<irept<<" "<<endl;   ApplyEnergyDispersion(E_true_,spectrum_true,E_meas_,spectrum_meas,debug); }
 cout<<endl<<"completed general case speed test"<<endl;

  } // test_general

 cout<<"<<  EnergyDispersion::test"<<endl;
 return;
}

///////////////////////////////////main test program, normally commented out ////////////////////////////////////////
// obsolete, use test_EnergyDispersionFermiLAT.cc

// g++ EnergyDispersionFermiLAT.cc -I/afs/ipp-garching.mpg.de/home/a/aws/propagate/c/cfitsio/3.26/gcc_sles11_olga2/cfitsio/include -L/afs/ipp-garching.mpg.de/home/a/aws/propagate/c/cfitsio/3.26/gcc_sles11_olga2/cfitsio/lib -lcfitsio -L/afs/ipp-garching.mpg.de/home/a/aws/gsl/gsl-1.10/olga/lib -lgsl -lgslcblas

// setenv LD_LIBRARY_PATH /usr/lib64/mpi/gcc/openmpi/lib64:/afs/ipp-garching.mpg.de/home/a/aws/gsl/gsl-1.10/olga/lib

/*
int main()
{
  EnergyDispersionFermiLAT energyDispersionFermiLAT;
  energyDispersionFermiLAT.test();
  return 0;
}
*/

 /////////////////////////////////////////////////////////////// separate test program //////////////////////////////
 /*
  g++ test_EnergyDispersionFermiLAT.cc EnergyDispersionFermiLAT.cc -I/afs/ipp-garching.mpg.de/home/a/aws/propagate/c/cfitsio/3.26/gcc_sles11_olga2/cfitsio/include -L/afs/ipp-garching.mpg.de/home/a/aws/propagate/c/cfitsio/3.26/gcc_sles11_olga2/cfitsio/lib -lcfitsio -L/afs/ipp-garching.mpg.de/home/a/aws/gsl/gsl-1.10/olga/lib -lgsl -lgslcblas -O3

  setenv LD_LIBRARY_PATH /usr/lib64/mpi/gcc/openmpi/lib64:/afs/ipp-garching.mpg.de/home/a/aws/gsl/gsl-1.10/olga/lib
  
./a.out

*/
