using namespace std;
#include <iostream>
#include <string>
#include <vector>


#include "Skymap.h"
#include "Counts.h"
#include "Exposure.h"
#include "Psf.h"

int test_Skymap(int verbose)
{
  // routines from gardian

  cout<<">>>> test_Skymap"<<endl;
  int i,j;

  string mapname;

  mapname="../FITS/bremss_healpix_53_61034hpS";
  cout<<mapname<<endl;
  
  Skymap<double> skymap;
  skymap.load(mapname);
  cout<<"energies:"<<endl;
  for(i=0;i<skymap.getSpectra().size();i++)    cout<<skymap.getSpectra()[i]<<endl; // the energies are called 'Spectra'

  //  if (verbose==-1601)  skymap.print(cout); AWS20091204
  





  // exposure as Skymap
  mapname="../FITS/exposure_Healpix_gardian_Sample.fits";
  cout<<mapname<<endl;
  Skymap <double> skymap2;
  skymap2.load(mapname);

  cout<<"energies:"<<endl;
  for(i=0;i<skymap2.getSpectra().size();i++)    cout<<skymap2.getSpectra()[i]<<endl; // the energies are called 'Spectra'

  //  if (verbose==-1602)  skymap2.print(cout); AWS20091204


  // exposure as Exposure
  //  Exposure exposure(mapname); OK
  mapname="../FITS/exposure_Healpix_gardian_Sample.fits";
  cout<<mapname<<endl;
  Exposure exposure;
  exposure.readFile(mapname); // alternative
  cout<<"energies:"<<endl;
  for(i=0;i<exposure.getEnergies().size();i++)    cout<<exposure.getEnergies()[i]<<endl;

  Skymap<double> exposureskymap=exposure .getExposureMap();
  //  if (verbose==-1602)  exposureskymap.print(cout); AWS20091204








  // counts have different extensions from skymaps and require another routine

  mapname="../FITS/counts_Healpix_gardian_Sample.fits";
  cout<<mapname<<endl;
  CountsMap  countsmap(mapname) ; //CountsMap.load does not exist

  cout<<"counts energy bounds:"<<endl;
  for(i=0;i<countsmap.getEMin().size();i++)    cout<<countsmap.getEMin()[i]<<" -  " <<countsmap.getEMax()[i] <<endl; // returns a valarray<double>
 

  //  countsmap .print(cout); does not exist
  
//Skymap<long> countsskymap=countsmap.getCountsMap();  AWS20091210
  Skymap<int>  countsskymap=countsmap.getCountsMap();//AWS20091210

  //  if (verbose==-1603)  countsskymap.print(cout); AWS20091204




 mapname="../FITS/gardian_Sample_psf.fits";
  cout<<mapname<<endl;
  Psf psf(mapname);

  cout<<"energies:"<<endl;
  for(i=0;i<     psf.getEnergies().size();i++)    cout<<     psf.getEnergies()[i]<<endl;
  cout<<"theta   :"<<endl;
  for(i=0;i<     psf.getTheta   ().size();i++)    cout<<     psf.getTheta   ()[i]<<endl;
  cout<<"PsfVector   :"<<endl;
  for(i=0;i<     psf.getPsfVector().size();i++)
   {cout<<endl;
   for(j=0;j<     psf.getPsfVector()[i].size();j++)    cout<< psf.getPsfVector   ()[i][j]<<" ";
   }

  cout<<"<<<< test_Skymap"<<endl;

  return 0;
}
