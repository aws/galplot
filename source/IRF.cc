using namespace std;

#include<iostream>
#include"fitsio.h"

#include"IRF.h"



//////////////////////////////////////////////////////
  
  
///////////////////////////////////////////////////////
  int IRF:: read (char *directory,int idet1,int idet2)
{
  char infile[500];
  int status;
  int hdunum,hdutype,total_hdus,colnum;
  
  int i,j,k;
  int i_E;
  

// 1=photopeak, 2=Compton detector hit first 3=Compton surroundings hit first
  int irf_type;

  
  n_E=51; // number of energies (hard coded)
  
  //  n_E=34; // truncated version up to 1 MeV


// using /afs/ipp-garching.mpg.de/mpe/gamma/instruments/integral/data/ic/osa-3.0/ic/spi/rsp/spi_irf_grp_0015.fits, ordered by increasing energy 

  int idlist[51]={243,246,249,252,255,258,261,264,267,270,273,276,279,281,232,234,236,238,240,242,245,248,251,254,257,260,263,266,269,272,275,278,280,231,233,235,237,239,241,244,247,250,253,256,259,262,265,268,271,274,277};



 




  /*
  n_E=5 ; // number of energies (hard coded)
 
// using /afs/ipp-garching.mpg.de/mpe/gamma/instruments/integral/data/ic/osa-3.0/ic/spi/rsp/spi_irf_grp_0015.fits, ordered by increasing energy 

  int idlist[5 ]={243,246,249,252,255};
  */



  sum =new  double[n_E];
  sum1=new  double[n_E];
  sum2=new  double[n_E];
  sum3=new  double[n_E];

  double *compton_fraction;
          compton_fraction=new  double[n_E];

  cout<< "IRF::read  (" << directory<<")" <<endl;

  
  cout<<"detectors "<<idet1<<" - "<<idet2<<endl;

  for (i_E=0;i_E<n_E;i_E++)
  {
    //strcpy(infile,directory);
    //strcat(infile,"spi_irf_rsp_0231.fits");

  sprintf(infile,"%sspi_irf_rsp_0%d.fits",directory,idlist[i_E]);
  cout<<"   infile "<<infile<<endl;

  hdunum=2;
  matrix.read(infile,hdunum);

  //if(i_E==40)matrix.print();

  sum[i_E]=matrix.sum();

  cout<<i_E<<"  sum="<<matrix.sum()<<endl;

  //sum over detectors and directions for each IRF type

  sum1[i_E]=0;  sum2[i_E]=0;  sum3[i_E]=0;

  // for(i=0;i<matrix.NAXES[0];i++) // would give all 85 detectors

  cout<<"matrix NAXES "<<    matrix.NAXES[0]<<" "<<matrix.NAXES[1]
                      <<" "<<matrix.NAXES[2]    <<" "<<matrix.NAXES[3]<<endl;

  // singles only case
  //idet1= 0;
  //idet2=18;

  //  doubles only case
  //idet1=19;
  //idet2=65;

  // singles + doubles case
  //idet1=0 ;
  //idet2=65;

  for(i=idet1;i<=idet2         ;i++)
  for(j=0    ;j<matrix.NAXES[1];j++)
  for(k=0    ;k<matrix.NAXES[2];k++)
  {
   sum1[i_E]+=matrix(i,j,k,0);
   sum2[i_E]+=matrix(i,j,k,1);
   sum3[i_E]+=matrix(i,j,k,2);

   //if(i_E==40)cout<<i<<" "<<j<<" "<<k<<" matrix="<<matrix(i,j,k,0)<<" sum1="<<sum1[i_E]<<endl;
  }// i j k


  //-------------------------------------------------------------------------------------
  int directional_analysis=1;
  if(directional_analysis==1)
  {
    /*
   for(i=idet1;i<=idet2         ;i++)
   for(j=0    ;j<matrix.NAXES[1];j++)
   for(k=0    ;k<matrix.NAXES[2];k++)
   {
    double s  =matrix(i,j,k,0)+matrix(i,j,k,1)+matrix(i,j,k,2);
    double r1 =matrix(i,j,k,0)/s;
    double r2 =matrix(i,j,k,1)/s;
    double r3 =matrix(i,j,k,2)/s;
    cout<<i<<" "<<j<<" "<<k<<" matrix(i,j,k,0)="<<matrix(i,j,k,0)<<" matrix(i,j,k,1)="<<matrix(i,j,k,1)<<" matrix(i,j,k,2)="<<matrix(i,j,k,2)<<endl  ;
    cout<<"i_E="<<i_E<<" detector "<<i<<" direction j="<<j<<" k="<<k<<" r1="<<r1<<" r2="<<r2<<" r3="<<r3<<endl;
   }//i j k
    */

    int jkedge=10;// border to avoid in j,k   0=no border
   
   
    double r1_min,r1_max;
    double r2_min,r2_max;
    double r3_min,r3_max;

    double r1_bar,  r2_bar,  r3_bar;
    double r1sq_bar,r2sq_bar,r3sq_bar;
    double r1_rms,  r2_rms,  r3_rms;

    double r1s_bar,  r2s_bar,  r3s_bar; // s-weighted values
    double r1ssq_bar,r2ssq_bar,r3ssq_bar;
    double r1s_rms,  r2s_rms,  r3s_rms;
    double   s_bar;

    int n_jk;

    r1_bar   =0.;
    r2_bar   =0.;
    r3_bar   =0.;
    r1sq_bar =0.;
    r2sq_bar =0.; 
    r3sq_bar =0.;
    r1s_bar  =0.;
    r2s_bar  =0.;
    r3s_bar  =0.;
    r1ssq_bar=0.;
    r2ssq_bar=0.; 
    r3ssq_bar=0.;
        s_bar=0.;


    r1_min=100.;r1_max=-100.;
    r2_min=100.;r2_max=-100.;
    r3_min=100.;r3_max=-100.;

    n_jk=0;

   for(j=0    ;j<matrix.NAXES[1];j++)
   for(k=0    ;k<matrix.NAXES[2];k++)
   {
    double s =0;
    double s1=0;
    double s2=0; 
    double s3=0;

    for(i=idet1;i<=idet2         ;i++)
    {
     s1+=matrix(i,j,k,0)  ;
     s2+=matrix(i,j,k,1)  ;
     s3+=matrix(i,j,k,2)  ;

    }//i

    s=s1+s2+s3;
    double r1=s1/s;
    double r2=s2/s;
    double r3=s3/s;

    if(s>0.&& j>=jkedge && k>=jkedge && j< matrix.NAXES[1]-jkedge && k< matrix.NAXES[2]-jkedge )
    {
     r1_min=min(r1_min,r1);
     r2_min=min(r2_min,r2);
     r3_min=min(r3_min,r3);
     r1_max=max(r1_max,r1);
     r2_max=max(r2_max,r2);
     r3_max=max(r3_max,r3);
    
     r1_bar  +=r1;
     r2_bar  +=r2;
     r3_bar  +=r3;
     r1sq_bar+=r1*r1;
     r2sq_bar+=r2*r2;
     r3sq_bar+=r3*r3;
     n_jk++;

     r1s_bar  +=r1   *s;
     r2s_bar  +=r2   *s;
     r3s_bar  +=r3   *s;
     r1ssq_bar+=r1*r1*s;
     r2ssq_bar+=r2*r2*s;
     r3ssq_bar+=r3*r3*s;
         s_bar+=      s;

    if(s>0. && 0==1)
    cout<<"i_E="<<i_E<<" direction j="<<j<<" k="<<k
        <<" r1="<<r1<<" r2="<<r2<<" r3="<<r3
        <<" s1="<<s1<<" s2="<<s2<<" s3="<<s3 <<" s="<<s<<endl  ;

    }//if

    }//j k

   r1_bar  /=n_jk;
   r2_bar  /=n_jk;
   r3_bar  /=n_jk;
   r1sq_bar/=n_jk;
   r2sq_bar/=n_jk;
   r3sq_bar/=n_jk;

   r1_rms=sqrt(r1sq_bar-r1_bar*r1_bar);
   r2_rms=sqrt(r2sq_bar-r2_bar*r2_bar);
   r3_rms=sqrt(r3sq_bar-r3_bar*r3_bar);

   r1s_bar/=s_bar;
   r2s_bar/=s_bar;
   r3s_bar/=s_bar;

   r1s_rms=sqrt((r1ssq_bar-r1s_bar*r1s_bar*s_bar)/s_bar );
   r2s_rms=sqrt((r2ssq_bar-r2s_bar*r2s_bar*s_bar)/s_bar );
   r3s_rms=sqrt((r3ssq_bar-r3s_bar*r3s_bar*s_bar)/s_bar );

    cout<<" statistics excluding border of j,k ="<<jkedge<<endl;
    cout<<"i_E="<<i_E     <<" r1_min ="<<r1_min <<" r2_min ="<<r2_min <<" r3_min ="<<r3_min<<endl;
    cout<<"i_E="<<i_E     <<" r1_max ="<<r1_max <<" r2_max ="<<r2_max <<" r3_max ="<<r3_max<<endl;
    cout<<"i_E="<<i_E     <<" r1_bar ="<<r1_bar <<" r2_bar ="<<r2_bar <<" r3_bar ="<<r3_bar<<endl;
    cout<<"i_E="<<i_E     <<" r1_rms ="<<r1_rms <<" r2_rms ="<<r2_rms <<" r3_rms ="<<r3_rms<<endl;
    cout<<"i_E="<<i_E     <<" r1s_bar="<<r1s_bar<<" r2s_bar="<<r2s_bar<<" r3s_bar="<<r3s_bar<<endl;
    cout<<"i_E="<<i_E     <<" r1s_rms="<<r1s_rms<<" r2s_rms="<<r2s_rms<<" r3s_rms="<<r3s_rms<<endl;


  }// if directional_analysis
  //---------------------------------------------------------------------------------------


  }//i_E


  /////////////////////////////////////////////////////////////////////////////////////////


  for (i_E=0;i_E<n_E;i_E++)
   compton_fraction[i_E]=(           sum2[i_E]+sum3[i_E])
                        /(sum1 [i_E]+sum2[i_E]+sum3[i_E]+1e-10);

  cout<<"detectors "<<idet1<<" - "<<idet2<<endl;
  cout<<"matrix sum as function of energy for 3 IRF types:"<<endl;
  for (i_E=0;i_E<n_E;i_E++) 
   cout<<i_E<<" sum1 2 3 total "<<sum1[i_E]<<" "<<sum2[i_E]
                           <<" "<<sum3[i_E]
                           <<" "<<sum1 [i_E]+sum2[i_E]+sum3[i_E]
                <<"  compton fraction= "<< compton_fraction[i_E]
                           <<endl;

  cout<<endl;

  return 0;
}


