



class EGRET_catalogue
{
public:
   int n_source;
   double  *ra,*dec,*l,*b,*theta,*F,*dF,*index,*counts,*sqrtTS;
   char **name,**vp,**ID;

   int *selected;

  int read(int debug);
  int print(ofstream &txt_stream);

  int select(int options);
};
