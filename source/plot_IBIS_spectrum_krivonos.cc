/* 
Krivonos et al. astro-ph/0605420
Fig 14 : points APPROXIMATELY estimated from plot (no numbers given)
flux per IBIS FOV 15*15 deg, presumably for whole inner Galaxy ?
*/
#include"Galplot.h"                 
#include"galprop_classes.h"
#include"galprop.h"
#include"constants.h"
#include"fitsio.h"

int Galplot::plot_IBIS_spectrum_krivonos    (int mode)
{

 
  

   cout<<" >>>> plot_IBIS_spectrum_krivonos        "<<endl;
   int status=0;

   

 
   int n_energy;
   double *E,*Emin,*Emax,*intensity;  
   double solid_angle;             
   int i_energy;
   int il,ib;
   double l,b;
   int select,n_mean;

   double A,dA,g;
   double factor;
   double sum,average,intensity_17_60,flux_17_60,intensity0;
   double e1,e2;
   TGraph *spectrum;
  
  
   FITS DIRBE4p9; // DIRBE 4.9 micron skymap

   char  infile[100];
 


   n_energy=9;
   E    =    new double[n_energy   ];
   intensity=new double[n_energy   ];

 
  
  
   // assume ALL emission is WITHIN the chosen sky region ! (not used)

   solid_angle= (galplotdef.  lat_max1 -galplotdef.  lat_min1 + galplotdef.  lat_max2 - galplotdef.  lat_min2 )  /(57.3)
               *(galplotdef. long_max1 -galplotdef. long_min1 + galplotdef. long_max2 - galplotdef. long_min2 )  /(57.3);

   // assume the intensity is typical of the chose region

   solid_angle=pow(15./57.3, 2); // approximation to IBIS 15*15 deg FOV
   factor= 1.0e-3/solid_angle; // keV->MeV


   // spectrum 1

 

 
   spectrum=new TGraph(n_energy);
   // keV^2 cm-2 s-1 keV-1 in IBIS FOV
   E[0]=0.018;  intensity[0]=1.40;   //  18 keV  in MeV
   E[1]=0.025;  intensity[1]=1.10;   //  25 keV
   E[2]=0.028;  intensity[2]=1.00;   //  28 keV
   E[3]=0.035;  intensity[3]=0.65;   //  35 keV
   E[4]=0.045;  intensity[4]=0.32;   //  45 keV
   E[5]=0.052;  intensity[5]=0.25;   //  52 keV 0.11-0.25: plotted as upper limit
   E[6]=0.070;  intensity[6]=0.22;   //  70 keV upper limit
   E[7]=0.100;  intensity[7]=0.35;   // 100 keV upper limit
   E[8]=0.150;  intensity[8]=0.70;   // 150 keV upper limit 
   

   if( mode==1 || mode==3)
   {

   for (i_energy=0;i_energy<n_energy;i_energy++)
   {
    intensity[i_energy]     *= factor;
    spectrum   ->SetPoint(i_energy, E[i_energy], intensity[i_energy] );
    }



  spectrum->SetMarkerColor(kRed    );// also kCyan
  spectrum->SetMarkerStyle(22); // triangles
  spectrum->SetMarkerSize (1.5);//
  spectrum->SetLineColor(kRed);// kCyan kMagenta kRed
  spectrum->SetLineWidth(3     );
  spectrum->SetLineStyle(1      );//1=solid 2=dash 3=dot 4=dash-dot
  spectrum->Draw("PL");  // points as markers + line 
  //  spectrum->Draw("L");                              

 } //if

  //-----------------------------------------------------------------------------


  if( mode==2 || mode==3)
   {

  // use DIRBE 4.9 micron skymaps with scaling given by Krivonos
  /*
  /afs/ipp-garching.mpg.de/mpe/gamma/instruments/integral/data/user_halloin/Maps/DIRBE/SPI_DIRBE_ZodSub_Knoedl_4.9um.fits.gz
   HISTORY spirescaleimg v1.0 : image values rescaled to a global flux of 1.00000e-03 ph/(s.cm**2) (original flux = 3.05837e-04 ph/(s.cm**2))

   Header of Knodlseder dir49.fits:
   HISTORY The map is scaled to obey a total flux of 10e-4 ph/cm^2/s
   HISTORY from the central Galatic steradian (i.e. intensity integrated
   HISTORY in a circular region of radius 0.572 rad around the Galactic
   HISTORY center).
   HISTORY To obtain the original units of the input map, the intensities
   HISTORY must be multiplied by 17292.3.

   NB the scaling factor should be checked since not obvious how zodical light affect it.

  dir49.fits (Jurgen)  brightest GC pixel=.00202  .00786
  202./785.  =   0.257325 which is less than .3058 : should use this instead ?
  */

  strcpy( infile,configure.fits_directory);
  strcat(infile,"SPI_DIRBE_ZodSub_Knoedl_4.9um.fits");

  DIRBE4p9.read(infile,3); // map is in extension 3  (primary, grouping, map)


  if(galplotdef.verbose==-1200) // selective debug
  {
   cout<<"DIRBE 4.9 micron skymap"<<endl;
   DIRBE4p9.print();
  }

//DIRBE4p9 *= (3.05837e-04 / 1.e-3 * 17292.3) ;//  rescale to original DIRBE  (MJy/sr) via original Knoedlseder using header values
  DIRBE4p9 *= (0.257325            * 17292.3) ;//  rescale to original DIRBE  (MJy/sr) via original Knoedlseder using GC max. pixel ratio

  // DIRBE units MJy/sr  to Krivonos units  erg cm-2 sr-1 s-1

  factor=1.0e6 * 1.0e-26 * 8.19e12  * 1.0e7* 1.e-4; // MJy -> erg cm-2  Jy=1e-26 W m-2 Hz-1    bandwith=8.19THz   W=1e7 erg
  DIRBE4p9 *= factor;

 if(galplotdef.verbose==-1201) // selective debug
  {
   cout<<"DIRBE 4.9 micron skymap converted to  erg cm-2  sr-1 s-1"<<endl;
   DIRBE4p9.print();
  }

  sum   =0.;
  n_mean=0;


   for ( il       =0;        il< DIRBE4p9.NAXES[0];               il++)
   for ( ib       =0;        ib< DIRBE4p9.NAXES[1];               ib++)   
   {
     l= DIRBE4p9.CRVAL[0]+ (il- DIRBE4p9.CRPIX[0])* DIRBE4p9.CDELT[0];
     b= DIRBE4p9.CRVAL[1]+ (ib- DIRBE4p9.CRPIX[1])* DIRBE4p9.CDELT[1];
     if(l<  0.0) l+=360.;
     if(l>360.0) l-=360.;
     //  cout<<"  DIRBE4p9  l="<<l<<" b="<<b<<endl;

   select=0;
     // check whether point is in any of the 4 regions
     if(l>=galplotdef.long_min1&&l<=galplotdef.long_max1 && b>=galplotdef.lat_min1 && b<=galplotdef.lat_max1) select=1;
     if(l>=galplotdef.long_min2&&l<=galplotdef.long_max2 && b>=galplotdef.lat_min1 && b<=galplotdef.lat_max1) select=1;
     if(l>=galplotdef.long_min1&&l<=galplotdef.long_max1 && b>=galplotdef.lat_min2 && b<=galplotdef.lat_max2) select=1;
     if(l>=galplotdef.long_min2&&l<=galplotdef.long_max2 && b>=galplotdef.lat_min2 && b<=galplotdef.lat_max2) select=1;

     if(select==1)
     {
       //      cout<<"DIRBE selected point l = "<<l<<" b = "<<b<<endl;
       sum+=DIRBE4p9(il,ib);
       n_mean++;
       
     }
    }

   average=sum/n_mean;
   cout<<"DIRBE erg cm-2 sr-1 s-1 sum="<<sum<<" n_mean="<<n_mean<<" average="<<average<<endl;
   
   // Krivonos et al. scaling factors for same FOV
   // whole Galaxy  F(17-60 keV)/F4.9micron=(7.52+-0.33) e-5
   // bulge l<10    F(17-60 keV)/F4.9micron=(7.73+-0.34) e-5
   // disk  l>20    F(17-60 keV)/F4.9micron=(6.53+-0.72) e-5
   // original units (http://lambda.gsfc.nasa.gov/product/cobe/dirbe_products.cfm ) are MJy/sr  with bandwidth 8.19 THz

   intensity_17_60= average* 7.52e-5; // using whole Galaxy value
   flux_17_60=intensity_17_60*solid_angle; // using approx IBIS FOV as above, for check only

   cout<<"DIRBE scaled intensity 17-60 keV = " << intensity_17_60 << " erg cm-2 sr-1 s-1"<<"  flux for IBIS FOV="<<flux_17_60<<" erg cm-2 s-1"<<endl;


   // convert to MeV cm-2 sr-1 s-1
   intensity_17_60 *= erg_to_eV * 1.e-6;
   cout<<"DIRBE scaled intensity 17-60 keV = " << intensity_17_60 << " MeV cm-2 sr-1 s-1"<<endl;


   g=3.0; // index assumed to allow computation of differential point using AE^-g, energy integral = A/(-g+2) E^(-g+2)
   e1=.017; e2=.060; // 17-60 keV in MeV
   A=intensity_17_60* (-g+2) /(pow(e2,-g+2.)-pow(e1,-g+2.));
   cout<<"DIRBE  A = " << A<<endl;


   intensity0=A*pow(E[0],-g+2); // E^2 I(E) for plot
   cout<<"DIRBE : E^2 I(E) at E="<<E[0]<<"MeV = "<<intensity0<<endl;

   spectrum=new TGraph(n_energy);

   // use first energy as basis and scale using spectrum given above
   



   for (i_energy=1;i_energy<n_energy;i_energy++)
   {
    intensity[i_energy]   = intensity0 * intensity[i_energy]/intensity[0];
    spectrum   ->SetPoint(i_energy, E[i_energy], intensity[i_energy] );
    }

   i_energy=0;
   intensity[i_energy]=intensity0; // since original value is needed in loop above
   spectrum   ->SetPoint(i_energy, E[i_energy], intensity[i_energy] );

  spectrum->SetMarkerColor(kRed   );// also kCyan
  spectrum->SetMarkerStyle(22); // triangles
  spectrum->SetMarkerSize (1.5);//
  spectrum->SetLineColor  (kRed);// kCyan kMagenta kRed
  spectrum->SetLineWidth(3     );
  spectrum->SetLineStyle(1      );//1=solid 2=dash 3=dot 4=dash-dot
  spectrum->Draw("PL");  // points as markers + line 


   }//if mode

  cout<<" <<<< plot_IBIS_spectrum_krivonos    "<<endl;
   return status;
}

