
//**.****|****.****|****.****|****.****|****.****|****.****|****.****|****.****|
// * plot_SPI_spectrum.cc *                             
//**"****!****"****!****"****!****"****!****"****!****"****!****"****!****"****|
#include"Galplot.h"                    //AWS20050920
#include"galprop_classes.h"
#include"galprop.h"
#include"constants.h"
#include"fitsio.h"

//int analyse_MCMC_SPI();
/////////////////////////////////////////////////////////////////////////
namespace SPI//  to protect local variables and share with local routines
{
   int status=0;

   int spidiffit_version;
   int test_comparison;

 
   int n_energy;
   double *emin,*emax,*flux,*dflux;
   double emean,ymin,ymax,y,ymean;
   double solid_angle,dtr;
   int i_energy;
   int color;
   
   TLine *line;


char  infile[1000];
 char comment[400];
 fitsfile *fptr;
 int hdunum,hdutype,total_hdus;

 

 int felement=1;

 int anynul;
 double nulval=0.;
 int i,il,ib,ilmin,ilmax,ibmin,ibmax;
 double delta_longitude;

 int n_theta;
 double *theta_ML,*theta_MC;
 double *  cov_ML,*  cov_MC;
 double *map;
 int n_chi,n_psi;

 int n_component,i_component,j_component;
 int n_map_component,n_source_component;
 int i_component1,i_component2;

 double e_min,e_max,e_mean;
 double shift;

 int N_theta_chain, n_accepted;   // as in spidiffit MCMC.h
 float  *theta_chain, *logL_chain;// as in spidiffit MCMC.h
 int i_sample_chain, i_theta_chain,i_chain;

 FITS model_map;
 double *intensity,dintensity,intensity_total;
 double  dintensity_ML, dintensity_MC;
 double *component_average;
 double sigma;
 int n_significant_sources;
 int bin_count;
 int region;
 int id;
 int *idlist;
 int nidlist;
 int iid;
 int idlistN;
 int iplot;
 int energy_select1,energy_select2,select_energy;
 int i_energy1,i_energy2;
 int viewdiffit_flag;
}
/////////////////////////////////////////////////////////////
int Galplot::plot_SPI_spectrum(int mode)
///////////////////////////////////////////////////////////
{
  using namespace SPI;

   cout<<" >>>> plot_SPI_spectrum    "<<endl;

   test_comparison=0;

if (test_comparison==1)
{
   n_energy=100;
   emin=      new double[ n_energy];
   emax=      new double[ n_energy];
    flux=new double[ n_energy];
   dflux=new double[ n_energy];

   /*
/afs/ipp-garching.mpg.de/mpe/gamma/instruments/integral/data/surveys_3/user_aws/GCDE_v4_continuum/spi
spidiffit_flux_spectrum_component_1_3_ML.698.699.700.701.702.txt
              keV                               E^2*flux,  keV cm^-2 s^-1
energy=      338.000 -       498.000 flux=      4.29186+-      2.93101
energy=      498.000 -       658.000 flux=      3.03001+-      5.68624
energy=      658.000 -       818.000 flux=      0.00000+-      4.68905
energy=      28.0000 -       38.0000 flux=      1.98630+-     0.117083
energy=      38.0000 -       48.0000 flux=      1.43072+-     0.115706
energy=      48.0000 -       58.0000 flux=      1.42441+-     0.303144
energy=      58.0000 -       68.0000 flux=      1.90925+-      1.05778
energy=      68.0000 -       78.0000 flux=      1.20811+-     0.516668
energy=      78.0000 -       98.0000 flux=      1.38830+-     0.591936
energy=      98.0000 -       118.000 flux=     0.843975+-     0.295578
energy=      118.000 -       138.000 flux=      1.71046+-     0.799019
energy=      138.000 -       178.000 flux=      1.31831+-     0.568805
energy=      178.000 -       258.000 flux=      1.60624+-      1.56828
energy=      258.000 -       338.000 flux=      1.26202+-      2.00116
   */

   

   i_energy=0;
   emin      [i_energy]= 28.;   emax      [i_energy]= 38.;   flux [i_energy]=1.98;  dflux[i_energy]=0.117 ;  i_energy++;
   emin      [i_energy]= 38.;   emax      [i_energy]= 48.;   flux [i_energy]=1.43;  dflux[i_energy]=0.116;   i_energy++;
   emin      [i_energy]= 48.;   emax      [i_energy]= 58.;   flux [i_energy]=1.42;  dflux[i_energy]=0.303 ;  i_energy++;
   emin      [i_energy]= 58.;   emax      [i_energy]= 68.;   flux [i_energy]=1.91;  dflux[i_energy]=1.060;   i_energy++;
   emin      [i_energy]= 68.;   emax      [i_energy]= 78.;   flux [i_energy]=1.21;  dflux[i_energy]=0.517 ;  i_energy++;
   emin      [i_energy]= 78.;   emax      [i_energy]= 98.;   flux [i_energy]=1.39;  dflux[i_energy]=0.592;   i_energy++;
   emin      [i_energy]= 98.;   emax      [i_energy]=118.;   flux [i_energy]=0.84;  dflux[i_energy]=0.296 ;  i_energy++;
   emin      [i_energy]=118.;   emax      [i_energy]=138.;   flux [i_energy]=1.71;  dflux[i_energy]=0.799;   i_energy++;
   emin      [i_energy]=138.;   emax      [i_energy]=178.;   flux [i_energy]=1.32;  dflux[i_energy]=0.569;   i_energy++;
   emin      [i_energy]=178.;   emax      [i_energy]=258.;   flux [i_energy]=1.61;  dflux[i_energy]=1.576 ;  i_energy++;
   emin      [i_energy]=258.;   emax      [i_energy]=338.;   flux [i_energy]=1.26;  dflux[i_energy]=2.000;   i_energy++;
   emin      [i_energy]=338.;   emax      [i_energy]=498.;   flux [i_energy]=4.29;  dflux[i_energy]=2.930;   i_energy++;
   emin      [i_energy]=498.;   emax      [i_energy]=658.;   flux [i_energy]=3.03;  dflux[i_energy]=5.680;   i_energy++;
 
   solid_angle= (galplotdef. lat_max1-galplotdef. lat_min1 + galplotdef. lat_max2-galplotdef. lat_min2 )
               *(galplotdef.long_max1-galplotdef.long_min1 + galplotdef.long_max2-galplotdef.long_min2 )
                /(57.3*57.3);// sterad;


   for (i_energy=0;i_energy<n_energy;i_energy++)
   {
    ymin= (flux[i_energy]-dflux[i_energy])*1e-3;   // keV-> MeV
    ymax= (flux[i_energy]+dflux[i_energy])*1e-3;
    ymin/=solid_angle;
    ymax/=solid_angle;
    emean=sqrt(emin[i_energy]*emax[i_energy])*1e-3;

    line=new TLine(emean*1.04 ,ymin,emean*1.04,ymax);// shift energy for visibility
    line->SetLineColor(kRed);
    line->SetLineWidth(1  );
    line->Draw();
    }

}//test comparison

   /////////////////////////////////////////////////////////////

/*------------------------------------------------------------------------------------------------

Storage of spidiffit parameters and maps.
========================================
Convention here: i_energy, i_component start at 0
(NB cf spidiffit.pro:hdunum=1+extension number, i_energy i_component start at 1)


spidiffit version 21:

 primary header
 grouping extension

 for all energies
|   theta_ML
|     cov_ML
|   theta_MC
|     cov_MC


 
  for all  components
|   for all energies
| |      map       (ML or MC according to spidiffit.par)
|    for all energies
| |      map_error              ""

 total
  for all energies
       map                    ""
  for all energies
       map_error              ""



theta_ML: hdunum= 2 + i_energy*4 + 1
  cov_ML: hdunum= 2 + i_energy*4 + 2
theta_MC: hdunum= 2 + i_energy*4 + 3
  cov_MC: hdunum= 2 + i_energy*4 + 4

  map   : hdunum= 3 + n_energy*4 + i_energy   +i_map_component * n_energy*2


  n_component=n_map_component + n_source_component

  total headers=2+ n_energy*4 +    n_energy  * n_map_component * n_energy*2 + n_energy*2
              = 2+ n_energy*(6+n_map_component *2)

   n_energy=  (total headers -2)/(6+n_map_component *2)

......................................................................
spidiffit version 22:

 primary header
 grouping extension

 for all energies
|    theta_ML
|      cov_ML
|    theta_MC
|      cov_MC
| theta_chain
|  logL_chain

then maps as version 21
  n_energy=  (total headers -2)/(8+n_map_component *2)
theta_ML: hdunum= 2 + i_energy*6 + 1
  cov_ML: hdunum= 2 + i_energy*6 + 2
theta_MC: hdunum= 2 + i_energy*6 + 3
  cov_MC: hdunum= 2 + i_energy*6 + 4

  map   : hdunum= 3 + n_energy*6 + i_energy   +i_map_component * n_energy*2

.......................................................................
------------------------------------------------------------------------------------------------- */
 
 
 //-----------------------------------------------------------------------

 viewdiffit_flag=0;// 1=avoid negative theta_MC as in viewdiffit17.pro

// i_component1=0;// start at 0
// i_component2=2;

// i_component1=galplotdef.data_INTEGRAL_comp[0];
// i_component2=galplotdef.data_INTEGRAL_comp[1];

 select_energy=1;

 
 cout<<"galplotdef.verbose="<<galplotdef.verbose<<endl;
 /*
508-514 & 1-3 & 0.822 & 0.0287 & 901\\% LaTex NB FAKE ERRORS
508-514 & 3-3 & 0.619 & 0 & 901\\% LaTex      NB FAKE ERRORS
508-514 & 1-3 & 0.822 & 0.121 & 1018\\% LaTex ML ERROR OK
508-514 & 1-3 & 0.757 & 0.121 & 1018\\% LaTex corrected ilmin,ilmax

18-28 & 1-3 & -8.73 & 0.966 & 927\\% LaTex
 28-38 & 1-3 & 14.7 & 0.644 & 930\\% LaTex
 38-48 & 1-3 & 5.54 & 0.377 & 930\\% LaTex
 48-58 & 1-3 & 4.87 & 0.654 & 933\\% LaTex
 58-68 & 1-3 & 2.36 & 0.764 & 933\\% LaTex
 78-88 & 1-3 & 0.703 & 0.214 & 934\\% LaTex
 88-98 & 1-3 & 0.692 & 0.643 & 943\\% LaTex
 98-108 & 1-3 & 0.359 & 0.444 & 944\\% LaTex
 108-118 & 1-3 & 0.142 & 0.181 & 945\\% LaTex
 18-38 & 1-3 & 0 & 0 & 946\\% LaTex
 38-58 & 1-3 & 6.22 & 0.43 & 947\\% LaTex
 58-78 & 1-3 & 1.71 & 0.408 & 948\\% LaTex
 268-518 & 1-3 & 0.182 & 0.0116 & 995\\% LaTex
 178-338 & 1-3 & 0.288 & 0.0186 & 996\\% LaTex
 338-498 & 1-3 & 0.163 & 0.0161 & 997\\% LaTex
 338-418 & 1-3 & 0.19 & 0.0221 & 998\\% LaTex
 418-498 & 1-3 & 0.149 & 0.026 & 999\\% LaTex
 18-28 & 1-3 & -8.73 & 0.966 & 927\\% LaTex
 28-38 & 1-3 & 14.7 & 0.644 & 930\\% LaTex
 38-48 & 1-3 & 5.54 & 0.377 & 930\\% LaTex
 48-58 & 1-3 & 4.87 & 0.654 & 933\\% LaTex
 58-68 & 1-3 & 2.36 & 0.764 & 933\\% LaTex
 78-88 & 1-3 & 0.703 & 0.214 & 934\\% LaTex
 88-98 & 1-3 & 0.692 & 0.643 & 943\\% LaTex
 98-108 & 1-3 & 0.359 & 0.444 & 944\\% LaTex
 108-118 & 1-3 & 0.142 & 0.181 & 945\\% LaTex
 18-38 & 1-3 & 0 & 0 & 946\\% LaTex
 38-58 & 1-3 & 6.22 & 0.43 & 947\\% LaTex
 58-78 & 1-3 & 1.71 & 0.408 & 948\\% LaTex
 98-118 & 1-3 & 0.412 & 0.275 & 950\\% LaTex
 138-158 & 1-3 & 0.0928 & 0.245 & 952\\% LaTex
 158-178 & 1-3 & 0.0165 & 0.0709 & 953\\% LaTex
 178-198 & 1-3 & 0.0429 & 0.12 & 954\\% LaTex
 198-218 & 1-3 & 0 & 0 & 955\\% LaTex
 58-98 & 1-3 & 1.35 & 0.339 & 961\\% LaTex
 98-138 & 1-3 & 0.484 & 0.105 & 962\\% LaTex
 138-178 & 1-3 & 0.0464 & 0.14 & 963\\% LaTex
 178-218 & 1-3 & 0 & 0.0891 & 964\\% LaTex
 218-258 & 1-3 & 0.109 & 0.0853 & 965\\% LaTex
 98-178 & 1-3 & 0.368 & 0.088 & 971\\% LaTex
 178-258 & 1-3 & 0.0233 & 0.135 & 972\\% LaTex
 258-338 & 1-3 & 0 & 0.033 & 973\\% LaTex
 338-418 & 1-3 & 0.0257 & 0.0348 & 974\\% LaTex
 418-498 & 1-3 & 0.00102 & 0.0412 & 975\\% LaTex
 178-338 & 1-3 & 0 & 0 & 977\\% LaTex
 338-498 & 1-3 & 0.0494 & 0.0252 & 978\\% LaTex
 268-518 & 1-3 & 0.15 & 0.049 & 981\\% LaTex
 518-768 & 1-3 & 0.0201 & 0.0538 & 982\\% LaTex
 18-98 & 1-3 & 21 & 0.312 & 1000\\% LaTex
 98-178 & 1-3 & 1.13 & 0.0309 & 1001\\% LaTex
 178-258 & 1-3 & 0.359 & 0.0322 & 1002\\% LaTex                  
 178-258 & 1-3 & 0.359 & 0.0322 & 1003\\% LaTex (same as 1002)
 518-768 & 1-3 & 0.0483 & 0.0124 & 1004\\% LaTex
 768-1.02e+03 & 1-3 & 0.0172 & 0.0125 & 1005\\% LaTex
 518-1.02e+03 & 1-3 & 0.0321 & 0.00807 &1006\\% LaTex
 268-518 & 1-3 & 0.17 & 0.0131 &        1010\\% LaTex
 268-518 & 1-3 & 0.141 & 0.0152 & 1011\\% LaTex
 258-338 & 1-3 & 0.248 & 0.0218 & 1020\\% LaTex
 518-1.02e+03 & 1-3 & 0.0304 & 0.00913 & 1012\\% LaTex
 918-1.82e+03 & 1-3 & 0.0156 & 0.00494 & 1015\\% LaTex
 918-1.82e+03 & 2-3 & 0.0156 & 0.015 & 1015\\% LaTex
 818-1.62e+03 & 1-3 & 0.0182 & 0.00567 & 1017\\% LaTex
 818-1.62e+03 & 2-3 & 0.0182 & 0.0171 & 1017\\% LaTex
 918-1.82e+03 & 1-3 & 0.0141 & 0.00559 & 1019\\% LaTex

 28-38 & 1-3 & 14.7 & 0.644 & 1009\\% LaTex  spidiffit v22
 */

 int  idlist1[ 5]={698,699,700,701,702}                        ;                                               int nidlist1= 5;
 int  idlist2[11]={927,930,933,934,943,944,945,946,947,948,989};                                               int nidlist2=11;
 int  idlist3[11]={950,951,952,953,954,955,961,962,963,964,965};                                               int nidlist3=11;
 int  idlist4[ 9]={971,972,973,974,975,977,978,981,982        };                                               int nidlist4= 9;
 int  idlist5[ 5]={995,996,997,998,999}                                                                       ;int nidlist5= 5;
 int  idlist6[21]={927,930,933,934,943,944,945,946,947,948,995,996,997,998,999,1000,1001,1002,1003,1004,1005} ;int nidlist6=21;
 int  idlist7[19]={927,930,933,934,943,944,945,946,947,948,995,996,997,998,999,1002,1003,1004,1005}           ;int nidlist7=19;
 int  idlist8[21]={    930,933,934,962,                    995,996,997,998,999,1002,1003,1004,1005,1006,1010,     1012,1015,1017,1018,1019,1020} ;int nidlist8=21; // to 2 MeV
 int  idlist9[ 2]={970,1009}                                                                                  ;int nidlist9= 2;//     970:more MC
 // int  idlist10[10]={930,933,934,962,996,998,999,1006,1018,1020} ;int nidlist10=10;// for FB2004 poster
 int  idlist10[10]={930,933,934,962,1002,998,999,1006,1018,1020} ;int nidlist10=10;// for FB2004 poster, FB2004 talk, ESTEC talk

 int  idlist11[19]={    930,933,934,962,995,996,997,998,999,1002,1003,1004,1005,1006,1010,1011,1012,1018,1020} ;int nidlist11=19; // < 1 MeV           
 int  idlist12[12]={930,933,934,962,1002,998,999,1006,1018,1015,1019,1020} ;int nidlist12=12;// to 2 MeV
 int  idlist13[17]={1045,1046,1047,1048,1049,1050,1051,1052,1071,1072,1073,1074,1075,1076,1077,1078,1080         } ;int nidlist13=17;// 10 components 0/ 91 sources
 int  idlist14[50]={1081,1082,1083,1084,1085,1086,1087,1088,1089                                                 } ;int nidlist14= 9;// 10 components reordered 0/91 sources
 int  idlist15[50]={1081,1082,1090,1091,1092,1093,1094,1095,1096                                                 } ;int nidlist15= 9;// 10 components reordered   91 sources
 int  idlist16[50]={1081,1082,1090,1091,1092,1093,1094,1095,1096,1097,1098,1083,1084,1085,1086,1087,1088,1089    } ;int nidlist16=18;// 10 components reordered 0/91 sources
 int  idlist17[50]={1101,1102,1103,1104,1105,1106,1107,1108,1109                                                 } ;int nidlist17= 9;// 10 components reordered   91 FAKE sources
 int  idlist18[50]={1081,1082,1090,1091,1092,1093,1094,1095,1096,1101,1102,1103,1104,1105,1106,1107,1108,1109    } ;int nidlist18=18;// 10 comp reordered 91 sources/FAKE sources


 idlistN=galplotdef.data_INTEGRAL;     


 if(idlistN== 1){  idlist=idlist1;  nidlist=nidlist1;}
 if(idlistN== 2){  idlist=idlist2;  nidlist=nidlist2;}
 if(idlistN== 3){  idlist=idlist3;  nidlist=nidlist3;}
 if(idlistN== 4){  idlist=idlist4;  nidlist=nidlist4;}
 if(idlistN== 5){  idlist=idlist5;  nidlist=nidlist5;}
 if(idlistN== 6){  idlist=idlist6;  nidlist=nidlist6;}
 if(idlistN== 7){  idlist=idlist7;  nidlist=nidlist7;}
 if(idlistN== 8){  idlist=idlist8;  nidlist=nidlist8;}
 if(idlistN== 9){  idlist=idlist9;  nidlist=nidlist9;}
 if(idlistN==10){  idlist=idlist10; nidlist=nidlist10;}
 if(idlistN==11){  idlist=idlist11; nidlist=nidlist11;}
 if(idlistN==12){  idlist=idlist12; nidlist=nidlist12;}
 if(idlistN==13){  idlist=idlist13; nidlist=nidlist13;}
 if(idlistN==14){  idlist=idlist14; nidlist=nidlist14;}
 if(idlistN==15){  idlist=idlist15; nidlist=nidlist15;}
 if(idlistN==16){  idlist=idlist16; nidlist=nidlist16;}
 if(idlistN==17){  idlist=idlist17; nidlist=nidlist17;}
 if(idlistN==18){  idlist=idlist18; nidlist=nidlist18;}

 // for (id=698;id<=702;id++)
 // for (id=698;id<=948;id++)
 //           for (id= 15;id<= 17;id++)
 //         for (id= 969;id<=969;id++)
 // for (id=698;id<=999;id++)
 //for (id=995;id<=999;id++)


//      txtFILE is in global, and opened in plot_spectrum
fprintf(txtFILE,"======================================================= \n");     
fprintf(txtFILE,"\n INTEGRAL/SPI spidiffit results for list %d \n",idlistN);



   for(iid=0;iid<nidlist;iid++)
{

  id=idlist [iid];
 
 strcpy( infile,configure.fits_directory);

 energy_select1=0;
 spidiffit_version=21;

 //                                      NB users energy selection index  starting at 1 as in viewdiffit17.pro
 // GCDE_v4_continuum
 
 if(id==698){strcat( infile,"SPI-SKY-IMA.spidiffit.698.fits"   );  n_map_component=3 ; energy_select1=3; energy_select2=5;color=kBlue; } 
 if(id==699){strcat( infile,"SPI-SKY-IMA.spidiffit.699.fits"   );  n_map_component=3 ; energy_select1=2; energy_select2=6;color=kBlue; } 
 if(id==700){strcat( infile,"SPI-SKY-IMA.spidiffit.700.fits"   );  n_map_component=3 ; energy_select1=4; energy_select2=6;color=kBlue; } 
 if(id==701){strcat( infile,"SPI-SKY-IMA.spidiffit.701.fits"   );  n_map_component=3 ; energy_select1=4; energy_select2=4;color=kBlue; } 
 if(id==702){strcat( infile,"SPI-SKY-IMA.spidiffit.702.fits"   );  n_map_component=3 ; energy_select1=3; energy_select2=4;color=kBlue; } 
 
 // GCDE_1_2_3_511keV_new
 if(id==901 ){strcat( infile,"SPI-SKY-IMA.spidiffit.901.fits"   );  n_map_component=3 ; energy_select1=1; energy_select2=1;color=kRed; } 
 if(id==1018){strcat( infile,"SPI-SKY-IMA.spidiffit.1018.fits"   ); n_map_component=3 ; energy_select1=1; energy_select2=1;color=kRed; } 

 // GCDE_1_2_3_continuum_new
 if(id==927){strcat( infile,"SPI-SKY-IMA.spidiffit.927.fits"   );  n_map_component=3 ; energy_select1=1; energy_select2=1;color=kRed; } 
 if(id==930){strcat( infile,"SPI-SKY-IMA.spidiffit.930.fits"   );  n_map_component=3 ; energy_select1=1; energy_select2=2;color=kRed; } 
 if(id==933){strcat( infile,"SPI-SKY-IMA.spidiffit.933.fits"   );  n_map_component=3 ; energy_select1=1; energy_select2=2;color=kRed; } 
 if(id==934){strcat( infile,"SPI-SKY-IMA.spidiffit.934.fits"   );  n_map_component=3 ; energy_select1=2; energy_select2=2;color=kRed; } 
 if(id==943){strcat( infile,"SPI-SKY-IMA.spidiffit.943.fits"   );  n_map_component=3 ; energy_select1=1; energy_select2=1;color=kRed; }
 if(id==944){strcat( infile,"SPI-SKY-IMA.spidiffit.944.fits"   );  n_map_component=3 ; energy_select1=1; energy_select2=1;color=kRed; }
 if(id==945){strcat( infile,"SPI-SKY-IMA.spidiffit.945.fits"   );  n_map_component=3 ; energy_select1=1; energy_select2=1;color=kRed; }
 if(id==946){strcat( infile,"SPI-SKY-IMA.spidiffit.946.fits"   );  n_map_component=3 ; energy_select1=1; energy_select2=1;color=kRed; }
 if(id==947){strcat( infile,"SPI-SKY-IMA.spidiffit.947.fits"   );  n_map_component=3 ; energy_select1=1; energy_select2=1;color=kRed; }
 if(id==948){strcat( infile,"SPI-SKY-IMA.spidiffit.948.fits"   );  n_map_component=3 ; energy_select1=1; energy_select2=1;color=kRed; }
 if(id==949){strcat( infile,"SPI-SKY-IMA.spidiffit.949.fits"   );  n_map_component=3 ; energy_select1=1; energy_select2=1;color=kRed; }

 if(id==950){strcat( infile,"SPI-SKY-IMA.spidiffit.950.fits"   );  n_map_component=3 ; energy_select1=1; energy_select2=1;color=kRed; }
 if(id==951){strcat( infile,"SPI-SKY-IMA.spidiffit.951.fits"   );  n_map_component=3 ; energy_select1=1; energy_select2=1;color=kRed; }
 if(id==952){strcat( infile,"SPI-SKY-IMA.spidiffit.952.fits"   );  n_map_component=3 ; energy_select1=1; energy_select2=1;color=kRed; }
 if(id==953){strcat( infile,"SPI-SKY-IMA.spidiffit.953.fits"   );  n_map_component=3 ; energy_select1=1; energy_select2=1;color=kRed; }
 if(id==954){strcat( infile,"SPI-SKY-IMA.spidiffit.954.fits"   );  n_map_component=3 ; energy_select1=1; energy_select2=1;color=kRed; }
 if(id==955){strcat( infile,"SPI-SKY-IMA.spidiffit.955.fits"   );  n_map_component=3 ; energy_select1=1; energy_select2=1;color=kRed; }

 if(id==961){strcat( infile,"SPI-SKY-IMA.spidiffit.961.fits"   );  n_map_component=3 ; energy_select1=1; energy_select2=1;color=kRed; }
 if(id==962){strcat( infile,"SPI-SKY-IMA.spidiffit.962.fits"   );  n_map_component=3 ; energy_select1=1; energy_select2=1;color=kRed; }
 if(id==963){strcat( infile,"SPI-SKY-IMA.spidiffit.963.fits"   );  n_map_component=3 ; energy_select1=1; energy_select2=1;color=kRed; }
 if(id==964){strcat( infile,"SPI-SKY-IMA.spidiffit.964.fits"   );  n_map_component=3 ; energy_select1=1; energy_select2=1;color=kRed; }
 if(id==965){strcat( infile,"SPI-SKY-IMA.spidiffit.965.fits"   );  n_map_component=3 ; energy_select1=1; energy_select2=1;color=kRed; }

 if(id==971){strcat( infile,"SPI-SKY-IMA.spidiffit.971.fits"   );  n_map_component=3 ; energy_select1=1; energy_select2=1;color=kRed; }
 if(id==972){strcat( infile,"SPI-SKY-IMA.spidiffit.972.fits"   );  n_map_component=3 ; energy_select1=1; energy_select2=1;color=kRed; }
 if(id==973){strcat( infile,"SPI-SKY-IMA.spidiffit.973.fits"   );  n_map_component=3 ; energy_select1=1; energy_select2=1;color=kRed; }
 if(id==974){strcat( infile,"SPI-SKY-IMA.spidiffit.974.fits"   );  n_map_component=3 ; energy_select1=1; energy_select2=1;color=kRed; }
 if(id==975){strcat( infile,"SPI-SKY-IMA.spidiffit.975.fits"   );  n_map_component=3 ; energy_select1=1; energy_select2=1;color=kRed; }
 if(id==977){strcat( infile,"SPI-SKY-IMA.spidiffit.977.fits"   );  n_map_component=3 ; energy_select1=1; energy_select2=1;color=kRed; }
 if(id==978){strcat( infile,"SPI-SKY-IMA.spidiffit.978.fits"   );  n_map_component=3 ; energy_select1=1; energy_select2=1;color=kRed; }
 if(id==981){strcat( infile,"SPI-SKY-IMA.spidiffit.981.fits"   );  n_map_component=3 ; energy_select1=1; energy_select2=1;color=kRed; }
 if(id==982){strcat( infile,"SPI-SKY-IMA.spidiffit.982.fits"   );  n_map_component=3 ; energy_select1=1; energy_select2=1;color=kRed; }

 // no sources
 if(id== 995){strcat( infile,"SPI-SKY-IMA.spidiffit.995.fits"   );  n_map_component=3 ; energy_select1=1; energy_select2=1;color=kBlack;}
 if(id== 996){strcat( infile,"SPI-SKY-IMA.spidiffit.996.fits"   );  n_map_component=3 ; energy_select1=1; energy_select2=1;color=kBlack;}
 if(id== 997){strcat( infile,"SPI-SKY-IMA.spidiffit.997.fits"   );  n_map_component=3 ; energy_select1=1; energy_select2=1;color=kBlack;}
 if(id== 998){strcat( infile,"SPI-SKY-IMA.spidiffit.998.fits"   );  n_map_component=3 ; energy_select1=1; energy_select2=1;color=kBlack;}
 if(id== 999){strcat( infile,"SPI-SKY-IMA.spidiffit.999.fits"   );  n_map_component=3 ; energy_select1=1; energy_select2=1;color=kBlack;}
 if(id==1000){strcat( infile,"SPI-SKY-IMA.spidiffit.1000.fits"  );  n_map_component=3 ; energy_select1=1; energy_select2=1;color=kBlack;}
 if(id==1001){strcat( infile,"SPI-SKY-IMA.spidiffit.1001.fits"  );  n_map_component=3 ; energy_select1=1; energy_select2=1;color=kBlack;}
 if(id==1002){strcat( infile,"SPI-SKY-IMA.spidiffit.1002.fits"  );  n_map_component=3 ; energy_select1=1; energy_select2=1;color=kBlack;}
 if(id==1003){strcat( infile,"SPI-SKY-IMA.spidiffit.1003.fits"  );  n_map_component=3 ; energy_select1=1; energy_select2=1;color=kBlack;}
 if(id==1004){strcat( infile,"SPI-SKY-IMA.spidiffit.1004.fits"  );  n_map_component=3 ; energy_select1=1; energy_select2=1;color=kBlack;}
 if(id==1005){strcat( infile,"SPI-SKY-IMA.spidiffit.1005.fits"  );  n_map_component=3 ; energy_select1=1; energy_select2=1;color=kBlack;}
 if(id==1006){strcat( infile,"SPI-SKY-IMA.spidiffit.1006.fits"  );  n_map_component=3 ; energy_select1=1; energy_select2=1;color=kBlack;}
 if(id==1015){strcat( infile,"SPI-SKY-IMA.spidiffit.1015.fits"  );  n_map_component=3 ; energy_select1=1; energy_select2=1;color=kBlack;}
 if(id==1017){strcat( infile,"SPI-SKY-IMA.spidiffit.1017.fits"  );  n_map_component=3 ; energy_select1=1; energy_select2=1;color=kBlack;}
 if(id==1020){strcat( infile,"SPI-SKY-IMA.spidiffit.1020.fits"  );  n_map_component=3 ; energy_select1=1; energy_select2=1;color=kBlack;}

 // 5 or N sources
 if(id==1010){strcat( infile,"SPI-SKY-IMA.spidiffit.1010.fits"  );  n_map_component=3 ; energy_select1=1; energy_select2=1;color=kBlue ;}//5 sources
 if(id==1011){strcat( infile,"SPI-SKY-IMA.spidiffit.1011.fits"  );  n_map_component=3 ; energy_select1=1; energy_select2=1;color=kBlue ;}//14sources
 if(id==1012){strcat( infile,"SPI-SKY-IMA.spidiffit.1012.fits"  );  n_map_component=3 ; energy_select1=1; energy_select2=1;color=kBlue ;}//5 sources
 if(id==1019){strcat( infile,"SPI-SKY-IMA.spidiffit.1019.fits"  );  n_map_component=3 ; energy_select1=1; energy_select2=1;color=kBlue ;}//5 sources

 // test spidiffit version 22
 if(id==  4){strcat( infile,"SPI-SKY-IMA.spidiffit.4.fits"    );  spidiffit_version=22; n_map_component=1 ; energy_select1=1; energy_select2=2;color=kRed; }
 if(id== 13){strcat( infile,"SPI-SKY-IMA.spidiffit.13.fits"   );  spidiffit_version=22; n_map_component=3 ; energy_select1=1; energy_select2=1;color=kRed; }
 if(id== 14){strcat( infile,"SPI-SKY-IMA.spidiffit.14.fits"   );  spidiffit_version=22; n_map_component=3 ; energy_select1=1; energy_select2=2;color=kRed; }
 if(id== 15){strcat( infile,"SPI-SKY-IMA.spidiffit.15.fits"   );  spidiffit_version=22; n_map_component=3 ; energy_select1=1; energy_select2=2;color=kRed; }
 if(id== 16){strcat( infile,"SPI-SKY-IMA.spidiffit.16.fits"   );  spidiffit_version=22; n_map_component=3 ; energy_select1=1; energy_select2=1;color=kRed; }
 if(id== 17){strcat( infile,"SPI-SKY-IMA.spidiffit.17.fits"   );  spidiffit_version=22; n_map_component=3 ; energy_select1=1; energy_select2=1;color=kRed; }

 if(id==969){strcat( infile,"SPI-SKY-IMA.spidiffit.969.fits"   );  spidiffit_version=22; n_map_component=3 ; energy_select1=1; energy_select2=1;color=kRed; }//GCDE_123_continuum_new 28-38 keV
 if(id==970){strcat( infile,"SPI-SKY-IMA.spidiffit.970.fits"   );  spidiffit_version=22; n_map_component=3 ; energy_select1=1; energy_select2=1;color=kRed; }//GCDE_123_continuum_new 28-38 keV
 if(id==1008){strcat( infile,"SPI-SKY-IMA.spidiffit.1008.fits"   );  spidiffit_version=22; n_map_component=3 ; energy_select1=1; energy_select2=1;color=kRed; }//GCDE_123_continuum_new 28-38 keV as 970 BUT NO IMAGE SCALING
 if(id==1009){strcat( infile,"SPI-SKY-IMA.spidiffit.1009.fits"   );  spidiffit_version=22; n_map_component=3 ; energy_select1=1; energy_select2=1;color=kRed; }//GCDE_123_continuum_new 28-38 keV as 970 BUT NO IMAGE SCALING and with ML errors

 if(id==1045){strcat( infile,"SPI-SKY-IMA.spidiffit.1045.fits"   );  spidiffit_version=22; n_map_component=10; energy_select1=1; energy_select2=1;color=kRed; }//GCDE_123_continuum_new 258-338 keV  0 sources
 if(id==1046){strcat( infile,"SPI-SKY-IMA.spidiffit.1046.fits"   );  spidiffit_version=22; n_map_component=10; energy_select1=1; energy_select2=1;color=kRed; }//GCDE_123_continuum_new 338-418 keV  0 sources
 if(id==1047){strcat( infile,"SPI-SKY-IMA.spidiffit.1047.fits"   );  spidiffit_version=22; n_map_component=10; energy_select1=1; energy_select2=1;color=kRed; }//GCDE_123_continuum_new 418-498 keV  0 sources
 if(id==1048){strcat( infile,"SPI-SKY-IMA.spidiffit.1048.fits"   );  spidiffit_version=22; n_map_component=10; energy_select1=1; energy_select2=1;color=kRed; }//GCDE_123_continuum_new 178-258 keV  0 sources
 if(id==1049){strcat( infile,"SPI-SKY-IMA.spidiffit.1049.fits"   );  spidiffit_version=22; n_map_component=10; energy_select1=1; energy_select2=1;color=kRed; }//GCDE_123_continuum_new 498-578 keV  0 sources
 if(id==1050){strcat( infile,"SPI-SKY-IMA.spidiffit.1050.fits"   );  spidiffit_version=22; n_map_component=10; energy_select1=1; energy_select2=1;color=kRed; }//GCDE_123_continuum_new 578-658 keV  0 sources

 if(id==1051){strcat( infile,"SPI-SKY-IMA.spidiffit.1051.fits"   );  spidiffit_version=22; n_map_component=10; energy_select1=1; energy_select2=1;color=kRed; }//GCDE_123_continuum_new  18- 98 keV 91 sources
 if(id==1052){strcat( infile,"SPI-SKY-IMA.spidiffit.1052.fits"   );  spidiffit_version=22; n_map_component=10; energy_select1=1; energy_select2=1;color=kRed; }//GCDE_123_continuum_new  98-178 keV 91 sources

 if(id==1071){strcat( infile,"SPI-SKY-IMA.spidiffit.1071.fits"   );  spidiffit_version=22; n_map_component=10; energy_select1=1; energy_select2=1;color=kRed; }//GCDE_123_continuum_new 18-58 keV 91 sources
 if(id==1072){strcat( infile,"SPI-SKY-IMA.spidiffit.1072.fits"   );  spidiffit_version=22; n_map_component=10; energy_select1=1; energy_select2=1;color=kRed; }//GCDE_123_continuum_new 58-98 keV 91 sources
 if(id==1073){strcat( infile,"SPI-SKY-IMA.spidiffit.1073.fits"   );  spidiffit_version=22; n_map_component=10; energy_select1=1; energy_select2=1;color=kRed; }//GCDE_123_continuum_new 98-138 keV 91 sources
 if(id==1074){strcat( infile,"SPI-SKY-IMA.spidiffit.1074.fits"   );  spidiffit_version=22; n_map_component=10; energy_select1=1; energy_select2=1;color=kRed; }//GCDE_123_continuum_new 138-178 keV 91 sources
 if(id==1075){strcat( infile,"SPI-SKY-IMA.spidiffit.1075.fits"   );  spidiffit_version=22; n_map_component=10; energy_select1=1; energy_select2=1;color=kRed; }//GCDE_123_continuum_new 178-218 keV 91 sources
 if(id==1076){strcat( infile,"SPI-SKY-IMA.spidiffit.1076.fits"   );  spidiffit_version=22; n_map_component=10; energy_select1=1; energy_select2=1;color=kRed; }//GCDE_123_continuum_new 218-258 keV 91 sources
 if(id==1077){strcat( infile,"SPI-SKY-IMA.spidiffit.1077.fits"   );  spidiffit_version=22; n_map_component=10; energy_select1=1; energy_select2=1;color=kRed; }//GCDE_123_continuum_new  18-38  keV 91 sources
 if(id==1078){strcat( infile,"SPI-SKY-IMA.spidiffit.1078.fits"   );  spidiffit_version=22; n_map_component=10; energy_select1=1; energy_select2=1;color=kRed; }//GCDE_123_continuum_new  18-48  keV 91 sources
 if(id==1080){strcat( infile,"SPI-SKY-IMA.spidiffit.1080.fits"   );  spidiffit_version=22; n_map_component=10; energy_select1=1; energy_select2=1;color=kRed; }//GCDE_123_continuum_new 518-1008keV 0  sources

 // reordered components: disk then bulge
 if(id==1081){strcat( infile,"SPI-SKY-IMA.spidiffit.1081.fits"   );  spidiffit_version=22; n_map_component=10; energy_select1=1; energy_select2=1;color=kRed; }//GCDE_123_continuum_new  18-  98 keV    91 sources 
 if(id==1082){strcat( infile,"SPI-SKY-IMA.spidiffit.1082.fits"   );  spidiffit_version=22; n_map_component=10; energy_select1=1; energy_select2=1;color=kRed; }//GCDE_123_continuum_new  98- 178 keV    91 sources 
 if(id==1083){strcat( infile,"SPI-SKY-IMA.spidiffit.1083.fits"   );  spidiffit_version=22; n_map_component=10; energy_select1=1; energy_select2=1;color=kRed; }//GCDE_123_continuum_new 178- 258 keV     0 sources 
 if(id==1084){strcat( infile,"SPI-SKY-IMA.spidiffit.1084.fits"   );  spidiffit_version=22; n_map_component=10; energy_select1=1; energy_select2=1;color=kRed; }//GCDE_123_continuum_new 258- 338 keV     0 sources 
 if(id==1085){strcat( infile,"SPI-SKY-IMA.spidiffit.1085.fits"   );  spidiffit_version=22; n_map_component=10; energy_select1=1; energy_select2=1;color=kRed; }//GCDE_123_continuum_new 338- 418 keV     0 sources 
 if(id==1086){strcat( infile,"SPI-SKY-IMA.spidiffit.1086.fits"   );  spidiffit_version=22; n_map_component=10; energy_select1=1; energy_select2=1;color=kRed; }//GCDE_123_continuum_new 418- 498 keV     0 sources 
 if(id==1087){strcat( infile,"SPI-SKY-IMA.spidiffit.1087.fits"   );  spidiffit_version=22; n_map_component=10; energy_select1=1; energy_select2=1;color=kRed; }//GCDE_123_continuum_new 498- 578 keV     0 sources 
 if(id==1088){strcat( infile,"SPI-SKY-IMA.spidiffit.1088.fits"   );  spidiffit_version=22; n_map_component=10; energy_select1=1; energy_select2=1;color=kRed; }//GCDE_123_continuum_new 578- 658 keV     0 sources 
 if(id==1089){strcat( infile,"SPI-SKY-IMA.spidiffit.1089.fits"   );  spidiffit_version=22; n_map_component=10; energy_select1=1; energy_select2=1;color=kRed; }//GCDE_123_continuum_new 518-1018 keV     0 sources 

  // reordered components: disk then bulge, with 91 sources

 if(id==1090){strcat( infile,"SPI-SKY-IMA.spidiffit.1090.fits"   );  spidiffit_version=22; n_map_component=10; energy_select1=1; energy_select2=1;color=kRed; }//GCDE_123_continuum_new 178-258 keV 91 sources
 if(id==1091){strcat( infile,"SPI-SKY-IMA.spidiffit.1091.fits"   );  spidiffit_version=22; n_map_component=10; energy_select1=1; energy_select2=1;color=kRed; }//GCDE_123_continuum_new 258-338 keV 91 sources
 if(id==1092){strcat( infile,"SPI-SKY-IMA.spidiffit.1092.fits"   );  spidiffit_version=22; n_map_component=10; energy_select1=1; energy_select2=1;color=kRed; }//GCDE_123_continuum_new 338-418 keV 91 sources
 if(id==1093){strcat( infile,"SPI-SKY-IMA.spidiffit.1093.fits"   );  spidiffit_version=22; n_map_component=10; energy_select1=1; energy_select2=1;color=kRed; }//GCDE_123_continuum_new 418-498 keV 91 sources
 if(id==1094){strcat( infile,"SPI-SKY-IMA.spidiffit.1094.fits"   );  spidiffit_version=22; n_map_component=10; energy_select1=1; energy_select2=1;color=kRed; }//GCDE_123_continuum_new 498-578 keV 91 sources
 if(id==1095){strcat( infile,"SPI-SKY-IMA.spidiffit.1095.fits"   );  spidiffit_version=22; n_map_component=10; energy_select1=1; energy_select2=1;color=kRed; }//GCDE_123_continuum_new 578-658 keV 91 sources
 if(id==1096){strcat( infile,"SPI-SKY-IMA.spidiffit.1096.fits"   );  spidiffit_version=22; n_map_component=10; energy_select1=1; energy_select2=1;color=kRed; }//GCDE_123_continuum_new 518-1018keV 91 sources


 if(id==1097){strcat( infile,"SPI-SKY-IMA.spidiffit.1097.fits"   );  spidiffit_version=22; n_map_component=10; energy_select1=1; energy_select2=1;color=kRed; }//GCDE_123_continuum_new  18-  98 keV     0 sources 
 if(id==1098){strcat( infile,"SPI-SKY-IMA.spidiffit.1098.fits"   );  spidiffit_version=22; n_map_component=10; energy_select1=1; energy_select2=1;color=kRed; }//GCDE_123_continuum_new  98- 178 keV     0 sources 

  // reordered components: disk then bulge, with 91 FAKE sources 

 if(id==1101){strcat( infile,"SPI-SKY-IMA.spidiffit.1101.fits"   );  spidiffit_version=22; n_map_component=10; energy_select1=1; energy_select2=1;color=kRed; }//GCDE_123_continuum_new  18- 98 keV 91 sources
 if(id==1102){strcat( infile,"SPI-SKY-IMA.spidiffit.1102.fits"   );  spidiffit_version=22; n_map_component=10; energy_select1=1; energy_select2=1;color=kRed; }//GCDE_123_continuum_new  98-178 keV 91 sources
 if(id==1103){strcat( infile,"SPI-SKY-IMA.spidiffit.1103.fits"   );  spidiffit_version=22; n_map_component=10; energy_select1=1; energy_select2=1;color=kRed; }//GCDE_123_continuum_new 178-258 keV 91 sources
 if(id==1104){strcat( infile,"SPI-SKY-IMA.spidiffit.1104.fits"   );  spidiffit_version=22; n_map_component=10; energy_select1=1; energy_select2=1;color=kRed; }//GCDE_123_continuum_new 258-338 keV 91 sources
 if(id==1105){strcat( infile,"SPI-SKY-IMA.spidiffit.1105.fits"   );  spidiffit_version=22; n_map_component=10; energy_select1=1; energy_select2=1;color=kRed; }//GCDE_123_continuum_new 338-418 keV 91 sources
 if(id==1106){strcat( infile,"SPI-SKY-IMA.spidiffit.1106.fits"   );  spidiffit_version=22; n_map_component=10; energy_select1=1; energy_select2=1;color=kRed; }//GCDE_123_continuum_new 418-498 keV 91 sources
 if(id==1107){strcat( infile,"SPI-SKY-IMA.spidiffit.1107.fits"   );  spidiffit_version=22; n_map_component=10; energy_select1=1; energy_select2=1;color=kRed; }//GCDE_123_continuum_new 498-578 keV 91 sources
 if(id==1108){strcat( infile,"SPI-SKY-IMA.spidiffit.1108.fits"   );  spidiffit_version=22; n_map_component=10; energy_select1=1; energy_select2=1;color=kRed; }//GCDE_123_continuum_new 578-658 keV 91 sources
 if(id==1109){strcat( infile,"SPI-SKY-IMA.spidiffit.1109.fits"   );  spidiffit_version=22; n_map_component=10; energy_select1=1; energy_select2=1;color=kRed; }//GCDE_123_continuum_new 518-1018keV 91 sources


 if(idlistN==10)  color=kRed; //FB2004 poster all red
 if(idlistN==13)  color=kRed;

 if(spidiffit_version!=21 && spidiffit_version!=22){cout<<"invalid spidiffit version: "<<spidiffit_version<< endl;return 1;}

 if(energy_select1!=0) // found data
 {

 fits_open_file(&fptr,infile,READONLY,&status) ; 
 cout<<"FITS read open status= "<<status<<" "<<infile<<endl;
   



    fits_get_num_hdus(fptr,&total_hdus,&status);
    cout<<"total number of header units="<<total_hdus<<endl;


     if(spidiffit_version==21)n_energy=  (total_hdus  - 2 )/(6+n_map_component *2);
     if(spidiffit_version==22)n_energy=  (total_hdus  - 2 )/(8+n_map_component *2);

    cout<<"spidiffit version= "<<spidiffit_version<<" n_energy  ="<<n_energy  <<endl;


    
 
    i_component=0;
    i_energy   =0;

// hdu 1=primary header hdu 2= grouping

     if(spidiffit_version==21)  hdunum= 2 + i_energy*4;
     if(spidiffit_version==22)  hdunum= 2 + i_energy*6;
    fits_movabs_hdu(fptr,hdunum+1,&hdutype,&status ); 
    cout<<"FITS movabs_hdu status= "<<status<<" hdutype="<<hdutype<<endl;

    n_theta=0;
    fits_read_key(fptr,TLONG,"NAXIS1",&n_theta,comment,&status);

    fits_movabs_hdu(fptr,hdunum+2,&hdutype,&status ); 
    cout<<"FITS movabs_hdu status= "<<status<<" hdutype="<<hdutype<<endl;

    n_component=0;
    fits_read_key(fptr,TLONG,"NAXIS1",&n_component ,comment,&status);
   
    cout<<"n_theta="<<n_theta<<" n_component="<< n_component<<endl;

    cout<<"total number of header units="<<total_hdus<<endl;
 
   

    theta_ML=new double[n_theta];
    theta_MC=new double[n_theta];
      cov_ML=new double[n_component*n_component];
      cov_MC=new double[n_component*n_component];


    i_energy1=0;
    i_energy2=n_energy-1;

    if(select_energy==1){ i_energy1=energy_select1-1;   i_energy2=energy_select2-1;     }

   for (i_energy=i_energy1; i_energy<=i_energy2;i_energy++)
   {

     if(spidiffit_version==21)  hdunum= 2 + i_energy*4;
     if(spidiffit_version==22)  hdunum= 2 + i_energy*6;


     // read theta and covariance
    fits_movabs_hdu(fptr,hdunum+1,&hdutype,&status );
    fits_read_img(fptr,TDOUBLE,felement,n_theta,                &nulval,theta_ML,&anynul,&status) ;

    fits_movabs_hdu(fptr,hdunum+2,&hdutype,&status );
    fits_read_img(fptr,TDOUBLE,felement,n_component*n_component,&nulval,  cov_ML,&anynul,&status) ;

    fits_movabs_hdu(fptr,hdunum+3,&hdutype,&status );
    fits_read_img(fptr,TDOUBLE,felement,n_theta,                &nulval,theta_MC,&anynul,&status) ;

    fits_movabs_hdu(fptr,hdunum+4,&hdutype,&status );
    fits_read_img(fptr,TDOUBLE,felement,n_component*n_component,&nulval,  cov_MC,&anynul,&status) ;


    // read MCMC chain  if present
    if(spidiffit_version==22)
    {
    fits_movabs_hdu(fptr,hdunum+5,&hdutype,&status );
    cout<<"FITS movabs_hdu status= "<<status<<" hdutype="<<hdutype<<endl;
    fits_read_key(fptr,TLONG,"NAXIS1",&N_theta_chain,comment,&status);
    fits_read_key(fptr,TLONG,"NAXIS2",&n_accepted   ,comment,&status);
    cout<<" MCMC chain: number of theta values="  <<N_theta_chain<<" number of samples="<< n_accepted<< endl;

    theta_chain=new  float[N_theta_chain*n_accepted];
     logL_chain=new  float[              n_accepted];

    fits_read_img(fptr,TFLOAT ,felement,N_theta_chain*n_accepted,&nulval,theta_chain,&anynul,&status) ;

    fits_movabs_hdu(fptr,hdunum+6,&hdutype,&status );
    cout<<"FITS movabs_hdu status= "<<status<<" hdutype="<<hdutype<<endl;
    fits_read_img(fptr,TFLOAT ,felement,              n_accepted,&nulval, logL_chain,&anynul,&status) ;

    if(galplotdef.verbose== -306)// selectable debug
    {
     i_chain=0;
     for (i_sample_chain=0;i_sample_chain<n_accepted;i_sample_chain++)
     {
      cout<<"sample #"<<i_sample_chain<<": ";
      for(i_theta_chain=0;i_theta_chain<N_theta_chain;i_theta_chain++,i_chain++)
      cout<<theta_chain[i_chain]<<" ";
      cout<<" logL=" <<logL_chain[i_sample_chain];
      cout<<endl;
     }
      cout<<endl;
    }

    }// read MCMC chain



    if(galplotdef.verbose== -301)// selectable debug
    {
    cout<<endl<<"   theta_ML: ";    for(int i=0; i<n_theta;i++)cout<< theta_ML[i]<<" ";
    cout<<endl<<"   theta_MC: ";    for(int i=0; i<n_theta;i++)cout<< theta_MC[i]<<" ";
    }

    if(galplotdef.verbose==  -302)// selectable debug
    {
    cout<<endl<<"   cov_ML: ";
    for(int i=0; i<n_component*n_component;i++)cout<< cov_ML[i]<<" ";
    cout<<endl<<"    cov_MC: ";
    for(int i=0; i<n_component*n_component;i++)cout<< cov_MC[i]<<" ";
    }

    fits_read_key(fptr,TDOUBLE,"E_MIN",&e_min       ,comment,&status);
    fits_read_key(fptr,TDOUBLE,"E_MAX",&e_max       ,comment,&status);
   
    if(galplotdef.verbose==  -305)// selectable debug
    cout<<"e_min = "<<e_min  <<" e_max = "<< e_max  <<" keV"    <<endl;

   
    // read model maps


    intensity        =new double[n_component];
    component_average=new double[n_component];
   
    for(i_component=0; i_component<n_map_component; i_component++)
    {
     if(spidiffit_version==21)  hdunum= 3 + n_energy*4 + i_energy   + i_component * n_energy*2;
     if(spidiffit_version==22)  hdunum= 3 + n_energy*6 + i_energy   + i_component * n_energy*2;

    if(galplotdef.verbose==  -305)// selectable debug
     cout<<endl<<"i_energy icomponent hdunum "<<i_energy<<" "<<i_component<<" "<<hdunum<<endl;

   
    component_average[i_component]=0.;

    model_map.read(infile,hdunum);

    if(galplotdef.verbose==  -304)// selectable debug
    {
     cout<<endl<<"model map for i_energy i_component hdunum "<<i_energy<<" "<<i_component<<" "<<hdunum<<endl;
     model_map.print();
    }

    bin_count=0;

    for(region=1;region<=4;region++)
    {
    if(region==1)
    {
     delta_longitude=galplotdef.long_min1 - model_map.CRVAL[0];
     if    (delta_longitude <   0.0) delta_longitude+=360.0;
     if    (delta_longitude > 360.0) delta_longitude-=360.0;
     ilmin=(int)((delta_longitude  + .0001) / model_map.CDELT[0]);

     delta_longitude=galplotdef.long_max1 - model_map.CRVAL[0];
     if    (delta_longitude <   0.0) delta_longitude+=360.0;
     if    (delta_longitude > 360.0) delta_longitude-=360.0;
     ilmax=(int)((delta_longitude  + .0001) / model_map.CDELT[0]);

     //  ilmin=(galplotdef.long_min1 - model_map.CRVAL[0] + .0001) / model_map.CDELT[0];
     //  ilmax=(galplotdef.long_max1 - model_map.CRVAL[0] + .0001) / model_map.CDELT[0];
    ibmin=(int)((galplotdef. lat_min1 - model_map.CRVAL[1] + .0001) / model_map.CDELT[1]);
    ibmax=(int)((galplotdef. lat_max1 - model_map.CRVAL[1] + .0001) / model_map.CDELT[1]);
    }

    if(region==2)
    {
     delta_longitude=galplotdef.long_min1 - model_map.CRVAL[0];
     if    (delta_longitude <   0.0) delta_longitude+=360.0;
     if    (delta_longitude > 360.0) delta_longitude-=360.0;
     ilmin=(int)((delta_longitude  + .0001) / model_map.CDELT[0]);

     delta_longitude=galplotdef.long_max1 - model_map.CRVAL[0];
     if    (delta_longitude <   0.0) delta_longitude+=360.0;
     if    (delta_longitude > 360.0) delta_longitude-=360.0;
     ilmax=(int)((delta_longitude  + .0001) / model_map.CDELT[0]);

      //    ilmin=(galplotdef.long_min1 - model_map.CRVAL[0] + .0001) / model_map.CDELT[0];
      //   ilmax=(galplotdef.long_max1 - model_map.CRVAL[0] + .0001) / model_map.CDELT[0];
    ibmin=(int)((galplotdef. lat_min2 - model_map.CRVAL[1] + .0001) / model_map.CDELT[1]);
    ibmax=(int)((galplotdef. lat_max2 - model_map.CRVAL[1] + .0001) / model_map.CDELT[1]);
    }
    if(region==3)
    {

     delta_longitude=galplotdef.long_min2 - model_map.CRVAL[0];
     if    (delta_longitude <   0.0) delta_longitude+=360.0;
     if    (delta_longitude > 360.0) delta_longitude-=360.0;
     ilmin=(int)((delta_longitude  + .0001) / model_map.CDELT[0]);

     delta_longitude=galplotdef.long_max2 - model_map.CRVAL[0];
     if    (delta_longitude <   0.0) delta_longitude+=360.0;
     if    (delta_longitude > 360.0) delta_longitude-=360.0;
     ilmax=(int)((delta_longitude  + .0001) / model_map.CDELT[0]);

     //  ilmin=(galplotdef.long_min2 - model_map.CRVAL[0] + .0001) / model_map.CDELT[0];
     //  ilmax=(galplotdef.long_max2 - model_map.CRVAL[0] + .0001) / model_map.CDELT[0];
    ibmin=(int)((galplotdef. lat_min1 - model_map.CRVAL[1] + .0001) / model_map.CDELT[1]);
    ibmax=(int)((galplotdef. lat_max1 - model_map.CRVAL[1] + .0001) / model_map.CDELT[1]);
    }

    if(region==4)
    {

     delta_longitude=galplotdef.long_min2 - model_map.CRVAL[0];
     if    (delta_longitude <   0.0) delta_longitude+=360.0;
     if    (delta_longitude > 360.0) delta_longitude-=360.0;
     ilmin=(int)((delta_longitude  + .0001) / model_map.CDELT[0]);

     delta_longitude=galplotdef.long_max2 - model_map.CRVAL[0];
     if    (delta_longitude <   0.0) delta_longitude+=360.0;
     if    (delta_longitude > 360.0) delta_longitude-=360.0;
     ilmax=(int)((delta_longitude  + .0001) / model_map.CDELT[0]);

     //ilmin=(galplotdef.long_min2 - model_map.CRVAL[0] + .0001) / model_map.CDELT[0];
     //ilmax=(galplotdef.long_max2 - model_map.CRVAL[0] + .0001) / model_map.CDELT[0];

    ibmin=(int)((galplotdef. lat_min2 - model_map.CRVAL[1] + .0001) / model_map.CDELT[1]);
    ibmax=(int)((galplotdef. lat_max2 - model_map.CRVAL[1] + .0001) / model_map.CDELT[1]);
    }
    if(ilmin<0)ilmin=0;
    if(ilmax<0)ilmax=0;
    if(ibmin<0)ibmin=0;
    if(ibmax<0)ibmax=0;
    if(ilmin>model_map.NAXES[0]-1 )ilmin=model_map.NAXES[0]-1;
    if(ilmax>model_map.NAXES[0]-1 )ilmax=model_map.NAXES[0]-1;
    if(ibmin>model_map.NAXES[1]-1 )ibmin=model_map.NAXES[1]-1;
    if(ibmax>model_map.NAXES[1]-1 )ibmax=model_map.NAXES[1]-1;

 
    for(il=ilmin;il<=ilmax;il++)
    for(ib=ibmin;ib<=ibmax;ib++)
    {
     component_average[i_component]+=model_map(il,ib);
     bin_count++;
    }



      cout<<" ilmin="<<ilmin <<" ilmax="<<ilmax<<" ibmin="<<ibmin<<" ibmax="<<ibmax<<" intensity="<<intensity[i_component]<< " bin_count="<<bin_count<<endl;
    }//region




 
  
    component_average[i_component]/=bin_count;

    if ((spidiffit_version==21 || id<= 970) && theta_MC[i_component]!=0.0)
    component_average[i_component]/=theta_MC[i_component]; // case maps created with MCMC parameters (spidiffit.par skymap_method=1)


    intensity[i_component]=theta_ML[i_component]*component_average[i_component]; 
    intensity[i_component]/=(e_max-e_min)*1.e-3; // MeV^-1



    if(galplotdef.verbose==  -305)// selectable debug
    cout<<"SPI energy "<<e_min<<"-"<<e_max<<"keV   i_component "<<i_component<<" intensity="<<intensity[i_component]<< " bin_count="<<bin_count<<endl;

    if(viewdiffit_flag==1 && theta_MC[i_component]<=0.0) // for more exact comparison with viewdiffit17.pro
      {
       intensity        [i_component]=0.0;// as in viewdiffit17.pro
       component_average[i_component]=0.0;
      }

    }//i_component map components


// sources

    dtr=acos(-1.0)/180.; //degrees to radians
    solid_angle=bin_count*model_map.CDELT[0]*model_map.CDELT[1]*dtr*dtr;//solid angle in sterad


    n_significant_sources=0;

    for(i_component=n_map_component; i_component<n_component; i_component++)
    {
      component_average[i_component]=1.0                  /solid_angle;
      intensity[i_component]        =theta_ML[i_component]/solid_angle;
      intensity[i_component]/=(e_max-e_min)*1.e-3; // MeV^-1

      sigma= theta_ML[i_component]   /    sqrt(cov_ML[i_component*n_component + i_component]);

      if(sigma>=3.0)n_significant_sources+=1;

      cout<<"id "<<id<<" source "<<i_component-n_map_component+1
          <<" E="<<e_min<<"-"<<e_max
	  <<" flux="  <<theta_ML[i_component]<<"+-"<<sqrt(cov_ML[i_component*n_component + i_component])
          <<" sigma="<< sigma;
      if(sigma>=3.0)cout<<"*";
      cout<< " bin_count="<<bin_count
          <<" solid angle="<<solid_angle<<" intensity= "<< intensity[i_component]<<endl;

    }//i_component source components

    cout<<"id "<<id<<"  E="<<e_min<<"-"<<e_max<<" :  "<<n_significant_sources<<" sources above 3 sigma"<<endl;

    //////////////////////////////////////////////////////////////////////////////

   for (iplot=0;iplot<=1;iplot++)
   {
  
    if(iplot==0)color=kBlue;
    if(iplot==1)color=kRed ;

    i_component1=galplotdef.data_INTEGRAL_comp[iplot*2  ]-1; // user value starts at 1, internal starts at 0
    i_component2=galplotdef.data_INTEGRAL_comp[iplot*2+1]-1;

    if(i_component1>=0 && i_component2>=0 )
    if(i_component1<= n_component-1 )
    {

    if(i_component1>n_component-1)i_component1=n_component-1;
    if(i_component2>n_component-1)i_component2=n_component-1;

    cout<<"components "<<i_component1+1<<" to  "<<i_component2+1<<endl;

    intensity_total=0.;
    for(i_component=i_component1;i_component<=i_component2;i_component++)
    {
     intensity_total+=intensity[i_component];
     cout<<"i_component intensity[i_component] intensity[i_component] "<<i_component<<" "<<intensity[i_component]<<" "<< intensity[i_component]<<endl;
    }


    dintensity=0;
    dintensity_MC=0;   dintensity_ML=0;

    for(i_component=i_component1;i_component<=i_component2;i_component++)
    for(j_component=i_component1;j_component<=i_component2;j_component++)
    {
      dintensity_ML+=   cov_ML[i_component*n_component + j_component]
                   * component_average[i_component]* component_average[j_component];
      dintensity_MC+=   cov_MC[i_component*n_component + j_component]
                   * component_average[i_component]* component_average[j_component];


    }

    dintensity_ML=sqrt(dintensity_ML);
    dintensity_ML/=(e_max-e_min)*1.e-3; // MeV^-1
    dintensity_MC=sqrt(dintensity_MC);
    dintensity_MC/=(e_max-e_min)*1.e-3; // MeV^-1

    dintensity=sqrt(dintensity);
    dintensity/=(e_max-e_min)*1.e-3; // MeV^-1

    dintensity=    dintensity_ML;

    cout.precision(3);
    cout<<"SPI: energy "<<e_min<<"-"<<e_max<<"keV components "
        <<i_component1+1<<"-"<<i_component2+1<<"  total intensity="<<intensity_total
        <<" +/- (ML)"<<dintensity_ML<<" (MC)"<<dintensity_MC<<endl;


    // LATeX table
    cout.precision(3);

    cout<<" "<<e_min<<"-"<<e_max <<" & "                       
        <<i_component1+1<<"-"<<i_component2+1  <<" & "  
        <<intensity_total                  <<" & "
        <<dintensity_ML                    <<" & "
        <<id                  
        <<"\\\\"                                                 // yields "\\"  
        <<"% LaTex"
	<<endl;

    
    fprintf(txtFILE,"%6.1f-%6.1f  & %d-%d  &",e_min,e_max,i_component1+1,i_component2+1);
    fprintf(txtFILE,"%5.3f &  %5.3f &  %d \\\\ ",intensity_total,dintensity_ML,id);                // "\\\\" yields "\\" 
    fprintf(txtFILE,"\n");

    e_mean=sqrt(e_min*e_max)*1e-3;
    //   dintensity=intensity_total*.1;
    ymin= (intensity_total-dintensity)*e_mean*e_mean;
    ymax= (intensity_total+dintensity)*e_mean*e_mean;

    ymean=0.5*(ymin+ymax);

    if(ymin < galplotdef.gamma_spectrum_Imin)ymin = galplotdef.gamma_spectrum_Imin;
    //cout<<"emean ymin ymax="<<e_mean<<" "<<ymin<<" "<<ymax<<endl;

    // horizontal shift for multiple points at same energy
    shift=1.0 + iplot*0.05;

    line=new TLine(e_mean*shift,ymin,e_mean*shift,ymax);
    line->SetLineColor(color);
    line->SetLineWidth(3  );
    line->Draw();

    line=new TLine(e_min*1e-3*shift, ymean, e_max*1e-3*shift, ymean);
    line->SetLineColor(color);
    line->SetLineWidth(3  );
    line->Draw();


    if(spidiffit_version==22) analyse_MCMC_SPI();
  
     }//if icomponent1
    }//iplot


   }//i_energy




  } //if found

  }//id


  fprintf(txtFILE,"=============================================================\n");

  cout<<" <<<< plot_SPI_spectrum    "<<endl;
   return status;
}


/////////////////////////////////////////////////////////////////////////////////////


int Galplot::analyse_MCMC_SPI()

{ 

  using namespace SPI;

  double intensity_total_sample;
  double *intensity_total_posterior;
  double *intensity_MC;
  double  intensity_total_bin;
  double  intensity_total_min;
  double  intensity_total_max;
  int   n_intensity_total_posterior;
  int     ibin;

 cout<<" >>>> analyse_MCMC_SPI     "<<endl;

 cout<<"n_theta      ="<<n_theta      <<endl;
 cout<<"n_theta_chain="<<N_theta_chain<<endl;

    if(galplotdef.verbose== -306)// selectable debug
    {
     i_chain=0;
     for (i_sample_chain=0;i_sample_chain<n_accepted;i_sample_chain++)
     {
      cout<<"sample #"<<i_sample_chain<<": ";
      for(i_theta_chain=0;i_theta_chain<N_theta_chain;i_theta_chain++,i_chain++)
          cout<<theta_chain[i_chain]<<" ";

      cout<<" logL=" <<logL_chain[i_sample_chain];
      cout<<endl;
     }
      cout<<endl;
    }

intensity_MC=new double[n_component]; //AWS20050131

for(i_component=i_component1;i_component<=i_component2;i_component++) intensity_MC[i_component]=0;


for (i_sample_chain=0;i_sample_chain<n_accepted;i_sample_chain++)
{
   for(i_component=i_component1;i_component<=i_component2;i_component++)
    intensity_MC[i_component] += theta_chain[i_sample_chain*N_theta_chain + i_component] * component_average[i_component];

}


 for(i_component=i_component1;i_component<=i_component2;i_component++)
 {
  intensity_MC[i_component] /= n_accepted;
 }

intensity_total=0.;
for(i_component=i_component1;i_component<=i_component2;i_component++)    intensity_total += intensity_MC[i_component];
 
// variance and min, max of total intensity

dintensity=0;
 intensity_total_min= 1e20;
 intensity_total_max=-1e20;

for (i_sample_chain=0;i_sample_chain<n_accepted;i_sample_chain++)
{
  intensity_total_sample=0;

  for(i_component=i_component1;i_component<=i_component2;i_component++)
    intensity_total_sample += theta_chain[i_sample_chain*N_theta_chain + i_component] * component_average[i_component];

  dintensity += pow(intensity_total_sample - intensity_total ,2);

  if  ( intensity_total_sample> intensity_total_max)intensity_total_max=intensity_total_sample;
  if  ( intensity_total_sample< intensity_total_min)intensity_total_min=intensity_total_sample;

  // cout<<intensity_total_sample<<" "<<intensity_total_min<<" "<<intensity_total_max<<endl;
}

 dintensity /= n_accepted;
 dintensity  =sqrt( dintensity);

// posterior distribution of total intensity

n_intensity_total_posterior=10;
intensity_total_posterior=new double[n_intensity_total_posterior];
intensity_total_bin=(intensity_total_max - intensity_total_min)/(n_intensity_total_posterior-1);

for (i_sample_chain=0;i_sample_chain<n_accepted;i_sample_chain++)
{
  intensity_total_sample=0;

  for(i_component=i_component1;i_component<=i_component2;i_component++)
    intensity_total_sample += theta_chain[i_sample_chain*N_theta_chain + i_component] * component_average[i_component];

  ibin= (int)((intensity_total_sample  - intensity_total_min )/intensity_total_bin);
  //cout<<intensity_total_sample<<" "<<intensity_total_min<<" "<<intensity_total_bin<<" " <<ibin<<endl;
  if(ibin>=0 && ibin < n_intensity_total_posterior)intensity_total_posterior[ibin]+=1;
}

// normalize posterior 
for(ibin=0;ibin< n_intensity_total_posterior;ibin++)intensity_total_posterior[ibin]/=n_accepted;


// convert to required units

 for(i_component=i_component1;i_component<=i_component2;i_component++)
 {
  intensity_MC[i_component]/= (e_max-e_min)*1.e-3; // MeV^-1
 }

 intensity_total        /= (e_max-e_min)*1.e-3; // MeV^-1
 dintensity             /= (e_max-e_min)*1.e-3; // MeV^-1
 intensity_total_min    /= (e_max-e_min)*1.e-3; // MeV^-1
 intensity_total_max    /= (e_max-e_min)*1.e-3; // MeV^-1
 intensity_total_bin    /= (e_max-e_min)*1.e-3; // MeV^-1

 cout<<"SPI: energy "<<e_min<<"-"<<e_max<<" keV "<<endl;
 cout<<"number of  MCMC samples = "<<n_accepted<<endl;

 for(i_component=i_component1;i_component<=i_component2;i_component++)
 {
  cout<<" component "<<i_component<<" intensity= "<<intensity_MC[i_component]<<endl; 
 }
 cout<<"       total intensity= "<<intensity_total<<" +/- "<<dintensity<<endl;
 cout<<"sample total min inten= "<<intensity_total_min <<endl;
 cout<<"sample total max inten= "<<intensity_total_max <<endl;

 cout<<" ** posterior of total intensity **" <<endl;

for(ibin=0;ibin< n_intensity_total_posterior;ibin++)
{
  cout<<"intensity "<<intensity_total_min+intensity_total_bin* ibin
      <<" - "       <<intensity_total_min+intensity_total_bin*(ibin+1)
      <<" prob = "  <<intensity_total_posterior[ibin]   <<endl;
}

 cout<<" <<<< analyse_MCMC_SPI     "<<endl;


 return 0;
}
