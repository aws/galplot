#include"Galplot.h"                    //AWS20050920
#include"galprop_classes.h"
#include"galprop.h"





int Galplot::convolve_EGRET()
{
    
   cout<<">>>>convolve_EGRET "<<endl;





   int i_long,i_lat,ip,i_E_EGRET,i_comp;
   double emin, emax, e1,e2, intensity_esq1,  intensity_esq2;
   double delta;
   double isotropic_intensity;

   int i_warn=0;

     convolved.EGRET_bremss_skymap.init             (galaxy.n_long,galaxy.n_lat,data.n_E_EGRET);
   unconvolved.EGRET_bremss_skymap.init             (galaxy.n_long,galaxy.n_lat,data.n_E_EGRET);

     convolved.EGRET_IC_iso_skymap=new Distribution[galaxy.n_ISRF_components];
   for(i_comp=0;i_comp<galaxy.n_ISRF_components;i_comp++)
     convolved.EGRET_IC_iso_skymap[i_comp].init   (galaxy.n_long,galaxy.n_lat,data.n_E_EGRET);
    
   unconvolved.EGRET_IC_iso_skymap=new Distribution[galaxy.n_ISRF_components];
   for(i_comp=0;i_comp<galaxy.n_ISRF_components;i_comp++)
     unconvolved.EGRET_IC_iso_skymap[i_comp].init   (galaxy.n_long,galaxy.n_lat,data.n_E_EGRET);

  
     convolved.EGRET_IC_aniso_skymap=new Distribution[galaxy.n_ISRF_components];                   //AWS20060907
   for(i_comp=0;i_comp<galaxy.n_ISRF_components;i_comp++)                                          //AWS20060907
     convolved.EGRET_IC_aniso_skymap[i_comp].init   (galaxy.n_long,galaxy.n_lat,data.n_E_EGRET);   //AWS20060907
    
   unconvolved.EGRET_IC_aniso_skymap=new Distribution[galaxy.n_ISRF_components];                   //AWS20060907
   for(i_comp=0;i_comp<galaxy.n_ISRF_components;i_comp++)                                          //AWS20060907
   unconvolved.EGRET_IC_aniso_skymap[i_comp].init   (galaxy.n_long,galaxy.n_lat,data.n_E_EGRET);   //AWS20060907
   

     convolved.EGRET_pi0_decay_skymap.init          (galaxy.n_long,galaxy.n_lat,data.n_E_EGRET);
   unconvolved.EGRET_pi0_decay_skymap.init          (galaxy.n_long,galaxy.n_lat,data.n_E_EGRET);

     convolved.EGRET_isotropic_skymap.init          (galaxy.n_long,galaxy.n_lat,data.n_E_EGRET);
   unconvolved.EGRET_isotropic_skymap.init          (galaxy.n_long,galaxy.n_lat,data.n_E_EGRET);


     convolved.EGRET_total_skymap    .init          (galaxy.n_long,galaxy.n_lat,data.n_E_EGRET);
   unconvolved.EGRET_total_skymap    .init          (galaxy.n_long,galaxy.n_lat,data.n_E_EGRET);



     






   cout<<"i_long "<<endl;

   for (i_long   =0;  i_long<galaxy.n_long; i_long++)
   {
    cout<<i_long<<" ";
   for (i_lat    =0;  i_lat <galaxy.n_lat ; i_lat++)
   {
   for (i_E_EGRET=0;  i_E_EGRET  <data.n_E_EGRET;i_E_EGRET++)
   {

     //cout<<"i_long i_lat i_E_EGRET "<<i_long<<" "<<i_lat<<" "<<i_E_EGRET<<endl;

     convolved.EGRET_bremss_skymap   .d2[i_long][i_lat].s[i_E_EGRET]=0;

     for(i_comp=0;i_comp<galaxy.n_ISRF_components;i_comp++)
     convolved.EGRET_IC_iso_skymap[i_comp]   .d2[i_long][i_lat].s[i_E_EGRET]=0;

     convolved.EGRET_pi0_decay_skymap.d2[i_long][i_lat].s[i_E_EGRET]=0;

     emin=data.E_EGRET[i_E_EGRET  ];
     emax=data.E_EGRET[i_E_EGRET+1];

     // for (ip=0; ip<galaxy.n_E_gammagrid-1 ; ip++)   avoid last input energy @ [ip+1] since sometimes NULL for bremss
   for (ip=0; ip<galaxy.n_E_gammagrid-1 ; ip++)                    
     {
    
     e1=  galaxy.E_gamma[ip];
     e2=  galaxy.E_gamma[ip+1];
     intensity_esq1=galaxy.bremss_skymap.d2[i_long][i_lat].s[ip  ];
     intensity_esq2=galaxy.bremss_skymap.d2[i_long][i_lat].s[ip+1];

     // -------------------------------------------------- trap error in input map
     if(intensity_esq1<0.0||intensity_esq2<0.0)
     { 
       if(i_warn<10)
	 {
       cout<< " warning: bremsstrahlung intensity -ve ! this warning will not be repeated"<<endl;
   
       cout<<"i_long i_lat emin emax ip e1 e2 intensity_esq1 intensity_esq2 delta bremss "<<i_long<<" "<<i_lat<<" "<<emin<<" "<<emax<<" "
       	 << ip<<" "<<e1<<" "<<e2<<" " <<intensity_esq1<<" "<<intensity_esq2
	   <<" delta=" <<delta<<" bremss="<<convolved.EGRET_bremss_skymap   .d2[i_long][i_lat].s[i_E_EGRET]<<endl;
       i_warn++;
	 }//if
     }

     //----------------------------------------------------------------------------

     delta=energy_integral( emin, emax, e1, e2, intensity_esq1,intensity_esq2);
     
       
     convolved.EGRET_bremss_skymap   .d2[i_long][i_lat].s[i_E_EGRET] += delta;

     //if(i_E_EGRET==1&&i_long==146&&i_lat==198)
     //  cout<<"i_long i_lat emin emax ip e1 e2 intensity_esq1 intensity_esq2 delta bremss "<<i_long<<" "<<i_lat<<" "<<emin<<" "<<emax<<" "
     //  	 << ip<<" "<<e1<<" "<<e2<<" " <<intensity_esq1<<" "<<intensity_esq2
     //	   <<" delta=" <<delta<<" bremss="<<convolved.EGRET_bremss_skymap   .d2[i_long][i_lat].s[i_E_EGRET]<<endl;
     //if(i_E_EGRET==1&&i_long==146)cout<<i_long<<" "<<i_lat<<" "<<convolved.EGRET_bremss_skymap   .d2[i_long][i_lat].s[i_E_EGRET]<<endl;


   for(i_comp=0;i_comp<galaxy.n_ISRF_components;i_comp++)
     {
     intensity_esq1=galaxy.IC_iso_skymap[i_comp].d2[i_long][i_lat].s[ip  ];
     intensity_esq2=galaxy.IC_iso_skymap[i_comp].d2[i_long][i_lat].s[ip+1];

     delta=energy_integral( emin, emax, e1, e2, intensity_esq1,intensity_esq2);
     /*
     cout<<"emin emax e1 e2 intensity_esq1 intensity_esq2"<<" "<<emin<<" "<<emax<<" "
      <<e1<<" "<<e2<<" " <<intensity_esq1<<" "<<intensity_esq2
      <<" delta=" <<delta<<endl;
     */
     convolved.EGRET_IC_iso_skymap[i_comp]   .d2[i_long][i_lat].s[i_E_EGRET] += delta;


     // aniso IC                                                                           AWS20060907
     if(galdef.IC_anisotropic > 0)                                                       //AWS20060907
     {
      intensity_esq1=galaxy.IC_aniso_skymap[i_comp].d2[i_long][i_lat].s[ip  ];           //AWS20060907
      intensity_esq2=galaxy.IC_aniso_skymap[i_comp].d2[i_long][i_lat].s[ip+1];           //AWS20060907

      delta=energy_integral( emin, emax, e1, e2, intensity_esq1,intensity_esq2);         //AWS20060907
     
      convolved.EGRET_IC_aniso_skymap[i_comp]   .d2[i_long][i_lat].s[i_E_EGRET] += delta;//AWS20060907
     }//if 

     }

     intensity_esq1=galaxy.pi0_decay_skymap.d2[i_long][i_lat].s[ip  ];
     intensity_esq2=galaxy.pi0_decay_skymap.d2[i_long][i_lat].s[ip+1];

     delta=energy_integral( emin, emax, e1, e2, intensity_esq1,intensity_esq2);

     /*
     cout<<"emin emax e1 e2 intensity_esq1 intensity_esq2"<<" "<<emin<<" "<<emax<<" "
      <<e1<<" "<<e2<<" " <<intensity_esq1<<" "<<intensity_esq2
      <<" delta=" <<delta<<endl;
     */
     convolved.EGRET_pi0_decay_skymap   .d2[i_long][i_lat].s[i_E_EGRET] += delta;
     }
   }
   }
   }


   cout<<endl;

   //cout<<"convolved.EGRET_bremss_skymap:"<<endl; convolved.EGRET_bremss_skymap.print();

   // save unconvolved map before in-place convolution
    unconvolved.EGRET_bremss_skymap   =convolved.EGRET_bremss_skymap;

   for(i_comp=0;i_comp<galaxy.n_ISRF_components;i_comp++)
    unconvolved.EGRET_IC_iso_skymap[i_comp]=convolved.EGRET_IC_iso_skymap[i_comp];

   if(galdef.IC_anisotropic > 0)                                                       //AWS20060907
   for(i_comp=0;i_comp<galaxy.n_ISRF_components;i_comp++)                                //AWS20060907
    unconvolved.EGRET_IC_aniso_skymap[i_comp]=convolved.EGRET_IC_aniso_skymap[i_comp];   //AWS20060907


    unconvolved.EGRET_pi0_decay_skymap=convolved.EGRET_pi0_decay_skymap;


   if(galplotdef.convolve_EGRET>=1)
   {

      convolve(convolved.EGRET_bremss_skymap,        data.EGRET_psf);

     for(i_comp=0;i_comp<galaxy.n_ISRF_components;i_comp++)
      convolve(convolved.EGRET_IC_iso_skymap[i_comp],data.EGRET_psf);

     // aniso IC                                                                           AWS20060907
     if(galdef.IC_anisotropic > 0)                                                       //AWS20060907
     for(i_comp=0;i_comp<galaxy.n_ISRF_components;i_comp++)                              //AWS20060907
      convolve(convolved.EGRET_IC_aniso_skymap[i_comp],data.EGRET_psf);                  //AWS20060907

      convolve(convolved.EGRET_pi0_decay_skymap,     data.EGRET_psf);

     }//if


   // isotropic components

   for (i_E_EGRET=0;  i_E_EGRET  <data.n_E_EGRET;i_E_EGRET++)
   {
    isotropic_intensity=0.;//

    if(galplotdef.isotropic_type==1) // integrate power law
    isotropic_intensity=galplotdef.isotropic_const
                        *(  pow(data.E_EGRET[i_E_EGRET+1],1.0-galplotdef.isotropic_g)
                           -pow(data.E_EGRET[i_E_EGRET  ],1.0-galplotdef.isotropic_g)  )
                        /                         (1.0-galplotdef.isotropic_g);

    if(galplotdef.isotropic_type==2) // explicit list of values
    isotropic_intensity=galplotdef.isotropic_EGRET[i_E_EGRET];

    cout<<"isotropic_intensity="<<isotropic_intensity<<endl;


    for (i_long   =0;  i_long<galaxy.n_long; i_long++)
    {  
     for (i_lat    =0;  i_lat <galaxy.n_lat ; i_lat++)
     {
      unconvolved.EGRET_isotropic_skymap   .d2[i_long][i_lat].s[i_E_EGRET]=isotropic_intensity;
     }//i_long
    }//i_lat

   }//i_E_EGRET

   convolved.EGRET_isotropic_skymap = unconvolved.EGRET_isotropic_skymap;

   // total skymaps


unconvolved.EGRET_total_skymap =unconvolved.EGRET_bremss_skymap;
unconvolved.EGRET_total_skymap+=unconvolved.EGRET_pi0_decay_skymap;


if(galdef.IC_anisotropic ==0)                                                                        //AWS20060907                                                
for(i_comp=0;i_comp<galaxy.n_ISRF_components;i_comp++)
unconvolved.EGRET_total_skymap+=unconvolved.EGRET_IC_iso_skymap[i_comp];


if(galdef.IC_anisotropic > 0)                                                                        //AWS20060907  
for(i_comp=0;i_comp<galaxy.n_ISRF_components;i_comp++)//AWS20060907  
unconvolved.EGRET_total_skymap+=unconvolved.EGRET_IC_aniso_skymap[i_comp];                           //AWS20060907  


unconvolved.EGRET_total_skymap+=unconvolved.EGRET_isotropic_skymap;




  convolved.EGRET_total_skymap =  convolved.EGRET_bremss_skymap;
  convolved.EGRET_total_skymap+=  convolved.EGRET_pi0_decay_skymap;


if(galdef.IC_anisotropic ==0)                                                                        //AWS20060907  
for(i_comp=0;i_comp<galaxy.n_ISRF_components;i_comp++)
  convolved.EGRET_total_skymap+=  convolved.EGRET_IC_iso_skymap[i_comp];

if(galdef.IC_anisotropic > 0)                                                                        //AWS20060907  
for(i_comp=0;i_comp<galaxy.n_ISRF_components;i_comp++)                                               //AWS20060907
  convolved.EGRET_total_skymap+=  convolved.EGRET_IC_aniso_skymap[i_comp];                             //AWS20060907

  convolved.EGRET_total_skymap+=  convolved.EGRET_isotropic_skymap;

 cout<<"unconvolved.EGRET_bremss_skymap    max="<<unconvolved.EGRET_bremss_skymap.max()<<endl;
 for(i_comp=0;i_comp<galaxy.n_ISRF_components;i_comp++)
 cout<<"unconvolved.EGRET_IC_iso_skymap    max="<<unconvolved.EGRET_IC_iso_skymap[i_comp].max()<<endl;

 if(galdef.IC_anisotropic > 0)                                                                        //AWS20060907
 for(i_comp=0;i_comp<galaxy.n_ISRF_components;i_comp++)
 cout<<"unconvolved.EGRET_IC_aniso_skymap  max="<<unconvolved.EGRET_IC_aniso_skymap[i_comp].max()<<endl;

 cout<<"unconvolved.EGRET_pi0_decay_skymap max="<<unconvolved.EGRET_pi0_decay_skymap.max()<<endl;
 cout<<"unconvolved.EGRET_isotropic_skymap max="<<unconvolved.EGRET_isotropic_skymap.max()<<endl;
 cout<<"unconvolved.EGRET_total_skymap     max="<<unconvolved.EGRET_total_skymap.max()<<endl;

 cout<<"  convolved.EGRET_bremss_skymap    max="<<  convolved.EGRET_bremss_skymap.max()<<endl;
 for(i_comp=0;i_comp<galaxy.n_ISRF_components;i_comp++)

 if(galdef.IC_anisotropic > 0)                                                                        //AWS20060907
 for(i_comp=0;i_comp<galaxy.n_ISRF_components;i_comp++)
 cout<<"  convolved.EGRET_IC_aniso_skymap  max="<<  convolved.EGRET_IC_aniso_skymap[i_comp].max()<<endl;

 cout<<"  convolved.EGRET_IC_iso_skymap    max="<<  convolved.EGRET_IC_iso_skymap[i_comp].max()<<endl;
 cout<<"  convolved.EGRET_pi0_decay_skymap max="<<  convolved.EGRET_pi0_decay_skymap.max()<<endl;
 cout<<"  convolved.EGRET_isotropic_skymap max="<<  convolved.EGRET_isotropic_skymap.max()<<endl;
 cout<<"  convolved.EGRET_total_skymap     max="<<  convolved.EGRET_total_skymap.max()<<endl;

  cout<<"<<<<convolve_EGRET "<<endl;
   return 0;

}
