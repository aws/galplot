using namespace std;

#include<iostream>
#include"fitsio.h"
#include <cstring>  //AWS20111010 for sles11 gcc 4.3.4

#include"RMF.h"
//RMF::RMF(){} 

//////////////////////////////////////////////////////
  
  double RMF:: f(double E_in, double E_out)
{
  return 0.;
}
///////////////////////////////////////////////////////
  int RMF:: read (char *directory)
{
  char infile[500];
  char err_text[100];
  char comment[100];
  fitsfile *fptr;
  int status;
  int hdunum,hdutype,total_hdus,colnum;
  int n_columns,n_rows;
  double nulval=0.;
  int anynul   =0;
  int i,j;

  cout<< "RMF::read  (" << directory<<")" <<endl;
  strcpy(infile,directory);
  strcat(infile,"spi_rmf1_rsp_0001.fits");
  cout<<"   infile "<<infile<<endl;

  status=0; // needed
  fits_open_file(&fptr,infile,READONLY,&status) ;
  cout<<"FITS read open status= "<<status<<" "<<infile<<endl;

  fits_get_errstatus(status,err_text);
  cout<<err_text<<endl;

  fits_get_num_hdus(fptr,&total_hdus,&status);
  cout<<"total number of header units="<<total_hdus<<endl;

  hdunum=1;
  
  fits_movabs_hdu(fptr,hdunum+1,&hdutype,&status );
  cout<<"FITS movabs_hdu hdunum="<<hdunum
      <<" status= "<<status<<" hdutype="<<hdutype<<endl  ;

   fits_read_key(fptr,TLONG,"TFIELDS",&n_columns,comment,&status);
   cout<<" n_columns ="<<n_columns ;
   fits_read_key(fptr,TLONG,"NAXIS2",&n_rows  ,comment,&status);
   cout<<" n_rows    ="<<n_rows    <<endl;




    
   n_E=n_rows;
   E_lo  =new double[n_E];
   E_hi  =new double[n_E];
   E_in  =new double[n_E];
   matrix1=new double[n_E*n_E];
   matrix2=new double[n_E*n_E];
   matrix3=new double[n_E*n_E];
 
   fits_get_colnum(fptr,CASEINSEN,"ENERG_LO",&colnum,&status);
   cout<<"ENERG_LO column ="<< colnum<<endl;

   fits_read_col(fptr, TDOUBLE, colnum, 1,1,n_E,&nulval,E_lo, &anynul,&status);
    
   cout<<"E_lo:";for (i=0;i<n_E;i++)cout<<" "<<E_lo[i];cout<<endl;
   
  fits_get_colnum(fptr,CASEINSEN,"ENERG_HI",&colnum,&status);
  cout<<"ENERG_HI column ="<< colnum<<endl;

  fits_read_col(fptr, TDOUBLE, colnum, 1,1,n_E,&nulval,E_hi, &anynul,&status);
    
  cout<<"E_hi:";for (i=0;i<n_E;i++)cout<<" "<<E_hi[i];cout<<endl;

  // assume matrix input energies are average of output bin energies
  for (i=0;i<n_E;i++)E_in[i]=( E_lo[i] + E_hi[i] )/2.0;

  cout<<"E_in   :";for (i=0;i<n_E;i++)cout<<" "<<E_in[i];cout<<endl;

  fits_get_colnum(fptr,CASEINSEN,"MATRIX",&colnum,&status);
  cout<<"MATRIX   column ="<< colnum<<endl;
  fits_read_col(fptr, TDOUBLE, colnum, 1,1,n_E*n_E,&nulval,matrix1, &anynul,&status);
    
  cout<<"matrix1:"<<endl;
  cout.precision(3);
  for  (i=0;i<n_E   ;i++)
  {
    // cout<<i<<"  "<<E_lo[i]<<"-"<<E_hi[i]<<" keV: ";
   cout<<i<<"  "<<E_in[i]<<" keV: ";
   for (j=0;j<n_E   ;j++)cout<<" "<<matrix1[i*n_E+j];
   cout<<endl;
  }

  fits_close_file(fptr,&status) ;
  cout<<"FITS read close file status= "<<status<<" "<<infile<<endl;

  //////////////////////////////////////////////////////////
  
  strcpy(infile,directory);
  strcat(infile,"spi_rmf2_rsp_0001.fits");
  cout<<"   infile "<<infile<<endl;
  
  fits_open_file(&fptr,infile,READONLY,&status) ;
  cout<<"FITS read open status= "<<status<<" "<<infile<<endl;
    hdunum=1;
  
  fits_movabs_hdu(fptr,hdunum+1,&hdutype,&status );
  cout<<"FITS movabs_hdu hdunum="<<hdunum
      <<" status= "<<status<<" hdutype="<<hdutype<<endl  ;

  fits_get_colnum(fptr,CASEINSEN,"MATRIX",&colnum,&status);
  cout<<"MATRIX   column ="<< colnum<<endl;
  fits_read_col(fptr, TDOUBLE, colnum, 1,1,n_E*n_E,&nulval,matrix2, &anynul,&status);
    
  cout<<"matrix2:"<<endl;
  cout.precision(3);
  for  (i=0;i<n_E   ;i++)
  {
    //   cout<<i<<"  "<<E_lo[i]<<"-"<<E_hi[i]<<" keV: ";
   cout<<i<<"  "<<E_in[i]<<" keV: ";
   for (j=0;j<n_E   ;j++)cout<<" "<<matrix2[i*n_E+j];
   cout<<endl;
  }

  fits_close_file(fptr,&status) ;
  cout<<"FITS read close file status= "<<status<<" "<<infile<<endl;


  //////////////////////////////////////////////////////
  strcpy(infile,directory);
  strcat(infile,"spi_rmf3_rsp_0001.fits");
  cout<<"   infile "<<infile<<endl;
  
  fits_open_file(&fptr,infile,READONLY,&status) ;
  cout<<"FITS read open status= "<<status<<" "<<infile<<endl;
    hdunum=1;
  
  fits_movabs_hdu(fptr,hdunum+1,&hdutype,&status );
  cout<<"FITS movabs_hdu hdunum="<<hdunum
      <<" status= "<<status<<" hdutype="<<hdutype<<endl  ;

  fits_get_colnum(fptr,CASEINSEN,"MATRIX",&colnum,&status);
  cout<<"MATRIX   column ="<< colnum<<endl;
  fits_read_col(fptr, TDOUBLE, colnum, 1,1,n_E*n_E,&nulval,matrix3, &anynul,&status);
    
  cout<<"matrix3:<<endl";
  cout.precision(3);
  for  (i=0;i<n_E   ;i++)
  {
    //   cout<<i<<"  "<<E_lo[i]<<"-"<<E_hi[i]<<" keV: ";
   cout<<i<<"  "<<E_in[i]<<" keV: ";
   for (j=0;j<n_E   ;j++)cout<<" "<<matrix3[i*n_E+j];
   cout<<endl;
  }


  return 0;
}


