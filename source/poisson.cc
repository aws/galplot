// Poisson random number generator
// A. Strong 2 Nov  2005 from spidiffit Version 22

using namespace std;
#include<iostream>
#include<cmath>
#include<cstdlib>

unsigned int seed=1234; // file scope, value preserved
int init=1;    // file scope, value preserved
//////////////////////////////
int poisson(double  x)
{

double p=0.05;  // small probablility to generate Poisson distribution
               // smaller values give more accurate Poisson but use more time
int m=int(x/p);     // number of trials //AWS20051102
if(m<1)m=1;    // at least one trial
p=x/m;         // since m is rounded, recompute p

 if(init==1){srand(seed); init=0;}


int P=int(p*RAND_MAX); //AWS20051102
int N=0;
//cout<<m<<endl;
//cout<<p<<endl;
//cout<<P<<endl;
for(int i=0;i<m;i++)if(rand()<P)N++;

//cout.precision(20);
//cout<<"poisson x N"<<x<<" "<<N<<endl;

return N;

}

///////////////////////////////
int poisson(unsigned int initialize_seed)               //AWS20030402
{
  // use to re-initialize sequence
 seed=initialize_seed;
 srand(seed);
 return 0;
}
/////////////////////////////// test program
/*
int main(){

int nrept=100;
int nk=10;
double xx[]={0.01,0.1,1.,2.,3.,100.,1000,1e4,1e5,1e6};
for(int k=0;k<nk;k++){
double x=xx[k];
int N;
double   total=0.;
double totalsq=0.;
 
// for good generator mean square deviation should be close to x 

for(int irept=0;irept<nrept;irept++)
 {N=poisson(x);cout<<N<<" ";total+=N;totalsq+=pow(double(N),2);}
 double xbar=double(total)/nrept;
 double ms  =totalsq/nrept - xbar*xbar;

 cout<<endl<<"x="<<x<<" average= "<<xbar<<" mean square dev="<<ms<<endl;

 }
return 0;
}

*/
