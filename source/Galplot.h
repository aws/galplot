
#include "Configure.h"
#include "Galdef.h"
#include "Galplotdef.h"
#include "Spectrum.h"
#include "Distribution.h"
#include "Particle.h"
#include "Galaxy.h"
#include "CAT.h"
#include "SourcePopulation.h"

class Galplot
{

 public:

  Configure configure;
  Galdef galdef;
  Galplotdef galplotdef;
  Particle *gcr; // all species
  Galaxy    galaxy; 

  Data data;
  Convolved   convolved;
  UnConvolved unconvolved;

  SourcePopulation sourcepop1,sourcepop2,sourcepop3,sourcepop4,sourcepop5,sourcepop6,sourcepop7,sourcepop8;//AWS20081215 AWS20090107 AWS20140114

   FILE *  txtFILE; 
   ofstream txt_stream;       //AWS20051124
   char txt_stream_file[400]; //AWS20051129

   int n_species;

   int run(int argc, char*argv[]);

   int set_output();   //AWS20051124

   int read_HIR();
   int read_COR();


   int create_galaxy();
   int gen_isrf_energy_density();
   int read_gcr();
   int read_isrf();
   int read_isrf(  bool useNewMethod); //AWS20080611
   int read_isrf(int);                 //AWS20100409 new read_isrf routine

   int create_gcr(int mode); //AWS20130131  default=0 
   int read_EGRET_data();
   int read_EGRET_psf ();
   int read_COMPTEL_data();
   int read_GLAST_data(); //AWS20080507
   int read_GLAST_psf (); //AWS20080509

   int read_IC_skymap(char *IC_type);
   int read_bremss_skymap();
   int read_bremss_HIR_skymap();
   int read_bremss_H2R_skymap();
   int read_pi0_decay_skymap();
   int read_pi0_decay_HIR_skymap();
   int read_pi0_decay_H2R_skymap();
   int read_synchrotron_data();
   int read_synchrotron_skymap();
   int read_synchrotron_emiss();//AWS20100118

   int read_bremss_emiss()   ;//AWS20100118
   int read_pi0_decay_emiss();//AWS20100118
   int read_IC_emiss()       ;//AWS20100118


   int read_bremss_emiss   (char *galdef_ID);//AWS20100324
   int read_pi0_decay_emiss(char *galdef_ID);//AWS20100324
   int read_IC_emiss       (char *galdef_ID);//AWS20100324

   int read_gcr_source_functions(Particle &particle);// AWS20100312

   int read_solar_skymap();   //AWS20100210

   int write_convolved_EGRET(); //AWS20060905
   int gen_source_population();

   int plot_synchrotron_skymap();
   int plot_spectrum(int ic,int bremss,int pi0,int total);
   int plot_spectrum_healpix(int mode);                       //AWS20080609

   int plot_EGRET_profile(int longprof, int latprof,int ip,int mode);
   int plot_EGRET_spectrum(int mode);
   double isotropic_EGRET_from_list(double Egamma,double g);
   int plot_EGRET_skymaps(int mode);
   void deconvolve_intensity(TH1D *intensity);

   int plot_GLAST_spectrum(int mode);                                //AWS20080507
   int plot_GLAST_profile(int longprof, int latprof,int ip,int mode);//AWS20080509
   int plot_convolved_GLAST_profile                                  //AWS20080509
        (int longprof, int latprof,int ip,int ic,int bremss,int pi0,int total);


   int plot_GLAST_spectrum_healpix(int mode);                                 //AWS20080519
   int plot_GLAST_profile_healpix(int longprof, int latprof,int ip,int mode); //AWS20080519
   int plot_convolved_GLAST_profile_healpix                                   //AWS20080519
        (int longprof, int latprof,int ip,int ic,int bremss,int pi0,int total);
 
   int gen_GLAST_models();                                                    //AWS20080603

   int plot_COMPTEL_spectrum(int mode);
   int plot_convolved_EGRET_profile
        (int longprof, int latprof,int ip,int ic,int bremss,int pi0,int total);

   int plot_model_ridge  (int mode);
   int plot_SPI_spiskymax_profile(int longprof,int latprof,int mode);
   int plot_SPI_model_profile
      (int longprof, int latprof,double e_min,double e_max, int ic,int bremss,int total);
   int plot_COMPTEL_profile(int longprof,int latprof,int mode);                             //AWS20080201
   int plot_COMPTEL_model_profile                                                           //AWS20080201
      (int longprof, int latprof,double e_min,double e_max, int ic,int bremss,int total);   //AWS20080201
   int plot_gcr_spectra_ratios();
   int plot_gcr_spectra_ratios_data(int i_ratio);
   int plot_hunter_10_50GeV_spectrum();
   int plot_isotropic_EGRET_spectrum();
   int plot_isotropic_sreekumar_spectrum();
   int plot_gcr_spectra();
   int plot_gcr_spectra_data(int Z, int A, int index_mult,int Ek_or_momentum);  //AWS20130201
   int plot_gcr_spectra_data_legend();
   int plot_MILAGRO_spectrum(int mode);
   int plot_MILAGRO_profile(int longprof, int latprof,int ip,int mode); //AWS20080701
   int plot_OSSE_spectrum(int mode);
   int plot_RXTE_spectrum(int mode);
   int plot_GINGA_spectrum(int mode);
   int plot_IBIS_spectrum(int mode);
   int plot_Chandra_spectrum(int mode);
   int plot_SPI_spectrum(int mode);
   int plot_SPI_spectrum_spimodfit(int mode);
   int plot_SPI_spectrum_knoedlseder(int mode); //AWS20061002
   int plot_SPI_spectrum_bouchet    (int mode); //AWS20061004
   int plot_SPI_bouchet_profile(int longprof,int latprof,int mode);//AWS20080116
   int plot_IBIS_spectrum_krivonos  (int mode); //AWS20061005

   int plot_source_population_profile(int longprof, int latprof,int ip,int sublimit,int soplimit, int total,int sourcepop=1); //AWS20090107
   int plot_source_population_NS();

   int plot_synchrotron_profile       (int longprof, int latprof, int frequency);//AWS20070110
   int plot_synchrotron_profile_planck(int longprof, int latprof, int frequency);//AWS20120516
   int plot_synchrotron_clouds_profile(int longprof, int latprof, int frequency);//AWS20130701

   int plot_mw_isrf(); //AWS20080611
   int plot_luminosity();//AWS20100118
   int plot_luminosity_multiple_galdef();//AWS20100324

   int convolve(Distribution &map, Distribution psf);
   int convolve_EGRET();
   int convolve_EGRET_HI_H2();
   int convolve_GLAST();             //AWS20080514
   int convolve_GLAST_HI_H2();       //AWS20080514

   void   differential_spectrum(double e1,double e2, double intensity, double g,double &intensity_esq1,double &intensity_esq2);
   void   differential_spectrum(double *e,    int n, double *intensity,         double *intensity_esq1,double *intensity_esq2,
                             double *Ebar,double *Ibar);
   void   differential_spectrum(double e1,double e2, double intensity, double g,double &intensity_esq1,double &intensity_esq2,double &Ebar, double &Ibar);


   int fit_EGRET(int options);
   int fit_EGRET_Xco(int options); 
   int rebin_EGRET_data(int longbin, int latbin,double &long_min, double &lat_min, double &d_long, double &d_lat);


   int analyse_MCMC_SPI();
   int analyse_MCMC_SPI_spimodfit();
   int fit_spectrum_SPI();
   int exclude_source_(CAT cat,char  **par_id, int i_component);
   int convolve_SPI(double emin,double emax);
   int convolve_COMPTEL(double emin,double emax); //AWS20080201

   void gcr_modulate       (int i_species,double phi,int key);
   void gcr_modulate_ratios(int i_species,double phi,int key);

};
