


#include"Galplot.h"
#include"Isotropic.h" //AWS20191231

#include"galprop_classes.h"
#include"galprop.h"
//////////////////////////////////////////////////////////////////////////
int Galplot::run(int argc, char*argv[])

 {
   cout<<">>Galplot::run"<<endl;

 

  char *version=argv[1]; //IMOS20070416

  cout<<argc; //IMOS20070416
  for(int i=0;i<argc;i++) cout<<" "<<argv[i]; cout<<endl; //IMOS20070416
  if(argc<=2){cout<<"no galprop version and/or no galdef file specified !"<<endl;return -1;} //AWS20060423
 

   cout<<">>>>galprop version "<<version<<endl;
   cout<<">>>>galprop ID  "<<version<<"_"<<argv[2]<<endl;

 

  



   if(configure.init() !=0)return 1;
   cout<<configure.galdef_directory<<endl;// just a test



  if(galdef    .read  (version,argv[2],configure.galdef_directory) !=0) return 1;     //IMOS20070416
  if(galplotdef.read  (version,argv[2],configure.galdef_directory) !=0) return 1;     //IMOS20070416

  if(galplotdef.verbose==10000) {EBV_convert(0); exit(0);}
  if(galplotdef.verbose==10001) {EBV_convert(1); exit(0);} //AWS20100429
  if(galplotdef.verbose==10002) {EBV_convert(2); exit(0);} //AWS20100429

  if(galplotdef.verbose == -1600||galplotdef.verbose == -1601||galplotdef.verbose == -1602|| galplotdef.verbose == -1603) test_Skymap(galplotdef.verbose); //AWS20080516
  


   set_output();                                            //AWS20051124  


   if(create_galaxy() !=0) return 1;


   if(galplotdef.verbose==-4000 || galplotdef.verbose==-4001|| galplotdef.verbose==-4002 ) //AWS20200716 -4000 sourcepop no long cout printout  
   {
    cout<<"galplotdef.verbose="<<galplotdef.verbose;
    cout<<": Only gen_source_population"<<endl;
    gen_source_population(); 
    plot_source_population_NS();
    cout<<"Finished gen_source_population"<<endl;exit(0);
   }//AWS20110829 

   if(galplotdef.verbose==-5000 ) //AWS20191231
   {
    cout<<"read_isotropic at start"<<endl;
    Isotropic isotropic;
    isotropic.read(configure.fits_directory,galplotdef.isotropic_bgd_file);
    cout<<"Finished read_isotropic"<<endl;
    cout<<"continuing..."<<endl;
    //exit(0);
   }
   
   



   read_bremss_emiss     (); //AWS20100118 to test here only
   read_pi0_decay_emiss  (); //AWS20100118 to test here only
   read_IC_emiss         (); //AWS20100118 to test here only
   read_synchrotron_emiss(); //AWS20100118 to test here only
   read_gcr              (); //AWS20100202 needed by plot_luminosity

  if(galplotdef.luminosity==1&&galplotdef.ISRF_read==1)       //AWS20100823
  { 
   plot_luminosity       ();         //AWS20100118 
   plot_luminosity_multiple_galdef();//AWS20100324
  }

   extern void InitGui();
   VoidFuncPtr_t initFuncs[] = {InitGui, 0};
   TROOT root ("ISDC", "galplot", initFuncs);
   //int argc=0; char * argv[]={" "};
   TApplication theApp("galplot", &argc, argv);

   if(galplotdef.screen_output==0)gROOT->SetBatch(kTRUE); // no screen output




   //------------------------------------------------------------------



   if(galplotdef.gcr_spectra==1||galplotdef.gcr_spectra_n_ratio>=1)
   {
  
    if(  read_gcr   () !=0) return 1;

    if(galplotdef.gcr_spectra        ==1) plot_gcr_spectra       ();
    if(galplotdef.gcr_spectra_n_ratio>=1) plot_gcr_spectra_ratios();

   }


   if(galplotdef.gamma_spectrum   >=1||galplotdef.gamma_long_profile==1 
                                     ||galplotdef. gamma_lat_profile==1 )
   {


    if(galplotdef.data_GLAST>=1     )  //AWS20080508
    {

     read_GLAST_data(); 
       


     if(galdef.skymap_format==3)      //AWS20080609
      gen_GLAST_models();             //AWS20080603 
    
    }


   read_COMPTEL_data();//AWS20030910

   // AWS20110719 NB emissivities for luminosity read in above but need skymaps AWS20110722

                               read_IC_skymap(  "isotropic");
   if(galdef.IC_anisotropic>0) read_IC_skymap("anisotropic");//AWS20060511

   read_IC_emiss();        //AWS20100118

   read_bremss_skymap();
   read_bremss_emiss(); //AWS20100118

   if(galplotdef.fit_EGRET==2)
   read_bremss_HIR_skymap();    //AWS20041223
   if(galplotdef.fit_EGRET==2)
   read_bremss_H2R_skymap();    //AWS20041223

   read_pi0_decay_skymap();
   read_pi0_decay_emiss();      //AWS20100118

   if(galplotdef.fit_EGRET==2)
   read_pi0_decay_HIR_skymap(); //AWS20041223
   if(galplotdef.fit_EGRET==2)
   read_pi0_decay_H2R_skymap(); //AWS20041223
   


   if(galplotdef.sourcepop_total!=0)//AWS20191231  
   {
     gen_source_population();
     plot_source_population_NS();
   }






   
 

  if(galplotdef.luminosity==1&&galplotdef.ISRF_read==1)       //AWS20100823
  { 
         plot_mw_isrf(); //AWS20080611 AWS20090107 AWS20091223
  }


   if(galplotdef.gamma_spectrum>=1)plot_spectrum( 1,  1,  1,  1);

   if(galplotdef.gamma_long_profile==1 ||galplotdef.gamma_lat_profile==1 )
   {
     


    if(galplotdef.data_GLAST>=1)                                                 //AWS20080509
      for(int ip=0;ip<  data.GLAST_counts_healpix.getEMin().size()  ;ip++)       //AWS20080715
       {

	   {
	     
     if(galplotdef.gamma_long_profile== -1)
       plot_convolved_GLAST_profile(1,       0,      ip, 1,  1,   1,   1);
	     
     if(galplotdef.gamma_long_profile==1 && galdef.skymap_format==3 )                     //AWS20080609
       plot_convolved_GLAST_profile_healpix(1,       0,      ip, 1,  1,   1,   1);        //AWS20080604


     if(galplotdef.gamma_lat_profile== -1)
       plot_convolved_GLAST_profile(0,       1,      ip, 1,  1,   1,   1);

     if(galplotdef.gamma_lat_profile==1  && galdef.skymap_format==3 )                     //AWS20080609
       plot_convolved_GLAST_profile_healpix(0,       1,      ip, 1,  1,   1,   1);        //AWS20080604


	   }//if
       }//ip
   


   }// if profile

   }//if gamma



      /*
       test_energy_integral();
      */

   /* AWS20110718
  if(galplotdef.spiskymax_profile==1)  //AWS20080118
  {
   plot_SPI_spiskymax_profile(1,0,0);
   plot_SPI_spiskymax_profile(0,1,0);
   plot_SPI_spiskymax_profile(0,0,1);
  }

  if(galplotdef.spiskymax_profile==2)  //AWS20080118
  {
   plot_SPI_bouchet_profile(0,1,0);    //AWS20080118
  }

  if(galplotdef.spiskymax_profile==3)  //AWS20080201
  {
   plot_COMPTEL_profile(0,1,0);        //AWS20080201
  }
   */


  if(galdef.synchrotron >=1 ) //AWS20080304
   {

    read_synchrotron_skymap(); //AWS20050726
    read_synchrotron_emiss (); //AWS20100118
    read_synchrotron_data  (); //AWS20110622 moved from before read_synchrotron_skymap

    plot_synchrotron_skymap(); //AWS20050726

    plot_synchrotron_clouds_profile(1,0,0); //long profile AWS20130801
    plot_synchrotron_clouds_profile(0,1,0); //lat  profile AWS20130816

    plot_synchrotron_profile(1,0,0); //long profile AWS20070111
    plot_synchrotron_profile(0,1,0); //lat  profile AWS20070111

    plot_synchrotron_profile_planck(1,0,0); //long profile AWS20120516
    plot_synchrotron_profile_planck(0,1,0); //lat  profile AWS20120516

 


   }

   cout<<"xemacs "<<txt_stream_file<<endl;  //AWS20051129
   cout<<"completed processing of galdef_"<<version<<"_"<<argv[1]<<endl;
   cout<<"<<<<galplot"<<endl;
   cout << '\a';                         

   // following gave  problem when no gammas plotted since file not opened  AWS20060313
  
   if(galplotdef.gamma_spectrum   >=1)                                    //AWS20060313
   fclose(txtFILE);                                             

   txt_stream.close();

   cout<<"<<Galplot::run"<<endl;


   if(galplotdef.screen_output==1)   //to keep plots if on screen, otherwise terminate
      theApp.Run();

  return 0;
};
////////////////////////////////////////////////////////////////////////////
