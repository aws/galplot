

#include"Galplot.h"                   
#include"galprop_classes.h"
#include"galprop.h"

#include "fitsio.h" 
#include "Sun.h" // model class from gardian

double  interpolated(double energy_,valarray<double> energy, valarray<double>spectrum, int n_energy,int debug=0);

int Galplot::read_solar_skymap()
{
int stat;

 char   comment[100];

  cout<<" >>>> read_solar_skymap"<<endl;

stat=0;

 int read_write_Skymap=0; // controls conversion to healpix consistent with actual galprop energies

   fitsfile *fptr;       /* pointer to the FITS file; defined in fitsio.h */
    int status, ii, jj;
    long  fpixel = 1, naxis = 4, nelements, exposure;
    long naxes[5]  ; 

  
     naxis=3; 
     // values for solar_tot_sep2008_aug2009.fits
     naxes[0]=1440;
     naxes[1]= 720;
     naxes[2]=  30; // fv shows 39 but problem with >30
   

     nelements=naxes[0]*naxes[1]*naxes[2];
    
     
  
    
    float *array;          
    array=new float[nelements];

    char  infile[100];
  
    strcpy( infile,configure.fits_directory);
 
    strcat( infile,"solar_tot_sep2008_aug2009.fits"); // this has total 

   
   
    cout<<"  reading  solar skymap  from file "<<infile<<endl;

    status = 0;         /* initialize status before calling fitsio routines */
    fits_open_file(&fptr,  infile,READONLY, &status);   
    cout<<"  fits open status = "<<status<<endl;



    /* Read the array of floats  */
    float nulval=0;
    int anynul;
    fits_read_img(fptr, TFLOAT, fpixel, nelements, &nulval,array, &anynul,&status);
 
  
    Distribution solar_skymap_in;
    solar_skymap_in.init(naxes[0],naxes[1],naxes[2]);


  

   int i=0; 
   for (int ip       =0;        ip<naxes[2];       ip++)
   for (int ib       =0;        ib<naxes[1];       ib++)
   for (int il       =0;        il<naxes[0];       il++)
         {             
	   //       cout<<array[i]<<endl;
	            solar_skymap_in.d2[il][ib].s[ip]=array[i];
                       
             i++;
         }

    double factor = 2./(365.*24.); // number of 2-hour snapshots summed over a year to make the skymap

    solar_skymap_in *= factor;

    //  solar_skymap_in .print();

    double crval3=2.0;       // from FITS header of solar_tot_sep2008_aug2009.fits
    double cdelt3=0.0791813; // from FITS header of solar_tot_sep2008_aug2009.fits

    valarray<double> E_solar(naxes[2]);

    for (int ip       =0;        ip<naxes[2];       ip++) E_solar[ip]=pow(10, crval3+ cdelt3*ip);

    // checks 
   for (int ip       =0;        ip<naxes[2];       ip++)
     {
   double average_intensity=0;
   int npix=0;
   for (int ib       =0;        ib<naxes[1];       ib++)
   for (int il       =0;        il<naxes[0];       il++)
         {             
	   if(solar_skymap_in.d2[il][ib].s[ip]>0 )
	     {
	   average_intensity+=         solar_skymap_in.d2[il][ib].s[ip];
                       
           npix++;
	     }
         }

   average_intensity/= npix; 
   cout<<"read_solar_skymap: energy="<<E_solar[ip]<<"  average intensity="<<average_intensity<<endl;
     }

   // interpolate to galprop grid

    Distribution solar_skymap;
    //    solar_skymap.init(galaxy.n_long,galaxy.n_lat,galaxy.n_E_gammagrid); need l,b interpolation for this
    solar_skymap.init(naxes[0],naxes[1],galaxy.n_E_gammagrid);
  
   valarray<double> spectrum(naxes[2]);

 
   int interpolated_debug=0;
   
   for (int ib       =0;        ib<naxes[1];       ib++)
   for (int il       =0;        il<naxes[0];       il++)
   {             
   {
    for (int ip       =0;        ip<naxes[2];       ip++)
    spectrum[ip]=     solar_skymap_in.d2[il][ib].s[ip];

    for (int ip       =0;        ip<galaxy.n_E_gammagrid;       ip++)
      {
	double value=interpolated(galaxy.E_gamma[ip],E_solar,spectrum,naxes[2],interpolated_debug);
        solar_skymap.d2[il][ib].s[ip] = value;
	//	if(value>0.)cout<<"E_gamma="<<galaxy.E_gamma[ip]<<" interpolated value="<<value<<endl;
      }
         
	     }
         }
     
   //   solar_skymap.print();





    fits_close_file(fptr, &status);            /* close the file */

    fits_report_error(stderr, status);  /* print out any error messages */


    delete[] array; //AWS20010216

    /////////////////////// fix the file since it was corrupt


    FITS solar_skymap_fits(naxes[0],naxes[1],naxes[2]);
   for (int ip       =0;        ip<naxes[2];       ip++)
   for (int ib       =0;        ib<naxes[1];       ib++)
   for (int il       =0;        il<naxes[0];       il++)
         {             
	 
	   solar_skymap_fits(il,ib,ip)=        solar_skymap_in.d2[il][ib].s[ip];
                       
          
         }

   solar_skymap_fits.CRVAL[0]=  0.25;
   solar_skymap_fits.CRVAL[1]=-89.75;
   solar_skymap_fits.CRVAL[2]= crval3; // defined above
   solar_skymap_fits.CDELT[0]=0.25;
   solar_skymap_fits.CDELT[1]=0.25;
   solar_skymap_fits.CDELT[2]= cdelt3; // defined above
   solar_skymap_fits.CRPIX[0]= 1;
   solar_skymap_fits.CRPIX[1]= 1;
   solar_skymap_fits.CRPIX[2]= 1;

   char  outfile[100];
   strcpy( outfile,configure.fits_directory);
   strcat( outfile,"solar_tot_sep2008_aug2009_fix.fits"); // this has total 
   solar_skymap_fits.write(outfile);
  
   // output interpolated maps for conversion via Skymap class

      FITS solar_skymap_interp_fits(naxes[0],naxes[1],galaxy.n_E_gammagrid);

   for (int ip       =0;        ip<galaxy.n_E_gammagrid;       ip++)
   for (int ib       =0;        ib<naxes[1];                   ib++)
   for (int il       =0;        il<naxes[0];                   il++)
         {             
	 
	   solar_skymap_interp_fits(il,ib,ip)=        solar_skymap.d2[il][ib].s[ip];
                       
          
         }


   // only the energies are interpolated, the (l,b) grid is unchanged here since healpix will take care of that
   solar_skymap_interp_fits.CRVAL[0]=  0.25;
   solar_skymap_interp_fits.CRVAL[1]=-89.75;
   solar_skymap_interp_fits.CRVAL[2]= log10(galaxy.E_gamma_min);
   solar_skymap_interp_fits.CDELT[0]=  0.25;
   solar_skymap_interp_fits.CDELT[1]=  0.25;
   solar_skymap_interp_fits.CDELT[2]=  log10(galaxy.E_gamma_factor);
   solar_skymap_interp_fits.CRPIX[0]= 1;
   solar_skymap_interp_fits.CRPIX[1]= 1;
   solar_skymap_interp_fits.CRPIX[2]= 1;

   
   strcpy( outfile,configure.fits_directory);
   strcat( outfile,"solar_tot_sep2008_aug2009_interp.fits"); // this has total 
   solar_skymap_interp_fits.write(outfile);
   

    // healpix
    string mapname;
   int order=data.GLAST_counts_healpix.getCountsMap().Order(); // from healpix_base

   if( read_write_Skymap==1)
     {
       //    Skymap<double> solar_hp_skymap;

    mapname=outfile;
 
    // Skymap load converts to healpix
    cout<<"read_solar_skymap: reading solar IC Skymap from "<<mapname<<endl;
    //       solar_hp_skymap.load(mapname);



   unconvolved.GLAST_unconvolved_counts_solar_IC.load(mapname,order);

 

   //   cout<<"rebinning solar IC skymap with healpix order = "<<order<<endl;
   //    unconvolved.GLAST_unconvolved_counts_solar_IC.rebin(order);

    strcpy( outfile,configure.fits_directory);
    strcat( outfile,"solar_tot_sep2008_aug2009_healpix.fits");
    mapname=outfile;
    cout<<"read_solar_skymap: writing healpix IC Skymap to "<<mapname<<endl;
    //        solar_hp_skymap.write(mapname);
       unconvolved.GLAST_unconvolved_counts_solar_IC.write(mapname);
     }

    strcpy(  infile,configure.fits_directory);
    strcat(  infile,"solar_tot_sep2008_aug2009_healpix.fits");
    mapname= infile;
    cout<<"read_solar_skymap: reading healpix IC Skymap from "<<mapname<<endl;
 
    unconvolved.GLAST_unconvolved_intensity_solar_IC.load (mapname,order);
      convolved.GLAST_convolved_intensity_solar_IC  .load (mapname,order); // convolution not yet done !

	 cout<<"read_solar_skymap: input IC Skymap healpix order="<<unconvolved.GLAST_unconvolved_intensity_solar_IC.Order()<<endl;
    


	 // disk map is not yet available so set equal to IC to satisfy plot routines
    unconvolved.GLAST_unconvolved_intensity_solar_disk.load (mapname,order);
      convolved.  GLAST_convolved_intensity_solar_disk.load (mapname,order); // convolution not yet done !

 
      /////////////////////////////////////////////////////////////////////////////

      // use gardian model Sun
	cout<<"read_solar_skymap: using gardian model Sun"<<endl;
    Parameters pars;

 // filter Skymap is needed by BaseModel now   AWS20091204
     Skymap<char> filterMap(data.GLAST_counts_healpix.getCountsMap().Order(), data.GLAST_counts_healpix.getEMin(), data.GLAST_counts_healpix.getEMax());
                  filterMap = char(1);

                                   // NB avoid name clash with galplot variable configure
  unsigned int configuremodel = 3; //AWS20091211 for new gardian    1=EXPOSURE_CORRECT, 2=CONVOLVE, 3=both (default for this optional parameter)
                                  configuremodel = 3;
  if(galplotdef.convolve_GLAST==0)configuremodel = 1; //AWS20100219 

 // parameters (read in from parameter file by gardian) need to be supplied

  string parameter, value;
  
  parameter="galpropFitsDirectory";  value=configure.fits_directory; pars.setParameter(parameter,value);  pars.getParameter(parameter,value    );  cout<< parameter <<"="<<value<<endl;
  mapname=string(configure.fits_directory)+"solar_tot_sep2008_aug2009_healpix.fits"; // Sun class uses full path not galpropFitsDirectory

  parameter="Sun_disk_ModelMapName";  value=mapname; pars.setParameter(parameter,value);  pars.getParameter(parameter,value    );  cout<< parameter <<"="<<value<<endl;
  parameter="Sun_IC_ModelMapName";    value=mapname; pars.setParameter(parameter,value);  pars.getParameter(parameter,value    );  cout<< parameter <<"="<<value<<endl;

  // needs 2 values - start and delta since context is gardian fitting
  parameter="Sun_disk_MapScale";      value="1.0 1.0"; pars.setParameter(parameter,value);  pars.getParameter(parameter,value    );  cout<< parameter <<"="<<value<<endl;
  parameter="Sun_IC_MapScale"  ;      value="1.0 1.0"; pars.setParameter(parameter,value);  pars.getParameter(parameter,value    );  cout<< parameter <<"="<<value<<endl;

  Variables variables;
  variables.add(string("Sun_disk_MapScale"), 0.0, 0.0) ;
  variables.add(string("Sun_IC_MapScale")  , 0.0, 0.0) ;


  Sources sources_input; // not used, just for interface

  Sun sun (  data.GLAST_exposure_healpix,  data.GLAST_counts_healpix,  data.GLAST_psf_healpix, sources_input,pars, filterMap,configuremodel);
  
  cout<<" read_solar_skymap: generating counts map "<<endl;
  // does not automatically create the output skymap, have to create it first

  Skymap <double> countsmap(data.GLAST_counts_healpix.getCountsMap().Order(), data.GLAST_counts_healpix.getEMin(), data.GLAST_counts_healpix.getEMax());
  cout<<" read_solar_skymap: generating counts maps "<<endl;


  variables["Sun_disk_MapScale"]= 1.0 ;
  variables["Sun_IC_MapScale"  ]= 0.0 ;
 
  sun.getMap(variables, countsmap);
  
  cout<<" read_solar_skymap: generated disk counts map Npix="<<countsmap.Npix()<<endl;
  
  unconvolved.GLAST_unconvolved_counts_solar_disk=countsmap;
    convolved.GLAST_convolved_counts_solar_disk  =countsmap;// for fast testing


  variables["Sun_disk_MapScale"]= 0.0 ;
  variables["Sun_IC_MapScale"  ]= 1.0 ;
 
  sun.getMap(variables, countsmap);
  
  cout<<" read_solar_skymap: generated IC counts map Npix="<<countsmap.Npix()<<endl;
  
  unconvolved.GLAST_unconvolved_counts_solar_IC=countsmap;
    convolved.GLAST_convolved_counts_solar_IC  =countsmap;// for fast testing



  
  mapname="GLAST_convolved_counts_solar_IC.fits";
  cout<<" writing counts map to "<<mapname<<endl;
  convolved.GLAST_convolved_counts_solar_IC.write(mapname);

  mapname="GLAST_convolved_counts_solar_disk.fits";
  cout<<" writing counts map to "<<mapname<<endl;
  convolved.GLAST_convolved_counts_solar_disk.write(mapname);

   /////////////////////////////////////////////////////


  cout<<" <<<< read_solar_skymap"<<endl;

    return( status );
return stat;
}
////////////////////////////////////////////////////////////////



double  interpolated(double energy_,valarray<double>energy, valarray<double> spectrum, int n_energy,int debug)
{
  double interpolated_;

 double A;
 int i;
 int found=0;



  for (i=0;i<n_energy-1;i++)
  {
    if(energy_ >= energy[i] && energy_ <= energy[i+1])
      {

          if(debug==2)
          cout   <<  i<<" "  <<energy_<<" "<<energy  [i]<<" "<<  energy[i+1]
                                      <<" "<<spectrum[i]<<" "<<spectrum[i+1]<<endl;
          found=1;
          break;
      }
  }

  // logarithmic interpolation

                interpolated_= 0.;
  if (found==0) interpolated_= 0.;

  if (found==1)
  {

   if(spectrum[i]>0.&&spectrum[i+1]>0.)
   {
    interpolated_ = log(spectrum[i])
                  + (log(energy_)-log(energy[i]))/( log(energy  [i+1])-  log(energy  [i]) )
                                                 *( log(spectrum[i+1]) - log(spectrum[i]) )  ;

    interpolated_ = exp(interpolated_);
   }
  }

  if(debug==1)
    cout<< "Isotropic::interpolated"  <<  i<<" "  <<energy_<<" "<<energy  [i]<<" "<<  energy[i+1]
                                               <<" "<<spectrum[i]<<" "<<spectrum[i+1]
         <<" interpolated isotropic spectrum ="<<interpolated_<<endl;


  return interpolated_;
}

