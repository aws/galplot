
 
#include"Galplot.h"                    //AWS20050920
#include"galprop_classes.h"
#include"galprop.h"
#include"constants.h"
#include"fitsio.h"


static void   differential_spectrum(double e1,double e2, double intensity, double g,double &intensity_esq1,double &intensity_esq2,double &Ebar, double &Ibar);
 

int Galplot::plot_isotropic_sreekumar_spectrum()
{

  // Sreekumar et al. 1998 ApJ 494, 523. Table 1 and Fig 4.

   cout<<" >>>> plot_isotropic_sreekumar_spectrum    "<<endl;
   int status=0;


 TGraph *spectrum;
 double A=2.74e-3;
 double g=2.1;
 double e1,e2;


   
   spectrum=new TGraph(2);
   e1=    30.;
   e2=120000.;//AWS20031027
   spectrum->SetPoint(0,e1,A*pow(e1,2.0-g));
   spectrum->SetPoint(1,e2,A*pow(e2,2.0-g));

  spectrum->SetMarkerColor(kCyan);
  spectrum->SetMarkerStyle(21); 
  spectrum->SetMarkerStyle(22); // triangles
  spectrum->SetMarkerSize (0.5);// 
  spectrum->SetLineColor(kCyan);
  spectrum->SetLineWidth(5     );
  spectrum->SetLineStyle(1      );//1=solid 2=dash 3=dash-dot 4=dot-dot
  spectrum->SetMarkerStyle(22); // triangles
  spectrum->SetMarkerSize (0.5);// 
  spectrum->Draw();
  spectrum->Draw(" P ");  // points as markers  
  spectrum->Draw();




  // energy ranges


int i_E_EGRET;
double  *dintensity,*dintensity_errorbar,*intensity,*intensity_esq1,*intensity_esq2;

double *Ebar,*Ibar;
double segment[2],Ebar_segment[2];
int errorbar; double errorbarfactor;

  dintensity         =new double[data.n_E_EGRET];
  dintensity_errorbar=new double[data.n_E_EGRET];
  intensity          =new double[data.n_E_EGRET];
  intensity_esq1    = new double[data.n_E_EGRET];
  intensity_esq2    = new double[data.n_E_EGRET];

  // cm^-2 sr^-1 s^-1 MeV^-1 from Sreekumar Table 1

  dintensity[ 0]= 1.20e-6;  dintensity_errorbar[ 0]= 0.35e-6;  
  dintensity[ 1]= 6.63e-7;  dintensity_errorbar[ 1]= 1.29e-7; 
  dintensity[ 2]= 2.61e-7;  dintensity_errorbar[ 2]= 0.35e-7; 
  dintensity[ 3]= 1.10e-7;  dintensity_errorbar[ 3]= 0.15e-7; 
  dintensity[ 4]= 3.60e-8;  dintensity_errorbar[ 4]= 0.48e-8; 
  dintensity[ 5]= 9.85e-9;  dintensity_errorbar[ 5]= 1.34e-9; 
  dintensity[ 6]= 2.72e-9;  dintensity_errorbar[ 6]= 0.37e-9; 
  dintensity[ 7]= 6.17e-10; dintensity_errorbar[ 7]= 0.84e-10; 
  dintensity[ 8]= 1.52e-10; dintensity_errorbar[ 8]= 0.22e-10; 
  dintensity[ 9]= 3.26e-11; dintensity_errorbar[ 9]= 0.48e-11; 

// 10-20, 20-50,50-120 GeV  from Sreekumar Fig 4               AWS20031027
  if (data.n_E_EGRET>10)
  {dintensity[10]=  5.3e-12; dintensity_errorbar[10]=  1.6e-12; } 
  if (data.n_E_EGRET>11)
  {dintensity[11]=  9.2e-13; dintensity_errorbar[11]=  3.2e-13; }
  if (data.n_E_EGRET>12)
  {dintensity[12]=  2.1e-13; dintensity_errorbar[12]=  0.9e-13; }


  Ebar=new double[data.n_E_EGRET];
  Ibar=new double[data.n_E_EGRET];

         for(i_E_EGRET=0; i_E_EGRET<data.n_E_EGRET                    ; i_E_EGRET++)
        {
          // convert to integrated intensity
	  intensity[i_E_EGRET]=dintensity[i_E_EGRET]*(data.E_EGRET[i_E_EGRET+1]-data.E_EGRET[i_E_EGRET]) ;
          
          differential_spectrum(data.E_EGRET[i_E_EGRET],data.E_EGRET[i_E_EGRET+1] ,
                                intensity[i_E_EGRET], g,
                                intensity_esq1[i_E_EGRET],intensity_esq2[i_E_EGRET],Ebar[i_E_EGRET] ,Ibar[i_E_EGRET] );

         
          cout<<"plot_isotropic_sreekumar_spectrum: e1,e2="<<data.E_EGRET[i_E_EGRET]<<" "<<data.E_EGRET[i_E_EGRET+1]
              <<" intensity="<<intensity[i_E_EGRET]<<" intensity_esq1, 2 ="<<intensity_esq1[i_E_EGRET]<<" "<<intensity_esq2[i_E_EGRET]<<endl;


        }//i_E_EGRET




for(i_E_EGRET=0; i_E_EGRET<data.n_E_EGRET                    ; i_E_EGRET++)
{

 if(galplotdef.spectrum_style_EGRET==1 ||  galplotdef.spectrum_style_EGRET==3)
 {
  for (errorbar=1;errorbar<=2; errorbar++)
    {

         
          if(errorbar==1)errorbarfactor=  1.0+dintensity_errorbar[i_E_EGRET]/dintensity[i_E_EGRET] ;
          if(errorbar==2)errorbarfactor=  1.0-dintensity_errorbar[i_E_EGRET]/dintensity[i_E_EGRET];

          segment[0]=intensity_esq1[i_E_EGRET]*errorbarfactor;
          segment[1]=intensity_esq2[i_E_EGRET]*errorbarfactor;

          

          spectrum=new TGraph(2,&data.E_EGRET[i_E_EGRET],segment);

          spectrum->SetMarkerColor(kCyan     );
          spectrum->SetMarkerStyle(21); 
          spectrum->SetMarkerStyle(22); // triangles
          spectrum->SetMarkerSize (0.5);// 
          spectrum->SetLineColor(kCyan    );
          spectrum->SetLineWidth(3     );
          spectrum->SetLineStyle(1      );
          spectrum->SetMarkerStyle(22); // triangles
          spectrum->SetMarkerSize (0.5);// 
          spectrum->Draw();       // don't use "same" for TGraph
          spectrum->Draw(" P ");  // points as markers  

          spectrum->Draw();
     }//errorbar
    }//if


if(galplotdef.spectrum_style_EGRET==2 ||  galplotdef.spectrum_style_EGRET==3)
 {
         segment[0]=Ibar[i_E_EGRET]*(1.0+dintensity_errorbar[i_E_EGRET]/dintensity[i_E_EGRET]  );
         segment[1]=Ibar[i_E_EGRET]*(1.0-dintensity_errorbar[i_E_EGRET]/dintensity[i_E_EGRET]  );
    Ebar_segment[0]=Ebar[i_E_EGRET];
    Ebar_segment[1]=Ebar[i_E_EGRET];
    spectrum=new TGraph(2,Ebar_segment,segment);

          spectrum->SetMarkerColor(kCyan     );
          spectrum->SetMarkerStyle(21); 
          spectrum->SetMarkerStyle(22); // triangles
          spectrum->SetMarkerSize (0.5);// 
          spectrum->SetLineColor(kCyan    );
          spectrum->SetLineWidth(3     );
          spectrum->SetLineStyle(1      );
          spectrum->SetMarkerStyle(22); // triangles
          spectrum->SetMarkerSize (0.5);// 
          spectrum->Draw();       // don't use "same" for TGraph
          spectrum->Draw(" P ");  // points as markers  

          spectrum->Draw();

  }//if

 }//i_EGRET



   cout<<" <<<< plot_isotropic_sreekumar_spectrum   "<<endl;
   return status;
}

 
//------------------------------------------------------------------------------------------

static void differential_spectrum
(double e1,double e2,double intensity, double g,double &intensity_esq1,double &intensity_esq2,double &Ebar, double &Ibar)
{
  // converts integral intensity between e1 and e2 to two differential values at e1, e2
  // for a given spectral index g
  // result in form E^2 I(E)
  double A;
  
  A=(-g+1.)/(pow(e2,-g+1.)-pow(e1,-g+1)) * intensity;

  intensity_esq1=A*pow(e1,-g+2);
  intensity_esq2=A*pow(e2,-g+2);


  Ebar  =pow(e1*e2,0.5);
  Ibar=A*pow(Ebar,-g+2.0);

 cout<<"A="<<A<<endl;
} 
