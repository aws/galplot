
//**.****|****.****|****.****|****.****|****.****|****.****|****.****|****.****|
// * Galplotdef.cc *                                   galprop package * 4/14/2000 
//**"****!****"****!****"****!****"****!****"****!****"****!****"****!****"****|
using namespace std;//AWS20050919
#include<iostream>  //AWS20050919
#include<string>    //AWS20050919
#include <cstring>  //AWS20111010 for sles11 gcc 4.3.4 
#include <cstdlib>  //AWS20111010 for sles11 gcc 4.3.4 for exit

#include"Galplotdef.h"

//**.****|****.****|****.****|****.****|****.****|****.****|****.****|****.****|

int Galplotdef::read(char *version_, char *run_no_, char *galdef_directory)
{
   int stat=0;
   char workstring[400];
  
   cout<<" >>>>galplotdef read"<<endl;

   strcpy(version,version_);
   strcpy(run_no,run_no_);

 
   strcpy(galdef_ID,version);
   strcat(galdef_ID,"_");
   strcat(galdef_ID,run_no);

   char galdef_file[100];
   strcpy(galdef_file,galdef_directory);
   strcat(galdef_file,"galplotdef_");
   strcat(galdef_file,galdef_ID);
   cout<<galdef_file<<endl;


   char  filename[100];
   strcpy(filename,galdef_file);

   FILE *ft;
   ft=fopen(filename,"r");
   if(ft==NULL)
   {
      cout<<"no galplotdef file called "<<filename<<endl; return -1;
   }
   fclose(ft);

   char parstring[100];
  

   /*-------------- not needed, uses Galdef.h: remove in future
   strcpy(parstring,                              "n_spatial_dimensions"  );
   stat= read_galdef_parameter(filename,parstring,&n_spatial_dimensions   );

   if(stat!=0)
   {
      cout<<"no spatial dimensions specified in "<<filename<<endl; return -1;
   }

//radial grid
   strcpy(parstring,                              "r_min"   );
   stat= read_galdef_parameter(filename,parstring,&r_min    );
   strcpy(parstring,                              "r_max"   );
   stat= read_galdef_parameter(filename,parstring,&r_max    );
   strcpy(parstring,                              "dr"      );
   stat= read_galdef_parameter(filename,parstring,&dr       );

//z-grid
   strcpy(parstring,                              "z_min"   );
   stat= read_galdef_parameter(filename,parstring,&z_min    );
   strcpy(parstring,                              "z_max"   );
   stat= read_galdef_parameter(filename,parstring,&z_max    );
   strcpy(parstring,                              "dz"      );
   stat= read_galdef_parameter(filename,parstring,&dz       );

//x-grid
   strcpy(parstring,                              "x_min"   );
   stat= read_galdef_parameter(filename,parstring,&x_min    );
   strcpy(parstring,                              "x_max"   );
   stat= read_galdef_parameter(filename,parstring,&x_max    );
   strcpy(parstring,                              "dx"      );
   stat= read_galdef_parameter(filename,parstring,&dx       );

//y-grid
   strcpy(parstring,                              "y_min"   );
   stat= read_galdef_parameter(filename,parstring,&y_min    );
   strcpy(parstring,                              "y_max"   );
   stat= read_galdef_parameter(filename,parstring,&y_max    );
   strcpy(parstring,                              "dy"      );
   stat= read_galdef_parameter(filename,parstring,&dy       );

//momentum grid
   strcpy(parstring,                              "p_min"   );
   stat= read_galdef_parameter(filename,parstring,&p_min    );
   strcpy(parstring,                              "p_max"   );
   stat= read_galdef_parameter(filename,parstring,&p_max    );
   strcpy(parstring,                              "p_factor");
   stat= read_galdef_parameter(filename,parstring,&p_factor );

//kinetic energy grid
   strcpy(parstring,                              "Ekin_min"   );
   stat= read_galdef_parameter(filename,parstring,&Ekin_min    );
   strcpy(parstring,                              "Ekin_max"   );
   stat= read_galdef_parameter(filename,parstring,&Ekin_max    );
   strcpy(parstring,                              "Ekin_factor");
   stat= read_galdef_parameter(filename,parstring,&Ekin_factor );

//p||Ekin option
   strcpy(parstring,                              "p_Ekin_grid");
   stat= read_galdef_parameter(filename,parstring, p_Ekin_grid );

    end of not needed ------------------------- */

//gamma-ray energy grid
   strcpy(parstring,                              "E_gamma_min"   );
   stat= read_galdef_parameter(filename,parstring,&E_gamma_min    );
   strcpy(parstring,                              "E_gamma_max"   );
   stat= read_galdef_parameter(filename,parstring,&E_gamma_max    );
   strcpy(parstring,                              "E_gamma_factor");
   stat= read_galdef_parameter(filename,parstring,&E_gamma_factor );

//synchrotron grid
   strcpy(parstring,                              "nu_synch_min"   );
   stat= read_galdef_parameter(filename,parstring,&nu_synch_min    );
   strcpy(parstring,                              "nu_synch_max"   );
   stat= read_galdef_parameter(filename,parstring,&nu_synch_max    );
   strcpy(parstring,                              "nu_synch_factor");
   stat= read_galdef_parameter(filename,parstring,&nu_synch_factor );

//longitude-latitude grid
   strcpy(parstring,                              "long_min"       );
   stat= read_galdef_parameter(filename,parstring,&long_min        );
   strcpy(parstring,                              "long_max"       );
   stat= read_galdef_parameter(filename,parstring,&long_max        );
   strcpy(parstring,                              "lat_min"        );
   stat= read_galdef_parameter(filename,parstring,&lat_min         );
   strcpy(parstring,                              "lat_max"        );
   stat= read_galdef_parameter(filename,parstring,&lat_max         );

   strcpy(parstring,                              "d_long"        );
   stat= read_galdef_parameter(filename,parstring,&d_long         );
   strcpy(parstring,                              "d_lat"         );
   stat= read_galdef_parameter(filename,parstring,&d_lat          );

  
   strcpy(parstring,                              "verbose"       );
   stat= read_galdef_parameter(filename,parstring,&verbose        );
   strcpy(parstring,                              "test_suite"       );
   stat= read_galdef_parameter(filename,parstring,&test_suite        );

   psfile_tag=new char[200];                                //AWS20060123
   strcpy(parstring,                              "psfile_tag"          );
   stat= read_galdef_parameter(filename,parstring, psfile_tag           );

   strcpy(parstring,                              "screen_output"       );
   stat= read_galdef_parameter(filename,parstring,&screen_output        );

   strcpy(parstring,                              "output_format"       );
   stat= read_galdef_parameter(filename,parstring,&output_format        );

   // galplot specific parameters
   strcpy(parstring,                              "gamma_spectrum"   );
   stat= read_galdef_parameter(filename,parstring,&gamma_spectrum    );
   strcpy(parstring,                              "gamma_long_profile");
   stat= read_galdef_parameter(filename,parstring,&gamma_long_profile);
   strcpy(parstring,                              "gamma_lat_profile");
   stat= read_galdef_parameter(filename,parstring,&gamma_lat_profile );

   strcpy(parstring,                              "long_min1"        );
   stat= read_galdef_parameter(filename,parstring,&long_min1         );
   strcpy(parstring,                              "long_max1"        );
   stat= read_galdef_parameter(filename,parstring,&long_max1         );
   strcpy(parstring,                              "long_min2"        );
   stat= read_galdef_parameter(filename,parstring,&long_min2         );
   strcpy(parstring,                              "long_max2"        );
   stat= read_galdef_parameter(filename,parstring,&long_max2         );
   strcpy(parstring,                              "long_binsize"     );
   stat= read_galdef_parameter(filename,parstring,&long_binsize      );

   long_log_scale=0;                                                     //AWS20050722
   strcpy(parstring,                              "long_log_scale"   );  //AWS20050722
   stat= read_galdef_parameter(filename,parstring,&long_log_scale    );  //AWS20050722

   strcpy(parstring,                              "lat_min1"        );
   stat= read_galdef_parameter(filename,parstring,&lat_min1         );
   strcpy(parstring,                              "lat_max1"        );
   stat= read_galdef_parameter(filename,parstring,&lat_max1         );
   strcpy(parstring,                              "lat_min2"        );
   stat= read_galdef_parameter(filename,parstring,&lat_min2         );
   strcpy(parstring,                              "lat_max2"        );
   stat= read_galdef_parameter(filename,parstring,&lat_max2         );
   strcpy(parstring,                              "lat_binsize"     );
   stat= read_galdef_parameter(filename,parstring,&lat_binsize      );

   strcpy(parstring,                              "lat_log_scale"   );
   stat= read_galdef_parameter(filename,parstring,&lat_log_scale    );




   strcpy(parstring,                              "gamma_spectrum_Emin"        );
   stat= read_galdef_parameter(filename,parstring,&gamma_spectrum_Emin         );
   strcpy(parstring,                              "gamma_spectrum_Emax"        );
   stat= read_galdef_parameter(filename,parstring,&gamma_spectrum_Emax         );
   strcpy(parstring,                              "gamma_spectrum_Imin"        );
   stat= read_galdef_parameter(filename,parstring,&gamma_spectrum_Imin         );
   strcpy(parstring,                              "gamma_spectrum_Imax"        );
   stat= read_galdef_parameter(filename,parstring,&gamma_spectrum_Imax         );

   gamma_IC_selectcomp=0;                          // default                   ;//AWS20060310
   strcpy(parstring,                              "gamma_IC_selectcomp"        );//AWS20060310
   stat= read_galdef_parameter(filename,parstring,&gamma_IC_selectcomp         );//AWS20060310


   strcpy(parstring,                              "sources_EGRET"        );
   stat= read_galdef_parameter(filename,parstring,&sources_EGRET         );

   strcpy(parstring,                              "energies_EGRET"        ); //AWS20031021
   stat= read_galdef_parameter(filename,parstring,&energies_EGRET         ); //AWS20031021


   strcpy(parstring,                              "convolve_EGRET"        );
   stat= read_galdef_parameter(filename,parstring,&convolve_EGRET         );

   strcpy(parstring,                              "convolve_EGRET_iEmax"  ); //AWS20031017
   stat= read_galdef_parameter(filename,parstring,&convolve_EGRET_iEmax   ); //AWS20031017

   strcpy(parstring,                              "convolve_EGRET_thmax"  );
   stat= read_galdef_parameter(filename,parstring,&convolve_EGRET_thmax   );

   strcpy(parstring,                              "error_bar_EGRET"        );
   stat= read_galdef_parameter(filename,parstring,&error_bar_EGRET         );

   strcpy(parstring,                              "spectrum_style_EGRET"        );
   stat= read_galdef_parameter(filename,parstring,&spectrum_style_EGRET         );

   spectrum_index_EGRET=0.0;                                                      //AWS20050916
   strcpy(parstring,                              "spectrum_index_EGRET"        );//AWS20050916
   stat= read_galdef_parameter(filename,parstring,&spectrum_index_EGRET         );//AWS20050916


   strcpy(parstring,                              "spectrum_50GeV_EGRET"        );
   stat= read_galdef_parameter(filename,parstring,&spectrum_50GeV_EGRET         );

   strcpy(parstring,                              "spectrum_cut_total"          );
   stat= read_galdef_parameter(filename,parstring,&spectrum_cut_total           );

   strcpy(parstring,                              "isotropic_use"         );
   stat= read_galdef_parameter(filename,parstring,&isotropic_use          );

   strcpy(parstring,                              "isotropic_type"         );
   stat= read_galdef_parameter(filename,parstring,&isotropic_type          );

   strcpy(parstring,                              "isotropic_const"       );
   stat= read_galdef_parameter(filename,parstring,&isotropic_const        );

   strcpy(parstring,                              "isotropic_g"            );
   stat= read_galdef_parameter(filename,parstring,&isotropic_g             );


   // the following is not very elegant, would be better to have this function in read_galdef_parameter 
   n_E_EGRET=13;//AWS20031023
   char *isotropic_EGRET_char=new char[100];
   strcpy(parstring,                              "isotropic_EGRET"   );
   stat= read_galdef_parameter(filename,parstring, isotropic_EGRET_char    );
   //cout<<"isotropic_EGRET_char="<<isotropic_EGRET_char<<endl;
   isotropic_EGRET=new double[n_E_EGRET];

   sscanf(isotropic_EGRET_char,"%le,%le,%le,%le,%le,%le,%le,%le,%le,%le,%le,%le,%le", // %le for double   comma is separator character
          &isotropic_EGRET[0] ,&isotropic_EGRET[1] ,&isotropic_EGRET[2] ,&isotropic_EGRET[3], &isotropic_EGRET[4],
          &isotropic_EGRET[5] ,&isotropic_EGRET[6] ,&isotropic_EGRET[7] ,&isotropic_EGRET[8], &isotropic_EGRET[9],
          &isotropic_EGRET[10],&isotropic_EGRET[11],&isotropic_EGRET[12]); //AWS20031023

   //cout<<"isotropic_EGRET     =";
   //for(int i_E_EGRET=0;i_E_EGRET<n_E_EGRET;i_E_EGRET++)cout<<isotropic_EGRET[i_E_EGRET] <<" ";
   //cout    <<endl;

   strcpy(parstring,                              "isotropic_sree_plot"            );
   stat= read_galdef_parameter(filename,parstring,&isotropic_sree_plot             );

   convolve_GLAST=0;                                                         //AWS20080515
   strcpy(parstring,                              "convolve_GLAST"        ); //AWS20080515
   stat= read_galdef_parameter(filename,parstring,&convolve_GLAST         ); //AWS20080515


                                                   energy_disp_FermiLAT=0;         //AWS20140825
   strcpy(parstring,                              "energy_disp_FermiLAT"        ); //AWS20140825
   stat= read_galdef_parameter(filename,parstring,&energy_disp_FermiLAT         ); //AWS20140825


                                                   energy_disp_factor=1.01;      //AWS20140829
   strcpy(parstring,                              "energy_disp_factor"        ); //AWS20140829
   stat= read_galdef_parameter(filename,parstring,&energy_disp_factor         ); //AWS20140829


   strcpy(parstring,                              "fit_EGRET"         );
   stat= read_galdef_parameter(filename,parstring,&fit_EGRET          );

   strcpy(parstring,                              "fit_nrebin_EGRET"         );
   stat= read_galdef_parameter(filename,parstring,&fit_nrebin_EGRET          );

   strcpy(parstring,                              "fit_counts_min_EGRET"         );
   stat= read_galdef_parameter(filename,parstring,&fit_counts_min_EGRET          );

   strcpy(parstring,                              "fit_function_EGRET"         );
   stat= read_galdef_parameter(filename,parstring,&fit_function_EGRET          );

   strcpy(parstring,                              "fit_options_EGRET"         );
   stat= read_galdef_parameter(filename,parstring,&fit_options_EGRET          );



   strcpy(parstring,                              "data_EGRET"         );
   stat= read_galdef_parameter(filename,parstring,&data_EGRET          );

   data_GLAST=0;                //default if not present                 //AWS20080507
   strcpy(parstring,                              "data_GLAST"         );//AWS20080507
   stat= read_galdef_parameter(filename,parstring,&data_GLAST          );//AWS20080507


   GLAST_counts_file=new char[200];                                        //AWS20080819
   strcpy(GLAST_counts_file,"undefined");                                  //AWS20080819
   strcpy(parstring,                              "GLAST_counts_file");    //AWS20080819
   stat= read_galdef_parameter(filename,parstring, GLAST_counts_file );    //AWS20080819
   if(data_GLAST!=0 && strcmp(GLAST_counts_file,"undefined")==0)           //AWS20080819
     {cout<<"GLAST_counts_file undefined !"<<endl; exit(-1);}              //AWS20080819

   GLAST_exposure_file=new char[200];                                      //AWS20080819
   strcpy(GLAST_exposure_file,"undefined");                                //AWS20080819
   strcpy(parstring,                              "GLAST_exposure_file");  //AWS20080819
   stat= read_galdef_parameter(filename,parstring, GLAST_exposure_file );  //AWS20080819
   if(data_GLAST!=0 && strcmp(GLAST_exposure_file,"undefined")==0)         //AWS20080819
     {cout<<"GLAST_exposure_file undefined !"<<endl; exit(-1);}            //AWS20080819


   GLAST_psf_file=new char[200];                                           //AWS20080819
   strcpy(GLAST_psf_file,"undefined");                                     //AWS20080819
   strcpy(parstring,                              "GLAST_psf_file");       //AWS20080819
   stat= read_galdef_parameter(filename,parstring, GLAST_psf_file );       //AWS20080819
   if(data_GLAST!=0 && strcmp(GLAST_psf_file,"undefined")==0)              //AWS20080819
     {cout<<"GLAST_psf_file undefined !"<<endl; exit(-1);}                 //AWS20080819


   Fermi_edisp_file=new char[200];                                         //AWS20150226
   strcpy(Fermi_edisp_file,"p8_thibaut/edisp_P8V1_ULTRACLEAN_EDISP3.fits");//AWS20150226
   strcpy(parstring,                              "Fermi_edisp_file");     //AWS20150226
   stat= read_galdef_parameter(filename,parstring, Fermi_edisp_file );     //AWS20150226
   

   Fermi_edisp_file_type=new char[200];                                    //AWS20150226
   strcpy(Fermi_edisp_file_type,"ST_FITS");                                //AWS20150226
   strcpy(parstring,                              "Fermi_edisp_ftype");    //AWS20150226 
   stat= read_galdef_parameter(filename,parstring, Fermi_edisp_file_type );//AWS20150226
   


   Fermi_edisp_use_Aeff=1;                                                 //AWS20150226
   strcpy(parstring,                               "Fermi_edisp_use_Aeff");//AWS20150226 
   stat= read_galdef_parameter(filename,parstring, &Fermi_edisp_use_Aeff );//AWS20150226

   Fermi_edisp_E_true_threshold=5.;                                                //AWS20150226
   strcpy(parstring,                               "Fermi_edisp_Etrue_th");        //AWS20150226 
   stat= read_galdef_parameter(filename,parstring, &Fermi_edisp_E_true_threshold );//AWS20150226

   Fermi_edisp_E_meas_threshold=5.;                                                //AWS20150226
   strcpy(parstring,                               "Fermi_edisp_Emeas_th");        //AWS20150226 
   stat= read_galdef_parameter(filename,parstring, &Fermi_edisp_E_meas_threshold );//AWS20150226

  

   isotropic_bgd_file=new char[200];                                       //AWS20081119
   strcpy(isotropic_bgd_file,"undefined");                                 //AWS20081119
   strcpy(parstring,                              "isotropic_bgd_file");   //AWS20081119
   stat= read_galdef_parameter(filename,parstring, isotropic_bgd_file );   //AWS20081119
   if(data_GLAST!=0 && strcmp(isotropic_bgd_file,"undefined")==0)          //AWS20081119
     {cout<<"isotopic_bgd_file undefined !"<<endl; exit(-1);}              //AWS20081119



   Fermi_cat_file    =new char[200];                                       //AWS20081215
   strcpy(Fermi_cat_file    ,"undefined");                                 //AWS20081215
   strcpy(parstring,                              "Fermi_cat_file"    );   //AWS20081215
   stat= read_galdef_parameter(filename,parstring, Fermi_cat_file     );   //AWS20081215
   if(data_GLAST!=0 && strcmp(Fermi_cat_file    ,"undefined")==0)          //AWS20081215
     {cout<<"Fermi_cat_file    undefined !"<<endl; exit(-1);}              //AWS20081215


   Fermi_cat_file2   =new char[200];                                       //AWS20140114
   strcpy(Fermi_cat_file2   ,Fermi_cat_file);                              //AWS20140114
   strcpy(parstring,                              "Fermi_cat_file2"    );  //AWS20140114
   stat= read_galdef_parameter(filename,parstring, Fermi_cat_file2     );  //AWS20140114


   Fermi_cat_file3   =new char[200];                                       //AWS20140115
   strcpy(Fermi_cat_file3   ,Fermi_cat_file);                              //AWS20140115
   strcpy(parstring,                              "Fermi_cat_file3"    );  //AWS20140115
   stat= read_galdef_parameter(filename,parstring, Fermi_cat_file3     );  //AWS20140115


   Fermi_sensitivity_file   =new char[200];                                              //AWS20170112
   strcpy(Fermi_sensitivity_file   ,"undefined");                                        //AWS20170112
   strcpy(parstring,                              "Fermi_sens_file"    );                //AWS20170112
   stat= read_galdef_parameter(filename,parstring, Fermi_sensitivity_file     );         //AWS20170112



   strcpy(parstring,                              "data_COMPTEL"       );
   stat= read_galdef_parameter(filename,parstring,&data_COMPTEL        );
   strcpy(parstring,                              "data_OSSE"          );
   stat= read_galdef_parameter(filename,parstring,&data_OSSE           );
   strcpy(parstring,                              "data_RXTE"          );
   stat= read_galdef_parameter(filename,parstring,&data_RXTE           );

   strcpy(parstring,                              "data_INTEGRAL"      );
   stat= read_galdef_parameter(filename,parstring,&data_INTEGRAL       );
   strcpy(parstring,                              "data_INTEGRAL_comp" );
   stat= read_galdef_parameter(filename,parstring,workstring           );               
   data_INTEGRAL_comp=new int[4];
   stat=sscanf(workstring, "%d,%d,%d,%d",data_INTEGRAL_comp,data_INTEGRAL_comp+1,data_INTEGRAL_comp+2,data_INTEGRAL_comp+3 );

   strcpy(parstring,                              "data_INTEGRAL_mode"   );//AWS20050509
   stat= read_galdef_parameter(filename,parstring,&data_INTEGRAL_mode    );//AWS20050509


   strcpy(parstring,                              "data_INTEGRAL_syserr" );//AWS20050412
   stat= read_galdef_parameter(filename,parstring,&data_INTEGRAL_syserr  );//AWS20050412
   strcpy(parstring,                              "data_INTEGRAL_sysemn" );//AWS20050412
   stat= read_galdef_parameter(filename,parstring,&data_INTEGRAL_sysemn  );//AWS20050412


   strcpy(parstring,                              "data_IBIS"      );
   stat= read_galdef_parameter(filename,parstring,&data_IBIS       );


   strcpy(parstring,                              "data_MILAGRO"       );
   stat= read_galdef_parameter(filename,parstring,&data_MILAGRO        );
   strcpy(parstring,                              "data_HEGRA"         );
   stat= read_galdef_parameter(filename,parstring,&data_HEGRA          );
   strcpy(parstring,                              "data_WHIPPLE"       );
   stat= read_galdef_parameter(filename,parstring,&data_WHIPPLE        );
   strcpy(parstring,                              "data_GINGA"         );
   stat= read_galdef_parameter(filename,parstring,&data_GINGA          );
   strcpy(parstring,                              "data_Chandra"       );//AWS20050909
   data_Chandra=0;                                                       //AWS20050909
   stat= read_galdef_parameter(filename,parstring,&data_Chandra        );//AWS20050909

   strcpy(parstring,                              "model_ridge"        );
   stat= read_galdef_parameter(filename,parstring,&model_ridge         );

   spiskymax_profile  =0;         // set values in case absent from galdef file
   spiskymax_image_ID =new char[100];
   strcpy(spiskymax_image_ID,"NOT_DEFINED");
   spiskymax_iteration=0; 

   strcpy(parstring,                              "spiskymax_profile"  );//AWS20050722
   stat= read_galdef_parameter(filename,parstring,&spiskymax_profile   );//AWS20050722
   strcpy(parstring,                              "spiskymax_image_ID" );//AWS20050722
   stat= read_galdef_parameter(filename,parstring, spiskymax_image_ID  );//AWS20050722
   strcpy(parstring,                              "spiskymax_iteration");//AWS20050722
   stat= read_galdef_parameter(filename,parstring,&spiskymax_iteration );//AWS20050722

   strcpy(parstring,                              "sourcepop1_verbose"  );//AWS20051109
   stat= read_galdef_parameter(filename,parstring,&sourcepop1_verbose   );//AWS20051109

   sourcepop1_print_ctl = 0;                                                //AWS20200721
   strcpy(parstring,                              "sourcepop1_print_ctl"  );//AWS20200721
   stat= read_galdef_parameter(filename,parstring,&sourcepop1_print_ctl   );//AWS20200721

   strcpy(parstring,                              "sourcepop1_density0" );//AWS20051109
   stat= read_galdef_parameter(filename,parstring,&sourcepop1_density0  );//AWS20051109

   sourcepop1_oversample=1.;                  // default                    AWS20060109                         
   strcpy(parstring,                              "sourcepop1_oversampl");//AWS20060109
   stat= read_galdef_parameter(filename,parstring,&sourcepop1_oversample);//AWS20060109

   strcpy(parstring,                              "sourcepop1_L_min"    );//AWS20051109
   stat= read_galdef_parameter(filename,parstring,&sourcepop1_L_min     );//AWS20051109
   strcpy(parstring,                              "sourcepop1_L_max"    );//AWS20051109
   stat= read_galdef_parameter(filename,parstring,&sourcepop1_L_max     );//AWS20051109
   strcpy(parstring,                              "sourcepop1_alpha_L"  );//AWS20051109
   stat= read_galdef_parameter(filename,parstring,&sourcepop1_alpha_L   );//AWS20051109
   strcpy(parstring,                              "sourcepop1_fluxlimit");//AWS20051109
   stat= read_galdef_parameter(filename,parstring,&sourcepop1_fluxlimit );//AWS20051109
   strcpy(parstring,                              "sourcepop1_alpha_R"  );//AWS20051109
   stat= read_galdef_parameter(filename,parstring,&sourcepop1_alpha_R   );//AWS20051109
   strcpy(parstring,                              "sourcepop1_beta_R"   );//AWS20051202
   stat= read_galdef_parameter(filename,parstring,&sourcepop1_beta_R    );//AWS20051202
   strcpy(parstring,                              "sourcepop1_zscale"   );//AWS20051202
   stat= read_galdef_parameter(filename,parstring,&sourcepop1_zscale    );//AWS20051202


   strcpy(parstring,                              "sourcepop1_alpha_z"  );//AWS20051109
   stat= read_galdef_parameter(filename,parstring,&sourcepop1_alpha_z   );//AWS20051109


   sourcepop1_spectral_model = 1;                                              //AWS20100824
   strcpy(parstring,                              "sourcepop1_specmodel");     //AWS20100824
   stat= read_galdef_parameter(filename,parstring,&sourcepop1_spectral_model );//AWS20100824

   strcpy(parstring,                              "sourcepop1_spectrumg");//AWS20051109
   stat= read_galdef_parameter(filename,parstring,&sourcepop1_spectrumg );//AWS20051109


   sourcepop1_spect_g_0=sourcepop1_spectrumg; // default
   sourcepop1_spect_g_1=sourcepop1_spectrumg; // default
   sourcepop1_spect_g_2=sourcepop1_spectrumg; // default
   sourcepop1_spect_br0=1e3                 ; // default
   sourcepop1_spect_br1=1e9                 ; // default

   strcpy(parstring,                              "sourcepop1_spect_g_0");//AWS20051219
   stat= read_galdef_parameter(filename,parstring,&sourcepop1_spect_g_0 );//AWS20051219
   strcpy(parstring,                              "sourcepop1_spect_br0");//AWS20051219
   stat= read_galdef_parameter(filename,parstring,&sourcepop1_spect_br0 );//AWS20051219
   strcpy(parstring,                              "sourcepop1_spect_g_1");//AWS20051219
   stat= read_galdef_parameter(filename,parstring,&sourcepop1_spect_g_1 );//AWS20051219
   strcpy(parstring,                              "sourcepop1_spect_br1");//AWS20051219
   stat= read_galdef_parameter(filename,parstring,&sourcepop1_spect_br1 );//AWS20051219
   strcpy(parstring,                              "sourcepop1_spect_g_2");//AWS20051219
   stat= read_galdef_parameter(filename,parstring,&sourcepop1_spect_g_2 );//AWS20051219


   sourcepop1_spectrum_g_0_sigma=0.0;// default
   sourcepop1_spectrum_br0_sigma=0.0;// default
   sourcepop1_spectrum_g_1_sigma=0.0;// default
   sourcepop1_spectrum_br1_sigma=0.0;// default
   sourcepop1_spectrum_g_2_sigma=0.0;// default

   strcpy(parstring,                              "sourcepop1_sp_g0_sig");         //AWS20100824
   stat= read_galdef_parameter(filename,parstring,&sourcepop1_spectrum_g_0_sigma );//AWS20100824
   strcpy(parstring,                              "sourcepop1_spbr0_sig");         //AWS20100824
   stat= read_galdef_parameter(filename,parstring,&sourcepop1_spectrum_br0_sigma );//AWS20100824
   strcpy(parstring,                              "sourcepop1_sp_g1_sig");         //AWS20100824
   stat= read_galdef_parameter(filename,parstring,&sourcepop1_spectrum_g_1_sigma );//AWS20100824
   strcpy(parstring,                              "sourcepop1_spbr1_sig");         //AWS20100824
   stat= read_galdef_parameter(filename,parstring,&sourcepop1_spectrum_br1_sigma );//AWS20100824
   strcpy(parstring,                              "sourcepop1_sp_g2_sig");         //AWS20100824
   stat= read_galdef_parameter(filename,parstring,&sourcepop1_spectrum_g_2_sigma );//AWS20100824


   sourcepop1_E_ref_low =100.  ;              //default                     AWS20060109
   sourcepop1_E_ref_high=10000.;              //default                     AWS20060109
   strcpy(parstring,                              "sourcepop1_E_ref_low");//AWS20060109
   stat= read_galdef_parameter(filename,parstring,&sourcepop1_E_ref_low );//AWS20060109
   strcpy(parstring,                              "sourcepop1_E_ref_hig");//AWS20060109
   stat= read_galdef_parameter(filename,parstring,&sourcepop1_E_ref_high);//AWS20060109

   sourcepop_SPI_cat=new char[100];   
   strcpy(sourcepop_SPI_cat, "catalogue.spimodfit.2481.fits");//default   //AWS20060111
   strcpy(parstring,                              "sourcepop_SPI_cat"   );//AWS20060111
   stat= read_galdef_parameter(filename,parstring, sourcepop_SPI_cat    );//AWS20060111

   strcpy(parstring,                              "sourcepop1_specnormE");//AWS20051109
   stat= read_galdef_parameter(filename,parstring,&sourcepop1_specnormE );//AWS20051109
   strcpy(parstring,                              "sourcepop_total"     );//AWS20051109
   stat= read_galdef_parameter(filename,parstring,&sourcepop_total      );//AWS20051109
   strcpy(parstring,                              "sourcepop_sublimit"  );//AWS20051109
   stat= read_galdef_parameter(filename,parstring,&sourcepop_sublimit   );//AWS20051109
   strcpy(parstring,                              "sourcepop_soplimit"  );//AWS20051109
   stat= read_galdef_parameter(filename,parstring,&sourcepop_soplimit   );//AWS20051109

   sourcepop_ext_model=0;                                                 //AWS20120522
   strcpy(parstring,                              "sourcepop_ext_model" );//AWS20120522
   stat= read_galdef_parameter(filename,parstring,&sourcepop_ext_model  );//AWS20120522

   ///////////////////////////////////////////////////////////////////////////////

   strcpy(parstring,                              "gcr_spectra"         );
   stat= read_galdef_parameter(filename,parstring,&gcr_spectra          );

   strcpy(parstring,                              "gcr_spectra_r_min"         );
   stat= read_galdef_parameter(filename,parstring,&gcr_spectra_r_min         );

   strcpy(parstring,                              "gcr_spectra_r_max"         );
   stat= read_galdef_parameter(filename,parstring,&gcr_spectra_r_max         );

   strcpy(parstring,                              "gcr_spectra_z_min"         );
   stat= read_galdef_parameter(filename,parstring,&gcr_spectra_z_min         );

   strcpy(parstring,                              "gcr_spectra_z_max"         );
   stat= read_galdef_parameter(filename,parstring,&gcr_spectra_z_max         );


  





   strcpy(parstring,                              "gcr_spectra_Emin"        );
   stat= read_galdef_parameter(filename,parstring,&gcr_spectra_Emin         );
   strcpy(parstring,                              "gcr_spectra_Emax"        );
   stat= read_galdef_parameter(filename,parstring,&gcr_spectra_Emax         );
   strcpy(parstring,                              "gcr_spectra_Imin"        );
   stat= read_galdef_parameter(filename,parstring,&gcr_spectra_Imin         );
   strcpy(parstring,                              "gcr_spectra_Imax"        );
   stat= read_galdef_parameter(filename,parstring,&gcr_spectra_Imax         );



   strcpy(parstring,                              "gcr_spectra_Z"             );
   stat= read_galdef_parameter(filename,parstring, workstring                );
   cout<<workstring        <<endl;
   gcr_spectra_Z=new int[100];
   gcr_spectra_n_ZA=sscanf(workstring,            
                          "%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d",
			  gcr_spectra_Z  , gcr_spectra_Z+1, gcr_spectra_Z+2, gcr_spectra_Z+3, gcr_spectra_Z+4, gcr_spectra_Z+5,
		          gcr_spectra_Z+6, gcr_spectra_Z+7, gcr_spectra_Z+8, gcr_spectra_Z+9, gcr_spectra_Z+10,gcr_spectra_Z+11,
		          gcr_spectra_Z+12,gcr_spectra_Z+13,gcr_spectra_Z+14,gcr_spectra_Z+15,gcr_spectra_Z+16,gcr_spectra_Z+17, //AWS20060620
                          gcr_spectra_Z+18,gcr_spectra_Z+19,gcr_spectra_Z+20,gcr_spectra_Z+21,gcr_spectra_Z+22,gcr_spectra_Z+23, //AWS20060620
                          gcr_spectra_Z+24,gcr_spectra_Z+25,gcr_spectra_Z+26,gcr_spectra_Z+27,gcr_spectra_Z+28,gcr_spectra_Z+29, //AWS20060620
                          gcr_spectra_Z+30,gcr_spectra_Z+31,gcr_spectra_Z+32,gcr_spectra_Z+33,gcr_spectra_Z+34,gcr_spectra_Z+35);//AWS20060620
  
   strcpy(parstring,                              "gcr_spectra_A"             );
   stat= read_galdef_parameter(filename,parstring, workstring                );
   gcr_spectra_A=new int[100];
   stat            =sscanf(workstring,          
                          "%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d",
			  gcr_spectra_A  , gcr_spectra_A+1, gcr_spectra_A+2, gcr_spectra_A+3, gcr_spectra_A+4, gcr_spectra_A+5,
			  gcr_spectra_A+6, gcr_spectra_A+7, gcr_spectra_A+8, gcr_spectra_A+9, gcr_spectra_A+10,gcr_spectra_A+11,
		          gcr_spectra_A+12,gcr_spectra_A+13,gcr_spectra_A+14,gcr_spectra_A+15,gcr_spectra_A+16,gcr_spectra_A+17, //AWS20060620
                          gcr_spectra_A+18,gcr_spectra_A+19,gcr_spectra_A+20,gcr_spectra_A+21,gcr_spectra_A+22,gcr_spectra_A+23, //AWS20060620
                          gcr_spectra_A+24,gcr_spectra_A+25,gcr_spectra_A+26,gcr_spectra_A+27,gcr_spectra_A+28,gcr_spectra_A+29, //AWS20060620
                          gcr_spectra_A+30,gcr_spectra_A+31,gcr_spectra_A+32,gcr_spectra_A+33,gcr_spectra_A+34,gcr_spectra_A+35);//AWS20060620

   strcpy(parstring,                              "gcr_spectra_n_ratio"     );
   stat= read_galdef_parameter(filename,parstring,&gcr_spectra_n_ratio      );

   gcr_spectra_n_sec_ZA =new int    [gcr_spectra_n_ratio];
   gcr_spectra_n_pri_ZA =new int    [gcr_spectra_n_ratio];
   gcr_spectra_sec_Z    =new int   *[gcr_spectra_n_ratio];
   gcr_spectra_sec_A    =new int   *[gcr_spectra_n_ratio];
   gcr_spectra_pri_Z    =new int   *[gcr_spectra_n_ratio];
   gcr_spectra_pri_A    =new int   *[gcr_spectra_n_ratio];
   gcr_spectra_ratio_min=new double [gcr_spectra_n_ratio];
   gcr_spectra_ratio_max=new double [gcr_spectra_n_ratio];

   int i_ratio,i_ZA;
   for(i_ratio=0;i_ratio<gcr_spectra_n_ratio;i_ratio++)
   {
     gcr_spectra_sec_Z[i_ratio]=new int[100];
     gcr_spectra_sec_A[i_ratio]=new int[100];
     gcr_spectra_pri_Z[i_ratio]=new int[100];
     gcr_spectra_pri_A[i_ratio]=new int[100];
   }

   int *buffer=new int[100];
   for(i_ratio=0;i_ratio<gcr_spectra_n_ratio;i_ratio++)
   {
    sprintf(parstring,"gcr_spectra_sec_Z_%02d",i_ratio+1);
    stat= read_galdef_parameter(filename,parstring,workstring      );
    cout<<"parstring="<<parstring<<" workstring "<<workstring<<endl;
    gcr_spectra_n_sec_ZA[i_ratio]=sscanf(workstring,          
                        "%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d",
			 buffer,buffer+1,buffer+2,buffer+3,buffer+4,buffer+5,buffer+6,buffer+7,buffer+8,buffer+9);

    cout<<gcr_spectra_n_sec_ZA[i_ratio]<<" buffer=";for(i_ZA=0;i_ZA<gcr_spectra_n_sec_ZA[i_ratio];i_ZA++)cout<<buffer[i_ZA]<<" ";cout<<endl;
    for(i_ZA=0;i_ZA<gcr_spectra_n_sec_ZA[i_ratio];i_ZA++)gcr_spectra_sec_Z[i_ratio][i_ZA]=buffer[i_ZA];

    sprintf(parstring,"gcr_spectra_sec_A_%02d",i_ratio+1);
    stat= read_galdef_parameter(filename,parstring,workstring      );
    cout<<"parstring="<<parstring<<" workstring "<<workstring<<endl;
    gcr_spectra_n_sec_ZA[i_ratio]=sscanf(workstring,          
                        "%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d",
			 buffer,buffer+1,buffer+2,buffer+3,buffer+4,buffer+5,buffer+6,buffer+7,buffer+8,buffer+9);

    cout<<gcr_spectra_n_sec_ZA[i_ratio]<<" buffer=";for(i_ZA=0;i_ZA<gcr_spectra_n_sec_ZA[i_ratio];i_ZA++)cout<<buffer[i_ZA]<<" ";cout<<endl;
    for(i_ZA=0;i_ZA<gcr_spectra_n_sec_ZA[i_ratio];i_ZA++)gcr_spectra_sec_A[i_ratio][i_ZA]=buffer[i_ZA];


    sprintf(parstring,"gcr_spectra_pri_Z_%02d",i_ratio+1);
    stat= read_galdef_parameter(filename,parstring,workstring      );
    cout<<"parstring="<<parstring<<" workstring "<<workstring<<endl;
    gcr_spectra_n_pri_ZA[i_ratio]=sscanf(workstring,          
                        "%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d",
			 buffer,buffer+1,buffer+2,buffer+3,buffer+4,buffer+5,buffer+6,buffer+7,buffer+8,buffer+9);

    cout<<gcr_spectra_n_pri_ZA[i_ratio]<<" buffer=";for(i_ZA=0;i_ZA<gcr_spectra_n_pri_ZA[i_ratio];i_ZA++)cout<<buffer[i_ZA]<<" ";cout<<endl;
    for(i_ZA=0;i_ZA<gcr_spectra_n_pri_ZA[i_ratio];i_ZA++)gcr_spectra_pri_Z[i_ratio][i_ZA]=buffer[i_ZA];

    sprintf(parstring,"gcr_spectra_pri_A_%02d",i_ratio+1);
    stat= read_galdef_parameter(filename,parstring,workstring      );
    cout<<"parstring="<<parstring<<" workstring "<<workstring<<endl;
    gcr_spectra_n_pri_ZA[i_ratio]=sscanf(workstring,          
                        "%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d",
			 buffer,buffer+1,buffer+2,buffer+3,buffer+4,buffer+5,buffer+6,buffer+7,buffer+8,buffer+9);

    cout<<gcr_spectra_n_pri_ZA[i_ratio]<<" buffer=";for(i_ZA=0;i_ZA<gcr_spectra_n_pri_ZA[i_ratio];i_ZA++)cout<<buffer[i_ZA]<<" ";cout<<endl;
    for(i_ZA=0;i_ZA<gcr_spectra_n_pri_ZA[i_ratio];i_ZA++)gcr_spectra_pri_A[i_ratio][i_ZA]=buffer[i_ZA];

    sprintf(parstring,"gcr_spectra_ramin_%02d",i_ratio+1);
    stat= read_galdef_parameter(filename,parstring,&gcr_spectra_ratio_min[i_ratio] );
   
    sprintf(parstring,"gcr_spectra_ramax_%02d",i_ratio+1);
    stat= read_galdef_parameter(filename,parstring,&gcr_spectra_ratio_max[i_ratio] );

   }//i_ratio

   strcpy(parstring,                              "gcr_spectra_mod_phi"      );
   stat= read_galdef_parameter(filename,parstring, workstring                );
   gcr_spectra_mod_phi=new double[100];
   gcr_spectra_n_mod_phi            =sscanf(workstring,          
                          "%le,%le,%le,%le,%le,%le,%le,%le,%le%le,%le,%le,%le,%le,%le,%le,%le,%le",
			   gcr_spectra_mod_phi  ,gcr_spectra_mod_phi+1,gcr_spectra_mod_phi+2,gcr_spectra_mod_phi+3,
                           gcr_spectra_mod_phi+4,gcr_spectra_mod_phi+5,gcr_spectra_mod_phi+6,gcr_spectra_mod_phi+7);
                       

   gcr_database_file = new char[200];                                            //AWS20090220
   strcpy(gcr_database_file,"GCR_data_1.dat");                                   //AWS20090220  default
   strcpy(parstring,                              "gcr_database_file"   );       //AWS20090220    
   stat= read_galdef_parameter(filename,parstring, gcr_database_file    );       //AWS20090220

   strcpy(parstring,                              "sync_index_nu"             ); //AWS20070523
   stat= read_galdef_parameter(filename,parstring, workstring                );  //AWS20070523
   cout<<workstring        <<endl;
   sync_index_nu=new double[100];                                                //AWS20070523
   sync_index_n_nu=sscanf(workstring,                                            //AWS20070523
   "%le,%le,%le,%le,%le,%le,%le,%le,%le,%le,%le,%le,%le,%le,%le,%le,%le,%le,%le,%le,%le,%le,%le,%le,%le,%le,%le,%le,%le,%le,%le,%le,%le,%le,%le,%le,%le",
		           sync_index_nu  , sync_index_nu+1,sync_index_nu+2, sync_index_nu+3,sync_index_nu+4, sync_index_nu+5,
		       	   sync_index_nu+6, sync_index_nu+7,sync_index_nu+8, sync_index_nu+9,sync_index_nu+10,sync_index_nu+11);       

   sync_index_n_nu/=2; // number of spectral indices (since they come in pairs)

   strcpy(parstring,                              "sync_data_WMAP"     );//AWS20071221
   sync_data_WMAP=0;                                                     //AWS20071221
   stat= read_galdef_parameter(filename,parstring,&sync_data_WMAP      );//AWS20071221

   strcpy(parstring,                              "sync_data_options"  );//AWS20120110
   sync_data_options=0;                                                  //AWS20120110
   stat= read_galdef_parameter(filename,parstring,&sync_data_options   );//AWS20120110


   strcpy(parstring,                              "sync_data____10MHz" );//AWS20071221
   sync_data____10MHz=0;                                                 //AWS20071221
   stat= read_galdef_parameter(filename,parstring,&sync_data____10MHz  );//AWS20071221

   strcpy(parstring,                              "sync_data____22MHz" );//AWS20071221
   sync_data____22MHz=0;                                                 //AWS20071221
   stat= read_galdef_parameter(filename,parstring,&sync_data____22MHz  );//AWS20071221

   strcpy(parstring,                              "sync_data____45MHz" );//AWS20071221
   sync_data____45MHz=0;                                                 //AWS20071221
   stat= read_galdef_parameter(filename,parstring,&sync_data____45MHz  );//AWS20071221

   strcpy(parstring,                              "sync_data___150MHz" );//AWS20071221
   sync_data___150MHz=0;                                                 //AWS20071221
   stat= read_galdef_parameter(filename,parstring,&sync_data___150MHz  );//AWS20071221


   strcpy(parstring,                              "sync_data___408MHz" );//AWS20071221
   sync_data___408MHz=0;                                                 //AWS20071221
   stat= read_galdef_parameter(filename,parstring,&sync_data___408MHz  );//AWS20071221

   strcpy(parstring,                              "sync_data___820MHz" );//AWS20071221
   sync_data___820MHz=0;                                                 //AWS20071221
   stat= read_galdef_parameter(filename,parstring,&sync_data___820MHz  );//AWS20071221

   strcpy(parstring,                              "sync_data__1420MHz" );//AWS20071221
   sync_data__1420MHz=0;                                                 //AWS20071221
   stat= read_galdef_parameter(filename,parstring,&sync_data__1420MHz  );//AWS20071221

   strcpy(parstring,                              "sync_data__2326MHz" );//AWS20071221
   sync_data__2326MHz=0;                                                 //AWS20071221
   stat= read_galdef_parameter(filename,parstring,&sync_data__2326MHz  );//AWS20071221


   strcpy(parstring,                              "sync_data_22800MHz" );//AWS20071221
   sync_data_22800MHz=0;                                                 //AWS20071221
   stat= read_galdef_parameter(filename,parstring,&sync_data_22800MHz  );//AWS20071221

   strcpy(parstring,                              "sync_data_33000MHz" );//AWS20071221
   sync_data_33000MHz=0;                                                 //AWS20071221
   stat= read_galdef_parameter(filename,parstring,&sync_data_33000MHz  );//AWS20071221

   strcpy(parstring,                              "sync_data_41000MHz" );//AWS20071221
   sync_data_41000MHz=0;                                                 //AWS20071221
   stat= read_galdef_parameter(filename,parstring,&sync_data_41000MHz  );//AWS20071221

   strcpy(parstring,                              "sync_data_61000MHz" );//AWS20071221
   sync_data_61000MHz=0;                                                 //AWS20071221
   stat= read_galdef_parameter(filename,parstring,&sync_data_61000MHz  );//AWS20071221

   strcpy(parstring,                              "sync_data_94000MHz" );//AWS20071221
   sync_data_94000MHz=0;                                                 //AWS20071221
   stat= read_galdef_parameter(filename,parstring,&sync_data_94000MHz  );//AWS20071221

   strcpy(parstring,                              "free_free_options"  );//AWS20111222
   free_free_options =1;                                                 //AWS20111222
   stat= read_galdef_parameter(filename,parstring,&free_free_options   );//AWS20111222


   strcpy(parstring,                              "spin_dust_options"  );//AWS20120117
   spin_dust_options =1;                                                 //AWS20120117
   stat= read_galdef_parameter(filename,parstring,&spin_dust_options   );//AWS20120117

   strcpy(parstring,                                "luminosity"        );//AWS20100823
   luminosity        = 1  ;                                               //AWS20100823
   stat= read_galdef_parameter(filename,parstring,  &luminosity         );//AWS20100823


   strcpy(parstring,                                    "galdef_series" );//AWS20100329
   galdef_series=0;                                                       //AWS20100329
   stat= read_galdef_parameter(filename,parstring,      &galdef_series  );//AWS20100329




   strcpy(parstring,                                "ISRF_luminosity_R" );//AWS20100421
   ISRF_luminosity_R = 30.;                                               //AWS20100421
   stat= read_galdef_parameter(filename,parstring,  &ISRF_luminosity_R  );//AWS20100421


   strcpy(parstring,                                "ISRF_luminosity_z" );//AWS20100421
   ISRF_luminosity_z = 30.;                                               //AWS20100421
   stat= read_galdef_parameter(filename,parstring,  &ISRF_luminosity_z  );//AWS20100421

   strcpy(parstring,                                "ISRF_read"         );//AWS20100818
   ISRF_read         = 1  ;                                               //AWS20100818
   stat= read_galdef_parameter(filename,parstring,  &ISRF_read          );//AWS20100818


   print();
   cout<<" <<<<galplotdef read"<<endl;
   return stat;
}

//**.****|****.****|****.****|****.****|****.****|****.****|****.****|****.****|

int Galplotdef::read_galdef_parameter(char *filename, char *parstring, double *value)
{

   FILE *ft;
   ft=fopen(filename,"r"); if (ft==NULL) return -1;
   char *flag;
   int   found;   
  
   char dumstring[100];
   char input[500]; //AWS20131210
   int stat=0;

   found=-1;
   while(found!=0)
   {
      flag=fgets(input,1000,ft); // read string until newline (Schildt p.222)
      if(flag==0)
      {
         cout<<parstring<<" not found in galdef file!"<<endl;
         fclose(ft); 
         stat=1;
         return stat;
      }
//  printf("%s",input);       // string is \0 terminated
      sscanf(input,"%s"  ,dumstring  );
      found=strcmp(dumstring,parstring); // search for parstring  in input
  
      if(found==0)
      {
         sscanf(input,"%22c%le"  ,dumstring, value);  // le for double
	 //cout<<parstring<<"    found! "<<input<<" "<<dumstring<<endl;
         //cout<<endl<<"difread: value of "<< parstring <<"="<<*value <<endl;
      }
   }

   fclose(ft); 

//  cout<<" <<<< read_galdef_parameter"<<endl;
   return stat;
}

//**.****|****.****|****.****|****.****|****.****|****.****|****.****|****.****|

int Galplotdef::read_galdef_parameter(char *filename, char *parstring, int *value)
{
   FILE *ft;
   ft=fopen(filename,"r"); if (ft==NULL) return -1;
   char *flag;
   int   found; 
   char dumstring[100];
   char input[500]; //AWS20131210
   int stat=0;

   found=-1;
   while(found!=0)
   {
      flag=fgets(input,1000,ft);   // read string until newline (Schildt p.222)
      if(flag==0)
      {
         cout<<parstring<<" not found in galdef file!"<<endl;
         fclose(ft);
         stat=1;
         return stat;
      }
//  printf("%s",input);       // string is \0 terminated
      sscanf(input,"%s"  ,dumstring  );
      found=strcmp(dumstring,parstring); // search for parstring  in input
  
      if(found==0)
      {
         sscanf(input,"%22c%d"   ,dumstring, value);  // d for int    
//  cout<<parstring<<"    found!"<<endl;
//  cout<<endl<<"difread: value of "<< parstring <<"="<<*value <<endl;
      }  
   }
   fclose(ft); 
//  cout<<" <<<< read_galdef_parameter"<<endl;
   return stat;
}

//**.****|****.****|****.****|****.****|****.****|****.****|****.****|****.****|

int Galplotdef::read_galdef_parameter(char *filename, char *parstring, char *value)
{
   FILE *ft;
   ft=fopen(filename,"r"); if (ft==NULL) return -1;
   char *flag;
   int   found;   
   char dumstring[100];
   char input[500]; //AWS20131210
   int stat=0;

   found=-1; 
   while(found!=0)
   {
      flag=fgets(input,1000,ft);   // read string until newline (Schildt p.222)
      if(flag==0)
      {
         cout<<parstring<<" not found in galdef file!"<<endl;
         fclose(ft); 
         stat=1;
         return stat;
      }
//  printf("%s",input);       // string is \0 terminated
      sscanf(input,"%s"  ,dumstring  );
      found=strcmp(dumstring,parstring); // search for parstring  in input
   
      if(found==0)
      {
	 sscanf(input,"%22c%s"   ,dumstring, value      ); // s for string 
// cout<<parstring<<"    found!"<<endl;
// cout<<endl<<"difread: value of "<< parstring <<"="<<*value <<endl;
      }  
   }
   fclose(ft); 
//  cout<<" <<<< read_galdef_parameter"<<endl;
   return stat;
}

void Galplotdef::print()
{
   cout<<"  =================================="<<endl;
   cout<<"  ======= galplotdef: "<<galdef_ID   <<endl;
   cout<<"  =================================="<<endl;

   cout<<"  version  "<<version <<endl;
   cout<<"  run_no   "<<run_no      <<endl;

   /*-------------- not needed, uses Galdef.h
   cout<<"  n_spatial_dimensions "<<n_spatial_dimensions   <<endl;

   cout<<"  r_min    "<<r_min   <<endl;
   cout<<"  r_max    "<<r_max   <<endl; 
   cout<<"  dr       "<<dr      <<endl;
   cout<<"  z_min    "<<z_min   <<endl;
   cout<<"  z_max    "<<z_max   <<endl;
   cout<<"  dz       "<<dz      <<endl;
   cout<<"  p_min    "<<p_min   <<endl;
   cout<<"  p_max    "<<p_max   <<endl;
   cout<<"  p_factor "<<p_factor<<endl;
   cout<<"  Ekin_min "<<   Ekin_min      <<endl;
   cout<<"  Ekin_max "<<   Ekin_max      <<endl;
   cout<<"  Ekin_factor "<<Ekin_factor   <<endl;

   cout<<"  p_Ekin_grid "<<p_Ekin_grid   <<endl;

   ---------- end of not needed */

   cout<<"  E_gamma_min   "<<   E_gamma_min      <<endl;
   cout<<"  E_gamma_max   "<<   E_gamma_max      <<endl;
   cout<<"  E_gamma_factor"<<   E_gamma_factor   <<endl;

   cout<<" nu_synch_min    "<<  nu_synch_min      <<endl;
   cout<<" nu_synch_max    "<<  nu_synch_max      <<endl;
   cout<<" nu_synch_factor "<<  nu_synch_factor   <<endl;

   cout<<"  long_min      "<<   long_min         <<endl;
   cout<<"  long_max      "<<   long_max         <<endl;
   cout<<"   lat_min      "<<    lat_min         <<endl;
   cout<<"   lat_max      "<<    lat_max         <<endl;
   cout<<"  d_long        "<<   d_long           <<endl;
   cout<<"  d_lat         "<<   d_lat            <<endl;

  
   cout<<" verbose       "<<verbose      <<endl;
 
   cout<<" test_suite    "<<test_suite   <<endl;
   cout<<" psfile_tag    "<<psfile_tag   <<endl;
   cout<<" screen_output "<<screen_output<<endl;

   cout<<"  long_min1     "<<   long_min1        <<endl;
   cout<<"  long_max1     "<<   long_max1        <<endl;
   cout<<"  long_min2     "<<   long_min2        <<endl;
   cout<<"  long_max2     "<<   long_max2        <<endl;
   cout<<"  long_binsize  "<<   long_binsize     <<endl;
   cout<<"  long_log_scale "<<  long_log_scale   <<endl;

   cout<<"   lat_min1     "<<    lat_min1        <<endl;
   cout<<"   lat_max1     "<<    lat_max1        <<endl;
   cout<<"   lat_min2     "<<    lat_min2        <<endl;
   cout<<"   lat_max2     "<<    lat_max2        <<endl;
   cout<<"   lat_binsize  "<<    lat_binsize     <<endl;
   cout<<"   lat_log_scale "<<   lat_log_scale   <<endl;


   cout<<"  gamma_spectrum_Emin "<<  gamma_spectrum_Emin <<endl;
   cout<<"  gamma_spectrum_Emax "<<  gamma_spectrum_Emax <<endl;
   cout<<"  gamma_spectrum_Imin "<<  gamma_spectrum_Imin <<endl;
   cout<<"  gamma_spectrum_Imax "<<  gamma_spectrum_Imax <<endl;

   cout<<"  gamma_IC_selectcomp "<<  gamma_IC_selectcomp <<endl;

   cout<<"  sources_EGRET       "<<  sources_EGRET           <<endl;
   cout<<"  energies_EGRET      "<<  energies_EGRET          <<endl;

   cout<<" convolve_EGRET       "<<  convolve_EGRET          <<endl;
   cout<<" convolve_EGRET_iEmax "<<  convolve_EGRET_iEmax    <<endl;
   cout<<" convolve_EGRET_thmax "<<  convolve_EGRET_thmax    <<endl;
   cout<<" error_bar_EGRET      "<<  error_bar_EGRET         <<endl;
   cout<<" spectrum_style_EGRET "<<  spectrum_style_EGRET    <<endl;
   cout<<" spectrum_index_EGRET "<<  spectrum_index_EGRET    <<endl;
   cout<<" spectrum_50GeV_EGRET "<<  spectrum_50GeV_EGRET    <<endl;
   cout<<" spectrum_cut_total   "<<  spectrum_cut_total      <<endl;

   cout<<" isotropic_use        "<<  isotropic_use           <<endl;
   cout<<" isotropic_type       "<<  isotropic_type          <<endl;
   cout<<" isotropic_const      "<<  isotropic_const         <<endl;
   cout<<" isotropic_g          "<<  isotropic_g             <<endl;
   cout<<" isotropic_EGRET     =";
   for(int i_E_EGRET=0;i_E_EGRET<n_E_EGRET;i_E_EGRET++)cout<<isotropic_EGRET[i_E_EGRET] <<" ";
   cout    <<endl;
   cout<<" isotropic_sree_plot  "<<  isotropic_sree_plot     <<endl;

   cout<<" convolve_GLAST       "<<  convolve_GLAST          <<endl;
   cout<<" energy_disp_FermiLAT "<<energy_disp_FermiLAT      <<endl;
   cout<<" energy_disp_factor   "<<energy_disp_factor        <<endl;

   cout<<" fit_EGRET            "<<  fit_EGRET               <<endl;
   cout<<" fit_nrebin_EGRET     "<<  fit_nrebin_EGRET        <<endl;
   cout<<" fit_counts_min_EGRET "<<  fit_counts_min_EGRET    <<endl;
   cout<<" fit function_EGRET   "<<  fit_function_EGRET      <<endl;
   cout<<" fit_options_EGRET    "<<  fit_options_EGRET       <<endl;

   cout<<"        data_EGRET    "<<         data_EGRET       <<endl;

   cout<<"        data_GLAST    "<<           data_GLAST              <<endl;
   cout<<"             GLAST_counts_file    "<<    GLAST_counts_file  <<endl;
   cout<<"             GLAST_exposure_file  "<<    GLAST_exposure_file<<endl;
   cout<<"             GLAST_psf_file       "<<    GLAST_psf_file     <<endl;

   cout<<"        Fermi_edisp_file             "<<    Fermi_edisp_file            <<endl;
   cout<<"        Fermi_edisp_file_type        "<<    Fermi_edisp_file_type       <<endl;
   cout<<"        Fermi_edisp_use_Aeff         "<<    Fermi_edisp_use_Aeff        <<endl;
   cout<<"        Fermi_edisp_E_true_threshold "<<    Fermi_edisp_E_true_threshold<<endl;
   cout<<"        Fermi_edisp_E_meas_threshold "<<    Fermi_edisp_E_meas_threshold<<endl;

   cout<<"        isotropic_bgd_file        "<<    isotropic_bgd_file <<endl;
   cout<<"        Fermi_cat_file            "<<    Fermi_cat_file     <<endl;
   cout<<"        Fermi_cat_file2           "<<    Fermi_cat_file2    <<endl;
   cout<<"        Fermi_cat_file3           "<<    Fermi_cat_file3    <<endl;

   cout<<"        Fermi_sensitivity_file    "<<    Fermi_sensitivity_file    <<endl;


   cout<<"        data_COMPTEL  "<<         data_COMPTEL     <<endl;
   cout<<"        data_OSSE     "<<         data_OSSE        <<endl;
   cout<<"        data_RXTE     "<<         data_RXTE        <<endl;
   cout<<"        data_INTEGRAL "<<         data_INTEGRAL    <<endl;
   cout<<"        data_INTEGRAL_comp "<<    data_INTEGRAL_comp[0]<<" - "<<  data_INTEGRAL_comp[1]<<",     "; 
   cout<<                                   data_INTEGRAL_comp[2]<<" - "<<  data_INTEGRAL_comp[3]<<endl;

   cout<<"        data_INTEGRAL_mode   "<<  data_INTEGRAL_mode  <<endl;
   cout<<"        data_INTEGRAL_syserr "<<  data_INTEGRAL_syserr<<endl;
   cout<<"        data_INTEGRAL_sysemn "<<  data_INTEGRAL_sysemn<<endl;

   cout<<"        data_IBIS     "<<         data_IBIS        <<endl;
   cout<<"        data_MILAGRO  "<<         data_MILAGRO     <<endl;
   cout<<"        data_HEGRA    "<<         data_HEGRA       <<endl;
   cout<<"        data_WHIPPLE  "<<         data_WHIPPLE     <<endl;
   cout<<"        data_GINGA    "<<         data_GINGA       <<endl;
   cout<<"        data_Chandra  "<<         data_Chandra     <<endl;
   cout<<"       model_ridge    "<<        model_ridge       <<endl;

   cout<<"  spiskymax_profile   "<<      spiskymax_profile   <<endl;
   cout<<"  spiskymax_image_ID  "<<      spiskymax_image_ID  <<endl;
   cout<<"  spiskymax_iteration "<<      spiskymax_iteration <<endl;

   cout<<"  sourcepop1_verbose   "<<      sourcepop1_verbose  <<endl;
   cout<<"  sourcepop1_print_ctl "<<      sourcepop1_print_ctl<<endl;
   cout<<"  sourcepop1_density0  "<<      sourcepop1_density0 <<endl;
   cout<<"  sourcepop1_oversample "<<     sourcepop1_oversample<<endl;
   cout<<"  sourcepop1_L_min     "<<      sourcepop1_L_min    <<endl;
   cout<<"  sourcepop1_L_max     "<<      sourcepop1_L_max    <<endl;
   cout<<"  sourcepop1_alpha_L   "<<      sourcepop1_alpha_L  <<endl;
   cout<<"  sourcepop1_fluxlimit "<<      sourcepop1_fluxlimit<<endl;
   cout<<"  sourcepop1_alpha_R   "<<      sourcepop1_alpha_R  <<endl;
   cout<<"  sourcepop1_beta_R    "<<      sourcepop1_beta_R   <<endl;
   cout<<"  sourcepop1_zscale    "<<      sourcepop1_zscale   <<endl;

   cout<<"  sourcepop1_alpha_z   "<<      sourcepop1_alpha_z  <<endl;

   cout<<"  sourcepop1_spectral_model "<< sourcepop1_spectral_model<<endl;

   cout<<"  sourcepop1_spectrumg "<<      sourcepop1_spectrumg<<endl;
   cout<<"  sourcepop1_spect_g_0 "<<      sourcepop1_spect_g_0<<endl;
   cout<<"  sourcepop1_spect_br0 "<<      sourcepop1_spect_br0<<endl;
   cout<<"  sourcepop1_spect_g_1 "<<      sourcepop1_spect_g_1<<endl;
   cout<<"  sourcepop1_spect_br1 "<<      sourcepop1_spect_br1<<endl;
   cout<<"  sourcepop1_spect_g_2 "<<      sourcepop1_spect_g_2<<endl;

   cout<<"  sourcepop1_spectrum_g_0_sigma "<<      sourcepop1_spectrum_g_0_sigma<<endl;
   cout<<"  sourcepop1_spectrum_br0_sigma "<<      sourcepop1_spectrum_br0_sigma<<endl;
   cout<<"  sourcepop1_spectrum_g_1_sigma "<<      sourcepop1_spectrum_g_1_sigma<<endl;
   cout<<"  sourcepop1_spectrum_br1_sigma "<<      sourcepop1_spectrum_br1_sigma<<endl;
   cout<<"  sourcepop1_spectrum_g_2_sigma "<<      sourcepop1_spectrum_g_2_sigma<<endl;



   cout<<"  sourcepop1_E_ref_low "<<      sourcepop1_E_ref_low <<endl;
   cout<<"  sourcepop1_E_ref_high "<<     sourcepop1_E_ref_high<<endl;

   cout<<"  sourcepop_SPI_cat     "<<     sourcepop_SPI_cat    <<endl;

   cout<<"  sourcepop1_specnormE "<<      sourcepop1_specnormE<<endl;
   cout<<"  sourcepop_total      "<<      sourcepop_total     <<endl;
   cout<<"  sourcepop_sublimit   "<<      sourcepop_sublimit  <<endl;
   cout<<"  sourcepop_soplimit   "<<      sourcepop_soplimit  <<endl;
   cout<<"  sourcepop_ext_model  "<<      sourcepop_ext_model <<endl;


   cout<<" gcr_spectra          "<<  gcr_spectra             <<endl;
   cout<<" gcr_spectra_r_min    "<<  gcr_spectra_r_min       <<endl;
   cout<<" gcr_spectra_r_max    "<<  gcr_spectra_r_max       <<endl;
   cout<<" gcr_spectra_z_min    "<<  gcr_spectra_z_min       <<endl;
   cout<<" gcr_spectra_z_max    "<<  gcr_spectra_z_max       <<endl;
   cout<<" gcr_spectra_r_min    "<<  gcr_spectra_r_min       <<endl;
   int i;
   cout<<" gcr_spectra_Z        ";  for(i=0;i<gcr_spectra_n_ZA;i++)cout<< gcr_spectra_Z[i]<<" ";cout  <<endl;
   cout<<" gcr_spectra_A        ";  for(i=0;i<gcr_spectra_n_ZA;i++)cout<< gcr_spectra_A[i]<<" ";cout  <<endl;

   cout<<" gcr_spectra_Emin     "<<  gcr_spectra_Emin <<endl;
   cout<<" gcr_spectra_Emax     "<<  gcr_spectra_Emax <<endl;
   cout<<" gcr_spectra_Imin     "<<  gcr_spectra_Imin <<endl;
   cout<<" gcr_spectra_Imax     "<<  gcr_spectra_Imax <<endl;
   
   cout<<" gcr_spectra_ratio_min ";   for(i=0;i<gcr_spectra_n_ratio;i++)cout<<gcr_spectra_ratio_min[i]<<" ";cout<<endl;
   cout<<" gcr_spectra_ratio_max ";   for(i=0;i<gcr_spectra_n_ratio;i++)cout<<gcr_spectra_ratio_max[i]<<" ";cout<<endl;

   cout<<" gcr_spectra_mod_phi   ";   for(i=0;i<gcr_spectra_n_mod_phi;i++)cout<<gcr_spectra_mod_phi[i]<<" ";cout<<endl;

   cout<<" gcr_database_file     "<< gcr_database_file<<endl;

   cout<<" sync_index_nu         ";   for(i=0;i<sync_index_n_nu*2      ;i++)cout<<      sync_index_nu[i]<<" ";cout<<endl; //AWS20070524


   cout<<" sync_data____10MHz    " << sync_data____10MHz                                                          <<endl; //AWS20071221
   cout<<" sync_data____22MHz    " << sync_data____22MHz                                                          <<endl; //AWS20071221
   cout<<" sync_data____45MHz    " << sync_data____45MHz                                                          <<endl; //AWS20071221
   cout<<" sync_data___150MHz    " << sync_data___150MHz                                                          <<endl; //AWS20071221
   cout<<" sync_data___408MHz    " << sync_data___408MHz                                                          <<endl; //AWS20071221
   cout<<" sync_data___820MHz    " << sync_data___820MHz                                                          <<endl; //AWS20071221
   cout<<" sync_data__1420MHz    " << sync_data__1420MHz                                                          <<endl; //AWS20071221
   cout<<" sync_data__2326MHz    " << sync_data__2326MHz                                                          <<endl; //AWS20071221
   cout<<" sync_data_22800MHz    " << sync_data_22800MHz                                                          <<endl; //AWS20071221
   cout<<" sync_data_33000MHz    " << sync_data_33000MHz                                                          <<endl; //AWS20071221
   cout<<" sync_data_41000MHz    " << sync_data_41000MHz                                                          <<endl; //AWS20071221
   cout<<" sync_data_61000MHz    " << sync_data_61000MHz                                                          <<endl; //AWS20071221
   cout<<" sync_data_94000MHz    " << sync_data_94000MHz                                                          <<endl; //AWS20071221
   cout<<" sync_data_WMAP        " << sync_data_WMAP                                                              <<endl; //AWS20071221
   cout<<" sync_data_options     " << sync_data_options                                                           <<endl; //AWS20120110

   cout<<" free_free_options     " << free_free_options                                                           <<endl; //AWS20111222
   cout<<" spin_dust_options     " << spin_dust_options                                                           <<endl; //AWS20120117

   cout<<" luminosity            " << luminosity                                                                  <<endl; //AWS20100823
   cout<<" galdef_series         " << galdef_series                                                               <<endl; //AWS20100329
   cout<<" ISRF_luminosity_R     " << ISRF_luminosity_R                                                           <<endl; //AWS20100421
   cout<<" ISRF_luminosity_z     " << ISRF_luminosity_z                                                           <<endl; //AWS20100421
   cout<<" ISRF_read             " << ISRF_read                                                                   <<endl; //AWS20100818

   cout<<"  =================================="<<endl;

   
}












