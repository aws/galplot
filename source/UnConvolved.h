//energy integrated, NOT convolved model skymaps

#include"Distribution.h"
#include"HIH2IC.h"   //AWS20081208
#include"Sources.h"  //AWS20081208
#include"Skymap.h"   //AWS20081208

class UnConvolved
{
 public:

  Distribution  EGRET_bremss_skymap;
  Distribution *EGRET_IC_iso_skymap;   // 3 ISRF components
  Distribution *EGRET_IC_aniso_skymap; // 3 ISRF components //AWS20060907
  Distribution  EGRET_pi0_decay_skymap;
  Distribution  EGRET_isotropic_skymap;
  Distribution  EGRET_total_skymap;

  Distribution  EGRET_HIR_skymap; //AWS20041223
  Distribution  EGRET_H2R_skymap; //AWS20041223

  Distribution  EGRET_HIT_skymap; //AWS20050103
  Distribution  EGRET_H2T_skymap; //AWS20050103



  Distribution  GLAST_bremss_skymap; //AWS20080514
  Distribution *GLAST_IC_iso_skymap;   // 3 ISRF components
  Distribution *GLAST_IC_aniso_skymap; // 3 ISRF components 
  Distribution  GLAST_pi0_decay_skymap;
  Distribution  GLAST_isotropic_skymap;
  Distribution  GLAST_total_skymap;

  Distribution  GLAST_HIR_skymap; 
  Distribution  GLAST_H2R_skymap; 

  Distribution  GLAST_HIT_skymap; 
  Distribution  GLAST_H2T_skymap; 

// GLAST models from gardian
// has to be a pointer apparently (otherwise galplot.cc gives compile error: "no matching function for Galplot()" )
// something to do with how inheritance from BaseModel works ?
// JoinModel.cc also uses (a vector of) pointers to models
// HIH2IC      GLAST_model_HIH2IC; //AWS20080718  NO
   HIH2IC     *GLAST_model_HIH2IC; //AWS20080718 YES

   

   Skymap<double> GLAST_unconvolved_counts;              //AWS20080718
   Skymap<double> GLAST_unconvolved_counts_over_exposure;//AWS20080718

   Skymap<double> GLAST_unconvolved_counts_HI;           //AWS20080718
   Skymap<double> GLAST_unconvolved_counts_H2;           //AWS20080718
   Skymap<double> GLAST_unconvolved_counts_IC;           //AWS20080718
   Skymap<double> GLAST_unconvolved_counts_HII;          //AWS20080810

   Skymap<double> GLAST_unconvolved_counts_sources;      //AWS20081209
   Skymap<double> GLAST_unconvolved_intensity_sources;   //AWS20081212


   Skymap<double> GLAST_unconvolved_counts_sourcepop_sublimit; //AWS20090109
   Skymap<double> GLAST_unconvolved_counts_sourcepop_soplimit; //AWS20090109


   Skymap<double> GLAST_unconvolved_intensity_sourcepop_sublimit; //AWS20090109
   Skymap<double> GLAST_unconvolved_intensity_sourcepop_soplimit; //AWS20090109

   Skymap<double> GLAST_unconvolved_counts_solar_IC;        //AWS20100215
   Skymap<double> GLAST_unconvolved_intensity_solar_IC;     //AWS20100215

   Skymap<double> GLAST_unconvolved_counts_solar_disk;      //AWS20100215
   Skymap<double> GLAST_unconvolved_intensity_solar_disk;   //AWS20100215

};
