
//**.****|****.****|****.****|****.****|****.****|****.****|****.****|****.****|
// * read_IC_skymap.cc *                           galprop package * 4/14/2000 
//**"****!****"****!****"****!****"****!****"****!****"****!****"****!****"****|
#include"Galplot.h"                    //AWS20050920
#include"galprop_classes.h"
#include"galprop.h"

#include "fitsio.h" 
int  Galplot::read_IC_skymap(char *IC_type)
{
int stat;

 char   comment[100];

  cout<<" >>>> read_IC_skymap"<<endl;

stat=0;
   fitsfile *fptr;       /* pointer to the FITS file; defined in fitsio.h */
    int status, ii, jj;
    status=0;//AWS20100616
    long  fpixel = 1, naxis = 4, nelements, exposure;
    long naxes[5]  ; 

  
     naxis=4;
     naxes[0]=galaxy.n_long;
     naxes[1]=galaxy.n_lat;
     naxes[2]=galaxy.n_E_gammagrid;
     naxes[3]=galaxy.n_ISRF_components;

     nelements=naxes[0]*naxes[1]*naxes[2]*naxes[3];
    

  
    
    float *array;          
    array=new float[nelements];

    char  infile[100];
  
    if(galdef.skymap_format==0) //AWS20100616
    {

    strcpy( infile,configure.fits_directory);
 
//  strcat( infile,"ics_isotropic_skymap_"); // this has total 

    if(strcmp(IC_type,"isotropic")  ==0)
    strcat( infile,"ics_skymap_comp_");                   // for each ISRF component
 
    if(strcmp(IC_type,"anisotropic")==0) 
    strcat( infile,"ics_anisotropic_skymap_comp_");       // for each ISRF component


    strcat( infile,galdef.galdef_ID);
    cout<<"  reading  ICS isotropic skymap  from file "<<infile<<endl;

    status = 0;         /* initialize status before calling fitsio routines */
    fits_open_file(&fptr,  infile,READONLY, &status);   
    cout<<"  fits open status = "<<status<<endl;



    /* Read the array of floats  */
    float nulval=0;
    int anynul;
    fits_read_img(fptr, TFLOAT, fpixel, nelements, &nulval,array, &anynul,&status);
 
  

  

   int i=0; 
   for (int i_comp   =0;    i_comp<naxes[3];   i_comp++)
   for (int ip       =0;        ip<naxes[2];       ip++)
   for (int ib       =0;        ib<naxes[1];       ib++)
   for (int il       =0;        il<naxes[0];       il++)
         {             
              // cout<<array[i]<<endl;
               if(strcmp(IC_type,"isotropic")  ==0) galaxy.IC_iso_skymap  [i_comp].d2[il][ib].s[ip]=array[i];
               if(strcmp(IC_type,"anisotropic")==0) galaxy.IC_aniso_skymap[i_comp].d2[il][ib].s[ip]=array[i];
            
              //array[i]*=pow(galaxy.E_gamma[ip],2);
            i++;
         }


    //galaxy.IC_iso_skymap[i_comp] .print();

    fits_close_file(fptr, &status);            /* close the file */

    fits_report_error(stderr, status);  /* print out any error messages */


    delete[] array; //AWS20010216

    } // skymap_format==0

  //---------------------------------------------------------------------------------

    // healpix galprop files


    
   if(galdef.skymap_format==3)
   {
    //                 this works but why is *[3] wrong syntax ?
    galaxy.IC_iso_hp_skymap=new Skymap<double>[3]; // array of 3 pointers to Skymaps (since it's done this way in Galaxy.h from galprop)

    for (int i_comp=0;i_comp<=2;i_comp++)
    { 
    strcpy( infile,configure.fits_directory);
    
    if(i_comp==0)
    strcat( infile,"ics_isotropic_comp_1_healpix_"); 
    if(i_comp==1)
    strcat( infile,"ics_isotropic_comp_2_healpix_");
    if(i_comp==2)
    strcat( infile,"ics_isotropic_comp_3_healpix_");

    strcat( infile,galdef.galdef_ID);


    string mapname;
    mapname=infile;
    cout<<"  reading ics for i_comp= "<<i_comp<<" healpix skymap  from file "<<mapname<<endl;



    galaxy.IC_iso_hp_skymap[i_comp].load(mapname); 

    //    if(galplotdef.verbose==-2003-i_comp){cout<<" ics for  i_comp= "<<i_comp<<" hp_skymap  "<<endl;       galaxy.IC_iso_hp_skymap[i_comp].print(cout);}
    } //AWS20091204
   }

    


 //---------------------------------------------------------------------------------

  cout<<" <<<< read_IC_skymap"<<endl;

    return( status );
return stat;
}
