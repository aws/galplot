
//**.****|****.****|****.****|****.****|****.****|****.****|****.****|****.****|
// * plot_SPI_spectrum_spimodfit.cc *                             
//**"****!****"****!****"****!****"****!****"****!****"****!****"****!****"****|
#include"Galplot.h"                    //AWS20050920
#include"galprop_classes.h"
#include"galprop.h"
#include"constants.h"
#include"fitsio.h"
#include"SPIERSP.h"
//#include"CAT.h"

//int analyse_MCMC_SPI_spimodfit();
//int fit_spectrum_SPI();
//int exclude_source_(CAT cat,char  **par_id, int i_component);// AWS20050630
/////////////////////////////////////////////////////////////////////////
namespace SPI_spimodfit//  to protect local variables and share with local routines
{
   int status=0;

   double spimodfit_version;
   int test_comparison;

 
   int n_energy;
   double *emin,*emax,*flux,*dflux;
   double emean,ymin,ymax,y,ymean;
   double solid_angle,dtr;
   int i_energy;
   int i_ebds; //AWS20070302
   int color;
   
   TLine *line;


char  infile[1000];
 char comment[400];
 fitsfile *fptr;
 int hdunum,hdutype,total_hdus,colnum;

 int naxis1,naxis2;
 int n_columns,n_rows;

 int felement=1;

 int anynul;
 double nulval=0.;
 int typecode;
 long repeat,width;

 int i,il,ib,ilmin,ilmax,ibmin,ibmax;
 double delta_longitude;

 int n_theta,i_theta;
 double *theta_ML,*theta_MC;
 double *cov_ML,*cov_MC;
 double *flux_ref;
 double *energy_cor;  //AWS20070302
 double *map;
 int n_chi,n_psi;

 int n_component,i_component,j_component;
 int n_map_component,n_source_component;
 int i_component1,i_component2;

 double e_min,e_max,e_mean;
 double shift;

 int N_theta_chain, n_accepted;   // as in spidiffit MCMC.h
 float  *theta_chain, *logL_chain;// as in spidiffit MCMC.h
 int i_sample_chain, i_theta_chain,i_chain;

 FITS model_map;
 double *intensity,dintensity,intensity_total;
 double  dintensity_ML, dintensity_MC;
 double *component_average;
 double sigma;
 int exclude_source,exclude_source_i,exclude_source_j;

 double *energy_to_fit,*intensity_to_fit,*dintensity_to_fit;
 double *energy_to_fit_min,*energy_to_fit_max;
 
 int  *n_significant_sources;
 double *significance_levels;
 int   n_significance_levels, i_significance_levels;
 int bin_count;
 int region;
 int id;
 int *idlist;
 int nidlist;
 int iid;
 int idlistN;
 int iplot;
 int energy_select1,energy_select2,select_energy;
 int i_energy1,i_energy2;
 int viewdiffit_flag;

   CAT cat;
   double ra_source,dec_source,l_source,b_source;
}

static void fcn_chisq        (int& npar,double*gin,double &f,double *par,int iflag);
static void fcn_chisq_deconv (int& npar,double*gin,double &f,double *par,int iflag);
static void fcn_chisq_deconv2(int& npar,double*gin,double &f,double *par,int iflag);

/////////////////////////////////////////////////////////////
int Galplot::plot_SPI_spectrum_spimodfit(int mode)
///////////////////////////////////////////////////////////
{
  using namespace SPI_spimodfit;

   cout<<" >>>> plot_SPI_spectrum_spimodfit    "<<endl;



   /////////////////////////////////////////////////////////////

/*------------------------------------------------------------------------------------------------

Storage of spidiffit parameters and maps.
========================================
Convention here: i_energy, i_component start at 0
(NB cf spidiffit.pro:hdunum=1+extension number, i_energy i_component start at 1)


spidiffit version 21:

 primary header
 grouping extension

 for all energies
|   theta_ML
|     cov_ML
|   theta_MC
|     cov_MC


 
  for all  components
|   for all energies
| |      map       (ML or MC according to spidiffit.par)
|    for all energies
| |      map_error              ""

 total
  for all energies
       map                    ""
  for all energies
       map_error              ""



theta_ML: hdunum= 2 + i_energy*4 + 1
  cov_ML: hdunum= 2 + i_energy*4 + 2
theta_MC: hdunum= 2 + i_energy*4 + 3
  cov_MC: hdunum= 2 + i_energy*4 + 4

  map   : hdunum= 3 + n_energy*4 + i_energy   +i_map_component * n_energy*2


  n_component=n_map_component + n_source_component

  total headers=2+ n_energy*4 +    n_energy  * n_map_component * n_energy*2 + n_energy*2
              = 2+ n_energy*(6+n_map_component *2)

   n_energy=  (total headers -2)/(6+n_map_component *2)

......................................................................
spidiffit version 22:

 primary header
 grouping extension

 for all energies
|    theta_ML
|      cov_ML
|    theta_MC
|      cov_MC
| theta_chain
|  logL_chain

then maps as version 21
  n_energy=  (total headers -2)/(8+n_map_component *2)
theta_ML: hdunum= 2 + i_energy*6 + 1
  cov_ML: hdunum= 2 + i_energy*6 + 2
theta_MC: hdunum= 2 + i_energy*6 + 3
  cov_MC: hdunum= 2 + i_energy*6 + 4

  map   : hdunum= 3 + n_energy*6 + i_energy   +i_map_component * n_energy*2

.......................................................................

spimodfit version 2.1

 primary header
 grouping extension


 for all components
| input skymaps
  
 energy bounds

 for all energies
| ML   results
| |for all parameters
| | name ....... value....  covariance
| | skymodels
| | sources
| | background
|
| MCMC chain results
|
| total ML   map
| total MCMC map 



------------------------------------------------------------------------------------------------- */
 
 
 //-----------------------------------------------------------------------

 viewdiffit_flag=0;// 1=avoid negative theta_MC as in viewdiffit17.pro



 select_energy=1;

 n_significance_levels=10;
 n_significant_sources=new    int[n_significance_levels];
 significance_levels  =new double[n_significance_levels];
 significance_levels[0]=1.0;
 significance_levels[1]=2.0;
 significance_levels[2]=3.0;
 significance_levels[3]=4.0;
 significance_levels[4]=5.0;
 significance_levels[5]=10.;
 significance_levels[6]=20.;
 significance_levels[7]=50.;
 significance_levels[8]=100.;
 significance_levels[9]=200.;

 cout<<"galplotdef.verbose="<<galplotdef.verbose<<endl;
 

 int  idlist201[20]={2008,2009,2013,2014   }    ;                    int nidlist201= 4;
 int  idlist202[20]={2008,2015,2016,2017,2018,2019,2021,2022   } ;   int nidlist202= 8;
 int  idlist203[20]={     2015,2016,2017,2018,2019,2021,2022,2023,2024,2025,2026,2027,2028,2041,2042  } ;   int nidlist203=15;// 1 day background
 // int  idlist204[20]={2029,2030,2031,2032,2033,2035,2036,2037,2038,2039,2040,2043,2044                 } ;   int nidlist204=13;// 1 pointing background
 //     int  idlist204[20]={2046,2049,2053,2050,2060,2051 ,2058                          } ;   int nidlist204= 7;// 1 pointing background
  int  idlist204[20]={2046,2072,2055,2054,  2049,2073,2057,2056,  2050,2074,2060,   2051,2075,2059,2058  } ;   int nidlist204=15;// 1 pointing background sigma=0,1,2,3
  
  int  idlist205[50]={2079,2081,2087,2089,   2082,2090,2091,2092, 2083,2093,2094,2095,   2084,2096,2097,2098, 
                      2085,2100,2101,2102,  2086,2103,2104,2105,  2106,2112,2113,2114,
                      2107,2115,2116,2117,2108,     2119,2120,2109,2121,2122,2110,2123,2124,2111,2125,2126} ; 
  int nidlist205=44;// spimodfit2.4 1 pointing background sigma=0,1,2,3
  
  int  idlist206[50]={2087,2090,2093,2096,                
                           2100,2103,2129,2132,2112,             
                           2115,2118,2121,2123,2125     } ;    int nidlist206=14;// spimodfit2.4 1 pointing background sigma=1

  int  idlist207[50]={2088,2091,2094,2097,                
                           2101,2104,2130,2133,2113,             
                           2116,2119,2122,2124,2126     } ;    int nidlist207=14;// spimodfit2.4 1 pointing background sigma=2

  /*
  int  idlist208[50]={2089,2092,2095,2098,                
                           2102,2105,2131,2134,2114,             
                           2117,2120,2122,2124,2126     } ;    int nidlist208=14;// spimodfit2.4 1 pointing background sigma=3
  */
  int  idlist208[50]={2089,2092,2095,2098,                
                           2102,          2134,2114,             
                           2117,2120,2122,2124,2126     } ;    int nidlist208=12;// spimodfit2.4 1 pointing background sigma=3  68-78 78-88 removed



  int  idlist209[50]={2087,2136,2135,2137,2090,2138,2093,2139,2096,2140,2100,2141,2103,2142,2129,2143,2132,2144,
                      2112,2145};     int nidlist209=20;// spimodfit2.4 1 pointing background sigma=1 source const,50,100  200d

  int  idlist210[50]={2147,2148,2149,2150,2151,2152,2155,2158,2084}; int nidlist210=9;//gnrl cat 0020 ibis selected 203 sources

  int  idlist211[50]={2160,2161,2162,2163,2164,2165,2166,2167,2168,2184,2169,2170,2185}; int nidlist211=13;//gnrl cat 0020 ibis selected 203 sources Continuum20050407

  int  idlist212[50]={2089,2172,2173,2174,2187,2175,2188,2176,2177,2178,2179,2180,2181,2182,2183,2186}; int nidlist212=16;//gnrl cat 0020 ibis selected 203 sources Continuum20050407 3sigma     #2089 from list 208# compare 2174/2187 2175/2188 difference sources

  int  idlist213[50]={     2172,2173,     2187,     2188,2176,2177,2178,2179,2180,2181,2182,2183,2186}; int nidlist213=13;//gnrl cat 0020 ibis selected 203 sources Continuum20050407 3sigma     #2089 removed (from list 208)# 2187,2188 using sources from 2161 instead

  int  idlist214[50]={     2172,2173,     2187,     2188,2176,2177,2178,2179,2180,2181,2182,2183, 2207,2186}; int nidlist214=14;//gnrl cat 0020 ibis selected 203 sources Continuum20050407 3sigma     #2089 removed (from list 208)# 2187,2188 using sources from 2161 instead; 2207=511 line

  int  idlist215[50]={ 2238,2239,2230,2251,2252,2233,2234,2235,2244,2245,2247,2248,2249,2183,2207,2186}; int nidlist215=16;// 2nd IBIS cat 3 sigma 2207=511 line Continuum20050407  replaced 2246 by 2244,2245 (100-200keV divided in 2)

  int  idlist216[50]={ 2281,2282,2283,2294,2295,2286,2287,2288,2289,2290,2291,2248,2249,2183,2207,2186}; int nidlist216=16;// 2nd IBIS catalogue revised 3sigma (2248+ kept since rerun not required)                         2251 2252 to be replaced by 2294 2295 when ready


  int  idlist217[50]={ 2261,2262,2263,2264,2265,2266,2267,2268,2269,2270,2271,2272,2273,2274          }; int nidlist217=14;// 2nd IBIS catalogue revised; all sources


  int  idlist218[50]={ 2281,2301,2302,2303,2304,2305,2306,2307,2308,2309,2311,2317,        2282,2314,2315,  2283,2320,2321,2322          }; int nidlist218=19;// 2nd IBIS catalogue revised - time variability

  int  idlist219[50]={2332,2334,2336          }; int nidlist219=3; // time var 3 sources (separate since otherwise too much memory)
  int  idlist220[50]={               2337,2338}; int nidlist220=2; // time var 3 sources (separate since otherwise too much memory)

  int  idlist221[50]={ 2341,2342,2343,2344,2345,2346,2347,2348,2349,2350,2351,2352,2249,2183,2207,2186}; int nidlist221=16;// 2nd IBIS catalogue revised 1 sigma
  int  idlist222[50]={ 2361,2362,2363,2364,2365,2366,2367,2368,2369,2370,2371,2372,2249,2183,2207,2186}; int nidlist222=16;// 2nd IBIS catalogue revised 2 sigma

  int  idlist223[50]={      2382,2383,2384,2385,2386,2387,2388,2389,2390                               }; int nidlist223= 9;// 2nd IBIS catalogue all sources expanded catalogues
  int  idlist224[50]={2481, 2482,2483,2484,2485,2486,2487,2488,2489,2490, 2291,2248,2249,2183,2207,2186}; int nidlist224=16;// 2nd IBIS cat expanded 3sigma
  int  idlist225[50]={2441, 2442,2443,2444,2445,2446,2447,2448,2449,2450, 2351,2352,2249,2183,2207,2186}; int nidlist225=16;// 2nd IBIS cat expanded 1sigma
  int  idlist226[50]={2461, 2462,2463,2464,2465,2466,2467,2468,2469,2470, 2371,2372,2249,2183,2207,2186}; int nidlist226=16;// 2nd IBIS cat expanded 2sigma

  int  idlist227[50]={2501, 2502,2503,2504,2505                                         }; int nidlist227= 5;// 2nd IBIS cat expanded 3sigma time var 18-28 keV
  int  idlist228[50]={2511, 2512,2513,2514,2515                                         }; int nidlist228= 5;// 2nd IBIS cat expanded 3sigma time var 28-38 keV
  int  idlist229[50]={2521, 2522,2523,     2525                                         }; int nidlist229= 4;// 2nd IBIS cat expanded 3sigma time var 38-48 keV
  int  idlist230[50]={2531, 2532,2533,     2535                                         }; int nidlist230= 4;// 2nd IBIS cat expanded 3sigma time var 48-58 keV
  int  idlist231[50]={2541, 2542,2543                                                   }; int nidlist231= 3;// 2nd IBIS cat expanded 3sigma time var 58-68 keV

  //  int  idlist232[50]={3001,3002,3003,3004,3005,3006,     3008,3009,3010,     3012,3013,3014,3015,3016,3017,3018,3019,3020 }; int nidlist232=16;//      Petry rev 15-494
  int  idlist232[50]={3001,3002,3003,3004,3005,3006,3007,3008,3009,3010,3011,3012,3013,3014,3015,3016,3017,3018,3020,3021,3022 }; int nidlist232=22;//      Petry rev 15-494
  int  idlist233[50]={3101,3102,3103,3104,3105,3106,3107,3108,3109,3110,3111,3112,3113,3114,3115,3116,3117,3118,3119,3120,3121,3122}; int nidlist233=21;//      Petry rev 15-254

 idlistN=galplotdef.data_INTEGRAL;     
 

 if(idlistN== 201){  idlist=idlist201;  nidlist=nidlist201;}
 if(idlistN== 202){  idlist=idlist202;  nidlist=nidlist202;}
 if(idlistN== 203){  idlist=idlist203;  nidlist=nidlist203;}
 if(idlistN== 204){  idlist=idlist204;  nidlist=nidlist204;}
 if(idlistN== 205){  idlist=idlist205;  nidlist=nidlist205;}
 if(idlistN== 206){  idlist=idlist206;  nidlist=nidlist206;}
 if(idlistN== 207){  idlist=idlist207;  nidlist=nidlist207;}
 if(idlistN== 208){  idlist=idlist208;  nidlist=nidlist208;}
 if(idlistN== 209){  idlist=idlist209;  nidlist=nidlist209;}
 if(idlistN== 210){  idlist=idlist210;  nidlist=nidlist210;}
 if(idlistN== 211){  idlist=idlist211;  nidlist=nidlist211;}
 if(idlistN== 212){  idlist=idlist212;  nidlist=nidlist212;}
 if(idlistN== 213){  idlist=idlist213;  nidlist=nidlist213;}
 if(idlistN== 214){  idlist=idlist214;  nidlist=nidlist214;}
 if(idlistN== 215){  idlist=idlist215;  nidlist=nidlist215;}
 if(idlistN== 216){  idlist=idlist216;  nidlist=nidlist216;}
 if(idlistN== 217){  idlist=idlist217;  nidlist=nidlist217;}
 if(idlistN== 218){  idlist=idlist218;  nidlist=nidlist218;}
 if(idlistN== 219){  idlist=idlist219;  nidlist=nidlist219;}
 if(idlistN== 220){  idlist=idlist220;  nidlist=nidlist220;}
 if(idlistN== 221){  idlist=idlist221;  nidlist=nidlist221;}
 if(idlistN== 222){  idlist=idlist222;  nidlist=nidlist222;}
 if(idlistN== 223){  idlist=idlist223;  nidlist=nidlist223;}
 if(idlistN== 224){  idlist=idlist224;  nidlist=nidlist224;}
 if(idlistN== 225){  idlist=idlist225;  nidlist=nidlist225;}
 if(idlistN== 226){  idlist=idlist226;  nidlist=nidlist226;}
 if(idlistN== 227){  idlist=idlist227;  nidlist=nidlist227;}
 if(idlistN== 228){  idlist=idlist228;  nidlist=nidlist228;}
 if(idlistN== 229){  idlist=idlist229;  nidlist=nidlist229;}
 if(idlistN== 230){  idlist=idlist230;  nidlist=nidlist230;}
 if(idlistN== 231){  idlist=idlist231;  nidlist=nidlist231;}
 if(idlistN== 232){  idlist=idlist232;  nidlist=nidlist232;}
 if(idlistN== 233){  idlist=idlist233;  nidlist=nidlist233;}

  intensity_to_fit     =new double[nidlist];
 dintensity_to_fit     =new double[nidlist];
     energy_to_fit     =new double[nidlist];
     energy_to_fit_min =new double[nidlist];
     energy_to_fit_max =new double[nidlist];




   cat.read(configure.fits_directory);
   cat.print();

   // cat.find("Crab",&ra_source,&dec_source,&l_source,&b_source);cout<<"test Crab source: ra="<<ra_source<<" dec="<<dec_source<<" l="<<l_source<<" b="<<b_source<<endl;


//      txtFILE is in global, and opened in plot_spectrum

fprintf(txtFILE,"======================================================= \n");     
fprintf(txtFILE,"\n INTEGRAL/SPI spimodfit results for list %d \n",idlistN);

fprintf(txtFILE,"\n");
fprintf(txtFILE,"\\caption{SPI diffuse emission spectrum for \n");

fprintf(txtFILE,"$%6.2f\\deg<l<%6.2f\\deg$,    ",galplotdef.long_min1,galplotdef.long_max1);
fprintf(txtFILE,"$%6.2f\\deg<l<%6.2f\\deg$,\n  ",galplotdef.long_min2,galplotdef.long_max2);
fprintf(txtFILE,"$%6.2f\\deg<b<%6.2f\\deg$,    ",galplotdef. lat_min1,galplotdef. lat_max1);
fprintf(txtFILE,"$%6.2f\\deg<b<%6.2f\\deg$\n ",  galplotdef. lat_min2,galplotdef. lat_max2);
fprintf(txtFILE,".   list %d",idlistN);
fprintf(txtFILE,"}");
fprintf(txtFILE,"\n");


   for(iid=0;iid<nidlist;iid++)
{

  id=idlist [iid];
 
 strcpy( infile,configure.fits_directory);

 energy_select1=0;
 spimodfit_version=2.1;

 //                                      NB users energy selection index  starting at 1 

  //scaling OK:
 if(id==2008){strcat( infile,"results.spimodfit.2008.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 // scaling varies with the following 3
 if(id==2009){strcat( infile,"results.spimodfit.2009.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2013){strcat( infile,"results.spimodfit.2013.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2014){strcat( infile,"results.spimodfit.2014.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 //scaling OK:
 if(id==2015){strcat( infile,"results.spimodfit.2015.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2016){strcat( infile,"results.spimodfit.2016.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2017){strcat( infile,"results.spimodfit.2017.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2018){strcat( infile,"results.spimodfit.2018.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2019){strcat( infile,"results.spimodfit.2019.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 

 if(id==2020){strcat( infile,"results.spimodfit.2020.fits"   );   energy_select1=1; energy_select2=6;color=kBlue; } 
 if(id==2021){strcat( infile,"results.spimodfit.2021.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2022){strcat( infile,"results.spimodfit.2022.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2023){strcat( infile,"results.spimodfit.2023.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2024){strcat( infile,"results.spimodfit.2024.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2025){strcat( infile,"results.spimodfit.2025.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2026){strcat( infile,"results.spimodfit.2026.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2027){strcat( infile,"results.spimodfit.2027.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2028){strcat( infile,"results.spimodfit.2028.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2029){strcat( infile,"results.spimodfit.2029.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 

 if(id==2030){strcat( infile,"results.spimodfit.2030.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2031){strcat( infile,"results.spimodfit.2031.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2032){strcat( infile,"results.spimodfit.2032.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2033){strcat( infile,"results.spimodfit.2033.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2034){strcat( infile,"results.spimodfit.2034.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2035){strcat( infile,"results.spimodfit.2035.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2036){strcat( infile,"results.spimodfit.2036.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2037){strcat( infile,"results.spimodfit.2037.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2038){strcat( infile,"results.spimodfit.2038.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2039){strcat( infile,"results.spimodfit.2039.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 

 if(id==2040){strcat( infile,"results.spimodfit.2040.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2041){strcat( infile,"results.spimodfit.2041.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2042){strcat( infile,"results.spimodfit.2042.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2043){strcat( infile,"results.spimodfit.2043.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2044){strcat( infile,"results.spimodfit.2044.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 

 if(id==2046){strcat( infile,"results.spimodfit.2046.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2047){strcat( infile,"results.spimodfit.2047.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2049){strcat( infile,"results.spimodfit.2049.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2050){strcat( infile,"results.spimodfit.2050.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2051){strcat( infile,"results.spimodfit.2051.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2052){strcat( infile,"results.spimodfit.2052.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2053){strcat( infile,"results.spimodfit.2053.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2054){strcat( infile,"results.spimodfit.2054.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2055){strcat( infile,"results.spimodfit.2055.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2056){strcat( infile,"results.spimodfit.2056.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2057){strcat( infile,"results.spimodfit.2057.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2058){strcat( infile,"results.spimodfit.2058.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2059){strcat( infile,"results.spimodfit.2059.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2060){strcat( infile,"results.spimodfit.2060.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2061){strcat( infile,"results.spimodfit.2061.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2062){strcat( infile,"results.spimodfit.2062.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2063){strcat( infile,"results.spimodfit.2063.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2064){strcat( infile,"results.spimodfit.2064.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2065){strcat( infile,"results.spimodfit.2065.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2066){strcat( infile,"results.spimodfit.2066.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2067){strcat( infile,"results.spimodfit.2067.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2068){strcat( infile,"results.spimodfit.2068.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2069){strcat( infile,"results.spimodfit.2069.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2070){strcat( infile,"results.spimodfit.2070.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2071){strcat( infile,"results.spimodfit.2071.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2072){strcat( infile,"results.spimodfit.2072.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2073){strcat( infile,"results.spimodfit.2073.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2074){strcat( infile,"results.spimodfit.2074.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2075){strcat( infile,"results.spimodfit.2075.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2076){strcat( infile,"results.spimodfit.2076.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2077){strcat( infile,"results.spimodfit.2077.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2078){strcat( infile,"results.spimodfit.2078.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2079){strcat( infile,"results.spimodfit.2079.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2080){strcat( infile,"results.spimodfit.2080.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2081){strcat( infile,"results.spimodfit.2081.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2082){strcat( infile,"results.spimodfit.2082.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2083){strcat( infile,"results.spimodfit.2083.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2084){strcat( infile,"results.spimodfit.2084.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2085){strcat( infile,"results.spimodfit.2085.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2086){strcat( infile,"results.spimodfit.2086.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2087){strcat( infile,"results.spimodfit.2087.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2088){strcat( infile,"results.spimodfit.2088.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2089){strcat( infile,"results.spimodfit.2089.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2090){strcat( infile,"results.spimodfit.2090.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2091){strcat( infile,"results.spimodfit.2091.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2092){strcat( infile,"results.spimodfit.2092.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2093){strcat( infile,"results.spimodfit.2093.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2094){strcat( infile,"results.spimodfit.2094.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2095){strcat( infile,"results.spimodfit.2095.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2096){strcat( infile,"results.spimodfit.2096.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2097){strcat( infile,"results.spimodfit.2097.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2098){strcat( infile,"results.spimodfit.2098.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2099){strcat( infile,"results.spimodfit.2099.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2100){strcat( infile,"results.spimodfit.2100.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2101){strcat( infile,"results.spimodfit.2101.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2102){strcat( infile,"results.spimodfit.2102.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2103){strcat( infile,"results.spimodfit.2103.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2104){strcat( infile,"results.spimodfit.2104.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2105){strcat( infile,"results.spimodfit.2105.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2106){strcat( infile,"results.spimodfit.2106.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2107){strcat( infile,"results.spimodfit.2107.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2108){strcat( infile,"results.spimodfit.2108.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2109){strcat( infile,"results.spimodfit.2109.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2110){strcat( infile,"results.spimodfit.2110.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2111){strcat( infile,"results.spimodfit.2111.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2112){strcat( infile,"results.spimodfit.2112.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2113){strcat( infile,"results.spimodfit.2113.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2114){strcat( infile,"results.spimodfit.2114.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2115){strcat( infile,"results.spimodfit.2115.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2116){strcat( infile,"results.spimodfit.2116.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2117){strcat( infile,"results.spimodfit.2117.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2118){strcat( infile,"results.spimodfit.2118.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2119){strcat( infile,"results.spimodfit.2119.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2120){strcat( infile,"results.spimodfit.2120.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2121){strcat( infile,"results.spimodfit.2121.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2122){strcat( infile,"results.spimodfit.2122.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2123){strcat( infile,"results.spimodfit.2123.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2124){strcat( infile,"results.spimodfit.2124.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2125){strcat( infile,"results.spimodfit.2125.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2126){strcat( infile,"results.spimodfit.2126.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2127){strcat( infile,"results.spimodfit.2127.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2128){strcat( infile,"results.spimodfit.2128.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2129){strcat( infile,"results.spimodfit.2129.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2130){strcat( infile,"results.spimodfit.2130.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2131){strcat( infile,"results.spimodfit.2131.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2132){strcat( infile,"results.spimodfit.2132.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2133){strcat( infile,"results.spimodfit.2133.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2134){strcat( infile,"results.spimodfit.2134.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2135){strcat( infile,"results.spimodfit.2135.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2136){strcat( infile,"results.spimodfit.2136.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2137){strcat( infile,"results.spimodfit.2137.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2138){strcat( infile,"results.spimodfit.2138.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2139){strcat( infile,"results.spimodfit.2139.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2140){strcat( infile,"results.spimodfit.2140.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2141){strcat( infile,"results.spimodfit.2141.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2142){strcat( infile,"results.spimodfit.2142.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2143){strcat( infile,"results.spimodfit.2143.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2144){strcat( infile,"results.spimodfit.2144.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2145){strcat( infile,"results.spimodfit.2145.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2146){strcat( infile,"results.spimodfit.2146.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2147){strcat( infile,"results.spimodfit.2147.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2148){strcat( infile,"results.spimodfit.2148.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2149){strcat( infile,"results.spimodfit.2149.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2150){strcat( infile,"results.spimodfit.2150.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2151){strcat( infile,"results.spimodfit.2151.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2152){strcat( infile,"results.spimodfit.2152.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2153){strcat( infile,"results.spimodfit.2153.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2154){strcat( infile,"results.spimodfit.2154.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2155){strcat( infile,"results.spimodfit.2155.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2156){strcat( infile,"results.spimodfit.2156.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2157){strcat( infile,"results.spimodfit.2157.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2158){strcat( infile,"results.spimodfit.2158.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2159){strcat( infile,"results.spimodfit.2159.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2160){strcat( infile,"results.spimodfit.2160.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2161){strcat( infile,"results.spimodfit.2161.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2162){strcat( infile,"results.spimodfit.2162.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2163){strcat( infile,"results.spimodfit.2163.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2164){strcat( infile,"results.spimodfit.2164.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2165){strcat( infile,"results.spimodfit.2165.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2166){strcat( infile,"results.spimodfit.2166.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2167){strcat( infile,"results.spimodfit.2167.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2168){strcat( infile,"results.spimodfit.2168.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2169){strcat( infile,"results.spimodfit.2169.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2170){strcat( infile,"results.spimodfit.2170.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2171){strcat( infile,"results.spimodfit.2171.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2172){strcat( infile,"results.spimodfit.2172.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2173){strcat( infile,"results.spimodfit.2173.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2174){strcat( infile,"results.spimodfit.2174.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2175){strcat( infile,"results.spimodfit.2175.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2176){strcat( infile,"results.spimodfit.2176.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2177){strcat( infile,"results.spimodfit.2177.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2178){strcat( infile,"results.spimodfit.2178.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2179){strcat( infile,"results.spimodfit.2179.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2180){strcat( infile,"results.spimodfit.2180.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2181){strcat( infile,"results.spimodfit.2181.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2182){strcat( infile,"results.spimodfit.2182.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2183){strcat( infile,"results.spimodfit.2183.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2184){strcat( infile,"results.spimodfit.2184.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2185){strcat( infile,"results.spimodfit.2185.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2186){strcat( infile,"results.spimodfit.2186.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2187){strcat( infile,"results.spimodfit.2187.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2188){strcat( infile,"results.spimodfit.2188.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2189){strcat( infile,"results.spimodfit.2189.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2207){strcat( infile,"results.spimodfit.2207.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2230){strcat( infile,"results.spimodfit.2230.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2231){strcat( infile,"results.spimodfit.2231.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2232){strcat( infile,"results.spimodfit.2232.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2233){strcat( infile,"results.spimodfit.2233.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2234){strcat( infile,"results.spimodfit.2234.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2235){strcat( infile,"results.spimodfit.2235.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2238){strcat( infile,"results.spimodfit.2238.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2239){strcat( infile,"results.spimodfit.2239.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2244){strcat( infile,"results.spimodfit.2244.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2245){strcat( infile,"results.spimodfit.2245.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2246){strcat( infile,"results.spimodfit.2246.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2247){strcat( infile,"results.spimodfit.2247.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2248){strcat( infile,"results.spimodfit.2248.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2249){strcat( infile,"results.spimodfit.2249.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2251){strcat( infile,"results.spimodfit.2251.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2252){strcat( infile,"results.spimodfit.2252.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 

 if(id==2261){strcat( infile,"results.spimodfit.2261.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2262){strcat( infile,"results.spimodfit.2262.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2263){strcat( infile,"results.spimodfit.2263.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2264){strcat( infile,"results.spimodfit.2264.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2265){strcat( infile,"results.spimodfit.2265.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2266){strcat( infile,"results.spimodfit.2266.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2267){strcat( infile,"results.spimodfit.2267.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2268){strcat( infile,"results.spimodfit.2268.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2269){strcat( infile,"results.spimodfit.2269.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2270){strcat( infile,"results.spimodfit.2270.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2271){strcat( infile,"results.spimodfit.2271.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2272){strcat( infile,"results.spimodfit.2272.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2273){strcat( infile,"results.spimodfit.2273.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2274){strcat( infile,"results.spimodfit.2274.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 

 if(id==2281){strcat( infile,"results.spimodfit.2281.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2282){strcat( infile,"results.spimodfit.2282.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2283){strcat( infile,"results.spimodfit.2283.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2284){strcat( infile,"results.spimodfit.2284.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2285){strcat( infile,"results.spimodfit.2285.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2286){strcat( infile,"results.spimodfit.2286.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2287){strcat( infile,"results.spimodfit.2287.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2288){strcat( infile,"results.spimodfit.2288.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2289){strcat( infile,"results.spimodfit.2289.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2290){strcat( infile,"results.spimodfit.2290.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2291){strcat( infile,"results.spimodfit.2291.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2292){strcat( infile,"results.spimodfit.2292.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2293){strcat( infile,"results.spimodfit.2293.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2294){strcat( infile,"results.spimodfit.2294.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2295){strcat( infile,"results.spimodfit.2295.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 

 if(id==2301){strcat( infile,"results.spimodfit.2301.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2302){strcat( infile,"results.spimodfit.2302.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2303){strcat( infile,"results.spimodfit.2303.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2304){strcat( infile,"results.spimodfit.2304.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2305){strcat( infile,"results.spimodfit.2305.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2306){strcat( infile,"results.spimodfit.2306.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2307){strcat( infile,"results.spimodfit.2307.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2308){strcat( infile,"results.spimodfit.2308.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2309){strcat( infile,"results.spimodfit.2309.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2310){strcat( infile,"results.spimodfit.2310.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2311){strcat( infile,"results.spimodfit.2311.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2312){strcat( infile,"results.spimodfit.2312.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2313){strcat( infile,"results.spimodfit.2313.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2314){strcat( infile,"results.spimodfit.2314.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2315){strcat( infile,"results.spimodfit.2315.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2316){strcat( infile,"results.spimodfit.2316.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2317){strcat( infile,"results.spimodfit.2317.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2318){strcat( infile,"results.spimodfit.2318.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2319){strcat( infile,"results.spimodfit.2319.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2320){strcat( infile,"results.spimodfit.2320.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2321){strcat( infile,"results.spimodfit.2321.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2322){strcat( infile,"results.spimodfit.2322.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 

 if(id==2332){strcat( infile,"results.spimodfit.2332.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2333){strcat( infile,"results.spimodfit.2333.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2334){strcat( infile,"results.spimodfit.2334.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2335){strcat( infile,"results.spimodfit.2335.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2336){strcat( infile,"results.spimodfit.2336.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2337){strcat( infile,"results.spimodfit.2337.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2338){strcat( infile,"results.spimodfit.2338.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 

 if(id==2341){strcat( infile,"results.spimodfit.2341.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2342){strcat( infile,"results.spimodfit.2342.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2343){strcat( infile,"results.spimodfit.2343.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2344){strcat( infile,"results.spimodfit.2344.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2345){strcat( infile,"results.spimodfit.2345.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2346){strcat( infile,"results.spimodfit.2346.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2347){strcat( infile,"results.spimodfit.2347.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2348){strcat( infile,"results.spimodfit.2348.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2349){strcat( infile,"results.spimodfit.2349.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2350){strcat( infile,"results.spimodfit.2350.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2351){strcat( infile,"results.spimodfit.2351.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2352){strcat( infile,"results.spimodfit.2352.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 

 if(id==2361){strcat( infile,"results.spimodfit.2361.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2362){strcat( infile,"results.spimodfit.2362.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2363){strcat( infile,"results.spimodfit.2363.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2364){strcat( infile,"results.spimodfit.2364.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2365){strcat( infile,"results.spimodfit.2365.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2366){strcat( infile,"results.spimodfit.2366.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2367){strcat( infile,"results.spimodfit.2367.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2368){strcat( infile,"results.spimodfit.2368.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2369){strcat( infile,"results.spimodfit.2369.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2370){strcat( infile,"results.spimodfit.2370.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2371){strcat( infile,"results.spimodfit.2371.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2372){strcat( infile,"results.spimodfit.2372.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 

 if(id==2381){strcat( infile,"results.spimodfit.2381.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2382){strcat( infile,"results.spimodfit.2382.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2383){strcat( infile,"results.spimodfit.2383.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2384){strcat( infile,"results.spimodfit.2384.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2385){strcat( infile,"results.spimodfit.2385.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2386){strcat( infile,"results.spimodfit.2386.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2387){strcat( infile,"results.spimodfit.2387.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2388){strcat( infile,"results.spimodfit.2388.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2389){strcat( infile,"results.spimodfit.2389.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2390){strcat( infile,"results.spimodfit.2390.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 

 if(id==2441){strcat( infile,"results.spimodfit.2441.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2442){strcat( infile,"results.spimodfit.2442.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2443){strcat( infile,"results.spimodfit.2443.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2444){strcat( infile,"results.spimodfit.2444.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2445){strcat( infile,"results.spimodfit.2445.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2446){strcat( infile,"results.spimodfit.2446.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2447){strcat( infile,"results.spimodfit.2447.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2448){strcat( infile,"results.spimodfit.2448.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2449){strcat( infile,"results.spimodfit.2449.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2450){strcat( infile,"results.spimodfit.2450.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 

 if(id==2461){strcat( infile,"results.spimodfit.2461.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2462){strcat( infile,"results.spimodfit.2462.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2463){strcat( infile,"results.spimodfit.2463.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2464){strcat( infile,"results.spimodfit.2464.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2465){strcat( infile,"results.spimodfit.2465.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2466){strcat( infile,"results.spimodfit.2466.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2467){strcat( infile,"results.spimodfit.2467.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2468){strcat( infile,"results.spimodfit.2468.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2469){strcat( infile,"results.spimodfit.2469.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2470){strcat( infile,"results.spimodfit.2470.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 

 if(id==2481){strcat( infile,"results.spimodfit.2481.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2482){strcat( infile,"results.spimodfit.2482.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2483){strcat( infile,"results.spimodfit.2483.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2484){strcat( infile,"results.spimodfit.2484.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2485){strcat( infile,"results.spimodfit.2485.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2486){strcat( infile,"results.spimodfit.2486.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2487){strcat( infile,"results.spimodfit.2487.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2488){strcat( infile,"results.spimodfit.2488.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2489){strcat( infile,"results.spimodfit.2489.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2490){strcat( infile,"results.spimodfit.2490.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 

 if(id==2501){strcat( infile,"results.spimodfit.2501.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2502){strcat( infile,"results.spimodfit.2502.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2503){strcat( infile,"results.spimodfit.2503.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2504){strcat( infile,"results.spimodfit.2504.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2505){strcat( infile,"results.spimodfit.2505.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 

 if(id==2511){strcat( infile,"results.spimodfit.2511.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2512){strcat( infile,"results.spimodfit.2512.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2513){strcat( infile,"results.spimodfit.2513.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2514){strcat( infile,"results.spimodfit.2514.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2515){strcat( infile,"results.spimodfit.2515.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 

 if(id==2521){strcat( infile,"results.spimodfit.2521.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2522){strcat( infile,"results.spimodfit.2522.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2523){strcat( infile,"results.spimodfit.2523.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2524){strcat( infile,"results.spimodfit.2524.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2525){strcat( infile,"results.spimodfit.2525.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 

 if(id==2531){strcat( infile,"results.spimodfit.2531.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2532){strcat( infile,"results.spimodfit.2532.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2533){strcat( infile,"results.spimodfit.2533.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2534){strcat( infile,"results.spimodfit.2534.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2535){strcat( infile,"results.spimodfit.2535.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 

 if(id==2541){strcat( infile,"results.spimodfit.2541.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2542){strcat( infile,"results.spimodfit.2542.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2543){strcat( infile,"results.spimodfit.2543.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2544){strcat( infile,"results.spimodfit.2544.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==2545){strcat( infile,"results.spimodfit.2545.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 

 /*
 if(id==3001){strcat( infile,"petry/rev15-494/v3/results.spimodfit.bin1.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==3002){strcat( infile,"petry/rev15-494/v3/results.spimodfit.bin2.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==3003){strcat( infile,"petry/rev15-494/v3/results.spimodfit.bin3.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==3004){strcat( infile,"petry/rev15-494/v3/results.spimodfit.bin4.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==3005){strcat( infile,"petry/rev15-494/v3/results.spimodfit.bin5.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==3006){strcat( infile,"petry/rev15-494/v3/results.spimodfit.bin6.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==3007){strcat( infile,"petry/rev15-494/v3/results.spimodfit.bin7.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==3008){strcat( infile,"petry/rev15-494/v3/results.spimodfit.bin8.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==3009){strcat( infile,"petry/rev15-494/v3/results.spimodfit.bin9.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==3010){strcat( infile,"petry/rev15-494/v3/results.spimodfit.bin10.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==3011){strcat( infile,"petry/rev15-494/v3/results.spimodfit.bin11.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==3012){strcat( infile,"petry/rev15-494/v3/results.spimodfit.bin12.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==3013){strcat( infile,"petry/rev15-494/v3/results.spimodfit.bin13.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==3014){strcat( infile,"petry/rev15-494/v3/results.spimodfit.bin14.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==3015){strcat( infile,"petry/rev15-494/v3/results.spimodfit.bin15.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==3016){strcat( infile,"petry/rev15-494/v3/results.spimodfit.bin16.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==3017){strcat( infile,"petry/rev15-494/v3/results.spimodfit.bin17.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==3018){strcat( infile,"petry/rev15-494/v3/results.spimodfit.bin18.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==3019){strcat( infile,"petry/rev15-494/v3/results.spimodfit.bin19.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==3020){strcat( infile,"petry/rev15-494/v3/results.spimodfit.bin20.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==3021){strcat( infile,"petry/rev15-494/v3/results.spimodfit.bin21.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==3022){strcat( infile,"petry/rev15-494/v3/results.spimodfit.bin22.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 */
 
 if(id==3001){strcat( infile,"petry/results.spimodfit.bin1.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==3002){strcat( infile,"petry/results.spimodfit.bin2.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==3003){strcat( infile,"petry/results.spimodfit.bin3.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==3004){strcat( infile,"petry/results.spimodfit.bin4.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==3005){strcat( infile,"petry/results.spimodfit.bin5.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==3006){strcat( infile,"petry/results.spimodfit.bin6.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==3007){strcat( infile,"petry/results.spimodfit.bin7.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==3008){strcat( infile,"petry/results.spimodfit.bin8.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==3009){strcat( infile,"petry/results.spimodfit.bin9.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==3010){strcat( infile,"petry/results.spimodfit.bin10.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==3011){strcat( infile,"petry/results.spimodfit.bin11.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==3012){strcat( infile,"petry/results.spimodfit.bin12.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==3013){strcat( infile,"petry/results.spimodfit.bin13.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==3014){strcat( infile,"petry/results.spimodfit.bin14.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==3015){strcat( infile,"petry/results.spimodfit.bin15.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==3016){strcat( infile,"petry/results.spimodfit.bin16.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==3017){strcat( infile,"petry/results.spimodfit.bin17.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==3018){strcat( infile,"petry/results.spimodfit.bin18.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==3019){strcat( infile,"petry/results.spimodfit.bin19.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==3020){strcat( infile,"petry/results.spimodfit.bin20.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==3021){strcat( infile,"petry/results.spimodfit.bin21.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==3022){strcat( infile,"petry/results.spimodfit.bin22.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 
 


 if(id==3101){strcat( infile,"petry/rev15-254/v3/results.spimodfit.bin1.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==3102){strcat( infile,"petry/rev15-254/v3/results.spimodfit.bin2.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==3103){strcat( infile,"petry/rev15-254/v3/results.spimodfit.bin3.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==3104){strcat( infile,"petry/rev15-254/v3/results.spimodfit.bin4.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==3105){strcat( infile,"petry/rev15-254/v3/results.spimodfit.bin5.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==3106){strcat( infile,"petry/rev15-254/v3/results.spimodfit.bin6.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==3107){strcat( infile,"petry/rev15-254/v3/results.spimodfit.bin7.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==3108){strcat( infile,"petry/rev15-254/v3/results.spimodfit.bin8.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==3109){strcat( infile,"petry/rev15-254/v3/results.spimodfit.bin9.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==3110){strcat( infile,"petry/rev15-254/v3/results.spimodfit.bin10.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==3111){strcat( infile,"petry/rev15-254/v3/results.spimodfit.bin11.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==3112){strcat( infile,"petry/rev15-254/v3/results.spimodfit.bin12.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==3113){strcat( infile,"petry/rev15-254/v3/results.spimodfit.bin13.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==3114){strcat( infile,"petry/rev15-254/v3/results.spimodfit.bin14.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==3115){strcat( infile,"petry/rev15-254/v3/results.spimodfit.bin15.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==3116){strcat( infile,"petry/rev15-254/v3/results.spimodfit.bin16.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==3117){strcat( infile,"petry/rev15-254/v3/results.spimodfit.bin17.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==3118){strcat( infile,"petry/rev15-254/v3/results.spimodfit.bin18.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==3119){strcat( infile,"petry/rev15-254/v3/results.spimodfit.bin19.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==3120){strcat( infile,"petry/rev15-254/v3/results.spimodfit.bin20.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==3121){strcat( infile,"petry/rev15-254/v3/results.spimodfit.bin21.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 
 if(id==3122){strcat( infile,"petry/rev15-254/v3/results.spimodfit.bin22.fits"   );   energy_select1=1; energy_select2=1;color=kBlue; } 

 if(idlistN==13)  color=kRed;

 if(spimodfit_version!=2.1){cout<<"invalid spimodfit version: "<<spimodfit_version<< endl;return 1;}

 if(energy_select1!=0) // found data
 {

 fits_open_file(&fptr,infile,READONLY,&status) ; 
 cout<<"FITS read open status= "<<status<<" "<<infile<<endl;
   



    fits_get_num_hdus(fptr,&total_hdus,&status);
    cout<<"total number of header units="<<total_hdus<<endl;


   
    
    //  cout<<"spimodfit version= "<<spimodfit_version<<" n_energy  ="<<n_energy  <<endl;


    
 
    i_component=0;
    i_energy   =0;

// hdu 1=primary header hdu 2= grouping
// hdu= (hdunum as seen by fv starting with 0) + 1 

// component input images
    cout<<"  --- component input images:"<<endl;

    n_map_component=0;
    hdunum=2;
    for(;;) // infinite loop with break
      {
    fits_movabs_hdu(fptr,hdunum+1,&hdutype,&status ); 
    cout<<"FITS movabs_hdu hdunum="<<hdunum<<" status= "<<status<<" hdutype="<<hdutype  ;
    if(hdutype!=0)break; // end of images

    n_map_component++;
    fits_read_key(fptr,TLONG,"NAXIS1",&naxis1,comment,&status);
    cout<<" naxis1    ="<<naxis1    ;
    fits_read_key(fptr,TLONG,"NAXIS2",&naxis2  ,comment,&status);
    cout<<" naxis2    ="<<naxis2    <<endl;

    //model_map.read(infile,hdunum+1);model_map.print();
    hdunum++;
      }


    cout<<endl<<"n_map_component="<<n_map_component<<endl;

    cout<<endl<<"  --- energy bounds:"<<endl;
   
    fits_movabs_hdu(fptr,hdunum+1,&hdutype,&status ); 
    cout<<"FITS movabs_hdu hdunum="<<hdunum<<" status= "<<status<<" hdutype="<<hdutype      ;

 
    fits_read_key(fptr,TLONG,"TFIELDS",&n_columns,comment,&status);
    cout<<" n_columns ="<<n_columns ;

    n_energy=0;
    fits_read_key(fptr,TLONG,"NAXIS2",&n_energy,comment,&status);
    cout<<" n_energy  ="<<n_energy  <<endl;


    emin=new double[n_energy];                                                             //AWS20070302
    emax=new double[n_energy];                                                             //AWS20070302

    fits_get_colnum(fptr,CASEINSEN,"E_MIN",&colnum,&status);                               //AWS20070302
    cout<<"  E_MIN column ="<< colnum<<endl;
    fits_read_col(fptr, TDOUBLE, colnum, 1,1,n_energy,&nulval,emin      , &anynul,&status);//AWS20070302
    fits_get_colnum(fptr,CASEINSEN,"E_MAX",&colnum,&status);                               //AWS20070302
    cout<<"  E_MIN column ="<< colnum<<endl;
    fits_read_col(fptr, TDOUBLE, colnum, 1,1,n_energy,&nulval,emax      , &anynul,&status);//AWS20070302

    if(galplotdef.verbose== -307)// selectable debug
    {
      for (i_ebds =0;i_ebds <n_energy;i_ebds++){cout<<" from EBDS extension: emin="<<emin[i_ebds ]<<" emax="<<emax[i_ebds ];cout<<endl;}
    }


    cout<<"  --- ML   results:"<<endl;
    hdunum++;
    fits_movabs_hdu(fptr,hdunum+1,&hdutype,&status ); 
    cout<<"FITS movabs_hdu hdunum="<<hdunum<<" status= "<<status<<" hdutype="<<hdutype      ;
    fits_read_key(fptr,TLONG,"TFIELDS",&n_columns,comment,&status);
    cout<<" n_columns ="<<n_columns ;
    fits_read_key(fptr,TLONG,"NAXIS2",&n_rows  ,comment,&status);
    cout<<" n_rows    ="<<n_rows    <<endl;

    fits_get_colnum(fptr,CASEINSEN,"FIT_VALUES_ML",&colnum,&status);
    cout<<"FIT_VALUES_ML column ="<< colnum<<endl;

    n_theta=n_rows;
    theta_ML=new double[n_theta];
    flux_ref=new double[n_theta];
    energy_cor=new double[n_theta];//AWS20070302

    nulval=0.;
    anynul=0 ;
    fits_read_col(fptr, TDOUBLE, colnum, 1,1,n_theta,&nulval,theta_ML, &anynul,&status);

    if(galplotdef.verbose== -307)// selectable debug
    {
     cout<<"theta_ML:";for (i_theta=0;i_theta<n_theta;i_theta++)cout<<" "<<theta_ML[i_theta];cout<<endl;
    }


    // NB sources have scaling factor


    fits_get_colnum(fptr,CASEINSEN,"FIT_COV_ML",&colnum,&status);
    cout<<"FIT_COV_ML column ="<< colnum<<endl;

    fits_get_coltype(fptr,colnum,&typecode,&repeat,&width,&status);
    cout<<"length of vector (=repeat) ="<<repeat<<endl;
    n_component=repeat;
    cov_ML=new double[n_component*n_component];

    nulval=0.;
    anynul=0 ;
    fits_read_col(fptr, TDOUBLE, colnum, 1,1,n_component*n_component,&nulval,cov_ML, &anynul,&status);

    if(galplotdef.verbose== -307)// selectable debug
    {
     cout<<"cov_ML:"<<endl;
     for (i_component=0;i_component<n_component;i_component++)
     {
      cout<<endl<<endl;
      for (j_component=0;j_component<n_component;j_component++)
      cout<<" "<< cov_ML[i_component*n_component+j_component]<<" ";
     }
    }

    cout<<"  --- MCMC results:"<<endl;


    hdunum++;
    fits_movabs_hdu(fptr,hdunum+1,&hdutype,&status ); 
    cout<<"FITS movabs_hdu hdunum="<<hdunum<<" status= "<<status<<" hdutype="<<hdutype      ;
    fits_read_key(fptr,TLONG,"TFIELDS",&n_columns,comment,&status);
    cout<<" n_columns ="<<n_columns ;
    fits_read_key(fptr,TLONG,"NAXIS2",&n_rows  ,comment,&status);
    cout<<" n_rows    ="<<n_rows    <<endl;

    theta_MC=new double[n_theta];
      cov_MC=new double[n_component*n_component];

// total images
    cout<<"  --- total image: ML"<<endl;
    hdunum++;
    fits_movabs_hdu(fptr,hdunum+1,&hdutype,&status ); 
    cout<<"FITS movabs_hdu hdunum="<<hdunum<<" status= "<<status<<" hdutype="<<hdutype  ;
    

    fits_read_key(fptr,TLONG,"NAXIS1",&naxis1,comment,&status);
    cout<<" naxis1    ="<<naxis1    ;
    fits_read_key(fptr,TLONG,"NAXIS2",&naxis2  ,comment,&status);
    cout<<" naxis2    ="<<naxis2    <<endl;

    cout<<"  --- total image: MCMC"<<endl;
    hdunum++;
    fits_movabs_hdu(fptr,hdunum+1,&hdutype,&status ); 
    cout<<"FITS movabs_hdu hdunum="<<hdunum<<" status= "<<status<<" hdutype="<<hdutype  ;
    

    fits_read_key(fptr,TLONG,"NAXIS1",&naxis1,comment,&status);
    cout<<" naxis1    ="<<naxis1    ;
    fits_read_key(fptr,TLONG,"NAXIS2",&naxis2  ,comment,&status);
    cout<<" naxis2    ="<<naxis2    <<endl;




   

    ////////////////////////////////////////////////////////////////////////////////////////////////////   
 


    cout<<"starting main loop"<<endl;



    i_energy1=0;
    i_energy2=n_energy-1;

    //select_energy=0;// temporary for testing

    if(select_energy==1){ i_energy1=energy_select1-1;   i_energy2=energy_select2-1;     }

   for (i_energy=i_energy1; i_energy<=i_energy2;i_energy++)
   {

     
     
     cout<<"i_energy="<<i_energy<<endl;

     // read theta and covariance
     /*
    fits_movabs_hdu(fptr,hdunum+1,&hdutype,&status );
    fits_read_img(fptr,TDOUBLE,felement,n_theta,                &nulval,theta_ML,&anynul,&status) ;

    fits_movabs_hdu(fptr,hdunum+2,&hdutype,&status );
    fits_read_img(fptr,TDOUBLE,felement,n_component*n_component,&nulval,  cov_ML,&anynul,&status) ;

    fits_movabs_hdu(fptr,hdunum+3,&hdutype,&status );
    fits_read_img(fptr,TDOUBLE,felement,n_theta,                &nulval,theta_MC,&anynul,&status) ;

    fits_movabs_hdu(fptr,hdunum+4,&hdutype,&status );
    fits_read_img(fptr,TDOUBLE,felement,n_component*n_component,&nulval,  cov_MC,&anynul,&status) ;


    */
     
     hdunum=1 + n_map_component + 1 + i_energy*4 + 1; //grouping maps energybounds +(4 results/energy)
    fits_movabs_hdu(fptr,hdunum+1,&hdutype,&status ); 
    cout<<"FITS movabs_hdu hdunum="<<hdunum<<" status= "<<status<<" hdutype="<<hdutype <<endl   ;


    fits_read_key(fptr,TDOUBLE,"E_MIN",&e_min       ,comment,&status);//AWS20070302
    fits_read_key(fptr,TDOUBLE,"E_MAX",&e_max       ,comment,&status);//AWS20070302
   
    if(galplotdef.verbose>=  -305)// selectable debug
    cout<<"this dataset: e_min = "<<e_min  <<" e_max = "<< e_max  <<" keV"    <<endl;


    fits_get_colnum(fptr,CASEINSEN,"PAR_ID",&colnum,&status);
    cout<<"  PAR_ID column ="<< colnum<<endl;
    char **par_id; par_id=new char*[n_theta];
    char nulstr[100];strcpy(nulstr,"null");

    for (i_theta=0;i_theta<n_theta;i_theta++)     par_id[i_theta]=new char[100];

     fits_read_col_str(fptr,  colnum, 1,1, n_theta,nulstr,  par_id, &anynul,&status);
    
     cout<<"fits_read_col_str status="<<status<<endl;
    
    if(galplotdef.verbose== -307)// selectable debug
    {
     cout<<"par_id  :";for (i_theta=0;i_theta<n_theta;i_theta++)cout<<" "<< par_id [i_theta];cout<<endl;
    }
   

    fits_get_colnum(fptr,CASEINSEN,"FLUX_REF",&colnum,&status);
    cout<<"  FLUX_REF column ="<< colnum<<endl;
    fits_read_col(fptr, TDOUBLE, colnum, 1,1,n_theta,&nulval,flux_ref, &anynul,&status);

    if(galplotdef.verbose== -307)// selectable debug
    {
     cout<<"flux_ref:";for (i_theta=0;i_theta<n_theta;i_theta++)cout<<" "<<flux_ref[i_theta];cout<<endl;
    }



    fits_get_colnum(fptr,CASEINSEN,"ENERGY_COR",&colnum,&status);                         //AWS20070302
    cout<<"  ENERGY_COR column ="<< colnum<<endl;
    fits_read_col(fptr, TDOUBLE, colnum, 1,1,n_theta,&nulval,energy_cor, &anynul,&status);//AWS20070302

    if(galplotdef.verbose== -307)// selectable debug
    {
     cout<<"energy_cor:";for (i_theta=0;i_theta<n_theta;i_theta++)cout<<" "<<energy_cor[i_theta];cout<<endl;
    }

    // both factors have to be applied
    for (i_theta=0;i_theta<n_theta;i_theta++)flux_ref[i_theta]*=energy_cor[i_theta]; //AWS20070302



    fits_get_colnum(fptr,CASEINSEN,"FIT_VALUES_ML",&colnum,&status);
    cout<<"  FIT_VALUES_ML column ="<< colnum<<endl;
    fits_read_col(fptr, TDOUBLE, colnum, 1,1,n_theta,&nulval,theta_ML, &anynul,&status);

    if(galplotdef.verbose== -307)// selectable debug
    {
     cout<<"theta_ML:";for (i_theta=0;i_theta<n_theta;i_theta++)cout<<" "<<theta_ML[i_theta];cout<<endl;
    }


    // image parameters have a scaling factor if EBDS has >1 energy. The scaling may be E^-2 or energy_cor but not sure          //AWS20070302
    if(n_energy>1)                                                                                                               //AWS20070302
      //    for(i_theta=0;i_theta<n_map_component;i_theta++)theta_ML[i_theta]*=pow(sqrt(e_min*e_max)/sqrt(emin[0]*emax[0]),-2.);         //AWS20070302
        for(i_theta=0;i_theta<n_map_component;i_theta++)theta_ML[i_theta]*=energy_cor[i_theta];                                //AWS20070302

    // sources have scaling factor flux_ref
    for(i_theta=n_map_component; i_theta<n_component; i_theta++)theta_ML[i_theta]*=flux_ref[i_theta];




    fits_get_colnum(fptr,CASEINSEN,"FIT_COV_ML",&colnum,&status);
    cout<<"FIT_COV_ML column ="<< colnum<<endl;
                 
    fits_read_col(fptr, TDOUBLE, colnum, 1,1,n_component*n_component,&nulval,cov_ML, &anynul,&status);

    if(galplotdef.verbose== -307)// selectable debug
    {
     cout<<"cov_ML:"<<endl;
     for (i_component=0;i_component<n_component;i_component++)
     {
      cout<<endl<<endl;
      for (j_component=0;j_component<n_component;j_component++)
      cout<<" "<< cov_ML[i_component*n_component+j_component]<<" ";
     }
    }



    // image parameters have a scaling factor if EBDS has >1 energy. The scaling may be E^-2 or energy_cor but not sure          //AWS20070302
    // so have to correct covariances accordingly
    if(n_energy>1)    
    {                                                                                                           //AWS20070302
     for (i_component=0;i_component<n_component;i_component++)
     {
      for (j_component=0;j_component<n_component;j_component++)
      {
       if(i_component< n_map_component)
	 //       cov_ML[i_component*n_component+j_component]*=pow(sqrt(e_min*e_max)/sqrt(emin[0]*emax[0]),-2.);           //AWS20070302
       cov_ML[i_component*n_component+j_component]*=energy_cor[i_component];           //AWS20070305
       if(j_component< n_map_component)
	 //       cov_ML[i_component*n_component+j_component]*=pow(sqrt(e_min*e_max)/sqrt(emin[0]*emax[0]),-2.);           //AWS20070302 
       cov_ML[i_component*n_component+j_component]*=energy_cor[j_component];           //AWS20070305
     }
    }
   }//if                                                                                                        //AWS20070302



    // sources have scaling factor flux_ref, so have to correct covariances accordingly
     for (i_component=0;i_component<n_component;i_component++)
     {
      for (j_component=0;j_component<n_component;j_component++)
      {
       if(i_component>=n_map_component)
       cov_ML[i_component*n_component+j_component]*=flux_ref[i_component];
       if(j_component>=n_map_component)
       cov_ML[i_component*n_component+j_component]*=flux_ref[j_component];
     }
    }



    fits_get_colnum(fptr,CASEINSEN,"FIT_VALUES_MCMC",&colnum,&status);
    cout<<"  FIT_VALUES_MCMC column ="<< colnum<<endl;
    fits_read_col(fptr, TDOUBLE, colnum, 1,1,n_theta,&nulval,theta_MC, &anynul,&status);

    if(galplotdef.verbose== -307)// selectable debug
    {
     cout<<"theta_MC:";for (i_theta=0;i_theta<n_theta;i_theta++)cout<<" "<<theta_MC[i_theta];cout<<endl;
    }


   fits_get_colnum(fptr,CASEINSEN,"FIT_COV_MCMC",&colnum,&status);
    cout<<"FIT_COV_MCMC column ="<< colnum<<endl;
                 
    fits_read_col(fptr, TDOUBLE, colnum, 1,1,n_component*n_component,&nulval,cov_MC, &anynul,&status);

    if(galplotdef.verbose== -307)// selectable debug
    {
     cout<<"cov_MC:"<<endl;
     for (i_component=0;i_component<n_component;i_component++)
     {
      cout<<endl<<endl;
      for (j_component=0;j_component<n_component;j_component++)
      cout<<" "<< cov_MC[i_component*n_component+j_component]<<" ";
     }
    }




    // read MCMC chain  if present
    /*
    if(spidiffit_version==22)
    {
    fits_movabs_hdu(fptr,hdunum+5,&hdutype,&status );
    cout<<"FITS movabs_hdu status= "<<status<<" hdutype="<<hdutype<<endl;
    fits_read_key(fptr,TLONG,"NAXIS1",&N_theta_chain,comment,&status);
    fits_read_key(fptr,TLONG,"NAXIS2",&n_accepted   ,comment,&status);
    cout<<" MCMC chain: number of theta values="  <<N_theta_chain<<" number of samples="<< n_accepted<< endl;

    theta_chain=new  float[N_theta_chain*n_accepted];
     logL_chain=new  float[              n_accepted];

    fits_read_img(fptr,TFLOAT ,felement,N_theta_chain*n_accepted,&nulval,theta_chain,&anynul,&status) ;

    fits_movabs_hdu(fptr,hdunum+6,&hdutype,&status );
    cout<<"FITS movabs_hdu status= "<<status<<" hdutype="<<hdutype<<endl;
    fits_read_img(fptr,TFLOAT ,felement,              n_accepted,&nulval, logL_chain,&anynul,&status) ;

    if(galplotdef.verbose== -306)// selectable debug
    {
     i_chain=0;
     for (i_sample_chain=0;i_sample_chain<n_accepted;i_sample_chain++)
     {
      cout<<"sample #"<<i_sample_chain<<": ";
      for(i_theta_chain=0;i_theta_chain<N_theta_chain;i_theta_chain++,i_chain++)
      cout<<theta_chain[i_chain]<<" ";
      cout<<" logL=" <<logL_chain[i_sample_chain];
      cout<<endl;
     }
      cout<<endl;
    }

    }// read MCMC chain

    */

    if(galplotdef.verbose== -301)// selectable debug
    {
    cout<<endl<<"   theta_ML: ";    for(int i=0; i<n_theta;i++)cout<< theta_ML[i]<<" ";
    cout<<endl<<"   theta_MC: ";    for(int i=0; i<n_theta;i++)cout<< theta_MC[i]<<" ";
    }

    if(galplotdef.verbose==  -302)// selectable debug
    {
    cout<<endl<<"   cov_ML: ";
    for(int i=0; i<n_component*n_component;i++)cout<< cov_ML[i]<<" ";
    cout<<endl<<"    cov_MC: ";
    for(int i=0; i<n_component*n_component;i++)cout<< cov_MC[i]<<" ";
    }

   

    fits_read_key(fptr,TDOUBLE,"E_MIN",&e_min       ,comment,&status);
    fits_read_key(fptr,TDOUBLE,"E_MAX",&e_max       ,comment,&status);
   
    if(galplotdef.verbose>=  -305)// selectable debug
    cout<<"e_min = "<<e_min  <<" e_max = "<< e_max  <<" keV"    <<endl;

 
    // read model maps


    intensity        =new double[n_component];
    component_average=new double[n_component];
   
    for(i_component=0; i_component<n_map_component; i_component++)
    {
      hdunum=1 + i_component + 1;// grouping  maps

    if(galplotdef.verbose>=  -305)// selectable debug
     cout<<endl<<"i_energy icomponent hdunum "<<i_energy<<" "<<i_component<<" "<<hdunum<<endl;

   
    component_average[i_component]=0.;

    model_map.read(infile,hdunum+1);

    if(galplotdef.verbose==  -304)// selectable debug
    {
     cout<<endl<<"model map for i_energy i_component hdunum "<<i_energy<<" "<<i_component<<" "<<hdunum<<endl;
     model_map.print();
    }


    /* for testing 
    galplotdef.long_min1=+1;
    galplotdef.long_max1=+20;
    galplotdef.long_min2=-20;
    galplotdef.long_max2=-01;
    galplotdef.lat_min1=-05;
    galplotdef.lat_max1=+05;
    galplotdef.lat_min2=-05;
    galplotdef.lat_max2=+05;
    */

    bin_count=0;


    // move origin to first pixel, equivalent to CRPIX=1.0
    // so the subsequent  code is same as in plot_SPI_spectrum.cc

    model_map.CRVAL[0] -= (model_map.CRPIX[0] - 1.0)* model_map.CDELT[0];
    model_map.CRVAL[1] -= (model_map.CRPIX[1] - 1.0)* model_map.CDELT[1];


    for(region=1;region<=4;region++)
    {
    if(region==1)
    {
     delta_longitude=galplotdef.long_min1 - model_map.CRVAL[0];
     if    (delta_longitude <   0.0) delta_longitude+=360.0;
     if    (delta_longitude > 360.0) delta_longitude-=360.0;
     ilmin=(int)((delta_longitude  + .0001) / model_map.CDELT[0]);

     delta_longitude=galplotdef.long_max1 - model_map.CRVAL[0];
     if    (delta_longitude <   0.0) delta_longitude+=360.0;
     if    (delta_longitude > 360.0) delta_longitude-=360.0;
     ilmax=(int)((delta_longitude  + .0001) / model_map.CDELT[0]);

     //  ilmin=(galplotdef.long_min1 - model_map.CRVAL[0] + .0001) / model_map.CDELT[0];
     //  ilmax=(galplotdef.long_max1 - model_map.CRVAL[0] + .0001) / model_map.CDELT[0];
    ibmin=(int)((galplotdef. lat_min1 - model_map.CRVAL[1] + .0001) / model_map.CDELT[1]);
    ibmax=(int)((galplotdef. lat_max1 - model_map.CRVAL[1] + .0001) / model_map.CDELT[1]);
    }

    if(region==2)
    {
     delta_longitude=galplotdef.long_min1 - model_map.CRVAL[0];
     if    (delta_longitude <   0.0) delta_longitude+=360.0;
     if    (delta_longitude > 360.0) delta_longitude-=360.0;
     ilmin=(int)((delta_longitude  + .0001) / model_map.CDELT[0]);

     delta_longitude=galplotdef.long_max1 - model_map.CRVAL[0];
     if    (delta_longitude <   0.0) delta_longitude+=360.0;
     if    (delta_longitude > 360.0) delta_longitude-=360.0;
     ilmax=(int)((delta_longitude  + .0001) / model_map.CDELT[0]);

      //    ilmin=(galplotdef.long_min1 - model_map.CRVAL[0] + .0001) / model_map.CDELT[0];
      //   ilmax=(galplotdef.long_max1 - model_map.CRVAL[0] + .0001) / model_map.CDELT[0];
    ibmin=(int)((galplotdef. lat_min2 - model_map.CRVAL[1] + .0001) / model_map.CDELT[1]);
    ibmax=(int)((galplotdef. lat_max2 - model_map.CRVAL[1] + .0001) / model_map.CDELT[1]);
    }
    if(region==3)
    {

     delta_longitude=galplotdef.long_min2 - model_map.CRVAL[0];
     if    (delta_longitude <   0.0) delta_longitude+=360.0;
     if    (delta_longitude > 360.0) delta_longitude-=360.0;
     ilmin=(int)((delta_longitude  + .0001) / model_map.CDELT[0]);

     delta_longitude=galplotdef.long_max2 - model_map.CRVAL[0];
     if    (delta_longitude <   0.0) delta_longitude+=360.0;
     if    (delta_longitude > 360.0) delta_longitude-=360.0;
     ilmax=(int)((delta_longitude  + .0001) / model_map.CDELT[0]);

     //  ilmin=(galplotdef.long_min2 - model_map.CRVAL[0] + .0001) / model_map.CDELT[0];
     //  ilmax=(galplotdef.long_max2 - model_map.CRVAL[0] + .0001) / model_map.CDELT[0];
    ibmin=(int)((galplotdef. lat_min1 - model_map.CRVAL[1] + .0001) / model_map.CDELT[1]);
    ibmax=(int)((galplotdef. lat_max1 - model_map.CRVAL[1] + .0001) / model_map.CDELT[1]);
    }

    if(region==4)
    {

     delta_longitude=galplotdef.long_min2 - model_map.CRVAL[0];
     if    (delta_longitude <   0.0) delta_longitude+=360.0;
     if    (delta_longitude > 360.0) delta_longitude-=360.0;
     ilmin=(int)((delta_longitude  + .0001) / model_map.CDELT[0]);

     delta_longitude=galplotdef.long_max2 - model_map.CRVAL[0];
     if    (delta_longitude <   0.0) delta_longitude+=360.0;
     if    (delta_longitude > 360.0) delta_longitude-=360.0;
     ilmax=(int)((delta_longitude  + .0001) / model_map.CDELT[0]);

     //ilmin=(galplotdef.long_min2 - model_map.CRVAL[0] + .0001) / model_map.CDELT[0];
     //ilmax=(galplotdef.long_max2 - model_map.CRVAL[0] + .0001) / model_map.CDELT[0];

    ibmin=(int)((galplotdef. lat_min2 - model_map.CRVAL[1] + .0001) / model_map.CDELT[1]);
    ibmax=(int)((galplotdef. lat_max2 - model_map.CRVAL[1] + .0001) / model_map.CDELT[1]);
    }
    if(ilmin<0)ilmin=0;
    if(ilmax<0)ilmax=0;
    if(ibmin<0)ibmin=0;
    if(ibmax<0)ibmax=0;
    if(ilmin>model_map.NAXES[0]-1 )ilmin=model_map.NAXES[0]-1;
    if(ilmax>model_map.NAXES[0]-1 )ilmax=model_map.NAXES[0]-1;
    if(ibmin>model_map.NAXES[1]-1 )ibmin=model_map.NAXES[1]-1;
    if(ibmax>model_map.NAXES[1]-1 )ibmax=model_map.NAXES[1]-1;

 
    for(il=ilmin;il<=ilmax;il++)
    for(ib=ibmin;ib<=ibmax;ib++)
    {
     component_average[i_component]+=model_map(il,ib);
     bin_count++;
    }



      cout<<" ilmin="<<ilmin <<" ilmax="<<ilmax<<" ibmin="<<ibmin<<" ibmax="<<ibmax
          <<" component_average="<<component_average[i_component]<< " bin_count="<<bin_count<<endl;

    }//region




 
  
    component_average[i_component]/=bin_count;

   


    intensity[i_component]=theta_ML[i_component]*component_average[i_component]; 
    intensity[i_component]/=(e_max-e_min)*1.e-3; // MeV^-1



    if(galplotdef.verbose>=  -305)// selectable debug
    cout<<"SPI energy "<<e_min<<"-"<<e_max<<"keV   i_component "<<i_component<<" intensity="<<intensity[i_component]<< " bin_count="<<bin_count<<endl;

    if(viewdiffit_flag==1 && theta_MC[i_component]<=0.0) // for more exact comparison with viewdiffit17.pro
      {
       intensity        [i_component]=0.0;// as in viewdiffit17.pro
       component_average[i_component]=0.0;
      }

    }//i_component map components



//////////////////////////////////////////////////////////////////////////////////////////////////////  
//=========================================  sources =================================================
////////////////////////////////////////////////////////////////////////////////////////////////////// 

    dtr=acos(-1.0)/180.; //degrees to radians
    solid_angle=bin_count*model_map.CDELT[0]*model_map.CDELT[1]*dtr*dtr;//solid angle in sterad

    for(i_significance_levels=0;i_significance_levels<n_significance_levels;i_significance_levels++)
    n_significant_sources[i_significance_levels]=0;

    for(i_component=n_map_component; i_component<n_component; i_component++)
    {
      component_average[i_component]=1.0                  /solid_angle;
      intensity[i_component]        =theta_ML[i_component]/solid_angle;
      intensity[i_component]/=(e_max-e_min)*1.e-3; // MeV^-1

      sigma= theta_ML[i_component]   /    sqrt(cov_ML[i_component*n_component + i_component]);

      // --------------- select sources for sigma statistics AWS20050629
      /*
     exclude_source=0;

     if(i_component>=n_map_component)
     {
      cat.find(par_id[i_component],&ra_source,&dec_source,&l_source,&b_source);
      cout<<" catalogue  source : "<<par_id[i_component]<<"  ra="<<ra_source<<" dec="<<dec_source<<" l="<<l_source<<" b="<<b_source<<endl;
      if(ra_source<0.0) cout<<" warning: source "<<par_id[i_component] <<" not found in catalogue !"<<endl;

      exclude_source=1;
      if(ra_source>=0.0) // consider including source  only if found in catalogue
      {
      // check all 4 regions
      if(l_source>=galplotdef.long_min1 && l_source<=galplotdef.long_max1 && b_source>=galplotdef.lat_min1 && b_source<=galplotdef. lat_max1)exclude_source=0;
      if(l_source>=galplotdef.long_min2 && l_source<=galplotdef.long_max2 && b_source>=galplotdef.lat_min1 && b_source<=galplotdef. lat_max1)exclude_source=0;
      if(l_source>=galplotdef.long_min1 && l_source<=galplotdef.long_max1 && b_source>=galplotdef.lat_min2 && b_source<=galplotdef. lat_max2)exclude_source=0;
      if(l_source>=galplotdef.long_min2 && l_source<=galplotdef.long_max2 && b_source>=galplotdef.lat_min2 && b_source<=galplotdef. lat_max2)exclude_source=0;
      }
      if(exclude_source==1)  cout<<" excluding  source : "<<par_id[i_component]<<"  ra="<<ra_source<<" dec="<<dec_source<<" l="<<l_source<<" b="<<b_source<<endl;
     }
      */

     exclude_source= exclude_source_(cat, par_id,  i_component);// AWS20050630

     if(exclude_source==0)
     {
      for    (i_significance_levels=0;i_significance_levels<n_significance_levels;i_significance_levels++)
       if(sigma>=significance_levels[i_significance_levels])n_significant_sources[i_significance_levels]+=1;
     }

     //------------------------------------------------    AWS20050629

      cout<<"id "<<id
          <<" E="<<e_min<<"-"<<e_max
          <<" source "<<i_component-n_map_component+1
          <<" = "<<par_id[i_component]
          <<" sigma="<< sigma;
           if(sigma>=3.0)cout<<"*";
	   cout<<" flux="  <<theta_ML[i_component]<<"+-"<<sqrt(cov_ML[i_component*n_component + i_component]);

      cout<< " bin_count="<<bin_count
          <<" solid angle="<<solid_angle<<" intensity= "<< intensity[i_component]<<endl;

    }//i_component source components

     for(i_significance_levels=0;i_significance_levels<n_significance_levels;i_significance_levels++)
     cout<<"id "<<id<<"  E="<<e_min<<"-"<<e_max<<" :  "<<n_significant_sources[i_significance_levels]
          <<" sources > "<<significance_levels[i_significance_levels] << " sigma"<<endl;


     cout<<endl<<"id "<<id<<"  E="<<e_min<<"-"<<e_max<<" number(>sigma) :  ";
     for(i_significance_levels=0;i_significance_levels<n_significance_levels;i_significance_levels++)
       cout <<"  "<<n_significant_sources[i_significance_levels];
     cout<<endl;

     cout<<endl<<"id "<<id<<"  E="<<e_min<<"-"<<e_max<<" number(dsigma) :  ";
     for(i_significance_levels=1;i_significance_levels<n_significance_levels;i_significance_levels++)
       cout <<"  "<<n_significant_sources[i_significance_levels-1]-n_significant_sources[i_significance_levels];
     cout<<endl;

     fprintf(txtFILE,"%6.1f-%6.1f  ",e_min,e_max);
     for(i_significance_levels=0;i_significance_levels<n_significance_levels;i_significance_levels++)
     fprintf(txtFILE,"&%d",n_significant_sources[i_significance_levels] );
     fprintf(txtFILE,"&%d  ",id);
     fprintf(txtFILE,"\\\\ %% number(>sigma) \n");                                             // "\\\\"gives "\\"  "%%" gives %"

    //////////////////////////////////////////////////////////////////////////////
    /* for testing only
    galplotdef.data_INTEGRAL_comp=new int[4];
    galplotdef.data_INTEGRAL_comp[0]=1;
    galplotdef.data_INTEGRAL_comp[1]=92;
    galplotdef.data_INTEGRAL_comp[2]=6;
    galplotdef.data_INTEGRAL_comp[3]=10;
    */

   for (iplot=0;iplot<=1;iplot++)
   {
  
    if(iplot==0)color=kBlue;
    if(iplot==1)color=kRed ;

    i_component1=galplotdef.data_INTEGRAL_comp[iplot*2  ]-1; // user value starts at 1, internal starts at 0
    i_component2=galplotdef.data_INTEGRAL_comp[iplot*2+1]-1;

    if(i_component1>=0 && i_component2>=0 )
    if(i_component1<= n_component-1 )
    {

    if(i_component1>n_component-1)i_component1=n_component-1;
    if(i_component2>n_component-1)i_component2=n_component-1;

    cout<<"components "<<i_component1+1<<" to  "<<i_component2+1<<endl;

    intensity_total=0.;
    for(i_component=i_component1;i_component<=i_component2;i_component++)
    {
      /*
      //cout<<"checking for Crab  "<<par_id[i_component]<<"strncmp= "<<strncmp(par_id[i_component],"Crab",4)<<endl;
     exclude_source=0;
     if (strncmp(par_id[i_component],"Crab",4)==0)exclude_source=1;
     if (strncmp(par_id[i_component],"Cyg" ,3)==0)exclude_source=1;
     if(exclude_source==1)cout<<"excluding source from total: "<<par_id[i_component]<<endl;
      */


     exclude_source=0;

     /*                                                            AWS20050630
     if(i_component>=n_map_component)
     {
      cat.find(par_id[i_component],&ra_source,&dec_source,&l_source,&b_source);
      cout<<" catalogue  source : "<<par_id[i_component]<<"  ra="<<ra_source<<" dec="<<dec_source<<" l="<<l_source<<" b="<<b_source<<endl;
      if(ra_source<0.0) cout<<" warning: source "<<par_id[i_component] <<" not found in catalogue !"<<endl;

      exclude_source=1;
      if(ra_source>=0.0) // consider including source  only if found in catalogue
      {
      // check all 4 regions
      if(l_source>=galplotdef.long_min1 && l_source<=galplotdef.long_max1 && b_source>=galplotdef.lat_min1 && b_source<=galplotdef. lat_max1)exclude_source=0;
      if(l_source>=galplotdef.long_min2 && l_source<=galplotdef.long_max2 && b_source>=galplotdef.lat_min1 && b_source<=galplotdef. lat_max1)exclude_source=0;
      if(l_source>=galplotdef.long_min1 && l_source<=galplotdef.long_max1 && b_source>=galplotdef.lat_min2 && b_source<=galplotdef. lat_max2)exclude_source=0;
      if(l_source>=galplotdef.long_min2 && l_source<=galplotdef.long_max2 && b_source>=galplotdef.lat_min2 && b_source<=galplotdef. lat_max2)exclude_source=0;
      }
      if(exclude_source==1)  cout<<" excluding  source : "<<par_id[i_component]<<"  ra="<<ra_source<<" dec="<<dec_source<<" l="<<l_source<<" b="<<b_source<<endl;
     }
     */

     if(i_component>=n_map_component)
     exclude_source= exclude_source_(cat, par_id,  i_component);// AWS20050630

     if(exclude_source==0)
     intensity_total+=intensity[i_component];
     cout<<"i_component intensity[i_component]    intensity_total      "<<i_component<<" "<<intensity[i_component]<<" "<< intensity_total<<endl;
    }


    dintensity=0;
    dintensity_MC=0;   dintensity_ML=0;

    for(i_component=i_component1;i_component<=i_component2;i_component++)
    for(j_component=i_component1;j_component<=i_component2;j_component++)
    {
     
     exclude_source  =0;
     exclude_source_i=0;
     exclude_source_j=0;

     /*
     if (strncmp(par_id[i_component],"Crab",4)==0)exclude_source=1;
     if (strncmp(par_id[j_component],"Crab",4)==0)exclude_source=1;
     if (strncmp(par_id[i_component],"Cyg" ,3)==0)exclude_source=1;
     if (strncmp(par_id[j_component],"Cyg" ,3)==0)exclude_source=1;
      */

     /*                                                                         AWS20050630
    if(i_component>=n_map_component)
     {
      cat.find(par_id[i_component],&ra_source,&dec_source,&l_source,&b_source);
      if(galplotdef.verbose== -600)// selectable debug
      cout<<" catalogue  source : "<<par_id[i_component]<<"  ra="<<ra_source<<" dec="<<dec_source<<" l="<<l_source<<" b="<<b_source<<endl;
      if(ra_source<0.0) cout<<" warning: source "<<par_id[i_component] <<" not found in catalogue !"<<endl;

      exclude_source_i=1;
      if(ra_source>=0.0) // consider including source  only if found in catalogue
      {
      // check all 4 regions
      if(l_source>=galplotdef.long_min1 && l_source<=galplotdef.long_max1 && b_source>=galplotdef.lat_min1 && b_source<=galplotdef. lat_max1)exclude_source_i=0;
      if(l_source>=galplotdef.long_min2 && l_source<=galplotdef.long_max2 && b_source>=galplotdef.lat_min1 && b_source<=galplotdef. lat_max1)exclude_source_i=0;
      if(l_source>=galplotdef.long_min1 && l_source<=galplotdef.long_max1 && b_source>=galplotdef.lat_min2 && b_source<=galplotdef. lat_max2)exclude_source_i=0;
      if(l_source>=galplotdef.long_min2 && l_source<=galplotdef.long_max2 && b_source>=galplotdef.lat_min2 && b_source<=galplotdef. lat_max2)exclude_source_i=0;
      }
      if(galplotdef.verbose== -600)// selectable debug
      if(exclude_source_i==1)  cout<<" dintensity:excluding  source i : "<<par_id[i_component]<<"  ra="<<ra_source<<" dec="<<dec_source<<" l="<<l_source<<" b="<<b_source<<endl;
     }

    if(j_component>=n_map_component)
     {
      cat.find(par_id[j_component],&ra_source,&dec_source,&l_source,&b_source);
      if(galplotdef.verbose== -600)// selectable debug
      cout<<" catalogue  source : "<<par_id[j_component]<<"  ra="<<ra_source<<" dec="<<dec_source<<" l="<<l_source<<" b="<<b_source<<endl;
      if(ra_source<0.0) cout<<" warning: source "<<par_id[j_component] <<" not found in catalogue !"<<endl;

      exclude_source_j=1;
      if(ra_source>=0.0) // consider including source  only if found in catalogue
      {
      // check all 4 regions
      if(l_source>=galplotdef.long_min1 && l_source<=galplotdef.long_max1 && b_source>=galplotdef.lat_min1 && b_source<=galplotdef. lat_max1)exclude_source_j=0;
      if(l_source>=galplotdef.long_min2 && l_source<=galplotdef.long_max2 && b_source>=galplotdef.lat_min1 && b_source<=galplotdef. lat_max1)exclude_source_j=0;
      if(l_source>=galplotdef.long_min1 && l_source<=galplotdef.long_max1 && b_source>=galplotdef.lat_min2 && b_source<=galplotdef. lat_max2)exclude_source_j=0;
      if(l_source>=galplotdef.long_min2 && l_source<=galplotdef.long_max2 && b_source>=galplotdef.lat_min2 && b_source<=galplotdef. lat_max2)exclude_source_j=0;
      }
      if(galplotdef.verbose== -600)// selectable debug
      if(exclude_source_j==1)  cout<<" dintensity:excluding  source j : "<<par_id[j_component]<<"  ra="<<ra_source<<" dec="<<dec_source<<" l="<<l_source<<" b="<<b_source<<endl;
     }
     */

    if(i_component>=n_map_component)
     exclude_source_i= exclude_source_(cat, par_id,  i_component);// AWS20050630

    if(j_component>=n_map_component)
     exclude_source_j= exclude_source_(cat, par_id,  j_component);// AWS20050630

     exclude_source=exclude_source_i + exclude_source_j; // if either excluded then exclude

     if(exclude_source==0)
     {
      dintensity_ML+=   cov_ML[i_component*n_component + j_component]
                   * component_average[i_component]* component_average[j_component];
      dintensity_MC+=   cov_MC[i_component*n_component + j_component]
                   * component_average[i_component]* component_average[j_component];
     }

    }

    dintensity_ML=sqrt(dintensity_ML);
    dintensity_ML/=(e_max-e_min)*1.e-3; // MeV^-1
    dintensity_MC=sqrt(dintensity_MC);
    dintensity_MC/=(e_max-e_min)*1.e-3; // MeV^-1

    dintensity=sqrt(dintensity);
    dintensity/=(e_max-e_min)*1.e-3; // MeV^-1


    // apply systematic error if below specified energy and if plotting map components
    // galplotdef.data_INTEGRAL_syserr=0.2;                                 //AWS20050412
    //galplotdef.data_INTEGRAL_sysemn=0.1; // MeV                          //AWS20050412
    if(e_max<galplotdef.data_INTEGRAL_sysemn * 1.e3                      //AWS20050412
       && i_component1<n_map_component)
     dintensity_ML+= intensity_total * galplotdef.data_INTEGRAL_syserr ; //AWS20050412

    dintensity=    dintensity_ML;


    cout.precision(3);
    cout<<"SPI: energy "<<e_min<<"-"<<e_max<<"keV components "
        <<i_component1+1<<"-"<<i_component2+1<<"  total intensity="<<intensity_total
        <<" +/- (ML)"<<dintensity_ML<<" (MC)"<<dintensity_MC<<endl;


    // LATeX table
    cout.precision(3);

    cout<<" "<<e_min<<"-"<<e_max <<" & "                       
        <<i_component1+1<<"-"<<i_component2+1  <<" & "  
        <<intensity_total                  <<" & "
        <<dintensity_ML                    <<" & "
        <<id                  
        <<"\\\\"                                                 // yields "\\"  
        <<"% LaTex"
	<<endl;

    

    fprintf(txtFILE,"%6.1f-%6.1f  & %d-%d  &",e_min,e_max,i_component1+1,i_component2+1);
    fprintf(txtFILE,"%5.3f &  %5.3f &  %d \\\\ ",intensity_total,dintensity_ML,id);                // "\\\\" yields "\\" 
    fprintf(txtFILE,"\n");

    e_mean=sqrt(e_min*e_max)*1e-3;


    energy_to_fit    [iid]=e_mean;
    energy_to_fit_min[iid]=e_min*1e-3;
    energy_to_fit_max[iid]=e_max*1e-3;
    intensity_to_fit [iid]=intensity_total;
    dintensity_to_fit[iid]=dintensity_ML;

    //   dintensity=intensity_total*.1;
    ymin= (intensity_total-dintensity)*e_mean*e_mean;
    ymax= (intensity_total+dintensity)*e_mean*e_mean;

    ymean=0.5*(ymin+ymax);

    if(ymin < galplotdef.gamma_spectrum_Imin)ymin = galplotdef.gamma_spectrum_Imin;
    //cout<<"emean ymin ymax="<<e_mean<<" "<<ymin<<" "<<ymax<<endl;

    // horizontal shift for multiple points at same energy
    shift=1.0 + iplot*0.05;

    if(ymax>0.0)//AWS20050513
    {


    if(galplotdef.data_INTEGRAL_mode==1||galplotdef.data_INTEGRAL_mode==3) //AWS20050509
    {
    line=new TLine(e_mean*shift,ymin,e_mean*shift,ymax);
    line->SetLineColor(color);
    line->SetLineWidth(3  );
    line->Draw();

    line=new TLine(e_min*1e-3*shift, ymean, e_max*1e-3*shift, ymean);
    line->SetLineColor(color);
    line->SetLineWidth(3  );
    line->Draw();
    }

    }// if >0

    /// analyse_MCMC_SPI_spimodfit();
  
     }//if icomponent1
    }//iplot


   }//i_energy




  } //if found

  }//id

  if(galplotdef.data_INTEGRAL_comp[2]<0) // fit only if one spectrum
  fit_spectrum_SPI();

  fprintf(txtFILE,"=============================================================\n");

  cout<<" <<<< plot_SPI_spectrum_spimodfit   "<<endl;
   return status;
}


/////////////////////////////////////////////////////////////////////////////////////


int  Galplot::analyse_MCMC_SPI_spimodfit()

{ 

  using namespace SPI_spimodfit;

  double intensity_total_sample;
  double *intensity_total_posterior;
  double *intensity_MC;
  double  intensity_total_bin;
  double  intensity_total_min;
  double  intensity_total_max;
  int   n_intensity_total_posterior;
  int     ibin;

 cout<<" >>>> analyse_MCMC_SPI     "<<endl;

 cout<<"n_theta      ="<<n_theta      <<endl;
 cout<<"n_theta_chain="<<N_theta_chain<<endl;

    if(galplotdef.verbose== -306)// selectable debug
    {
     i_chain=0;
     for (i_sample_chain=0;i_sample_chain<n_accepted;i_sample_chain++)
     {
      cout<<"sample #"<<i_sample_chain<<": ";
      for(i_theta_chain=0;i_theta_chain<N_theta_chain;i_theta_chain++,i_chain++)
          cout<<theta_chain[i_chain]<<" ";

      cout<<" logL=" <<logL_chain[i_sample_chain];
      cout<<endl;
     }
      cout<<endl;
    }

intensity_MC=new double[n_component]; //AWS20050131

for(i_component=i_component1;i_component<=i_component2;i_component++) intensity_MC[i_component]=0;


for (i_sample_chain=0;i_sample_chain<n_accepted;i_sample_chain++)
{
   for(i_component=i_component1;i_component<=i_component2;i_component++)
    intensity_MC[i_component] += theta_chain[i_sample_chain*N_theta_chain + i_component] * component_average[i_component];

}


 for(i_component=i_component1;i_component<=i_component2;i_component++)
 {
  intensity_MC[i_component] /= n_accepted;
 }

intensity_total=0.;
for(i_component=i_component1;i_component<=i_component2;i_component++)    intensity_total += intensity_MC[i_component];
 
// variance and min, max of total intensity

dintensity=0;
 intensity_total_min= 1e20;
 intensity_total_max=-1e20;

for (i_sample_chain=0;i_sample_chain<n_accepted;i_sample_chain++)
{
  intensity_total_sample=0;

  for(i_component=i_component1;i_component<=i_component2;i_component++)
    intensity_total_sample += theta_chain[i_sample_chain*N_theta_chain + i_component] * component_average[i_component];

  dintensity += pow(intensity_total_sample - intensity_total ,2);

  if  ( intensity_total_sample> intensity_total_max)intensity_total_max=intensity_total_sample;
  if  ( intensity_total_sample< intensity_total_min)intensity_total_min=intensity_total_sample;

  // cout<<intensity_total_sample<<" "<<intensity_total_min<<" "<<intensity_total_max<<endl;
}

 dintensity /= n_accepted;
 dintensity  =sqrt( dintensity);

// posterior distribution of total intensity

n_intensity_total_posterior=10;
intensity_total_posterior=new double[n_intensity_total_posterior];
intensity_total_bin=(intensity_total_max - intensity_total_min)/(n_intensity_total_posterior-1);

for (i_sample_chain=0;i_sample_chain<n_accepted;i_sample_chain++)
{
  intensity_total_sample=0;

  for(i_component=i_component1;i_component<=i_component2;i_component++)
    intensity_total_sample += theta_chain[i_sample_chain*N_theta_chain + i_component] * component_average[i_component];

  ibin= (int)((intensity_total_sample  - intensity_total_min )/intensity_total_bin);
  //cout<<intensity_total_sample<<" "<<intensity_total_min<<" "<<intensity_total_bin<<" " <<ibin<<endl;
  if(ibin>=0 && ibin < n_intensity_total_posterior)intensity_total_posterior[ibin]+=1;
}

// normalize posterior 
for(ibin=0;ibin< n_intensity_total_posterior;ibin++)intensity_total_posterior[ibin]/=n_accepted;


// convert to required units

 for(i_component=i_component1;i_component<=i_component2;i_component++)
 {
  intensity_MC[i_component]/= (e_max-e_min)*1.e-3; // MeV^-1
 }

 intensity_total        /= (e_max-e_min)*1.e-3; // MeV^-1
 dintensity             /= (e_max-e_min)*1.e-3; // MeV^-1
 intensity_total_min    /= (e_max-e_min)*1.e-3; // MeV^-1
 intensity_total_max    /= (e_max-e_min)*1.e-3; // MeV^-1
 intensity_total_bin    /= (e_max-e_min)*1.e-3; // MeV^-1

 cout<<"SPI: energy "<<e_min<<"-"<<e_max<<" keV "<<endl;
 cout<<"number of  MCMC samples = "<<n_accepted<<endl;

 for(i_component=i_component1;i_component<=i_component2;i_component++)
 {
  cout<<" component "<<i_component<<" intensity= "<<intensity_MC[i_component]<<endl; 
 }
 cout<<"       total intensity= "<<intensity_total<<" +/- "<<dintensity<<endl;
 cout<<"sample total min inten= "<<intensity_total_min <<endl;
 cout<<"sample total max inten= "<<intensity_total_max <<endl;

 cout<<" ** posterior of total intensity **" <<endl;

for(ibin=0;ibin< n_intensity_total_posterior;ibin++)
{
  cout<<"intensity "<<intensity_total_min+intensity_total_bin* ibin
      <<" - "       <<intensity_total_min+intensity_total_bin*(ibin+1)
      <<" prob = "  <<intensity_total_posterior[ibin]   <<endl;
}

 cout<<" <<<< analyse_MCMC_SPI_spimodfit     "<<endl;


 return 0;
}

///////////////////////////////////////////////////////////////////////////////////7

int Galplot::fit_spectrum_SPI()
{
  using namespace SPI_spimodfit;
  double pred_total,pred_power_law,pred_positronium,pred_511_line;
  TGraph *plot_spectrum1,*plot_spectrum2,*plot_spectrum3,*plot_spectrum4;
  int n_plot_energy,i_plot_energy;
  double Emin,Emax,E,dE;
  double linewidth;
  int colour;

  cout<<"fit_spectrum_SPI"<<endl;
  cout<<"spectrum to fit:"<<endl;
  for (iid=0;iid<nidlist;iid++)  
   cout<<iid<<" "<<energy_to_fit[iid]<<" "<<intensity_to_fit[iid]<<" "<<dintensity_to_fit[iid]<<endl;

  Double_t arglist[10];

  int ierflg;
  int npar,ipar;
  double *par,*par_error;

  npar=3;

  TMinuit *gMinuit;
  gMinuit=new TMinuit(nidlist);
  gMinuit->SetFCN(fcn_chisq);
  //gMinuit->SetFCN(fcn_chisq_deconv);

  arglist[0]=1.0;  // 1 sigma errors
  
  gMinuit->mnexcm("SET  ERR",arglist,1,ierflg); //                              sets sigma=arglist[0]
  gMinuit->mnparm(0,"A", 0.1e-7,0.1,0,0,  ierflg);

  gMinuit->mnparm(1,"g1",2.0,0.1,1.0,3.0,  ierflg);

  gMinuit->mnparm(2,"C", 0.1e-7,0.1,0,0,  ierflg);

  //  gMinuit->FixParameter(0);
  //  gMinuit->FixParameter(1);
  //  gMinuit->FixParameter(2);

  arglist[0]=500;  //maxcalls
  arglist[1]=1.e-10;  // tolerance
  gMinuit->mnexcm("MIGRAD",arglist,1,  ierflg);

  par      =new double[npar];
  par_error=new double[npar];

 
  fprintf(txtFILE,"\n INTEGRAL/SPI spectral fit for list %d \n",idlistN);
  fprintf(txtFILE,"$%6.2f\\deg<l<%6.2f\\deg$,    ",galplotdef.long_min1,galplotdef.long_max1);
  fprintf(txtFILE,"$%6.2f\\deg<l<%6.2f\\deg$,\n  ",galplotdef.long_min2,galplotdef.long_max2);
  fprintf(txtFILE,"$%6.2f\\deg<b<%6.2f\\deg$,    ",galplotdef. lat_min1,galplotdef. lat_max1);
  fprintf(txtFILE,"$%6.2f\\deg<b<%6.2f\\deg$\n ",  galplotdef. lat_min2,galplotdef. lat_max2);
  fprintf(txtFILE,"\n -------------------------------------------------------------------------- \n");

  for (ipar=0;ipar<npar;ipar++)
  {
   gMinuit->GetParameter(ipar,par[ipar],par_error[ipar]);
   cout<<"parameter "<<ipar<<"  value= "<<par[ipar]<<" error="<<par_error[ipar]<<endl;

   // TeX table
   if(ipar==0)   fprintf(txtFILE,"A       & %8.2e & %8.1e  \\\\ %% SPI spectrum fit \n",par[ipar],par_error[ipar]);
   if(ipar==1)   fprintf(txtFILE,"$\\gamma$& %8.2f & %8.2f  \\\\ %% SPI spectrum fit \n",par[ipar],par_error[ipar]);
   if(ipar==2)   fprintf(txtFILE,"C       & %8.2e & %8.1e  \\\\ %% SPI spectrum fit \n",par[ipar],par_error[ipar]);

  }


  fprintf(txtFILE,"\n -------------------------------------------------------------------------- \n");

  cout<<"spectrum  fitted:"<<endl;
  for (iid=0;iid<nidlist;iid++)
  {
 
   pred_power_law   =  par[0]*pow(energy_to_fit[iid],-par[1]) ;

   pred_positronium=0.;
   if (energy_to_fit[iid] <0.511 )
   pred_positronium =  par[2]*energy_to_fit[iid];

   pred_total=pred_power_law + pred_positronium;
   
  cout<<iid<<"   E= "<<energy_to_fit[iid]<<" pred power law="<<pred_power_law<<" positronium="<<pred_positronium<<" total="<<pred_total
   <<"  obs= "<<intensity_to_fit[iid]<<"+- "<<dintensity_to_fit[iid]<<endl;
  }


  //////// plot fitted spectrum
 if(galplotdef.verbose== -600)// selectable debug
 {
  Emin=0.02;//MeV
  Emax=1.0;
  dE= 0.001;

  n_plot_energy=  int((Emax-Emin)/dE);
  
  plot_spectrum1=new TGraph(n_plot_energy);
  plot_spectrum2=new TGraph(n_plot_energy);
  plot_spectrum3=new TGraph(n_plot_energy);


  for (i_plot_energy=0;i_plot_energy<n_plot_energy;i_plot_energy++)
  {
   E=Emin+i_plot_energy*dE;
   pred_power_law   =  par[0]*pow(E,-par[1]) ;

   pred_positronium=0.;
   if (E <0.511 )
   pred_positronium =  par[2]*E;

   pred_total=pred_power_law + pred_positronium;
   plot_spectrum1->SetPoint(i_plot_energy,E,pred_total      *E*E);
   plot_spectrum2->SetPoint(i_plot_energy,E,pred_power_law  *E*E);
   plot_spectrum3->SetPoint(i_plot_energy,E,pred_positronium*E*E);

   //cout<<"E= "<<E<<" pred_total*E*E="<<pred_total*E*E<<endl;

   }


 colour=kBlue;

 plot_spectrum1->SetMarkerColor(colour  );// also kCyan kMagenta
 plot_spectrum1->SetMarkerStyle(21);
 plot_spectrum1->SetMarkerStyle(22); // triangles
 plot_spectrum1->SetMarkerSize (0.5);//

 plot_spectrum2->SetLineStyle(1      );//1=solid 2=dash 3=dot 4=dash-dot
 plot_spectrum1->SetLineColor(colour  );// also kCyan kMagenta
 plot_spectrum1->SetLineWidth(1     );
 

 plot_spectrum2->SetLineStyle(2      );//1=solid 2=dash 3=dot 4=dash-dot
 plot_spectrum2->SetLineColor(colour  );// also kCyan kMagenta
 plot_spectrum2->SetLineWidth(1     );
 

 plot_spectrum3->SetLineStyle(3      );//1=solid 2=dash 3=dot 4=dash-dot
 plot_spectrum3->SetLineColor(colour  );// also kCyan kMagenta
 plot_spectrum3->SetLineWidth(1     );
 

 

  //spectrum->Draw("PL");  // points as markers + line
 plot_spectrum1->Draw("L");
 plot_spectrum2->Draw("L"); 
 plot_spectrum3->Draw("L");

 }//if

 //------------------------------------------- spectral deconvolution 


  gMinuit->SetFCN(fcn_chisq_deconv);
  gMinuit->mnexcm("MIGRAD",arglist,1,  ierflg);

  fprintf(txtFILE,"\n INTEGRAL/SPI spectral deconvolved fit for list %d \n",idlistN);
  fprintf(txtFILE,"$%6.2f\\deg<l<%6.2f\\deg$,    ",galplotdef.long_min1,galplotdef.long_max1);
  fprintf(txtFILE,"$%6.2f\\deg<l<%6.2f\\deg$,\n  ",galplotdef.long_min2,galplotdef.long_max2);
  fprintf(txtFILE,"$%6.2f\\deg<b<%6.2f\\deg$,    ",galplotdef. lat_min1,galplotdef. lat_max1);
  fprintf(txtFILE,"$%6.2f\\deg<b<%6.2f\\deg$\n ",  galplotdef. lat_min2,galplotdef. lat_max2);
  fprintf(txtFILE,"\n -------------------------------------------------------------------------- \n");

  for (ipar=0;ipar<npar;ipar++)
  {
   gMinuit->GetParameter(ipar,par[ipar],par_error[ipar]);
   cout<<"parameter "<<ipar<<"  value= "<<par[ipar]<<" error="<<par_error[ipar]<<endl;

   // TeX table
   if(ipar==0)   fprintf(txtFILE,"A       & %8.2e & %8.1e  \\\\ %% SPI spectrum fit \n",par[ipar],par_error[ipar]);
   if(ipar==1)   fprintf(txtFILE,"$\\gamma$& %8.2f & %8.2f  \\\\ %% SPI spectrum fit \n",par[ipar],par_error[ipar]);
   if(ipar==2)   fprintf(txtFILE,"C       & %8.2e & %8.1e  \\\\ %% SPI spectrum fit \n",par[ipar],par_error[ipar]);

  }


  fprintf(txtFILE,"\n -------------------------------------------------------------------------- \n");

  cout<<"deconvolved spectrum  fitted:"<<endl;

  for (iid=0;iid<nidlist;iid++)
  {
 
   pred_power_law   =  par[0]*pow(energy_to_fit[iid],-par[1]) ;

   pred_positronium=0.;
   if (energy_to_fit[iid] <0.511 )
   pred_positronium =  par[2]*energy_to_fit[iid];

   pred_total=pred_power_law + pred_positronium;
   
  cout<<iid<<"   E= "<<energy_to_fit[iid]<<" pred power law="<<pred_power_law<<" positronium="<<pred_positronium<<" total="<<pred_total
   <<"  obs= "<<intensity_to_fit[iid]<<"+- "<<dintensity_to_fit[iid]<<endl;
  }




  //////// plot fitted spectrum
 if(galplotdef.verbose== -600)// selectable debug
 {
  Emin=0.02;//MeV
  Emax=1.0;
  dE= 0.001;

  n_plot_energy=  int((Emax-Emin)/dE);
  
  plot_spectrum1=new TGraph(n_plot_energy);
  plot_spectrum2=new TGraph(n_plot_energy);
  plot_spectrum3=new TGraph(n_plot_energy);


  for (i_plot_energy=0;i_plot_energy<n_plot_energy;i_plot_energy++)
  {
   E=Emin+i_plot_energy*dE;
   pred_power_law   =  par[0]*pow(E,-par[1]) ;

   pred_positronium=0.;
   if (E <0.511 )
   pred_positronium =  par[2]*E;

   pred_total=pred_power_law + pred_positronium;
   plot_spectrum1->SetPoint(i_plot_energy,E,pred_total      *E*E);
   plot_spectrum2->SetPoint(i_plot_energy,E,pred_power_law  *E*E);
   plot_spectrum3->SetPoint(i_plot_energy,E,pred_positronium*E*E);

   //cout<<"E= "<<E<<" pred_total*E*E="<<pred_total*E*E<<endl;

   }

  
  colour=kGreen;

 plot_spectrum1->SetMarkerColor(colour  );// also kCyan kMagenta
 plot_spectrum1->SetMarkerStyle(21);
 plot_spectrum1->SetMarkerStyle(22); // triangles
 plot_spectrum1->SetMarkerSize (0.5);//

 plot_spectrum2->SetLineStyle(1      );//1=solid 2=dash 3=dot 4=dash-dot
 plot_spectrum1->SetLineColor(colour  );// also kCyan kMagenta
 plot_spectrum1->SetLineWidth(1     );
 

 plot_spectrum2->SetLineStyle(2      );//1=solid 2=dash 3=dot 4=dash-dot
 plot_spectrum2->SetLineColor(colour  );// also kCyan kMagenta
 plot_spectrum2->SetLineWidth(1     );
 

 plot_spectrum3->SetLineStyle(3      );//1=solid 2=dash 3=dot 4=dash-dot
 plot_spectrum3->SetLineColor(colour  );// also kCyan kMagenta
 plot_spectrum3->SetLineWidth(1     );
 

 

  //spectrum->Draw("PL");  // points as markers + line
 plot_spectrum1->Draw("L");
 plot_spectrum2->Draw("L"); 
 plot_spectrum3->Draw("L");


   }//if 

 //------------------------------------------------------------------------------
 cout<<"fitting power-law + positronium + line"<<endl;

  gMinuit->SetFCN(fcn_chisq_deconv2);
  gMinuit->mnparm(0,"A", 0.1e-7,0.1,1.0e-8,100.0,  ierflg);
  gMinuit->mnparm(1,"g1",2.0,   0.1,1.0,   3.0,    ierflg);
  gMinuit->mnparm(2,"C", 0.1e-7,0.1,1.0e-8,100.0,  ierflg);
  gMinuit->mnparm(3,"D", 0.1e-7,0.1,1.0e-8,100.0,  ierflg);
  gMinuit->mnexcm("MIGRAD",arglist,1,  ierflg);



  fprintf(txtFILE,"\n INTEGRAL/SPI spectral deconvolved fit for list %d \n",idlistN);
  fprintf(txtFILE,"$%6.2f\\deg<l<%6.2f\\deg$,    ",galplotdef.long_min1,galplotdef.long_max1);
  fprintf(txtFILE,"$%6.2f\\deg<l<%6.2f\\deg$,\n  ",galplotdef.long_min2,galplotdef.long_max2);
  fprintf(txtFILE,"$%6.2f\\deg<b<%6.2f\\deg$,    ",galplotdef. lat_min1,galplotdef. lat_max1);
  fprintf(txtFILE,"$%6.2f\\deg<b<%6.2f\\deg$\n ",  galplotdef. lat_min2,galplotdef. lat_max2);
  fprintf(txtFILE,"\n -------------------------------------------------------------------------- \n");

  npar     =4;
  par      =new double[npar];
  par_error=new double[npar];
  for (ipar=0;ipar<npar;ipar++)
  {
   gMinuit->GetParameter(ipar,par[ipar],par_error[ipar]);
   cout<<"parameter "<<ipar<<"  value= "<<par[ipar]<<" error="<<par_error[ipar]<<endl;

   // TeX table
   if(ipar==0)   fprintf(txtFILE,"A        & %8.2e & %8.1e  \\\\ %% SPI spectrum fit \n",par[ipar],par_error[ipar]);
   if(ipar==1)   fprintf(txtFILE,"$\\gamma$& %8.2f & %8.2f  \\\\ %% SPI spectrum fit \n",par[ipar],par_error[ipar]);
   if(ipar==2)   fprintf(txtFILE,"C        & %8.2e & %8.1e  \\\\ %% SPI spectrum fit \n",par[ipar],par_error[ipar]);

   linewidth=0.006; // give parameter for line integrated over 6 keV energy
   if(ipar==3)   fprintf(txtFILE,"D        & %8.2e & %8.1e  \\\\ %% SPI spectrum fit \n",par[ipar]*linewidth,par_error[ipar]*linewidth);
  }


  fprintf(txtFILE,"\n -------------------------------------------------------------------------- \n");

  cout<<"deconvolved spectrum  fitted to power-law + positronium + line:"<<endl;

  for (iid=0;iid<nidlist;iid++)
  {
 
   pred_power_law   =  par[0]*pow(energy_to_fit[iid],-par[1]) ;

   pred_positronium=0.;
   if (energy_to_fit[iid] <0.511 )
   pred_positronium =  par[2]*energy_to_fit[iid];





   pred_511_line   =0;
   if(energy_to_fit[iid] >=0.508 &&
      energy_to_fit[iid] <=0.514    ) pred_511_line= par[3]                     ;

   pred_total=pred_power_law + pred_positronium+ pred_511_line;
   
  cout<<iid<<"   E= "<<energy_to_fit[iid]<<" pred power law="<<pred_power_law<<" positronium="<<pred_positronium
      <<" pred_511_line="<<pred_511_line
      <<" total="<<pred_total
     <<"  obs= "<<intensity_to_fit[iid]<<"+- "<<dintensity_to_fit[iid]<<endl;


  }

  //////// plot fitted spectrum power-law + positronium + line

  Emin=0.02;//MeV
  Emax=1.0;
  dE= 0.001;

  n_plot_energy=  int((Emax-Emin)/dE);
  
  plot_spectrum1=new TGraph(n_plot_energy);
  plot_spectrum2=new TGraph(n_plot_energy);
  plot_spectrum3=new TGraph(n_plot_energy);
  plot_spectrum4=new TGraph(n_plot_energy);

  for (i_plot_energy=0;i_plot_energy<n_plot_energy;i_plot_energy++)
  {
   E=Emin+i_plot_energy*dE;
   pred_power_law   =  par[0]*pow(E,-par[1]) ;

   pred_positronium=0.;
   if (E <0.511 )
   pred_positronium =  par[2]*E;

   pred_511_line   =0;
   if(E                  >=0.508 &&
      E                  <=0.514    ) pred_511_line= par[3]                     ;


   pred_total=pred_power_law + pred_positronium+pred_511_line;
   plot_spectrum1->SetPoint(i_plot_energy,E,pred_total      *E*E);
   plot_spectrum2->SetPoint(i_plot_energy,E,pred_power_law  *E*E);
   plot_spectrum3->SetPoint(i_plot_energy,E,pred_positronium*E*E);
   plot_spectrum4->SetPoint(i_plot_energy,E,pred_511_line   *E*E);

   //cout<<"E= "<<E<<" pred_total*E*E="<<pred_total*E*E<<endl;

   }

  
 colour=kRed  ;

 plot_spectrum1->SetMarkerColor(colour  );// also kCyan kMagenta
 plot_spectrum1->SetMarkerStyle(21);
 plot_spectrum1->SetMarkerStyle(22); // triangles
 plot_spectrum1->SetMarkerSize (0.5);//

 plot_spectrum2->SetLineStyle(1      );//1=solid 2=dash 3=dot 4=dash-dot
 plot_spectrum1->SetLineColor(colour  );// also kCyan kMagenta
 plot_spectrum1->SetLineWidth(1     );
 

 plot_spectrum2->SetLineStyle(2      );//1=solid 2=dash 3=dot 4=dash-dot
 plot_spectrum2->SetLineColor(colour  );// also kCyan kMagenta
 plot_spectrum2->SetLineWidth(1     );
 

 plot_spectrum3->SetLineStyle(3      );//1=solid 2=dash 3=dot 4=dash-dot
 plot_spectrum3->SetLineColor(colour  );// also kCyan kMagenta
 plot_spectrum3->SetLineWidth(1     );
 

 

  //spectrum->Draw("PL");  // points as markers + line
 if(galplotdef.data_INTEGRAL_mode==2||galplotdef.data_INTEGRAL_mode==3) //AWS20050509
 {
 plot_spectrum1->Draw("L");
 plot_spectrum2->Draw("L"); 
 plot_spectrum3->Draw("L");
 }

  return 0;
}



////////////////////////////////////////////////////////////////////////////////////////////////////////


static void fcn_chisq(int &npar,double *gin,double &f,double *par,int iflag)
{
  using namespace SPI_spimodfit;
  int i; double pred;
 
  f=0;
  
  for(i=0;i<nidlist;i++)
  {
   
   pred                            = par[0]*pow(energy_to_fit[i],-par[1]);
   if(energy_to_fit[i]<0.511) pred+= par[2]    *energy_to_fit[i];

   f+= pow( (intensity_to_fit[i]-pred)/dintensity_to_fit[i], 2) ;
  
   // cout<<energy_to_fit[i]<<" "<<intensity_to_fit[i]<<" "<<pred<<endl; 
   }
  // cout<<f<<endl;
}

//////////////////////////////////////////////////////////////////////////////////

static void fcn_chisq_deconv(int &npar,double *gin,double &f,double *par,int iflag)
{
  
  using namespace SPI_spimodfit;

  int i; double pred;
  int ii_E,jj_E;


  double E_min_in ,E_max_in;
  double E_min_out,E_max_out;

  static double *E_in, *E_out_lo,*E_out_hi,dE_in,dE_out; // interpolated response energies
  static int   n_E_in,n_E_out;                           // interpolated response energies


  
  static int fcn_chisq_deconv_init=0;
  static double *intensity_conv;

  static SPIERSP spiersp;
 

  char directory[500];

  if (fcn_chisq_deconv_init==0) // initialize
 {
  cout<<"fcn_chisq_deconv"<<endl;

  fcn_chisq_deconv_init=1;

  E_min_in =0.010 ;   // MeV  
  E_max_in =2.000 ;   // MeV
     dE_in= 0.00201;  // MeV





  n_E_in =int((E_max_in  - E_min_in )/dE_in );



  n_E_out=nidlist;

  E_in     =new double[n_E_in];
  E_out_lo =new double[n_E_out];
  E_out_hi =new double[n_E_out];

  for (ii_E=0;ii_E<n_E_in ;ii_E++)    E_in[ii_E] =(E_min_in + ii_E*dE_in) * 1e3;//since response matrix uses keV


  
  
  for (jj_E=0;jj_E<n_E_out;jj_E++)
  {
    E_out_lo[jj_E]=energy_to_fit_min[jj_E]*1e3; // response matrix is in keV
    E_out_hi[jj_E]=energy_to_fit_max[jj_E]*1e3; // but galplot works in  MeV
   }
  
  strcpy(directory,
"/afs/ipp-garching.mpg.de/mpe/gamma/instruments/integral/data/ic/osa-3.0/ic/spi/rsp/");


    spiersp.read(directory,00,18,E_in,n_E_in,E_out_lo,E_out_hi,n_E_out);

  // response is probability per output bin, convert to MeV^-1 as for intensity_to_fit
  for (ii_E=0;ii_E<n_E_in ;ii_E++)
  for (jj_E=0;jj_E<n_E_out;jj_E++) 
    spiersp.response[ ii_E*spiersp.n_E_out + jj_E ] /= (energy_to_fit_max[jj_E]-energy_to_fit_min[jj_E] ) ;

    intensity_conv=new double[n_E_out];
 
    for (ii_E=0;ii_E<n_E_in ;ii_E++) E_in[ii_E]*=1e-3  ; // since the fitting works in MeV


    /*
   cout<<"response  as used  by fcn_chisq_deconv:"<<endl;
   spiersp.print();
    */

   }//if init==0





    //  cout<<"spiersp.response["<< ii_E*spiersp.n_E_out + jj_E  <<"  ] " <<spiersp.response[ ii_E*spiersp.n_E_out + jj_E ]<<endl;


  for (jj_E=0;jj_E<n_E_out;jj_E++)    intensity_conv[jj_E]=0.;


  for (ii_E=0;ii_E<n_E_in ;ii_E++) 
  {
   pred                            = par[0]*pow( E_in[ii_E]  ,-par[1]);
   if( E_in[ii_E]     <0.511) pred+= par[2]    * E_in[ii_E]     ;

   pred *= dE_in; 
 
   for (jj_E=0;jj_E<n_E_out;jj_E++)
   {
    intensity_conv[jj_E] += pred * spiersp.response[ii_E*spiersp.n_E_out + jj_E];
    // cout<<ii_E<<" "<<jj_E<<" "<<pred<<" "<<spiersp.response[ii_E*spiersp.n_E_out + jj_E]<<" "<<intensity_conv[jj_E]<<endl;
   }
  }


  f=0;
  
  for(i=0;i<nidlist;i++)
  {    
   f+= pow( (intensity_to_fit[i]-intensity_conv[i])/dintensity_to_fit[i], 2) ;
  
   // cout<<"energy intensity_to_fit intensity_conv "<<energy_to_fit[i]<<" "<<intensity_to_fit[i]<<" "<<intensity_conv[i]<<endl; 
  }





  // cout<<"fcn_chisq_deconv  f= "<<f<<endl;
  //     exit(0);
}


//////////////////////////////////////////////////////////////////////////////////

static void fcn_chisq_deconv2(int &npar,double *gin,double &f,double *par,int iflag)
{
  // power-law, continuum, 511 keV line

  using namespace SPI_spimodfit;

  int i; double pred;
  int ii_E,jj_E;


  double E_min_in ,E_max_in;
  double E_min_out,E_max_out;

  static double *E_in, *E_out_lo,*E_out_hi,dE_in,dE_out; // interpolated response energies
  static int   n_E_in,n_E_out;                           // interpolated response energies


  
  static int fcn_chisq_deconv_init=0;
  static double *intensity_conv;

  static SPIERSP spiersp;
 

  char directory[500];

  if (fcn_chisq_deconv_init==0) // initialize
 {
  cout<<"fcn_chisq_deconv2"<<endl;

  fcn_chisq_deconv_init=1;

  E_min_in =0.010 ;   // MeV  
  E_max_in =2.000 ;   // MeV
     dE_in= 0.00051;  // MeV





  n_E_in =int((E_max_in  - E_min_in )/dE_in );



  n_E_out=nidlist;

  E_in     =new double[n_E_in];
  E_out_lo =new double[n_E_out];
  E_out_hi =new double[n_E_out];

  for (ii_E=0;ii_E<n_E_in ;ii_E++)    E_in[ii_E] =(E_min_in + ii_E*dE_in) * 1e3;//since response matrix uses keV


  
  
  for (jj_E=0;jj_E<n_E_out;jj_E++)
  {
    E_out_lo[jj_E]=energy_to_fit_min[jj_E]*1e3; // response matrix is in keV
    E_out_hi[jj_E]=energy_to_fit_max[jj_E]*1e3; // but galplot works in  MeV
   }
  
  strcpy(directory,
"/afs/ipp-garching.mpg.de/mpe/gamma/instruments/integral/data/ic/osa-3.0/ic/spi/rsp/");


    spiersp.read(directory,00,18,E_in,n_E_in,E_out_lo,E_out_hi,n_E_out);

  // response is probability per output bin, convert to MeV^-1 as for intensity_to_fit
  for (ii_E=0;ii_E<n_E_in ;ii_E++)
  for (jj_E=0;jj_E<n_E_out;jj_E++) 
    spiersp.response[ ii_E*spiersp.n_E_out + jj_E ] /= (energy_to_fit_max[jj_E]-energy_to_fit_min[jj_E] ) ;

    intensity_conv=new double[n_E_out];
 
    for (ii_E=0;ii_E<n_E_in ;ii_E++) E_in[ii_E]*=1e-3  ; // since the fitting works in MeV


    /*
   cout<<"response  as used  by fcn_chisq_deconv:"<<endl;
   spiersp.print();
    */

   }//if init==0





    //  cout<<"spiersp.response["<< ii_E*spiersp.n_E_out + jj_E  <<"  ] " <<spiersp.response[ ii_E*spiersp.n_E_out + jj_E ]<<endl;


  for (jj_E=0;jj_E<n_E_out;jj_E++)    intensity_conv[jj_E]=0.;


  for (ii_E=0;ii_E<n_E_in ;ii_E++) 
  {
   pred                            = par[0]*pow( E_in[ii_E]  ,-par[1]);

   if( E_in[ii_E]     <0.511) pred+= par[2]    * E_in[ii_E]     ;

   if( E_in[ii_E]>=0.508 &&
       E_in[ii_E]<=0.514    ) pred+= par[3]                     ;

   pred *= dE_in; 
 
   for (jj_E=0;jj_E<n_E_out;jj_E++)
   {
    intensity_conv[jj_E] += pred * spiersp.response[ii_E*spiersp.n_E_out + jj_E];
    // cout<<ii_E<<" "<<jj_E<<" "<<pred<<" "<<spiersp.response[ii_E*spiersp.n_E_out + jj_E]<<" "<<intensity_conv[jj_E]<<endl;
   }
  }


  f=0;
  
  for(i=0;i<nidlist;i++)
  {    
   f+= pow( (intensity_to_fit[i]-intensity_conv[i])/dintensity_to_fit[i], 2) ;
  
   // cout<<"energy intensity_to_fit intensity_conv "<<energy_to_fit[i]<<" "<<intensity_to_fit[i]<<" "<<intensity_conv[i]<<endl; 
  }





  // cout<<"fcn_chisq_deconv2 f= "<<f<<endl;
  //     exit(0);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////
int Galplot::exclude_source_(CAT cat,char  **par_id, int i_component)// AWS20050630
    {
      int exclude_source;
      double ra_source,dec_source,l_source,b_source;
      double long_min1_, long_min2_,long_max1_, long_max2_;
      double  lat_min1_,  lat_min2_, lat_max1_,  lat_max2_;

      // allow for definition of bins via bin centres

      long_min1_ = galplotdef.long_min1 - 0.5*galplotdef.d_long;
      long_min2_ = galplotdef.long_min2 - 0.5*galplotdef.d_long;
      long_max1_ = galplotdef.long_max1 + 0.5*galplotdef.d_long;
      long_max2_ = galplotdef.long_max2 + 0.5*galplotdef.d_long;

       lat_min1_ = galplotdef. lat_min1 - 0.5*galplotdef.d_lat ;
       lat_min2_ = galplotdef. lat_min2 - 0.5*galplotdef.d_lat ;
       lat_max1_ = galplotdef. lat_max1 + 0.5*galplotdef.d_lat ;
       lat_max2_ = galplotdef. lat_max2 + 0.5*galplotdef.d_lat ;

      cat.find(par_id[i_component],&ra_source,&dec_source,&l_source,&b_source);
      cout<<" catalogue  source : "<<par_id[i_component]<<"  ra="<<ra_source<<" dec="<<dec_source<<" l="<<l_source<<" b="<<b_source<<endl;
      if(ra_source<0.0) cout<<" warning: source "<<par_id[i_component] <<" not found in catalogue !"<<endl;

      exclude_source=1;
      if(ra_source>=0.0) // consider including source  only if found in catalogue
      {
      // check all 4 regions
	/* 
      if(l_source>=galplotdef.long_min1 && l_source<=galplotdef.long_max1 && b_source>=galplotdef.lat_min1 && b_source<=galplotdef. lat_max1)exclude_source=0;
      if(l_source>=galplotdef.long_min2 && l_source<=galplotdef.long_max2 && b_source>=galplotdef.lat_min1 && b_source<=galplotdef. lat_max1)exclude_source=0;
      if(l_source>=galplotdef.long_min1 && l_source<=galplotdef.long_max1 && b_source>=galplotdef.lat_min2 && b_source<=galplotdef. lat_max2)exclude_source=0;
      if(l_source>=galplotdef.long_min2 && l_source<=galplotdef.long_max2 && b_source>=galplotdef.lat_min2 && b_source<=galplotdef. lat_max2)exclude_source=0;
	*/

      if(l_source>=           long_min1_&& l_source<=           long_max1_&& b_source>=           lat_min1_&& b_source<=            lat_max1_)exclude_source=0;
      if(l_source>=           long_min2_&& l_source<=           long_max2_&& b_source>=           lat_min1_&& b_source<=            lat_max1_)exclude_source=0;
      if(l_source>=           long_min1_&& l_source<=           long_max1_&& b_source>=           lat_min2_&& b_source<=            lat_max2_)exclude_source=0;
      if(l_source>=           long_min2_&& l_source<=           long_max2_&& b_source>=           lat_min2_&& b_source<=            lat_max2_)exclude_source=0;

      }

      if(exclude_source==0)  cout<<" exclude_source_:including  source : "<<par_id[i_component]<<"  ra="<<ra_source<<" dec="<<dec_source<<" l="<<l_source<<" b="<<b_source<<endl; //AWS20061031
      if(exclude_source==1)  cout<<" exclude_source_:excluding  source : "<<par_id[i_component]<<"  ra="<<ra_source<<" dec="<<dec_source<<" l="<<l_source<<" b="<<b_source<<endl;
     

return exclude_source;
}
///////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////
/// enable this for stand-alone test

/*
int main(int argc,char*argv[])   
{
  cout<<"testing plot_SPI_spectrum_spimodfit"<<endl;
     char version[]="47";   //AWS20050222
 
 
   cout<<">>>>galplot version "<<version<<endl;
 
   if(argc!=2){cout<<"no galdef file specified!"<<endl;return -1;}
   cout<<argc<<" "<<argv[0]<<" "<<argv[1]<<endl;
 
   if(configure.init() !=0)return 1;
   cout<<configure.galdef_directory<<endl;// just a test
 
   if(galdef.read  (version,argv[1],configure.galdef_directory) !=0) return 1;
 
   if(galplotdef.read  (version,argv[1],configure.galdef_directory) !=0) return 1;

  configure.init();
  
  CAT cat;
  cat.read(configure.fits_directory);
  cat.print();
  double ra,dec,l,b;
  cat.find("Crab",&ra,&dec,&l,&b);cout<<ra<<" "<<dec<<" "<<l<<" "<<b<<endl;
  cat.find("1RXS J232953.9+06281",&ra,&dec,&l,&b);cout<<ra<<" "<<dec<<" "<<l<<" "<<b<<endl;
  cat.find("testcase",&ra,&dec,&l,&b);cout<<ra<<" "<<dec<<" "<<l<<" "<<b<<endl;
  
  txtFILE=fopen("junk","w");

  plot_SPI_spectrum_spimodfit(0);

  return 0;

}

*/
