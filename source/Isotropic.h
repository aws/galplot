


class Isotropic
{
 public:

  int n_energy;

  double  *energy, *spectrum;

       
  int read (char *directory, char*filename);      
  void print();

  double interpolated(double energy_,int debug);

  double integrated  (double energy_1, double energy_2, int debug); //AWS20081202

  
};
