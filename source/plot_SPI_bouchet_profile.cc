

using namespace std;
#include"Galplot.h"                 
#include"galprop_classes.h"
#include"galprop.h"
#include"constants.h"
#include"fitsio.h"
#include"SPIERSP.h"
   

/////////////////////////////////////////////////////////////////////////
namespace SPI_bouchet//  to protect local variables and share with local routines
{
   int status=0;
   char  infile[1000];
   int id,hdunum;

 

int longprof,latprof;

int i_lat,i_long;
int ii_long,ii_lat;
int il_min,il_max,ib_min,ib_max;
int il_min1,il_max1,ib_min1,ib_max1;
int il_min2,il_max2,ib_min2,ib_max2;
int il_temp;

double long_min,long_max,lat_min,lat_max;
double l,b,l_off;

int    i_E_range;
double e_min,e_max;

char name[100],canvastitle[100], workstring1[100],workstring2[100],workstring3[100],workstring4[100];
char psfile[400];
}



/////////////////////////////////////////////////////////////
int Galplot::  plot_SPI_bouchet_profile(int longprof,int latprof,int mode)
///////////////////////////////////////////////////////////
{
  using namespace SPI_bouchet;

   cout<<" >>>> plot_SPI_bouchet_profile    "<<endl;


 for(i_E_range= 1; i_E_range<=2;i_E_range++)
 {
  
  if(i_E_range==0){ e_min=0.025;  e_max=0.050;}
  if(i_E_range==1){ e_min=0.050;  e_max=0.100;}
  if(i_E_range==2){ e_min=0.100;  e_max=0.200;}
  if(i_E_range==3){ e_min=0.200;  e_max=0.600;}
  if(i_E_range==4){ e_min=1.800;  e_max=7.800;}

cout<<"e_min= "<<e_min<< " MeV " ;
cout<<"e_max= "<<e_max<< " MeV "<<endl;

 

TCanvas *c1;

TH1D *profile,*profile_CO,*profile_NIR;



int ncolors=1; int *colors=0;
gStyle->SetPalette(ncolors,colors);

  //====== see HowTo Style: and do before creating canvas
 
 
   TStyle *plain  = new TStyle("Plain","Plain Style (no colors/fill areas)"); 

   
   plain->SetCanvasBorderMode(0);
   plain->SetPadBorderMode(0);
   plain->SetPadColor(0);
   plain->SetCanvasColor(0);
   plain->SetTitleColor(0);
   plain->SetStatColor(0);
   gROOT->SetStyle("Plain");

 // names must be different or canvas disappears

  
  sprintf(name," %5.3f - %5.3f MeV",e_min,e_max);
  strcpy(canvastitle,"galdef_");
  strcat(canvastitle,galdef.galdef_ID);
  strcat(canvastitle,name);


  



  // --------------------------------- profiles

//------------------------------     axes      -------------------------
  TF1 *f1=new TF1("f1","-x",   0,180); // root manual p. 149
  TF1 *f2=new TF1("f2","-x", 180,340); // root manual p. 149

  //                      xmin ymin xmax ymax, funcname   ndiv chopt gridlength
  TGaxis *axis1=new TGaxis(-180,-90 ,  0.,-90  , "f1",       9,""                );
  axis1->SetLabelSize(0.03);
  TGaxis *axis2=new TGaxis(  20,-90 ,180.,-90  , "f2",       8,""                );
  axis2->SetLabelSize(0.03);
  axis2->SetTitle("Galactic longitude");
  // fill in 0-340 between two axis segments, unlabelled
  // at present has no visibile effect but could be useful if more minor tickmarks added
  //                         xmin ymin xmax ymax   wmin wmax ndiv chopt gridlength
  TGaxis *axis3=new TGaxis(  0,  -90.  , 20., -90., 0., 20. ,0, "U"               );

  // construct conventional Galactic longitude axis in two segments, 180-0 and 340-180
  f1=new TF1("f1","-x",   0,180); // root manual p. 149
  f2=new TF1("f2","-x", 180,340); // root manual p. 149

  //                      xmin ymin xmax ymax, funcname   ndiv chopt gridlength
          axis1=new TGaxis(-180,0.  ,  0.,0.   , "f1",       9,""                );
  axis1->SetLabelSize(0.02);
          axis2=new TGaxis(  20,0.  ,180.,0.   , "f2",       8,""                );
  axis2->SetLabelSize(0.02);
  axis2->SetTitle("Galactic longitude");

  
 //////////////////////////////////////////////////////////////////////////////////////////////





  // ----------------------------------longitude profile
  /*
  if(longprof==1)
    // not yet implemented
  {
  ib_min1=int((galplotdef.lat_min1-lat_min+.0001)/ fits_map.CDELT[1]);
  ib_max1=int((galplotdef.lat_max1-lat_min+.0001)/ fits_map.CDELT[1]);
  ib_min2=int((galplotdef.lat_min2-lat_min+.0001)/ fits_map.CDELT[1]);
  ib_max2=int((galplotdef.lat_max2-lat_min+.0001)/ fits_map.CDELT[1]);

  cout<<"ib_min1 ibmax1:"<<ib_min1<<" "<<ib_max1<<endl;
  cout<<"ib_min2 ibmax2:"<<ib_min2<<" "<<ib_max2<<endl;

  profile=     map->ProjectionX("profile1" ,ib_min1,ib_max1); // here name must be distinct
  profile->Add(map->ProjectionX("dummy1" ,  ib_min2,ib_max2),1.0);

  profile->Scale(1.0/(ib_max1-ib_min1+1 + ib_max2-ib_min2+1));
 
  profile->GetXaxis()->SetTitle("Galactic longitude");
  profile->GetXaxis()->SetNdivisions(0); // since axis drawn separately
  profile->GetYaxis()->SetTitle("intensity, cm^{-2} sr^{-1} s^{-1}  ");
  profile->GetYaxis()->SetTitleOffset(1.2);
  profile->GetYaxis()->SetLabelSize  (0.030);

  profile->SetTitle(""         ); //removes text written in box on plot
  //  profile->SetMaximum(profile->GetMaximum()*1.5); // to allow for data with larger range
 
  if(galplotdef.lat_log_scale==0)
   profile->SetMaximum(profile->GetMaximum()*1.5); // to allow for data with larger range
  if(galplotdef.lat_log_scale==1)
   profile->SetMaximum(profile->GetMaximum()*2.0); 
  if(galplotdef.lat_log_scale==1)                     
   profile->SetMinimum(profile->GetMaximum()/1e5);   



  TGaxis::SetMaxDigits(4);  // see root #1329 and mail root 20 Aug 2002

  profile->SetStats(kFALSE);
  profile->SetLineStyle(1     );// 1=solid 2=dash
  profile->SetLineWidth(3     );
  profile->SetLineColor(kBlack);


  strcpy(name,"longitude profile");
   strcpy(canvastitle,name);
 

  c1=new TCanvas(name,canvastitle,400,250,600,600);
  if(galplotdef.long_log_scale==1)c1->SetLogy(); //a TCanvas is a TPad, TPad has this function.  
  profile->Draw("L");

  

//------------------------------     axes      -------------------------
  

  axis1->Draw();   axis2->Draw();   axis3->Draw();

  }// longprof

  */



  // --------------------------- latitude profile


  if(latprof==1)
  {

    /*
email from Laurent Bouchet 9 Jan 2008
Profiles from arXiv:0801.2086  Fig 5
flux per 48 * 2.6 deg l*b pixel

NB CO, NIR column names were reversed in original mail. Here corrected.

 25-50 keV (integrated for -24 < l < 24 deg.)
          b (deg)        flux           erro           NIR               CO        << corrected NIR CO column names
      -20.800000    0.0015998552   0.00080309401   0.00072747813   0.00075489162
      -18.200000   0.00029767129   0.00067929906   0.00076496091   0.00076025177
      -15.600000    0.0018138018   0.00058841981   0.00083319280   0.00072655357
      -13.000000    0.0017497492   0.00052972177   0.00090826309   0.00072603138
      -10.400000    0.0013274201   0.00046267708    0.0010015689   0.00072608237
      -7.8000000    0.0013371818   0.00043827745    0.0011701325   0.00075322805
      -5.2000000    0.0018067316   0.00042025153    0.0016000905   0.00083279509
      -2.6000000    0.0025304953   0.00042055528    0.0034864478    0.0021524196
       0.0000000    0.0093474188   0.00076251478    0.0096438525     0.010569939
       2.6000000    0.0058992010   0.00044768183    0.0042751904    0.0019194194
       5.2000000    0.0015126625   0.00040408957    0.0014881807    0.0011566868
       7.8000000    0.0011098151   0.00044703105    0.0010141787    0.0011906600
       10.400000    0.0031907210   0.00045530512   0.00086969277   0.00099456703
       13.000000   0.00063594426   0.00047380103   0.00080908119   0.00087472696
       15.600000   0.00054661513   0.00053302461   0.00075095995    0.0010303197
       18.200000   0.00064964823   0.00057676040   0.00072774815   0.00097001052
       20.800000    0.0013613295   0.00064781397   0.00072603138   0.00086450206

50-100 keV (integrated for -24 < l < 24 deg.)
          b (deg)       flux           erro           NIR                CO                 
      -20.800000   6.6486422e-05    0.0012273086   0.00083169063   0.00084489669
      -18.200000    0.0010445070    0.0010307939   0.00084971515   0.00084747863
      -15.600000    0.0014115207   0.00085487077   0.00088252613   0.00083124646
      -13.000000   0.00055941047   0.00075990202   0.00091862553   0.00083099493
      -10.400000    0.0018830788   0.00065008077   0.00096349391   0.00083101949
      -7.8000000   0.00049471855   0.00060397085    0.0010445518   0.00084409536
      -5.2000000    0.0018094770   0.00058667386    0.0012513078   0.00088242219
      -2.6000000    0.0019962361   0.00056808956    0.0021584090    0.0015180752
       0.0000000    0.0054503186   0.00064642238    0.0051193484    0.0055727294
       2.6000000    0.0026214142   0.00061978235    0.0025376953    0.0014058408
       5.2000000   0.00039503481   0.00056840364    0.0011974932    0.0010384383
       7.8000000    0.0013199281   0.00059244911   0.00096955765    0.0010548030
       10.400000    0.0012431506   0.00062658729   0.00090007804   0.00096034649
       13.000000   0.00092170350   0.00066368033   0.00087093147   0.00090262045
       15.600000    0.0010397129   0.00073933042   0.00084298245   0.00097756830
       18.200000   0.00031439027   0.00085483347   0.00083182048   0.00094851781
       20.800000   0.00092698226   0.00096460090   0.00083099493   0.00089769519

100-200 keV (integrated for -24 < l < 24 deg.)
          b (deg)        flux           erro           NIR                CO                       
      -20.800000   0.00018423101    0.0010785995   0.00040081825   0.00040885309
      -18.200000   0.00057888468   0.00091710290   0.00041224010   0.00041042727
      -15.600000   0.00014820052   0.00075313225   0.00043303191   0.00040053075
      -13.000000   0.00087864872   0.00063292765   0.00045590753   0.00040037739
      -10.400000   0.00083597678   0.00055902288   0.00048433992   0.00040039237
      -7.8000000   0.00068194231   0.00051020555   0.00053570507   0.00040836453
      -5.2000000    0.0010400436   0.00048990448   0.00066672305   0.00043173185
      -2.6000000    0.0013320365   0.00047257359    0.0012415389   0.00081928041
       0.0000000    0.0031205474   0.00050077305    0.0031178399    0.0032913451
       2.6000000    0.0011120367   0.00050891479    0.0014818867   0.00075085268
       5.2000000   0.00064093262   0.00048878001   0.00063262158   0.00052685267
       7.8000000   0.00027411109   0.00050074142   0.00048818242   0.00053682994
       10.400000   0.00059720125   0.00052946783   0.00044415427   0.00047924119
       13.000000   0.00044884495   0.00057725951   0.00042568455   0.00044404644
       15.600000   2.7996345e-08   0.00065300352   0.00040797369   0.00048974108
       18.200000   0.00035978770   0.00075675492   0.00040090053   0.00047202941
       20.800000   0.00028152789   0.00085005628   0.00040037739   0.00044104359
       
200-600  keV (integrated for -24 < l < 24 deg.)
         b (deg)        flux           erro            NIR              CO           Gauss 8 deg.
      -20.800000    0.0012004903    0.0015775618   0.00079205400   0.00079772246   1.3515603e-11
      -18.200000   0.00067748532    0.0013542828   0.00079845984   0.00079882208   9.2087260e-10
      -15.600000   0.00032830303    0.0011213765   0.00081015362   0.00079194928   3.5474658e-08
      -13.000000    0.0022340133   0.00094116180   0.00082372036   0.00079258123   7.7445758e-07
      -10.400000   0.00096042496   0.00083146688   0.00084849511   0.00080142265   9.6054252e-06
      -7.8000000    0.0013791855   0.00076747407   0.00093554589   0.00086523409   6.7852635e-05
      -5.2000000    0.0011958399   0.00073987325    0.0012147943    0.0010873221   0.00027363130
      -2.6000000    0.0032857459   0.00071292032    0.0018946765    0.0017153624   0.00063117920
       0.0000000    0.0039492116   0.00071216171    0.0031494949    0.0036434204   0.00083384116
       2.6000000    0.0028422628   0.00070933437    0.0020294542    0.0016676028   0.00063117920
       5.2000000    0.0010804405   0.00072754747    0.0011956715    0.0011537124   0.00027363130
       7.8000000   0.00060385238   0.00075340080   0.00090889704   0.00095489748   6.7852635e-05
       10.400000    0.0016389928   0.00079031726   0.00082596056   0.00085645577   9.6054252e-06
       13.000000   0.00066409957   0.00085756720   0.00080677250   0.00082306037   7.7445758e-07
       15.600000   0.00073977291   0.00096939916   0.00079610195   0.00085421429   3.5474658e-08
       18.200000   1.7822085e-08    0.0011169749   0.00079210105   0.00084181775   9.2087260e-10
       20.800000   0.00019251255    0.0012469902   0.00079180678   0.00082019006   1.3515603e-11

1800-7800 keV  (integrated for -48 < l < 48 deg.)
        b (deg)            flux           erro           NIR             CO          
      -26.000000     0.012002154     0.012805718       0.0000000   1.3858632e-07
      -20.800000   0.00035722283    0.0079030270   5.7012780e-06   3.9181116e-05
      -15.600000    0.0056571436    0.0058021210   0.00014201424   4.1312144e-05
      -10.400000    0.0097809964    0.0051386665   0.00048442386   2.1453825e-05
      -5.2000000    0.0073647281    0.0046673107    0.0018514057   0.00034270602
       0.0000000     0.014152155    0.0046232706     0.011452762     0.011682127
       5.2000000    0.0011961704    0.0048784748    0.0017403071    0.0011514581
       10.400000    0.0092085365    0.0051746628   0.00025212004   0.00046323612
       15.600000    0.0042539399    0.0056130733   3.7078313e-05   0.00033743782
       20.800000    0.0039184845    0.0063519129       0.0000000   0.00018779820
       26.000000    0.0029638248    0.0080366631       0.0000000   1.0645690e-05

        */

    int n_lat=17;
    double latitude[n_lat],flux[n_lat],flux_err[n_lat],flux_CO[n_lat],flux_NIR[n_lat];
    double flux_zero_level;

    if(i_E_range==1)
      {
	i_lat=0;
	//50-100 keV (integrated for -24 < l < 24 deg.)
	//         b (deg)       flux           erro           CO               NIR  
	      
      	latitude[i_lat]=-20.800000 ; flux[i_lat]=   6.6486422e-05 ;  flux_err[i_lat]=   0.0012273086 ; flux_NIR[i_lat]=  0.00083169063 ; flux_CO[i_lat]=  0.00084489669;i_lat++;
      	latitude[i_lat]=-18.200000 ; flux[i_lat]=    0.0010445070 ;   flux_err[i_lat]=  0.0010307939 ; flux_NIR[i_lat]=  0.00084971515 ; flux_CO[i_lat]=  0.00084747863;i_lat++;
      	latitude[i_lat]=-15.600000 ; flux[i_lat]=    0.0014115207 ;  flux_err[i_lat]=  0.00085487077 ; flux_NIR[i_lat]=  0.00088252613 ; flux_CO[i_lat]=  0.00083124646;i_lat++;
      	latitude[i_lat]=-13.000000 ; flux[i_lat]=   0.00055941047 ;  flux_err[i_lat]=  0.00075990202 ; flux_NIR[i_lat]=  0.00091862553 ; flux_CO[i_lat]=  0.00083099493;i_lat++;
      	latitude[i_lat]=-10.400000 ; flux[i_lat]=    0.0018830788 ;  flux_err[i_lat]=  0.00065008077 ; flux_NIR[i_lat]=  0.00096349391 ; flux_CO[i_lat]=  0.00083101949;i_lat++;
      	latitude[i_lat]=-7.8000000 ; flux[i_lat]=   0.00049471855 ;  flux_err[i_lat]=  0.00060397085 ; flux_NIR[i_lat]=  0.0010445518  ; flux_CO[i_lat]=  0.00084409536;i_lat++;
      	latitude[i_lat]=-5.2000000 ; flux[i_lat]=    0.0018094770 ;  flux_err[i_lat]=  0.00058667386 ; flux_NIR[i_lat]=  0.0012513078  ; flux_CO[i_lat]=  0.00088242219;i_lat++;
      	latitude[i_lat]=-2.6000000 ; flux[i_lat]=    0.0019962361 ;  flux_err[i_lat]=  0.00056808956 ; flux_NIR[i_lat]=  0.0021584090  ; flux_CO[i_lat]=   0.0015180752;i_lat++;
      	latitude[i_lat]= 0.0000000 ; flux[i_lat]=    0.0054503186 ;  flux_err[i_lat]=  0.00064642238 ; flux_NIR[i_lat]=  0.0051193484  ; flux_CO[i_lat]=   0.0055727294;i_lat++;
       	latitude[i_lat]=2.6000000  ; flux[i_lat]=   0.0026214142  ;  flux_err[i_lat]= 0.00061978235  ; flux_NIR[i_lat]=  0.0025376953  ; flux_CO[i_lat]=  0.0014058408;i_lat++;
       	latitude[i_lat]=5.2000000  ; flux[i_lat]=  0.00039503481  ;  flux_err[i_lat]= 0.00056840364  ; flux_NIR[i_lat]=  0.0011974932  ; flux_CO[i_lat]=   0.0010384383;i_lat++;
       	latitude[i_lat]=7.8000000  ; flux[i_lat]=   0.0013199281  ;  flux_err[i_lat]= 0.00059244911  ; flux_NIR[i_lat]=  0.00096955765 ; flux_CO[i_lat]=   0.0010548030;i_lat++;
       	latitude[i_lat]=10.400000  ; flux[i_lat]=   0.0012431506 ;  flux_err[i_lat]=  0.00062658729  ; flux_NIR[i_lat]=  0.00090007804 ; flux_CO[i_lat]=  0.00096034649;i_lat++;
       	latitude[i_lat]=13.000000  ; flux[i_lat]=  0.00092170350 ;  flux_err[i_lat]=  0.00066368033  ; flux_NIR[i_lat]=  0.00087093147 ; flux_CO[i_lat]=  0.00090262045;i_lat++;
       	latitude[i_lat]=15.600000  ; flux[i_lat]=   0.0010397129 ;  flux_err[i_lat]=  0.00073933042  ; flux_NIR[i_lat]=  0.00084298245 ; flux_CO[i_lat]=  0.00097756830;i_lat++;
       	latitude[i_lat]=18.200000  ; flux[i_lat]=  0.00031439027 ;  flux_err[i_lat]=  0.00085483347  ; flux_NIR[i_lat]=  0.00083182048 ; flux_CO[i_lat]=  0.00094851781;i_lat++;
       	latitude[i_lat]=20.800000  ; flux[i_lat]=  0.00092698226 ;  flux_err[i_lat]=  0.00096460090  ; flux_NIR[i_lat]=  0.00083099493 ; flux_CO[i_lat]=  0.00089769519;
	

	flux_zero_level= 3 * 2.7699831e-4; // Bouchet mail Jan 17:  cm-2 s-1 per 16*2.6 deg pixel
  }


    

    if(i_E_range==2)
      {
	i_lat=0;
	//100-200 keV (integrated for -24 < l < 24 deg.)
	//          b (deg)        flux           erro           CO               NIR        
	latitude[i_lat]= -20.800000; flux[i_lat]=  0.00018423101;  flux_err[i_lat]= 0.0010785995; flux_CO[i_lat]= 0.00040081825; flux_NIR[i_lat]=0.00040885309;i_lat++;
      	latitude[i_lat]=-18.200000; flux[i_lat]=   0.00057888468 ;  flux_err[i_lat]=     0.00091710290; flux_NIR[i_lat]=   0.00041224010; flux_CO[i_lat]=   0.00041042727;i_lat++;
      	latitude[i_lat]=-15.600000; flux[i_lat]=   0.00014820052 ;  flux_err[i_lat]=  0.00075313225   ; flux_NIR[i_lat]=  0.00043303191 ; flux_CO[i_lat]=  0.00040053075;i_lat++;
      	latitude[i_lat]=-13.000000 ; flux[i_lat]=  0.00087864872 ;  flux_err[i_lat]=  0.00063292765   ; flux_NIR[i_lat]=  0.00045590753 ; flux_CO[i_lat]=  0.00040037739;i_lat++;
      	latitude[i_lat]=-10.400000 ; flux[i_lat]=   0.00083597678;  flux_err[i_lat]=   0.00055902288  ; flux_NIR[i_lat]=  0.00048433992 ; flux_CO[i_lat]=  0.00040039237;i_lat++;
      	latitude[i_lat]=-7.8000000 ; flux[i_lat]=   0.00068194231;  flux_err[i_lat]=   0.00051020555  ; flux_NIR[i_lat]=  0.00053570507 ; flux_CO[i_lat]=  0.00040836453;i_lat++;
      	latitude[i_lat]=-5.2000000 ; flux[i_lat]=    0.0010400436;  flux_err[i_lat]=   0.00048990448;   flux_NIR[i_lat]=  0.00066672305 ; flux_CO[i_lat]=  0.00043173185;i_lat++;
      	latitude[i_lat]=-2.6000000  ; flux[i_lat]=   0.0013320365;  flux_err[i_lat]=   0.00047257359;   flux_NIR[i_lat]=    0.0012415389; flux_CO[i_lat]=   0.00081928041;i_lat++;
       	latitude[i_lat]=0.0000000  ; flux[i_lat]=   0.0031205474 ;  flux_err[i_lat]=  0.00050077305;    flux_NIR[i_lat]=   0.0031178399 ; flux_CO[i_lat]=   0.0032913451;i_lat++;
       	latitude[i_lat]=2.6000000  ; flux[i_lat]=   0.0011120367 ;  flux_err[i_lat]=  0.00050891479;    flux_NIR[i_lat]=    0.0014818867 ;flux_CO[i_lat]=  0.00075085268;i_lat++;
       	latitude[i_lat]=5.2000000 ; flux[i_lat]=   0.00064093262 ;  flux_err[i_lat]=  0.00048878001;    flux_NIR[i_lat]=   0.00063262158 ;flux_CO[i_lat]=  0.00052685267;i_lat++;
       	latitude[i_lat]=7.8000000 ; flux[i_lat]=   0.00027411109 ;  flux_err[i_lat]=  0.00050074142;    flux_NIR[i_lat]=   0.00048818242; flux_CO[i_lat]=   0.00053682994;i_lat++;
       	latitude[i_lat]=10.400000 ; flux[i_lat]=   0.00059720125 ;  flux_err[i_lat]=  0.00052946783;    flux_NIR[i_lat]=   0.00044415427; flux_CO[i_lat]=   0.00047924119;i_lat++;
       	latitude[i_lat]=13.000000 ; flux[i_lat]=   0.00044884495 ;  flux_err[i_lat]=  0.00057725951;    flux_NIR[i_lat]=   0.00042568455 ;flux_CO[i_lat]=  0.00044404644;i_lat++;
       	latitude[i_lat]=15.600000 ; flux[i_lat]=   2.7996345e-08 ;  flux_err[i_lat]=  0.00065300352;    flux_NIR[i_lat]=   0.00040797369 ;flux_CO[i_lat]=  0.00048974108;i_lat++;
       	latitude[i_lat]=18.200000 ; flux[i_lat]=   0.00035978770 ;  flux_err[i_lat]=  0.00075675492;    flux_NIR[i_lat]=   0.00040090053; flux_CO[i_lat]=   0.00047202941;i_lat++;
       	latitude[i_lat]=20.800000 ; flux[i_lat]=   0.00028152789 ;  flux_err[i_lat]=  0.00085005628 ;   flux_NIR[i_lat]=  0.00040037739 ; flux_CO[i_lat]=  0.00044104359;
	
	flux_zero_level= 3 * 1.3345913e-4; // Bouchet mail Jan 17: cm-2 s-1 per 16*2.6 deg pixel

      }


    lat_min= -20.8 - 1.3; // first  bin lower boundary
    lat_max= +20.8 + 1.3; // last   bin upper boundary

    

    profile    =new TH1D("SPI skymap",    canvastitle,  n_lat,    lat_min, lat_max) ; 
    profile_CO =new TH1D("SPI skymap CO", canvastitle,  n_lat,    lat_min, lat_max) ; 
    profile_NIR=new TH1D("SPI skymap NIR",canvastitle,  n_lat,    lat_min, lat_max) ; 

    double dtr=acos(-1.0)/180.; // degrees to radians
    double solid_angle=48.0 * 2.6 *dtr*dtr; // 48*2.6 deg pixels  
  
    double average_intensity     = 0.0;
    double average_intensity_NIR = 0.0;
    double average_intensity_CO  = 0.0;
    
    int n_lat_av=0;

    double fudge_factor=1.00; //  flexible to compare the shapes

    double lat_min_av=-15.7; // check exactly which bins Bouchet used - this choice gives nearly the same total flux as in 17 Jan email
    double lat_max_av=+15.7;

    for(i_lat=0;i_lat<n_lat;i_lat++)
    {
 
     
      double intensity    =   (flux       [i_lat]-flux_zero_level)   /solid_angle;
      double intensity_err=    flux_err   [i_lat]                    /solid_angle;
      double intensity_CO    =(flux_CO    [i_lat] - flux_zero_level )/solid_angle;
      double intensity_NIR   =(flux_NIR   [i_lat] - flux_zero_level )/solid_angle;
     
      cout<<"b="<<latitude[i_lat]<<" SPI flux-zero level="<<flux[i_lat]<<" solid_angle="<<solid_angle<<" intensity="<<intensity<<endl;


      intensity    *=fudge_factor;
      intensity_err*=fudge_factor;
      intensity_CO *=fudge_factor;
      intensity_NIR*=fudge_factor;


      ii_lat=i_lat+1; // TH1D bins start at 1

      profile->    SetBinContent(ii_lat,intensity);
      profile->    SetBinError  (ii_lat,intensity_err);

      profile_CO ->SetBinContent(ii_lat,intensity_CO);
      profile_NIR->SetBinContent(ii_lat,intensity_NIR);

      if(latitude[i_lat]>=lat_min_av && latitude[i_lat]<=lat_max_av)
      {
       average_intensity    += intensity;
       average_intensity_NIR+= intensity_NIR;
       average_intensity_CO += intensity_CO ;
       n_lat_av++;
      }
    

    } //for i_lat


    // ---------------- printout

    average_intensity    /=n_lat_av;
    average_intensity_NIR/=n_lat_av;
    average_intensity_CO /=n_lat_av;

    cout<<"latitude range for average:"<<lat_min_av<<" to "<<lat_max_av<<endl;

    cout<<"latitude averaged     intensity="<<average_intensity    <<" cm-2 sr-1 s-1"<<endl;
    cout<<"latitude averaged NIR intensity="<<average_intensity_NIR<<" cm-2 sr-1 s-1"<<endl;
    cout<<"latitude averaged CO  intensity="<<average_intensity_CO <<" cm-2 sr-1 s-1"<<endl;

    double g=1.55; // Bouchet et al Table 2 Diffuse 2 = power law component. In fact varies with energy for total emission

    // AE^-g spectrum integrated over energy = A/(1-g)*[ e_max^(-g+1)-e_min^(-g+1) ]

    for (int icase=1;icase<=3;icase++)
    {

    double A;
   if(icase==1) A=average_intensity     *(1-g)/(pow(e_max,-g+1.) - pow(e_min,-g+1));
   if(icase==2) A=average_intensity_NIR *(1-g)/(pow(e_max,-g+1.) - pow(e_min,-g+1));
   if(icase==3) A=average_intensity_CO  *(1-g)/(pow(e_max,-g+1.) - pow(e_min,-g+1));

   cout<<endl;
   if(icase==1)cout<<"------------------- using image profile"<<endl;
   if(icase==2)cout<<"------------------- using NIR   profile"<<endl;
   if(icase==3)cout<<"------------------- using CO    profile"<<endl;

   cout<<"differential average intensity      at "<<e_min <<" MeV =" << A*pow(e_min,-g  )<<"     cm-2 sr-1 s-1 MeV-1"<<endl;
   cout<<"differential average intensity      at "<<e_max <<" MeV =" << A*pow(e_max,-g  )<<"     cm-2 sr-1 s-1 MeV-1 "<<endl;

   cout<<"differential average intensity *E^2 at "<<e_min <<" MeV =" << A*pow(e_min,-g+2)<<"     cm-2 sr-1 s-1 MeV "<<endl;
   cout<<"differential average intensity *E^2 at "<<e_max <<" MeV =" << A*pow(e_max,-g+2)<<"     cm-2 sr-1 s-1 MeV "<<endl;

   // for comparison with Bouchet et al. 2008 Fig 5 latitude profiles
   if(icase==1)   cout<<"total flux  "<<e_min<<"-"  <<e_max <<" MeV =" << average_intensity     *solid_angle*n_lat_av<<"     cm-2 s-1      "<<endl;
   if(icase==2)   cout<<"total flux  "<<e_min<<"-"  <<e_max <<" MeV =" << average_intensity_NIR *solid_angle*n_lat_av<<"     cm-2 s-1      "<<endl;
   if(icase==3)   cout<<"total flux  "<<e_min<<"-"  <<e_max <<" MeV =" << average_intensity_CO  *solid_angle*n_lat_av<<"     cm-2 s-1      "<<endl;


   cout<<endl;
   // for comparison with Bouchet et al. 2008 Fig 9 spectrum
   cout<<"differential total flux             at "<<e_min <<" MeV =" << A*pow(e_min,-g  )*solid_angle*n_lat_av<<"     cm-2 s-1 MeV-1"<<endl;
   cout<<"differential total flux             at "<<e_max <<" MeV =" << A*pow(e_max,-g  )*solid_angle*n_lat_av<<"     cm-2 s-1 MeV-1"<<endl;
   // Bouchet et al. Table 2 Diffuse 2 : 1.4e-4 cm-2 s-1 keV-1 at 50 keV
   cout<<"differential total flux Bouchet     at "<<e_min <<" MeV =" << 1.4e-1 *  pow(e_min/.05,-g  )      <<"     cm-2 s-1 MeV-1"<<endl;
   cout<<"differential total flux Bouchet     at "<<e_max <<" MeV =" << 1.4e-1 *  pow(e_max/.05,-g  )      <<"     cm-2 s-1 MeV-1"<<endl;

   // the same in keV-1
   // for comparison with Bouchet et al. 2008 Fig 9 spectrum
   cout<<endl;
   cout<<"differential total flux             at "<<e_min <<" MeV =" << A*pow(e_min,-g  )*solid_angle*n_lat_av*1.e-3<<"     cm-2 s-1 keV-1"<<endl;
   cout<<"differential total flux             at "<<e_max <<" MeV =" << A*pow(e_max,-g  )*solid_angle*n_lat_av*1.e-3<<"     cm-2 s-1 keV-1"<<endl;
   // Bouchet et al. Table 2 Diffuse 2 : 1.4e-4 cm-2 s-1 keV-1 at 50 keV
   cout<<"differential total flux Bouchet     at "<<e_min <<" MeV =" << 1.4e-4 *  pow(e_min/.05,-g  )      <<"     cm-2 s-1 keV-1"<<endl;
   cout<<"differential total flux Bouchet     at "<<e_max <<" MeV =" << 1.4e-4 *  pow(e_max/.05,-g  )      <<"     cm-2 s-1 keV-1"<<endl;

    }// for icase

   //-------------------------------------------------------------------------------------------------------------------------------------------


  profile->GetXaxis()->SetTitle("Galactic  latitude");
  profile->GetYaxis()->SetTitle("intensity, cm^{-2} sr^{-1} s^{-1}  ");
  profile->GetYaxis()->SetTitleOffset(1.2);
  profile->GetYaxis()->SetLabelSize  (0.030);

  profile->SetTitle(""         ); // removes text written in box on plot


   if(galplotdef.lat_log_scale==0)
   profile->SetMaximum(profile->GetMaximum()*1.5); // to allow for data with larger range
  if(galplotdef.lat_log_scale==1)
   profile->SetMaximum(profile->GetMaximum()*2.0); 
  if(galplotdef.lat_log_scale==1)                     
   profile->SetMinimum(profile->GetMaximum()/1e5);   

 



  TGaxis::SetMaxDigits(4);  

  profile->SetStats(kFALSE);
  profile->SetLineStyle(1     );// 1=solid 2=dash
  profile->SetLineWidth(3     );
  profile->SetLineColor(kBlack);

    

  
  sprintf(name,"SPI latitude profile %d",i_E_range); // vary name and title to keep all plots on screen
  strcpy(canvastitle,name);

  c1=new TCanvas(name,canvastitle, 500+i_E_range*100, 350+i_E_range*100, 600,600);

     

 

                                                                     
  
  if(galplotdef.lat_log_scale==1)c1->SetLogy();                //a TCanvas is a TPad, TPad has this function
  profile->Draw("L");

  profile_CO ->SetLineColor(kCyan);
  profile_CO ->Draw("same L");                  // NB this is really NIR

  profile_NIR->SetLineColor(kMagenta);
  profile_NIR->Draw("same L");                  // NB this is really CO

  
  }

  // parameters labelling

  sprintf(workstring1,"  %5.2f<l<%5.2f , %5.2f<l<%5.2f",                 //AWS20040315
                          galplotdef.long_min1,galplotdef.long_max1,
                          galplotdef.long_min2,galplotdef.long_max2);
 
  sprintf(workstring2,"  %5.2f<b<%5.2f , %5.2f<b<%5.2f",                 //AWS20040315
                         galplotdef.lat_min1,galplotdef.lat_max1,
                         galplotdef.lat_min2,galplotdef.lat_max2);
  
  sprintf(workstring3," %5.3f - %5.3f MeV",e_min,e_max);


  strcpy(workstring4," galdef ID ");
  strcat(workstring4,galdef.galdef_ID);


  TText *text=new TText();
  text->SetTextFont(62);
  text->SetTextColor(kBlack);
  text->SetTextSize(0.025 );
  text->SetTextAlign(12);

  if(longprof)
  text->DrawTextNDC(.56 ,.85 ,workstring2);// NDC=normalized coord system  AWS20040315
  if(latprof)
  text->DrawTextNDC(.52 ,.85 ,workstring1);// NDC=normalized coord system  AWS20040315
  text->DrawTextNDC(.58 ,.88 ,workstring3);// NDC=normalized coord system
  text->DrawTextNDC(.20 ,.92 ,workstring4);// NDC=normalized coord system



 plot_SPI_model_profile( longprof,  latprof, e_min , e_max ,    1     , 1  ,1  );


  //============== postscript output

  sprintf(workstring1,"l_%.2f_%.2f_%.2f_%.2f",                          
                          galplotdef.long_min1,galplotdef.long_max1,
                          galplotdef.long_min2,galplotdef.long_max2);

  sprintf(workstring2,"b_%.2f_%.2f_%.2f_%.2f",                         
                          galplotdef.lat_min1,galplotdef.lat_max1,
                          galplotdef.lat_min2,galplotdef.lat_max2);

  sprintf(workstring3,"%.3f-%.3f_MeV",e_min,e_max);



  strcpy(psfile,"plots/");
  if(longprof==1)
  strcat(psfile,"longitude_profile_");
  if(latprof==1)
  strcat(psfile,"latitude_profile_");
  if(   mode==1)
  strcat(psfile,"lego_"  
          );
  strcat(psfile,galdef.galdef_ID);
  strcat(psfile,"_"        );
  strcat(psfile,workstring3);
  strcat(psfile,"_"        );
  if(longprof==1)
  strcat(psfile,workstring2);
  if( latprof==1)
  strcat(psfile,workstring1);

  strcat(psfile,"_spiskymax_");



  strcat(psfile,galplotdef.psfile_tag);

  strcat(psfile,".eps");

  cout<<"postscript file="<<psfile<<endl;
  c1->Print(psfile       );
  //==============




 }//i_E_range

   cout<<endl;
   cout<<" <<<< plot_SPI_bouchet_profile    "<<endl;
 
 return 0;
}
///////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////
/// enable this for stand-alone test

/*
int main(int argc,char*argv[])   
{
  cout<<"testing plot_SPI_spiskymax_profile"<<endl;
     char version[]="48";  
 
 
   cout<<">>>>galplot version "<<version<<endl;
 
   if(argc!=2){cout<<"no galdef file specified!"<<endl;return -1;}
   cout<<argc<<" "<<argv[0]<<" "<<argv[1]<<endl;
 
   if(configure.init() !=0)return 1;
   cout<<configure.galdef_directory<<endl;// just a test
 
   if(galdef.read  (version,argv[1],configure.galdef_directory) !=0) return 1;
 
   if(galplotdef.read  (version,argv[1],configure.galdef_directory) !=0) return 1;

  configure.init();
  

  txtFILE=fopen("junk","w");


  extern void InitGui();
   VoidFuncPtr_t initFuncs[] = {InitGui, 0};
   TROOT root ("ISDC", "galplot", initFuncs);
   //int argc=0; char * argv[]={" "};
   TApplication theApp("galplot", &argc, argv);

  

  plot_SPI_spiskymax_profile(1,0,0);
  plot_SPI_spiskymax_profile(0,1,0);
  plot_SPI_spiskymax_profile(0,0,1);

   theApp.Run();
  return 0;

}


*/
