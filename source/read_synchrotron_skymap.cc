
using namespace std;
#include"Galplot.h"                    //AWS20050920
#include"galprop_classes.h"
#include"galprop.h"

#include "fitsio.h" 

int  Galplot::read_synchrotron_skymap()
{
int stat;

 char   comment[100];

  cout<<" >>>> read_synchrotron_skymap"<<endl;

   stat=0;
   fitsfile *fptr;       /* pointer to the FITS file; defined in fitsio.h */
    int status, ii, jj;
    status=0; //AWS20100616

 

    long  fpixel = 1, naxis = 4, nelements, exposure;
    long naxes[5]  ; 

  
     naxis=3;
     naxes[0]=galaxy.n_long;
     naxes[1]=galaxy.n_lat;
     naxes[2]=galaxy.n_nu_synchgrid;
   

     nelements=naxes[0]*naxes[1]*naxes[2];
    

  
    
    float *array;          
    array=new float[nelements];

    char  infile[100];



   if(galdef.skymap_format==0) //AWS20100616
   {  

  for(int icase=1;icase<=4;icase++) //AWS20110407
  {
    strcpy( infile,configure.fits_directory);
 
    if(icase==1)strcat( infile,"synchrotron_skymap_");  //AWS20110407  
    if(icase==2)strcat( infile,"synchrotron_Q_skymap_");//AWS20110407  
    if(icase==3)strcat( infile,"synchrotron_U_skymap_");//AWS20110407  
    if(icase==4)strcat( infile,"synchrotron_P_skymap_");//AWS20110519  
   
    strcat( infile,galdef.galdef_ID);
    cout<<"  reading  synchrotron skymap  from file "<<infile<<endl;

    status = 0;         /* initialize status before calling fitsio routines */
    fits_open_file(&fptr,  infile,READONLY, &status);   
    cout<<"  fits open status = "<<status<<endl;



    /* Read the array of floats  */
    float nulval=0;
    int anynul;
    fits_read_img(fptr, TFLOAT, fpixel, nelements, &nulval,array, &anynul,&status);
 
  

  

   int i=0; 
   for (int ip       =0;        ip<naxes[2];       ip++)
   for (int ib       =0;        ib<naxes[1];       ib++)
   for (int il       =0;        il<naxes[0];       il++)
   {             
           // cout<<array[i]<<endl;

     if(icase==1)  galaxy.synchrotron_skymap  .d2[il][ib].s[ip]=array[i];//AWS20110411
     if(icase==2)  galaxy.synchrotron_Q_skymap.d2[il][ib].s[ip]=array[i];//AWS20110411
     if(icase==3)  galaxy.synchrotron_U_skymap.d2[il][ib].s[ip]=array[i];//AWS20110411
     if(icase==4)  galaxy.synchrotron_P_skymap.d2[il][ib].s[ip]=array[i];//AWS20110411

                       
             i++;
   }


   //galaxy.synchrotron_skymap .print();

    fits_close_file(fptr, &status);            /* close the file */

    fits_report_error(stderr, status);  /* print out any error messages */

   } // for icase


    delete[] array;

   } // skymap_format==0      AWS20100616

   //---------------------------------------------------------------------------------
    // healpix galprop files       AWS20100614

   if(galdef.skymap_format==3)
   {
  for(int icase=1;icase<=4;icase++) //AWS20110407
  {

    strcpy( infile,configure.fits_directory);
    if(icase==1)strcat( infile,"synchrotron_healpix_");  //  this has total AWS20110407
    if(icase==2)strcat( infile,"synchrotron_Q_healpix_"); // this has Q     AWS20110407
    if(icase==3)strcat( infile,"synchrotron_U_healpix_"); // this has U     AWS20110407
    if(icase==4)strcat( infile,"synchrotron_P_healpix_"); // this has P     AWS20110407

    strcat( infile,galdef.galdef_ID);


    string mapname;
    mapname=infile;
    cout<<"  reading synchrotron healpix skymap  from file "<<mapname<<endl;
    
    if(icase==1)galaxy.synchrotron_hp_skymap  .load(mapname); //AWS20110407
    if(icase==2)galaxy.synchrotron_Q_hp_skymap.load(mapname); //AWS20110407
    if(icase==3)galaxy.synchrotron_U_hp_skymap.load(mapname); //AWS20110407
    if(icase==4)galaxy.synchrotron_P_hp_skymap.load(mapname); //AWS20110407

    //    if(galplotdef.verbose==-2002){cout<<" synchrotron_hp_skymap  "<<endl;       galaxy.synchrotron_hp_skymap.print(cout);} 
   }// for icase

    strcpy( infile,configure.fits_directory);                               //AWS20110906
    strcat( infile,"free_free_healpix_");                                   //AWS20110906
    strcat( infile,galdef.galdef_ID);                                       //AWS20110906


    string mapname;                                                         //AWS20110906
    mapname=infile;                                                         //AWS20110906
    cout<<"  reading free-free   healpix skymap  from file "<<mapname<<endl;//AWS20110906
    
    galaxy.free_free_hp_skymap  .load(mapname);                             //AWS20110906

  }// format==3

   //---------------------------------------------------------------------------------


    cout<<" <<<< read_synchrotron_skymap"<<endl;

   
    return status;
}
