

#include <cassert>
#include <cstring>
#include <string>
#include <sstream>

#include"Galplot.h"
#include "galprop_classes.h"
#include "galprop.h"

using namespace std;

#include "fitsio.h" 



int Galplot::read_gcr_source_functions(Particle &particle) {

  cout<<">>read_gcr_source_functions"<<endl;

  int status = 0;

  ostringstream buf;
  long nAxis = -1, nElements = -1;
  valarray<long> nAxes;

  if (2 == gcr[0].n_spatial_dimensions) {
  
    nAxis = 3;
    nAxes.resize(nAxis);

    nAxes[0] = gcr[0].n_rgrid;
    nAxes[1] = gcr[0].n_zgrid;
    nAxes[2] = gcr[0].n_pgrid;
    
    
    nElements = nAxes[0]*nAxes[1]*nAxes[2];
    
  }

  if (3 == gcr[0].n_spatial_dimensions) {
  
    nAxis = 4;
    nAxes.resize(nAxis);
    
    nAxes[0] = gcr[0].n_xgrid;
    nAxes[1] = gcr[0].n_ygrid;
    nAxes[2] = gcr[0].n_zgrid;
    nAxes[3] = gcr[0].n_pgrid;
   
   
    nElements = nAxes[0]*nAxes[1]*nAxes[2]*nAxes[3];
  
  }

  assert (nAxis > 0 && nElements > 0 && nAxes.size());

  valarray<float> array(0., nElements);


  string origin="origin_not_assigned";    
  string filename,particle_name;
  particle_name=particle.name;

  string configure_fits_directory=configure.fits_directory;
  string galdef_galdef_ID        = galdef.galdef_ID;

  cout<<"read_gcr_source_functions: particle="<<particle_name<<endl;
  

  filename="filename_not_assigned";

  if(particle_name=="Hydrogen_1"       )  { filename="primary_protons_source_function";     origin="primary";  }
  if(particle_name=="Helium_4"         )  { filename="primary_Helium_source_function";      origin="primary";  }
  if(particle_name=="primary_electrons")  { filename="primary_electrons_source_function";   origin="primary";  }

  if(particle_name=="secondary_electrons"){ filename="secondary_electrons_source_function"; origin="secondary";}
  if(particle_name=="secondary_positrons"){ filename="secondary_positrons_source_function"; origin="secondary";}

  cout<<" read_gcr_source_functions: origin="<<origin<<" filename="<<filename<<endl;

  if(origin=="origin_not_assigned"||filename=="filename_not_assigned") {  cout<<buf                   ;status=1; return status;}

  string infile = configure_fits_directory  + filename + "_"+ galdef_galdef_ID + ".gz";


  cout << "reading " <<particle_name   <<" as "<<origin<<" source function array from " << infile;
 

  
  
  




  fitsfile* fptr = 0; /* pointer to the FITS file; defined in fitsio.h */
    
 

  

 

    status = 0;         /* initialize status before calling fitsio routines */
    fits_open_file(&fptr,  infile.c_str(),READONLY, &status); // see galprop store_gcr_source_functions.cc for syntax with string

    cout<<"  fits open status = "<<status<<endl;

    /* Read the array of floats  */
    float nulval=0;
    int anynul;
    long fpixel = 1;

    fits_read_img(fptr, TFLOAT, fpixel, nElements, &nulval, &array[0], &anynul,&status);  // see galprop store_gcr_source_functions.cc for syntax with valarr


    particle.create_transport_arrays();


  if (2 == gcr[0].n_spatial_dimensions) {

    int i = 0;

  
 
      for (int ip = 0; ip < nAxes[2]; ++ip) {

	for (int iz = 0; iz < nAxes[1]; ++iz) {
	  
	  for (int ir = 0; ir < nAxes[0]; ++ir) {
	    
	
	    if(origin=="primary"  ) particle.  primary_source_function.d2[ir][iz].s[ip]=array[i] ;
	    if(origin=="secondary") particle.secondary_source_function.d2[ir][iz].s[ip]=array[i] ;	       
	    
	    //   if(array[i]>0.0)	    cout << "ip= " << ip << " " << iz << " " << ir << " " << i << " " << array[i]  << endl;
	    ++i;

	  }//ir

	}//iz

      }//ip

   

  }//n_spatial_dimensions==2
  


  if (3 == gcr[0].n_spatial_dimensions) {
    
    int i = 0;

    
 
      for (int ip = 0; ip < nAxes[3]; ++ip) {

	for (int iz = 0; iz < nAxes[2]; ++iz) {

	  for (int iy = 0; iy < nAxes[1]; ++iy) {

	    for (int ix = 0; ix < nAxes[0]; ++ix) {
	      
	   
		 if(origin=="primary"  )    particle.primary_source_function  .d3[ix][iy][iz].s[ip]=array[i] ; 
		 if(origin=="secondary")    particle.secondary_source_function.d3[ix][iy][iz].s[ip]=array[i] ; 
	      
	      ++i;

	    }//ix

	  }//iy
	
	}//iz
      
      }//ip
    
    
  
  }//n_spatial_dimensions==3
  

  particle.print();
 
    

  
  fits_close_file(fptr, &status);            /* close the file */
  
  fits_report_error(stderr, status);  /* print out any error messages */

 
  
  
    cout<<"<<read_gcr_source_functions"<<endl;

  return status;

}
