

#include"Galplot.h"                    
#include"galprop_classes.h"
#include"galprop.h"
#include"constants.h"
#include"fitsio.h"

/*
void   differential_spectrum(double e1,double e2, double intensity, double g,double &intensity_esq1,double &intensity_esq2);
void   differential_spectrum(double *e,    int n, double *intensity,         double *intensity_esq1,double *intensity_esq2,
                             double *Ebar,double *Ibar);
*/

//void Galplot::deconvolve_intensity(TH1D *intensity);

int Galplot::plot_GLAST_spectrum(int mode)
{
   cout<<" >>>> plot_GLAST_spectrum    "<<endl;
   int status=0;


int i_comp,i_lat,i_long,i_E_EGRET;
int ii_long,ii_lat;
int il_min,il_max,ib_min,ib_max;
int il_min1,il_max1,ib_min1,ib_max1;
int il_min2,il_max2,ib_min2,ib_max2;
double l,b,l_off;
double exposure, intensity,*intensity_,*intensity_esq1,*intensity_esq2;
double           counts,      *counts_,stat_err;                             //AWS20060322
double *Ebar,*Ibar;
double long_min,lat_min;
double d_long, d_lat;
double *g;
double segment[2],Ebar_segment[2];

char name[100],canvastitle[100], workstring1[100],workstring2[100];
int errorbar; double errorbarfactor;
double delta_err;//AWS20040312

TCanvas *c1;
TH3F    *egret_map;
TH1D    *profile;  
TH1D *profile11,*profile12,*profile21,*profile22; // four (l,b) intervals
TGraph  *spectrum;

TH3F    *counts_map;                                                              //AWS20060322
TH1D    *counts_profile;                                                          //AWS20060322
TH1D    *counts_profile11,*counts_profile12,*counts_profile21,*counts_profile22;  //AWS20060322

int ncolors=1; int *colors=0;
gStyle->SetPalette(ncolors,colors);


 cout<<data.GLAST_counts.n_rgrid<<" "<<data.GLAST_counts.n_zgrid<<endl;

  
 
 
 g=new double[data.n_E_GLAST]; //AWS20080509
 g[0]=1.5;
 g[1]=1.5;
 g[2]=1.5;
 g[3]=1.5;
 g[4]=1.6;
 g[5]=1.7;
 g[6]=1.9;
 g[7]=2.0;
 g[8]=2.5;
 g[9]=2.7;

  //       map is 2D Distribution, hence coords are formally r, z

  egret_map=new TH3F(canvastitle,canvastitle,
                     data.GLAST_counts.n_rgrid,       -180,           180.,
		     data.GLAST_counts.n_zgrid,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1),
                     data.GLAST_counts.n_pgrid,data.E_GLAST[0],data.E_GLAST[data.n_E_GLAST]);



  counts_map=new TH3F(canvastitle,canvastitle,                                              //AWS20060322
                     data.GLAST_counts.n_rgrid,       -180,           180.,
		     data.GLAST_counts.n_zgrid,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1),
                     data.GLAST_counts.n_pgrid,data.E_GLAST[0],data.E_GLAST[data.n_E_GLAST]);


  long_min= -179.75;//specific for EGRET data counts.gp1234_30.g001
   lat_min  =-89.75;
  d_long      =0.5;     
  d_lat       =0.5;

        for(i_E_EGRET=0; i_E_EGRET<data.n_E_GLAST                    ; i_E_EGRET++)
        {
	 for (i_lat =0;  i_lat <data.GLAST_counts.n_zgrid            ; i_lat++    )
         {
          for(i_long=0;  i_long<data.GLAST_counts.n_rgrid            ; i_long++   )
          {

            l=       long_min+ d_long   *i_long;
            b=        lat_min+ d_lat    *i_lat;

            exposure =data.GLAST_exposure.d2[i_long][i_lat].s[i_E_EGRET];          //AWS20080508
            intensity=0.;
            if(exposure>0.)
            intensity=data.GLAST_counts.  d2[i_long][i_lat].s[i_E_EGRET]/exposure; //AWS20080508


            intensity*=sabin(lat_min,d_long,d_lat,i_lat); // multiply by solid angle of bin


            counts = data.GLAST_counts.  d2[i_long][i_lat].s[i_E_EGRET];//AWS20080508

            //cout<<i_long<<" "<<i_lat<<" "<<exposure<<" "<<intensity<<endl;
            l_off=l-180.;
            if(l_off<0.)l_off+=360.0;
            ii_long=  data.EGRET_counts.n_rgrid-l_off/d_long;
	    //cout<<"l l_off ii_long"<<l<<" "<<l_off<<" "<<ii_long<<endl;

               egret_map    ->SetBinContent(ii_long,i_lat,i_E_EGRET,intensity);   
  
              counts_map    ->SetBinContent(ii_long,i_lat,i_E_EGRET,counts   ); //AWS20060322

            }// i_long
	   }// i_lat
          }// i_E_EGRET

 


  // NB reversed longitude axis starting at +180 and decreasing

  l_off=180.-galplotdef.long_max1; if(l_off>360.)l_off-=360.; if(l_off<  0.)l_off+=360.;
  il_min1=l_off/d_long;
  l_off=180.-galplotdef.long_min1; if(l_off>360.)l_off-=360.; if(l_off<  0.)l_off+=360.;
  il_max1=l_off/d_long;
  cout<<"il_min1 ilmax1:"<<il_min1<<" "<<il_max1<<endl;

  l_off=180.-galplotdef.long_max2; if(l_off>360.)l_off-=360.; if(l_off<  0.)l_off+=360.;
  il_min2=l_off/d_long;
  l_off=180.-galplotdef.long_min2; if(l_off>360.)l_off-=360.; if(l_off<  0.)l_off+=360.;
  il_max2=l_off/d_long;
  cout<<"il_min2 ilmax2:"<<il_min2<<" "<<il_max2<<endl;




  ib_min1=(galplotdef.lat_min1-lat_min+.0001)/d_lat;//AWS20030911
  ib_max1=(galplotdef.lat_max1-lat_min+.0001)/d_lat;//AWS20030911

  cout<<"ib_min1 ibmax1:"<<ib_min1<<" "<<ib_max1<<endl;

  ib_min2=(galplotdef.lat_min2-lat_min+.0001)/d_lat;//AWS20030911
  ib_max2=(galplotdef.lat_max2-lat_min+.0001)/d_lat;//AWS20030911

  cout<<"ib_min2 ibmax2:"<<ib_min2<<" "<<ib_max2<<endl;

  double sa_norm_11,   sa_norm_12,   sa_norm_21,   sa_norm_22;
         sa_norm_11=0.;sa_norm_12=0.;sa_norm_21=0.;sa_norm_22=0.;


          for (i_long=il_min1;  i_long<=il_max1           ; i_long++   )//AWS20030910
	  for (i_lat =ib_min1;  i_lat <=ib_max1           ; i_lat++    )//AWS20030910
              sa_norm_11+=sabin(lat_min,d_long,d_lat,i_lat); // multiply by solid angle of bin


          for (i_long=il_min1;  i_long<=il_max1           ; i_long++   )//AWS20030910
          for (i_lat =ib_min2;  i_lat <=ib_max2           ; i_lat++    )//AWS20030910
              sa_norm_12+=sabin(lat_min,d_long,d_lat,i_lat); 

          for (i_long=il_min2;  i_long<=il_max2           ; i_long++   )//AWS20030910
	  for (i_lat =ib_min1;  i_lat <=ib_max1           ; i_lat++    )//AWS20030910
              sa_norm_21+=sabin(lat_min,d_long,d_lat,i_lat); // multiply by solid angle of bin


          for (i_long=il_min2;  i_long<=il_max2           ; i_long++   )//AWS20030910
          for (i_lat =ib_min2;  i_lat <=ib_max2           ; i_lat++    )//AWS20030910
              sa_norm_22+=sabin(lat_min,d_long,d_lat,i_lat); 


  profile11=egret_map->ProjectionZ("EGRET_spectrum1" ,il_min1,il_max1, ib_min1,ib_max1);//AWS20040301
  profile12=egret_map->ProjectionZ("EGRET_spectrum2" ,il_min1,il_max1, ib_min2,ib_max2);
  profile21=egret_map->ProjectionZ("EGRET_spectrum3" ,il_min2,il_max2, ib_min1,ib_max1);
  profile22=egret_map->ProjectionZ("EGRET_spectrum4" ,il_min2,il_max2, ib_min2,ib_max2);
  profile11->Scale(1./sa_norm_11);
  profile12->Scale(1./sa_norm_12);
  profile21->Scale(1./sa_norm_21);
  profile22->Scale(1./sa_norm_22);

  /*
  profile11->Scale(1./((il_max1-il_min1+1)*(ib_max1-ib_min1+1)));
  profile12->Scale(1./((il_max1-il_min1+1)*(ib_max2-ib_min2+1)));
  profile21->Scale(1./((il_max2-il_min2+1)*(ib_max1-ib_min1+1)));
  profile22->Scale(1./((il_max2-il_min2+1)*(ib_max2-ib_min2+1)));
  */
  profile  =egret_map->ProjectionZ("EGRET_spectrum5" ,il_min1,il_max1, ib_min1,ib_max1);

  for(i_E_EGRET=0; i_E_EGRET<data.n_E_GLAST                    ; i_E_EGRET++)
   profile->SetBinContent(i_E_EGRET,0);

  profile->Add(profile11,0.25);
  profile->Add(profile12,0.25);
  profile->Add(profile21,0.25);
  profile->Add(profile22,0.25);

  if(galplotdef.convolve_GLAST>=1)
   deconvolve_intensity(profile);

 // profile ->Draw();


 //AWS20060322
  counts_profile11=counts_map->ProjectionZ("EGRET_counts1"   ,il_min1,il_max1, ib_min1,ib_max1);
  counts_profile12=counts_map->ProjectionZ("EGRET_counts2"   ,il_min1,il_max1, ib_min2,ib_max2);
  counts_profile21=counts_map->ProjectionZ("EGRET_counts3"   ,il_min2,il_max2, ib_min1,ib_max1);
  counts_profile22=counts_map->ProjectionZ("EGRET_counts4"   ,il_min2,il_max2, ib_min2,ib_max2);


  
  counts_profile  =counts_map->ProjectionZ("EGRET_counts5"   ,il_min1,il_max1, ib_min1,ib_max1);

  for(i_E_EGRET=0; i_E_EGRET<data.n_E_GLAST                    ; i_E_EGRET++)
   counts_profile->SetBinContent(i_E_EGRET,0);

  // this was an bug:   the counts should not be scaled. only the error bars are affected  AWS20080207
  /*
  counts_profile->Add(counts_profile11,0.25);
  counts_profile->Add(counts_profile12,0.25);
  counts_profile->Add(counts_profile21,0.25);
  counts_profile->Add(counts_profile22,0.25);
  */

  counts_profile->Add(counts_profile11,1.00);
  counts_profile->Add(counts_profile12,1.00);
  counts_profile->Add(counts_profile21,1.00);
  counts_profile->Add(counts_profile22,1.00);

  counts_       =new double[data.n_E_GLAST]; 
 //AWS20060322


  intensity_    =new double[data.n_E_GLAST];
  intensity_esq1=new double[data.n_E_GLAST];
  intensity_esq2=new double[data.n_E_GLAST];


 

         for(i_E_EGRET=0; i_E_EGRET<data.n_E_GLAST                    ; i_E_EGRET++)
        {
	  intensity_[i_E_EGRET]=       profile ->GetBinContent(i_E_EGRET);
	     counts_[i_E_EGRET]=counts_profile ->GetBinContent(i_E_EGRET); //AWS20060322
          
          differential_spectrum(data.E_GLAST[i_E_EGRET],data.E_GLAST[i_E_EGRET+1] ,
                                intensity_[i_E_EGRET], g[i_E_EGRET],
                                intensity_esq1[i_E_EGRET],intensity_esq2[i_E_EGRET]);

	 
	  cout<<"e1,e2="<<data.E_GLAST[i_E_EGRET]<<" "<<data.E_GLAST[i_E_EGRET+1]
              <<" intensity="<<intensity_[i_E_EGRET]<<"intensity_esq1, 2 ="<<intensity_esq1[i_E_EGRET]<<" "<<intensity_esq2[i_E_EGRET]
	      <<" counts="<<counts_[i_E_EGRET]<<endl; //AWS20060322


	  profile11->SetName("axxx");
	  profile12->SetName("bxxx");
	  profile21->SetName("cxxx");
	  profile22->SetName("dxxx");

	  counts_profile11->SetName("exxx"); //AWS20060322
	  counts_profile12->SetName("fxxx");
	  counts_profile21->SetName("gxxx");
	  counts_profile22->SetName("hxxx");


	}//i_E_EGRET

         Ebar=new double[data.n_E_GLAST];
         Ibar=new double[data.n_E_GLAST];

	 // automatic determination of spectral index
	 differential_spectrum(data.E_GLAST, data.n_E_GLAST, intensity_, intensity_esq1, intensity_esq2, Ebar,Ibar);

         cout<<"plot_EGRET_spectrum: E^2*spectrum"<<endl;                            //AWS20040309
	 for(i_E_EGRET=0; i_E_EGRET<data.n_E_GLAST                    ; i_E_EGRET++) //AWS20040309
	   cout<<"GLAST spectrum *E^2: E1 E2 Ebar Ibar "                                                  //AWS20040309
               <<data.E_GLAST[i_E_EGRET]<<" "<<data.E_GLAST[i_E_EGRET+1]             //AWS20040309
               <<" "<<Ebar[i_E_EGRET]<<" "<<Ibar[i_E_EGRET]<<endl;                   //AWS20040309


// tex output  AWS20041119
// txtFILE in global and assigned in plot_spectrum

fprintf(txtFILE,"======================================================\n");
fprintf(txtFILE,"GLAST spectrum                                        \n");   
 fprintf(txtFILE,"     E(MeV)      E^2 * intensity (MeV cm^-2 sr^-1 s^-1)    counts    \n"); //AWS20070906
for(i_E_EGRET=0; i_E_EGRET<data.n_E_GLAST ; i_E_EGRET++)
  {
  fprintf(txtFILE,"%6.0f-%6.0f & %9.5f &                             %9.1f \\\\ \n",data.E_GLAST[i_E_EGRET],data.E_GLAST[i_E_EGRET+1],Ibar[i_E_EGRET],counts_[i_E_EGRET]   );  //AWS20070906
                                   // "\\\\" yields "\\"
  }
fprintf(txtFILE,"======================================================\n");


for(i_E_EGRET=0; i_E_EGRET<data.n_E_GLAST                    ; i_E_EGRET++)
{

 if(galplotdef.spectrum_style_EGRET==1 ||  galplotdef.spectrum_style_EGRET==3)
 {
  for (errorbar=1;errorbar<=2; errorbar++)
    {

          delta_err=0;                                                                  //AWS20040312



          if(errorbar==1)errorbarfactor=    1.0+galplotdef.error_bar_EGRET + delta_err; //AWS20040312
          if(errorbar==2)errorbarfactor=1./(1.0+galplotdef.error_bar_EGRET + delta_err);//AWS20040312

          stat_err=1.0/sqrt(1.0+counts_[i_E_EGRET]);                                    //AWS20060322
          if(errorbar==1)errorbarfactor*= 1.0+stat_err;                                 //AWS20060322
          if(errorbar==2)errorbarfactor/= 1.0+stat_err;                                 //AWS20060322

          cout<<data.E_GLAST[i_E_EGRET]<<" MeV counts="<<counts_[i_E_EGRET] <<" stat_err="<<stat_err<<endl;

	  segment[0]=intensity_esq1[i_E_EGRET]*errorbarfactor;
 	  segment[1]=intensity_esq2[i_E_EGRET]*errorbarfactor;

          

          spectrum=new TGraph(2,&data.E_GLAST[i_E_EGRET],segment);

	  spectrum->SetMarkerColor(kCyan  ); //AWS20080610
	  spectrum->SetMarkerStyle(21); 
	  spectrum->SetMarkerStyle(22); // triangles
	  spectrum->SetMarkerSize (0.5);// 
	  spectrum->SetLineColor(kCyan ); //AWS20080610
	  spectrum->SetLineWidth(3     );
	  spectrum->SetLineStyle(1      );
	  spectrum->SetMarkerStyle(22); // triangles
	  spectrum->SetMarkerSize (0.5);// 
	  spectrum->Draw();       // don't use "same" for TGraph
	  spectrum->Draw(" P ");  // points as markers  

          spectrum->Draw();
     }//errorbar
    }//if


 if(galplotdef.spectrum_style_EGRET==2 ||  galplotdef.spectrum_style_EGRET==3)
 {

          delta_err=0;                                                                  //AWS20040312



         segment[0]=Ibar[i_E_EGRET]*(1.0+galplotdef.error_bar_EGRET + delta_err);       //AWS20040312
         segment[1]=Ibar[i_E_EGRET]/(1.0+galplotdef.error_bar_EGRET + delta_err);       //AWS20040312

         stat_err=1.0/sqrt(1.0+counts_[i_E_EGRET]);                                    //AWS20060322
         segment[0]      *=         (1.0+stat_err);                                    //AWS20060322
         segment[1]      /=         (1.0+stat_err);                                    //AWS20060322

         cout<<data.E_GLAST[i_E_EGRET]<<" MeV counts="<<counts_[i_E_EGRET] <<" stat_err="<<stat_err<<endl;

    Ebar_segment[0]=Ebar[i_E_EGRET];
    Ebar_segment[1]=Ebar[i_E_EGRET];
    spectrum=new TGraph(2,Ebar_segment,segment);

	  spectrum->SetMarkerColor(kCyan  ); //AWS20080610
	  spectrum->SetMarkerStyle(21); 
	  spectrum->SetMarkerStyle(22); // triangles
	  spectrum->SetMarkerSize (0.5);// 
	  spectrum->SetLineColor(kCyan ); //AWS20080610
	  spectrum->SetLineWidth(3     );
	  spectrum->SetLineStyle(1      );
	  spectrum->SetMarkerStyle(22); // triangles
	  spectrum->SetMarkerSize (0.5);// 
	  spectrum->Draw();       // don't use "same" for TGraph
	  spectrum->Draw(" P ");  // points as markers  

          spectrum->Draw();

  }//if

 }//i_EGRET





   cout<<" <<<< plot_GLAST_spectrum    "<<endl;
   return status;
}


