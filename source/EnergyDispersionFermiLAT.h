#include"Fermi_Aeff.h"

class EnergyDispersionFermiLAT
{
 public:

  int nE_true,nE_meas;


  // for thibaut method
//valarray<double> F,S1,K1,BIAS,S2,K2,PINDEX1,PINDEX2;         //AWS20150220
  valarray<double> BIAS;                                       //AWS20150220 for compatibility, to be removed later

  valarray<double> F,S1,K1,BIAS1,S2,K2,BIAS2,PINDEX1,PINDEX2;  //AWS20150220

   valarray<double> parameters_E_low,   parameters_E_high;
   int n_parameter_sets;
   double parameters_E_min,parameters_E_factor;

   valarray<double> Sd_parameters;                             //AWS20150220

   double E_true_threshold;                                    //AWS20150223
   double E_meas_threshold;                                    //AWS20150301

   int parameters_initialized;
   int EnergyDispersionMatrix2_initialized;
   string method;
   int use_Aeff;

   string parameter_file_name;
   string parameter_file_type;                                 //AWS20150220
   string  exposure_file_name;
   string  exposure_file_type;

   vector<valarray<double> > EnergyDispersionMatrix2;
   Fermi_Aeff fermi_Aeff;
   int use_matrix;

  // for rsp method
  vector<valarray<double> > EnergyDispersionMatrix;  


         valarray<double>     E_true,         E_meas;
	          double  log_E_true_min, log_E_meas_min;
	 valarray<double>    dE_true    ,    dE_meas    ;
		  double dlog_E_true,    dlog_E_meas;
  void read(int debug);

  int  read_parameters(int debug);
  int  read_exposure  (int debug);
  int  set_method(string method_);
  int  set_use_Aeff(int use_Aeff_);
  int  set_E_true_threshold(double E_true_threshold_);       //AWS20150223
  int  set_E_meas_threshold(double E_meas_threshold_);       //AWS20150301

  int  set_parameter_file     (string parameter_file_name_);
  int  set_parameter_file_type(string parameter_file_type_); //AWS20150220
  int  set_exposure_file      (string  exposure_file_name_);
  int  set_exposure_file_type (string  exposure_file_type_);

  int  set_use_matrix(int use_matrix_);
  int  reset();

  double value(double E_true,double E_meas, int debug);

  double value_thibaut(double E_true,double E_meas, int debug);
  double g(double x, double sigma, double k, double b, double p,int debug);
  double Sd(double E,int debug);

  void ApplyEnergyDispersion(valarray<double> E,  valarray<double> &spectrum, int debug);
  void ApplyEnergyDispersion(valarray<double> E,  valarray<double> &spectrum, double E_interp_factor,int debug);
  void ApplyEnergyDispersion(valarray<double> E_true_,  valarray<double>  spectrum_true,valarray<double> E_meas_,  valarray<double> &spectrum_meas, int debug);

  void test();
  int initialized; // for rsp method

 

  

 
};
