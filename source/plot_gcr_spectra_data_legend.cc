
#include"Galplot.h"                    //AWS20050920
#include"galprop_classes.h"
#include"galprop.h"
#include"constants.h"
#include"fitsio.h"


int  Galplot::plot_gcr_spectra_data_legend()
{
   cout<<" >>>> plot_gcr_spectra_data_legend    "<<endl;
   int status=0;

GCR_data gcr_data;

char *database_file=new char[400];
char *area_units  =new char[4];
char *energy_units=new char[4];




char name[100],canvastitle[100], workstring1[100],workstring2[100];
char psfile[100];

TCanvas *c1;
TH1F    *h;
TGraph *spectrum;
TText *text;


 int ip;
 double x,y;
 char last_experiment[100];

strcpy(area_units,   "cm2");
strcpy(energy_units, "MeV");
//strcpy(database_file,"GCR_data_1.dat");
strcpy(database_file,galplotdef.gcr_database_file); //AWS20090220

   
 gcr_data.read(database_file ,       area_units ,       energy_units);
 gcr_data.set_plotting();

 // gcr_data.print();

 
 

int ncolors=1; int *colors=0;
gStyle->SetPalette(ncolors,colors);

 // names must be different or canvas disappears

  
  strcpy(canvastitle," galdef ID ");
  strcat(canvastitle,galdef.galdef_ID);
 

  //====== see HowTo Style: and do before creating canvas
 
 
   TStyle *plain  = new TStyle("Plain","Plain Style (no colors/fill areas)"); 

   
   plain->SetCanvasBorderMode(0);
   plain->SetPadBorderMode(0);
   plain->SetPadColor(0);
   plain->SetCanvasColor(0);
   plain->SetTitleColor(0);
   plain->SetStatColor(0);
   gROOT->SetStyle("Plain");
  


  strcpy(name,"gcr_data_legend");// to identify it: name is not plotted

  c1=new TCanvas(name,name       ,20 ,20,300, 900); //AWS20091109  800->900 for more values
 

  strcpy(last_experiment,"dummy");
  y=-0.01; // 0.1 -> 0.01 AWS20090220 -> -0.01 AWS20090602 bottom up

  for  (ip    =0;  ip    <gcr_data.n    ;ip    ++)
  {
    if(strcmp(last_experiment, gcr_data.experiment[ip] ) != 0 )
    {
   spectrum=new TGraph(1);
   x=0.2;
   y+=0.025;//.030; AWS20090911
   spectrum->SetPoint     (0,x,y   );

 
  cout<<" gcr_spectra-data_legend: experiment: "<<  gcr_data.experiment[ip]
      <<" colour= "<<gcr_data.color[ip]<<" style="<<gcr_data.style[ip]<<" size="<<gcr_data.size[ip] <<endl;       

  spectrum->SetMarkerColor(gcr_data.color[ip]);                          //AWS20060621
  spectrum->SetMarkerStyle(gcr_data.style[ip]); // see TAttMarker        //AWS20060621
  spectrum->SetMarkerSize (gcr_data.size [ip]);                          //AWS20060621
  spectrum->SetLineColor  (gcr_data.color[ip] );                         //AWS20060621

  spectrum->Draw(" P ");  // points as markers  
     

  text=new TText();
  text->SetTextFont(62);
  text->SetTextColor(1);
  text->SetTextSize(0.035 );
  text->SetTextAlign(12);


  x=0.3;
  text->DrawText(x ,y, gcr_data.experiment[ip] );


  text=new TText();
  text->SetTextFont(62);
  text->SetTextColor(1);
  text->SetTextSize(0.035 );
  text->SetTextAlign(12);


  x=0.6;
  text->DrawText(x ,y, gcr_data.reference[ip] );
  }


  strcpy(last_experiment, gcr_data.experiment[ip] );

  }// for

  strcpy(psfile,"plots/");
  strcat(psfile,"gcr_spectra_data_legend_");
  strcat(psfile,galdef.galdef_ID);

  strcat(psfile,".eps");

  cout<<"postscript file="<<psfile<<endl;
  c1->Print(psfile       );





   cout<<" <<<< plot_gcr_spectra_data_legend   "<<endl;
   return status;
}


