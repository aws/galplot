


#include"Galplot.h"                    //AWS20050920
#include"galprop_classes.h"
#include"galprop.h"


//#include "fitsio.h" 


int Galplot::read_COMPTEL_data() //AWS20030910 AWS20050921
{
int status=0;


 FITS work;

 char  infile[100];



  cout<<" >>>> read_COMPTEL_data"<<endl;
 

   data.n_E_COMPTEL=3;
   data.E_COMPTEL=new double[data.n_E_COMPTEL+1];
   data.E_COMPTEL[ 0]=    1;
   data.E_COMPTEL[ 1]=    3;
   data.E_COMPTEL[ 2]=   10;
   data.E_COMPTEL[ 3]=   30;

   strcpy( infile,configure.fits_directory);
   strcat( infile,"skymos.SKY.M0210124_method_3_iter49"   ); 
   work.read(infile);
   data.COMPTEL_intensity.init(work.NAXES[0],work.NAXES[1],data.n_E_COMPTEL);

   
   for (int ip       =0;        ip<data.n_E_COMPTEL;    ip++)
     {
    strcpy( infile,configure.fits_directory);

    if(ip==0)strcat( infile,"skymos.SKY.M0210124_method_3_iter49"   ); 
    if(ip==1)strcat( infile,"skymos.SKY.M0210224_method_3_iter70"   ); 
    if(ip==2)strcat( infile,"skymos.SKY.M0210424_method_2_iter99"   ); 

    cout<<"  reading  COMPTEL intensity  from file "<<infile<<endl;

    work.read(infile);

    cout<<"work NAXES: "<<work.NAXES[0]<<" "<<work.NAXES[1]<<endl;

    //work.print();

   for (int ib       =0;        ib<work.NAXES[1];       ib++)
   for (int il       =0;        il<work.NAXES[0];       il++)
         {             
                     data.COMPTEL_intensity.d2[il][ib].s[ip]=work(il,ib);
         }
	 }

   //data.COMPTEL_intensity.print();








  cout<<" <<<< read_COMPTEL:data"<<endl;

  
return status;
}
