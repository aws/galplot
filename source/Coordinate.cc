#include "Coordinate.h"
#include "pointing.h"
#include "PhysicalConstants.h"
#include <iostream>

SM::Coordinate::Coordinate(const pointing &point) {
   m_b = 90 - point.theta/utl::kConvertDegreesToRadians;
   m_l = point.phi/utl::kConvertDegreesToRadians;
}

pointing SM::Coordinate::healpixAng() const{
   const double theta = utl::kPi/2 - utl::kConvertDegreesToRadians*m_b;
   const double phi = utl::kConvertDegreesToRadians*m_l;
   return pointing(theta,phi);
}

namespace SM {
std::ostream & operator << (std::ostream &os, const SM::Coordinate &coord) {
   os << "(" << coord.l() <<","<<coord.b()<<")";
   return os;
}
}
