class FITS {

 public:

  long  NAXIS;
  long *NAXES;
  double *CRVAL,*CDELT,*CRPIX;

  double CHIPOL,PSIPOL,CHIORI,PSIORI;


  int nelements;
  char filename[1000];


  FITS() ;
  FITS(    int NAXIS1);             //AWS20061121
  int init(int NAXIS1);             //AWS20061121
  FITS(    int NAXIS1,int NAXIS2);
  int init(int NAXIS1,int NAXIS2);
  FITS(    int NAXIS1,int NAXIS2,int NAXIS3);
  int init(int NAXIS1,int NAXIS2,int NAXIS3);
  FITS(    int NAXIS1,int NAXIS2,int NAXIS3,int NAXIS4);
  int init(int NAXIS1,int NAXIS2,int NAXIS3,int NAXIS4);


  void setvalues();

  float  *array;
  float & operator()(int i);        //AWS20061121
  float & operator()(int i,int j);
  float & operator()(int i,int j,int k);
  float & operator()(int i,int j,int k,int l);
  float & operator()(int i,int j,int k,int l,int m); //AWS20130903

  FITS operator =(double a);
  FITS operator+=(double a);
  FITS operator-=(double a);        //AWS20101223
  FITS operator*=(double a);
  FITS operator/=(double a);        //AWS20101223

  FITS operator =(FITS a);        //AWS20101223 
  FITS operator+=(FITS a);        //AWS20101223  
  FITS operator-=(FITS a);        //AWS20101223
  FITS operator*=(FITS a);        //AWS20101223  
  FITS operator/=(FITS a);        //AWS20101223  


  void print();
  double sum();
  double max();
  int smooth(int n_smooth);

  int read (char *filename_);
  int read (char *filename_,int hdunum);//AWS20040406
  int read (char *filename_,int hdunum1,int hdunum2,int options);//AWS20070928

  int read(char *filename_,int naxis_min,int naxis_max);
  int write(char *filename_);

  int read_keyword_double (char *filename_,int hdunum,char *keyword,double &value);//AWS20050720
  int write_keyword_double(char *filename_,int hdunum,char *keyword,double  value,char * comment);//AWS20061122
  int write_keyword_double(                int hdunum,char *keyword,double  value,char * comment);//AWS20061122

 private:
  float junk; // used to return pointer when bounds violated AWS20061121

};
















