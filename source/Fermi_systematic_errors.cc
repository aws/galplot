using namespace std;
#include<iostream>
#include<cmath>

double Fermi_systematic_errors(double E,  int mode)
{
  double value,logE;

  value = 0.0; // default

  if (mode==1)
  {

  // mail from Seth Digel, Philippe Bruel 8 April 2009
  //  systematic error on Fermi-LAT Aeff using P6_v3 irfs
  /*
  10% below 2 in log(E)
  10% to  5% linearly in [2,2.75]
   5% to 20% linearly in [2.75,4]
  20% above 4 in log(E)
  */

      logE=log10(E);
                    value = 0.10;
   if(logE > 2.00)  value = 0.10 - 0.05 * (logE-2.0 )/0.75;
   if(logE > 2.75)  value = 0.05 + 0.15 * (logE-2.75)/1.25;
   if(logE > 4.00)  value = 0.20;

  }

  

  return value;

}

//////////////////////////////////////////////////////////////////////
// test program
/*
int main ()
{

  double E,value;
  int mode=1;

  cout<<"testing  Fermi_systematic_errors"<<endl;
  E=100.;
  for(E=10;E<1e5;E*=1.2)
    {
  value= Fermi_systematic_errors( E,  mode);
  cout<<"E="<<E<<"  value="<<value<<endl;
    }
  return 0;
}
*/

