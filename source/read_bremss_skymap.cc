
//**.****|****.****|****.****|****.****|****.****|****.****|****.****|****.****|
// * read_bremss_skymap.cc *                           galprop package * 4/14/2000 
//**"****!****"****!****"****!****"****!****"****!****"****!****"****!****"****|
#include"Galplot.h"                    //AWS20050920
#include"galprop_classes.h"
#include"galprop.h"

#include "fitsio.h" 

int Galplot::read_bremss_skymap()
{
int stat;

 char   comment[100];

  cout<<" >>>> read_bremss_skymap"<<endl;

stat=0;
   fitsfile *fptr;       /* pointer to the FITS file; defined in fitsio.h */
    int status, ii, jj;
    status=0; //AWS20100617
    long  fpixel = 1, naxis = 4, nelements, exposure;
    long naxes[5]  ; 

  
     naxis=3;
     naxes[0]=galaxy.n_long;
     naxes[1]=galaxy.n_lat;
     naxes[2]=galaxy.n_E_gammagrid;
   

     nelements=naxes[0]*naxes[1]*naxes[2];
    

  
    
    float *array;          
    array=new float[nelements];

    char  infile[100];
  
    if(galdef.skymap_format==0)//AWS20100617
   {

    strcpy( infile,configure.fits_directory);
 
   strcat( infile,"bremss_skymap_"); // this has total 

   
    strcat( infile,galdef.galdef_ID);
    cout<<"  reading  bremss skymap  from file "<<infile<<endl;

    status = 0;         /* initialize status before calling fitsio routines */
    fits_open_file(&fptr,  infile,READONLY, &status);   
    cout<<"  fits open status = "<<status<<endl;



    /* Read the array of floats  */
    float nulval=0;
    int anynul;
    fits_read_img(fptr, TFLOAT, fpixel, nelements, &nulval,array, &anynul,&status);
 
  

  

   int i=0; 
    for (int ip       =0;        ip<naxes[2];       ip++)
   for (int ib       =0;        ib<naxes[1];       ib++)
   for (int il       =0;        il<naxes[0];       il++)
         {             
           // cout<<array[i]<<endl;
           galaxy.bremss_skymap.d2[il][ib].s[ip]=array[i];
                       
             i++;
         }


    // galaxy.bremss_skymap .print();

    fits_close_file(fptr, &status);            /* close the file */

    fits_report_error(stderr, status);  /* print out any error messages */


    delete[] array; //AWS20010216


   }// skymap_format==0



    //---------------------------------------------------------------------------------
    // healpix galprop files

   if(galdef.skymap_format==3)
   {
    strcpy( infile,configure.fits_directory);
    strcat( infile,"bremss_healpix_"); // this has total
    strcat( infile,galdef.galdef_ID);


    string mapname;
    mapname=infile;
    cout<<"  reading bremss healpix skymap  from file "<<mapname<<endl;
    galaxy.bremss_hp_skymap.load(mapname);

    //    if(galplotdef.verbose==-2002){cout<<" bremss_hp_skymap  "<<endl;       galaxy.bremss_hp_skymap.print(cout);} AWS20091204
   }

   //---------------------------------------------------------------------------------









  cout<<" <<<< read_bremss_skymap"<<endl;

    return( status );
return stat;
}
