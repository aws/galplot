// in place application of Fermi-LAT energy dispersion to Healpix Skymap
// AWS20140721

using namespace std; 
#include<iostream>

#include"Skymap.h"
#include"EnergyDispersionFermiLAT.h"


////////////////////////////////////////////////////////////////////////////////////////////////////////////
int Skymap_Energy_Dispersion(Skymap<double> &skymap,  double E_interp_factor,  int interpolation, string parameter_file_name, string parameter_file_type, string exposure_file_name, string exposure_file_type,
			     int use_Aeff, double E_true_threshold, double E_meas_threshold,
                             int debug)

{

  cout<<">>Skymap_Energy_Dispersion: exposure_file_name="<<exposure_file_name<<" exposure_file_type="<<exposure_file_type<<endl;
  EnergyDispersionFermiLAT energyDispersionFermiLAT;

  energyDispersionFermiLAT.reset();
  energyDispersionFermiLAT.set_method             ("thibaut");

  //  energyDispersionFermiLAT.set_parameter_file     ("default");
  //  energyDispersionFermiLAT.set_parameter_file_type("default");

  //  energyDispersionFermiLAT.set_parameter_file_type ("ST_FITS");
  //  energyDispersionFermiLAT.set_parameter_file      ("/afs/ipp-garching.mpg.de/home/a/aws/propagate/c/FITS/GLAST/p8_thibaut/edisp_P8V1_ULTRACLEAN_EDISP3.fits");

  energyDispersionFermiLAT.set_parameter_file     (parameter_file_name); //AWS20150225
  energyDispersionFermiLAT.set_parameter_file_type(parameter_file_type); //AWS20150225

  //  exposure_file_name = "/afs/ipp-garching.mpg.de/home/a/aws/propagate/c/FITS/GLAST/p8_thibaut/exp2_healpix7_andy.fits"; // for testing

  energyDispersionFermiLAT.set_exposure_file       (exposure_file_name); 
  energyDispersionFermiLAT.set_exposure_file_type  (exposure_file_type);


  energyDispersionFermiLAT.set_use_Aeff         (use_Aeff);        //AWS20150301
  energyDispersionFermiLAT.set_E_true_threshold (E_true_threshold);//AWS20150301
  energyDispersionFermiLAT.set_E_meas_threshold (E_meas_threshold);//AWS20150301

  energyDispersionFermiLAT.set_use_matrix       (1);

 

  valarray<double> E, spectrum;
  E       .resize(skymap.nSpectra()); // number of energies in spectrum
  spectrum.resize(skymap.nSpectra());

  int iE, ipix;

  int energyDispersion_debug=0;

 

  for (iE=0;iE<skymap.nSpectra();iE++) E[iE]=skymap.getSpectra()[iE];

  if(debug==1) {cout<<"Skymap_Energy_Dispersion: E="; for (iE=0;iE<skymap.nSpectra();iE++)cout<< E[iE]<<" "; cout<<endl;}

  for (ipix=0;ipix<skymap.Npix();ipix++)
  {
   for (iE=0;iE<skymap.nSpectra();iE++)   spectrum    [iE] = skymap[ipix][iE];

   energyDispersion_debug=0; if(ipix==0 && debug==1) energyDispersion_debug=1; //AWS20150204. was 2

   if(interpolation==0) energyDispersionFermiLAT.ApplyEnergyDispersion(E, spectrum,                  energyDispersion_debug);

   if(interpolation==1) energyDispersionFermiLAT.ApplyEnergyDispersion(E, spectrum, E_interp_factor, energyDispersion_debug);

   if(debug==1) for (iE=0;iE<skymap.nSpectra();iE++){cout<<"Skymap_Energy_Dispersion: ipix="<<ipix<<" E="<<E[iE]<<" spectrum before dispersion="<<skymap[ipix][iE] <<" after dispersion="<<spectrum[iE] <<endl;}
 

   for (iE=0;iE<skymap.nSpectra();iE++)   skymap[ipix][iE] = spectrum[iE];
   
  }


  int status=0;
  cout<<">>Skymap_Energy_Dispersion"<<endl;
  return status;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////
int spectrum_Energy_Dispersion(valarray<double> E, valarray<double> &spectrum,  double E_interp_factor,  int interpolation, string parameter_file_name, string parameter_file_type, string exposure_file_name, string exposure_file_type, 
                               int use_Aeff, double E_true_threshold, double E_meas_threshold,
                               int debug)

{

  cout<<">>spectrum_Energy_Dispersion: exposure_file_name="<<exposure_file_name<<" exposure_file_type="<<exposure_file_type<<endl;
  EnergyDispersionFermiLAT energyDispersionFermiLAT;

  energyDispersionFermiLAT.reset();
  energyDispersionFermiLAT.set_method             ("thibaut");

  //  energyDispersionFermiLAT.set_parameter_file     ("default");
  //  energyDispersionFermiLAT.set_parameter_file_type("default");

  //  energyDispersionFermiLAT.set_parameter_file_type     ("ST_FITS");
  //  energyDispersionFermiLAT.set_parameter_file          ("/afs/ipp-garching.mpg.de/home/a/aws/propagate/c/FITS/GLAST/p8_thibaut/edisp_P8V1_ULTRACLEAN_EDISP3.fits");

  energyDispersionFermiLAT.set_parameter_file     (parameter_file_name); //AWS20150225
  energyDispersionFermiLAT.set_parameter_file_type(parameter_file_type); //AWS20150225

  //  exposure_file_name = "/afs/ipp-garching.mpg.de/home/a/aws/propagate/c/FITS/GLAST/p8_thibaut/exp2_healpix7_andy.fits"; // for testing

  energyDispersionFermiLAT.set_exposure_file       (exposure_file_name); 
  energyDispersionFermiLAT.set_exposure_file_type  (exposure_file_type);


  energyDispersionFermiLAT.set_use_Aeff         (use_Aeff);        //AWS20150301
  energyDispersionFermiLAT.set_E_true_threshold (E_true_threshold);//AWS20150301
  energyDispersionFermiLAT.set_E_meas_threshold (E_meas_threshold);//AWS20150301

  energyDispersionFermiLAT.set_use_matrix       (1);

 



  int energyDispersion_debug = debug; 

  valarray<double> spectrum_before= spectrum;

  
  int iE;
  if(debug==1) {cout<<"spectrum_Energy_Dispersion: E="; for (iE=0;spectrum.size();iE++)cout<< E[iE]<<" "; cout<<endl;}

  

    

   if(interpolation==0) energyDispersionFermiLAT.ApplyEnergyDispersion(E, spectrum,                  energyDispersion_debug);

   if(interpolation==1) energyDispersionFermiLAT.ApplyEnergyDispersion(E, spectrum, E_interp_factor, energyDispersion_debug);

   for (iE=0;iE<spectrum.size();iE++){cout<<"E="<<E[iE]<<" spectrum before dispersion="<<spectrum_before[iE] <<" after dispersion="<<spectrum[iE] <<endl;}
 

  

  int status=0;
  cout<<">>spectrum_Energy_Dispersion"<<endl;
  return status;
}
