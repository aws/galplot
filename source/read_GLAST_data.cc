

#include"Galplot.h"                    
#include"galprop_classes.h"
#include"galprop.h"





int Galplot::read_GLAST_data()        
{
 int status=0;

 double exposure_factor;

 double long_min,lat_min,d_long,d_lat;        

 FITS work;

 char  infile[100];



  cout<<" >>>> read_GLAST_data"<<endl;

  /* AWS20131103 this is old pre-launch material, obsolete

  ///////////////////////////// counts
  
    strcpy( infile,configure.fits_directory);

 
 
    // changed to data_GLAST AWS20080508
    if(galplotdef.data_GLAST   ==100)strcat(infile,"GLAST/counts_dc2_full_gal_v1.converted.fits");   // GLAST DC2 simulation official data v1       AWS20060404
    if(galplotdef.data_GLAST   ==101)strcat(infile,"GLAST/counts_obssim_full_gal_v1.converted.fits");// GLAST DC2 simulation using galprop only     AWS20060406
    if(galplotdef.data_GLAST   ==102)strcat(infile,"GLAST/counts_residual_rando_v1.converted.fits"); // GLAST DC2 residuals from Riccardo Rando     AWS20060427
    if(galplotdef.data_GLAST   ==103)strcat(infile,"GLAST/counts_dc2_full_gal_v2.converted.fits");   // GLAST DC2 simulation official data v2       AWS20060517

    if(galplotdef.data_GLAST   ==200)strcat(infile,"GLAST/counts_SC2_all_events.fits");   // GLAST SC2 simulation, >100 MeV, produced by gt2danalysis AWS20070709

    if(galplotdef.sources_EGRET==300)strcat(infile,"GLAST/counts_dc2_full_gal_v2.converted.fits");   // GLAST DC2 v2 simulation, but energy range for MILAGRO  AWS20080104
  
     cout<<"  reading  GLAST counts  from file "<<infile<<endl;


    work.read(infile);

    cout<<"work NAXES: "<<work.NAXES[0]<<" "<<work.NAXES[1]<<" "<<work.NAXES[2]<<endl;


    if(galplotdef.verbose==-1300) // selectable debug                AWS20070709
     work.print();


//   data.EGRET_counts.init(work.NAXES[0],work.NAXES[1],work.NAXES[2]);             AWS20031021

// standard energies + 10000-20000,20000-50000,50000-120000 MeV                     AWS20031021
   if(galplotdef.data_EGRET==0)
   data.EGRET_counts.init(work.NAXES[0],work.NAXES[1],           13);             //AWS20031021

   data.GLAST_counts.init(work.NAXES[0],work.NAXES[1],           13);             //AWS20080509

   

   for (int ip       =0;        ip<work.NAXES[2];       ip++)
   for (int ib       =0;        ib<work.NAXES[1];       ib++)
   for (int il       =0;        il<work.NAXES[0];       il++)
         {               
          if(galplotdef.data_EGRET==0)
	  data.EGRET_counts.d2[il][ib].s[ip]=work(il,ib,ip);  // kept while profiles still use it, but don't overwrite if EGRET also required
          data.GLAST_counts.d2[il][ib].s[ip]=work(il,ib,ip);  
     
	  if(galplotdef.data_GLAST   ==200)  //SC2: longitude scale reversed relative to EGRET
	  data.GLAST_counts.d2[il][ib].s[ip]=work(719-il,ib,0); //AWS20080509 
     
         }


    if(galplotdef.verbose==-1301) // selectable debug                AWS20070710
      data.GLAST_counts.print();                                   //AWS20080509 







   //////////////////////////// exposure 

    strcpy( infile,configure.fits_directory);

 

    // changed to data_GLAST           AWS20080508
    if(galplotdef.data_GLAST   ==100)strcat(infile,"GLAST/exposr_dc2_integ_full_gal_sr_v1.converted.fits");// GLAST DC2 simulation               AWS20060404
    if(galplotdef.data_GLAST   ==101)strcat(infile,"GLAST/exposr_dc2_integ_full_gal_sr_v1.converted.fits");// GLAST DC2 simulation               AWS20060406
    if(galplotdef.data_GLAST   ==102)strcat(infile,"GLAST/exposr_dc2_integ_full_gal_sr_v1.converted.fits");// GLAST DC2 simulation               AWS20060427
    if(galplotdef.data_GLAST   ==103)strcat(infile,"GLAST/exposr_dc2_integ_full_gal_sr_v1.converted.fits");// GLAST DC2 simulation v2 (not yet but should be almost same)AWS20060517


    if(galplotdef.data_GLAST   ==200)strcat(infile,"GLAST/integr_exposure_SC2_all_events.fits");   // GLAST SC2 simulation, >100 MeV, produced by gt2danalysis AWS20070709
                                                                                                   // original has 2D, added third axis keyword NAXIS3(=NAXIS[2])=1 with fv


    if(galplotdef.data_GLAST   ==300)strcat(infile,"GLAST/exposr_dc2_integ_full_gal_sr_v1.converted.fits");   // GLAST DC2 simulation, used also for Milagro
 
   
    cout<<"  reading  GLAST exposure  from file "<<infile<<endl;

    work.read(infile);

    cout<<"work NAXES: "<<work.NAXES[0]<<" "<<work.NAXES[1]<<" "<<work.NAXES[2]<<endl;

    if(galplotdef.verbose==-1300) // selectable debug                AWS20070709
     work.print();


   
   if(galplotdef.data_EGRET==0)
   data.EGRET_exposure.init(work.NAXES[0],work.NAXES[1],  13);

   data.GLAST_exposure.init(work.NAXES[0],work.NAXES[1],  13);

// standard energies   
   for (int ip       =0;        ip<work.NAXES[2];       ip++)
   for (int ib       =0;        ib<work.NAXES[1];       ib++)
   for (int il       =0;        il<work.NAXES[0];       il++)
         {     
          if(galplotdef.data_EGRET==0)        
	  data.EGRET_exposure.d2[il][ib].s[ip]=work(il,ib,ip); // kept while profiles still use it but don't overwrite if EGRET also required
          data.GLAST_exposure.d2[il][ib].s[ip]=work(il,ib,ip);

	 
	  if(galplotdef.data_GLAST   ==200)  //SC2 exposure cm2 -> cm2 sr AWS20080508
	    {
	     lat_min=-89.75; d_long=0.5; d_lat=0.5;     // this information is not in the header. the longitude scale is reversed relative to EGRET
	     //            data.EGRET_exposure.d2[il][ib].s[ip]*=sabin(lat_min,d_long,d_lat,ib);
             data.GLAST_exposure.d2[il][ib].s[ip]= work(719-il,ib,ip)* sabin(lat_min,d_long,d_lat,ib);//AWS20070817
	    }
         }

    if(galplotdef.verbose==-1301) // selectable debug                AWS20070710
     data.GLAST_exposure.print();

   if(galplotdef.data_EGRET==0)
   {
   data.n_E_EGRET = galplotdef.energies_EGRET;//AWS20031021 keep while still used for profiles and convolve
   data.  E_EGRET = new double[14];           //AWS20031021
   }

   data.n_E_GLAST = galplotdef.energies_EGRET;//AWS20080509
   data.  E_GLAST = new double[14];           //AWS20080509

   
   if(galplotdef.data_GLAST   ==100 || galplotdef.data_GLAST   ==101 || galplotdef.data_GLAST   ==102  || galplotdef.data_GLAST   ==103  ) //GLAST DC2 simulations AWS20080508
   {
    if(galplotdef.data_EGRET==0)
   {      
   data.E_EGRET[ 0]=    30;
   data.E_EGRET[ 1]=    60;
   data.E_EGRET[ 2]=   120;
   data.E_EGRET[ 3]=   240;
   data.E_EGRET[ 4]=   480;
   data.E_EGRET[ 5]=   960;
   data.E_EGRET[ 6]=  1920;
   data.E_EGRET[ 7]=  3840;
   data.E_EGRET[ 8]=  7680;
   data.E_EGRET[ 9]= 15360;
   data.E_EGRET[10]= 30720;
   data.E_EGRET[11]= 61440;
   data.E_EGRET[12]=122880;
   data.E_EGRET[13]=245760;
   }

   data.E_GLAST[ 0]=    30;
   data.E_GLAST[ 1]=    60;
   data.E_GLAST[ 2]=   120;
   data.E_GLAST[ 3]=   240;
   data.E_GLAST[ 4]=   480;
   data.E_GLAST[ 5]=   960;
   data.E_GLAST[ 6]=  1920;
   data.E_GLAST[ 7]=  3840;
   data.E_GLAST[ 8]=  7680;
   data.E_GLAST[ 9]= 15360;
   data.E_GLAST[10]= 30720;
   data.E_GLAST[11]= 61440;
   data.E_GLAST[12]=122880;
   data.E_GLAST[13]=245760;

   }

   
  
   if(galplotdef.data_GLAST   ==200)     //GLAST SC2 simulation     AWS20080508
   {
     // only one energy range from gtd2analysis 
    data.E_GLAST[ 0]=   100;
    data.E_GLAST[ 1]= 99999;//150000 in counts EBINS table: 5 digits gave problem with generating plot  
    data.E_GLAST[ 2]=   120;// dummy values
    data.E_GLAST[ 3]=   240;
    data.E_GLAST[ 4]=   480;
    data.E_GLAST[ 5]=   960;
    data.E_GLAST[ 6]=  1920;
    data.E_GLAST[ 7]=  3840;
    data.E_GLAST[ 8]=  7680;
    data.E_GLAST[ 9]= 15360;
    data.E_GLAST[10]= 30720;
    data.E_GLAST[11]= 61440;
    data.E_GLAST[12]=122880;
    data.E_GLAST[13]=245760;

    // fill the maps with duplicates to allow fitting routines to work as for EGRET and DC2 
    for (int ip       =1;        ip<=12;                 ip++)
    for (int ib       =0;        ib<work.NAXES[1];       ib++)
    for (int il       =0;        il<work.NAXES[0];       il++)
    {
     data.GLAST_exposure.d2[il][ib].s[ip] = data.GLAST_exposure.d2[il][ib].s[0];
     data.GLAST_counts  .d2[il][ib].s[ip] = data.GLAST_counts  .d2[il][ib].s[0];
    }   
}
   
   
   if(galplotdef.data_GLAST   ==300)  //MILAGRO 15 TeV point at end    AWS20080508
   {
    data.E_GLAST[ 0]=    30;
    data.E_GLAST[ 1]=    60;                                                                
    data.E_GLAST[ 2]=   120;
    data.E_GLAST[ 3]=   240;
    data.E_GLAST[ 4]=   480;
    data.E_GLAST[ 5]=   960;
    data.E_GLAST[ 6]=  1920;
    data.E_GLAST[ 7]=  3840;
    data.E_GLAST[ 8]=  7680;
    data.E_GLAST[ 9]= 15360;
    data.E_GLAST[10]= 30720;
    data.E_GLAST[11]= 61440;
    data.E_GLAST[12]=15e6  ;//Milagro 15-16 TeV (to get TeV^-1 units)
    data.E_GLAST[13]=16e6  ;

   
}


  obsolete pre-launch material */



   /////////////////////////////////////////////////////
   // healpix data
   /////////////////////////////////////////////////////

  
  string mapname;

  // --------------- counts

  //  mapname="../FITS/counts_Healpix_gardian_Sample.fits"; //AWS20080527

      mapname=configure.fits_directory;                         //AWS20080527
  //  mapname+="GLAST/counts_Healpix_gardian_Sample.fits";      //AWS20080527
  //  mapname+="GLAST/FT1_counts_healpix_flight_1.fits";        //AWS20080527
  //  mapname+="GLAST/FT1_counts_healpix_flight_v2.fits";                 //AWS20080714  original gardian 2 byte counts
  //  mapname+="GLAST/FT1_counts_new_skymap_healpix_flight_v2.fits";      //AWS20080723  new gardian 8 byte skymap format
  //  mapname+="GLAST/FT1_counts_healpix_flight_using_dubois.fits" ;      //AWS20080819 Richard's Aug 13 data, diffuse class by AWS, gardian healpix

      mapname+= galplotdef.GLAST_counts_file;                             //AWS20080819   filename is now a parameter

  cout<<"reading GLAST   counts from "<<mapname<<endl;
  //  data.GLAST_counts_healpix(mapname) ; does not exist so so it another way
  CountsMap  countsmap(mapname) ;
  data.GLAST_counts_healpix= countsmap;

 if (galplotdef.verbose==-1701)
 {
  cout<<" read_GLAST_data"<<endl;
  cout<<"GLAST counts energy bounds:"<<endl;
  for(int i=0;i<  data.GLAST_counts_healpix .getEMin().size();i++)   
    cout<< data.GLAST_counts_healpix.getEMin()[i]<<" -  " << data.GLAST_counts_healpix.getEMax()[i] <<endl; // returns a valarray<double>
  
 

//Skymap<long> countsskymap=data.GLAST_counts_healpix.getCountsMap();//AWS20091210
  Skymap<int>  countsskymap=data.GLAST_counts_healpix.getCountsMap();//AWS20091210

  cout<<" data.GLAST_counts_healpix as Skymap countsskymap Npix= "<< countsskymap .Npix()<<endl;// AWS20091207

  //  cout<<"GLAST counts:"<<endl;  countsskymap.print(cout); AWS20091204
 }

  // --------------- exposure

 // mapname="../FITS/exposure_Healpix_gardian_Sample.fits";//AWS20080527

      mapname=configure.fits_directory;                              //AWS20080527
  //  mapname+="GLAST/exposure_Healpix_gardian_Sample.fits";         //AWS20080527
  //  mapname+="GLAST/exposureHealpix_flight_1.fits";            //AWS20080703 test flight data
  //  mapname+="GLAST/exposureHealpix_flight_v2.fits";           //AWS20080703 test flight data
  //  mapname+="GLAST/exposureHealpix_v2_gulli.fits";            //AWS20080801 from Gulli: previous one invalid
  //  mapname+="GLAST/exposureHealpix_flight_using_dubois.fits"; //AWS20080819   Richard's Aug 13 data, gtltcube gtexpmap by AWS, gardian healpix

      mapname+= galplotdef.GLAST_exposure_file;                  //AWS20080819   filename is now a parameter

  cout<<"reading GLAST exposure from "<<mapname<<endl;

  data.GLAST_exposure_healpix.readFile(mapname);


  data.GLAST_exposure_integrated_init = 0; // flags whether the GLAST_exposure_integrated has been computed: 0=no 1=yes

 if (galplotdef.verbose==-1702)
 {
  cout<<" read_GLAST_data"<<endl;
  cout<<"GLAST exposure energies :"<<endl;
  
  for(int i=0;i< data.GLAST_exposure_healpix.getEnergies().size();i++)    cout<<data.GLAST_exposure_healpix.getEnergies()[i]<<endl;

  Skymap<double> exposureskymap=   data.GLAST_exposure_healpix .getExposureMap();

  cout<<" data.GLAST_exposure_healpix as Skymap exposureskymap Npix= "<< exposureskymap .Npix()<<endl;// AWS20091207
  //  cout<<"GLAST exposure:"<<endl;  exposureskymap.print(cout); AWS20091204
 }

 // --------------- PSF

 // mapname="../FITS/gardian_Sample_psf.fits";  //AWS20080527

  mapname=configure.fits_directory;             //AWS20080527
//mapname+="GLAST/gardian_Sample_psf.fits";     //AWS20080527

  mapname+=galplotdef.GLAST_psf_file;           //AWS20080819    filename is now a parameter

  cout<<"reading GLAST PSF      from "<<mapname<<endl;
  Psf psf(mapname);            // do it this way since read function not available
  data.GLAST_psf_healpix = psf;


 if (galplotdef.verbose==-1703)
 {
  cout<<" read_GLAST_data"<<endl;
  cout<<"GLAST PSF energies:"<<endl;
  for(int i=0;i<    data.GLAST_psf_healpix .getEnergies().size();i++)    cout<<     data.GLAST_psf_healpix.getEnergies()[i]<<endl;
  cout<<"GLAST PSF theta   :"<<endl;
  for(int i=0;i<    data.GLAST_psf_healpix .getTheta   ().size();i++)    cout<<     data.GLAST_psf_healpix.getTheta   ()[i]<<endl;
  cout<<"GLAST PsfVector   :"<<endl;
  for(int i=0;i<    data.GLAST_psf_healpix .getPsfVector().size();i++)
   {cout<<endl;
   for(int j=0;j<   data.GLAST_psf_healpix.getPsfVector()[i].size();j++)    cout<< data.GLAST_psf_healpix.getPsfVector   ()[i][j]<<" ";
   }
 }



  cout<<" <<<< read_GLAST_data"<<endl;

  
return status;
}
