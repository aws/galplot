
//**.****|****.****|****.****|****.****|****.****|****.****|****.****|****.****|
// * plot_EGRET_skymaps.cc *                                galprop package * 5/22/2000 
//**"****!****"****!****"****!****"****!****"****!****"****!****"****!****"****|
#include"Galplot.h"                    //AWS20050923
#include"galprop_classes.h"
#include"galprop.h"
#include"constants.h"
#include"fitsio.h"


int Galplot::plot_EGRET_skymaps(int mode)
{
   cout<<" >>>> plot_EGRET_skymaps    "<<endl;
   int status=0;


int i_comp,i_lat,i_long,ip;
int ii_long,ii_lat;
int il_min,il_max,ib_min,ib_max;
double l,b,l_off;
double exposure,intensity;
double long_min,lat_min;
double d_long, d_lat;
char name[100],canvastitle[100], workstring1[100],workstring2[100];



TCanvas *c1;
TH2F *egret_map;
 


int ncolors=1; int *colors=0;
gStyle->SetPalette(ncolors,colors);


 cout<<data.EGRET_counts.n_rgrid<<" "<<data.EGRET_counts.n_zgrid<<endl;

  ip=4;
  //  for(ip=0;ip<  data.EGRET_counts.n_pgrid                  ;ip++)
 
  {
 {
 // names must be different or canvas disappears

  sprintf(name," EGRET energy range %d",ip);
  strcpy(canvastitle," galdef ID");
  strcat(canvastitle,galdef.galdef_ID);
  strcat(canvastitle,name);


  // EGRET map is 2D Distribution, hence coords are formally r, z

  egret_map=new TH2F(canvastitle,canvastitle,
                  data.EGRET_counts.n_rgrid,       -180,           180.,
                  data.EGRET_counts.n_zgrid,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1));

  long_min= -179.75;//specific for EGRET data counts.gp1234_30.g001
   lat_min  =-89.75;
  d_long      =0.5;     
  d_lat       =0.5;

	 for (i_lat =0;  i_lat <data.EGRET_counts.n_zgrid            ;  i_lat++ ){
          for(i_long=0;  i_long<data.EGRET_counts.n_rgrid             ; i_long++){

            l=       long_min+ d_long   *i_long;
            b=        lat_min+ d_lat    *i_lat;

            exposure =data.EGRET_exposure.d2[i_long][i_lat].s[ip];
            intensity=0.;
            if(exposure>0.)
            intensity=data.EGRET_counts.  d2[i_long][i_lat].s[ip]/exposure;

            //cout<<i_long<<" "<<i_lat<<" "<<exposure<<" "<<intensity<<endl;
            l_off=l-180.;
            if(l_off<0.)l_off+=360.0;
            ii_long=  data.EGRET_counts.n_rgrid-l_off/d_long;
	    //cout<<"l l_off ii_long"<<l<<" "<<l_off<<" "<<ii_long<<endl;

               egret_map    ->SetBinContent(ii_long,i_lat,intensity);   
  

            }  //  i_long
           }   //  i_lat

 

  egret_map       ->GetXaxis()->SetTitle("Galactic longitude");
 

  // construct conventional Galactic longitude axis in two seqments, 180-0 and 340-180
  TF1 *f1=new TF1("f1","-x",   0,180); // root manual p. 149
  TF1 *f2=new TF1("f2","-x", 180,340); // root manual p. 149

  //                      xmin ymin xmax ymax, funcname   ndiv chopt gridlength
  TGaxis *axis1=new TGaxis(-180,-90 ,  0.,-90  , "f1",       9,""                );
  axis1->SetLabelSize(0.03);
  TGaxis *axis2=new TGaxis(  20,-90 ,180.,-90  , "f2",       8,""                );
  axis2->SetLabelSize(0.03);
  axis2->SetTitle("Galactic longitude");
  // fill in 0-340 between two axis segments, unlabelled
  // at present has no visibile effect but could be useful if more minor tickmarks added
  //                         xmin ymin xmax ymax   wmin wmax ndiv chopt gridlength
  TGaxis *axis3=new TGaxis(  0,  -90.  , 20., -90., 0., 20. ,0, "U"               );


  // do the plotting


  if(mode==1)
  c1=new TCanvas(canvastitle,       canvastitle,280+ip*10,200+ip*10,600,400);

  egret_map->GetXaxis()->SetNdivisions(0); //remove axis since longitude axis done separately

  if(mode==1||mode==2)
    {
  egret_map       ->Draw("colz");
  axis1->Draw();   axis2->Draw();   axis3->Draw();
    }
 

  


//==============================     Profiles  =============================



  // construct conventional Galactic longitude axis in two seqments, 180-0 and 340-180
       f1=new TF1("f1","-x",   0,180); // root manual p. 149
       f2=new TF1("f2","-x", 180,340); // root manual p. 149

  //                      xmin ymin xmax ymax, funcname   ndiv chopt gridlength
          axis1=new TGaxis(-180,0.  ,  0.,0.   , "f1",       9,""                );
  axis1->SetLabelSize(0.02);
          axis2=new TGaxis(  20,0.  ,180.,0.   , "f2",       8,""                );
  axis2->SetLabelSize(0.02);
  axis2->SetTitle("Galactic longitude");
  // fill in 0-340 between two axis segments, unlabelled
  // at present has no visibile effect but could be useful if more minor tickmarks added
  //                         xmin ymin xmax ymax   wmin wmax ndiv chopt gridlength
          axis3=new TGaxis(  0,    0.  , 20., 0.  , 0., 20. ,0, "U"               );

  ib_min=(galplotdef.lat_min-galdef.lat_min+.0001)/d_lat;
  ib_max=(galplotdef.lat_max-galdef.lat_min+.0001)/d_lat;
  cout<<"ib_min ibmax:"<<ib_min<<" "<<ib_max<<endl;


  sprintf(workstring2,"  %5.1f>b>%5.1f",galplotdef.lat_min,galplotdef.lat_max);
  strcpy(workstring1,canvastitle);
  strcat(workstring1,workstring2);


  strcat(name,"egret_longitude");// to identify it: name is not plotted
  if(mode==1)
  c1=new TCanvas(name,canvastitle,300+ip*10,150+ip*10,600,600);

  TH1D *profile;


  profile=egret_map->ProjectionX("longplot", ib_min,ib_max);
  profile->Scale(1./(ib_max-ib_min+1));
  profile->GetXaxis()->SetTitle("Galactic longitude");
  profile->GetXaxis()->SetNdivisions(0); // removes longitude axis since done separately
  profile->GetYaxis()->SetTitle("intensity");
  profile->SetTitle(workstring1); //written in box on plot

  if(mode==1)
  profile->Draw();
  if(mode==1)
    {
  axis1->Draw();   axis2->Draw();   axis3->Draw();
    }
 
  

  il_min=(galplotdef.long_min-galdef.long_min+.0001)/d_long;
  il_max=(galplotdef.long_max-galdef.long_min+.0001)/d_long;
  cout<<"il_min ilmax:"<<il_min<<" "<<il_max<<endl;

  sprintf(workstring2,"  %5.1f>l>%5.1f",galplotdef.long_min,galplotdef.long_max);
  strcpy(workstring1,canvastitle);
  strcat(workstring1,workstring2);



  strcat(name,"egret_latitude");
  if(mode==1)
  c1=new TCanvas(name,canvastitle,350+ip*10,150+ip*10,600,600);

  profile=egret_map->ProjectionY("inverse Compton", il_min,il_max);
  profile->Scale(1./(il_max-il_min+1));
  profile->GetXaxis()->SetTitle("Galactic latitude");
  profile->GetYaxis()->SetTitle("intensity");
  profile->SetTitle(workstring1); //written in box on plot

  if(mode==1)
  profile->Draw();


 }//if E_gamma

  
} //ip

//============================================================================




 
 

   cout<<" <<<< plot_EGRET_skymaps    "<<endl;
   return status;
}


 
