#ifndef EXPOSURE_H
#define EXPOSURE_H

#include "Skymap.h"
#include "ArraySlice.h"

#include <string>
#include <valarray>
#include <vector>

/** \brief Exposure stores the exposure maps in appropriate format.
 *
 * Initializes the healpix exposure map from either a healpix fits file or a mapcube fits file.
 * Also does Lagrange polynomial approximation for the exposure between energy points.  Does not
 * handle exposure correction, that is done by the BaseModel class.
 */
class Exposure {
	public:
		/** \brief Standard constructor, does nothing */
		Exposure(){}
		/** \brief Construct an exposure object from fits mapcube.
		 *
		 * \param fileName is the name of the fits file to be opened.  This should
		 * be either a Skymap formatted file or a mapcube in galactic coordinates using the CAR projection.  A full sky
		 * map is needed, but the program will continue with a warning if it finds
		 * something wrong.  The energy dependance of the mapcube should be given
		 * in a separate extension called ENERGIES, containing a table with a
		 * single column of energy in MeV.
		 *
		 * If the exposure is given as a mapcube, it will be rebinned to healpix
		 */
		Exposure(const std::string &fileName);
		/** \brief Read a file to create the exposure.
		 *
		 * \param fileName is the name of file to be read.  Its format can be
		 * either a mapcube as described in the constructor or a Skymap fits file.
		 */
		void readFile(const std::string &fileName);
		/** \brief Write the exposure out in Skymap format
		 *
		 * \param fileName is the name of the output file, which will be
		 * overwritten.
		 */
		void writeFile(const std::string &fileName);
		/** \brief Create a spatially flat exposure with energy dependence 
		 *
		 * \param order is the healpix order
		 * \param energies is the energy binning required for the exposure
		 * \param functionPars is a valarray of function parameters, different size
		 * is expected for different functions
		 * \param function is a string representing the energy dependance needed.
		 * Three values are accepted and the names in the parenthesis indicate the
		 * parameters needed in functionPars array:
		 *  - Linear (slope, constant)
		 *  - Powerlaw (index, prefactor)
		 *  - Array (exposure at every energy)
		 */
		void createTestMap(int order, const std::valarray<double> & energies, const std::valarray<double> & functionPars, const std::string & function);
		/** \brief Return a constant reference to the exposure skymap. */
		const Skymap<double> & getExposureMap() const;
		/** \brief Return a power law weighted exposure for a given coordinate and energy bin
		 *
		 * \param co is the coordinate for which the exposure is evaluated.
		 * \param eMin is the minimum energy boundary
		 * \param eMax is the maximum energy boundary
		 * \param index is the power law index (note that it is not negated)
		 *
		 * \return the exposure weighted with the power law index over the given energy bin.  
		 */
		double getWeightedExposure(SM::Coordinate &co, double eMin, double eMax, double index) const;
		/** \brief Convert an intensity map (units of cm^-2 s^-1 sr^-1
		 * MeV^-1, given that exposure energy is MeV) to counts.
		 *
		 * This method uses a semi-analytical integration, using a
		 * power law interpolation on the input map.
		 *
		 * \param inMap is the intensity map in units of  cm^-2 s^-1 * sr^-1 MeV^-1, given that exposure energy is MeV
		 * \param eMin gives the minimum energy boundary, has to be the
		 * same size as eMax
		 * \param eMax gives the maximum energy boundary
		 * \param order is the output order.  Defaults to the order of
		 * inMap
		 *
		 * \return a double precision skymap of the expected counts of
		 * inMap
		 */
		Skymap<double> mapToCounts(Skymap<double> inMap, const std::valarray<double> &eMin, const std::valarray<double> &eMax, int order = -1) const;
		/** \brief Convert a spectra (units of cm^-2 s^-1
		 * MeV^-1, given that exposure energy is MeV) at a given
		 * coordinate to counts.
		 *
		 * This method uses a semi-analytical integration, using a
		 * power law interpolation on the input spectra.
		 *
		 * \param intensities is the intensity spectra in units of  cm^-2 s^-1 MeV^-1, given that exposure energy is MeV
		 * \param energies is the energies corresponding to the
		 * intensities.  Has to be the same size as intensities.
		 * \param eMin gives the minimum energy boundary, has to be the
		 * same size as eMax
		 * \param eMax gives the maximum energy boundary
		 *
		 * \return a double precision valarray of the expected counts of
		 * the spectra
		 */
		std::valarray<double> spectraToCounts(const std::valarray<double> &intensities, const std::valarray<double> &energied, const std::valarray<double> &eMin, const std::valarray<double> &eMax, const SM::Coordinate &co) const;
		/** \brief Convert a power law spectra (units of cm^-2 s^-1
		 * MeV^-1, given that exposure energy is MeV) at a given
		 * coordinate to counts.
		 *
		 * \param index is the power law index
		 * \param prefactor is the intensity of the spectra at the pivot energy.  It is in units of  cm^-2 s^-1 sr^-1 MeV^-1, given that exposure energy is MeV
		 * \param energies is the energies corresponding to the
		 * intensities.  Has to be the same size as intensities.
		 * \param eMin gives the minimum energy boundary, has to be the
		 * same size as eMax
		 * \param eMax gives the maximum energy boundary
		 * \param order is the output order.  Defaults to the order of
		 * inMap
		 *
		 * \return a double precision valarray of the expected counts of
		 * the spectra
		 */
		std::valarray<double> spectraToCounts(double index, double prefactor, double pivotEnergy, const std::valarray<double> &eMin, const std::valarray<double> &eMax, const SM::Coordinate &co) const;
		/** \brief A constant reference to the energy value the exposure is
		 * evaluated at.
		 */
		const std::valarray<double> & getEnergies() const;
	private:
		/** \brief Create an array of indexes suitable for the exposure
		 * integration.
		 *
		 * \return a vector with all the energies per bin
		 */
		std::vector<std::vector<double> > indexSort(const std::valarray<double> &eMin, const std::valarray<double> &eMax, const std::valarray<double> &energies, std::vector<std::vector<int> > &fluxIndex, std::vector<std::vector<int> > &expIndex) const;
		/** \brief Convert a flux density spectra to counts.  The input
		 * should be in units of cm^-2 s^-1 MeV^-1.
		 *
		 * \param fluxIndex and \param expIndex should be initialized with
		 * indexSort
		 * \param energies is the output from indexSort
		 * \param flux is the input flux, evaluated at \param enFlux
		 */
		std::valarray<double> convertSpectra(const std::vector<std::vector<double> > &energies, const std::valarray<double> &flux, const std::valarray<double> &enFlux, const std::vector<std::vector<int> > &fluxIndex, std::vector<std::vector<int> > &expIndex, const SM::Coordinate &co) const;
		std::valarray<double> convertSpectra(const std::vector<std::vector<double> > &energies, const ArraySlice<double> &flux, const std::valarray<double> &enFlux, const std::vector<std::vector<int> > &fluxIndex, std::vector<std::vector<int> > &expIndex, const SM::Coordinate &co) const;
		/** \brief Calculate Lagrange interpolation */
		void CalculateInterpolation();
		Skymap<double> fDataSkymap;  //!< Store the exposure in a skymap
		Skymap<double> fPrefactor, fIndex;  //!< Store the power-law interpolation indexes
};
#endif

