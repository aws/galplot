


 
#include"Distribution.h"

// GLAST classes from gardian
#include"Counts.h"      //AWS20080519
#include"Exposure.h"    //AWS20080519
#include"Psf.h"         //AWS20080519
#include"Skymap.h"      //AWS20080523

#include "healpix_map.h" //AWS20110413

class Data
{
 public:

  int   n_E_EGRET;
  double *E_EGRET;

  double EGRET_psf_dtheta;

  Distribution EGRET_exposure;
  Distribution EGRET_counts;
  Distribution EGRET_psf;


  int      n_E_GLAST;             //AWS20080508
  double    *E_GLAST;             //AWS20080508
  Distribution GLAST_exposure;    //AWS20080508
  Distribution GLAST_counts;      //AWS20080508
  Distribution GLAST_psf;         //AWS20080508
  double       GLAST_psf_dtheta;  //AWS20080508

  // GLAST data in healpix

  CountsMap     GLAST_counts_healpix;          //AWS20080519
  Exposure      GLAST_exposure_healpix;        //AWS20080519
  Psf           GLAST_psf_healpix;             //AWS20080519
  Skymap<double>GLAST_exposure_integrated;     //AWS20080523
  int           GLAST_exposure_integrated_init;//AWS20080523

  int   n_E_COMPTEL;              //AWS20030910
  double *E_COMPTEL;              //AWS20030910
  Distribution COMPTEL_intensity; //AWS20030910

  // original survey data

  Distribution synchrotron____10MHz;//AWS20050728
  Distribution synchrotron____22MHz;//AWS20050728
  Distribution synchrotron____45MHz;//AWS20050728
  Distribution synchrotron___150MHz;//AWS20070315
  Distribution synchrotron___408MHz;//AWS20050728
  Distribution synchrotron___820MHz;//AWS20070209
  Distribution synchrotron__1420MHz;//AWS20050728
  Distribution synchrotron__2326MHz;//AWS20070219
  Distribution synchrotron_22800MHz;//AWS20050728
  Distribution synchrotron_33000MHz;//AWS20070226
  Distribution synchrotron_41000MHz;//AWS20070226
  Distribution synchrotron_61000MHz;//AWS20070226
  Distribution synchrotron_94000MHz;//AWS20070226

  // rebinned on galprop grid 
  Distribution synchrotron_skymap____10MHz;//AWS20070112
  Distribution synchrotron_skymap____22MHz;//AWS20070112
  Distribution synchrotron_skymap____45MHz;//AWS20070112
  Distribution synchrotron_skymap___150MHz;//AWS20070315
  Distribution synchrotron_skymap___408MHz;//AWS20070112
  Distribution synchrotron_skymap___820MHz;//AWS20070209 
  Distribution synchrotron_skymap__1420MHz;//AWS20070112
  Distribution synchrotron_skymap__2326MHz;//AWS20070219
  Distribution synchrotron_skymap_22800MHz;//AWS20070112
  Distribution synchrotron_skymap_33000MHz;//AWS20070226
  Distribution synchrotron_skymap_41000MHz;//AWS20070226
  Distribution synchrotron_skymap_61000MHz;//AWS20070226
  Distribution synchrotron_skymap_94000MHz;//AWS20070226


  // non-WMAP surveys as Healpix_Map's
  Healpix_Map<double> synchrotron_Healpix__408MHz;//AWS20130121  from lambda website

  // WMAP 7-year polarization data
  Healpix_Map<double> synchrotron_WMAP_I_22800MHz;//AWS20110413
  Healpix_Map<double> synchrotron_WMAP_I_33000MHz;//AWS20110413
  Healpix_Map<double> synchrotron_WMAP_I_41000MHz;//AWS20110413
  Healpix_Map<double> synchrotron_WMAP_I_61000MHz;//AWS20110413
  Healpix_Map<double> synchrotron_WMAP_I_94000MHz;//AWS20110413


  Healpix_Map<double> synchrotron_WMAP_Q_22800MHz;//AWS20110413
  Healpix_Map<double> synchrotron_WMAP_Q_33000MHz;//AWS20110413
  Healpix_Map<double> synchrotron_WMAP_Q_41000MHz;//AWS20110413
  Healpix_Map<double> synchrotron_WMAP_Q_61000MHz;//AWS20110413
  Healpix_Map<double> synchrotron_WMAP_Q_94000MHz;//AWS20110413

  Healpix_Map<double> synchrotron_WMAP_U_22800MHz;//AWS20110413
  Healpix_Map<double> synchrotron_WMAP_U_33000MHz;//AWS20110413
  Healpix_Map<double> synchrotron_WMAP_U_41000MHz;//AWS20110413
  Healpix_Map<double> synchrotron_WMAP_U_61000MHz;//AWS20110413
  Healpix_Map<double> synchrotron_WMAP_U_94000MHz;//AWS20110413

  Healpix_Map<double> synchrotron_WMAP_P_22800MHz;//AWS20110413
  Healpix_Map<double> synchrotron_WMAP_P_33000MHz;//AWS20110413
  Healpix_Map<double> synchrotron_WMAP_P_41000MHz;//AWS20110413
  Healpix_Map<double> synchrotron_WMAP_P_61000MHz;//AWS20110413
  Healpix_Map<double> synchrotron_WMAP_P_94000MHz;//AWS20110413

  // rebinned on galprop grid 
  Distribution synchrotron_I_skymap_22800MHz;//AWS20110426
  Distribution synchrotron_I_skymap_33000MHz;//AWS20110426
  Distribution synchrotron_I_skymap_41000MHz;//AWS20110426
  Distribution synchrotron_I_skymap_61000MHz;//AWS20110426
  Distribution synchrotron_I_skymap_94000MHz;//AWS20110426


  Distribution synchrotron_Q_skymap_22800MHz;//AWS20110426
  Distribution synchrotron_Q_skymap_33000MHz;//AWS20110426
  Distribution synchrotron_Q_skymap_41000MHz;//AWS20110426
  Distribution synchrotron_Q_skymap_61000MHz;//AWS20110426
  Distribution synchrotron_Q_skymap_94000MHz;//AWS20110426

  Distribution synchrotron_U_skymap_22800MHz;//AWS20110426
  Distribution synchrotron_U_skymap_33000MHz;//AWS20110426
  Distribution synchrotron_U_skymap_41000MHz;//AWS20110426
  Distribution synchrotron_U_skymap_61000MHz;//AWS20110426
  Distribution synchrotron_U_skymap_94000MHz;//AWS20110426

  Distribution synchrotron_P_skymap_22800MHz;//AWS20110426
  Distribution synchrotron_P_skymap_33000MHz;//AWS20110426
  Distribution synchrotron_P_skymap_41000MHz;//AWS20110426
  Distribution synchrotron_P_skymap_61000MHz;//AWS20110426
  Distribution synchrotron_P_skymap_94000MHz;//AWS20110426

  // WMAP 7-year           MEM templates

  // WMAP 7-year free-free MEM templates
  Healpix_Map<double> free_free_WMAP_MEM_22800MHz;//AWS20110621
  Healpix_Map<double> free_free_WMAP_MEM_33000MHz;//AWS20110621
  Healpix_Map<double> free_free_WMAP_MEM_41000MHz;//AWS20110621
  Healpix_Map<double> free_free_WMAP_MEM_61000MHz;//AWS20110621
  Healpix_Map<double> free_free_WMAP_MEM_94000MHz;//AWS20110621

  // skymaps for all frequencies derived from WMAP 7-year free-free MEM templates
  // keep consistent nomenclature: append _hp_skymap if Skymap,  _skymap if Distribution
  Skymap<double> free_free_WMAP_MEM_hp_skymap;      //AWS20110621
  Distribution   free_free_WMAP_MEM_skymap;         //AWS20110621



  // WMAP 7-year dust      MEM templates
  Healpix_Map<double>      dust_WMAP_MEM_22800MHz;//AWS20111221
  Healpix_Map<double>      dust_WMAP_MEM_33000MHz;//AWS20111222
  Healpix_Map<double>      dust_WMAP_MEM_41000MHz;//AWS20111222
  Healpix_Map<double>      dust_WMAP_MEM_61000MHz;//AWS20111222
  Healpix_Map<double>      dust_WMAP_MEM_94000MHz;//AWS20111222

  // skymaps for all frequencies derived from WMAP 7-year      dust MEM templates
 
  Skymap<double>      dust_WMAP_MEM_hp_skymap;      //AWS20111222
  Distribution        dust_WMAP_MEM_skymap;         //AWS20111222



  // WMAP 7-year synchrotron  MEM templates
  Healpix_Map<double> synchrotron_WMAP_MEM_22800MHz;//AWS20111221
  Healpix_Map<double> synchrotron_WMAP_MEM_33000MHz;//AWS20111222
  Healpix_Map<double> synchrotron_WMAP_MEM_41000MHz;//AWS20111222
  Healpix_Map<double> synchrotron_WMAP_MEM_61000MHz;//AWS20111222
  Healpix_Map<double> synchrotron_WMAP_MEM_94000MHz;//AWS20111222

  // skymaps for all frequencies derived from WMAP 7-year synchrotron MEM templates
 
  Skymap<double>      synchrotron_WMAP_MEM_hp_skymap;      //AWS20111222
  Distribution        synchrotron_WMAP_MEM_skymap;         //AWS20111222


  


  // WMAP 7-year MCMC spinning dust model templates
  // Gold etal. 2011 ApJSupp 192,15.
  // these are provided at one frequency, with power-law fixed at +2.0 for dust
  // and beta_d=+2.0, nu_sd=4.2 GHz in equation(10) for spinning dust.

  Healpix_Map<double>      dust_WMAP_MCMC_SD_94000MHz;     //AWS20120116
  Healpix_Map<double>    dust_Q_WMAP_MCMC_SD_94000MHz;     //AWS20120117
  Healpix_Map<double>    dust_U_WMAP_MCMC_SD_94000MHz;     //AWS20120117

  Healpix_Map<double> spin_dust_WMAP_MCMC_SD_22800MHz;     //AWS20120116

  // skymaps for all frequencies derived from WMAP 7-year MCMC spinning dust model templates
 
  Skymap<double>           dust_WMAP_MCMC_SD_hp_skymap;      //AWS20120116
  Distribution             dust_WMAP_MCMC_SD_skymap;         //AWS20120116

  Skymap<double>         dust_Q_WMAP_MCMC_SD_hp_skymap;      //AWS20120117
  Distribution           dust_Q_WMAP_MCMC_SD_skymap;         //AWS20120117

  Skymap<double>         dust_U_WMAP_MCMC_SD_hp_skymap;      //AWS20120117
  Distribution           dust_U_WMAP_MCMC_SD_skymap;         //AWS20120117

  Skymap<double>         dust_P_WMAP_MCMC_SD_hp_skymap;      //AWS20120117
  Distribution           dust_P_WMAP_MCMC_SD_skymap;         //AWS20120117


  Skymap<double>      spin_dust_WMAP_MCMC_SD_hp_skymap;      //AWS20120116
  Distribution        spin_dust_WMAP_MCMC_SD_skymap;         //AWS20120116


 };
