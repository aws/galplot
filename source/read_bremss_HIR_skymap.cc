
//**.****|****.****|****.****|****.****|****.****|****.****|****.****|****.****|
// * read_bremss_HIR_skymap.cc *                           galprop package * 4/14/2000 
//**"****!****"****!****"****!****"****!****"****!****"****!****"****!****"****|
#include"Galplot.h"                    //AWS20050920
#include"galprop_classes.h"
#include"galprop.h"

#include "fitsio.h" 
int Galplot::read_bremss_HIR_skymap()
{
int stat;

 char   comment[100];

  cout<<" >>>> read_bremss_HIR_skymap"<<endl;

stat=0;
   fitsfile *fptr;       /* pointer to the FITS file; defined in fitsio.h */
    int status, ii, jj;
    long  fpixel = 1, naxis = 4, nelements, exposure;
    long naxes[5]  ; 

  
     naxis=4;
     naxes[0]=galaxy.n_long;
     naxes[1]=galaxy.n_lat;
     naxes[2]=galaxy.HIR.n_zgrid;// number of rings
     naxes[3]=galaxy.n_E_gammagrid;
   

     nelements=naxes[0]*naxes[1]*naxes[2]*naxes[3];
    

  
    
    float *array;          
    array=new float[nelements];

    char  infile[100];
  
    strcpy( infile,configure.fits_directory);
 
   strcat( infile,"bremss_HIR_skymap_"); // 

   
    strcat( infile,galdef.galdef_ID);
    cout<<"  reading  bremss skymap  from file "<<infile<<endl;

    status = 0;         /* initialize status before calling fitsio routines */
    fits_open_file(&fptr,  infile,READONLY, &status);   
    cout<<"  fits open status = "<<status<<endl;
    if(status!=0) return status;


    /* Read the array of floats  */
    float nulval=0;
    int anynul;
    fits_read_img(fptr, TFLOAT, fpixel, nelements, &nulval,array, &anynul,&status);
 
  

  

   int i=0; 
   for (int ip       =0;        ip     <naxes[3];       ip++)
   for (int i_Ring   =0;        i_Ring <naxes[2];    i_Ring++)
   for (int ib       =0;        ib     <naxes[1];       ib++)
   for (int il       =0;        il     <naxes[0];       il++)
         {             
           // cout<<array[i]<<endl;
        
             galaxy.bremss_HIR_skymap .d3[il][ib][i_Ring].s[ip]=array[i];;
             i++;
         }


    // galaxy.bremss_HIR_skymap .print();

    fits_close_file(fptr, &status);            /* close the file */

    fits_report_error(stderr, status);  /* print out any error messages */


    delete[] array; //AWS20010216

  cout<<" <<<< read_bremss_HIR_skymap"<<endl;

    return( status );
return stat;
}
