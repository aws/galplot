#include "HIH2IC.h"
#include "Model.h"
#include "Exposure.h"
#include "Counts.h"
#include "Psf.h"
#include "Sources.h"
#include "Parameters.h"
#include "Variables.h"
#include "Skymap.h"
//#include "SkymapFitsio.h"  AWS20200601 SkymapFitsio.cc outdated, not used
#include <string>
#include <vector>

// developed by Andy Strong, 20080422 from TestModel_orig.cc
// a 3 component model consisting of 1. HI-related  (pi0-decay + bremss) 2. H2-related  (pi0-decay + bremss) 3. inverse Compton skymaps
// HI CO summed over rings
// HII added to HI          AWS20090916

HIH2IC::HIH2IC(const Exposure &exposure, const CountsMap &counts, const Psf &psf, const Sources &sources, const Parameters &pars, const Skymap<char> &filter,unsigned int configure):  //AWS20080717 
         BaseModel(counts,exposure,psf,sources,pars,filter,configure)
        {
          // bool conv is optional parameter (defined in header), default conv=true 


	std::cout<<"Model HIH2IC";
        std::cout<<std::endl;

	//Get some parameters from the Parameters object.  These might point to skymaps or mapcubes.
	//Note that the Parameters object throws an exception if the parameter name is not in the object.
      
        std::string dirName;                                            //AWS20080613
	std::string mapName;
	std::string galdefID;
	std::string galprop_version;                                    //AWS20080623
	std::string ring;
        Skymap<double> ringmap;
        int iring,nrings;

	//The Parameters object can handle most data types
        fparameters.getParameter("galpropFitsDirectory", dirName);      //AWS20080613
	std::cout<<"dirName="<<dirName<<std::endl;                      //AWS20080613
        fparameters.getParameter("galdefID",galdefID);
	std::cout<<"galdefID="<<galdefID<<std::endl;
	

	//The Parameters object should contain initial values for the variables we modify in the model
	//The variables object has a method to add variables from parameters objects.  We only 
	//need to supply a vector of variable names to look for
	std::vector<std::string> variableNames;
	variableNames.push_back("HIR_ModelMapScale");
       	variableNames.push_back("H2R_ModelMapScale");
	variableNames.push_back("ics_ModelMapScale");



	//Set up the variables from the parameters
	//Note that the fvariables is inherited from BaseModel
	fvariables.add(variableNames, fparameters);

        nrings=9; // standard maps with 9 Galactocentric rings
       
	// determine number of rings from galprop version
        galprop_version=galdefID.substr(0,2); // extract version eg 54_610202hpS -> version 54  (0=start, 2=length)

        if(galprop_version<= "53" ) nrings= 9; // old rings from Seth
        if(galprop_version>= "54" ) nrings=17; // new rings from Seth

	std::cout<<"HIH2IC: galprop_version= "<<galprop_version<<" number of skymap rings= "<<nrings<<std::endl;

        for(iring=1;iring<=nrings;iring++)
	{
        if(iring== 1)ring= "1";
        if(iring== 2)ring= "2";
        if(iring== 3)ring= "3";
        if(iring== 4)ring= "4";
        if(iring== 5)ring= "5";
        if(iring== 6)ring= "6";
        if(iring== 7)ring= "7";
        if(iring== 8)ring= "8";
        if(iring== 9)ring= "9";
        if(iring==10)ring="10"; //AWS20080623
        if(iring==11)ring="11";
        if(iring==12)ring="12";
        if(iring==13)ring="13";
        if(iring==14)ring="14";
        if(iring==15)ring="15";
        if(iring==16)ring="16";
        if(iring==17)ring="17";

	// HI rings

        mapName= dirName+ "pi0_decay_HIR_ring_"+ring+"_healpix_" + galdefID;    //AWS20080613

       	std::cout<<"mapName="<<mapName<<std::endl;
     
        ringmap.load(mapName);//Read in a skymap from a fits file
        if(iring==1) fHIMap =ringmap;
        if(iring> 1) fHIMap+=ringmap;

        mapName= dirName+ "bremss_HIR_ring_"+ring+"_healpix_"    + galdefID;    //AWS20080613

       	std::cout<<"mapName="<<mapName<<std::endl;

        ringmap.load(mapName);	//Read in a skymap from a fits file
                    fHIMap +=ringmap;
        

	// HII rings added to  HI                                                 AWS20090916

        mapName= dirName+ "pi0_decay_HII_ring_"+ring+"_healpix_" + galdefID;    //AWS20090916

       	std::cout<<"mapName="<<mapName<<std::endl;
     
        ringmap.load(mapName);//Read in a skymap from a fits file
                    fHIMap+=ringmap;

        mapName= dirName+ "bremss_HII_ring_"+ring+"_healpix_"    + galdefID;    //AWS20090916

       	std::cout<<"mapName="<<mapName<<std::endl;

        ringmap.load(mapName);	//Read in a skymap from a fits file
                    fHIMap +=ringmap;


        // H2 (CO) rings
        mapName= dirName+ "pi0_decay_H2R_ring_"+ring+"_healpix_" + galdefID;    //AWS20080613
      	std::cout<<"mapName="<<mapName<<std::endl;
     
        ringmap.load(mapName);//Read in a skymap from a fits file
        if(iring==1) fH2Map =ringmap;
        if(iring> 1) fH2Map+=ringmap;

	
        mapName= dirName+ "bremss_H2R_ring_"+ring+"_healpix_"    + galdefID;    //AWS20080613
       	std::cout<<"mapName="<<mapName<<std::endl;

        ringmap.load(mapName);	//Read in a skymap from a fits file
                    fH2Map +=ringmap;

	
	}


	// constrain summed HIR map to be a number (eliminate nan's)  to avoid data  problem (found for H2R but check anyway) AWS20080623
	//      fHIMap.print(std::cout);
	
        int nans_found=0;
        int negs_found=0; // if required later as an option

	for (int co = 0; co < fHIMap.Npix(); ++co)
        {
	 for (int i = 0; i < fHIMap.nSpectra(); ++i)
         {
	  if(isnan(fHIMap[co][i])    ){/*std::cout<<fHIMap[co][i]<<" ";*/    fHIMap[co][i]=0.0    ;nans_found++; /* std::cout<<fHIMap[co][i]<<std::endl;*/} // nan test 
	
	  if(      fHIMap[co][i]< 0.0){/*std::cout<<fHIMap[co][i]<<" ";*/    fHIMap[co][i]=0.0    ;negs_found++; /* std::cout<<fHIMap[co][i]<<std::endl;*/}  
	 }
	}

	if(nans_found>0) std::cout<<" WARNING: "<<nans_found<< " nans found in summed healpix  HIR rings: they are set to zero !!"<<std::endl;
      	if(negs_found>0) std::cout<<" WARNING: "<<negs_found<< " negs found in summed healpix  HIR rings: they are set to zero !!"<<std::endl;

	// constrain summed H2R map to be a number (eliminate nan's)  to avoid data  problem  AWS20080623
	//      fH2Map.print(std::cout);
	
            nans_found=0;
            negs_found=0; // if required later as an option

	for (int co = 0; co < fH2Map.Npix(); ++co)
        {
	 for (int i = 0; i < fH2Map.nSpectra(); ++i)
         {
	  if(isnan(fH2Map[co][i])    ){/*std::cout<<fH2Map[co][i]<<" ";*/    fH2Map[co][i]=0.0    ;nans_found++; /* std::cout<<fH2Map[co][i]<<std::endl;*/} // nan test 
	
	  if(      fH2Map[co][i]< 0.0){/*std::cout<<fH2Map[co][i]<<" ";*/    fH2Map[co][i]=0.0    ;negs_found++; /* std::cout<<fH2Map[co][i]<<std::endl;*/}               
	 }
	}

	if(nans_found>0) std::cout<<" WARNING: "<<nans_found<< " nans found in summed healpix  H2R rings: they are set to zero !!"<<std::endl;
        if(negs_found>0) std::cout<<" WARNING: "<<negs_found<< " negs found in summed healpix  H2R rings: they are set to zero !!"<<std::endl;





	//Convert flux to counts, use the fluxToCounts method of BaseModel
	 fHIMap = fluxToCounts( fHIMap);
	 fH2Map = fluxToCounts( fH2Map);

	//Convolve the maps, using the convolve method of BaseModel
	 fHIMap =     convolve( fHIMap);
	 fH2Map =     convolve( fH2Map);



	// ICS skymap

	 //	fparameters.getParameter("ics_ModelMapName", mapName); //AWS20080416 AWS20080620
	 //     automatic name as for other files

	mapName=dirName+"/ics_isotropic_healpix_" + galdefID;                      //AWS20080620
      	std::cout<<"mapName="<<mapName<<std::endl;                                 //AWS20080620

	//Read in a skymap from a fits file
	ficsMap.load(mapName);

	//Convert flux to counts, use the fluxToCounts method of BaseModel
	ficsMap = fluxToCounts(ficsMap);

	//Convolve the map, using the convolve method of BaseModel
	ficsMap = convolve(ficsMap);



}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
HIH2IC::HIH2IC(const Exposure &exposure, const CountsMap &counts, const Psf &psf, const Sources &sources, const Parameters &pars, const Skymap<char> &filter, int nrings_in,unsigned int configure): //AWS20080623 AWS20080717
         BaseModel(counts,exposure,psf,sources,pars,filter,configure)
        {
	  // version with number of rings as input parameter   
          // bool conv is optional parameter (defined in header), default conv=true        

	std::cout<<"Model HIH2IC, nrings_in="<<nrings_in;
        std::cout<<std::endl;

	//Get some parameters from the Parameters object.  These might point to skymaps or mapcubes.
	//Note that the Parameters object throws an exception if the parameter name is not in the object.
      
        std::string dirName;                                            //AWS20080613
	std::string mapName;
	std::string galdefID;
	std::string ring;
        Skymap<double> ringmap;
        int iring,nrings;

	//The Parameters object can handle most data types
        fparameters.getParameter("galpropFitsDirectory", dirName);      //AWS20080613
	std::cout<<"dirName="<<dirName<<std::endl;                      //AWS20080613
        fparameters.getParameter("galdefID",galdefID);
	std::cout<<"galdefID="<<galdefID<<std::endl;
	

	//The Parameters object should contain initial values for the variables we modify in the model
	//The variables object has a method to add variables from parameters objects.  We only 
	//need to supply a vector of variable names to look for
	std::vector<std::string> variableNames;
	variableNames.push_back("HIR_ModelMapScale");
       	variableNames.push_back("H2R_ModelMapScale");
	variableNames.push_back("ics_ModelMapScale");
        variableNames.push_back("HII_ModelMapScale");//AWS20091218



	//Set up the variables from the parameters
	//Note that the fvariables is inherited from BaseModel
	fvariables.add(variableNames, fparameters);

        nrings=nrings_in; // standard maps with number of Galactocentric rings specified on input 
       
        for(iring=1;iring<=nrings;iring++)
	{
        if(iring== 1)ring= "1";
        if(iring== 2)ring= "2";
        if(iring== 3)ring= "3";
        if(iring== 4)ring= "4";
        if(iring== 5)ring= "5";
        if(iring== 6)ring= "6";
        if(iring== 7)ring= "7";
        if(iring== 8)ring= "8";
        if(iring== 9)ring= "9";
        if(iring==10)ring="10";
        if(iring==11)ring="11";
        if(iring==12)ring="12";
        if(iring==13)ring="13";
        if(iring==14)ring="14";
        if(iring==15)ring="15";
        if(iring==16)ring="16";
        if(iring==17)ring="17";
     
	// HI rings

        mapName= dirName+ "pi0_decay_HIR_ring_"+ring+"_healpix_" + galdefID;    //AWS20080613

       	std::cout<<"mapName="<<mapName<<std::endl;
     
        ringmap.load(mapName);//Read in a skymap from a fits file
        if(iring==1) fHIMap =ringmap;
        if(iring> 1) fHIMap+=ringmap;

        mapName= dirName+ "bremss_HIR_ring_"+ring+"_healpix_"    + galdefID;    //AWS20080613

       	std::cout<<"mapName="<<mapName<<std::endl;

        ringmap.load(mapName);	//Read in a skymap from a fits file
                    fHIMap +=ringmap;
        

        // H2 (CO) rings
        mapName= dirName+ "pi0_decay_H2R_ring_"+ring+"_healpix_" + galdefID;    //AWS20080613
      	std::cout<<"mapName="<<mapName<<std::endl;
     
        ringmap.load(mapName);//Read in a skymap from a fits file
        if(iring==1) fH2Map =ringmap;
        if(iring> 1) fH2Map+=ringmap;

	
        mapName= dirName+ "bremss_H2R_ring_"+ring+"_healpix_"    + galdefID;    //AWS20080613
       	std::cout<<"mapName="<<mapName<<std::endl;

        ringmap.load(mapName);	//Read in a skymap from a fits file
                    fH2Map +=ringmap;

	
	
	

       // HII rings                                                            //AWS20091218
        mapName= dirName+ "pi0_decay_HII_ring_"+ring+"_healpix_" + galdefID;
        std::cout<<"mapName="<<mapName<<std::endl;

        ringmap.load(mapName);//Read in a skymap from a fits file
        if(iring==1) fHIIMap =ringmap;
        if(iring> 1) fHIIMap+=ringmap;


        mapName= dirName+ "bremss_HII_ring_"+ring+"_healpix_"    + galdefID;
        std::cout<<"mapName="<<mapName<<std::endl;

        ringmap.load(mapName);  //Read in a skymap from a fits file
                    fHIIMap +=ringmap;


        }



	// constrain summed HIR map to be a number (eliminate nan's)  to avoid data  problem (found for H2R but check anyway) AWS20080623
	//      fHIMap.print(std::cout);
	
        int nans_found=0;
        int negs_found=0; // if required later as an option

	for (int co = 0; co < fHIMap.Npix(); ++co)
        {
	 for (int i = 0; i < fHIMap.nSpectra(); ++i)
         {
	  if(isnan(fHIMap[co][i])    ){/*std::cout<<fHIMap[co][i]<<" ";*/    fHIMap[co][i]=0.0    ;nans_found++; /* std::cout<<fHIMap[co][i]<<std::endl;*/} // nan test 
	
	  if(      fHIMap[co][i]< 0.0){/*std::cout<<fHIMap[co][i]<<" ";*/    fHIMap[co][i]=0.0    ;negs_found++; /* std::cout<<fHIMap[co][i]<<std::endl;*/} //AWS20080722
	 }
	}

	if(nans_found>0) std::cout<<" WARNING: "<<nans_found<< " nans found in summed healpix  HIR rings: they are set to zero !!"<<std::endl;
      	if(negs_found>0) std::cout<<" WARNING: "<<negs_found<< " negs found in summed healpix  HIR rings: they are set to zero !!"<<std::endl;

	// constrain summed H2R map to be a number (eliminate nan's)  to avoid data  problem  AWS20080623
	//      fH2Map.print(std::cout);
	
            nans_found=0;
            negs_found=0; // if required later as an option

	for (int co = 0; co < fH2Map.Npix(); ++co)
        {
	 for (int i = 0; i < fH2Map.nSpectra(); ++i)
         {
	  if(isnan(fH2Map[co][i])    ){/*std::cout<<fH2Map[co][i]<<" ";*/    fH2Map[co][i]=0.0    ;nans_found++; /* std::cout<<fH2Map[co][i]<<std::endl;*/} // nan test 
	
	  if(      fH2Map[co][i]< 0.0){/*std::cout<<fH2Map[co][i]<<" ";*/    fH2Map[co][i]=0.0    ;negs_found++; /* std::cout<<fH2Map[co][i]<<std::endl;*/} //AWS20080722
	 }
	}

	if(nans_found>0) std::cout<<" WARNING: "<<nans_found<< " nans found in summed healpix  H2R rings: they are set to zero !!"<<std::endl;
        if(negs_found>0) std::cout<<" WARNING: "<<negs_found<< " negs found in summed healpix  H2R rings: they are set to zero !!"<<std::endl;







	//Convert flux to counts, use the fluxToCounts method of BaseModel
	 fHIMap = fluxToCounts( fHIMap);
	 fH2Map = fluxToCounts( fH2Map);
        fHIIMap = fluxToCounts(fHIIMap); //AWS20091218

	//Convolve the maps, using the convolve method of BaseModel
	 fHIMap =     convolve( fHIMap);
	 fH2Map =     convolve( fH2Map);
        fHIIMap =     convolve(fHIIMap); //AWS20091218


	// ICS skymap

	 //	fparameters.getParameter("ics_ModelMapName", mapName); //AWS20080416 AWS20080620
	 //     automatic name as for other files

	mapName=dirName+"/ics_isotropic_healpix_" + galdefID;                      //AWS20080620
      	std::cout<<"mapName="<<mapName<<std::endl;                                 //AWS20080620

	//Read in a skymap from a fits file
	ficsMap.load(mapName);

	//Convert flux to counts, use the fluxToCounts method of BaseModel
	ficsMap = fluxToCounts(ficsMap);

	//Convolve the map, using the convolve method of BaseModel
	ficsMap = convolve(ficsMap);



}

///////////////////////////////////////////////////////////////////////////////////////////////////////////
void HIH2IC::getMap(const Variables &variables, Skymap<double> &map){
	//Create the output map with the same size as the counts map, or in our case
	//we simply multiply the loadedMap with the scale factor and assign it to the output map
	//Here we could change the stored floadedMap, but that would make calculations of the gradient 
	//more expensive.  In this case, the multiplication should not be too cpu intensive.

  
	double  HIScale = variables["HIR_ModelMapScale"];
	double  H2Scale = variables["H2R_ModelMapScale"];
	double HIIScale = variables["HII_ModelMapScale"];//AWS20091218
	double  icScale = variables["ics_ModelMapScale"];

#pragma omp parallel for default(shared) schedule(dynamic)
	for (int co = 0; co < map.Npix(); ++co){
		for (int i = 0; i < map.nSpectra(); ++i){
	   //	  map[co][i] +=  fHIMap[co][i]*HIScale + fH2Map[co][i]*H2Scale + ficsMap[co][i]*icScale;
		  map[co][i] +=  fHIMap[co][i]*HIScale + fH2Map[co][i]*H2Scale + ficsMap[co][i]*icScale +  fHIIMap[co][i]*HIIScale; //AWS20091218
		}
	}
}

BaseModel::gradMap HIH2IC::getComponents(const Variables & variables, const std::string &prefix){
   gradMap output;
   output["HI"] =   fHIMap;
   output["HI"] *= variables["HIR_ModelMapScale"];
   output["H2"] =   fH2Map;
   output["H2"] *= variables["H2R_ModelMapScale"];
   output["IC"] =  ficsMap;
   output["IC"] *= variables["ics_ModelMapScale"];
   return output;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
void HIH2IC::getGrads(const Variables & variables, const std::string & varName, Skymap<double> &map){
	//Calculate the gradient and put it in a map of strings/skymap pairs.
	//If we are calculating	the gradient for the scaled map, then it is easy
	if (varName == "HIR_ModelMapScale") { 
	  map +=  fHIMap;
	}

	if (varName == "H2R_ModelMapScale") { 
	  map +=  fH2Map;                   
	}


	if (varName == "ics_ModelMapScale") { 
		map += ficsMap;
	}

}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
BaseModel::gradMap HIH2IC::getSecDer(const Variables & vars, const std::string & varName1, const std::string & varName2){
	gradMap output;
	//There is nothing to be done, since it is a linear model

	return output;
}
