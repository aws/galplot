#include"Galplot.h"                    //AWS20050920
#include"galprop_classes.h"
#include"galprop.h"
#include"constants.h"
#include"fitsio.h"


//                                                                                                     Galplot.h : int sourcepop=1 default parameter AWS20090107         

int Galplot::plot_source_population_profile(int longprof, int latprof,int ip,int sublimit,int soplimit, int total, int sourcepop)                  //AWS20090107
{
   cout<<" >>>> plot_source_population_profile    "<<endl;
   int status=0;


int i_comp,i_lat,i_long;
int ii_long,ii_lat;
int il_min,il_max,ib_min,ib_max;
int il_min1,il_max1,ib_min1,ib_max1;
int il_min2,il_max2,ib_min2,ib_max2;


double long_min;
double l,b,l_off;
double E_bar,spectrum_factor;
int iip;

char name[100],canvastitle[100], workstring1[100],workstring2[100],workstring3[100],workstring4[100];
char psfile[400];




TH2F *total_map, *sublimit_map, *soplimit_map;
TH1D *profile;

unsigned seed;


//====================================================================================

 



  
 
 // names must be different or get strange effects

//  sprintf(name,"sources total skymap %5.0f - %5.0f MeV",data.E_EGRET[ip],data.E_EGRET[ip+1]);
  sprintf(name,"sources total skymap %d",ip);//AWS20090107

  strcpy(canvastitle,"galdef_");
  strcat(canvastitle,galdef.galdef_ID);
  strcat(canvastitle,name);




  total_map =new TH2F(name,name,
                   galaxy.n_long,       -180,           180.,
                   galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1));

  //  sprintf(name,"sources sublimit map %5.0f - %5.0f MeV",data.E_EGRET[ip],data.E_EGRET[ip+1]);
  sprintf(name,"sources sublimit map %d",ip);//AWS20090107
  sublimit_map=new TH2F(name,name,            //AWS2090107
                   galaxy.n_long,       -180,           180.,
                   galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1));

  //  sprintf(name,"sources soplimit map %5.0f - %5.0f MeV",data.E_EGRET[ip],data.E_EGRET[ip+1]);
  sprintf(name,"sources soplimit map %d",ip);//AWS20090107
  soplimit_map=new TH2F(name,name,           //AWS20090107
                   galaxy.n_long,       -180,           180.,
                   galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1));


 for (i_lat =0;  i_lat <galaxy.n_lat;  i_lat++ )
 {
  for(i_long=0;  i_long<galaxy.n_long; i_long++)
  {

    l=galaxy.long_min+galaxy.d_long*i_long;
    b=galaxy. lat_min+galaxy. d_lat*i_lat;


    l_off=l-180.;
    if(l_off<0.)l_off+=360.0;
    ii_long= galaxy.n_long-int(l_off/galaxy.d_long);
    
    //  cout<<"l l_off ii_long i_lat sourcepop1.skymap_intensity  "<<l<<" "<<l_off<<" "<<ii_long<<" "<<i_lat<<" "<<sourcepop1.skymap_intensity   .d2[i_long][i_lat].s[0] <<endl;


   if(sourcepop==1) //AWS20090107
   {
    if(galplotdef.sourcepop_total==1) //AWS20060126               
        total_map->SetBinContent(ii_long,i_lat, sourcepop1.skymap_intensity           .d2[i_long][i_lat].s[ip]); 
    if(galplotdef.sourcepop_sublimit==1)//AWS20060126  
     sublimit_map->SetBinContent(ii_long,i_lat, sourcepop1.skymap_intensity_sublimit  .d2[i_long][i_lat].s[ip]); 
    if(galplotdef.sourcepop_soplimit==1)//AWS20060126  
     soplimit_map->SetBinContent(ii_long,i_lat, sourcepop1.skymap_intensity_soplimit  .d2[i_long][i_lat].s[ip]); 
   }


   if(sourcepop==5) //AWS20090107
   {
    if(galplotdef.sourcepop_total==1) //AWS20060126               
        total_map->SetBinContent(ii_long,i_lat, sourcepop5.skymap_intensity           .d2[i_long][i_lat].s[ip]); 
    if(galplotdef.sourcepop_sublimit==1)//AWS20060126  
     sublimit_map->SetBinContent(ii_long,i_lat, sourcepop5.skymap_intensity_sublimit  .d2[i_long][i_lat].s[ip]); 
    if(galplotdef.sourcepop_soplimit==1)//AWS20060126  
     soplimit_map->SetBinContent(ii_long,i_lat, sourcepop5.skymap_intensity_soplimit  .d2[i_long][i_lat].s[ip]); 
   }

    }  //  i_long
  }   //  i_lat



 if(galplotdef.verbose==-1000) // selective debug
   {
    if(sourcepop==1) //AWS20090107
    {
     cout<<"sourcepop1.skymap_intensity:"<<endl;
            sourcepop1.skymap_intensity.print();
    }

   if(sourcepop==5) //AWS20090107
    {
     cout<<"sourcepop5.skymap_intensity:"<<endl;
            sourcepop5.skymap_intensity.print();
    }

   }

				/*
  E_bar=sqrt(data.E_EGRET[ip]*data.E_EGRET[ip+1]);
  spectrum_factor=pow(E_bar/sourcepop1.spectrum_norm_E,  -sourcepop1.spectrum_g);
  spectrum_factor *= (data.E_EGRET[ip+1]-data.E_EGRET[ip])/(data.E_EGRET[1]-data.E_EGRET[0])  ;
  cout<<"E="<<data.E_EGRET[ip]<<" -  "<<data.E_EGRET[ip+1]<<" E_bar="<<E_bar<<" spectrum factor ="<<spectrum_factor<<endl;

     total_map->Scale(spectrum_factor);
  sublimit_map->Scale(spectrum_factor);
  soplimit_map->Scale(spectrum_factor);
				*/
 
	 

//==============================     Profiles  =============================




//----------------------------------------------------------------------
  // NB reversed longitude axis starting at +180 and decreasing

  l_off=180.-galplotdef.long_max ; if(l_off>360.)l_off-=360.; if(l_off<  0.)l_off+=360.;
  il_min =int(l_off/galdef.d_long);
  l_off=180.-galplotdef.long_min ; if(l_off>360.)l_off-=360.; if(l_off<  0.)l_off+=360.;
  il_max =int(l_off/galdef.d_long);
  cout<<"il_min  ilmax :"<<il_min <<" "<<il_max <<endl;



  ib_min=int((galplotdef.lat_min-galdef.lat_min+.0001)/galdef.d_lat);
  ib_max=int((galplotdef.lat_max-galdef.lat_min+.0001)/galdef.d_lat);
  cout<<"ib_min ibmax:"<<ib_min<<" "<<ib_max<<endl;




  // two l or two b ranges


  l_off=180.-galplotdef.long_max1; if(l_off>360.)l_off-=360.; if(l_off<  0.)l_off+=360.;
  il_min1=int(l_off/galdef.d_long);
  l_off=180.-galplotdef.long_min1; if(l_off>360.)l_off-=360.; if(l_off<  0.)l_off+=360.;
  il_max1=int(l_off/galdef.d_long);
  cout<<"il_min1 ilmax1:"<<il_min1<<" "<<il_max1<<endl;

  l_off=180.-galplotdef.long_max2; if(l_off>360.)l_off-=360.; if(l_off<  0.)l_off+=360.;
  il_min2=int(l_off/galdef.d_long);
  l_off=180.-galplotdef.long_min2; if(l_off>360.)l_off-=360.; if(l_off<  0.)l_off+=360.;
  il_max2=int(l_off/galdef.d_long);
  cout<<"il_min2 ilmax2:"<<il_min2<<" "<<il_max2<<endl;


  ib_min1=int((galplotdef.lat_min1-galdef.lat_min+.0001)/galdef.d_lat);
  ib_max1=int((galplotdef.lat_max1-galdef.lat_min+.0001)/galdef.d_lat);
  cout<<"ib_min1 ibmax1:"<<ib_min1<<" "<<ib_max1<<endl;

  ib_min2=int((galplotdef.lat_min2-galdef.lat_min+.0001)/galdef.d_lat);
  ib_max2=int((galplotdef.lat_max2-galdef.lat_min+.0001)/galdef.d_lat);
  cout<<"ib_min2 ibmax2:"<<ib_min2<<" "<<ib_max2<<endl;






  long_min=-180.; // ic_map etc have longitude centred at 0.0






	  //----- total sources

  if(galplotdef.sourcepop_total==1)
  {
   
	    
   if(longprof)
   {

  profile=     total_map   ->ProjectionX("source intensity1",  ib_min1,ib_max1);      
  profile->Add(total_map   ->ProjectionX("source intensity2" , ib_min2,ib_max2),1.0);
  profile->Scale(1.0/(ib_max1-ib_min1+1 + ib_max2-ib_min2+1));
  
  profile->GetXaxis()->SetTitle("Galactic longitude");
  profile->GetXaxis()->SetNdivisions(0); // removes longitude axis since done separately
  profile->GetYaxis()->SetTitle("intensity");
  profile->SetTitle(workstring1); //written in box on plot

  profile->SetLineStyle(1     );// 1=solid 2=dash
  profile->SetLineWidth(3     );
  profile->Draw("same L"); // L=line (instead of histogram)
  profile->SetLineColor(kCyan);
 

 }//longprof
 
 
  if(latprof)
  {
      
  profile=     total_map->ProjectionY("source intensity3", il_min1,il_max1);             
  profile->Add(total_map->ProjectionY("source intensity4", il_min2,il_max2),1.0);      
  profile->Scale(1./(il_max1-il_min1+1 +il_max2-il_min2+1 ));

  


  profile->SetLineStyle(1     );// 1=solid 2=dash 3=dot
  profile->SetLineWidth(3     );
  profile->SetLineColor(kCyan );
  profile->Draw("same L");//L=line (instead of histogram)

 }//latprof
	 


}//if total



	  //----- sources below detection limit

  if(galplotdef.sourcepop_sublimit==1)
  {
   
	    
   if(longprof)
   {

  profile=     sublimit_map   ->ProjectionX("source intensity5",  ib_min1,ib_max1);      
  profile->Add(sublimit_map   ->ProjectionX("source intensity6" , ib_min2,ib_max2),1.0);
  profile->Scale(1.0/(ib_max1-ib_min1+1 + ib_max2-ib_min2+1));
  
  profile->GetXaxis()->SetTitle("Galactic longitude");
  profile->GetXaxis()->SetNdivisions(0); // removes longitude axis since done separately
  profile->GetYaxis()->SetTitle("intensity");
  profile->SetTitle(workstring1); //written in box on plot

  profile->SetLineStyle(2     );// 1=solid 2=dash
  profile->SetLineWidth(6     );
  profile->Draw("same L"); // L=line (instead of histogram)
  profile->SetLineColor(kRed );//AWS20050916
 

 }//longprof
 
 
  if(latprof)
  {
      
  profile=     sublimit_map->ProjectionY("source intensity7", il_min1,il_max1);             
  profile->Add(sublimit_map->ProjectionY("source intensity8", il_min2,il_max2),1.0);      
  profile->Scale(1./(il_max1-il_min1+1 +il_max2-il_min2+1 ));

  


  profile->SetLineStyle(2     );// 1=solid 2=dash 3=dot
  profile->SetLineWidth(6     );
  profile->SetLineColor(kRed  );
  profile->Draw("same L");//L=line (instead of histogram)

 }//latprof
	 


}//if sublimit
	 

 

	  //----- sources above detection limit

  if(galplotdef.sourcepop_soplimit==1)
  {
   
	    
   if(longprof)
   {

  profile=     soplimit_map   ->ProjectionX("source intensity9 ",  ib_min1,ib_max1);      
  profile->Add(soplimit_map   ->ProjectionX("source intensity10" , ib_min2,ib_max2),1.0);
  profile->Scale(1.0/(ib_max1-ib_min1+1 + ib_max2-ib_min2+1));
  
  profile->GetXaxis()->SetTitle("Galactic longitude");
  profile->GetXaxis()->SetNdivisions(0); // removes longitude axis since done separately
  profile->GetYaxis()->SetTitle("intensity");
  profile->SetTitle(workstring1); //written in box on plot

  profile->SetLineStyle(1     );// 1=solid 2=dash
  profile->SetLineWidth(3     );
  profile->Draw("same L"); // L=line (instead of histogram)
  profile->SetLineColor(kBlue);
 

 }//longprof
 
 
  if(latprof)
  {
      
  profile=     soplimit_map->ProjectionY("source intensity11", il_min1,il_max1);             
  profile->Add(soplimit_map->ProjectionY("source intensity12", il_min2,il_max2),1.0);      
  profile->Scale(1./(il_max1-il_min1+1 +il_max2-il_min2+1 ));

  


  profile->SetLineStyle(1     );// 1=solid 2=dash 3=dot
  profile->SetLineWidth(3     );
  profile->SetLineColor(kBlue );
  profile->Draw("same L");//L=line (instead of histogram)

 }//latprof
	 


}//if soplimit



 

   cout<<" <<<< plot_source_population_profile   "<<endl;
   return status;
}


 
