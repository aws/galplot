#ifndef PSF_H
#define PSF_H

#include <string>
#include <vector>
#include <valarray>
#include "healpix_base.h"
#include "healpix_map.h"
#include "Exposure.h"

/** \brief Handles the conversion of the PSF from a table to a healpix map.
 *
 * Reads the point spread function from a fits table, having the same format as
 * the output of gtpsf.
 */
class Psf{
	public:
		/** \brief Standard constructor */
		Psf() {}
		/** \brief Construct the object from a fits file.
		 *
		 * \param fileName gives the name of the file to open.  This is a fits
		 * file, where the point spread function is defined in an extension named
		 * PSF.  This extension has a table with 3 columns, energy, exposure and
		 * psf, where the last column is a vector containing the function values
		 * for each value of theta.  The values of theta are stored in another
		 * extension, THETA in a table with a single column containing the theta
		 * values in degrees.  The energy in the PSF table is supposed to be in MeV.
		 */
		Psf(const std::string &fileName);
		/** \brief Construct the object from a 2 dimensional vector.
		 *
		 * \param psf is a 2 dimensional vector, where the first index corresponds
		 * to theta and the second index to energy
		 * \param energies a vector of energy values in MeV
		 * \param theta a vector of angles in degrees
		 */
		Psf(const std::vector< std::vector<double> > & psf, const std::vector<double> & energies, const std::vector<double> &theta);
		/** \brief Return a Healpix_Map of the point spread function for a given
		 * energy.
		 *
		 * \param energy is the energy for which to evaluate the psf.  The closest
		 * available energy is actually used, no interpolation performed.
		 * \param order gives the order of the map.  If it is 0 (the default) the
		 * order is calculated from the resolution of the psf.
		 * \param center gives the direction of the psf.  This is useful when
		 * calculating the maps of point sources.  The method used to calculate the
		 * map when the pointing is (0,0) (the default) is more accurate than if it
		 * is rotated.
		 * \return A Healpix_Map with the point spread function in the correct
		 * place.
		 */
		Healpix_Map<double> getPsfMap(double energy, int order = 0, const pointing &center = pointing(0,0), double psfFraction=1) const;
		/** \brief Returns an effective PSF map given an energy range and a power
		 * law index for the energy dependance of the source flux.
		 *
		 * \param eMin the minimum energy
		 * \param eMax the maximum energy
		 * \param index the power law index of the source flux
		 * \param order the order of the resulting map.  If it is 0 (the default)
		 * then the order is approximated from the psf resolution.
		 * \param center the position of the psf center.  Calculation is more
		 * accurate when it is (0,0) (the default).
		 * \return The power law weighted map of the psf in the correct position.
		 */
		Healpix_Map<double> getEffectiveMap(double eMin, double eMax, double index, int order = 0, const pointing &center = pointing(0,0), double psfFraction=1) const;
		/** \brief Convolve a skymap with the psf
		 *
		 * \param skyMap is the input skymap to be convolved.  It should be in RING
		 * scheme to make the computation faster.		 
		 *
		 * \param deconvolve can be set to true for deconvolution.
		 * Does not work very well.
		 *
		 * \return the convolved skymap, which has the same structure as the
		 * original one.
		 *
		 * Uses the fast spherical harmonics decomposition of healpix to do the
		 * convolution.  Assumes a homogeneous spherically symmetric psf, since that
		 * is the only thing that can be handled by this method.  In the case of
		 * GLAST, this should not matter much.
		 */
		Skymap<double> convolve(const Skymap<double> & skyMap, bool deconvolve=false) const;
		/** \brief Convolve a skymap with the psf
		 *
		 * \param skyMap is the input skymap to be convolved.  It should be in RING
		 * scheme to make the computation faster.		 
		 *
		 * \param fraction is set to the fraction of psf to take into
		 * account.  Will not speed up the convolution but combined
		 * with SparseSkymap it can make for less memory consumption.
		 *
		 * \return the convolved skymap, which has the same structure as the
		 * original one.
		 *
		 * Uses brute force to do the convolution, still assuming a
		 * homogeneous spherically symmetric psf.
		 */
		Skymap<double> convolveBruteForce(const Skymap<double> & skyMap, double fraction = 1.0) const;
		/** \brief Given an energy range and a power law index, calculate an effective psf vector, weighted with the power law.
		 *
		 * \param eMin the minimum energy
		 * \param eMax the maximum energy
		 * \param index the power law index of the source flux
		 * \return a vector of weighted psf values, corresponding to the theta
		 * values of the actual underlying psf.
		 */
		std::vector<double> getEffectivePsf(double eMin, double eMax, double index) const;
		/** \brief Given an energy value, return the approximate width of the psf
		 * at the energy closest to the expected value.
		 *
		 * \param energy is approximated to the nearest energy point of the psf
		 * object.
		 * \param fraction is the psf containment limit
		 * \return the value of theta in degrees
		 */
		double getPsfWidth(double energy, double fraction) const;
		/** \brief Return a constant reference to the raw psf values */
		const std::vector< std::vector<double> > &getPsfVector() const;
		/** \brief Return a constant reference to the theta values */
		const std::vector<double> &getTheta() const;
		/** \brief Return a constant reference to the energy values */
		const std::vector<double> &getEnergies() const;
	private:
		/** \brief Read the psf table from fits file.
		 *
		 * \param fileName is the name of fits file to read.
		 */
		void ReadPsfFits(const std::string &fileName);
		/** \brief Calculate Lagrange polynomyal interpolation of the energy
		 * dependance of the Psf.
		 *
		 * Used when calculating the effective psf.
		 */
		void CalculateInterpolation();
		/** \brief Given a psf vector values, return a map.
		 * 
		 * \param center the position of the psf
		 * \param psf a vector containing the psf values as a function of theta
		 * \param order is the order of the output map
		 * \return The psf in a Healpix_Map.
		 */
		Healpix_Map<double> createMap(const pointing & center, const std::vector<double> & psf, int order, double psfFraction=1) const;
		/** \brief Given a psf, return the approximate width of the psf
		 *
		 * \param psf is a vector containing the psf values as a
		 * function of theta
		 * \param energy is approximated to the nearest energy point of the psf
		 * object.
		 * \param fraction is the psf containment limit
		 * \return the value of theta in degrees
		 */
		double getPsfWidth(const std::vector<double> & psf, double fraction) const;
		std::vector<double> fEnergies, fTheta; //<! Store the energy values and theta values
		std::vector< std::vector<double> > fPsf; //<! Store the actual psf
		std::vector< std::vector< std::valarray<double> > > fLagrange; //<! Store the Lagrange approximation.
};
#endif
