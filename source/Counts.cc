#include "Counts.h"
#include <valarray>
#include <CCfits/CCfits>
#include <cmath>
#include <fstream>
#include "Parameters.h"


CountsMap::CountsMap(const Parameters &pars){
  
	//Start by trying a cached old file
	std::string countsOutFile="NONE";

{
		//Read from countsFile
		std::string countsFile;
		pars.getParameter("countsFile", countsFile);
		std::cout<<"Reading counts from file "<<countsFile<<std::endl;
		//Check the parameters for order and energy binning parameters.  If not
		//found, assume this is binned healpix output
		//TODO check the file for headers.
		int order = 0;
		try{
			pars.getParameter("countOrder", order);
		}catch (Parameters::ParameterError){ }
		//Check for order, if set we assume FT1 files
		if (order) {
			std::string energyBinning;
			std::valarray<double> eMinArr, eMaxArr;
			pars.getParameter("energyBinning", energyBinning);
			if (energyBinning == "linear" || energyBinning == "log"){
				int nEnergyBins;
				double eMin, eMax;
				pars.getParameter("nEnergyBins", nEnergyBins);
				pars.getParameter("eMin", eMin);
				pars.getParameter("eMax", eMax);
				eMinArr.resize(nEnergyBins);
				eMaxArr.resize(nEnergyBins);
				if (energyBinning == "linear" ){
					double binsize = (eMax-eMin)/nEnergyBins;
					for (int i = 0; i < nEnergyBins; ++i){
						eMinArr[i] = eMin+i*binsize;
						eMaxArr[i] = eMin+(i+1)*binsize;
					}
				}else {
					double binsize = log(eMax/eMin)/nEnergyBins;
					for (int i = 0; i < nEnergyBins; ++i){
						eMinArr[i] = eMin*exp(i*binsize);
						eMaxArr[i] = eMin*exp((i+1)*binsize);
					}
				}
			}else if(energyBinning == "list"){
				std::vector<double> eMinVec;
				std::vector<double> eMaxVec;
				pars.getParameter("eMinList", eMinVec);
				pars.getParameter("eMaxList", eMaxVec);
				eMinArr.resize(eMinVec.size());
				eMaxArr.resize(eMaxVec.size());
				eMinArr = std::valarray<double>(&eMinVec[0], eMinVec.size());
				eMaxArr = std::valarray<double>(&eMaxVec[0], eMaxVec.size());
			}else{
				std::cout<<"Energy binning specified \""<<energyBinning<<"\" not available!"<<std::endl;
				std::cout<<"Available values (case sensitive): linear, log and list"<<std::endl;
				throw (CountsMapException("Energy binning specified \""+energyBinning+"\" not available!\nAvailable values (case sensitive): linear, log and list"));
			}
			//Try to get the filter from the parameters
			std::string filter;
			try{
				pars.getParameter("FT1filter", filter);
			}catch (Parameters::ParameterError){ }
			//Get the energy column from the parameters
			fEnergyCol = "ENERGY";
			try{
			   pars.getParameter("FT1EnergyCol", fEnergyCol);
			} catch (Parameters::ParameterError) {}
			reset(order, eMinArr, eMaxArr);
			addFile(countsFile, filter);
		}else{
			ReadHealpixFits(countsFile);
		}
		if (countsOutFile != "NONE"){
			write(countsOutFile);
		}
	}
}

CountsMap::CountsMap(const std::string &fileName): fLost(0){
	ReadHealpixFits(fileName);
}	

CountsMap::CountsMap(int order, std::valarray<double> & eMin, std::valarray<double> & eMax){
	reset(order, eMin, eMax);
}

CountsMap::CountsMap(const CountsMap & oldCountsMap){
	(*this) = oldCountsMap;
}

CountsMap::CountsMap(const std::string &fileName, int order, std::valarray<double> & eMin, std::valarray<double> & eMax, const std::string & filter) :fLost(0){
	reset(order, eMin, eMax);
	addFile(fileName, filter);
}

void CountsMap::addPhotons(const std::valarray< std::pair< SM::Coordinate, double > > & photonArray){
	for (int i = 0; i < photonArray.size(); ++i){
		Add(photonArray[i]);
	}
}

void CountsMap::addFile(const std::string & fileName, const std::string & filter){
	//Open the file and throw exception if failure occurs
	std::ifstream is(fileName.c_str());
	if (is.fail()){
		throw(CountsMapException("Failed to open file \""+fileName+"\""));
	}
	
	//Read the first line and look for SIMPLE keyword.  If found, read the file
	//directly, otherwise, loop over each line in the file, assuming it is a text
	//file containing list of FT1 files
	std::string line;
	std::getline(is, line);
	if (line.compare(0,6,"SIMPLE") == 0) {
		//We have a FITS file
		ReadFT1Fits(fileName, filter);
	}else{
		//Assume we have a list of FITS files, one per line
		while (true) {
			ReadFT1Fits(line, filter);
			std::getline(is, line);
			if (is.eof()){
				break;
			}
			if (is.fail()){
				throw(CountsMapException("Error parsing file \""+fileName+"\" assuming it contains a list of FT1 FITS filenames, one per line"));
			}
		}
	}
}

void CountsMap::reset(int order, std::valarray<double> &eMin, std::valarray<double> & eMax){
	//Check the valarrays are the same size and in each case, EMin[i] < EMax[i]
	//and EMax[i] <= EMin[i+1];
	if (eMin.size() != eMax.size()){
		throw(CountsMapException("Boundary array sizes do not conform."));
	}else{
		if (eMin.size() < 1){
			throw(CountsMapException("Must have at least one energy bin."));
		}
		for (int i = 0; i < eMin.size(); ++i){
			if (eMin[i] >= eMax[i]){
				throw(CountsMapException("Energy bins must have size > 0"));
			}
			if (i != eMin.size() - 1 && eMax[i] > eMin[i+1]){
				throw(CountsMapException("The energy bins can't ovelap and must be in ascending order!"));
			}
		}
	}
	//Need to resize the valarrays manually, not done in assignment.
	fEMin.resize(eMin.size());
	fEMax.resize(eMax.size());
	fEMin = eMin;
	fEMax = eMax;
	//Resize the skymap in binned mode
	fDataSkymap.Resize(order, eMin, eMax);
	fLost = 0;
}

//const Skymap<long> & CountsMap::getCountsMap() const{
const Skymap<int> & CountsMap::getCountsMap() const{                           //AWS20091002
	return fDataSkymap;
}

const std::valarray<double> & CountsMap::getEMax() const{
	return fEMax;
}

const std::valarray<double> & CountsMap::getEMin() const{
	return fEMin;
}

int CountsMap::lost() const{
	return fLost;
}

void CountsMap::write(const std::string & fileName) const{
   fDataSkymap.write(fileName);
}


CountsMap & CountsMap::operator = (const CountsMap & oldCountsMap){
	if (this != &oldCountsMap){
		fDataSkymap = oldCountsMap.fDataSkymap;
		fEMin.resize(oldCountsMap.fEMin.size());
		fEMax.resize(oldCountsMap.fEMax.size());
		fEMin = oldCountsMap.fEMin;
		fEMax = oldCountsMap.fEMax;
		fLost = oldCountsMap.fLost;
	}
	return (*this);
}

void CountsMap::Add(const std::pair<SM::Coordinate, double> & photon){
#pragma omp critical
   {
	++fLost; //Increase lost and then decrease it later when successfully located the bin.
	for (int j = 0; j < fEMin.size(); ++j){
		if (photon.second >= fEMin[j] && photon.second < fEMax[j]){
			++fDataSkymap[photon.first][j];
			--fLost;
			break;
		}
	}
   }
}

void CountsMap::ReadFT1Fits(const std::string &fileName, const std::string & filter){
   std::string file(fileName);
   if (filter != "")
      file += "["+filter+"]";

	//Open the file
	CCfits::FITS fits(file, CCfits::Read, std::string("EVENTS"));

	//Get a reference to the table and filter it if necessary
	CCfits::ExtHDU &table = fits.extension("EVENTS");

	//Read the number of rows from the NAXIS2 keyword
	int nRows;
	table.readKey("NAXIS2", nRows);

	//Read the photon energy, l and b values
	std::valarray<double> energy(nRows), l(nRows), b(nRows);
	table.column(fEnergyCol).read(energy, 1, nRows);
	table.column("L").read(l, 1, nRows);
	table.column("B").read(b, 1, nRows);

	//Count lost photons, that fall outside the bins
	//Now we bin the data
	for (int i = 0; i < nRows; ++i){
		//Find the energy bin for the photon
		Add(std::pair<SM::Coordinate, double>(SM::Coordinate(l[i], b[i]), energy[i]));
	}
	std::cout<<"Total number of photons: "<<nRows<<std::endl;
	std::cout<<"Photons not belonging to an energy bin: "<<fLost<<std::endl;
	std::cout<<"Photons in map: "<<fDataSkymap.sum()<<std::endl;
}

void CountsMap::ReadHealpixFits(const std::string &fileName){
   fDataSkymap.load(fileName);
   //Set the arrays
   fDataSkymap.getBoundaries(fEMin, fEMax);
}
