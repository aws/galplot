
//**.****|****.****|****.****|****.****|****.****|****.****|****.****|****.****|
// * read_EGRET_data.cc *                           galprop package * 4/14/2000 
//**"****!****"****!****"****!****"****!****"****!****"****!****"****!****"****|
#include"Galplot.h"                    //AWS20050920
#include"galprop_classes.h"
#include"galprop.h"





int Galplot::read_EGRET_data()         //AWS20050920
{
 int status=0;

 double exposure_factor;

 double long_min,lat_min,d_long,d_lat;          //AWS200710

 FITS work;

 char  infile[100];



  cout<<" >>>> read_EGRET_data"<<endl;

  ///////////////////////////// counts
  
    strcpy( infile,configure.fits_directory);

    if(galplotdef.sources_EGRET==3)strcat( infile,    "counts.g1234.GSFC"   ); 
    if(galplotdef.sources_EGRET==2)strcat( infile,    "counts.g1234_30.g001"); 
    if(galplotdef.sources_EGRET==1)strcat( infile,   "counts.gp1234_30.g001"); 
    if(galplotdef.sources_EGRET==0)strcat( infile,"diffusecounts.P1234.g001"); 

    if(galplotdef.sources_EGRET==28)strcat(infile,"counts.v+0050.g028_converted");// Olaf Reimer 15 March 2006: GC Classes A+B+C  30deg to axis  AWS20060315
    if(galplotdef.sources_EGRET==29)strcat(infile,"counts.v+0050.g029_converted");// Olaf Reimer 15 March 2006  GC Class   A only 30deg to axis  AWS20060315 
   
    if(galplotdef.sources_EGRET==100)strcat(infile,"GLAST/counts_dc2_full_gal_v1.converted.fits");   // GLAST DC2 simulation official data v1       AWS20060404
    if(galplotdef.sources_EGRET==101)strcat(infile,"GLAST/counts_obssim_full_gal_v1.converted.fits");// GLAST DC2 simulation using galprop only     AWS20060406
    if(galplotdef.sources_EGRET==102)strcat(infile,"GLAST/counts_residual_rando_v1.converted.fits"); // GLAST DC2 residuals from Riccardo Rando     AWS20060427
    if(galplotdef.sources_EGRET==103)strcat(infile,"GLAST/counts_dc2_full_gal_v2.converted.fits");   // GLAST DC2 simulation official data v2       AWS20060517

    if(galplotdef.sources_EGRET==200)strcat(infile,"GLAST/counts_SC2_all_events.fits");   // GLAST SC2 simulation, >100 MeV, produced by gt2danalysis AWS20070709

    if(galplotdef.sources_EGRET==300)strcat(infile,"GLAST/counts_dc2_full_gal_v2.converted.fits");   // GLAST DC2 v2 simulation, but energy range for MILAGRO  AWS20080104

    if(galplotdef.sources_EGRET< 100)
     cout<<"  reading  EGRET counts  from file "<<infile<<endl;
    if(galplotdef.sources_EGRET>=100)
     cout<<"  reading  GLAST counts  from file "<<infile<<endl;


    work.read(infile);

    cout<<"work NAXES: "<<work.NAXES[0]<<" "<<work.NAXES[1]<<" "<<work.NAXES[2]<<endl;


    if(galplotdef.verbose==-1300) // selectable debug                AWS20070709
     work.print();


//   data.EGRET_counts.init(work.NAXES[0],work.NAXES[1],work.NAXES[2]);             AWS20031021

// standard energies + 10000-20000,20000-50000,50000-120000 MeV                     AWS20031021
   data.EGRET_counts.init(work.NAXES[0],work.NAXES[1],           13);             //AWS20031021


   

   for (int ip       =0;        ip<work.NAXES[2];       ip++)
   for (int ib       =0;        ib<work.NAXES[1];       ib++)
   for (int il       =0;        il<work.NAXES[0];       il++)
         {               
           data.EGRET_counts.d2[il][ib].s[ip]=work(il,ib,ip);  
     
	  if(galplotdef.sources_EGRET==200)  //SC2: longitude scale reversed relative to EGRET
	  data.EGRET_counts.d2[il][ib].s[ip]=work(719-il,ib,0); //AWS20070817 
     
         }

// energies >10000 MeV                                          AWS20031021

   if(galplotdef.sources_EGRET< 100)                          //EGRET only
  {
   for (int ip       =10;       ip<13;                  ip++)// AWS20031021
   {
    strcpy( infile,configure.fits_directory);

     
    if(galplotdef.sources_EGRET>=0 &&ip==10)strcat( infile,   "counts.vp0001.g006"); 
    if(galplotdef.sources_EGRET>=0 &&ip==11)strcat( infile,   "counts.vp0001.g007"); 
    if(galplotdef.sources_EGRET>=0 &&ip==12)strcat( infile,   "counts.vp0001.g008"); 


    cout<<"  reading  EGRET counts  from file "<<infile<<endl;

    work.read(infile);

   for (int ib       =0;        ib<work.NAXES[1];       ib++)
   for (int il       =0;        il<work.NAXES[0];       il++)
         {  
          data.EGRET_counts.d2[il][ib].s[ip]=work(il,ib,0); 

	  if(galplotdef.sources_EGRET==200)  //SC2: longitude scale reversed relative to EGRET
	  data.EGRET_counts.d2[il][ib].s[ip]=work(719-il,ib,0); //AWS20070817 
         }

   }//ip
  }//if

    if(galplotdef.verbose==-1301) // selectable debug                AWS20070710
      data.EGRET_counts.print();







   //////////////////////////// exposure 

    strcpy( infile,configure.fits_directory);
//  if(galplotdef.sources_EGRET>=1)strcat( infile,"exposr.gp1234_30.g001"); //AWS20060308
    if(galplotdef.sources_EGRET==3)strcat( infile,"exposr.gp1234_30.g001"); //AWS20060308
    if(galplotdef.sources_EGRET==2)strcat( infile,"exposr.gp1234_30.g001"); //AWS20060308
    if(galplotdef.sources_EGRET==1)strcat( infile,"exposr.gp1234_30.g001"); //AWS20060308
    if(galplotdef.sources_EGRET==0)strcat( infile,"exposure.P1234.g001"); 
   

    if(galplotdef.sources_EGRET==28)strcat(infile,"exposr.v+0050.g028_converted");// Olaf Reimer 15 March 2006: GC Classes A+B+C  30deg to axis  AWS20060315
    if(galplotdef.sources_EGRET==29)strcat(infile,"exposr.v+0050.g029_converted");// Olaf Reimer:15 March 2006  GC Class   A only 30deg to axis  AWS20060315 

    if(galplotdef.sources_EGRET==100)strcat(infile,"GLAST/exposr_dc2_integ_full_gal_sr_v1.converted.fits");// GLAST DC2 simulation               AWS20060404
    if(galplotdef.sources_EGRET==101)strcat(infile,"GLAST/exposr_dc2_integ_full_gal_sr_v1.converted.fits");// GLAST DC2 simulation               AWS20060406
    if(galplotdef.sources_EGRET==102)strcat(infile,"GLAST/exposr_dc2_integ_full_gal_sr_v1.converted.fits");// GLAST DC2 simulation               AWS20060427
    if(galplotdef.sources_EGRET==103)strcat(infile,"GLAST/exposr_dc2_integ_full_gal_sr_v1.converted.fits");// GLAST DC2 simulation v2 (not yet but should be almost same)AWS20060517


    if(galplotdef.sources_EGRET==200)strcat(infile,"GLAST/integr_exposure_SC2_all_events.fits");   // GLAST SC2 simulation, >100 MeV, produced by gt2danalysis AWS20070709
                                                                                                   // original has 2D, added third axis keyword NAXIS3(=NAXIS[2])=1 with fv


    if(galplotdef.sources_EGRET==300)strcat(infile,"GLAST/exposr_dc2_integ_full_gal_sr_v1.converted.fits");   // GLAST DC2 simulation, used also for Milagro

    if(galplotdef.sources_EGRET< 100)
    cout<<"  reading  EGRET exposure  from file "<<infile<<endl;
    if(galplotdef.sources_EGRET>=100)
    cout<<"  reading  GLAST exposure  from file "<<infile<<endl;

    work.read(infile);

    cout<<"work NAXES: "<<work.NAXES[0]<<" "<<work.NAXES[1]<<" "<<work.NAXES[2]<<endl;

    if(galplotdef.verbose==-1300) // selectable debug                AWS20070709
     work.print();


   data.EGRET_exposure.init(work.NAXES[0],work.NAXES[1],work.NAXES[2]);//AWS20031021
   data.EGRET_exposure.init(work.NAXES[0],work.NAXES[1],  13);

// standard energies   
   for (int ip       =0;        ip<work.NAXES[2];       ip++)
   for (int ib       =0;        ib<work.NAXES[1];       ib++)
   for (int il       =0;        il<work.NAXES[0];       il++)
         {             
          data.EGRET_exposure.d2[il][ib].s[ip]=work(il,ib,ip);

	  if(galplotdef.sources_EGRET==200)  //SC2 exposure cm2 -> cm2 sr AWS20070710
	    {
	     lat_min=-89.75; d_long=0.5; d_lat=0.5;     // this information is not in the header. the longitude scale is reversed relative to EGRET
	     //            data.EGRET_exposure.d2[il][ib].s[ip]*=sabin(lat_min,d_long,d_lat,ib);
             data.EGRET_exposure.d2[il][ib].s[ip]= work(719-il,ib,ip)* sabin(lat_min,d_long,d_lat,ib);//AWS20070817
	    }
         }

// energies >10000 MeV                                          AWS20031021

   if(galplotdef.sources_EGRET< 100)                          //EGRET only
  {
   for (int ip       =10;       ip<=12;                 ip++)// AWS20031021
   for (int ib       =0;        ib<work.NAXES[1];       ib++)
   for (int il       =0;        il<work.NAXES[0];       il++)
         {  
                                         //factors relative to 4000-10000 MeV
	   // as used in SMR2004
//           if(ip==10)exposure_factor=0.9;// 10000-20000 MeV 
//           if(ip==11)exposure_factor=0.8;// 20000-50000 MeV
//           if(ip==12)exposure_factor=0.7;//50000-120000 MeV

	   // from Olaf Reimer mail 22 Feb 2005
           if(ip==10)exposure_factor=0.808;// 10000-20000 MeV 
           if(ip==11)exposure_factor=0.512;// 20000-50000 MeV
           if(ip==12)exposure_factor=0.186;//50000-120000 MeV



           //          exposure_factor=1.0;// test case

           data.EGRET_exposure.d2[il][ib].s[ip]=work(il,ib,9)*exposure_factor; // ip=9:4000-10000 MeV 

         }//for
  }//if


    if(galplotdef.verbose==-1301) // selectable debug                AWS20070710
     data.EGRET_exposure.print();




   data.n_E_EGRET = galplotdef.energies_EGRET;//AWS20031021
   data.  E_EGRET = new double[14];           //AWS20031021

   data.E_EGRET[ 0]=    30;
   data.E_EGRET[ 1]=    50;
   data.E_EGRET[ 2]=    70;
   data.E_EGRET[ 3]=   100;
   data.E_EGRET[ 4]=   150;
   data.E_EGRET[ 5]=   300;
   data.E_EGRET[ 6]=   500;
   data.E_EGRET[ 7]=  1000;
   data.E_EGRET[ 8]=  2000;
   data.E_EGRET[ 9]=  4000;
   data.E_EGRET[10]= 10000;
   data.E_EGRET[11]= 20000;//AWS20031021
   data.E_EGRET[12]= 50000;//AWS20031021
   data.E_EGRET[13]=120000;//AWS20031021

   if(galplotdef.sources_EGRET==100 || galplotdef.sources_EGRET==101 || galplotdef.sources_EGRET==102  || galplotdef.sources_EGRET==103  ) //GLAST DC2 simulations    AWS20060517
   {
   data.E_EGRET[ 0]=    30;
   data.E_EGRET[ 1]=    60;
   data.E_EGRET[ 2]=   120;
   data.E_EGRET[ 3]=   240;
   data.E_EGRET[ 4]=   480;
   data.E_EGRET[ 5]=   960;
   data.E_EGRET[ 6]=  1920;
   data.E_EGRET[ 7]=  3840;
   data.E_EGRET[ 8]=  7680;
   data.E_EGRET[ 9]= 15360;
   data.E_EGRET[10]= 30720;
   data.E_EGRET[11]= 61440;
   data.E_EGRET[12]=122880;
   data.E_EGRET[13]=245760;
   }

   
   if(galplotdef.sources_EGRET==200)  //GLAST SC2 simulation     AWS20070709
   {
     // only one energy range from gtd2analysis 
    data.E_EGRET[ 0]=   100;
    data.E_EGRET[ 1]= 99999;//150000 in counts EBINS table: 5 digits gave problem with generating plot  
    data.E_EGRET[ 2]=   120;// dummy values
    data.E_EGRET[ 3]=   240;
    data.E_EGRET[ 4]=   480;
    data.E_EGRET[ 5]=   960;
    data.E_EGRET[ 6]=  1920;
    data.E_EGRET[ 7]=  3840;
    data.E_EGRET[ 8]=  7680;
    data.E_EGRET[ 9]= 15360;
    data.E_EGRET[10]= 30720;
    data.E_EGRET[11]= 61440;
    data.E_EGRET[12]=122880;
    data.E_EGRET[13]=245760;

    // fill the maps with duplicates to allow fitting routines to work as for EGRET and DC2 
    for (int ip       =1;        ip<=12;                 ip++)
    for (int ib       =0;        ib<work.NAXES[1];       ib++)
    for (int il       =0;        il<work.NAXES[0];       il++)
    {
     data.EGRET_exposure.d2[il][ib].s[ip] = data.EGRET_exposure.d2[il][ib].s[0];
     data.EGRET_counts  .d2[il][ib].s[ip] = data.EGRET_counts  .d2[il][ib].s[0];
    }   
}
   
   if(galplotdef.sources_EGRET==300)  //MILAGRO 15 TeV point at end    AWS20080104
   {
    data.E_EGRET[ 0]=    30;
    data.E_EGRET[ 1]=    60;                                                                
    data.E_EGRET[ 2]=   120;
    data.E_EGRET[ 3]=   240;
    data.E_EGRET[ 4]=   480;
    data.E_EGRET[ 5]=   960;
    data.E_EGRET[ 6]=  1920;
    data.E_EGRET[ 7]=  3840;
    data.E_EGRET[ 8]=  7680;
    data.E_EGRET[ 9]= 15360;
    data.E_EGRET[10]= 30720;
    data.E_EGRET[11]= 61440;
    data.E_EGRET[12]=15e6  ;//Milagro 15-16 TeV (to get TeV^-1 units)
    data.E_EGRET[13]=16e6  ;

   
}

  cout<<" <<<< read_EGRET:data"<<endl;

  
return status;
}
