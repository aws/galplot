#include"Galplot.h"                    //AWS20050920
#include"galprop_classes.h"
#include"galprop.h"
#include"constants.h"
#include"fitsio.h"  

#include"Isotropic.h"                 //AWS20081202

#include<cmath> // AWS20080630 


int Galplot::plot_convolved_GLAST_profile_healpix(int longprof, int latprof,int ip,int ic,int bremss,int pi0,int total)
{
   cout<<" >>>> plot_convolved_GLAST_profile_healpix    "<<endl;

   txt_stream<<"Profiles from plot_convolved_GLAST_profile_healpix      "<<endl;  //AWS20110826
   txt_stream<<data.GLAST_counts_healpix.getEMin()[ip]<<" -  "<<data.GLAST_counts_healpix.getEMax()[ip] <<" MeV"<<endl;//AWS20080715


   if(longprof==1)
   {
    txt_stream<<galplotdef. lat_min1<<" < b < "<<galplotdef. lat_max1<< " , ";
    txt_stream<<galplotdef. lat_min2<<" < b < "<<galplotdef. lat_max2<< endl;
   }

   if(latprof==1)
   {
    txt_stream<<galplotdef.long_min1<<" < l < "<<galplotdef.long_max1<< " , ";
    txt_stream<<galplotdef.long_min2<<" < l < "<<galplotdef.long_max2<< endl;
   }
 

   int status=0;


int i_comp,i_lat,i_long;
int ii_long,ii_lat;
int il_min,il_max,ib_min,ib_max;
int il_min1,il_max1,ib_min1,ib_max1;
int il_min2,il_max2,ib_min2,ib_max2;


double long_min;
double l,b,l_off;
double ic_total;
double ic_aniso_total;//AWS20060907
double isotropic_intensity;
Isotropic isotropic;  //AWS20081202

char name[400],canvastitle[400], workstring1[400],workstring2[400],workstring3[400],workstring4[400];
char psfile[400];


TCanvas *c1;
TH2F *        ic_map;
TH2F *  ic_aniso_map; //AWS20060907
TH2F *    bremss_map;
TH2F * pi0_decay_map;
TH2F *     total_map;
TH2F * isotropic_map;

 TH1D *profile,*profile_HI,*profile_H2,*profile_HII,*profile_IC,*profile_EB,*profile_so,*profile_to,  //AWS20080630 20081012 20090811
      *profile_sourcepop_sublimit,*profile_sourcepop_soplimit,*profile_total_with_sourcepop_sublimit, //AWS20090109
      *profile_total_with_sources_and_sourcepop_sublimit;                                             //AWS20090109

 TH1D *profile_solar_IC,*profile_solar_disk,*profile_solar_IC_disk;                                   //AWS20100216

double *profile_ic, *profile_ic_aniso, *profile_bremss, *profile_pi0_decay, *profile_total,*profile_sources,*profile_total_with_sources; //AWS20080206 for print output



 int use_errors=0; // 0=line, 1= predicted error bars


 int ncolors=0; int *colors=0;// default palette of 50 colours AWS20050915
 gStyle->SetPalette(ncolors,colors);

  //====== see HowTo Style: and do before creating canvas
 
 
   TStyle *plain  = new TStyle("Plain","Plain Style (no colors/fill areas)"); 

   
   plain->SetCanvasBorderMode(0);
   plain->SetPadBorderMode(0);
   plain->SetPadColor(0);
   plain->SetCanvasColor(0);
   plain->SetTitleColor(1,"X");//AWS20080125  was (0): changed for root 5.08
   plain->SetTitleColor(1,"Y");//AWS20080125
   plain->SetStatColor(0);
   gROOT->SetStyle("Plain");



  {
 {
 // names must be different or canvas disappears

   sprintf(name,"_healpix_%5.0f - %5.0f MeV",data.GLAST_counts_healpix.getEMin()[ip], data.GLAST_counts_healpix.getEMax()[ip] );
  strcpy(canvastitle,"galdef_");
  strcat(canvastitle,galdef.galdef_ID);
  strcat(canvastitle,name);


  

 




  // construct conventional Galactic longitude axis in two seqments, 180-0 and 340-180
  TF1 *f1=new TF1("f1","-x",   0,180); // root manual p. 149
  TF1 *f2=new TF1("f2","-x", 180,340); // root manual p. 149

  //                      xmin ymin xmax ymax, funcname   ndiv chopt gridlength
  TGaxis *axis1=new TGaxis(-180,-90 ,  0.,-90  , "f1",       9,""                );
  axis1->SetLabelSize(0.03);
  TGaxis *axis2=new TGaxis(  20,-90 ,180.,-90  , "f2",       8,""                );
  axis2->SetLabelSize(0.03);
  axis2->SetTitle("Galactic longitude");
  // fill in 0-340 between two axis segments, unlabelled
  // at present has no visibile effect but could be useful if more minor tickmarks added
  //                         xmin ymin xmax ymax   wmin wmax ndiv chopt gridlength
  TGaxis *axis3=new TGaxis(  0,  -90.  , 20., -90., 0., 20. ,0, "U"               );


  // do the plotting


  //  total_map->Draw("lego"); 
 


//==============================     Profiles  =============================

//------------------------------     axes      -------------------------

  // construct conventional Galactic longitude axis in two seqments, 180-0 and 340-180
  f1=new TF1("f1","-x",   0,180); // root manual p. 149
  f2=new TF1("f2","-x", 180,340); // root manual p. 149

  //                      xmin ymin xmax ymax, funcname   ndiv chopt gridlength
          axis1=new TGaxis(-180,0.  ,  0.,0.   , "f1",       9,""                );
  axis1->SetLabelSize(0.02);
          axis2=new TGaxis(  20,0.  ,180.,0.   , "f2",       8,""                );
  axis2->SetLabelSize(0.02);
  axis2->SetTitle("Galactic longitude");
  // fill in 0-340 between two axis segments, unlabelled
  // at present has no visibile effect but could be useful if more minor tickmarks added
  //                         xmin ymin xmax ymax   wmin wmax ndiv chopt gridlength
          axis3=new TGaxis(  0,    0.  , 20., 0.  , 0., 20. ,0, "U"               );




  // ------------------------------ healpix version ----------------------------------


   int i,j, ipix, i_E_GLAST;
   pointing point;
   double l,l_off,b,b_off;

   double pi= acos(-1.0);
   double rtd=180./pi;        // rad to deg
   double dtr=pi / 180.;      // deg to rad

   int intensity_method=1; //   1=sum(counts/exposure)(preferred)  2=(sum counts)/(sum exposure) 

   double isotropic_intensity; //AWS20080630

   if(0)                                                                   //AWS20200108 to  avoid crash, needs isotropic_type=1
   isotropic.read(configure.fits_directory,galplotdef.isotropic_bgd_file); //AWS20081202


   // source populations only available as intensity so compute counts for consistency with other components. NB this intensity is already per bin !

   
   unconvolved.GLAST_unconvolved_counts_sourcepop_sublimit   = unconvolved.GLAST_unconvolved_intensity_sourcepop_sublimit;     //AWS20090113

   /* this gives error message about incompatible skymaps and does not perform the multiplication. Hence replace by loop.

   cout<<" unconvolved.GLAST_unconvolved_counts_sourcepop_sublimit  *= data.GLAST_exposure_integrated; "<<endl;
   unconvolved.GLAST_unconvolved_counts_sourcepop_sublimit  *= data.GLAST_exposure_integrated;                                 //AWS20090113
   cout<<" unconvolved.GLAST_unconvolved_counts_sourcepop_sublimit  *= data.GLAST_exposure_integrated.solidAngle();"<<endl;    
   unconvolved.GLAST_unconvolved_counts_sourcepop_sublimit  *= data.GLAST_exposure_integrated.solidAngle();                    //AWS20090113
   */


   unconvolved.GLAST_unconvolved_counts_sourcepop_soplimit  = unconvolved.GLAST_unconvolved_intensity_sourcepop_soplimit;      //AWS20090113

   /*this gives error message about incompatible skymaps and does not perform the multiplication. Hence replace by loop.
   unconvolved.GLAST_unconvolved_counts_sourcepop_soplimit *= data.GLAST_exposure_integrated;                                  //AWS20090113
   unconvolved.GLAST_unconvolved_counts_sourcepop_soplimit *= data.GLAST_exposure_integrated.solidAngle();                     //AWS20090113


   */


   //AWS20110731 use explicit loop instead

   for (ipix=0;ipix< convolved.GLAST_convolved_counts   .Npix() ;ipix++)
    for( i_E_GLAST=0;i_E_GLAST< data.GLAST_counts_healpix.getEMin().size();i_E_GLAST++)
    {
       unconvolved.GLAST_unconvolved_counts_sourcepop_sublimit[ipix][i_E_GLAST] *=
                  (data.GLAST_exposure_integrated[ipix][i_E_GLAST]*convolved.GLAST_convolved_counts.solidAngle());

       unconvolved.GLAST_unconvolved_counts_sourcepop_soplimit[ipix][i_E_GLAST] *=
                  (data.GLAST_exposure_integrated[ipix][i_E_GLAST]*convolved.GLAST_convolved_counts.solidAngle());
    }



   // this is only without convolution, eventually need to convolve via gardian methods
     convolved.GLAST_convolved_counts_sourcepop_sublimit=  unconvolved.GLAST_unconvolved_counts_sourcepop_sublimit;            //AWS20090112
     convolved.GLAST_convolved_counts_sourcepop_soplimit=  unconvolved.GLAST_unconvolved_counts_sourcepop_soplimit;            //AWS20090112
   






     // now solar counts provided using Sun class
 
     
     convolved.GLAST_convolved_counts_solar_IC  .write("test_solar_IC_counts.fits");
     convolved.GLAST_convolved_counts_solar_disk.write("test_solar_disk_counts.fits");

  //---------------------------------------------------------------------------------------------------------------
  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  // longitude profile
  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

   if(longprof==1)
   {
 
   int ilong,nlong;
   nlong=int (360./galplotdef.long_binsize + .01) ;

   vector<valarray<double> >           counts_profile   (nlong );  // vector of valarrays to take advantage of valarray operations and vector indexing
   vector<valarray<double> >           counts_profile_HI(nlong );
   vector<valarray<double> >           counts_profile_H2(nlong );
   vector<valarray<double> >           counts_profile_HII(nlong );//AWS20090811
   vector<valarray<double> >           counts_profile_IC(nlong );
   vector<valarray<double> >           counts_profile_so(nlong ); //AWS20081210
   vector<valarray<double> >           counts_profile_to(nlong ); //AWS20081211 total including sources
   vector<valarray<double> >           counts_profile_sourcepop_sublimit(nlong); //AWS20090109
   vector<valarray<double> >           counts_profile_sourcepop_soplimit(nlong); //AWS20090109
   vector<valarray<double> >           counts_profile_solar_IC     (nlong ); //AWS20100216
   vector<valarray<double> >           counts_profile_solar_disk   (nlong ); //AWS20100216
   vector<valarray<double> >           counts_profile_solar_IC_disk(nlong ); //AWS20100216


   vector<valarray<double> >         exposure_profile(nlong );

   vector<valarray<double> >        intensity_profile   (nlong );
   vector<valarray<double> >        intensity_profile_HI(nlong );
   vector<valarray<double> >        intensity_profile_H2(nlong );
   vector<valarray<double> >        intensity_profile_HII(nlong );//AWS20090811
   vector<valarray<double> >        intensity_profile_IC(nlong );
   vector<valarray<double> >        intensity_profile_EB(nlong ); //AWS20080630
   vector<valarray<double> >        intensity_profile_so(nlong ); //AWS20081210
   vector<valarray<double> >        intensity_profile_to(nlong ); //AWS20081211 total including sources
   vector<valarray<double> >        intensity_profile_sourcepop_sublimit(nlong); //AWS20090109
   vector<valarray<double> >        intensity_profile_sourcepop_soplimit(nlong); //AWS20090109
   vector<valarray<double> >        intensity_profile_total_with_sourcepop_sublimit(nlong);//AWS20090114

   vector<valarray<double> >        intensity_profile_solar_IC     (nlong ); //AWS20100216
   vector<valarray<double> >        intensity_profile_solar_disk   (nlong ); //AWS20100216
   vector<valarray<double> >        intensity_profile_solar_IC_disk(nlong ); //AWS20100216
   
   vector<valarray<double> >  intensity_error_profile(nlong );
   vector<valarray<double> >  number_of_pixels       (nlong );

   for (ilong=0;ilong<nlong ;ilong++)
   {
             counts_profile[ilong].resize(data.GLAST_counts_healpix.getEMin().size()   );
             counts_profile[ilong]=0.;

          counts_profile_HI[ilong].resize(data.GLAST_counts_healpix.getEMin().size()   );
          counts_profile_HI[ilong]=0.;

          counts_profile_H2[ilong].resize(data.GLAST_counts_healpix.getEMin().size()   );
          counts_profile_H2[ilong]=0.;

          counts_profile_HII[ilong].resize(data.GLAST_counts_healpix.getEMin().size()   );//AWS20090811
          counts_profile_HII[ilong]=0.;                                                   //AWS20090811

          counts_profile_IC[ilong].resize(data.GLAST_counts_healpix.getEMin().size()   );
          counts_profile_IC[ilong]=0.;

          counts_profile_so[ilong].resize(data.GLAST_counts_healpix.getEMin().size()   );//AWS20081210
          counts_profile_so[ilong]=0.;                                                   //AWS20081210

          counts_profile_to[ilong].resize(data.GLAST_counts_healpix.getEMin().size()   );//AWS20081211
          counts_profile_to[ilong]=0.;                                                   //AWS20081211

          counts_profile_sourcepop_sublimit[ilong].resize(data.GLAST_counts_healpix.getEMin().size()   );//AWS20090901
          counts_profile_sourcepop_sublimit[ilong]=0.;                                                   //AWS20010901

          counts_profile_sourcepop_soplimit[ilong].resize(data.GLAST_counts_healpix.getEMin().size()   );//AWS20090901
          counts_profile_sourcepop_soplimit[ilong]=0.;                                                   //AWS20010901


          counts_profile_solar_IC          [ilong].resize(data.GLAST_counts_healpix.getEMin().size()   );//AWS20100218
          counts_profile_solar_IC          [ilong]=0.;                                                   //AWS20100218

          counts_profile_solar_disk        [ilong].resize(data.GLAST_counts_healpix.getEMin().size()   );//AWS20100218
          counts_profile_solar_disk        [ilong]=0.;                                                   //AWS20100218

          counts_profile_solar_IC_disk     [ilong].resize(data.GLAST_counts_healpix.getEMin().size()   );//AWS20100218
          counts_profile_solar_IC_disk     [ilong]=0.;                                                   //AWS20100218


           exposure_profile[ilong].resize(data.GLAST_counts_healpix.getEMin().size()   );
           exposure_profile[ilong]=0.;

          intensity_profile[ilong].resize(data.GLAST_counts_healpix.getEMin().size()   );
          intensity_profile[ilong]=0.;

          intensity_profile_HI[ilong].resize(data.GLAST_counts_healpix.getEMin().size()   );
          intensity_profile_HI[ilong]=0.;

          intensity_profile_H2[ilong].resize(data.GLAST_counts_healpix.getEMin().size()   );
          intensity_profile_H2[ilong]=0.;

          intensity_profile_HII[ilong].resize(data.GLAST_counts_healpix.getEMin().size()   );//AWS20090811
          intensity_profile_HII[ilong]=0.;                                                   //AWS20090811

          intensity_profile_IC[ilong].resize(data.GLAST_counts_healpix.getEMin().size()   );
          intensity_profile_IC[ilong]=0.;

          intensity_profile_EB[ilong].resize(data.GLAST_counts_healpix.getEMin().size()   );//AWS20080630
          intensity_profile_EB[ilong]=0.;                                                   //AWS20080630

          intensity_profile_so[ilong].resize(data.GLAST_counts_healpix.getEMin().size()   );//AWS20081210
          intensity_profile_so[ilong]=0.;                                                   //AWS20081210

          intensity_profile_to[ilong].resize(data.GLAST_counts_healpix.getEMin().size()   );//AWS20081211
          intensity_profile_to[ilong]=0.;                                                   //AWS20081211


          intensity_profile_sourcepop_sublimit[ilong].resize(data.GLAST_counts_healpix.getEMin().size()   );//AWS20090901
          intensity_profile_sourcepop_sublimit[ilong]=0.;                                                   //AWS20010901

          intensity_profile_sourcepop_soplimit[ilong].resize(data.GLAST_counts_healpix.getEMin().size()   );//AWS20090901
          intensity_profile_sourcepop_soplimit[ilong]=0.;                                                   //AWS20010901


          intensity_profile_total_with_sourcepop_sublimit[ilong].resize(data.GLAST_counts_healpix.getEMin().size()   );//AWS20090914
          intensity_profile_total_with_sourcepop_sublimit[ilong]=0.;                                                   //AWS20010914

          intensity_profile_solar_IC                     [ilong].resize(data.GLAST_counts_healpix.getEMin().size()   );//AWS20100216
          intensity_profile_solar_IC                     [ilong]=0.;                                                   //AWS20100216

          intensity_profile_solar_disk                   [ilong].resize(data.GLAST_counts_healpix.getEMin().size()   );//AWS20100216
          intensity_profile_solar_disk                   [ilong]=0.;                                                   //AWS20100216

          intensity_profile_solar_IC_disk                [ilong].resize(data.GLAST_counts_healpix.getEMin().size()   );//AWS20100216
          intensity_profile_solar_IC_disk                [ilong]=0.;                                                   //AWS20100216



          intensity_error_profile[ilong].resize(data.GLAST_counts_healpix.getEMin().size()   );
          intensity_error_profile[ilong]=0.;

              number_of_pixels[ilong].resize(data.GLAST_counts_healpix.getEMin().size()   );
              number_of_pixels[ilong]=0.;
   }

   for (ipix=0;ipix< convolved.GLAST_convolved_counts   .Npix() ;ipix++)
   {
    l =        convolved.GLAST_convolved_counts.pix2ang(ipix).phi   * rtd;
    b = 90.0 - convolved.GLAST_convolved_counts.pix2ang(ipix).theta * rtd;

    if(galplotdef.verbose==-2202)    cout<<"ipix="<<ipix<<" l = "<<l<<" b ="<<b      ; // l,b in deg

    SM::Coordinate coordinate=convolved.GLAST_convolved_counts.pix2coord( ipix);
    //    if(galplotdef.verbose==-2202)      {cout<<"  pixel coordinate from pix2coord:";coordinate.print(cout);cout<<endl;} AWS20091204


    int select=0;
    if(b>=galplotdef.lat_min1 && b<=galplotdef.lat_max1) select=1;
    if(b>=galplotdef.lat_min2 && b<=galplotdef.lat_max2) select=1;

    if (select==1)
    {
      //  cout<<"selected ipix="<<ipix<<" l = "<<l<<" b ="<<b<<endl; // l,b in deg
      l_off= l-180.0; // l:0-360, offset l_off relative to l=180 (since plot goes from 180...0..-180)
      if(l_off<  0.0)l_off+=360.;// put offset in range 0-360
      if(l_off>360.0)l_off-=360.;
      ilong= nlong-1 - int(l_off / galplotdef.long_binsize + .01);// reverse scale for plot, goes from nlong-1 to 0
      //     ilong=  int(l_off / galplotdef.long_binsize + .01);// no reverse scale for plot (test)

      if(galplotdef.verbose==-1901)cout<<"plot_GLAST_profile_healpix: l_off="<<l_off<<" ilong="<<ilong<<endl;
      if(ilong<0||ilong>nlong)cout<<"plot_GLAST_profile_healpix:error: ilong out of range:"<<ilong<<endl;

     if(ilong>=0 && ilong<nlong)
     for( i_E_GLAST=0;i_E_GLAST< data.GLAST_counts_healpix.getEMin().size();i_E_GLAST++)
     {

       if(galplotdef.convolve_GLAST==0) //AWS20080718
       {
          counts_profile    [ilong][i_E_GLAST] += unconvolved.GLAST_unconvolved_counts   [ipix][i_E_GLAST];
          counts_profile_HI [ilong][i_E_GLAST] += unconvolved.GLAST_unconvolved_counts_HI[ipix][i_E_GLAST];
          counts_profile_H2 [ilong][i_E_GLAST] += unconvolved.GLAST_unconvolved_counts_H2[ipix][i_E_GLAST];
	  counts_profile_HII[ilong][i_E_GLAST] += unconvolved.GLAST_unconvolved_counts_HII[ipix][i_E_GLAST];//AWS20090811
          counts_profile_IC [ilong][i_E_GLAST] += unconvolved.GLAST_unconvolved_counts_IC[ipix][i_E_GLAST];
          counts_profile_so [ilong][i_E_GLAST] += unconvolved.GLAST_unconvolved_counts_sources[ipix][i_E_GLAST];//AWS20081211

          counts_profile_sourcepop_sublimit [ilong][i_E_GLAST] += unconvolved.GLAST_unconvolved_counts_sourcepop_sublimit[ipix][i_E_GLAST]; //AWS20090112
          counts_profile_sourcepop_soplimit [ilong][i_E_GLAST] += unconvolved.GLAST_unconvolved_counts_sourcepop_soplimit[ipix][i_E_GLAST]; //AWS20090112

          counts_profile_solar_IC           [ilong][i_E_GLAST] += unconvolved.GLAST_unconvolved_counts_solar_IC          [ipix][i_E_GLAST]; //AWS20100216
          counts_profile_solar_disk         [ilong][i_E_GLAST] += unconvolved.GLAST_unconvolved_counts_solar_disk        [ipix][i_E_GLAST]; //AWS20100216

       	   exposure_profile [ilong][i_E_GLAST] +=                          data.GLAST_exposure_integrated[ipix][i_E_GLAST]*convolved.GLAST_convolved_counts.solidAngle(); 

        intensity_profile   [ilong][i_E_GLAST] += unconvolved.GLAST_unconvolved_counts[ipix][i_E_GLAST]/
                                                                          (data.GLAST_exposure_integrated[ipix][i_E_GLAST]*convolved.GLAST_convolved_counts.solidAngle());

        intensity_profile_HI[ilong][i_E_GLAST] += unconvolved.GLAST_unconvolved_counts_HI[ipix][i_E_GLAST]/
                                                                          (data.GLAST_exposure_integrated[ipix][i_E_GLAST]*convolved.GLAST_convolved_counts.solidAngle());
        intensity_profile_H2[ilong][i_E_GLAST] += unconvolved.GLAST_unconvolved_counts_H2[ipix][i_E_GLAST]/
                                                                          (data.GLAST_exposure_integrated[ipix][i_E_GLAST]*convolved.GLAST_convolved_counts.solidAngle());
        intensity_profile_HII[ilong][i_E_GLAST] += unconvolved.GLAST_unconvolved_counts_HII[ipix][i_E_GLAST]/
	                                                                  (data.GLAST_exposure_integrated[ipix][i_E_GLAST]*convolved.GLAST_convolved_counts.solidAngle());//AWS20090811


        intensity_profile_IC[ilong][i_E_GLAST] += unconvolved.GLAST_unconvolved_counts_IC[ipix][i_E_GLAST]/
                                                                          (data.GLAST_exposure_integrated[ipix][i_E_GLAST]*convolved.GLAST_convolved_counts.solidAngle());
        intensity_profile_so[ilong][i_E_GLAST] += unconvolved.GLAST_unconvolved_counts_sources[ipix][i_E_GLAST]/
                                                                          (data.GLAST_exposure_integrated[ipix][i_E_GLAST]*convolved.GLAST_convolved_counts.solidAngle());//AWS20081211

        intensity_profile_sourcepop_sublimit[ilong][i_E_GLAST] += unconvolved.GLAST_unconvolved_counts_sourcepop_sublimit[ipix][i_E_GLAST]/
                                                                          (data.GLAST_exposure_integrated[ipix][i_E_GLAST]*convolved.GLAST_convolved_counts.solidAngle());//AWS20090112
        intensity_profile_sourcepop_soplimit[ilong][i_E_GLAST] += unconvolved.GLAST_unconvolved_counts_sourcepop_soplimit[ipix][i_E_GLAST]/
                                                                          (data.GLAST_exposure_integrated[ipix][i_E_GLAST]*convolved.GLAST_convolved_counts.solidAngle());//AWS20090112


        intensity_profile_solar_IC          [ilong][i_E_GLAST] += unconvolved.GLAST_unconvolved_counts_solar_IC          [ipix][i_E_GLAST]/
                                                                          (data.GLAST_exposure_integrated[ipix][i_E_GLAST]*convolved.GLAST_convolved_counts.solidAngle());//AWS2100216

        intensity_profile_solar_disk        [ilong][i_E_GLAST] += unconvolved.GLAST_unconvolved_counts_solar_disk        [ipix][i_E_GLAST]/
                                                                          (data.GLAST_exposure_integrated[ipix][i_E_GLAST]*convolved.GLAST_convolved_counts.solidAngle());//AWS2100216                                                                     

     }


       /////////////////////////////////////////////////////////////////////////


       if(galplotdef.convolve_GLAST==1) //AWS20080718
       {
          counts_profile    [ilong][i_E_GLAST] +=   convolved.GLAST_convolved_counts   [ipix][i_E_GLAST];
          counts_profile_HI [ilong][i_E_GLAST] +=   convolved.GLAST_convolved_counts_HI[ipix][i_E_GLAST];
          counts_profile_H2 [ilong][i_E_GLAST] +=   convolved.GLAST_convolved_counts_H2[ipix][i_E_GLAST];
          counts_profile_HII[ilong][i_E_GLAST] +=   convolved.GLAST_convolved_counts_HII[ipix][i_E_GLAST];//AWS20090811
          counts_profile_IC [ilong][i_E_GLAST] +=   convolved.GLAST_convolved_counts_IC[ipix][i_E_GLAST];
          counts_profile_so [ilong][i_E_GLAST] +=   convolved.GLAST_convolved_counts_sources[ipix][i_E_GLAST];//AWS20081210

          counts_profile_sourcepop_sublimit [ilong][i_E_GLAST] +=   convolved.GLAST_convolved_counts_sourcepop_sublimit[ipix][i_E_GLAST]; //AWS20090112
          counts_profile_sourcepop_soplimit [ilong][i_E_GLAST] +=   convolved.GLAST_convolved_counts_sourcepop_soplimit[ipix][i_E_GLAST]; //AWS20090112

          counts_profile_solar_IC           [ilong][i_E_GLAST] +=   convolved.GLAST_convolved_counts_solar_IC          [ipix][i_E_GLAST]; //AWS20100216
          counts_profile_solar_disk         [ilong][i_E_GLAST] +=   convolved.GLAST_convolved_counts_solar_disk        [ipix][i_E_GLAST]; //AWS20100216


       	   exposure_profile [ilong][i_E_GLAST] +=                          data.GLAST_exposure_integrated[ipix][i_E_GLAST]*convolved.GLAST_convolved_counts.solidAngle(); 

        intensity_profile   [ilong][i_E_GLAST] +=   convolved.GLAST_convolved_counts[ipix][i_E_GLAST]/
                                                                          (data.GLAST_exposure_integrated[ipix][i_E_GLAST]*convolved.GLAST_convolved_counts.solidAngle());

        intensity_profile_HI[ilong][i_E_GLAST] +=   convolved.GLAST_convolved_counts_HI[ipix][i_E_GLAST]/
                                                                          (data.GLAST_exposure_integrated[ipix][i_E_GLAST]*convolved.GLAST_convolved_counts.solidAngle());
        intensity_profile_H2[ilong][i_E_GLAST] +=   convolved.GLAST_convolved_counts_H2[ipix][i_E_GLAST]/
                                                                          (data.GLAST_exposure_integrated[ipix][i_E_GLAST]*convolved.GLAST_convolved_counts.solidAngle());
        intensity_profile_HII[ilong][i_E_GLAST]+=   convolved.GLAST_convolved_counts_HII[ipix][i_E_GLAST]/
	  (data.GLAST_exposure_integrated[ipix][i_E_GLAST]*convolved.GLAST_convolved_counts.solidAngle());//AWS20090811

        intensity_profile_IC[ilong][i_E_GLAST] +=   convolved.GLAST_convolved_counts_IC[ipix][i_E_GLAST]/
                                                                          (data.GLAST_exposure_integrated[ipix][i_E_GLAST]*convolved.GLAST_convolved_counts.solidAngle());
        intensity_profile_so[ilong][i_E_GLAST] +=   convolved.GLAST_convolved_counts_sources[ipix][i_E_GLAST]/
	                                                                  (data.GLAST_exposure_integrated[ipix][i_E_GLAST]*convolved.GLAST_convolved_counts.solidAngle());//AWS20081210
       intensity_profile_sourcepop_sublimit[ilong][i_E_GLAST] +=   convolved.GLAST_convolved_counts_sourcepop_sublimit[ipix][i_E_GLAST]/
                                                                          (data.GLAST_exposure_integrated[ipix][i_E_GLAST]*convolved.GLAST_convolved_counts.solidAngle());//AWS20090112
        intensity_profile_sourcepop_soplimit[ilong][i_E_GLAST] +=   convolved.GLAST_convolved_counts_sourcepop_soplimit[ipix][i_E_GLAST]/
                                                                          (data.GLAST_exposure_integrated[ipix][i_E_GLAST]*convolved.GLAST_convolved_counts.solidAngle());//AWS20090112
       }

        intensity_profile_solar_IC          [ilong][i_E_GLAST] +=   convolved.GLAST_convolved_counts_solar_IC          [ipix][i_E_GLAST]/
                                                                          (data.GLAST_exposure_integrated[ipix][i_E_GLAST]*convolved.GLAST_convolved_counts.solidAngle());//AWS2100216

        intensity_profile_solar_disk        [ilong][i_E_GLAST] +=   convolved.GLAST_convolved_counts_solar_disk        [ipix][i_E_GLAST]/
                                                                          (data.GLAST_exposure_integrated[ipix][i_E_GLAST]*convolved.GLAST_convolved_counts.solidAngle());//AWS2100216 


	number_of_pixels [ilong][i_E_GLAST] ++;

     } //i_E_GLAST
    } // if select

   } // ipix


   /////////////////////////////////////////////////////////////////////////


  for (ilong=0;ilong<nlong ;ilong++)
  { 
   for( i_E_GLAST=0;i_E_GLAST< data.GLAST_counts_healpix.getEMin().size();i_E_GLAST++)
   {

    isotropic_intensity=0.0;//AWS20080630

   if(galplotdef.isotropic_type==1) // integrate power law over GLAST energy bin
    isotropic_intensity=galplotdef.isotropic_const
                        *(  pow(data.GLAST_counts_healpix.getEMax()[i_E_GLAST], 1.0-galplotdef.isotropic_g)
                           -pow(data.GLAST_counts_healpix.getEMin()[i_E_GLAST], 1.0-galplotdef.isotropic_g)  )
                        /                                                      (1.0-galplotdef.isotropic_g);

    if(galplotdef.isotropic_type==2) // integrate tabulated spectrum  over GLAST energy bin
      isotropic_intensity= isotropic.integrated(data.GLAST_counts_healpix.getEMin()[i_E_GLAST],data.GLAST_counts_healpix.getEMax()[i_E_GLAST], 1);



   //   cout<<"    isotropic_intensity="<<isotropic_intensity<<endl;



    if(exposure_profile[ilong][i_E_GLAST]>0.)
    {


      if(intensity_method==1)
      {
      //   average intensity over bin, error estimate from total counts
             intensity_profile  [ilong][i_E_GLAST]/= number_of_pixels [ilong][i_E_GLAST];
       intensity_error_profile  [ilong][i_E_GLAST]=  intensity_profile[ilong][i_E_GLAST]/ sqrt(counts_profile[ilong][i_E_GLAST]);
            intensity_profile_HI[ilong][i_E_GLAST]/= number_of_pixels [ilong][i_E_GLAST];
            intensity_profile_H2[ilong][i_E_GLAST]/= number_of_pixels [ilong][i_E_GLAST];
            intensity_profile_HII[ilong][i_E_GLAST]/=number_of_pixels [ilong][i_E_GLAST];//AWS20090811
            intensity_profile_IC[ilong][i_E_GLAST]/= number_of_pixels [ilong][i_E_GLAST];
            intensity_profile_so[ilong][i_E_GLAST]/= number_of_pixels [ilong][i_E_GLAST];//AWS20081210
            intensity_profile_sourcepop_sublimit
                                [ilong][i_E_GLAST]/= number_of_pixels [ilong][i_E_GLAST];//AWS20090112
            intensity_profile_sourcepop_soplimit
                                [ilong][i_E_GLAST]/= number_of_pixels [ilong][i_E_GLAST];//AWS20090112

            intensity_profile_solar_IC               
                                [ilong][i_E_GLAST]/= number_of_pixels [ilong][i_E_GLAST];//AWS20100216
            intensity_profile_solar_disk         
                                [ilong][i_E_GLAST]/= number_of_pixels [ilong][i_E_GLAST];//AWS20100216
      }

      if(intensity_method==2)
      {
      //  method ignoring variations of intensity and exposure
             intensity_profile   [ilong][i_E_GLAST]=      counts_profile   [ilong][i_E_GLAST] /exposure_profile[ilong][i_E_GLAST] ;
       intensity_error_profile   [ilong][i_E_GLAST]= sqrt(counts_profile   [ilong][i_E_GLAST])/exposure_profile[ilong][i_E_GLAST] ;
             intensity_profile_HI[ilong][i_E_GLAST]=      counts_profile_HI[ilong][i_E_GLAST] /exposure_profile[ilong][i_E_GLAST] ;
             intensity_profile_H2[ilong][i_E_GLAST]=      counts_profile_H2[ilong][i_E_GLAST] /exposure_profile[ilong][i_E_GLAST] ;
             intensity_profile_HII[ilong][i_E_GLAST]=     counts_profile_HII[ilong][i_E_GLAST]/exposure_profile[ilong][i_E_GLAST] ;//AWS20090811
             intensity_profile_IC[ilong][i_E_GLAST]=      counts_profile_IC[ilong][i_E_GLAST] /exposure_profile[ilong][i_E_GLAST] ;
             intensity_profile_so[ilong][i_E_GLAST]=      counts_profile_so[ilong][i_E_GLAST] /exposure_profile[ilong][i_E_GLAST] ;
             intensity_profile_sourcepop_sublimit
                                 [ilong][i_E_GLAST]=      counts_profile_sourcepop_sublimit
                                                                           [ilong][i_E_GLAST] /exposure_profile[ilong][i_E_GLAST] ;//AWS20090112
             intensity_profile_sourcepop_soplimit
                                 [ilong][i_E_GLAST]=      counts_profile_sourcepop_soplimit
                                                                           [ilong][i_E_GLAST] /exposure_profile[ilong][i_E_GLAST] ;//AWS20090112

             intensity_profile_solar_IC             
                                 [ilong][i_E_GLAST]=      counts_profile_solar_IC              
                                                                           [ilong][i_E_GLAST] /exposure_profile[ilong][i_E_GLAST] ;//AWS20100216
             intensity_profile_solar_disk          
                                 [ilong][i_E_GLAST]=      counts_profile_solar_disk          
                                                                           [ilong][i_E_GLAST] /exposure_profile[ilong][i_E_GLAST] ;//AWS20100216

      } 

    }// if exposure

      intensity_profile_EB[ilong][i_E_GLAST]  = isotropic_intensity; //AWS20080630
      intensity_profile   [ilong][i_E_GLAST] += isotropic_intensity; //AWS20080630

   }// i_E_GLAST



  }// ilong


   // total including sources
   // total including source population below threshold
   for (ilong=0;ilong<nlong  ;ilong++)
   {
       counts_profile_to[ilong] =    counts_profile[ilong] +    counts_profile_so[ilong];
    intensity_profile_to[ilong] = intensity_profile[ilong] + intensity_profile_so[ilong];
    intensity_profile_total_with_sourcepop_sublimit[ilong] = intensity_profile_to[ilong] + intensity_profile_sourcepop_sublimit[ilong]; //AWS20100812 was missing _to
   }




   //  if(galplotdef.verbose==-2201)
   for (ilong=0;ilong<nlong ;ilong++)
     {
       cout<<"counts longitude profile ilong= "<<ilong<<" l="<<ilong* galplotdef.long_binsize<<"-"<<( ilong+1)* galplotdef.long_binsize <<" : ";
       for( i_E_GLAST=0;i_E_GLAST< data.GLAST_counts_healpix.getEMin().size();i_E_GLAST++)
	 cout<<  counts_profile[ilong][i_E_GLAST]<< " "      ;
       cout<<endl;

       cout<<"  intensity profile:";
       for( i_E_GLAST=0;i_E_GLAST< data.GLAST_counts_healpix.getEMin().size();i_E_GLAST++)
	 cout<<  intensity_profile[ilong][i_E_GLAST]<< " "      ;
       cout<<endl;

     cout<<"  intensity profile EB:";
       for( i_E_GLAST=0;i_E_GLAST< data.GLAST_counts_healpix.getEMin().size();i_E_GLAST++)
	 cout<<  intensity_profile_EB[ilong][i_E_GLAST]<< " "      ;
       cout<<endl;

    cout<<"  intensity profile sources:";
       for( i_E_GLAST=0;i_E_GLAST< data.GLAST_counts_healpix.getEMin().size();i_E_GLAST++)
	 cout<<  intensity_profile_so[ilong][i_E_GLAST]<< " "      ;
       cout<<endl;


    cout<<"  intensity profile total including sources:";
       for( i_E_GLAST=0;i_E_GLAST< data.GLAST_counts_healpix.getEMin().size();i_E_GLAST++)
	 cout<<  intensity_profile_to[ilong][i_E_GLAST]<< " "      ;
       cout<<endl;

       cout<<"  intensity error profile:";
       for( i_E_GLAST=0;i_E_GLAST< data.GLAST_counts_healpix.getEMin().size();i_E_GLAST++)
	 cout<<  intensity_error_profile[ilong][i_E_GLAST]<< " "      ;
       cout<<endl;

       cout<<"counts profile sourcepop sublimit:";
       for( i_E_GLAST=0;i_E_GLAST< data.GLAST_counts_healpix.getEMin().size();i_E_GLAST++)
	 cout<<  counts_profile_sourcepop_sublimit[ilong][i_E_GLAST]<< " "      ;
       cout<<endl;

     cout<<"intensity profile sourcepop sublimit:";
       for( i_E_GLAST=0;i_E_GLAST< data.GLAST_counts_healpix.getEMin().size();i_E_GLAST++)
	 cout<<  intensity_profile_sourcepop_sublimit[ilong][i_E_GLAST]<< " "      ;
       cout<<endl;

       cout<<"counts profile sourcepop soplimit:";
       for( i_E_GLAST=0;i_E_GLAST< data.GLAST_counts_healpix.getEMin().size();i_E_GLAST++)
	 cout<<  counts_profile_sourcepop_soplimit[ilong][i_E_GLAST]<< " "      ;
       cout<<endl;

       cout<<"intensity profile sourcepop soplimit:";
       for( i_E_GLAST=0;i_E_GLAST< data.GLAST_counts_healpix.getEMin().size();i_E_GLAST++)
	 cout<<  intensity_profile_sourcepop_soplimit[ilong][i_E_GLAST]<< " "      ;
       cout<<endl;

      cout<<"intensity profile total with sourcepop sublimit:";
       for( i_E_GLAST=0;i_E_GLAST< data.GLAST_counts_healpix.getEMin().size();i_E_GLAST++)
	 cout<<  intensity_profile_total_with_sourcepop_sublimit[ilong][i_E_GLAST]<< " "      ;
       cout<<endl;

      cout<<"intensity profile solar IC:                     ";
       for( i_E_GLAST=0;i_E_GLAST< data.GLAST_counts_healpix.getEMin().size();i_E_GLAST++)
	 cout<<  intensity_profile_solar_IC                     [ilong][i_E_GLAST]<< " "      ;
       cout<<endl;

      cout<<"intensity profile solar disk:                   ";
       for( i_E_GLAST=0;i_E_GLAST< data.GLAST_counts_healpix.getEMin().size();i_E_GLAST++)
	 cout<<  intensity_profile_solar_disk                   [ilong][i_E_GLAST]<< " "      ;
       cout<<endl;

   }

   ///////////////////////////////

    i_E_GLAST=ip;

   if(FALSE)//AWS20110826
   {

    txt_stream<<endl<<"-------------------------------------------------"<<endl;
 
    for (ilong=0;ilong<nlong ;ilong++)
    {
        double   l =180.0-(ilong+0.5)*galplotdef.long_binsize;
        if(l<0.0)l+=360.0; // put in 0<l<360 

        txt_stream
            <<" model longitude profile: l = "<<l 
            <<"  b = "<<galplotdef.lat_min1<<" - "<<galplotdef.lat_max1
            <<", "    <<galplotdef.lat_min2<<" - "<<galplotdef.lat_max2
            <<"  E= "<< data.GLAST_counts_healpix.getEMin()[i_E_GLAST]<< " - "
                     << data.GLAST_counts_healpix.getEMax()[i_E_GLAST]<< " MeV "
            <<" counts = "         <<     counts_profile    [ilong][i_E_GLAST] 
            <<" total intensity = "<<  intensity_profile    [ilong][i_E_GLAST] 
            <<" HI =  "            <<  intensity_profile_HI [ilong][i_E_GLAST]
            <<" H2 =  "            <<  intensity_profile_H2 [ilong][i_E_GLAST]
            <<" HII = "            <<  intensity_profile_HII[ilong][i_E_GLAST]
            <<" IC =  "            <<  intensity_profile_IC [ilong][i_E_GLAST]
            <<" sources = "        <<  intensity_profile_so [ilong][i_E_GLAST]
            <<" sourcepop sublimit = "<<  intensity_profile_sourcepop_sublimit[ilong][i_E_GLAST]
            <<" sourcepop soplimit = "<<  intensity_profile_sourcepop_soplimit[ilong][i_E_GLAST]
            <<" EB = "             <<  intensity_profile_EB [ilong][i_E_GLAST]
            <<" cm-2 sr-1 s-1"
            <<endl;

  
       
      
     }//ilong

   }//if(FALSE)
   

    //////////////////////////////////////////////////////////////////////


   // generate the root objects
     
   profile   =new TH1D("GLAST convolved longitude profile healpix   ","GLAST convolved longitude  profile healpix",nlong,        -180.,180.);
   profile_HI=new TH1D("GLAST convolved longitude profile healpix HI","GLAST convolved longitude  profile healpix",nlong,        -180.,180.);
   profile_H2=new TH1D("GLAST convolved longitude profile healpix H2","GLAST convolved longitude  profile healpix",nlong,        -180.,180.);
   profile_HII=new TH1D("GLAST convolved longitude profile healpix HII","GLAST convolved longitude  profile healpix",nlong,        -180.,180.);//AWS20090811
   profile_IC=new TH1D("GLAST convolved longitude profile healpix IC","GLAST convolved longitude  profile healpix",nlong,        -180.,180.);
   profile_EB=new TH1D("GLAST convolved longitude profile healpix EB","GLAST convolved longitude  profile healpix",nlong,        -180.,180.); //AWS20080630
   profile_so=new TH1D("GLAST convolved longitude profile healpix so","GLAST convolved longitude  profile healpix",nlong,        -180.,180.); //AWS20081210
   profile_to=new TH1D("GLAST convolved longitude profile healpix to","GLAST convolved longitude  profile healpix",nlong,        -180.,180.); //AWS20081211
   profile_sourcepop_sublimit
             =new TH1D("GLAST convolved longitude profile healpix sourcepop sublimit","GLAST convolved longitude  profile healpix",nlong,         -180.,180.); //AWS20090112
   profile_sourcepop_soplimit
             =new TH1D("GLAST convolved longitude profile healpix sourcepop soplimit","GLAST convolved longitude  profile healpix",nlong,         -180.,180.); //AWS20090112
   profile_total_with_sourcepop_sublimit
             =new TH1D("GLAST convolved latitude profile healpix total with sourcepop soplimit","GLAST convolved latitude  profile healpix",nlong,-180.,180.); //AWS20090114

   profile_solar_IC            
             =new TH1D("GLAST convolved longitude profile healpix solar IC          ","GLAST convolved longitude  profile healpix",nlong,-180.,180.); //AWS20100216
   profile_solar_disk                     
             =new TH1D("GLAST convolved longitude profile healpix solar disk         ","GLAST convolved longitude  profile healpix",nlong,-180.,180.); //AWS20100216
   profile_solar_IC_disk                     
             =new TH1D("GLAST convolved longitude profile healpix solar IC disk      ","GLAST convolved longitude  profile healpix",nlong,-180.,180.); //AWS20100216

   int i_long,ii_long;
   i_E_GLAST=ip;

  for(i_long=0;i_long<profile->GetNbinsX();i_long++)
  {
     ii_long=i_long+1; // TH1D bins start at 1
     profile   ->SetBinContent(ii_long      , intensity_profile         [i_long][i_E_GLAST] );
     profile_HI->SetBinContent(ii_long      , intensity_profile_HI      [i_long][i_E_GLAST] );
     profile_H2->SetBinContent(ii_long      , intensity_profile_H2      [i_long][i_E_GLAST] );
     profile_HII->SetBinContent(ii_long     , intensity_profile_HII     [i_long][i_E_GLAST] ); //AWS20090811
     profile_IC->SetBinContent(ii_long      , intensity_profile_IC      [i_long][i_E_GLAST] );
     profile_EB->SetBinContent(ii_long      , intensity_profile_EB      [i_long][i_E_GLAST] ); //AWS20080630
     profile_so->SetBinContent(ii_long      , intensity_profile_so      [i_long][i_E_GLAST] ); //AWS20081210
     profile_to->SetBinContent(ii_long      , intensity_profile_to      [i_long][i_E_GLAST] ); //AWS20081211
     profile_sourcepop_sublimit
               ->SetBinContent(ii_long      , intensity_profile_sourcepop_sublimit
                                                                        [i_long][i_E_GLAST] );//AWS20090112
    profile_sourcepop_soplimit
               ->SetBinContent(ii_long      , intensity_profile_sourcepop_soplimit
                                                                        [i_long][i_E_GLAST] );//AWS20090112
    profile_total_with_sourcepop_sublimit
              ->SetBinContent(ii_long      , intensity_profile_total_with_sourcepop_sublimit
                                                                       [i_long][i_E_GLAST] ); //AWS20090114
    profile_solar_IC               
               ->SetBinContent(ii_long      , intensity_profile_solar_IC             
                                                                        [i_long][i_E_GLAST] );//AWS20100216
    profile_solar_disk               
               ->SetBinContent(ii_long      , intensity_profile_solar_disk           
                                                                        [i_long][i_E_GLAST] );//AWS20100216

     // this causes errors to be plotted, so make it an option
     if(use_errors==1)
     profile->SetBinError  (ii_long      , intensity_error_profile[i_long][i_E_GLAST] );

      if(galplotdef.verbose==-1901)
      cout<<"GLAST longitude profile: intensity = "<< profile->GetBinContent(ii_long)<<" error="<< profile->GetBinError(ii_long)<<endl;
  }

  }// longprof


 //---------------------------------------------------------------------------------------------------------------


 ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 // latitude profile
 ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

   if(latprof==1)
   {
 
   int ilong,nlong;
   int ilat, nlat;

   nlat = int (180./galplotdef. lat_binsize + .01) ;
  

   vector<valarray<double> >           counts_profile(nlat  );  // vector of valarrays to take advantage of valarray operations and vector indexing
   vector<valarray<double> >        counts_profile_HI(nlat);
   vector<valarray<double> >        counts_profile_H2(nlat );
   vector<valarray<double> >        counts_profile_HII(nlat); //AWS20090811
   vector<valarray<double> >        counts_profile_IC(nlat );
   vector<valarray<double> >        counts_profile_so(nlat ); //AWS20081210
   vector<valarray<double> >        counts_profile_to(nlat ); //AWS20081211 total including sources
   vector<valarray<double> >        counts_profile_sourcepop_sublimit(nlat); //AWS20090114
   vector<valarray<double> >        counts_profile_sourcepop_soplimit(nlat); //AWS20090114
   vector<valarray<double> >        counts_profile_solar_IC          (nlat); //AWS20100218
   vector<valarray<double> >        counts_profile_solar_disk        (nlat); //AWS20100218
   vector<valarray<double> >        counts_profile_solar_IC_disk     (nlat); //AWS20100218

   vector<valarray<double> >         exposure_profile(nlat );
   vector<valarray<double> >        intensity_profile(nlat );
   vector<valarray<double> >     intensity_profile_HI(nlat );
   vector<valarray<double> >     intensity_profile_H2(nlat );
   vector<valarray<double> >     intensity_profile_HII(nlat );//AWS20090811
   vector<valarray<double> >     intensity_profile_IC(nlat );
   vector<valarray<double> >     intensity_profile_EB(nlat ); //AWS20080630
   vector<valarray<double> >     intensity_profile_so(nlat ); //AWS20081210
   vector<valarray<double> >     intensity_profile_to(nlat ); //AWS20081211 total including sources
   vector<valarray<double> >     intensity_profile_sourcepop_sublimit(nlat); //AWS20090114
   vector<valarray<double> >     intensity_profile_sourcepop_soplimit(nlat); //AWS20090114
   vector<valarray<double> >     intensity_profile_total_with_sourcepop_sublimit(nlat);//AWS20090114

   vector<valarray<double> >     intensity_profile_solar_IC     (nlat); //AWS20100218
   vector<valarray<double> >     intensity_profile_solar_disk   (nlat); //AWS20100218
   vector<valarray<double> >     intensity_profile_solar_IC_disk(nlat); //AWS20100218

   vector<valarray<double> >  intensity_error_profile(nlat  );
   vector<valarray<double> >  number_of_pixels       (nlat  );

   for (ilong=0;ilong<nlat  ;ilong++)
   {
             counts_profile[ilong].resize(data.GLAST_counts_healpix.getEMin().size()   );
             counts_profile[ilong]=0.;

          counts_profile_HI[ilong].resize(data.GLAST_counts_healpix.getEMin().size()   );
          counts_profile_HI[ilong]=0.;

          counts_profile_H2[ilong].resize(data.GLAST_counts_healpix.getEMin().size()   );
          counts_profile_H2[ilong]=0.;

          counts_profile_HII[ilong].resize(data.GLAST_counts_healpix.getEMin().size()   );//AWS20090811
          counts_profile_HII[ilong]=0.;                                                   //AWS20090811

          counts_profile_IC[ilong].resize(data.GLAST_counts_healpix.getEMin().size()   );
          counts_profile_IC[ilong]=0.;

          counts_profile_so[ilong].resize(data.GLAST_counts_healpix.getEMin().size()   );//AWS20081210
          counts_profile_so[ilong]=0.;                                                   //AWS20081210

          counts_profile_to[ilong].resize(data.GLAST_counts_healpix.getEMin().size()   );//AWS20081211
          counts_profile_to[ilong]=0.;                                                   //AWS20081211

          counts_profile_sourcepop_sublimit[ilong].resize(data.GLAST_counts_healpix.getEMin().size()   );//AWS20090914
          counts_profile_sourcepop_sublimit[ilong]=0.;                                                   //AWS20010914

          counts_profile_sourcepop_soplimit[ilong].resize(data.GLAST_counts_healpix.getEMin().size()   );//AWS20090914
          counts_profile_sourcepop_soplimit[ilong]=0.;                                                   //AWS20010914


          counts_profile_solar_IC          [ilong].resize(data.GLAST_counts_healpix.getEMin().size()   );//AWS20100218
          counts_profile_solar_IC          [ilong]=0.;                                                   //AWS20100218

          counts_profile_solar_disk        [ilong].resize(data.GLAST_counts_healpix.getEMin().size()   );//AWS20100218
          counts_profile_solar_disk        [ilong]=0.;                                                   //AWS20100218

          counts_profile_solar_IC_disk     [ilong].resize(data.GLAST_counts_healpix.getEMin().size()   );//AWS20100218
          counts_profile_solar_IC_disk     [ilong]=0.;                                                   //AWS20100218

           exposure_profile[ilong].resize(data.GLAST_counts_healpix.getEMin().size()   );
           exposure_profile[ilong]=0.;

          intensity_profile[ilong].resize(data.GLAST_counts_healpix.getEMin().size()   );
          intensity_profile[ilong]=0.;


        intensity_profile_HI[ilong].resize(data.GLAST_counts_healpix.getEMin().size()   );
        intensity_profile_HI[ilong]=0.;

        intensity_profile_H2[ilong].resize(data.GLAST_counts_healpix.getEMin().size()   );
        intensity_profile_H2[ilong]=0.;

        intensity_profile_HII[ilong].resize(data.GLAST_counts_healpix.getEMin().size()   );//AWS20090811
        intensity_profile_HII[ilong]=0.;                                                   //AWS20090811

        intensity_profile_IC[ilong].resize(data.GLAST_counts_healpix.getEMin().size()   );
        intensity_profile_IC[ilong]=0.;

        intensity_profile_EB[ilong].resize(data.GLAST_counts_healpix.getEMin().size()   );
        intensity_profile_EB[ilong]=0.;

        intensity_profile_so[ilong].resize(data.GLAST_counts_healpix.getEMin().size()   );//AWS20081210
        intensity_profile_so[ilong]=0.;                                                   //AWS20081210


        intensity_profile_to[ilong].resize(data.GLAST_counts_healpix.getEMin().size()   );//AWS20081211
        intensity_profile_to[ilong]=0.;                                                   //AWS20081211


        intensity_profile_sourcepop_sublimit[ilong].resize(data.GLAST_counts_healpix.getEMin().size()   );//AWS20090914
        intensity_profile_sourcepop_sublimit[ilong]=0.;                                                   //AWS20010914

        intensity_profile_sourcepop_soplimit[ilong].resize(data.GLAST_counts_healpix.getEMin().size()   );//AWS20090914
        intensity_profile_sourcepop_soplimit[ilong]=0.;                                                   //AWS20010914

        intensity_profile_total_with_sourcepop_sublimit[ilong].resize(data.GLAST_counts_healpix.getEMin().size()   );//AWS20090914
        intensity_profile_total_with_sourcepop_sublimit[ilong]=0.;                                                   //AWS20010914

        intensity_profile_solar_IC                     [ilong].resize(data.GLAST_counts_healpix.getEMin().size()   );//AWS20100218
        intensity_profile_solar_IC                     [ilong]=0.;                                                   //AWS20100218

        intensity_profile_solar_disk                   [ilong].resize(data.GLAST_counts_healpix.getEMin().size()   );//AWS20100218
        intensity_profile_solar_disk                   [ilong]=0.;                                                   //AWS20100218

        intensity_profile_solar_IC_disk                [ilong].resize(data.GLAST_counts_healpix.getEMin().size()   );//AWS20100218
        intensity_profile_solar_IC_disk                [ilong]=0.;                                                   //AWS20100218

    intensity_error_profile[ilong].resize(data.GLAST_counts_healpix.getEMin().size()   );
    intensity_error_profile[ilong]=0.;
           number_of_pixels[ilong].resize(data.GLAST_counts_healpix.getEMin().size()   );
           number_of_pixels[ilong]=0.;
   }

   for (ipix=0;ipix<convolved.GLAST_convolved_counts .Npix() ;ipix++)
   {
    l =        convolved.GLAST_convolved_counts.pix2ang(ipix).phi   * rtd;
    b = 90.0 - convolved.GLAST_convolved_counts.pix2ang(ipix).theta * rtd;
    //  cout<<"ipix="<<ipix<<" l = "<<l<<" b ="<<b<<endl; // l,b in deg

    int select=0;
    if(l>=galplotdef.long_min1 && l<=galplotdef.long_max1) select=1;
    if(l>=galplotdef.long_min2 && l<=galplotdef.long_max2) select=1;

    if (select==1)
    {
      //  cout<<"selected ipix="<<ipix<<" l = "<<l<<" b ="<<b<<endl; // l,b in deg
      b_off= b+90.0; // l:0-360, offset l_off relative to b=-90
    
      ilong=           int(b_off / galplotdef. lat_binsize + .01);

      if(galplotdef.verbose==-1901)   cout<<"plot_GLAST_profile_healpix: b_off="<<b_off<<" ilong="<<ilong<<endl;
      if(ilong<0||ilong>nlat )        cout<<"plot_GLAST_profile_healpix:error: ilong out of range:"<<ilong<<endl;

     if(ilong>=0 && ilong<nlat )
     for( i_E_GLAST=0;i_E_GLAST< data.GLAST_counts_healpix.getEMin().size();i_E_GLAST++)
     {

      if(galplotdef.convolve_GLAST==0) //AWS20080718
       {
          counts_profile    [ilong][i_E_GLAST] += unconvolved.GLAST_unconvolved_counts   [ipix][i_E_GLAST];
          counts_profile_HI [ilong][i_E_GLAST] += unconvolved.GLAST_unconvolved_counts_HI[ipix][i_E_GLAST];
          counts_profile_H2 [ilong][i_E_GLAST] += unconvolved.GLAST_unconvolved_counts_H2[ipix][i_E_GLAST];
          counts_profile_HII[ilong][i_E_GLAST] += unconvolved.GLAST_unconvolved_counts_HII[ipix][i_E_GLAST];//AWS20090811
          counts_profile_IC [ilong][i_E_GLAST] += unconvolved.GLAST_unconvolved_counts_IC[ipix][i_E_GLAST];
          counts_profile_so [ilong][i_E_GLAST] += unconvolved.GLAST_unconvolved_counts_sources[ipix][i_E_GLAST];//AWS20081211

          counts_profile_sourcepop_sublimit [ilong][i_E_GLAST] += unconvolved.GLAST_unconvolved_counts_sourcepop_sublimit[ipix][i_E_GLAST]; //AWS20090114
          counts_profile_sourcepop_soplimit [ilong][i_E_GLAST] += unconvolved.GLAST_unconvolved_counts_sourcepop_soplimit[ipix][i_E_GLAST]; //AWS20090114


          counts_profile_solar_IC           [ilong][i_E_GLAST] += unconvolved.GLAST_unconvolved_counts_solar_IC          [ipix][i_E_GLAST]; //AWS20100219
          counts_profile_solar_disk         [ilong][i_E_GLAST] += unconvolved.GLAST_unconvolved_counts_solar_disk        [ipix][i_E_GLAST]; //AWS20100219

       	   exposure_profile [ilong][i_E_GLAST] +=                          data.GLAST_exposure_integrated[ipix][i_E_GLAST]*convolved.GLAST_convolved_counts.solidAngle(); 

        intensity_profile   [ilong][i_E_GLAST] += unconvolved.GLAST_unconvolved_counts[ipix][i_E_GLAST]/
                                                                          (data.GLAST_exposure_integrated[ipix][i_E_GLAST]*convolved.GLAST_convolved_counts.solidAngle());

        intensity_profile_HI[ilong][i_E_GLAST] += unconvolved.GLAST_unconvolved_counts_HI[ipix][i_E_GLAST]/
                                                                          (data.GLAST_exposure_integrated[ipix][i_E_GLAST]*convolved.GLAST_convolved_counts.solidAngle());
        intensity_profile_H2[ilong][i_E_GLAST] += unconvolved.GLAST_unconvolved_counts_H2[ipix][i_E_GLAST]/
                                                                          (data.GLAST_exposure_integrated[ipix][i_E_GLAST]*convolved.GLAST_convolved_counts.solidAngle());
        intensity_profile_HII[ilong][i_E_GLAST] += unconvolved.GLAST_unconvolved_counts_HII[ipix][i_E_GLAST]/
	                                                                  (data.GLAST_exposure_integrated[ipix][i_E_GLAST]*convolved.GLAST_convolved_counts.solidAngle());//AWS20090811

        intensity_profile_IC[ilong][i_E_GLAST] += unconvolved.GLAST_unconvolved_counts_IC[ipix][i_E_GLAST]/
                                                                          (data.GLAST_exposure_integrated[ipix][i_E_GLAST]*convolved.GLAST_convolved_counts.solidAngle());
        intensity_profile_so[ilong][i_E_GLAST] += unconvolved.GLAST_unconvolved_counts_sources[ipix][i_E_GLAST]/
                                                                          (data.GLAST_exposure_integrated[ipix][i_E_GLAST]*convolved.GLAST_convolved_counts.solidAngle());//AWS20081211

        intensity_profile_sourcepop_sublimit[ilong][i_E_GLAST] += unconvolved.GLAST_unconvolved_counts_sourcepop_sublimit[ipix][i_E_GLAST]/
                                                                          (data.GLAST_exposure_integrated[ipix][i_E_GLAST]*convolved.GLAST_convolved_counts.solidAngle());//AWS20090114
        intensity_profile_sourcepop_soplimit[ilong][i_E_GLAST] += unconvolved.GLAST_unconvolved_counts_sourcepop_soplimit[ipix][i_E_GLAST]/
                                                                          (data.GLAST_exposure_integrated[ipix][i_E_GLAST]*convolved.GLAST_convolved_counts.solidAngle());//AWS20090114


        intensity_profile_solar_IC          [ilong][i_E_GLAST] += unconvolved.GLAST_unconvolved_counts_solar_IC          [ipix][i_E_GLAST]/
                                                                          (data.GLAST_exposure_integrated[ipix][i_E_GLAST]*convolved.GLAST_convolved_counts.solidAngle());//AWS2100219

        intensity_profile_solar_disk        [ilong][i_E_GLAST] += unconvolved.GLAST_unconvolved_counts_solar_disk        [ipix][i_E_GLAST]/
                                                                          (data.GLAST_exposure_integrated[ipix][i_E_GLAST]*convolved.GLAST_convolved_counts.solidAngle());//AWS2100219  
       }




       if(galplotdef.convolve_GLAST==1) //AWS20080718
       {
          counts_profile    [ilong][i_E_GLAST] +=   convolved.GLAST_convolved_counts   [ipix][i_E_GLAST];
          counts_profile_HI [ilong][i_E_GLAST] +=   convolved.GLAST_convolved_counts_HI[ipix][i_E_GLAST];
          counts_profile_H2 [ilong][i_E_GLAST] +=   convolved.GLAST_convolved_counts_H2[ipix][i_E_GLAST];
          counts_profile_HII[ilong][i_E_GLAST] +=   convolved.GLAST_convolved_counts_HII[ipix][i_E_GLAST];//AWS20090811
          counts_profile_IC [ilong][i_E_GLAST] +=   convolved.GLAST_convolved_counts_IC[ipix][i_E_GLAST];
          counts_profile_so [ilong][i_E_GLAST] +=   convolved.GLAST_convolved_counts_sources[ipix][i_E_GLAST];//AWS20081210
          counts_profile_sourcepop_sublimit [ilong][i_E_GLAST] +=   convolved.GLAST_convolved_counts_sourcepop_sublimit[ipix][i_E_GLAST]; //AWS20090114
          counts_profile_sourcepop_soplimit [ilong][i_E_GLAST] +=   convolved.GLAST_convolved_counts_sourcepop_soplimit[ipix][i_E_GLAST]; //AWS20090114

          counts_profile_solar_IC           [ilong][i_E_GLAST] +=   convolved.GLAST_convolved_counts_solar_IC          [ipix][i_E_GLAST]; //AWS20100218
          counts_profile_solar_disk         [ilong][i_E_GLAST] +=   convolved.GLAST_convolved_counts_solar_disk        [ipix][i_E_GLAST]; //AWS20100218

       	 exposure_profile[ilong][i_E_GLAST] +=                             data.GLAST_exposure_integrated[ipix][i_E_GLAST]*convolved.GLAST_convolved_counts.solidAngle(); 

        intensity_profile[ilong][i_E_GLAST] +=   convolved.GLAST_convolved_counts[ipix][i_E_GLAST]/
                                                                          (data.GLAST_exposure_integrated[ipix][i_E_GLAST]*convolved.GLAST_convolved_counts.solidAngle());

        intensity_profile_HI[ilong][i_E_GLAST] +=   convolved.GLAST_convolved_counts_HI[ipix][i_E_GLAST]/
                                                                          (data.GLAST_exposure_integrated[ipix][i_E_GLAST]*convolved.GLAST_convolved_counts.solidAngle());
        intensity_profile_H2[ilong][i_E_GLAST] +=   convolved.GLAST_convolved_counts_H2[ipix][i_E_GLAST]/
                                                                          (data.GLAST_exposure_integrated[ipix][i_E_GLAST]*convolved.GLAST_convolved_counts.solidAngle());
        intensity_profile_HII[ilong][i_E_GLAST] +=   convolved.GLAST_convolved_counts_HII[ipix][i_E_GLAST]/
              	                                                          (data.GLAST_exposure_integrated[ipix][i_E_GLAST]*convolved.GLAST_convolved_counts.solidAngle());//AWS20090811

        intensity_profile_IC[ilong][i_E_GLAST] +=   convolved.GLAST_convolved_counts_IC[ipix][i_E_GLAST]/
                                                                          (data.GLAST_exposure_integrated[ipix][i_E_GLAST]*convolved.GLAST_convolved_counts.solidAngle());
        intensity_profile_so[ilong][i_E_GLAST] +=   convolved.GLAST_convolved_counts_sources[ipix][i_E_GLAST]/
                   	                                                  (data.GLAST_exposure_integrated[ipix][i_E_GLAST]*convolved.GLAST_convolved_counts.solidAngle());//AWS20081210

       intensity_profile_sourcepop_sublimit[ilong][i_E_GLAST] +=   convolved.GLAST_convolved_counts_sourcepop_sublimit[ipix][i_E_GLAST]/
                                                                          (data.GLAST_exposure_integrated[ipix][i_E_GLAST]*convolved.GLAST_convolved_counts.solidAngle());//AWS20090114
        intensity_profile_sourcepop_soplimit[ilong][i_E_GLAST] +=   convolved.GLAST_convolved_counts_sourcepop_soplimit[ipix][i_E_GLAST]/
                                                                          (data.GLAST_exposure_integrated[ipix][i_E_GLAST]*convolved.GLAST_convolved_counts.solidAngle());//AWS20090114

        intensity_profile_solar_IC          [ilong][i_E_GLAST] +=   convolved.GLAST_convolved_counts_solar_IC          [ipix][i_E_GLAST]/
                                                                          (data.GLAST_exposure_integrated[ipix][i_E_GLAST]*convolved.GLAST_convolved_counts.solidAngle());//AWS2100218

        intensity_profile_solar_disk        [ilong][i_E_GLAST] +=   convolved.GLAST_convolved_counts_solar_disk        [ipix][i_E_GLAST]/
                                                                          (data.GLAST_exposure_integrated[ipix][i_E_GLAST]*convolved.GLAST_convolved_counts.solidAngle());//AWS2100218 
       }

	number_of_pixels [ilong][i_E_GLAST] ++;

     } //i_E_GLAST
    } // if select

   } // ipix



  for (ilong=0;ilong<nlat  ;ilong++)
  { 
   for( i_E_GLAST=0;i_E_GLAST< data.GLAST_counts_healpix.getEMin().size();i_E_GLAST++)
   {

    isotropic_intensity=0.0;//AWS20080630

   if(galplotdef.isotropic_type==1) // integrate power law over GLAST energy bin
    isotropic_intensity=galplotdef.isotropic_const
                        *(  pow(data.GLAST_counts_healpix.getEMax()[i_E_GLAST], 1.0-galplotdef.isotropic_g)
                           -pow(data.GLAST_counts_healpix.getEMin()[i_E_GLAST], 1.0-galplotdef.isotropic_g)  )
                        /                                                      (1.0-galplotdef.isotropic_g);

     if(galplotdef.isotropic_type==2) // integrate tabulated spectrum  over GLAST energy bin
      isotropic_intensity= isotropic.integrated(data.GLAST_counts_healpix.getEMin()[i_E_GLAST],data.GLAST_counts_healpix.getEMax()[i_E_GLAST], 1);

   //   cout<<"    isotropic_intensity="<<isotropic_intensity<<endl;


    if(exposure_profile[ilong][i_E_GLAST]>0.)
    {

     if(intensity_method==1)
      {
      //   average intensity over bin, error estimate from total counts
           intensity_profile    [ilong][i_E_GLAST]/= number_of_pixels [ilong][i_E_GLAST];
            intensity_profile_HI[ilong][i_E_GLAST]/= number_of_pixels [ilong][i_E_GLAST];
            intensity_profile_H2[ilong][i_E_GLAST]/= number_of_pixels [ilong][i_E_GLAST];
            intensity_profile_HII[ilong][i_E_GLAST]/=number_of_pixels [ilong][i_E_GLAST];//AWS20090811
            intensity_profile_IC[ilong][i_E_GLAST]/= number_of_pixels [ilong][i_E_GLAST];
            intensity_profile_so[ilong][i_E_GLAST]/= number_of_pixels [ilong][i_E_GLAST];//AWS20081210
            intensity_profile_sourcepop_sublimit
                                [ilong][i_E_GLAST]/= number_of_pixels [ilong][i_E_GLAST];//AWS20090114
            intensity_profile_sourcepop_soplimit
                                [ilong][i_E_GLAST]/= number_of_pixels [ilong][i_E_GLAST];//AWS20090114

            intensity_profile_solar_IC               
                                [ilong][i_E_GLAST]/= number_of_pixels [ilong][i_E_GLAST];//AWS20100218
            intensity_profile_solar_disk         
                                [ilong][i_E_GLAST]/= number_of_pixels [ilong][i_E_GLAST];//AWS20100218

       intensity_error_profile[ilong][i_E_GLAST]=  intensity_profile[ilong][i_E_GLAST]/ sqrt(counts_profile[ilong][i_E_GLAST]);
      }

      if(intensity_method==2)
      {
     //  method ignoring variations of intensity and exposure
             intensity_profile[ilong][i_E_GLAST]=      counts_profile[ilong][i_E_GLAST] /exposure_profile[ilong][i_E_GLAST] ;

       intensity_error_profile[ilong][i_E_GLAST]=    sqrt(counts_profile[ilong][i_E_GLAST])/exposure_profile[ilong][i_E_GLAST] ;

       intensity_profile_HI   [ilong][i_E_GLAST]=      counts_profile_HI[ilong][i_E_GLAST] /exposure_profile[ilong][i_E_GLAST] ;
       intensity_profile_H2   [ilong][i_E_GLAST]=      counts_profile_H2[ilong][i_E_GLAST] /exposure_profile[ilong][i_E_GLAST] ;
       intensity_profile_HII  [ilong][i_E_GLAST]=      counts_profile_HII[ilong][i_E_GLAST] /exposure_profile[ilong][i_E_GLAST];//AWS20090811
       intensity_profile_IC   [ilong][i_E_GLAST]=      counts_profile_IC[ilong][i_E_GLAST] /exposure_profile[ilong][i_E_GLAST] ;
       intensity_profile_so   [ilong][i_E_GLAST]=      counts_profile_so[ilong][i_E_GLAST] /exposure_profile[ilong][i_E_GLAST] ;//AWS20081210
       intensity_profile_sourcepop_sublimit
                              [ilong][i_E_GLAST]=      counts_profile_sourcepop_sublimit
                                                                        [ilong][i_E_GLAST] /exposure_profile[ilong][i_E_GLAST] ;//AWS20090114
       intensity_profile_sourcepop_soplimit
                              [ilong][i_E_GLAST]=      counts_profile_sourcepop_soplimit
                                                                        [ilong][i_E_GLAST] /exposure_profile[ilong][i_E_GLAST] ;//AWS20090114


       intensity_profile_solar_IC             
                              [ilong][i_E_GLAST]=      counts_profile_solar_IC              
                                                                        [ilong][i_E_GLAST] /exposure_profile[ilong][i_E_GLAST] ;//AWS20100218
       intensity_profile_solar_disk          
                              [ilong][i_E_GLAST]=      counts_profile_solar_disk          
                                                                        [ilong][i_E_GLAST] /exposure_profile[ilong][i_E_GLAST] ;//AWS20100218
       }
    }// if exposure

      intensity_profile_EB[ilong][i_E_GLAST]  = isotropic_intensity; //AWS20080630
      intensity_profile   [ilong][i_E_GLAST] += isotropic_intensity; //AWS20080630

   } //i_E_GLAST
  } //i_long



  // total including catalogue sources
  // total including source population below threshold
   for (ilong=0;ilong<nlat  ;ilong++)
   {
       counts_profile_to[ilong] =    counts_profile[ilong] +    counts_profile_so[ilong];
    intensity_profile_to[ilong] = intensity_profile[ilong] + intensity_profile_so[ilong];
    intensity_profile_total_with_sourcepop_sublimit[ilong] = intensity_profile_to[ilong] + intensity_profile_sourcepop_sublimit[ilong]; //AWS20100812 (was missing _to)
   }

   //  if(galplotdef.verbose==-2201)
   for (ilong=0;ilong<nlat  ;ilong++)
     {
       cout<<"counts latitude profile ilong= "<<ilong<<" b="<<-90.+ilong* galplotdef. lat_binsize<<"-"<<-90.+( ilong+1)* galplotdef. lat_binsize <<" : ";
       for( i_E_GLAST=0;i_E_GLAST< data.GLAST_counts_healpix.getEMin().size();i_E_GLAST++)
	 cout<<  counts_profile[ilong][i_E_GLAST]<< " "      ;
       cout<<endl;

       cout<<"  intensity profile:";
       for( i_E_GLAST=0;i_E_GLAST< data.GLAST_counts_healpix.getEMin().size();i_E_GLAST++)
	 cout<<  intensity_profile[ilong][i_E_GLAST]<< " "      ;
       cout<<endl;

       cout<<"  intensity profile_EB:";
       for( i_E_GLAST=0;i_E_GLAST< data.GLAST_counts_healpix.getEMin().size();i_E_GLAST++)
	 cout<<  intensity_profile_EB[ilong][i_E_GLAST]<< " "      ;
       cout<<endl;


       cout<<"  intensity error profile:";
       for( i_E_GLAST=0;i_E_GLAST< data.GLAST_counts_healpix.getEMin().size();i_E_GLAST++)
	 cout<<  intensity_error_profile[ilong][i_E_GLAST]<< " "      ;
       cout<<endl;
       cout<<"counts profile sourcepop sublimit:";
       for( i_E_GLAST=0;i_E_GLAST< data.GLAST_counts_healpix.getEMin().size();i_E_GLAST++)
	 cout<<  counts_profile_sourcepop_sublimit[ilong][i_E_GLAST]<< " "      ;
       cout<<endl;

       cout<<"intensity profile sourcepop sublimit:";
       for( i_E_GLAST=0;i_E_GLAST< data.GLAST_counts_healpix.getEMin().size();i_E_GLAST++)
	 cout<<  intensity_profile_sourcepop_sublimit[ilong][i_E_GLAST]<< " "      ;
       cout<<endl;

       cout<<"counts profile sourcepop soplimit:";
       for( i_E_GLAST=0;i_E_GLAST< data.GLAST_counts_healpix.getEMin().size();i_E_GLAST++)
	 cout<<  counts_profile_sourcepop_soplimit[ilong][i_E_GLAST]<< " "      ;
       cout<<endl;

       cout<<"intensity profile sourcepop soplimit:";
       for( i_E_GLAST=0;i_E_GLAST< data.GLAST_counts_healpix.getEMin().size();i_E_GLAST++)
	 cout<<  intensity_profile_sourcepop_soplimit[ilong][i_E_GLAST]<< " "      ;
       cout<<endl;


      cout<<"intensity profile total with sourcepop sublimit:";
       for( i_E_GLAST=0;i_E_GLAST< data.GLAST_counts_healpix.getEMin().size();i_E_GLAST++)
	 cout<<  intensity_profile_total_with_sourcepop_sublimit[ilong][i_E_GLAST]<< " "      ;
       cout<<endl;



      cout<<"intensity profile solar IC:                     ";
       for( i_E_GLAST=0;i_E_GLAST< data.GLAST_counts_healpix.getEMin().size();i_E_GLAST++)
	 cout<<  intensity_profile_solar_IC                     [ilong][i_E_GLAST]<< " "      ;
       cout<<endl;

      cout<<"intensity profile solar disk:                   ";
       for( i_E_GLAST=0;i_E_GLAST< data.GLAST_counts_healpix.getEMin().size();i_E_GLAST++)
	 cout<<  intensity_profile_solar_disk                   [ilong][i_E_GLAST]<< " "      ;
       cout<<endl;
   }

   ///////////////////////////////

   

    i_E_GLAST=ip;

   if(FALSE) //AWS20110826
   {

    txt_stream<<endl<<"-------------------------------------------------"<<endl;

    for (ilong=0;ilong<nlat ;ilong++)
    {
           
      
       double   b=-90.0 + (ilong+0.5)*galplotdef.lat_binsize;

                 
        txt_stream
            <<" model latitude profile"
                       <<" l = "<<galplotdef.long_min1<<" - "<<galplotdef.long_max1
            <<", "              <<galplotdef.long_min2<<" - "<<galplotdef.long_max2
            <<"  b = "<<b 

            <<"  E= "<< data.GLAST_counts_healpix.getEMin()[i_E_GLAST]<< " - "
                     << data.GLAST_counts_healpix.getEMax()[i_E_GLAST]<< " MeV "
            <<" counts = "         <<     counts_profile    [ilong][i_E_GLAST] 
            <<" total intensity = "<<  intensity_profile    [ilong][i_E_GLAST] 
            <<" HI =  "            <<  intensity_profile_HI [ilong][i_E_GLAST]
            <<" H2 =  "            <<  intensity_profile_H2 [ilong][i_E_GLAST]
            <<" HII = "            <<  intensity_profile_HII[ilong][i_E_GLAST]
            <<" IC =  "            <<  intensity_profile_IC [ilong][i_E_GLAST]
            <<" sources = "        <<  intensity_profile_so [ilong][i_E_GLAST]
            <<" sourcepop sublimit = "<<  intensity_profile_sourcepop_sublimit[ilong][i_E_GLAST]
            <<" sourcepop soplimit = "<<  intensity_profile_sourcepop_soplimit[ilong][i_E_GLAST]
            <<" EB = "             <<  intensity_profile_EB [ilong][i_E_GLAST]
            <<" cm-2 sr-1 s-1"
            <<endl;

  
       
      
     }//ilong

   }//AWS20110826
 


  ////////////////////////////////////////////////////////////////////////////////////////////
   // generate the root objects
     
   profile=   new TH1D("GLAST convolved latitude profile healpix",   "GLAST convolved latitude  profile healpix",nlat ,      -90.,90.);
   profile_HI=new TH1D("GLAST convolved latitude profile healpix HI","GLAST convolved latitude  profile healpix",nlat,       -90.,90.);
   profile_H2=new TH1D("GLAST convolved latitude profile healpix H2","GLAST convolved latitude  profile healpix",nlat,       -90.,90.);
   profile_HII=new TH1D("GLAST convolved latitude profile healpix HII","GLAST convolved latitude  profile healpix",nlat,       -90.,90.);//AWS20090811
   profile_IC=new TH1D("GLAST convolved latitude profile healpix IC","GLAST convolved latitude  profile healpix",nlat,       -90.,90.);
   profile_EB=new TH1D("GLAST convolved latitude profile healpix EB","GLAST convolved latitude  profile healpix",nlat,       -90.,90.);//AWS20080630
   profile_so=new TH1D("GLAST convolved latitude profile healpix so","GLAST convolved latitude  profile healpix",nlat,       -90.,90.);//AWS20081210
   profile_to=new TH1D("GLAST convolved latitude profile healpix to","GLAST convolved latitude  profile healpix",nlat,       -90.,90.);//AWS20081211
   profile_sourcepop_sublimit
             =new TH1D("GLAST convolved latitude profile healpix sourcepop sublimit","GLAST convolved latitude  profile healpix",nlat,        -90.,90.); //AWS20090114
   profile_sourcepop_soplimit
             =new TH1D("GLAST convolved latitude profile healpix sourcepop soplimit","GLAST convolved latitude  profile healpix",nlat,        -90.,90.); //AWS20090114
   profile_total_with_sourcepop_sublimit
             =new TH1D("GLAST convolved latitude profile healpix total with sourcepop soplimit","GLAST convolved latitude  profile healpix",nlat,        -90.,90.); //AWS20090114


   profile_solar_IC            
             =new TH1D("GLAST convolved latiude profile healpix solar IC             ","GLAST convolved latitude  profile healpix",nlat,-180.,180.); //AWS20100218
   profile_solar_disk                     
             =new TH1D("GLAST convolved latiitude profile healpix solar disk         ","GLAST convolved latitude  profile healpix",nlat,-180.,180.); //AWS20100218
   profile_solar_IC_disk                     
             =new TH1D("GLAST convolved latiitude profile healpix solar IC disk      ","GLAST convolved latitude  profile healpix",nlat,-180.,180.); //AWS20100218

   int i_long,ii_long;
   i_E_GLAST=ip;

  for(i_long=0;i_long<profile->GetNbinsX();i_long++)
  {
     ii_long=i_long+1; // TH1D bins start at 1
     profile   ->SetBinContent(ii_long      , intensity_profile         [i_long][i_E_GLAST] );
     profile_HI->SetBinContent(ii_long      , intensity_profile_HI      [i_long][i_E_GLAST] );
     profile_H2->SetBinContent(ii_long      , intensity_profile_H2      [i_long][i_E_GLAST] );
     profile_HII->SetBinContent(ii_long     , intensity_profile_HII     [i_long][i_E_GLAST] );//AWS20090811
     profile_IC->SetBinContent(ii_long      , intensity_profile_IC      [i_long][i_E_GLAST] );
     profile_EB->SetBinContent(ii_long      , intensity_profile_EB      [i_long][i_E_GLAST] );
     profile_so->SetBinContent(ii_long      , intensity_profile_so      [i_long][i_E_GLAST] );//AWS20081210
     profile_to->SetBinContent(ii_long      , intensity_profile_to      [i_long][i_E_GLAST] );//AWS20081211
     profile_sourcepop_sublimit
               ->SetBinContent(ii_long      , intensity_profile_sourcepop_sublimit
                                                                        [i_long][i_E_GLAST] );//AWS20090114
    profile_sourcepop_soplimit
               ->SetBinContent(ii_long      , intensity_profile_sourcepop_soplimit
                                                                        [i_long][i_E_GLAST] );//AWS20090114


    profile_total_with_sourcepop_sublimit
              ->SetBinContent(ii_long      , intensity_profile_total_with_sourcepop_sublimit
                                                                       [i_long][i_E_GLAST] ); //AWS20090114

    profile_solar_IC               
               ->SetBinContent(ii_long      , intensity_profile_solar_IC             
                                                                        [i_long][i_E_GLAST] );//AWS20100218
    profile_solar_disk               
               ->SetBinContent(ii_long      , intensity_profile_solar_disk           
                                                                        [i_long][i_E_GLAST] );//AWS20100218

     // this causes errors to be plotted, so make it an option
     if(use_errors==1)
     profile->SetBinError  (ii_long      , intensity_error_profile[i_long][i_E_GLAST] );

      if(galplotdef.verbose==-1901)
      cout<<"GLAST latitude profile: intensity = "<< profile->GetBinContent(ii_long)<<" error="<< profile->GetBinError(ii_long)<<endl;
  }

  }// latprof



   //--------------------------------------------------------------------------------------------------------



   // plot the profile

   
  if(longprof)
  {

  sprintf(workstring2,"  %5.1f>b>%5.1f,%5.1f>b>%5.1f      ",
                         galplotdef.lat_min1,galplotdef.lat_max1,
                         galplotdef.lat_min2,galplotdef.lat_max2);
  strcpy(workstring1,canvastitle);
  strcat(workstring1,workstring2);

  strcat(name,"total_longitude healpix");// to identify it: name is not plotted
  c1=new TCanvas(name,canvastitle,600+ip*10,150+ip*10,600,600);
  if(galplotdef.long_log_scale==1)c1->SetLogy();                //a TCanvas is a TPad, TPad has this function

  profile->GetXaxis()->SetNdivisions(0); // eliminates X-axis since done separately

  if(galplotdef.long_log_scale==0)   profile->SetMaximum(profile->GetMaximum()*1.5); // to allow for data with larger range  
  if(galplotdef.long_log_scale==1)   profile->SetMaximum(profile->GetMaximum()*2.0); 


  if(galplotdef.long_log_scale==0)   profile->SetMinimum(0.0);                         
  if(galplotdef.long_log_scale==1)   profile->SetMinimum(profile->GetMaximum()/1e4);   

  }
   
 if(latprof)
 {
  sprintf(workstring2,"  %5.1f>l>%5.1f,%5.1f>l>%5.1f       ",
                 	  galplotdef.long_min1,galplotdef.long_max1,
                          galplotdef.long_min2,galplotdef.long_max2);
  strcpy(workstring1,canvastitle);
  strcat(workstring1,workstring2);

  strcat(name,"total_latitude_healpix");
  c1=new TCanvas(name,canvastitle,650+ip*10,150+ip*10,600,600);
  if(galplotdef.lat_log_scale==1)c1->SetLogy();                //a TCanvas is a TPad, TPad has this function

  profile->GetXaxis()->SetTitle("Galactic latitude");

  if(galplotdef.lat_log_scale==0)   profile->SetMaximum(profile->GetMaximum()*1.5); // to allow for data with larger range  
  if(galplotdef.lat_log_scale==1)   profile->SetMaximum(profile->GetMaximum()*2.0); 


  if(galplotdef.lat_log_scale==0)   profile->SetMinimum(0.0);                         
  if(galplotdef.lat_log_scale==1)   profile->SetMinimum(profile->GetMaximum()/1e4);   
 }




  profile->GetYaxis()->SetTitle("intensity, cm^{-2} sr^{-1} s^{-1}  ");
  profile->GetYaxis()->SetTitleOffset(1.2);
  profile->GetYaxis()->SetLabelSize  (0.030);

  profile->SetTitle(""         ); //written in box on plot


 

 


  TGaxis::SetMaxDigits(4);  // see root #1329 and mail root 20 Aug 2002

  profile->SetStats(kFALSE);

  profile->GetYaxis()->SetTitle("intensity, cm^{-2} sr^{-1} s^{-1}  ");

  /*
  root Rtypes.h:
  enum EColor { kWhite =0,   kBlack =1,   kGray=920,
                kRed   =632, kGreen =416, kBlue=600, kYellow=400, kMagenta=616, kCyan=432,
                kOrange=800, kSpring=820, kTeal=840, kAzure =860, kViolet =880, kPink=900 };
  */

  profile   ->SetLineColor(kBlue    );
  profile_HI->SetLineColor(kRed    );
  profile_H2->SetLineColor(kMagenta);
  profile_HII->SetLineColor(kRed  );//AWS20090812
  profile_HII->SetLineStyle(3     );//AWS20090812 // 1=solid 2=dash 3=dot 4=dash-dot
  profile_IC->SetLineColor(kGreen  );
  profile_EB->SetLineColor(kBlack  ); //AWS20080630
  profile_so->SetLineColor(kCyan   ); //AWS20081210
  profile_to->SetLineColor(kOrange ); //AWS20100920

  profile_sourcepop_sublimit->SetLineColor(kCyan   );   //AWS20110731
  profile_sourcepop_sublimit->SetLineStyle(3   );       //AWS20110819 dot
  profile_sourcepop_soplimit->SetLineColor(kCyan   );   //AWS20110731
  profile_sourcepop_soplimit->SetLineStyle(2   );       //AWS20110819 dash

  profile_total_with_sourcepop_sublimit->SetLineColor(kCyan   ); //AWS20110731
  profile_total_with_sourcepop_sublimit->SetLineStyle(3   );     //AWS20110819 dot 

  profile_solar_IC->SetLineColor(kBlack  ); //AWS20100218
  profile_solar_IC->SetLineStyle(2);        //AWS20100218

  profile   ->Draw("L");
  {
  profile_HI->Draw("L same");
  profile_H2->Draw("L same");
  profile_HII->Draw("L same");        //AWS20090811
  profile_IC->Draw("L same");
  profile_EB->Draw("L same");         //AWS20080630
  profile_so->Draw("L same");         //AWS20081210
  profile_to->Draw("L same");         //AWS20081211

  if(galplotdef.sourcepop_sublimit==3)profile_sourcepop_sublimit->Draw("L same");            //AWS20090114  AWS20100811 (was ==1)
  if(galplotdef.sourcepop_soplimit==3)profile_sourcepop_soplimit->Draw("L same");            //AWS20090114  AWS20100811 (was ==1)
  if(galplotdef.sourcepop_total   ==3)profile_total_with_sourcepop_sublimit->Draw("L same"); //AWS20090114  AWS20100811 (was ==1)

  profile_solar_IC->Draw("L same");  //AWS20100216

  }

  if(longprof){  axis1->Draw();   axis2->Draw();   axis3->Draw();}

  profile       ->SetName("xxxx");// to avoid problem on subsequent calls










  //////////////////////////////////////////////////////////////////////////////////////////////

  // parameters labelling

  sprintf(workstring1,"  %5.2f<l<%5.2f , %5.2f<l<%5.2f",                 //AWS20040315
                          galplotdef.long_min1,galplotdef.long_max1,
                          galplotdef.long_min2,galplotdef.long_max2);
 
  sprintf(workstring2,"  %5.2f<b<%5.2f , %5.2f<b<%5.2f",                 //AWS20040315
                         galplotdef.lat_min1,galplotdef.lat_max1,
                         galplotdef.lat_min2,galplotdef.lat_max2);
  
  sprintf(workstring3," %5.0f - %5.0f MeV",data.GLAST_counts_healpix.getEMin()[ip],data.GLAST_counts_healpix.getEMax()[ip]);


  strcpy(workstring4," galdef ID ");
  strcat(workstring4,galdef.galdef_ID);


  TText *text=new TText();
  text->SetTextFont(62);
  text->SetTextColor(kBlack);
  text->SetTextSize(0.025 );
  text->SetTextAlign(12);

  if(longprof)
  text->DrawTextNDC(.56 ,.85 ,workstring2);// NDC=normalized coord system  AWS20040315
  if(latprof)
  text->DrawTextNDC(.52 ,.85 ,workstring1);// NDC=normalized coord system  AWS20040315
  text->DrawTextNDC(.58 ,.88 ,workstring3);// NDC=normalized coord system
  text->DrawTextNDC(.20 ,.92 ,workstring4);// NDC=normalized coord system



  // overplot data
  //  plot_GLAST_profile(longprof,latprof,ip,0);           //l,b data, different GLAST simulation ?

    plot_GLAST_profile_healpix(longprof,latprof,ip,0);       // healpix data

    int sourcepop=5; // choose Fermi source population                                                               AWS20090107
    ///   plot_source_population_profile(longprof,  latprof, ip,  1,1, 1, sourcepop);//AWS20051108 AWS20080605 AWS20081219 AWS20090107 AWS20090114

  //  if(galplotdef.sources_EGRET==300)plot_MILAGRO_profile(longprof,latprof,ip,0); //AWS20080107


 }//if E_gamma
} //ip

//============================================================================

  //============== postscript and gif output

  sprintf(workstring1,"l_%.2f_%.2f_%.2f_%.2f",                           //AWS20040315
                          galplotdef.long_min1,galplotdef.long_max1,
                          galplotdef.long_min2,galplotdef.long_max2);

  sprintf(workstring2,"b_%.2f_%.2f_%.2f_%.2f",                           //AWS20040315
                          galplotdef.lat_min1,galplotdef.lat_max1,
                          galplotdef.lat_min2,galplotdef.lat_max2);

  sprintf(workstring3,"%.0f-%.0f_MeV",data.GLAST_counts_healpix.getEMin()[ip],data.GLAST_counts_healpix.getEMax()[ip]);

 if(galplotdef.output_format==1 || galplotdef.output_format==3) //AWS20080905
   {

  strcpy(psfile,"plots/");
  if(longprof==1)
  strcat(psfile,"longitude_profile_GLAST_healpix_");
  if(latprof==1)
  strcat(psfile,"latitude_profile_GLAST_healpix_");
  strcat(psfile,galdef.galdef_ID);
  strcat(psfile,"_"        );
  strcat(psfile,workstring3);
  strcat(psfile,"_"        );
  if(longprof==1)
  strcat(psfile,workstring2);
  if( latprof==1)
  strcat(psfile,workstring1);

  if(galplotdef.convolve_GLAST==0)
  strcat(psfile,"_unconv_"  );
  if(galplotdef.convolve_GLAST!=0)
  strcat(psfile,"_conv_"    );

  strcat(psfile,galplotdef.psfile_tag);

  strcat(psfile,".eps");

  cout<<"postscript file="<<psfile<<endl;
  c1->Print(psfile       );


   }


 if(galplotdef.output_format==2 || galplotdef.output_format==3) //AWS20080905
   {

  strcpy(psfile,"plots/");
  if(longprof==1)
  strcat(psfile,"longitude_profile_GLAST_healpix_");
  if(latprof==1)
  strcat(psfile,"latitude_profile_GLAST_healpix_");
  strcat(psfile,galdef.galdef_ID);
  strcat(psfile,"_"        );
  strcat(psfile,workstring3);
  strcat(psfile,"_"        );
  if(longprof==1)
  strcat(psfile,workstring2);
  if( latprof==1)
  strcat(psfile,workstring1);

  if(galplotdef.convolve_GLAST==0)
  strcat(psfile,"_unconv_"  );
  if(galplotdef.convolve_GLAST!=0)
  strcat(psfile,"_conv_"    );

  strcat(psfile,galplotdef.psfile_tag);

  strcat(psfile,".gif");

  cout<<"       gif file="<<psfile<<endl;
  c1->Print(psfile       );


   }


  //==============


  /*----  longitude txt_stream gave crash: obviously since profiles are removed above!

  // print output
  txt_stream<<endl<<"gamma-ray intensities cm^-2 sr^-1 s^-1 "<<endl;

 if(longprof==1)
 {
   txt_stream<<" l       pi0          bremss           IC       isotropic    total "<<endl; // same order as in spectrum printout
  for (i_long=0;i_long<galaxy.n_long;i_long++)
  {
 
   l= 180.-  +galaxy. d_long*i_long; //profile has reverse axis
   if(l<0.0)l+=360.0;
   txt_stream <<l<<"    "<< profile_pi0_decay[i_long]<<"  " << profile_bremss[i_long]
                 <<"  " << profile_ic[i_long]<<"  " << isotropic_intensity<<"  " << profile_total[i_long]         <<endl;
  }
 }



 if(latprof==1)
 {
    txt_stream<<" b        pi0          bremss           IC       isotropic    total"<<endl; // same order as in spectrum printout
  for (i_lat=0;i_lat<galaxy.n_lat;i_lat++)
  {

   b=    galaxy. lat_min+galaxy. d_lat*i_lat;
    txt_stream <<b<<"    "<< profile_pi0_decay[i_lat ]<<"  " << profile_bremss[i_lat ]
               <<    "  " << profile_ic       [i_lat ]<<"  " << isotropic_intensity<<"  " << profile_total[i_lat ]         <<endl;
  }
 }

  ---*/

 

   cout<<" <<<< plot_convolved_GLAST_profile_healpix   "<<endl;
   return status;
}


 
