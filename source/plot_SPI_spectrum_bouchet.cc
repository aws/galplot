/* 
Bouchet et al. 2005 ApJ 635:1103, Section 6.2



     -50<l<+50, -25<b<+25

1.Section 6.2 
  power-law spectrum 50-1800 keV, excluding 300-500 keV 

  index=1.8+.5-.4  (fitted) and a 100 keV flux of (6 +- 2)  10-5 photons cm-2 s-1 keV-1

2. Section 6.2 
  index=1.65 (fixed) and a 100 keV flux of (4.8 +- 2.2)  10-5 photons cm-2 s-1 keV-1
  so plot only 50-300 keV.

3. Section 6.3 
  complete model fitting 300-500 keV
  index=1.65 (fixed) and a 100 keV flux of (5   +- 3  )  10-5 photons cm-2 s-1 keV-1
*/
#include"Galplot.h"                 
#include"galprop_classes.h"
#include"galprop.h"
#include"constants.h"
#include"fitsio.h"

int Galplot::plot_SPI_spectrum_bouchet    (int mode)
{

 
  

   cout<<" >>>> plot_SPI_spectrum_bouchet        "<<endl;
   int status=0;

   

 
   int n_energy;
   double *E,*Emin,*Emax,*dE,*intensity;  
   double solid_angle;             
   int i_energy,i;
   double A,dA,g;
   double factor;
   TGraph *spectrum;
  
  
 

   n_energy=2;
   E    =    new double[n_energy   ];
   intensity=new double[n_energy   ];

 
   // data only valid for the area given above !!
   // -50<l<+50, -25<b<+25
  
   // assume ALL emission is WITHIN the chosen sky region !

   solid_angle= (galplotdef.  lat_max1 -galplotdef.  lat_min1 + galplotdef.  lat_max2 - galplotdef.  lat_min2 )  /(57.3)
               *(galplotdef. long_max1 -galplotdef. long_min1 + galplotdef. long_max2 - galplotdef. long_min2 )  /(57.3);

   factor= 1.0e3/solid_angle; 


   if(mode==1)       //AWS20070905
   {

   // spectrum 1

   A =6.0e-5; //photons cm-2 s-1 keV-1
   dA=2.0e-5;
   g=1.8   ;

   // box plot
   spectrum=new TGraph(5);
   E[0]= .05;  // 50 keV
   E[1]=0.3;   //300 keV
   E[2]=E[1];
   E[3]=E[0];
   E[4]=E[0];

   for (i_energy=0;i_energy<=1;i_energy++)
   {
    intensity[i_energy]     =(A+dA)*pow(E[i_energy]/.100, -g )*factor;
    spectrum   ->SetPoint(i_energy,E[i_energy],intensity[i_energy] * pow(E[i_energy]  ,2));
    }

    for (i_energy=2;i_energy<=3;i_energy++)
   {
    intensity[i_energy]     =(A-dA)*pow(E[i_energy]/.100, -g )*factor;
    spectrum   ->SetPoint(i_energy,E[i_energy],intensity[i_energy] * pow(E[i_energy]  ,2));
    }

   i_energy=4;
   intensity[i_energy]=intensity[0];
   spectrum   ->SetPoint(i_energy,E[i_energy],intensity[i_energy] * pow(E[i_energy]  ,2));

  spectrum->SetMarkerColor(kMagenta);// also kCyan
  spectrum->SetMarkerStyle(22); // triangles
  spectrum->SetMarkerSize (0.5);//
  spectrum->SetLineColor(kCyan);// kCyan kMagenta
  spectrum->SetLineWidth(3     );
  spectrum->SetLineStyle(1      );//1=solid 2=dash 3=dot 4=dash-dot
   //spectrum->Draw("PL");  // points as markers + line 
  spectrum->Draw("L");                              


  // spectrum 2

   A =4.8e-5; //photons cm-2 s-1 keV-1
   dA=2.2e-5;
   g=1.65  ;

   // box plot
   spectrum=new TGraph(5);
   E[0]= .05;  // 50 keV
   E[1]=0.3;   //300 keV
   E[2]=E[1];
   E[3]=E[0];
   E[4]=E[0];

   for (i_energy=0;i_energy<=1;i_energy++)
   {
    intensity[i_energy]     =(A+dA)*pow(E[i_energy]/.100, -g )*factor;
    spectrum   ->SetPoint(i_energy,E[i_energy],intensity[i_energy] * pow(E[i_energy]  ,2));
    }

    for (i_energy=2;i_energy<=3;i_energy++)
   {
    intensity[i_energy]     =(A-dA)*pow(E[i_energy]/.100, -g )*factor;
    spectrum   ->SetPoint(i_energy,E[i_energy],intensity[i_energy] * pow(E[i_energy]  ,2));
    }

   i_energy=4;
   intensity[i_energy]=intensity[0];
   spectrum   ->SetPoint(i_energy,E[i_energy],intensity[i_energy] * pow(E[i_energy]  ,2));

  spectrum->SetMarkerColor(kMagenta);// also kCyan
  spectrum->SetMarkerStyle(22); // triangles
  spectrum->SetMarkerSize (0.5);//
  spectrum->SetLineColor(kCyan);// kCyan kMagenta
  spectrum->SetLineWidth(3     );
  spectrum->SetLineStyle(2      );//1=solid 2=dash 3=dot 4=dash-dot
   //spectrum->Draw("PL");  // points as markers + line 
  spectrum->Draw("L");                 
 
  // spectrum 3

   A =5.0e-5; //photons cm-2 s-1 keV-1
   dA=3.0e-5;
   g=1.65  ;

   // box plot
   spectrum=new TGraph(5);
   E[0]=0.3 ;  //300 keV
   E[1]=0.5;   //500 keV
   E[2]=E[1];
   E[3]=E[0];
   E[4]=E[0];

   for (i_energy=0;i_energy<=1;i_energy++)
   {
    intensity[i_energy]     =(A+dA)*pow(E[i_energy]/.100, -g )*factor;
    spectrum   ->SetPoint(i_energy,E[i_energy],intensity[i_energy] * pow(E[i_energy]  ,2));
    }

    for (i_energy=2;i_energy<=3;i_energy++)
   {
    intensity[i_energy]     =(A-dA)*pow(E[i_energy]/.100, -g )*factor;
    spectrum   ->SetPoint(i_energy,E[i_energy],intensity[i_energy] * pow(E[i_energy]  ,2));
    }

   i_energy=4;
   intensity[i_energy]=intensity[0];
   spectrum   ->SetPoint(i_energy,E[i_energy],intensity[i_energy] * pow(E[i_energy]  ,2));

  spectrum->SetMarkerColor(kMagenta);// also kCyan
  spectrum->SetMarkerStyle(22); // triangles
  spectrum->SetMarkerSize (0.5);//
  spectrum->SetLineColor(kCyan);// kCyan kMagenta
  spectrum->SetLineWidth(6     );
  spectrum->SetLineStyle(3      );//1=solid 2=dash 3=dot 4=dash-dot
   //spectrum->Draw("PL");  // points as markers + line 
  spectrum->Draw("L");            
  
   }// mode==1


   //-----------------------------------------------------------------------------------------------------------------------------

   /*
From Laurent Bouchet 13 Aug 2007

The file is an XSPEC ooutput
units are photons/(cm2.s.keV)
Spectra are for |l| < 29 deg. and |b| < 15 deg.

Column 1 : mean energy
Column 2 : error bar on mean energy
Column 3 : data  
Column 4 : error on data
column 5 : sum of analytical models
column 6 : exponetial cutoff power law model   
column 7 : power law model  
column 8 : 511 keV line line model
column 9 : positronium model

READ SERR 1 2
@composite_all_20_2000.pco
!
23.5 3.25 2.8821667E-3 1.5739634E-4 2.8356118E-3 4.8558326E-4 2.349086E-3 0.E+-  9.4262315E-7
31.5 4.75 1.1338238E-3 5.596297E-5 1.163194E-3 3.076939E-4 8.542323E-4 0.E+-  1.2677425E-6
42.75 6.5 4.1658047E-4 3.1009116E-5 3.9991943E-4 1.90804E-4 2.0738806E-4 0.E+-  1.7274062E-6
57.75 8.5 1.6553109E-4 3.880411E-5 1.5304438E-4 1.1903029E-4 3.1668085E-5 0.E+- 2.3460142E-6
78 11.75 5.691481E-5 1.5335027E-5 8.02505E-5 7.440121E-5 2.6592831E-6 0.E+-  3.1900137E-6
105.25 15.5 5.508536E-5 9.625901E-6 5.0946222E-5 4.651021E-5 9.6432735E-8 0.E+-  4.33958E-6
142 21.25 4.1023202E-5 7.3903656E-6 3.502515E-5 2.9115272E-5 1.2427441E-9 0.E+-  5.908635E-6
191.75 28.5 2.1920756E-5 5.3183625E-6 2.623245E-5 1.8188515E-5 3.6068112E-12-  0.E+ 8.043931E-6
259 38.75 2.5296677E-5 2.5186311E-6 2.2237658E-5 1.1362322E-5 1.5490968E-15-  0.E+ 1.0875337E-5
349.75 52 2.0551359E-5 2.4561663E-6 2.1501626E-5 7.097959E-6 4.628677E-20 0.E+-  1.4403667E-5
453.5 51.75 2.3329872E-5 2.728104E-6 2.4212794E-5 4.696908E-6 5.850384E-26-  9.513809E-13 1.9515885E-5
510.75 5.5 1.0399837E-4 9.279516E-6 1.0398764E-4 3.8654502E-6 5.6349936E-31-  1.0012218E-4 0.E+
624 107.75 4.1208854E-6 1.7246925E-6 2.8826307E-6 2.8826261E-6 8.966082E-33-  4.5692772E-12 0.E+
859.75 128 2.1095185E-18 1.2132579E-6 1.7361555E-6 1.7361555E-6 4.2038953E-45-  0.E+ 0.E+
1160.5 172.75 1.5629524E-18 9.232092E-7 1.0854897E-6 1.0854897E-6 0.E+ 0.E+-  0.E+
1566.75 233.5 1.3393669E-6 6.7411775E-7 6.7850425E-7 6.7850425E-7 0.E+ 0.E+- 0.E+
		    */

   double *data,*data_err,*sum,*powlaw,*powlawcut,*posline,*positronium;
   double powlaw_fractional_error; //AWS20080128

   if(mode==2)       //AWS20070905
   {
   n_energy=16;
   E    =      new double[n_energy   ];
  dE    =      new double[n_energy   ];
   Emin =      new double[n_energy   ];
   Emax =      new double[n_energy   ];
   data       =new double[n_energy   ];
   data_err   =new double[n_energy   ];
   sum        =new double[n_energy   ];
   powlaw     =new double[n_energy   ];
   powlawcut  =new double[n_energy   ];
   posline    =new double[n_energy   ];
   positronium=new double[n_energy   ];


 if(galplotdef.lat_max2 >=14.5 && galplotdef.lat_max2 <= 15.0 ) // by convention
   {

 // |l| < 29 deg. and |b| < 15 deg

   i=0;

   E[i]= 23.5;  dE[i]=3.25; data[i]=2.8821667E-3; data_err[i]=1.5739634E-4; sum[i]=2.8356118E-3;  powlaw[i]=4.8558326E-4;   powlawcut[i]=2.349086E-3;   posline[i]=0.;  positronium[i]=9.4262315E-7; i++;
   
   E[i]= 31.5;  dE[i]=4.75; data[i]=1.1338238E-3; data_err[i]=5.596297E-5;  sum[i]= 1.163194E-3;  powlaw[i]= 3.076939E-4;   powlawcut[i]= 8.542323E-4;  posline[i]=0. ; positronium[i]= 1.2677425E-6; i++;

   E[i]= 42.75; dE[i]=6.5;  data[i]=4.1658047E-4; data_err[i]=3.1009116E-5; sum[i]= 3.9991943E-4; powlaw[i]= 1.90804E-4;    powlawcut[i]= 2.0738806E-4; posline[i]= 0.; positronium[i]= 1.7274062E-6; i++;

   E[i]= 57.75; dE[i]=8.5;  data[i]=1.6553109E-4; data_err[i]=3.880411E-5;  sum[i]= 1.5304438E-4; powlaw[i]= 1.1903029E-4;  powlawcut[i]= 3.1668085E-5; posline[i]= 0.; positronium[i]= 2.3460142E-6; i++;

   E[i]= 78;    dE[i]=11.75;data[i]=5.691481E-5;  data_err[i]=1.5335027E-5; sum[i]= 8.02505E-5;   powlaw[i]= 7.440121E-5;   powlawcut[i]= 2.6592831E-6; posline[i]= 0.; positronium[i]=  3.1900137E-6; i++;

   E[i]=105.25; dE[i]=15.5; data[i]=5.508536E-5;  data_err[i]=9.625901E-6;  sum[i]= 5.0946222E-5; powlaw[i]= 4.651021E-5;   powlawcut[i]= 9.6432735E-8; posline[i]=0  ; positronium[i]=4.33958E-6; i++;

   E[i]=142;    dE[i]=21.25;data[i]=4.1023202E-5; data_err[i]=7.3903656E-6; sum[i]= 3.502515E-5;  powlaw[i]= 2.9115272E-5;  powlawcut[i]= 1.2427441E-9; posline[i]=0  ; positronium[i]= 5.908635E-6; i++;

   E[i]=191.75; dE[i]=28.5; data[i]=2.1920756E-5; data_err[i]=5.3183625E-6; sum[i]= 2.623245E-5;  powlaw[i]= 1.8188515E-5;  powlawcut[i]= 3.6068112E-12; posline[i]=0 ; positronium[i]=8.043931E-6; i++;

   E[i]=259;    dE[i]=38.75;data[i]=2.5296677E-5; data_err[i]=2.5186311E-6; sum[i]= 2.2237658E-5; powlaw[i]= 1.1362322E-5;  powlawcut[i]= 1.5490968E-15; posline[i]=0 ; positronium[i]=1.0875337E-5; i++;

   E[i]=349.75; dE[i]=52;   data[i]=2.0551359E-5; data_err[i]=2.4561663E-6; sum[i]= 2.1501626E-5; powlaw[i]= 7.097959E-6;   powlawcut[i]= 4.628677E-20; posline[i]=0 ;  positronium[i]= 1.4403667E-5; i++;

   E[i]=453.5;  dE[i]=51.75;data[i]=2.3329872E-5; data_err[i]=2.728104E-6;  sum[i]= 2.4212794E-5; powlaw[i]= 4.696908E-6;   powlawcut[i]= 5.850384E-26 ;posline[i]=0;   positronium[i]= 1.9515885E-5; i++;

   E[i]=510.75; dE[i]=5.5;  data[i]=1.0399837E-4; data_err[i]=9.279516E-6;  sum[i]= 1.0398764E-4; powlaw[i]= 3.8654502E-6;  powlawcut[i]= 5.6349936E-31 ; posline[i]= 1.0012218E-4; positronium[i]=0. ; i++;

   E[i]=624;   dE[i]=107.75;data[i]=4.1208854E-6; data_err[i]=1.7246925E-6; sum[i]= 2.8826307E-6; powlaw[i]= 2.8826261E-6;  powlawcut[i]= 8.966082E-33;  posline[i]=0 ; positronium[i]= 4.5692772E-12 ; i++;

   E[i]=859.75;dE[i]=128 ;  data[i]=2.1095185E-18;data_err[i]=1.2132579E-6; sum[i]= 1.7361555E-6; powlaw[i]= 1.7361555E-6;  powlawcut[i]= 4.2038953E-45 ; posline[i]=0 ;positronium[i]= 0.; i++;

   E[i]=1160.5;dE[i]=172.75;data[i]=1.5629524E-18;data_err[i]=9.232092E-7 ; sum[i]=1.0854897E-6;  powlaw[i]= 1.0854897E-6;  powlawcut[i]= 0.  ; posline[i]=0.;positronium[i]= 0.; i++;

 E[i]=1566.75; dE[i]= 233.5;data[i]= 1.3393669E-6;data_err[i]= 6.7411775E-7;sum[i]=6.7850425E-7;  powlaw[i]= 6.7850425E-7 ; powlawcut[i]=0.  ; posline[i]= 0. ;positronium[i]= 0.;

 powlaw_fractional_error=0.4; //AWS20080128 based on summing latitude profiles for model and pixel map

   } // |b|<15

 //-------------------------------------------------------------------------------------------------------------------


 if(galplotdef.lat_max2 >= 9.5 && galplotdef.lat_max2 <= 10.0 ) // by convention
   {

 // |l| < 29 deg. and |b| < 10 deg

 i=0;

 E[i]=23.5; dE[i]= 3.25;data[i]= 2.7792847E-3;data_err[i]= 1.5172255E-4;sum[i]= 2.7355218E-3; powlaw[i]= 4.7973592E-4; powlawcut[i]= 2.254865E-3 ; posline[i]= 0.;positronium[i]= 9.211104E-7  ; i++;


 E[i]=31.5; dE[i]= 4.75;data[i]= 1.0941667E-3;data_err[i]= 5.395922E-5;sum[i]= 1.1218921E-3;  powlaw[i]= 3.0244016E-4; powlawcut[i]= 8.182131E-4 ; posline[i]= 0.;positronium[i]=  1.2388099E-6; i++;

 E[i]=42.75; dE[i]= 6.5;data[i]= 4.0214194E-4;data_err[i]= 2.9917202E-5;sum[i]= 3.8628105E-4; powlaw[i]= 1.8654253E-4; powlawcut[i]= 1.9805053E-4 ;posline[i]= 0.;positronium[i]= 1.687983E-6; i++;

 E[i]=57.75; dE[i]= 8.5;data[i]= 1.5955529E-4;data_err[i]= 3.7471294E-5;sum[i]= 1.4817114E-4 ;  powlaw[i]=1.157559E-4; powlawcut[i]= 3.0122752E-5; posline[i]= 0.;positronium[i]= 2.2924732E-6; i++;

 E[i]=78; dE[i]= 11.75;data[i]= 5.5350727E-5;data_err[i]= 1.4818256E-5;sum[i]= 7.76078E-5;  powlaw[i]= 7.197401E-5; powlawcut[i]= 2.5165811E-6 ; posline[i]= 0.  ;positronium[i]= 3.1172105E-6; i++;

 E[i]=105.25; dE[i]= 15.5;data[i]= 5.3139072E-5;data_err[i]= 9.308986E-6;sum[i]= 4.9087077E-5;  powlaw[i]= 4.4755903E-5 ; powlawcut[i]=9.063124E-8;posline[i]=  0. ;positronium[i]= 4.240541E-6; i++;

 E[i]=142; dE[i]= 21.25;data[i]= 3.963929E-5;data_err[i]=  7.1470876E-6;sum[i]= 3.364511E-5;  powlaw[i]= 2.7870165E-5; powlawcut[i]= 1.1575418E-9 ; posline[i]= 0.  ;positronium[i]= 5.773787E-6; i++;

 E[i]=191.75; dE[i]= 28.5 ;data[i]=2.1066997E-5 ;data_err[i]=  5.046105E-6;sum[i]= 2.5179259E-5;  powlaw[i]= 1.7318906E-5; powlawcut[i]= 3.3188115E-12  ; posline[i]= 0. ;positronium[i]= 7.860351E-6; i++;

 E[i]=259; dE[i]= 38.75;data[i]= 2.4184721E-5;data_err[i]= 2.4009791E-6;sum[i]= 2.1389209E-5;  powlaw[i]= 1.0762073E-5; powlawcut[i]= 1.4023402E-15 ; posline[i]=  0. ;positronium[i]= 1.0627137E-5; i++;

 E[i]=349.75; dE[i]= 52;data[i]= 1.9883702E-5;data_err[i]= 2.3442573E-6;sum[i]= 2.0762489E-5;  powlaw[i]= 6.687546E-6; powlawcut[i]= 4.0983031E-20  ; posline[i]= 0. ;positronium[i]= 1.4074943E-5; i++;

 E[i]=453.5; dE[i]= 51.75;data[i]= 2.2686645E-5;data_err[i]= 2.613383E-6;sum[i]= 2.3475153E-5;  powlaw[i]= 4.4046614E-6; powlawcut[i]= 5.0281994E-26 ; posline[i]=  9.427557E-13 ;positronium[i]= 1.907049E-5; i++;

 E[i]=510.75 ; dE[i]=5.5; data[i]= 1.0227116E-4;data_err[i]= 8.920262E-6;sum[i]= 1.02831276E-4; powlaw[i]= 3.6167955E-6; powlawcut[i]= 4.706408E-31  ; posline[i]= 9.9214485E-5 ;positronium[i]= 0.; i++;

 E[i]=624; dE[i]= 107.75;data[i]= 3.933635E-6;data_err[i]= 1.6498506E-6;sum[i]= 2.688704E-6;  powlaw[i]= 2.6886993E-6; powlawcut[i]= 7.457741E-33 ; posline[i]=  4.527852E-12 ;positronium[i]= 0.; i++;

 E[i]=859.75 ; dE[i]=128;data[i]= 1.9859948E-18;data_err[i]= 1.1422138E-6;sum[i]= 1.6101019E-6;  powlaw[i]= 1.6101019E-6; powlawcut[i]= 2.802597E-45 ; posline[i]=  0. ;positronium[i]= 0.; i++;

 E[i]=1160.5; dE[i]= 172.75;data[i]= 1.4714329E-18;data_err[i]= 8.578271E-7;sum[i]= 1.0013816E-6;  powlaw[i]= 1.0013816E-6; powlawcut[i]= 0. ; posline[i]= 0.  ;positronium[i]= 0.; i++;

 E[i]=1566.75; dE[i]= 233.5;data[i]= 1.2609397E-6 ;data_err[i]=6.4822285E-7;sum[i]= 6.2263643E-7;  powlaw[i]= 6.2263643E-7; powlawcut[i]= 0. ; posline[i]= 0.  ;positronium[i]= 0.;

 powlaw_fractional_error=0.4; //AWS20080128

   }// |b|<10 

 //-------------------------------------------------------------------------------------------------------------------
 if(galplotdef.lat_max2 >= 4.5 && galplotdef.lat_max2 <=  5.0 ) // by convention
   {

 // |l| < 29 deg. and |b| < 5  deg

 i=0;

 E[i]=23.5; dE[i]= 3.25;data[i]= 2.5218136E-3;data_err[i]= 1.3774505E-4;sum[i]= 2.4813124E-3; powlaw[i]= 4.273392E-4;powlawcut[i]= 2.0531576E-3;posline[i]= 0.;positronium[i]=  8.1551922E-7;i++;

 E[i]=31.5; dE[i]= 4.75;data[i]= 9.916561E-4;data_err[i]= 4.8969253E-5;sum[i]= 1.0172576E-3; powlaw[i]= 2.7045342E-4;powlawcut[i]= 7.4570736E-4 ; posline[i]= 0. ;positronium[i]=  1.0967993E-6;i++;

 E[i]=42.75; dE[i]= 6.5;data[i]= 3.6428042E-4;data_err[i]= 2.712496E-5;sum[i]= 3.497205E-4; powlaw[i]= 1.6749371E-4;powlawcut[i]= 1.8073233E-4 ; posline[i]= 0. ;positronium[i]=  1.4944817E-6;i++;

 E[i]=57.75; dE[i]= 8.5;data[i]= 1.4486842E-4;data_err[i]= 3.3927193E-5;sum[i]= 1.3391995E-4; powlaw[i]= 1.04354745E-4;powlawcut[i]= 2.7535532E-5 ; posline[i]=  0.   ;positronium[i]= 2.0296763E-6;i++;

 E[i]=78; dE[i]= 11.75;data[i]= 4.9565423E-5;data_err[i]= 1.3402864E-5;sum[i]= 7.021051E-5; powlaw[i]= 6.514513E-5;powlawcut[i]= 2.3055076E-6 ; posline[i]= 0. ;positronium[i]=  2.75987E-6;i++;

 E[i]=105.25; dE[i]= 15.5;data[i]= 4.8188107E-5;data_err[i]= 8.409422E-6;sum[i]= 4.4509797E-5; powlaw[i]= 4.0672097E-5; powlawcut[i]= 8.3276E-8 ; posline[i]= 0. ;positronium[i]=  3.754428E-6;i++;

 E[i]=142; dE[i]= 21.25;data[i]= 3.5853961E-5;data_err[i]= 6.456386E-6;sum[i]= 3.054131E-5; powlaw[i]= 2.5428333E-5;powlawcut[i]=  1.0677119E-9 ; posline[i]= 0. ;positronium[i]= 5.1119104E-6;i++;

 E[i]=191.75; dE[i]= 28.5;data[i]= 1.9035448E-5;data_err[i]= 4.636185E-6;sum[i]= 2.282429E-5; powlaw[i]= 1.5865004E-5; powlawcut[i]= 3.077344E-12  ; posline[i]= 0. ;positronium[i]= 6.9592824E-6;i++;

 E[i]=259; dE[i]= 38.75;data[i]= 2.2001086E-5;data_err[i]= 2.1926095E-6;sum[i]= 1.9307097E-5; powlaw[i]= 9.8982E-6; powlawcut[i]= 1.3094579E-15 ; posline[i]= 0. ;positronium[i]=  9.408899E-6;i++;

 E[i]=349.75; dE[i]= 52;data[i]= 1.7810587E-5;data_err[i]= 2.1374864E-6;sum[i]= 1.8636916E-5; powlaw[i]= 6.175449E-6;powlawcut[i]=  3.8634918E-20 ; posline[i]=  0. ;positronium[i]= 1.2461466E-5;i++;

 E[i]=453.5; dE[i]= 51.75 ;data[i]=2.0187708E-5;data_err[i]= 2.37161E-6;sum[i]= 2.0966194E-5; powlaw[i]= 4.081844E-6;powlawcut[i]=  4.801101E-26  ; posline[i]= 8.2308254E-13 ;positronium[i]= 1.6884349E-5;i++;

 E[i]=510.75; dE[i]= 5.5;data[i]= 8.9684814E-5;data_err[i]= 8.058698E-6;sum[i]= 8.997766E-5; powlaw[i]= 3.3574433E-6;powlawcut[i]= 4.549474E-31  ; posline[i]= 8.662022E-5 ;positronium[i]= 0.;i++;

 E[i]=624; dE[i]= 107.75;data[i]= 3.5856708E-6;data_err[i]= 1.4999318E-6;sum[i]= 2.5018825E-6; powlaw[i]= 2.5018784E-6;powlawcut[i]= 7.221854E-33  ; posline[i]= 3.953088E-12 ;positronium[i]= 0.;i++;

 E[i]=859.75; dE[i]= 128;data[i]= 1.8429782E-18 ;data_err[i]=1.0599599E-6;sum[i]= 1.5047544E-6; powlaw[i]= 1.5047544E-6;powlawcut[i]= 2.802597E-45 ; posline[i]=  0.   ;positronium[i]= 0.;i++;

 E[i]=1160.5; dE[i]= 172.75;data[i]= 1.3654713E-18;data_err[i]= 7.9605274E-7 ;sum[i]=9.3961443E-7; powlaw[i]= 9.3961443E-7;powlawcut[i]= 0. ; posline[i]= 0.  ;positronium[i]= 0.;i++;

 E[i]=1566.75; dE[i]= 233.5;data[i]= 1.1701361E-6;data_err[i]= 6.015427E-7;sum[i]= 5.8657474E-7; powlaw[i]= 5.8657474E-7;powlawcut[i]= 0. ; posline[i]= 0. ;positronium[i]= 0.;


 powlaw_fractional_error=0.4; //AWS20080128

  }// |b|<5

   }// mode==2



 //---------------------------------------------------------------------
 // IC model fits from Laurent Bouchet 20080423
 // "points below 50 keV are still DIRBE fits because IC is not finished yet"
 /*
NB here the powlawcut comes before the powlaw, unlike previous data !!
                                                        VVVVVVVVVVVVVVVVVVVVVVVV  
E          dE    data         dataerr      sum          powlawcut        powlaw    posline        positronium   
23.5      3.25 2.882167E-3   1.5739635E-4 2.8848121E-3 2.2058072E-3  6.774342E-4   0.E+-         1.5705459E-6
31.5      4.75 1.1338238E-3  5.5962973E-5 1.1312013E-3 6.8680365E-4  4.4228538E-4- 0.E+          2.1122416E-6
42.75     6.5  4.165805E-4   3.100912E-5  4.2019083E-4 1.3430107E-4  2.8301164E-4  0.E+-         2.8781073E-6
57.75     8.5  2.790389E-4   6.925635E-5  2.0142592E-4 1.5397312E-5  1.8211981E-4  0.E+-         3.9087976E-6
78       11.75 8.1915575E-5  2.516221E-5  1.2361034E-4 8.92458E-7    1.1740287E-4  0.E+-         5.315022E-6
105.25   15.5  8.823631E-5   1.650257E-5  8.294477E-5  1.9643725E-8  7.569476E-5   0.E+-         7.2303646E-6
142      21.25 6.582793E-5   1.3128659E-5 5.8709807E-5 1.3200194E-10 4.8865036E-5- 0.E+          9.844636E-6
191.75   28.5  4.4218253E-5  1.0790037E-5 4.4887423E-5 1.5791921E-13 3.1485076E-5- 0.E+          1.3402348E-5
259      38.75 4.5178876E-5  4.807014E-6  3.840597E-5  2.0707158E-17 2.0286094E-5  0.E+-         1.8119877E-5
349.75   52    3.621738E-5   4.6769087E-6 3.7069177E-5 1.2334454E-22 1.3070595E-5- 0.E+          2.3998582E-5
453.5    51.75 3.9758437E-5  4.7947064E-6 4.1405397E-5 1.7906155E-29 8.889122E-6-  1.211467E-12  3.2516272E-5
510.75    5.5  1.2871732E-4  1.4045068E-5 1.3490557E-4 2.1292822E-35 7.412227E-6-  1.2749335E-4  0.E+
624     107.75 7.4898694E-6  2.8286715E-6 5.630931E-6  2.5333E-37    5.630925E-6-  5.8184147E-12 0.E+
859.75  128    2.717576E-6   2.0266306E-6 3.5070033E-6 0.E+          3.5070033E-6  0.E+          0.E+
1160.5  172.75 2.5679096E-6  1.4922714E-6 2.2613944E-6 0.E+          2.2613944E-6  0.E+-         0.E+
1566.75 233.5  5.3222046E-19 9.911821E-7  1.4578501E-6 0.E+          1.4578501E-6  0.E+-         0.E+
 */

   if(mode==3)       //AWS20080425
   {
   n_energy=16;
   E    =      new double[n_energy   ];
  dE    =      new double[n_energy   ];
   Emin =      new double[n_energy   ];
   Emax =      new double[n_energy   ];
   data       =new double[n_energy   ];
   data_err   =new double[n_energy   ];
   sum        =new double[n_energy   ];
   powlaw     =new double[n_energy   ];
   powlawcut  =new double[n_energy   ];
   posline    =new double[n_energy   ];
   positronium=new double[n_energy   ];


 if(galplotdef.lat_max2 >=14.5 && galplotdef.lat_max2 <= 15.0 ) // by convention
   {

 // |l| < 29 deg. and |b| < 15 deg

   i=0;

   //                                                                                                      V -- note order is different from mode=3 --  V
   E[i]=23.5;   dE[i]=   3.25; data[i]= 2.882167E-3;     data_err[i]= 1.5739635E-4; sum[i]= 2.8848121E-3;  powlawcut[i]= 2.2058072E-3;   powlaw[i]= 6.774342E-4;    posline[i]= 0.  ;positronium[i]= 1.5705459E-6; i++;

   E[i]=31.5;   dE[i]=   4.75; data[i]= 1.1338238E-3;    data_err[i]= 5.5962973E-5; sum[i]= 1.1312013E-3;  powlawcut[i]= 6.8680365E-4;   powlaw[i]= 4.4228538E-4;   posline[i]=  0. ;positronium[i]= 2.1122416E-6; i++;

   E[i]=42.75;  dE[i]=   6.5;  data[i]= 4.165805E-4;     data_err[i]= 3.100912E-5;  sum[i]= 4.2019083E-4;  powlawcut[i]= 1.3430107E-4;   powlaw[i]= 2.8301164E-4;   posline[i]= 0.  ;positronium[i]= 2.8781073E-6; i++;

   E[i]=57.75;  dE[i]=   8.5;  data[i]= 2.790389E-4;     data_err[i]= 6.925635E-5;  sum[i]= 2.0142592E-4;  powlawcut[i]= 1.5397312E-5;   powlaw[i]= 1.8211981E-4;   posline[i]= 0.  ;positronium[i]= 3.9087976E-6; i++;

   E[i]=78;     dE[i]=  11.75; data[i]= 8.1915575E-5;    data_err[i]= 2.516221E-5;  sum[i]= 1.2361034E-4;  powlawcut[i]= 8.92458E-7;     powlaw[i]= 1.1740287E-4;   posline[i]= 0.  ;positronium[i]= 5.315022E-6; i++;

   E[i]=105.25; dE[i]=  15.5;  data[i]= 8.823631E-5;     data_err[i]= 1.650257E-5;  sum[i]= 8.294477E-5;   powlawcut[i]= 1.9643725E-8;   powlaw[i]= 7.569476E-5;    posline[i]= 0.  ;positronium[i]= 7.2303646E-6; i++;

   E[i]=142;    dE[i]=  21.25; data[i]= 6.582793E-5;     data_err[i]= 1.3128659E-5; sum[i]= 5.8709807E-5;  powlawcut[i]= 1.3200194E-10;  powlaw[i]= 4.8865036E-5 ;  posline[i]= 0.  ;positronium[i]=9.844636E-6; i++;

   E[i]=191.75; dE[i]=  28.5;  data[i]= 4.4218253E-5;    data_err[i]= 1.0790037E-5; sum[i]= 4.4887423E-5;  powlawcut[i]= 1.5791921E-13;  powlaw[i]= 3.1485076E-5 ;  posline[i]= 0.  ;positronium[i]=1.3402348E-5; i++;

   E[i]=259;    dE[i]=  38.75; data[i]= 4.5178876E-5;    data_err[i]= 4.807014E-6;  sum[i]= 3.840597E-5;   powlawcut[i]= 2.0707158E-17;  powlaw[i]= 2.0286094E-5;   posline[i]= 0.  ;positronium[i]= 1.8119877E-5; i++;

   E[i]=349.75 ;dE[i]=  52;    data[i]= 3.621738E-5;     data_err[i]= 4.6769087E-6; sum[i]= 3.7069177E-5;  powlawcut[i]= 1.2334454E-22;  powlaw[i]= 1.3070595E-5 ;  posline[i]= 0.  ;positronium[i]=2.3998582E-5; i++;

   E[i]=453.5 ; dE[i]=  51.75; data[i]= 3.9758437E-5;    data_err[i]= 4.7947064E-6; sum[i]= 4.1405397E-5;  powlawcut[i]= 1.7906155E-29;  powlaw[i]= 8.889122E-6 ;   posline[i]= 1.211467E-12  ;positronium[i]=3.2516272E-5; i++;

   E[i]=510.75 ;dE[i]=   5.5;  data[i]= 1.2871732E-4;    data_err[i]= 1.4045068E-5; sum[i]= 1.3490557E-4;  powlawcut[i]= 2.1292822E-35;  powlaw[i]= 7.412227E-6 ;   posline[i]= 1.2749335E-4 ;positronium[i]= 0.; i++;

   E[i]=624;    dE[i]= 107.75; data[i]= 7.4898694E-6;    data_err[i]= 2.8286715E-6; sum[i]= 5.630931E-6;   powlawcut[i]= 2.5333E-37;     powlaw[i]= 5.630925E-6 ;   posline[i]= 5.8184147E-12 ;positronium[i]= 0.; i++;

   E[i]=859.75; dE[i]= 128;    data[i]= 2.717576E-6;     data_err[i]= 2.0266306E-6; sum[i]= 3.5070033E-6;  powlawcut[i]= 0. ;            powlaw[i]=3.5070033E-6;    posline[i]= 0.   ;positronium[i]=  0.; i++;
																														  E[i]=1160.5; dE[i]= 172.75; data[i]= 2.5679096E-6;    data_err[i]= 1.4922714E-6; sum[i]= 2.2613944E-6 ; powlawcut[i]= 0. ;            powlaw[i]=2.2613944E-6;    posline[i]= 0.   ;positronium[i]=  0.; i++;
																														  E[i]=1566.75;dE[i]= 233.5;  data[i]= 5.3222046E-19;   data_err[i]= 9.911821E-7;  sum[i]= 1.4578501E-6;  powlawcut[i]= 0. ;            powlaw[i]=1.4578501E-6;    posline[i]= 0.   ;positronium[i]=  0.; i++;

  powlaw_fractional_error=0.4; //AWS20080128 based on summing latitude profiles for model and pixel map

   } //|b|<15

   }// mode==3


 //---------------------------------------------------------------------
 // IC+DIRBE model fits from Laurent Bouchet 20080430
 // powerlaw index fixed from 50-2000 keV IC model
 // bouchet_ics_dirbe_gauss_20080430.plt
 /*
NB here the powlawcut comes before the powlaw, unlike some previous data !!
                                                        VVVVVVVVVVVVVVVVVVVVVVVV  
E          dE    data         dataerr      sum          powlawcut        powlaw    posline        positronium   
23.5 3.25     6.522181E-3    2.6792517E-4 6.34005E-3   5.3468192E-3  9.919447E-4 0.E+-  1.2865211E-6
31.5 4.75     1.9074777E-3   8.981346E-5  2.0409172E-3 1.4168734E-3  6.2231364E-4 0.E+-  1.7302541E-6
42.75 6.5     7.066105E-4    4.7395962E-5 6.055416E-4  2.2130545E-4  3.8187851E-4 0.E+-  2.3576174E-6
57.75 8.5     2.3666974E-4   1.0425827E-4 2.578101E-4  1.88362E-5    2.3577201E-4 0.E+-  3.201913E-6
78 11.75      9.711023E-5    3.888287E-5  1.5095845E-4 7.4313726E-7  1.4586149E-4 0.E+-  4.353829E-6
105.25 15.5   8.0756064E-5 2.6295284E-5 9.617842E-5  9.7227E-9     9.02459E-5 0.E+-  5.9227922E-6
142 21.25     5.7498644E-5 2.1937738E-5   6.3980674E-5 3.3088955E-11 5.5916356E-5-  0.E+ 8.064288E-6
191.75 28.5   3.805418E-5  1.941314E-5  4.5551063E-5 1.5639516E-14 3.457246E-5- 0.E+ 1.0978605E-5
259 38.75     3.444193E-5    9.638595E-6  3.6218552E-5 5.907879E-19  2.1375556E-5 0.E+- 1.48429944E-5
349.75 52     3.1377774E-5   9.398254E-6  3.287453E-5  6.48097E-25   1.3215962E-5 0.E+- 1.9658568E-5
453.5 51.75   3.969499E-5    1.0063935E-5 3.5301614E-5 9.71592E-33   8.665732E-6- 1.0624506E-12 2.6635882E-5
510.75 5.5    1.1766602E-4   3.110548E-5  1.18911455E-4 0.E+         7.100431E-6  1.1181102E-4 0.E+
624 107.75    5.1874717E-6   6.4775936E-6 5.2625705E-6  0.E+         5.2625655E-6- 5.1027203E-12 0.E+
859.75 128    2.2997323E-6   5.69548E-6   3.134254E-6   0.E+         3.134254E-6 0.E+ 0.E+
1160.5 172.75 2.5575368E-6   4.320181E-6  1.9395251E-6  0.E+         1.9395251E-6 0.E+  0.E+
1566.75 233.5 1.1462394E-6   3.1575068E-6 1.199898E-6   0.E+         1.199898E-6  0.E+ 0.E+
 */

   if(mode==4)       //AWS20080430
   {
   n_energy=16;
   E    =      new double[n_energy   ];
  dE    =      new double[n_energy   ];
   Emin =      new double[n_energy   ];
   Emax =      new double[n_energy   ];
   data       =new double[n_energy   ];
   data_err   =new double[n_energy   ];
   sum        =new double[n_energy   ];
   powlaw     =new double[n_energy   ];
   powlawcut  =new double[n_energy   ];
   posline    =new double[n_energy   ];
   positronium=new double[n_energy   ];


 if(galplotdef.lat_max2 >=14.5 && galplotdef.lat_max2 <= 15.0 ) // by convention
   {

 // |l| < 29 deg. and |b| < 15 deg

   i=0;
  
E[i]=23.5;   dE[i]=  3.25 ;  data[i]=    6.522181E-3  ;  data_err[i]=  2.6792517E-4; sum[i]= 6.34005E-3;      powlawcut[i]=   5.3468192E-3 ;powlaw[i]= 9.919447E-4;       posline[i]= 0. ;positronium[i]= 1.2865211E-6; i++;
E[i]=31.5;   dE[i]=  4.75 ;  data[i]=    1.9074777E-3 ;  data_err[i]=  8.981346E-5 ; sum[i]= 2.0409172E-3;    powlawcut[i]= 1.4168734E-3;   powlaw[i]=  6.2231364E-4;     posline[i]= 0. ;positronium[i]= 1.7302541E-6; i++;
E[i]=42.75;  dE[i]=  6.5 ;   data[i]=    7.066105E-4  ;  data_err[i]=  4.7395962E-5; sum[i]= 6.055416E-4;     powlawcut[i]=  2.2130545E-4;  powlaw[i]=  3.8187851E-4;     posline[i]= 0. ;positronium[i]= 2.3576174E-6; i++;
E[i]=57.75;  dE[i]=  8.5 ;   data[i]=    2.3666974E-4;   data_err[i]=   1.0425827E-4;sum[i]= 2.578101E-4 ;    powlawcut[i]= 1.88362E-5  ;   powlaw[i]=  2.3577201E-4;     posline[i]= 0. ;positronium[i]=  3.201913E-6; i++;
E[i]=78;     dE[i]=  11.75  ;data[i]=    9.711023E-5  ;  data_err[i]=  3.888287E-5;  sum[i]=  1.5095845E-4;   powlawcut[i]= 7.4313726E-7 ;  powlaw[i]= 1.4586149E-4;      posline[i]= 0. ;positronium[i]=  4.353829E-6; i++;
E[i]=105.25; dE[i]=  15.5 ;  data[i]=     8.0756064E-5;  data_err[i]= 2.6295284E-5;  sum[i]= 9.617842E-5 ;    powlawcut[i]= 9.7227E-9   ;   powlaw[i]=  9.02459E-5;       posline[i]= 0. ;positronium[i]= 5.9227922E-6; i++;
E[i]=142;    dE[i]=  21.25  ;data[i]=   5.7498644E-5;    data_err[i]= 2.1937738E-5;  sum[i]=   6.3980674E-5;  powlawcut[i]= 3.3088955E-11;  powlaw[i]= 5.5916356E-5;      posline[i]= 0.; positronium[i]= 8.064288E-6; i++;
E[i]=191.75; dE[i]=  28.5 ;   data[i]=    3.805418E-5 ;   data_err[i]= 1.941314E-5;   sum[i]=  4.5551063E-5;   powlawcut[i]= 1.5639516E-14;  powlaw[i]= 3.457246E-5;       posline[i]= 0. ;positronium[i]= 1.0978605E-5; i++;
E[i]=259;    dE[i]=  38.75  ;data[i]=   3.444193E-5   ;  data_err[i]= 9.638595E-6;   sum[i]=  3.6218552E-5;   powlawcut[i]= 5.907879E-19 ;  powlaw[i]= 2.1375556E-5;      posline[i]= 0. ;positronium[i]= 1.48429944E-5; i++;
E[i]=349.75; dE[i]=  52  ;   data[i]=   3.1377774E-5  ;  data_err[i]= 9.398254E-6;   sum[i]=  3.287453E-5 ;   powlawcut[i]= 6.48097E-25 ;   powlaw[i]=  1.3215962E-5;     posline[i]= 0. ;positronium[i]= 1.9658568E-5; i++;
E[i]=453.5;  dE[i]=  51.75 ; data[i]=  3.969499E-5  ;    data_err[i]=  1.0063935E-5; sum[i]= 3.5301614E-5;    powlawcut[i]= 9.71592E-33 ;   powlaw[i]=  8.665732E-6 ;     posline[i]= 1.0624506E-12 ;positronium[i]= 2.6635882E-5; i++;
E[i]=510.75; dE[i]=  5.5  ;  data[i]=  1.1766602E-4 ;    data_err[i]=  3.110548E-5;  sum[i]=  1.18911455E-4;  powlawcut[i]= 0.  ;           powlaw[i]=       7.100431E-6 ;posline[i]= 1.1181102E-4 ;positronium[i]= 0.; i++;
E[i]=624;    dE[i]=  107.75 ;data[i]=   5.1874717E-6 ;   data_err[i]=  6.4775936E-6; sum[i]= 5.2625705E-6 ;   powlawcut[i]= 0.   ;          powlaw[i]=      5.2625655E-6; posline[i]= 5.1027203E-12 ;positronium[i]= 0.; i++;
E[i]=859.75; dE[i]=  128  ;  data[i]=  2.2997323E-6 ;    data_err[i]=  5.69548E-6;   sum[i]=   3.134254E-6  ; powlawcut[i]= 0.  ;           powlaw[i]=       3.134254E-6; posline[i]= 0.  ; positronium[i]=0.; i++;
E[i]=1160.5; dE[i]=  172.75; data[i]= 2.5575368E-6 ;     data_err[i]=  4.320181E-6;  sum[i]=  1.9395251E-6 ;  powlawcut[i]= 0.   ;          powlaw[i]=      1.9395251E-6 ;posline[i]= 0.  ;  positronium[i]= 0.; i++;
E[i]=1566.75;dE[i]=  233.5;  data[i]= 1.1462394E-6  ;    data_err[i]= 3.1575068E-6 ; sum[i]= 1.199898E-6 ;    powlawcut[i]=  0.     ;       powlaw[i]=    1.199898E-6 ;   posline[i]= 0. ;positronium[i]= 0.;
   }// |l| < 29 deg. and |b| < 15 deg
   }// mode==4


 //---------------------------------------------------------------------
 // IC+DIRBE model fits from Laurent Bouchet 20080430
 // IC component only (but both are fitted)
 //bouchet_icsonly_prem.plt- preliminary, not completed below 50 keV 
   /*
E    dE       data        err        IC model
23.5 3.25 6.4799105E-3 2.6785471E-4 6.120322E-4
31.5 4.75 1.860022E-3 8.966373E-5 3.7017825E-4
42.75 6.5 6.8374536E-4 4.7134686E-5 2.1859139E-4
57.75 8.5 1.1196197E-4 8.99918E-5 1.2992276E-4
78 11.75 7.2929986E-5 3.2348016E-5 7.739777E-5
105.25 15.5 4.7436027E-5 2.2323218E-5 4.6108423E-5
142 21.25 3.0139976E-5 1.8809065E-5 2.7512719E-5
191.75 28.5 1.913493E-5 1.6767519E-5 1.6378644E-5
259 38.75 9.762878E-6 8.243769E-6 9.7505135E-6
349.75 52 1.4728652E-5 8.040868E-6 5.804455E-6
453.5 51.75 2.9630117E-5 8.4865405E-6 3.6803471E-6
510.75 5.5 2.0337253E-5 2.5102017E-5 2.9672255E-6
624 107.75 1.0945255E-18 5.3590242E-6 2.1502878E-6
859.75 128 9.38896E-19 4.8140395E-6 1.2290986E-6
1160.5 172.75 2.557537E-6 3.6311041E-6 7.323774E-7
1566.75 233.5 5.322205E-19 2.5527756E-6 4.3627755E-7
    */
   if(mode==5)       //AWS20080505
   {
   n_energy=16;
   E    =      new double[n_energy   ];
  dE    =      new double[n_energy   ];
   Emin =      new double[n_energy   ];
   Emax =      new double[n_energy   ];
   data       =new double[n_energy   ];
   data_err   =new double[n_energy   ];
   sum        =new double[n_energy   ];
   powlaw     =new double[n_energy   ];
   powlawcut  =new double[n_energy   ];
   posline    =new double[n_energy   ];
   positronium=new double[n_energy   ];


 if(galplotdef.lat_max2 >=14.5 && galplotdef.lat_max2 <= 15.0 ) // by convention
   {

 // |l| < 29 deg. and |b| < 15 deg

   i=0;
  

E[i]= 23.5;     dE[i]=  3.25;    data[i]=  6.4799105E-3 ;  data_err[i]= 2.6785471E-4 ;powlaw[i]= 6.120322E-4; i++;
E[i]= 31.5;     dE[i]=  4.75;    data[i]=  1.860022E-3 ;   data_err[i]= 8.966373E-5 ; powlaw[i]= 3.7017825E-4; i++;
E[i]= 42.75;    dE[i]=  6.5;     data[i]=  6.8374536E-4 ;  data_err[i]= 4.7134686E-5 ;powlaw[i]= 2.1859139E-4; i++;
E[i]= 57.75;    dE[i]=  8.5;     data[i]=  1.1196197E-4 ;  data_err[i]= 8.99918E-5 ;  powlaw[i]= 1.2992276E-4; i++;
E[i]= 78;       dE[i]=  11.75;   data[i]=  7.2929986E-5 ;  data_err[i]= 3.2348016E-5 ;powlaw[i]= 7.739777E-5; i++;
E[i]= 105.25;   dE[i]=  15.5;    data[i]=  4.7436027E-5 ;  data_err[i]= 2.2323218E-5 ;powlaw[i]= 4.6108423E-5; i++;
E[i]= 142;      dE[i]=  21.25;   data[i]=  3.0139976E-5 ;  data_err[i]= 1.8809065E-5 ;powlaw[i]= 2.7512719E-5; i++;
E[i]= 191.75;   dE[i]=  28.5;    data[i]=  1.913493E-5 ;   data_err[i]= 1.6767519E-5 ;powlaw[i]= 1.6378644E-5; i++;
E[i]= 259;      dE[i]=  38.75;   data[i]=  9.762878E-6 ;   data_err[i]= 8.243769E-6 ; powlaw[i]= 9.7505135E-6; i++;
E[i]= 349.75;   dE[i]=  52;      data[i]=  1.4728652E-5 ;  data_err[i]= 8.040868E-6 ; powlaw[i]= 5.804455E-6; i++;
E[i]= 453.5;    dE[i]=  51.75;   data[i]=  2.9630117E-5 ;  data_err[i]= 8.4865405E-6 ;powlaw[i]= 3.6803471E-6; i++;
E[i]= 510.75;   dE[i]=  5.5;     data[i]=  2.0337253E-5 ;  data_err[i]= 2.5102017E-5 ;powlaw[i]= 2.9672255E-6; i++;
E[i]= 624;      dE[i]=  107.75;  data[i]=  1.0945255E-18 ; data_err[i]= 5.3590242E-6 ;powlaw[i]= 2.1502878E-6; i++;
E[i]= 859.75;   dE[i]=  128;     data[i]=  9.38896E-19 ;   data_err[i]= 4.8140395E-6 ;powlaw[i]= 1.2290986E-6; i++;
E[i]= 1160.5;   dE[i]=  172.75;  data[i]=  2.557537E-6 ;   data_err[i]= 3.6311041E-6 ;powlaw[i]= 7.323774E-7; i++;
E[i]= 1566.75;  dE[i]=  233.5;   data[i]=  5.322205E-19 ;  data_err[i]= 2.5527756E-6 ;powlaw[i]= 4.3627755E-7;


   } // if l,b
   } // mode==5


 //---------------------------------------------------------------------
 // IC+DIRBE model fits from Laurent Bouchet 20080430
 // DIRBE component only (but both are fitted)
 //bouchet_dirbe_only_prem.plt- preliminary, not completed below 50 keV 
   /*
E    dE       data        err        DIRBE model
23.5 3.25 0.E+ 0.15384614 1.5726083E-4
31.5 4.75 0.E+ 0.10526316 1.1477965E-4
42.75 6.5 0.E+ 0.07692308 8.255883E-5
57.75 8.5 1.2470748E-4 5.1882158E-5 5.963861E-5
78 11.75 1.02658705E-5 2.1198075E-5 4.3130272E-5
105.25 15.5 3.31449E-5 1.3564487E-5 3.1199524E-5
142 21.25 2.5155907E-5 1.1052187E-5 2.2587255E-5
191.75 28.5 1.4241404E-5 9.545028E-6 1.633054E-5
259 38.75 2.1525175E-5 4.8261117E-6 1.1806191E-5
349.75 52 9.683967E-6 4.6808572E-6 8.535698E-6
453.5 51.75 5.0122707E-18 5.1468086E-6 6.4262185E-6
510.75 5.5 2.5982188E-5 1.730102E-5 5.624871E-6
624 107.75 5.1874717E-6 3.4902068E-6 4.582639E-6
859.75 128 2.2997323E-6 3.0436026E-6 3.232924E-6
1160.5 172.75 1.501505E-18 2.3407365E-6 2.3386735E-6
1566.75 233.5 1.1462394E-6 1.8582746E-6 1.6914746E-6
   */
  if(mode==6)       //AWS20080505
   {
   n_energy=16;
   E    =      new double[n_energy   ];
  dE    =      new double[n_energy   ];
   Emin =      new double[n_energy   ];
   Emax =      new double[n_energy   ];
   data       =new double[n_energy   ];
   data_err   =new double[n_energy   ];
   sum        =new double[n_energy   ];
   powlaw     =new double[n_energy   ];
   powlawcut  =new double[n_energy   ];
   posline    =new double[n_energy   ];
   positronium=new double[n_energy   ];


 if(galplotdef.lat_max2 >=14.5 && galplotdef.lat_max2 <= 15.0 ) // by convention
   {

 // |l| < 29 deg. and |b| < 15 deg

   i=0;
  


E[i]= 23.55 ;  dE[i]= 3.25 ;  data[i]= 0.   ;         data_err[i]=0.15384614;   powlaw[i]= 1.5726083E-4; i++;
E[i]= 31.55 ;  dE[i]= 4.75 ;  data[i]= 0.   ;         data_err[i]=0.10526316;   powlaw[i]= 1.1477965E-4; i++;
E[i]= 42.755 ; dE[i]= 6.5 ;   data[i]= 0.   ;         data_err[i]=0.07692308;   powlaw[i]= 8.255883E-5; i++;
E[i]= 57.755 ; dE[i]= 8.5 ;   data[i]= 1.2470748E-4;  data_err[i]= 5.1882158E-5;powlaw[i]= 5.963861E-5; i++;
E[i]= 785 ;    dE[i]= 11.75 ; data[i]= 1.02658705E-5; data_err[i]= 2.1198075E-5;powlaw[i]= 4.3130272E-5; i++;
E[i]= 105.255 ;dE[i]= 15.5 ;  data[i]= 3.31449E-5;    data_err[i]= 1.3564487E-5;powlaw[i]= 3.1199524E-5; i++;
E[i]= 1425 ;   dE[i]= 21.25 ; data[i]= 2.5155907E-5;  data_err[i]= 1.1052187E-5;powlaw[i]= 2.2587255E-5; i++;
E[i]= 191.755 ;dE[i]= 28.5 ;  data[i]= 1.4241404E-5;  data_err[i]= 9.545028E-6; powlaw[i]= 1.633054E-5; i++;
E[i]= 2595 ;   dE[i]= 38.75 ; data[i]= 2.1525175E-5;  data_err[i]= 4.8261117E-6;powlaw[i]= 1.1806191E-5; i++;
E[i]= 349.755 ;dE[i]= 52 ;    data[i]= 9.683967E-6;   data_err[i]= 4.6808572E-6;powlaw[i]= 8.535698E-6; i++;
E[i]= 453.55 ; dE[i]= 51.75 ; data[i]= 5.0122707E-18; data_err[i]= 5.1468086E-6;powlaw[i]= 6.4262185E-6; i++;
E[i]= 510.755 ;dE[i]= 5.5 ;   data[i]= 2.5982188E-5;  data_err[i]= 1.730102E-5; powlaw[i]= 5.624871E-6; i++;
E[i]= 6245 ;   dE[i]= 107.75 ;data[i]= 5.1874717E-6;  data_err[i]= 3.4902068E-6;powlaw[i]= 4.582639E-6; i++;
E[i]= 859.755 ;dE[i]= 128 ;   data[i]= 2.2997323E-6 ; data_err[i]=3.0436026E-6; powlaw[i]= 3.232924E-6; i++;
E[i]= 1160.55 ;dE[i]= 172.75 ;data[i]= 1.501505E-18;  data_err[i]= 2.3407365E-6;powlaw[i]= 2.3386735E-6; i++;
E[i]= 1566.755;dE[i]= 233.5  ;data[i]=1.1462394E-6;   data_err[i]= 1.8582746E-6;powlaw[i]= 1.6914746E-6;
   } // if l,b
   } // mode==6
 //--------------------------------------------------------------------------------------------------------
 //----------------------------------------------------------------------------------------------------------

   if(mode==2||mode==3||mode==4||mode==5||mode==6) //AWS20080430
   {

 for(i=0;i<n_energy;i++)
   {
     cout<<"E="<< E[i]<<" data= "<<data[i]<<" data_err= "<<data_err[i]
         <<" sum= "<<sum[i] <<" powlaw="<< powlaw[i]<<" powlawcut="<< powlawcut[i] 
         <<" posline= "<<posline[i]<<" positronium="<<positronium[i]<<endl;


   spectrum=new TGraph(2);
   spectrum   ->SetPoint(0,E[i]*1.e-3, (data[i]-data_err[i])*1.e3/solid_angle * pow(E[i]*1.e-3  ,2) ); //keV-> MeV
   spectrum   ->SetPoint(1,E[i]*1.e-3, (data[i]+data_err[i])*1.e3/solid_angle * pow(E[i]*1.e-3  ,2) ); //keV-> MeV

   spectrum->SetMarkerColor(kCyan   );// 
   spectrum->SetMarkerStyle(22); // triangles
   spectrum->SetMarkerSize (0.5);//
   spectrum->SetLineColor(kCyan);// kCyan kMagenta
   spectrum->SetLineWidth(2     );
   spectrum->SetLineStyle(1      );//1=solid 2=dash 3=dot 4=dash-dot
   //spectrum->Draw("PL");  // points as markers + line 
   //    spectrum->Draw("L"); // don't plot data points AWS20080122   
}   

  //----------------------------------

spectrum=new TGraph(n_energy);
for(i=0;i<n_energy;i++)    spectrum   ->SetPoint(i,E[i]*1.e-3, sum[i]*1.e3/solid_angle * pow(E[i]*1.e-3  ,2) ); //keV-> MeV
    
   spectrum->SetLineColor(kMagenta);// kCyan kMagenta
   spectrum->SetLineWidth(1     );
   spectrum->SetLineStyle(1      );//1=solid 2=dash 3=dot 4=dash-dot
   spectrum->Draw("L");   

   //----------------------------------

   if(0==1) // plot power-law as lines
   {

   spectrum=new TGraph(n_energy);
   for(i=0;i<n_energy;i++)    spectrum   ->SetPoint(i,E[i]*1.e-3, powlaw[i]*1.e3/solid_angle * pow(E[i]*1.e-3  ,2) ); //keV-> MeV
    
   spectrum->SetLineColor(kMagenta);// kCyan kMagenta
   spectrum->SetLineWidth(3     );
   spectrum->SetLineStyle(1      );//1=solid 2=dash 3=dot 4=dash-dot
   spectrum->Draw("L");  

   spectrum=new TGraph(n_energy); //AWS20080128
   for(i=0;i<n_energy;i++)    spectrum   ->SetPoint(i,E[i]*1.e-3, powlaw[i]*1.e3/solid_angle * pow(E[i]*1.e-3  ,2) * (1+powlaw_fractional_error)  ); // lower bound
    
   spectrum->SetLineColor(kMagenta);// kCyan kMagenta
   spectrum->SetLineWidth(2     );
   spectrum->SetLineStyle(1      );//1=solid 2=dash 3=dot 4=dash-dot
   spectrum->Draw("L");  

   spectrum=new TGraph(n_energy); //AWS20080128
   for(i=0;i<n_energy;i++)    spectrum   ->SetPoint(i,E[i]*1.e-3, powlaw[i]*1.e3/solid_angle * pow(E[i]*1.e-3  ,2) / (1+powlaw_fractional_error)  ); // upper bound
    
   spectrum->SetLineColor(kMagenta);// kCyan kMagenta
   spectrum->SetLineWidth(2     );
   spectrum->SetLineStyle(1      );//1=solid 2=dash 3=dot 4=dash-dot
   spectrum->Draw("L");  
   }//   if(0==1) // plot power-law as lines





   // power-law error box representation
   // positive error only since based on difference CO/pixel-map

   spectrum=new TGraph(5       ); //AWS20080128
   spectrum   ->SetPoint(0,E[0]         *1.e-3, powlaw[0]         *1.e3/solid_angle * pow(E[0]         *1.e-3  ,2)                                );
   spectrum   ->SetPoint(1,E[0]         *1.e-3, powlaw[0]         *1.e3/solid_angle * pow(E[0]         *1.e-3  ,2) * (1+powlaw_fractional_error)  );
   spectrum   ->SetPoint(2,E[n_energy-1]*1.e-3, powlaw[n_energy-1]*1.e3/solid_angle * pow(E[n_energy-1]*1.e-3  ,2) * (1+powlaw_fractional_error)  );
   spectrum   ->SetPoint(3,E[n_energy-1]*1.e-3, powlaw[n_energy-1]*1.e3/solid_angle * pow(E[n_energy-1]*1.e-3  ,2)                                );
   spectrum   ->SetPoint(4,E[0]         *1.e-3, powlaw[0]         *1.e3/solid_angle * pow(E[0]         *1.e-3  ,2)                                );
  
   int color;
   color=kMagenta;// kCyan kMagenta
   if(mode==5)color=kGreen; // IC    component separately in this case
   if(mode==6)color=kCyan ; // DIRBE component separately in this case
   spectrum->SetLineColor(color);// kCyan kMagenta
   spectrum->SetLineWidth(2     );
   spectrum->SetLineStyle(1      );//1=solid 2=dash 3=dot 4=dash-dot
   spectrum->SetFillColor(color);// AWS2008031
   spectrum->SetFillStyle(3245    );// AWS2008031 3=hash 2=spacing   4=135deg 5=dummy see TAttFill documentation
   spectrum->Draw("LF"); // L=line F=fill area
  
//----------------------------------


   spectrum=new TGraph(n_energy);
   for(i=0;i<n_energy;i++)    spectrum   ->SetPoint(i,E[i]*1.e-3, powlawcut[i]*1.e3/solid_angle * pow(E[i]*1.e-3  ,2) ); //keV-> MeV
    
   spectrum->SetLineColor(kMagenta);// kCyan kMagenta
   spectrum->SetLineWidth(1     );
   spectrum->SetLineStyle(3      );//1=solid 2=dash 3=dot 4=dash-dot
   spectrum->Draw("L");  

  //----------------------------------

   spectrum=new TGraph(n_energy);
   for(i=0;i<n_energy;i++)    spectrum   ->SetPoint(i,E[i]*1.e-3,positronium[i]*1.e3/solid_angle * pow(E[i]*1.e-3  ,2) ); //keV-> MeV
    
   spectrum->SetLineColor(kMagenta );// kCyan kMagenta
   spectrum->SetLineWidth(1     );
   spectrum->SetLineStyle(2      );//1=solid 2=dash 3=dot 4=dash-dot
   spectrum->Draw("L"); 


   }// mode==2||mode==3||mode==4...


   ///////////////////////////////////////////////////////////////////////////////////////////////////////////////  2010 new analysis

   /*
Bouchet 2010

 made_allspectrum_composite_radians.dat

Subject:   	Re: Update diffuse spectrum
From:   	"L. Bouchet CESR 61556637" <laurent.bouchet@cesr.fr>
Date:   	Thu, April 22, 2010 16:06
To:   	aws@mpe.mpg.de
Priority:   	Normal
Options:   	View Full Header |  View Printable Version  | Download this as a file

Here are the spectrum part for the central region spectrum :

Description
4 components
sum of all all component (gaussian+ics+dirbe)
emin,emax,flux,error,dummy
gaussian
emin,emax,flux,error,dummy
ics
emin,emax,flux,error,dummy
dirbe
emin,emax,flux,error,dummy

flux and error in photons.cm^-2.s^-1

I you need, I can produce the same for the whole sky

Attachments:
made_allspectrum_composite_radians.dat 


"G3.2m0.6p0.0_G11.8m0.6p0.0" & "KRIVOS" & "ICS" &  : total spectrum
"G3.2m0.6p0.0_G11.8m0.6p0.0" : 2 gaussians for 511 keV and positronium, here the
spectrum start above 120 keV, below
it is too weak realtive to the orther parts. 
Kriovos : is Dirbe from Krivonos et al. 
ICS : IC

Normallly : Krivos + ICS + G3.2m0.6p0.0_G11.8m0.6p0.0" = total spectrum

Total spectrum
"G3.2m0.6p0.0_G11.8m0.6p0.0" & "KRIVOS" & "ICS" & 
       20.250000       26.750000     0.044115941    0.0023689938       1.0000000
       26.750000       36.250000     0.023739122    0.0010164225       1.0000000
       36.250000       49.250000     0.013082641   0.00071878194       1.0000000
       49.250000       66.250000    0.0027070601    0.0013917518       1.0000000
       66.250000       89.750000    0.0050508039   0.00072398116       1.0000000
       89.750000       120.75000    0.0026503624   0.00061742877       1.0000000
       120.75000       163.25000    0.0031976691   0.00071968790       1.0000000
       163.25000       220.25000    0.0024618073   0.00086917360       1.0000000
       220.25000       297.75000    0.0031567900   0.00057289189       1.0000000
       297.75000       401.75000    0.0036699001   0.00070940948       1.0000000
       401.75000       505.25000    0.0041111061   0.00077228135       1.0000000
       505.25000       516.25000    0.0016923455   0.00025983401       1.0000000
       516.25000       731.75000    0.0019774879   0.00056512809       1.0000000
       731.75000       987.75000    0.0011684482   0.00038197714       1.1764706
       987.75000       1333.2500    0.0013958414   0.00036909303       1.1764706
       1333.2500       1800.2500    0.0016474883   0.00029437636       1.1764706
       1800.2500       2413.7500    0.0011497086   0.00025869718       1.1764706
"
Gaussian
"G3.2m0.6p0.0_G11.8m0.6p0.0"
       20.250000       26.750000       0.0000000       0.0000000       1.0000000
       26.750000       36.250000       0.0000000       0.0000000       1.0000000
       36.250000       49.250000       0.0000000       0.0000000       1.0000000
       49.250000       66.250000       0.0000000       0.0000000       1.0000000
       66.250000       89.750000       0.0000000       0.0000000       1.0000000
       89.750000       120.75000       0.0000000       0.0000000       1.0000000
       120.75000       163.25000   4.4071467e-05   0.00012927571       1.0000000
       163.25000       220.25000   1.2058858e-05   0.00015542237       1.0000000
       220.25000       297.75000   0.00030369885   0.00012015133       1.0000000
       297.75000       401.75000   0.00091883511   0.00012451531       1.0000000
       401.75000       505.25000    0.0012371969   0.00016146508       1.0000000
       505.25000       516.25000   0.00090198825   6.6964487e-05       1.0000000
       516.25000       731.75000   0.00038334218   0.00024002718       1.0000000
       731.75000       987.75000       0.0000000       0.0000000       1.1764706
       987.75000       1333.2500       0.0000000       0.0000000       1.1764706
       1333.2500       1800.2500       0.0000000       0.0000000       1.1764706
       1800.2500       2413.7500       0.0000000       0.0000000       1.1764706
DIRBE
"KRIVOS"
       20.250000       26.750000    0.0056990010    0.0014028123       1.0000000
       26.750000       36.250000    0.0080249161   0.00057684730       1.0000000
       36.250000       49.250000    0.0037309644   0.00041836148       1.0000000
       49.250000       66.250000    0.0027070601   0.00073114161       1.0000000
       66.250000       89.750000   0.00081844219   0.00039374439       1.0000000
       89.750000       120.75000    0.0010929905   0.00031195117       1.0000000
       120.75000       163.25000    0.0016936312   0.00033656983       1.0000000
       163.25000       220.25000    0.0018461880   0.00039016484       1.0000000
       220.25000       297.75000    0.0014241464   0.00026191921       1.0000000
       297.75000       401.75000    0.0015370304   0.00032067815       1.0000000
       401.75000       505.25000   0.00052231900   0.00035165331       1.0000000
       505.25000       516.25000   7.0269578e-05   0.00012562993       1.0000000
       516.25000       731.75000       0.0000000       0.0000000       1.1764706
       731.75000       987.75000       0.0000000       0.0000000       1.1764706
       987.75000       1333.2500       0.0000000       0.0000000       1.1764706
       1333.2500       1800.2500       0.0000000       0.0000000       1.1764706
       1800.2500       2413.7500       0.0000000       0.0000000       1.1764706
"ICS"
       20.250000       26.750000     0.038416940    0.0019089917       1.0000000
       26.750000       36.250000     0.015714206   0.00083687627       1.0000000
       36.250000       49.250000    0.0093516765   0.00058448366       1.0000000
       49.250000       66.250000   8.2607694e-17    0.0011842318       1.0000000
       66.250000       89.750000    0.0042323617   0.00060754759       1.0000000
       89.750000       120.75000    0.0015573719   0.00053282713       1.0000000
       120.75000       163.25000    0.0014599665   0.00062286372       1.0000000
       163.25000       220.25000   0.00060356049   0.00076097177       1.0000000
       220.25000       297.75000    0.0014289447   0.00049514352       1.0000000
       297.75000       401.75000    0.0012140346   0.00062042185       1.0000000
       401.75000       505.25000    0.0023515902   0.00066834681       1.0000000
       505.25000       516.25000   0.00072008770   0.00021736281       1.0000000
       516.25000       731.75000    0.0016074589   0.00051165188       1.0000000
       731.75000       987.75000    0.0011684482   0.00038197714       1.1764706
       987.75000       1333.2500    0.0013958414   0.00036909303       1.1764706
       1333.2500       1800.2500    0.0016474883   0.00029437636       1.1764706
       1800.2500       2413.7500    0.0011497086   0.00025869718       1.1764706


   */

  if(mode==2010)       //AWS20080505
   {
     double *gaussian,    *dirbe,    *ics,    *total;
     double *gaussian_err,*dirbe_err,*ics_err,*total_err;
   n_energy=17;
   E    =           new double[n_energy   ];
  dE    =           new double[n_energy   ];
   Emin =           new double[n_energy   ];
   Emax =           new double[n_energy   ];
   gaussian  =      new double[n_energy   ];
   gaussian_err   = new double[n_energy   ];
   dirbe  =         new double[n_energy   ];
   dirbe_err   =    new double[n_energy   ];
   ics  =           new double[n_energy   ];
   ics_err   =      new double[n_energy   ];
   total=           new double[n_energy   ];
   total_err =      new double[n_energy   ];

   i=0;
   Emin[i]=   20.250000 ; Emax[i]=       26.750000 ; gaussian[i]= 0.;             gaussian_err[i]= 0.;            dirbe[i]=  0.0056990010 ; dirbe_err[i]=  0.0014028123 ; ics[i]=   0.038416940;   ics_err[i]= 0.0019089917;   i++;  
   Emin[i]=   26.750000 ; Emax[i]=       36.250000 ; gaussian[i]= 0.;             gaussian_err[i]= 0.;            dirbe[i]=  0.0080249161;  dirbe_err[i]=  0.00057684730; ics[i]=   0.015714206;   ics_err[i]= 0.00083687627 ; i++;    
   Emin[i]=   36.250000 ; Emax[i]=       49.250000 ; gaussian[i]= 0.;             gaussian_err[i]= 0.;            dirbe[i]=  0.0037309644;  dirbe_err[i]=  0.00041836148; ics[i]=   0.0093516765;  ics_err[i]= 0.00058448366 ; i++;
   Emin[i]=   49.250000 ; Emax[i]=       66.250000 ; gaussian[i]= 0.;             gaussian_err[i]= 0.;            dirbe[i]=  0.0027070601;  dirbe_err[i]=  0.00073114161; ics[i]=   8.2607694e-17; ics_err[i]= 0.0011842318  ; i++;
   Emin[i]=   66.250000 ; Emax[i]=       89.750000 ; gaussian[i]= 0.;             gaussian_err[i]= 0.;            dirbe[i]=  0.00081844219 ;dirbe_err[i]=  0.00039374439; ics[i]=   0.0042323617;  ics_err[i]= 0.00060754759 ; i++;
   Emin[i]=   89.750000 ; Emax[i]=      120.75000  ; gaussian[i]= 0.;             gaussian_err[i]= 0.;            dirbe[i]=  0.0010929905;  dirbe_err[i]=  0.00031195117; ics[i]=   0.0015573719;  ics_err[i]= 0.00053282713 ; i++;
   Emin[i]=  120.75000  ; Emax[i]=      163.25000  ; gaussian[i]=  4.4071467e-05 ;gaussian_err[i]= 0.00012927571 ;dirbe[i]=  0.0016936312;  dirbe_err[i]=  0.00033656983; ics[i]=   0.0014599665;  ics_err[i]= 0.00062286372 ; i++;
   Emin[i]=  163.25000  ; Emax[i]=      220.25000  ; gaussian[i]=  1.2058858e-05; gaussian_err[i]= 0.00015542237; dirbe[i]=  0.0018461880 ; dirbe_err[i]=  0.00039016484; ics[i]=   0.00060356049; ics_err[i]= 0.00076097177 ; i++;
   Emin[i]=  220.25000  ; Emax[i]=      297.75000  ; gaussian[i]=  0.00030369885; gaussian_err[i]= 0.00012015133; dirbe[i]=  0.0014241464;  dirbe_err[i]=  0.00026191921; ics[i]=   0.0014289447;  ics_err[i]= 0.00049514352 ; i++;
   Emin[i]=  297.75000  ; Emax[i]=      401.75000  ; gaussian[i]=  0.00091883511; gaussian_err[i]= 0.00012451531; dirbe[i]=  0.0015370304;  dirbe_err[i]=  0.00032067815; ics[i]=   0.0012140346;  ics_err[i]= 0.00062042185 ; i++;
   Emin[i]=  401.75000  ; Emax[i]=      505.25000  ; gaussian[i]=   0.0012371969; gaussian_err[i]= 0.00016146508; dirbe[i]=  0.00052231900; dirbe_err[i]=  0.00035165331; ics[i]=   0.0023515902;  ics_err[i]= 0.00066834681 ; i++;
   Emin[i]=  505.25000  ; Emax[i]=      516.25000  ; gaussian[i]=   0.00090198825;gaussian_err[i]= 6.6964487e-05; dirbe[i]=  7.0269578e-05; dirbe_err[i]=  0.00012562993; ics[i]=   0.00072008770; ics_err[i]= 0.00021736281 ; i++;
   Emin[i]=  516.25000  ; Emax[i]=      731.75000  ; gaussian[i]=   0.00038334218;gaussian_err[i]=0.00024002718 ; dirbe[i]=  0.;            dirbe_err[i]=  0.;            ics[i]=   0.0016074589;  ics_err[i]= 0.00051165188 ; i++;
   Emin[i]=  731.75000  ; Emax[i]=      987.75000  ; gaussian[i]= 0.;             gaussian_err[i]= 0.;            dirbe[i]=  0.;            dirbe_err[i]=  0.;            ics[i]=   0.0011684482;  ics_err[i]= 0.00038197714 ; i++;
   Emin[i]=  987.75000  ; Emax[i]=     1333.2500   ; gaussian[i]= 0.;             gaussian_err[i]= 0.;            dirbe[i]=  0.;            dirbe_err[i]=  0.;            ics[i]=   0.0013958414;  ics_err[i]= 0.00036909303 ; i++;
   Emin[i]= 1333.2500   ; Emax[i]=     1800.2500   ; gaussian[i]= 0.;             gaussian_err[i]= 0.;            dirbe[i]=  0.;            dirbe_err[i]=  0.;            ics[i]=   0.0016474883;  ics_err[i]= 0.00029437636 ; i++;
   Emin[i]= 1800.2500   ; Emax[i]=     2413.7500   ; gaussian[i]= 0.;             gaussian_err[i]= 0.;            dirbe[i]=  0.;            dirbe_err[i]=  0.;            ics[i]=   0.0011497086;  ics_err[i]= 0.00025869718 ;


   int redistribute = 1; // put ics 511 keV into dirbe component instead


 for(i=0;i<n_energy;i++)
   {
     Emin[i]*=1.e-3; // keV -> MeV
     Emax[i]*=1.e-3; // keV -> MeV
     E[i]=sqrt(Emax[i]*Emin[i]);
    dE[i]=     Emax[i]-Emin[i] ;

    double   gaussian_intensity_min= (  gaussian[i]-  gaussian_err[i])/dE[i] / solid_angle;
    double   gaussian_intensity_max= (  gaussian[i]+  gaussian_err[i])/dE[i] / solid_angle;

    double   ics_intensity_min= (  ics[i]-  ics_err[i])/dE[i] / solid_angle;
    double   ics_intensity_max= (  ics[i]+  ics_err[i])/dE[i] / solid_angle;

    double dirbe_intensity_min= (dirbe[i]-dirbe_err[i])/dE[i] / solid_angle;
    double dirbe_intensity_max= (dirbe[i]+dirbe_err[i])/dE[i] / solid_angle;

    double total_intensity_min= gaussian_intensity_min +  dirbe_intensity_min +  ics_intensity_min ;
    double total_intensity_max= gaussian_intensity_max +  dirbe_intensity_max +  ics_intensity_max; 

    if(redistribute==1)
      {
	if(Emin[i]>0.5050&&Emax[i]<0.5165) // select 0.511  MeV bin and move the flux from ics to dirbe
	  {
	    dirbe_intensity_min+= ics_intensity_min;
	    dirbe_intensity_max+= ics_intensity_max;
	                          ics_intensity_min=0.0;
	                          ics_intensity_max=0.0;
          }
      }
    
    cout<<"Emin[i]="<<Emin[i]<< " Emax[i]="<<Emax[i]   <<" E="<< E[i]
        <<" MeV gaussian= "<<gaussian[i]<<" gaussian_err= "<<gaussian_err[i]
        <<"    ics= "<<ics[i]<<" ics_err= "<<ics_err[i]
        <<"       dirbe= "<<dirbe[i]<<" dirbe_err= "<<dirbe_err[i]
	<<" gaussian_intensity_min="<< gaussian_intensity_min 	<<" gaussian_intensity_max="<< gaussian_intensity_max
	<<"    dirbe_intensity_min="<<    dirbe_intensity_min 	<<"    dirbe_intensity_max="<<    dirbe_intensity_max
	<<"      ics_intensity_min="<<      ics_intensity_min 	<<"      ics_intensity_max="<<      ics_intensity_max
	<<"    total_intensity_min="<<    total_intensity_min 	<<"    total_intensity_max="<<    total_intensity_max
        <<endl;

    if(gaussian_intensity_min<=0.0)  gaussian_intensity_min=1.e-20;// avoid negative logs
    if(     ics_intensity_min<=0.0)       ics_intensity_min=1.e-20;// avoid negative logs
    if(   dirbe_intensity_min<=0.0)     dirbe_intensity_min=1.e-20;// avoid negative logs


   double shift=1.0; // tiny shift to make points visible and not obscuring each other

   // the Gaussian has the 511 keV line where the energy is exact so no shift
   spectrum=new TGraph(2);
   spectrum   ->SetPoint(0,E[i], gaussian_intensity_min * pow(E[i],2) ); 
   spectrum   ->SetPoint(1,E[i], gaussian_intensity_max * pow(E[i],2) );

   spectrum->SetMarkerColor(kMagenta  );// 
   spectrum->SetMarkerStyle(22); // triangles
   spectrum->SetMarkerSize (0.5);//
   spectrum->SetLineColor(kMagenta);// kCyan kMagenta
   spectrum->SetLineWidth(1     );
   spectrum->SetLineStyle(1      );//1=solid 2=dash 3=dot 4=dash-dot
   spectrum->Draw("L"); 
 
   shift=0.97;
   spectrum=new TGraph(2);
   spectrum   ->SetPoint(0,E[i]*shift, dirbe_intensity_min * pow(E[i],2) ); 
   spectrum   ->SetPoint(1,E[i]*shift, dirbe_intensity_max * pow(E[i],2) );

   spectrum->SetMarkerColor(kBlue  );// 
   spectrum->SetMarkerStyle(22); // triangles
   spectrum->SetMarkerSize (0.5);//
   spectrum->SetLineColor(kBlue);// kCyan kMagenta
   spectrum->SetLineWidth(1     );
   spectrum->SetLineStyle(1      );//1=solid 2=dash 3=dot 4=dash-dot
   spectrum->Draw("L"); 

   shift=0.98;
   spectrum=new TGraph(2);
   spectrum   ->SetPoint(0,E[i]*shift, ics_intensity_min * pow(E[i],2) ); 
   spectrum   ->SetPoint(1,E[i]*shift, ics_intensity_max * pow(E[i],2) );

   spectrum->SetMarkerColor(kBlack  );// 
   spectrum->SetMarkerStyle(22); // triangles
   spectrum->SetMarkerSize (0.5);//
   spectrum->SetLineColor(kGreen);// kCyan kMagenta
   spectrum->SetLineWidth(1     );
   spectrum->SetLineStyle(1      );//1=solid 2=dash 3=dot 4=dash-dot
   spectrum->Draw("L");  

   shift=0.99; // tiny shift to make points visible and not obscuring each other
   spectrum=new TGraph(2);
   spectrum   ->SetPoint(0,E[i]*shift, total_intensity_min * pow(E[i],2) ); 
   spectrum   ->SetPoint(1,E[i]*shift, total_intensity_max * pow(E[i],2) );

   spectrum->SetMarkerColor(kBlack );// 
   spectrum->SetMarkerStyle(22); // triangles
   spectrum->SetMarkerSize (0.5);//
   spectrum->SetLineColor(kBlack);// kCyan kMagenta
   spectrum->SetLineWidth(1     );
   spectrum->SetLineStyle(1      );//1=solid 2=dash 3=dot 4=dash-dot
   spectrum->Draw("L"); 



}   
   
   }


  /////////////////// 21 June 2010 fits
    /*
made_allspectrum.dat.radians.allcomponents
"G3.2m0.6p0.0_G11.8m0.6p0.0" & "KRIVOS" & "ICS" & "240mu" &                        total
       20.250000       26.750000     0.045724573    0.0023103483       1.0000000
       26.750000       36.250000     0.024807438   0.00093625033       1.0000000
       36.250000       49.250000     0.013808730   0.00068790318       1.0000000
       49.250000       66.250000    0.0030601709    0.0014037696       1.0000000
       66.250000       89.750000    0.0052558090   0.00072876823       1.0000000
       89.750000       120.75000    0.0028872237   0.00062697752       1.0000000
       120.75000       163.25000    0.0034353715   0.00073180062       1.0000000
       163.25000       220.25000    0.0027385918   0.00088342143       1.0000000
       220.25000       297.75000    0.0032784028   0.00058352207       1.0000000
       297.75000       401.75000    0.0038320086   0.00072267059       1.0000000
       401.75000       505.25000    0.0043461070   0.00078692140       1.0000000
       505.25000       516.25000    0.0017071260   0.00026519921       1.0000000
       516.25000       731.75000    0.0016494795    0.0010646809       1.0000000
       731.75000       987.75000   0.00099380382   0.00041854070       1.1764706
       987.75000       1170.2500   0.00077250610   0.00022802003       1.1764706
       1170.2500       1176.2500   0.00011002104   7.5865392e-05       1.1764706
       1176.2500       1329.7500   0.00020601916   0.00015763039       1.1764706
       1329.7500       1335.7500   7.3231909e-05   7.4336884e-05       1.1764706
       1335.7500       1400.2500   0.00033078749   0.00010046551       1.1764706
       1400.2500       1600.2500   0.00055707583   0.00015165854       1.1764706
       1600.2500       1805.7500   0.00029355886   0.00014750447       1.1764706
       1805.7500       1812.2500   0.00044965070   6.6619402e-05       1.1764706
       1812.2500       2200.2500   0.00045809785   0.00017908048       1.1764706
       2200.2500       2413.7500   0.00018198095   0.00012037367       1.1764706
"G3.2m0.6p0.0_G11.8m0.6p0.0"                                                        Gaussian
       20.250000       26.750000       0.0000000       0.0000000       1.0000000
       26.750000       36.250000       0.0000000       0.0000000       1.0000000
       36.250000       49.250000       0.0000000       0.0000000       1.0000000
       49.250000       66.250000       0.0000000       0.0000000       1.0000000
       66.250000       89.750000       0.0000000       0.0000000       1.0000000
       89.750000       120.75000       0.0000000       0.0000000       1.0000000
       120.75000       163.25000   5.0000001e-16   0.00013187810       1.0000000
       163.25000       220.25000   4.0966753e-07   0.00015838202       1.0000000
       220.25000       297.75000   0.00025224287   0.00012262706       1.0000000
       297.75000       401.75000   0.00083218804   0.00012718240       1.0000000
       401.75000       505.25000    0.0012937068   0.00016527120       1.0000000
       505.25000       516.25000   0.00091797158   6.8679862e-05       1.0000000
       516.25000       731.75000   0.00031908143   0.00024561018       1.0000000
       731.75000       987.75000       0.0000000       0.0000000       1.1764706
       987.75000       1170.2500       0.0000000       0.0000000       1.1764706
       1170.2500       1176.2500       0.0000000       0.0000000       1.1764706
       1176.2500       1329.7500       0.0000000       0.0000000       1.1764706
       1329.7500       1335.7500       0.0000000       0.0000000       1.1764706
       1335.7500       1400.2500       0.0000000       0.0000000       1.1764706
       1400.2500       1600.2500       0.0000000       0.0000000       1.1764706
       1600.2500       1805.7500       0.0000000       0.0000000       1.1764706
       1805.7500       1812.2500       0.0000000       0.0000000       1.1764706
       1812.2500       2200.2500       0.0000000       0.0000000       1.1764706
       2200.2500       2413.7500       0.0000000       0.0000000       1.1764706
"KRIVOS"                                                                           DIRBE
       20.250000       26.750000    0.0055629696    0.0013686127       1.0000000
       26.750000       36.250000    0.0080110927   0.00053159806       1.0000000
       36.250000       49.250000    0.0034016172   0.00040045639       1.0000000
       49.250000       66.250000    0.0023691688   0.00073706837       1.0000000
       66.250000       89.750000   0.00031297730   0.00039612174       1.0000000
       89.750000       120.75000   0.00069015526   0.00031665208       1.0000000
       120.75000       163.25000    0.0014353235   0.00034162380       1.0000000
       163.25000       220.25000    0.0016898055   0.00039569375       1.0000000
       220.25000       297.75000    0.0013162813   0.00026630994       1.0000000
       297.75000       401.75000    0.0014485602   0.00032610312       1.0000000
       401.75000       505.25000   0.00034630684   0.00035770306       1.0000000
       505.25000       516.25000   5.1493571e-05   0.00012801136       1.0000000
       516.25000       731.75000   0.00079593601   0.00049244524       1.0000000
       731.75000       987.75000       0.0000000       0.0000000       1.1764706
       987.75000       1170.2500       0.0000000       0.0000000       1.1764706
       1170.2500       1176.2500       0.0000000       0.0000000       1.1764706
       1176.2500       1329.7500       0.0000000       0.0000000       1.1764706
       1329.7500       1335.7500       0.0000000       0.0000000       1.1764706
       1335.7500       1400.2500       0.0000000       0.0000000       1.1764706
       1400.2500       1600.2500       0.0000000       0.0000000       1.1764706
       1600.2500       1805.7500       0.0000000       0.0000000       1.1764706
       1805.7500       1812.2500       0.0000000       0.0000000       1.1764706
       1812.2500       2200.2500       0.0000000       0.0000000       1.1764706
       2200.2500       2413.7500       0.0000000       0.0000000       1.1764706
"ICS"                                                                             IC
       20.250000       26.750000     0.040161604    0.0018613459       1.0000000
       26.750000       36.250000     0.016796346   0.00077069331       1.0000000
       36.250000       49.250000     0.010407113   0.00055932590       1.0000000
       49.250000       66.250000   0.00069100213    0.0011946964       1.0000000
       66.250000       89.750000    0.0049428317   0.00061171128       1.0000000
       89.750000       120.75000    0.0021970684   0.00054113978       1.0000000
       120.75000       163.25000    0.0020000480   0.00063358779       1.0000000
       163.25000       220.25000    0.0010483766   0.00077380554       1.0000000
       220.25000       297.75000    0.0017098786   0.00050451921       1.0000000
       297.75000       401.75000    0.0015512604   0.00063224535       1.0000000
       401.75000       505.25000    0.0027060933   0.00068116022       1.0000000
       505.25000       516.25000   0.00073766087   0.00022187111       1.0000000
       516.25000       731.75000   0.00053446204   0.00091143772       1.0000000
       731.75000       987.75000   0.00099380382   0.00041854070       1.1764706
       987.75000       1170.2500   0.00077250610   0.00022802003       1.1764706
       1170.2500       1176.2500   1.4279535e-16   5.0045036e-05       1.1764706
       1176.2500       1329.7500   0.00020601916   0.00015763039       1.1764706
       1329.7500       1335.7500   1.3772126e-05   4.8701480e-05       1.1764706
       1335.7500       1400.2500   0.00033078749   0.00010046551       1.1764706
       1400.2500       1600.2500   0.00055707583   0.00015165854       1.1764706
       1600.2500       1805.7500   0.00029355886   0.00014750447       1.1764706
       1805.7500       1812.2500   3.7474063e-05   4.3011423e-05       1.1764706
       1812.2500       2200.2500   0.00045809785   0.00017908048       1.1764706
       2200.2500       2413.7500   0.00018198095   0.00012037367       1.1764706
"240mu"                                                                           26Al tracer   
       20.250000       26.750000       0.0000000       0.0000000       1.0000000
       26.750000       36.250000       0.0000000       0.0000000       1.0000000
       36.250000       49.250000       0.0000000       0.0000000       1.0000000
       49.250000       66.250000       0.0000000       0.0000000       1.0000000
       66.250000       89.750000       0.0000000       0.0000000       1.0000000
       89.750000       120.75000       0.0000000       0.0000000       1.0000000
       120.75000       163.25000       0.0000000       0.0000000       1.0000000
       163.25000       220.25000       0.0000000       0.0000000       1.0000000
       220.25000       297.75000       0.0000000       0.0000000       1.0000000
       297.75000       401.75000       0.0000000       0.0000000       1.0000000
       401.75000       505.25000       0.0000000       0.0000000       1.0000000
       505.25000       516.25000       0.0000000       0.0000000       1.0000000
       516.25000       731.75000       0.0000000       0.0000000       1.0000000
       731.75000       987.75000       0.0000000       0.0000000       1.1764706
       987.75000       1170.2500       0.0000000       0.0000000       1.1764706
       1170.2500       1176.2500   0.00011002104   5.7017997e-05       1.1764706
       1176.2500       1329.7500       0.0000000       0.0000000       1.1764706
       1329.7500       1335.7500   5.9459784e-05   5.6161714e-05       1.1764706
       1335.7500       1400.2500       0.0000000       0.0000000       1.1764706
       1400.2500       1600.2500       0.0000000       0.0000000       1.1764706
       1600.2500       1805.7500       0.0000000       0.0000000       1.1764706
       1805.7500       1812.2500   0.00041217663   5.0873983e-05       1.1764706
       1812.2500       2200.2500       0.0000000       0.0000000       1.1764706
       2200.2500       2413.7500       0.0000000       0.0000000       1.1764706
    */


  if(mode==2011)       //AWS20100622
   {
 
     cout<<endl<<"plot_SPI_spectrum_bouchet: data of 21 June 2010"<<endl;

double data_all[][5]=
  {
    20.250000 ,     26.750000,    0.045724573,   0.0023103483,      1.000000,
    26.750000,      36.250000,    0.024807438,  0.00093625033,      1.000000,
    36.250000,      49.250000,    0.013808730,  0.00068790318,      1.000000,
    49.250000,      66.250000,   0.0030601709,   0.0014037696,      1.0000000,
    66.250000 ,     89.750000,   0.0052558090,  0.00072876823,      1.0000000,
    89.750000,      120.75000,   0.0028872237,  0.00062697752,      1.0000000,
    120.75000,      163.25000,   0.0034353715,  0.00073180062,      1.0000000,
    163.25000,      220.25000,   0.0027385918,  0.00088342143,      1.0000000,
    220.25000,      297.75000,   0.0032784028,  0.00058352207,      1.0000000,
    297.75000,      401.75000,   0.0038320086,  0.00072267059,      1.0000000,
    401.75000,      505.25000,   0.0043461070,  0.00078692140,      1.0000000,
    505.25000,      516.25000,   0.0017071260,  0.00026519921,      1.0000000,
    516.25000,      731.75000,   0.0016494795,   0.0010646809,      1.0000000,
    731.75000,      987.75000,  0.00099380382,  0.00041854070,      1.1764706,
    987.75000,      1170.2500,  0.00077250610,  0.00022802003,      1.1764706,
    1170.2500,      1176.2500,  0.00011002104,  7.5865392e-05,      1.1764706,
    1176.2500,      1329.7500,  0.00020601916,  0.00015763039,      1.1764706,
    1329.7500,      1335.7500,  7.3231909e-05,  7.4336884e-05,      1.1764706,
    1335.7500,      1400.2500,  0.00033078749,  0.00010046551,      1.1764706,
    1400.2500,      1600.2500,  0.00055707583,  0.00015165854,      1.1764706,
    1600.2500,      1805.7500,  0.00029355886,  0.00014750447,      1.1764706,
    1805.7500,      1812.2500,  0.00044965070,  6.6619402e-05,      1.1764706,
    1812.2500,      2200.2500,  0.00045809785,  0.00017908048,      1.1764706,
    2200.2500,      2413.7500,  0.00018198095,  0.00012037367,      1.1764706
  };


double data_gaussian[][5]=
  {
    20.250000,      26.750000,      0.0000000,      0.0000000,      1.0000000,
    26.750000,      36.250000,      0.0000000,      0.0000000,      1.000000,
    36.250000,      49.250000,      0.0000000,      0.0000000,      1.0000000,
    49.250000,      66.250000,      0.0000000,      0.0000000,      1.0000000,
    66.250000,      89.750000,      0.0000000,      0.0000000,      1.0000000,
    89.750000,      120.75000,      0.0000000,      0.0000000,      1.0000000,
    120.75000,      163.25000,  5.0000001e-16,  0.00013187810,      1.0000000,
    163.25000,      220.25000,  4.0966753e-07,  0.00015838202,      1.0000000,
    220.25000,      297.75000,  0.00025224287,  0.00012262706,      1.0000000,
    297.75000,      401.75000,  0.00083218804,  0.00012718240,      1.0000000,
    401.75000,      505.25000,   0.0012937068,  0.00016527120,      1.0000000,
    505.25000,      516.25000,  0.00091797158,  6.8679862e-05,      1.0000000,
    516.25000,      731.75000,  0.00031908143,  0.00024561018,      1.0000000,
    731.75000,      987.75000,      0.0000000,      0.0000000,      1.1764706,
    987.75000,      1170.2500,      0.0000000,      0.0000000,      1.1764706,
    1170.2500,      1176.2500,      0.0000000,      0.0000000,      1.1764706,
    1176.2500,      1329.7500,      0.0000000,      0.0000000,      1.1764706,
    1329.7500,      1335.7500,      0.0000000,      0.0000000,      1.1764706,
    1335.7500,      1400.2500,      0.0000000,      0.0000000,      1.1764706,
    1400.2500,      1600.2500,      0.0000000,      0.0000000,      1.1764706,
    1600.2500,      1805.7500,      0.0000000,      0.0000000,      1.1764706,
    1805.7500,      1812.2500,      0.0000000,      0.0000000,      1.1764706,
    1812.2500,      2200.2500,      0.0000000,      0.0000000,      1.1764706,
    2200.2500,      2413.7500,      0.0000000,      0.0000000,      1.1764706
  };

double data_dirbe   [][5]=
 {
   20.250000,      26.750000,   0.0055629696,   0.0013686127,      1.0000000,
   26.750000,      36.250000,   0.0080110927,  0.00053159806,      1.0000000,
   36.250000,      49.250000,   0.0034016172,  0.00040045639,      1.0000000,
   49.250000,      66.250000,   0.0023691688,  0.00073706837,      1.0000000,
   66.250000,      89.750000,  0.00031297730,  0.00039612174,      1.0000000,
   89.750000,      120.75000,  0.00069015526,  0.00031665208,      1.0000000,
   120.75000,      163.25000,   0.0014353235,  0.00034162380,      1.0000000,
   163.25000,      220.25000,   0.0016898055,  0.00039569375,      1.0000000,
   220.25000,      297.75000,   0.0013162813,  0.00026630994,      1.0000000,
   297.75000,      401.75000,   0.0014485602,  0.00032610312,      1.0000000,
   401.75000,      505.25000,  0.00034630684,  0.00035770306,      1.0000000,
   505.25000,      516.25000,  5.1493571e-05,  0.00012801136,      1.0000000,
   516.25000,      731.75000,  0.00079593601,  0.00049244524,      1.0000000,
   731.75000,      987.75000,      0.0000000,      0.0000000,      1.1764706,
   987.75000,      1170.2500,      0.0000000,      0.0000000,      1.1764706,
   1170.2500,      1176.2500,      0.0000000,      0.0000000,      1.1764706,
   1176.2500,      1329.7500,      0.0000000,      0.0000000,      1.1764706,
   1329.7500,      1335.7500,      0.0000000,      0.0000000,      1.1764706,
   1335.7500,      1400.2500,      0.0000000,      0.0000000,      1.1764706,
   1400.2500,      1600.2500,      0.0000000,      0.0000000,      1.1764706,
   1600.2500,      1805.7500,      0.0000000,      0.0000000,      1.1764706,
   1805.7500,      1812.2500,      0.0000000,      0.0000000,      1.1764706,
   1812.2500,      2200.2500,      0.0000000,      0.0000000,      1.1764706,
   2200.2500,      2413.7500,      0.0000000,      0.0000000,      1.1764706
 };


double data_ics     [][5]=
 {
   20.250000,      26.750000,    0.040161604,   0.0018613459,      1.0000000,
   26.750000,      36.250000,    0.016796346,  0.00077069331,      1.0000000,
   36.250000,      49.250000,    0.010407113,  0.00055932590,      1.0000000,
   49.250000,      66.250000,  0.00069100213,   0.0011946964,      1.0000000,
   66.250000,      89.750000,   0.0049428317,  0.00061171128,      1.0000000,
   89.750000,      120.75000,   0.0021970684,  0.00054113978,      1.0000000,
   120.75000,      163.25000,   0.0020000480,  0.00063358779,      1.0000000,
   163.25000,      220.25000,   0.0010483766,  0.00077380554,      1.0000000,
   220.25000,      297.75000,   0.0017098786,  0.00050451921,      1.0000000,
   297.75000,      401.75000,   0.0015512604,  0.00063224535,      1.0000000,
   401.75000,      505.25000,   0.0027060933,  0.00068116022,      1.0000000,
   505.25000,      516.25000,  0.00073766087,  0.00022187111,      1.0000000,
   516.25000,      731.75000,  0.00053446204,  0.00091143772,      1.0000000,
   731.75000,      987.75000,  0.00099380382,  0.00041854070,      1.1764706,
   987.75000,      1170.2500,  0.00077250610,  0.00022802003,      1.1764706,
   1170.2500,      1176.2500,  1.4279535e-16,  5.0045036e-05,      1.1764706,
   1176.2500,      1329.7500,  0.00020601916,  0.00015763039,      1.1764706,
   1329.7500,      1335.7500,  1.3772126e-05,  4.8701480e-05,      1.1764706,
   1335.7500,      1400.2500,  0.00033078749,  0.00010046551,      1.1764706,
   1400.2500,      1600.2500,  0.00055707583,  0.00015165854,      1.1764706,
   1600.2500,      1805.7500,  0.00029355886,  0.00014750447,      1.1764706,
   1805.7500,      1812.2500,  3.7474063e-05,  4.3011423e-05,      1.1764706,
   1812.2500,      2200.2500,  0.00045809785,  0.00017908048,      1.1764706,
   2200.2500,      2413.7500,  0.00018198095,  0.00012037367,      1.1764706
 };

double data_line    [][5]=
 {
   20.250000,      26.750000,      0.0000000,      0.0000000,      1.0000000,
   26.750000,      36.250000,      0.0000000,      0.0000000,      1.000000,
   36.250000,      49.250000,      0.0000000,      0.0000000,      1.000000,
   49.250000,      66.250000,      0.0000000,      0.0000000,      1.0000000,
   66.250000,      89.750000,      0.0000000,      0.0000000,      1.0000000,
   89.750000,      120.75000,      0.0000000,      0.0000000,      1.0000000,
   120.75000,      163.25000,      0.0000000,      0.0000000,      1.0000000,
   163.25000,      220.25000,      0.0000000,      0.0000000,      1.0000000,
   220.25000,      297.75000,      0.0000000,      0.0000000,      1.0000000,
   297.75000,      401.75000,      0.0000000,      0.0000000,      1.0000000,
   401.75000,      505.25000,      0.0000000,      0.0000000,      1.0000000,
   505.25000,      516.25000,      0.0000000,      0.0000000,      1.0000000,
   516.25000,      731.75000,      0.0000000,      0.0000000,      1.0000000,
   731.75000,      987.75000,      0.0000000,      0.0000000,      1.1764706,
   987.75000,      1170.2500,      0.0000000,      0.0000000,      1.1764706,
   1170.2500,      1176.2500,  0.00011002104,  5.7017997e-05,      1.1764706,
   1176.2500,      1329.7500,      0.0000000,      0.0000000,      1.1764706,
   1329.7500,      1335.7500,  5.9459784e-05,  5.6161714e-05,      1.1764706,
   1335.7500,      1400.2500,      0.0000000,      0.0000000,      1.1764706,
   1400.2500,      1600.2500,      0.0000000,      0.0000000,      1.1764706,
   1600.2500,      1805.7500,      0.0000000,      0.0000000,      1.1764706,
   1805.7500,      1812.2500,  0.00041217663,  5.0873983e-05,      1.1764706,
   1812.2500,      2200.2500,      0.0000000,      0.0000000,      1.1764706,
   2200.2500,      2413.7500,      0.0000000,      0.0000000,      1.1764706
 };


 double *gaussian,    *dirbe,    *ics,    *line,    *total;
 double *gaussian_err,*dirbe_err,*ics_err,*line_err,*total_err;

   n_energy=24;
   E    =           new double[n_energy   ];
  dE    =           new double[n_energy   ];
   Emin =           new double[n_energy   ];
   Emax =           new double[n_energy   ];
   gaussian  =      new double[n_energy   ];
   gaussian_err   = new double[n_energy   ];
   dirbe  =         new double[n_energy   ];
   dirbe_err   =    new double[n_energy   ];
   ics  =           new double[n_energy   ];
   ics_err   =      new double[n_energy   ];
   line =           new double[n_energy   ];
   line_err =       new double[n_energy   ];
   total=           new double[n_energy   ];
   total_err =      new double[n_energy   ];


   for ( i=0;i<n_energy;i++)
   {
    Emin[i]        =data_gaussian[i][0];
    Emax[i]        =data_gaussian[i][1];
    gaussian    [i]=data_gaussian[i][2];
    gaussian_err[i]=data_gaussian[i][3];

       dirbe    [i]=data_dirbe   [i][2];
       dirbe_err[i]=data_dirbe   [i][3];

         ics    [i]=data_ics     [i][2];
         ics_err[i]=data_ics     [i][3];

        line    [i]=data_line    [i][2];
        line_err[i]=data_line    [i][3];


    cout<<"Emin[i]="<<Emin[i]<< " Emax[i]="<<Emax[i]
        <<" MeV gaussian= "<<gaussian[i]<<" gaussian_err= "<<gaussian_err[i]
      
        <<"    ics= "<<ics[i]<<" ics_err= "<<ics_err[i]
        <<"       dirbe= "<<dirbe[i]<<" dirbe_err= "<<dirbe_err[i]
        <<"       line = "<<line [i]<<"  line_err= "<< line_err[i]
      /*
	<<" gaussian_intensity_min="<< gaussian_intensity_min 	<<" gaussian_intensity_max="<< gaussian_intensity_max
	<<"    dirbe_intensity_min="<<    dirbe_intensity_min 	<<"    dirbe_intensity_max="<<    dirbe_intensity_max
	<<"      ics_intensity_min="<<      ics_intensity_min 	<<"      ics_intensity_max="<<      ics_intensity_max
	<<"    total_intensity_min="<<    total_intensity_min 	<<"    total_intensity_max="<<    total_intensity_max
      */
        <<endl;

   }


   int redistribute = 1; // put ics 511 keV into dirbe component instead
   int plot_gaussian = 0;
   int plot_ics      = 0;
   int plot_dirbe    = 0;
   int plot_lines    = 1;
   int plot_total    = 1;

 for(i=0;i<n_energy;i++)
   {
     Emin[i]*=1.e-3; // keV -> MeV
     Emax[i]*=1.e-3; // keV -> MeV
     E[i]=sqrt(Emax[i]*Emin[i]);
    dE[i]=     Emax[i]-Emin[i] ;

    double   gaussian_intensity_min= (  gaussian[i]-  gaussian_err[i])/dE[i] / solid_angle;
    double   gaussian_intensity_max= (  gaussian[i]+  gaussian_err[i])/dE[i] / solid_angle;

    double   ics_intensity_min= (  ics[i]-  ics_err[i])/dE[i] / solid_angle;
    double   ics_intensity_max= (  ics[i]+  ics_err[i])/dE[i] / solid_angle;

    double dirbe_intensity_min= (dirbe[i]-dirbe_err[i])/dE[i] / solid_angle;
    double dirbe_intensity_max= (dirbe[i]+dirbe_err[i])/dE[i] / solid_angle;

    double  line_intensity_min= ( line[i]- line_err[i])/dE[i] / solid_angle;
    double  line_intensity_max= ( line[i]+ line_err[i])/dE[i] / solid_angle;

    double total_intensity_min= gaussian_intensity_min +  dirbe_intensity_min +  ics_intensity_min;// + line_intensity_min;
    double total_intensity_max= gaussian_intensity_max +  dirbe_intensity_max +  ics_intensity_max;// + line_intensity_max; 

    if(redistribute==1)
      {
	if(Emin[i]>0.5050&&Emax[i]<0.5165) // select 0.511  MeV bin and move the flux from ics to dirbe
	  {
	    dirbe_intensity_min+= ics_intensity_min;
	    dirbe_intensity_max+= ics_intensity_max;
	                          ics_intensity_min=0.0;
	                          ics_intensity_max=0.0;
          }
      }
    
    // move 0.511 MeV line from total to lines. Use dirbe as assigned above for 0.511 MeV
	if(Emin[i]>0.5050&&Emax[i]<0.5165) // select 0.511  MeV line
	  {
	     line_intensity_min = gaussian_intensity_min + dirbe_intensity_min;
	     line_intensity_max = gaussian_intensity_max + dirbe_intensity_max;
	                          total_intensity_min=0.0;
	                          total_intensity_max=0.0;
          }

    // remove line ranges from total, since these have huge errors for continuum
    if(Emin[i]>1.170&&Emax[i]<1.177||Emin[i]>1.329&&Emax[i]<1.336||Emin[i]>1.805&&Emax[i]<1.813   )
      {
	total_intensity_min=1.e-20;
	total_intensity_max=1.e-20;
      }

    cout<<"Emin[i]="<<Emin[i]<< " Emax[i]="<<Emax[i]   <<" E="<< E[i]
        <<" MeV gaussian= "<<gaussian[i]<<" gaussian_err= "<<gaussian_err[i]
        <<"    ics= "<<ics[i]<<" ics_err= "<<ics_err[i]
        <<"       dirbe= "<<dirbe[i]<<" dirbe_err= "<<dirbe_err[i]
	<<" gaussian_intensity_min="<< gaussian_intensity_min 	<<" gaussian_intensity_max="<< gaussian_intensity_max
	<<"    dirbe_intensity_min="<<    dirbe_intensity_min 	<<"    dirbe_intensity_max="<<    dirbe_intensity_max
	<<"      ics_intensity_min="<<      ics_intensity_min 	<<"      ics_intensity_max="<<      ics_intensity_max
	<<"    total_intensity_min="<<    total_intensity_min 	<<"    total_intensity_max="<<    total_intensity_max
        <<endl;

    if(gaussian_intensity_min<=0.0)  gaussian_intensity_min=1.e-20;// avoid negative logs
    if(     ics_intensity_min<=0.0)       ics_intensity_min=1.e-20;// avoid negative logs
    if(   dirbe_intensity_min<=0.0)     dirbe_intensity_min=1.e-20;// avoid negative logs


   double shift=1.0; // tiny shift to make points visible and not obscuring each other

   // the Gaussian has the 511 keV line where the energy is exact so no shift
   spectrum=new TGraph(2);
   spectrum   ->SetPoint(0,E[i], gaussian_intensity_min * pow(E[i],2) ); 
   spectrum   ->SetPoint(1,E[i], gaussian_intensity_max * pow(E[i],2) );

   spectrum->SetMarkerColor(kMagenta  );// 
   spectrum->SetMarkerStyle(22); // triangles
   spectrum->SetMarkerSize (0.5);//
   spectrum->SetLineColor(kMagenta);// kCyan kMagenta
   spectrum->SetLineWidth(1     );
   spectrum->SetLineStyle(1      );//1=solid 2=dash 3=dot 4=dash-dot
   if(plot_gaussian==1)spectrum->Draw("L"); 
 
   shift=0.97;
   spectrum=new TGraph(2);
   spectrum   ->SetPoint(0,E[i]*shift, dirbe_intensity_min * pow(E[i],2) ); 
   spectrum   ->SetPoint(1,E[i]*shift, dirbe_intensity_max * pow(E[i],2) );

   spectrum->SetMarkerColor(kBlue  );// 
   spectrum->SetMarkerStyle(22); // triangles
   spectrum->SetMarkerSize (0.5);//
   spectrum->SetLineColor(kBlue);// kCyan kMagenta
   spectrum->SetLineWidth(1     );
   spectrum->SetLineStyle(1      );//1=solid 2=dash 3=dot 4=dash-dot
   if(plot_dirbe==1)spectrum->Draw("L"); 

   shift=0.98;
   spectrum=new TGraph(2);
   spectrum   ->SetPoint(0,E[i]*shift, ics_intensity_min * pow(E[i],2) ); 
   spectrum   ->SetPoint(1,E[i]*shift, ics_intensity_max * pow(E[i],2) );

   spectrum->SetMarkerColor(kBlack  );// 
   spectrum->SetMarkerStyle(22); // triangles
   spectrum->SetMarkerSize (0.5);//
   spectrum->SetLineColor(kGreen);// kCyan kMagenta
   spectrum->SetLineWidth(1     );
   spectrum->SetLineStyle(1      );//1=solid 2=dash 3=dot 4=dash-dot
   if(plot_ics==1)spectrum->Draw("L");  

   shift=1.00;
   spectrum=new TGraph(2);
   spectrum   ->SetPoint(0,E[i]*shift, line_intensity_min * pow(E[i],2) ); 
   spectrum   ->SetPoint(1,E[i]*shift, line_intensity_max * pow(E[i],2) );

   spectrum->SetMarkerColor(kBlack  );// 
   spectrum->SetMarkerStyle(22); // triangles
   spectrum->SetMarkerSize (0.5);//
   spectrum->SetLineColor(kBlue);// kCyan kMagenta
   spectrum->SetLineWidth(1     );
   spectrum->SetLineStyle(1      );//1=solid 2=dash 3=dot 4=dash-dot
   if(plot_lines==1)spectrum->Draw("L");  


   shift=0.99; // tiny shift to make points visible and not obscuring each other
   spectrum=new TGraph(2);
   spectrum   ->SetPoint(0,E[i]*shift, total_intensity_min * pow(E[i],2) ); 
   spectrum   ->SetPoint(1,E[i]*shift, total_intensity_max * pow(E[i],2) );

   spectrum->SetMarkerColor(kMagenta );             //AWS20100924 
   spectrum->SetMarkerStyle(22); // triangles
   spectrum->SetMarkerSize (0.5);//
   spectrum->SetLineColor(kMagenta);// kCyan kMagenta AWS20100924
   spectrum->SetLineWidth(2     );
   spectrum->SetLineStyle(1      );//1=solid 2=dash 3=dot 4=dash-dot
   if(plot_total==1)spectrum->Draw("L"); 

   }// i



   }// mode=2011


  ////////////////////////////////////////////////////////////////////////////////////////////////


  if(mode==2012)       //AWS20100922
   {
 
     cout<<endl<<"plot_SPI_spectrum_bouchet: data of 1 July 2010, |b|<10 "<<endl;

     /*
Subject:   	Re: SPI+Fermi
From:   	"L. Bouchet CESR 61556637" <laurent.bouchet@cesr.fr>
Date:   	Mon, July 12, 2010 13:48
To:   	aws@mpe.mpg.de
Priority:   	Normal
Options:   	View Full Header |  View Printable Version  | Download this as a file

Here are the data (attached file) for |b| < 10 and |l| < 29.75. A plot 
show also the spectrum for |b| < 15 (purple) and |b| < 10 (red) (in e^2).

For the lines I used the following values for |b| < 15 (blue on graph)

240mu" only fluxes
>        1170.2500       1176.2500   0.00010625607   3.7268001e-05
> (2.85113 sigma)
>        1329.7500       1335.7500   6.8654161e-05   3.6523392e-05
> (1.87973 sigma)
>        1805.7500       1812.2500   0.00044409527   3.1696206e-05
> (14.0110 sigma)

for |b| < 10, the values should be multiplied by 0.948148





"G3.2m0.6p0.0_G11.8m0.6p0.0" & "KRIVOS" & "ICS" & "240mu" & 
       20.250000       26.750000     0.035531153    0.0019269991       1.0000000
       26.750000       36.250000     0.020449433   0.00077739729       1.0000000
       36.250000       49.250000     0.011203425   0.00057562936       1.0000000
       49.250000       66.250000    0.0028262920    0.0011595252       1.0000000
       66.250000       89.750000    0.0041031899   0.00060727258       1.0000000
       89.750000       120.75000    0.0023679900   0.00051900478       1.0000000
       120.75000       163.25000    0.0029467333   0.00060716008       1.0000000
       163.25000       220.25000    0.0024583629   0.00073218657       1.0000000
       220.25000       297.75000    0.0028625643   0.00048728282       1.0000000
       297.75000       401.75000    0.0034334931   0.00060245839       1.0000000
       401.75000       505.25000    0.0037264718   0.00065961499       1.0000000
       505.25000       516.25000    0.0015231997   0.00022491983       1.0000000
       516.25000       731.75000    0.0015045000   0.00089720516       1.0000000
       731.75000       987.75000   0.00078763114   0.00033171103       1.1764706
       987.75000       1170.2500   0.00061267031   0.00018084143       1.1764706
       1170.2500       1176.2500   0.00010431626   6.7071240e-05       1.1764706
       1176.2500       1329.7500   0.00016343718   0.00012504986       1.1764706
       1329.7500       1335.7500   6.7303226e-05   6.5791185e-05       1.1764706
       1335.7500       1400.2500   0.00026245025   7.9710381e-05       1.1764706
       1400.2500       1600.2500   0.00044203067   0.00012033860       1.1764706
       1600.2500       1805.7500   0.00023293564   0.00011704313       1.1764706
       1805.7500       1812.2500   0.00042053991   5.9089090e-05       1.1764706
       1812.2500       2200.2500   0.00036347436   0.00014209009       1.1764706
       2200.2500       2413.7500   0.00014436881   9.5494632e-05       1.1764706
"G3.2m0.6p0.0_G11.8m0.6p0.0"
       20.250000       26.750000       0.0000000       0.0000000       1.0000000
       26.750000       36.250000       0.0000000       0.0000000       1.0000000
       36.250000       49.250000       0.0000000       0.0000000       1.0000000
       49.250000       66.250000       0.0000000       0.0000000       1.0000000
       66.250000       89.750000       0.0000000       0.0000000       1.0000000
       89.750000       120.75000       0.0000000       0.0000000       1.0000000
       120.75000       163.25000   4.8499837e-16   0.00012792132       1.0000000
       163.25000       220.25000   3.9737616e-07   0.00015363004       1.0000000
       220.25000       297.75000   0.00024467476   0.00011894785       1.0000000
       297.75000       401.75000   0.00080721967   0.00012336651       1.0000000
       401.75000       505.25000    0.0012548913   0.00016031252       1.0000000
       505.25000       516.25000   0.00089042942   6.6619241e-05       1.0000000
       516.25000       731.75000   0.00030950794   0.00023824107       1.0000000
       731.75000       987.75000       0.0000000       0.0000000       1.1764706
       987.75000       1170.2500       0.0000000       0.0000000       1.1764706
       1170.2500       1176.2500       0.0000000       0.0000000       1.1764706
       1176.2500       1329.7500       0.0000000       0.0000000       1.1764706
       1329.7500       1335.7500       0.0000000       0.0000000       1.1764706
       1335.7500       1400.2500       0.0000000       0.0000000       1.1764706
       1400.2500       1600.2500       0.0000000       0.0000000       1.1764706
       1600.2500       1805.7500       0.0000000       0.0000000       1.1764706
       1805.7500       1812.2500       0.0000000       0.0000000       1.1764706
       1812.2500       2200.2500       0.0000000       0.0000000       1.1764706
       2200.2500       2413.7500       0.0000000       0.0000000       1.1764706
"KRIVOS"
       20.250000       26.750000    0.0053966291    0.0013276893       1.0000000
       26.750000       36.250000    0.0077715499   0.00051570254       1.0000000
       36.250000       49.250000    0.0032999041   0.00038848219       1.0000000
       49.250000       66.250000    0.0022983273   0.00071502900       1.0000000
       66.250000       89.750000   0.00030361884   0.00038427716       1.0000000
       89.750000       120.75000   0.00066951866   0.00030718375       1.0000000
       120.75000       163.25000    0.0013924053   0.00033140878       1.0000000
       163.25000       220.25000    0.0016392780   0.00038386196       1.0000000
       220.25000       297.75000    0.0012769227   0.00025834690       1.0000000
       297.75000       401.75000    0.0014052462   0.00031635218       1.0000000
       401.75000       505.25000   0.00033595179   0.00034700724       1.0000000
       505.25000       516.25000   4.9953842e-05   0.00012418364       1.0000000
       516.25000       731.75000   0.00077213642   0.00047772044       1.0000000
       731.75000       987.75000       0.0000000       0.0000000       1.1764706
       987.75000       1170.2500       0.0000000       0.0000000       1.1764706
       1170.2500       1176.2500       0.0000000       0.0000000       1.1764706
       1176.2500       1329.7500       0.0000000       0.0000000       1.1764706
       1329.7500       1335.7500       0.0000000       0.0000000       1.1764706
       1335.7500       1400.2500       0.0000000       0.0000000       1.1764706
       1400.2500       1600.2500       0.0000000       0.0000000       1.1764706
       1600.2500       1805.7500       0.0000000       0.0000000       1.1764706
       1805.7500       1812.2500       0.0000000       0.0000000       1.1764706
       1812.2500       2200.2500       0.0000000       0.0000000       1.1764706
       2200.2500       2413.7500       0.0000000       0.0000000       1.1764706
"ICS"
       20.250000       26.750000     0.030134524    0.0013966268       1.0000000
       26.750000       36.250000     0.012677883   0.00058171937       1.0000000
       36.250000       49.250000    0.0079035210   0.00042477141       1.0000000
       49.250000       66.250000   0.00052796470   0.00091281557       1.0000000
       66.250000       89.750000    0.0037995710   0.00047022448       1.0000000
       89.750000       120.75000    0.0016984714   0.00041833492       1.0000000
       120.75000       163.25000    0.0015543280   0.00049238981       1.0000000
       163.25000       220.25000   0.00081868751   0.00060427227       1.0000000
       220.25000       297.75000    0.0013409669   0.00039566758       1.0000000
       297.75000       401.75000    0.0012210272   0.00049765260       1.0000000
       401.75000       505.25000    0.0021356286   0.00053756656       1.0000000
       505.25000       516.25000   0.00058281647   0.00017529754       1.0000000
       516.25000       731.75000   0.00042285567   0.00072111128       1.0000000
       731.75000       987.75000   0.00078763114   0.00033171103       1.1764706
       987.75000       1170.2500   0.00061267031   0.00018084143       1.1764706
       1170.2500       1176.2500   1.1327077e-16   3.9697652e-05       1.1764706
       1176.2500       1329.7500   0.00016343718   0.00012504986       1.1764706
       1329.7500       1335.7500   1.0926533e-05   3.8638795e-05       1.1764706
       1335.7500       1400.2500   0.00026245025   7.9710381e-05       1.1764706
       1400.2500       1600.2500   0.00044203067   0.00012033860       1.1764706
       1600.2500       1805.7500   0.00023293564   0.00011704313       1.1764706
       1805.7500       1812.2500   2.9735336e-05   3.4129182e-05       1.1764706
       1812.2500       2200.2500   0.00036347436   0.00014209009       1.1764706
       2200.2500       2413.7500   0.00014436881   9.5494632e-05       1.1764706
"240mu"
       20.250000       26.750000       0.0000000       0.0000000       1.0000000
       26.750000       36.250000       0.0000000       0.0000000       1.0000000
       36.250000       49.250000       0.0000000       0.0000000       1.0000000
       49.250000       66.250000       0.0000000       0.0000000       1.0000000
       66.250000       89.750000       0.0000000       0.0000000       1.0000000
       89.750000       120.75000       0.0000000       0.0000000       1.0000000
       120.75000       163.25000       0.0000000       0.0000000       1.0000000
       163.25000       220.25000       0.0000000       0.0000000       1.0000000
       220.25000       297.75000       0.0000000       0.0000000       1.0000000
       297.75000       401.75000       0.0000000       0.0000000       1.0000000
       401.75000       505.25000       0.0000000       0.0000000       1.0000000
       505.25000       516.25000       0.0000000       0.0000000       1.0000000
       516.25000       731.75000       0.0000000       0.0000000       1.0000000
       731.75000       987.75000       0.0000000       0.0000000       1.1764706
       987.75000       1170.2500       0.0000000       0.0000000       1.1764706
       1170.2500       1176.2500   0.00010431626   5.4061517e-05       1.1764706
       1176.2500       1329.7500       0.0000000       0.0000000       1.1764706
       1329.7500       1335.7500   5.6376692e-05   5.3249634e-05       1.1764706
       1335.7500       1400.2500       0.0000000       0.0000000       1.1764706
       1400.2500       1600.2500       0.0000000       0.0000000       1.1764706
       1600.2500       1805.7500       0.0000000       0.0000000       1.1764706
       1805.7500       1812.2500   0.00039080457   4.8236081e-05       1.1764706
       1812.2500       2200.2500       0.0000000       0.0000000       1.1764706
       2200.2500       2413.7500       0.0000000       0.0000000       1.1764706
     */


     double data_all[][5]=                                                                  //DONE
  {
    20.250000 ,     26.750000,   0.035531153,    0.0019269991,       1.0000000,
    26.750000,      36.250000,   0.020449433,    0.00077739729,      1.0000000,
    36.250000 ,     49.250000,   0.011203425,    0.00057562936,      1.0000000,
    49.250000,      66.250000,   0.0028262920,   0.0011595252,       1.0000000,
    66.25000,       89.750000,   0.0041031899,   0.00060727258,      1.0000000,
    89.750000   ,   120.75000,   0.0023679900,   0.00051900478,      1.0000000,
    120.75000 ,     163.25000,   0.0029467333,   0.00060716008,      1.0000000,
    163.25000 ,     220.25000,   0.0024583629,   0.00073218657,      1.0000000,
    220.25000,      297.75000,   0.0028625643,   0.00048728282,      1.0000000,
    297.75000,      401.75000,   0.0034334931,   0.00060245839,      1.0000000,
    401.75000,      505.25000,   0.0037264718,   0.00065961499,      1.0000000,
    505.25000,      516.25000,   0.0015231997,   0.00022491983,      1.0000000,
    516.25000,      731.75000,   0.0015045000,   0.00089720516,      1.0000000,
    731.75000,      987.75000,   0.00078763114,  0.00033171103,      1.1764706,
    987.75000,      1170.2500,   0.00061267031,  0.00018084143,      1.1764706,
    1170.2500,      1176.2500,   0.00010431626,  6.7071240e-05,      1.1764706,
    1176.2500,      1329.7500,   0.00016343718,  0.00012504986,      1.1764706,
    1329.7500,      1335.7500,   6.7303226e-05,  6.5791185e-05,      1.1764706,
    1335.7500,      1400.2500,   0.00026245025,  7.9710381e-05,      1.1764706,
    1400.2500,      1600.2500,   0.00044203067,  0.00012033860,      1.1764706,
    1600.250,       1805.7500,   0.00023293564,  0.00011704313,      1.1764706,
    1805.750,       1812.2500,   0.00042053991,  5.9089090e-05,      1.1764706,
    1812.250,       2200.2500,   0.00036347436,  0.00014209009,      1.1764706,
    2200.250,       2413.7500,   0.00014436881,  9.5494632e-05,      1.1764706
  };


     double data_gaussian[][5]=                                                          //DONE
  {
    20.250000,      26.750000,      0.0000000,      0.0000000,      1.0000000,
    26.750000,      36.250000,      0.0000000,      0.0000000,      1.000000,
    36.250000,      49.250000,      0.0000000,      0.0000000,      1.0000000,
    49.250000,      66.250000,      0.0000000,      0.0000000,      1.0000000,
    66.250000,      89.750000,      0.0000000,      0.0000000,      1.0000000,
    89.750000,      120.75000,      0.0000000,      0.0000000,      1.0000000,
    120.75000 ,     163.25000,   4.8499837e-16,     0.00012792132,  1.0000000,
    163.25000,      220.25000,   3.9737616e-07,     0.00015363004,  1.0000000,
    220.25000,      297.75000,   0.00024467476,     0.00011894785,  1.0000000,
    297.75000,      401.75000,   0.00080721967,     0.00012336651,  1.0000000,
    401.75000,      505.25000,   0.0012548913,      0.00016031252,  1.0000000,
    505.25000,      516.25000,   0.00089042942,     6.6619241e-05,  1.0000000,
    516.25000,      731.75000,   0.00030950794,     0.00023824107,  1.0000000,
    731.75000,      987.75000,      0.0000000,      0.0000000,      1.1764706,
    987.75000,      1170.2500,      0.0000000,      0.0000000,      1.1764706,
    1170.2500,      1176.2500,      0.0000000,      0.0000000,      1.1764706,
    1176.2500,      1329.7500,      0.0000000,      0.0000000,      1.1764706,
    1329.7500,      1335.7500,      0.0000000,      0.0000000,      1.1764706,
    1335.7500,      1400.2500,      0.0000000,      0.0000000,      1.1764706,
    1400.2500,      1600.2500,      0.0000000,      0.0000000,      1.1764706,
    1600.2500,      1805.7500,      0.0000000,      0.0000000,      1.1764706,
    1805.7500,      1812.2500,      0.0000000,      0.0000000,      1.1764706,
    1812.2500,      2200.2500,      0.0000000,      0.0000000,      1.1764706,
    2200.2500,      2413.7500,      0.0000000,      0.0000000,      1.1764706
  };

     double data_dirbe   [][5]=                                  //DONE
 {
   20.250000,       26.750000,   0.0053966291,   0.0013276893,       1.0000000,
   26.750000,       36.250000,   0.0077715499,   0.00051570254,      1.0000000,
   36.250000,       49.250000,   0.0032999041,   0.00038848219,      1.0000000,
   49.250000,       66.250000,   0.0022983273,   0.00071502900,      1.0000000,
   66.250000,       89.750000,   0.00030361884,  0.00038427716,      1.0000000,
   89.750000,      120.75000,    0.00066951866,  0.00030718375,      1.0000000,
   120.75000,      163.25000,    0.0013924053,   0.00033140878,      1.0000000,
   163.25000,      220.25000,    0.0016392780,   0.00038386196,      1.0000000,
   220.25000,      297.75000,    0.0012769227,   0.00025834690,      1.0000000,
   297.75000,      401.75000,    0.0014052462,   0.00031635218,      1.0000000,
   401.75000,      505.25000,    0.00033595179,  0.00034700724,      1.0000000,
   505.25000,      516.25000,    4.9953842e-05,  0.00012418364,      1.0000000,
   516.25000,      731.75000,    0.00077213642,  0.00047772044,      1.0000000,
   731.75000,      987.75000,      0.0000000,      0.0000000,      1.1764706,
   987.75000,      1170.2500,      0.0000000,      0.0000000,      1.1764706,
   1170.2500,      1176.2500,      0.0000000,      0.0000000,      1.1764706,
   1176.2500,      1329.7500,      0.0000000,      0.0000000,      1.1764706,
   1329.7500,      1335.7500,      0.0000000,      0.0000000,      1.1764706,
   1335.7500,      1400.2500,      0.0000000,      0.0000000,      1.1764706,
   1400.2500,      1600.2500,      0.0000000,      0.0000000,      1.1764706,
   1600.2500,      1805.7500,      0.0000000,      0.0000000,      1.1764706,
   1805.7500,      1812.2500,      0.0000000,      0.0000000,      1.1764706,
   1812.2500,      2200.2500,      0.0000000,      0.0000000,      1.1764706,
   2200.2500,      2413.7500,      0.0000000,      0.0000000,      1.1764706
 };


     double data_ics     [][5]=                                                         //DONE
 {
   20.250000,       26.750000,   0.030134524,     0.0013966268,        1.0000000,
   26.750000,       36.250000,   0.012677883,     0.00058171937,       1.0000000,
   36.250000,       49.250000,   0.0079035210,    0.00042477141,       1.0000000,
   49.250000,       66.250000,   0.00052796470,   0.00091281557,       1.0000000,
   66.250000,       89.750000,   0.0037995710,    0.00047022448,       1.0000000,
   89.750000,       120.75000,   0.0016984714,    0.00041833492,       1.0000000,
   120.75000,       163.25000,   0.0015543280,    0.00049238981,       1.0000000,
   163.25000,       220.25000,   0.00081868751,   0.00060427227,       1.0000000,
   220.25000,       297.75000,   0.0013409669,    0.00039566758,       1.0000000,
   297.75000,       401.75000,   0.0012210272,    0.00049765260,       1.0000000,
   401.75000,       505.25000,   0.0021356286,    0.00053756656,       1.0000000,
   505.25000,       516.25000,   0.00058281647,   0.00017529754,       1.0000000,
   516.25000,       731.75000,   0.00042285567,   0.00072111128,       1.0000000,
   731.75000,       987.75000,   0.00078763114,   0.00033171103,       1.1764706,
   987.75000,       1170.2500,   0.00061267031,   0.00018084143,       1.1764706,
   1170.2500,       1176.2500,   1.1327077e-16,   3.9697652e-05,       1.1764706,
   1176.2500,       1329.7500,   0.00016343718,   0.00012504986,       1.1764706,
   1329.7500,       1335.7500,   1.0926533e-05,   3.8638795e-05,       1.1764706,
   1335.7500,       1400.2500,   0.00026245025,   7.9710381e-05,       1.1764706,
   1400.2500,       1600.2500,   0.00044203067,   0.00012033860,       1.1764706,
   1600.2500,       1805.7500,   0.00023293564,   0.00011704313,       1.1764706,
   1805.7500,       1812.2500,   2.9735336e-05,   3.4129182e-05,       1.1764706,
   1812.2500,       2200.2500,   0.00036347436,   0.00014209009,       1.1764706,
   2200.2500,       2413.7500,   0.00014436881,   9.5494632e-05,       1.1764706
 };

     double data_line    [][5]=                                       //using more accurate values from mail above, not from table                 
 {
   20.250000,      26.750000,      0.0000000,      0.0000000,      1.0000000,
   26.750000,      36.250000,      0.0000000,      0.0000000,      1.000000,
   36.250000,      49.250000,      0.0000000,      0.0000000,      1.000000,
   49.250000,      66.250000,      0.0000000,      0.0000000,      1.0000000,
   66.250000,      89.750000,      0.0000000,      0.0000000,      1.0000000,
   89.750000,      120.75000,      0.0000000,      0.0000000,      1.0000000,
   120.75000,      163.25000,      0.0000000,      0.0000000,      1.0000000,
   163.25000,      220.25000,      0.0000000,      0.0000000,      1.0000000,
   220.25000,      297.75000,      0.0000000,      0.0000000,      1.0000000,
   297.75000,      401.75000,      0.0000000,      0.0000000,      1.0000000,
   401.75000,      505.25000,      0.0000000,      0.0000000,      1.0000000,
   505.25000,      516.25000,      0.0000000,      0.0000000,      1.0000000,
   516.25000,      731.75000,      0.0000000,      0.0000000,      1.0000000,
   731.75000,      987.75000,      0.0000000,      0.0000000,      1.1764706,
   987.75000,      1170.2500,      0.0000000,      0.0000000,      1.1764706,
   1170.2500,      1176.2500,      0.00010625607,  3.7268001e-05,  1.1764706,
   1176.2500,      1329.7500,      0.0000000,      0.0000000,      1.1764706,
   1329.7500,      1335.7500,      6.8654161e-05,  3.6523392e-05,  1.1764706,
   1335.7500,      1400.2500,      0.0000000,      0.0000000,      1.1764706,
   1400.2500,      1600.2500,      0.0000000,      0.0000000,      1.1764706,
   1600.2500,      1805.7500,      0.0000000,      0.0000000,      1.1764706,
   1805.7500,      1812.2500,      0.00044409527,  3.1696206e-05,  1.1764706,
   1812.2500,      2200.2500,      0.0000000,      0.0000000,      1.1764706,
   2200.2500,      2413.7500,      0.0000000,      0.0000000,      1.1764706
 };

     for(int i_energy=0;i_energy<n_energy;i_energy++) { data_line[i_energy][2]*=0.948148; data_line[i_energy][3]*=0.948148;    } // factor given in email


 double *gaussian,    *dirbe,    *ics,    *line,    *total;
 double *gaussian_err,*dirbe_err,*ics_err,*line_err,*total_err;

   n_energy=24;
   E    =           new double[n_energy   ];
  dE    =           new double[n_energy   ];
   Emin =           new double[n_energy   ];
   Emax =           new double[n_energy   ];
   gaussian  =      new double[n_energy   ];
   gaussian_err   = new double[n_energy   ];
   dirbe  =         new double[n_energy   ];
   dirbe_err   =    new double[n_energy   ];
   ics  =           new double[n_energy   ];
   ics_err   =      new double[n_energy   ];
   line =           new double[n_energy   ];
   line_err =       new double[n_energy   ];
   total=           new double[n_energy   ];
   total_err =      new double[n_energy   ];


   for ( i=0;i<n_energy;i++)
   {
    Emin[i]        =data_gaussian[i][0];
    Emax[i]        =data_gaussian[i][1];
    gaussian    [i]=data_gaussian[i][2];
    gaussian_err[i]=data_gaussian[i][3];

       dirbe    [i]=data_dirbe   [i][2];
       dirbe_err[i]=data_dirbe   [i][3];

         ics    [i]=data_ics     [i][2];
         ics_err[i]=data_ics     [i][3];

        line    [i]=data_line    [i][2];
        line_err[i]=data_line    [i][3];


    cout<<"Emin[i]="<<Emin[i]<< " Emax[i]="<<Emax[i]
        <<" MeV gaussian= "<<gaussian[i]<<" gaussian_err= "<<gaussian_err[i]
      
        <<"    ics= "<<ics[i]<<" ics_err= "<<ics_err[i]
        <<"       dirbe= "<<dirbe[i]<<" dirbe_err= "<<dirbe_err[i]
        <<"       line = "<<line [i]<<"  line_err= "<< line_err[i]
      /*
	<<" gaussian_intensity_min="<< gaussian_intensity_min 	<<" gaussian_intensity_max="<< gaussian_intensity_max
	<<"    dirbe_intensity_min="<<    dirbe_intensity_min 	<<"    dirbe_intensity_max="<<    dirbe_intensity_max
	<<"      ics_intensity_min="<<      ics_intensity_min 	<<"      ics_intensity_max="<<      ics_intensity_max
	<<"    total_intensity_min="<<    total_intensity_min 	<<"    total_intensity_max="<<    total_intensity_max
      */
        <<endl;

   }


   int redistribute = 1; // put ics 511 keV into dirbe component instead
   int plot_gaussian = 0;
   int plot_ics      = 0;
   int plot_dirbe    = 0;
   int plot_lines    = 1;
   int plot_total    = 1;

 for(i=0;i<n_energy;i++)
   {
     Emin[i]*=1.e-3; // keV -> MeV
     Emax[i]*=1.e-3; // keV -> MeV
     E[i]=sqrt(Emax[i]*Emin[i]);
    dE[i]=     Emax[i]-Emin[i] ;

    double   gaussian_intensity_min= (  gaussian[i]-  gaussian_err[i])/dE[i] / solid_angle;
    double   gaussian_intensity_max= (  gaussian[i]+  gaussian_err[i])/dE[i] / solid_angle;

    double   ics_intensity_min= (  ics[i]-  ics_err[i])/dE[i] / solid_angle;
    double   ics_intensity_max= (  ics[i]+  ics_err[i])/dE[i] / solid_angle;

    double dirbe_intensity_min= (dirbe[i]-dirbe_err[i])/dE[i] / solid_angle;
    double dirbe_intensity_max= (dirbe[i]+dirbe_err[i])/dE[i] / solid_angle;

    double  line_intensity_min= ( line[i]- line_err[i])/dE[i] / solid_angle;
    double  line_intensity_max= ( line[i]+ line_err[i])/dE[i] / solid_angle;

    double total_intensity_min= gaussian_intensity_min +  dirbe_intensity_min +  ics_intensity_min;// + line_intensity_min;
    double total_intensity_max= gaussian_intensity_max +  dirbe_intensity_max +  ics_intensity_max;// + line_intensity_max; 

    if(redistribute==1)
      {
	if(Emin[i]>0.5050&&Emax[i]<0.5165) // select 0.511  MeV bin and move the flux from ics to dirbe
	  {
	    dirbe_intensity_min+= ics_intensity_min;
	    dirbe_intensity_max+= ics_intensity_max;
	                          ics_intensity_min=0.0;
	                          ics_intensity_max=0.0;
          }
      }
    
    // move 0.511 MeV line from total to lines. Use dirbe as assigned above for 0.511 MeV
	if(Emin[i]>0.5050&&Emax[i]<0.5165) // select 0.511  MeV line
	  {
	     line_intensity_min = gaussian_intensity_min + dirbe_intensity_min;
	     line_intensity_max = gaussian_intensity_max + dirbe_intensity_max;
	                          total_intensity_min=0.0;
	                          total_intensity_max=0.0;
          }

    // remove line ranges from total, since these have huge errors for continuum
    if(Emin[i]>1.170&&Emax[i]<1.177||Emin[i]>1.329&&Emax[i]<1.336||Emin[i]>1.805&&Emax[i]<1.813   )
      {
	total_intensity_min=1.e-20;
	total_intensity_max=1.e-20;
      }

    cout<<"Emin[i]="<<Emin[i]<< " Emax[i]="<<Emax[i]   <<" E="<< E[i]
        <<" MeV gaussian= "<<gaussian[i]<<" gaussian_err= "<<gaussian_err[i]
        <<"    ics= "<<ics[i]<<" ics_err= "<<ics_err[i]
        <<"       dirbe= "<<dirbe[i]<<" dirbe_err= "<<dirbe_err[i]
	<<" gaussian_intensity_min="<< gaussian_intensity_min 	<<" gaussian_intensity_max="<< gaussian_intensity_max
	<<"    dirbe_intensity_min="<<    dirbe_intensity_min 	<<"    dirbe_intensity_max="<<    dirbe_intensity_max
	<<"      ics_intensity_min="<<      ics_intensity_min 	<<"      ics_intensity_max="<<      ics_intensity_max
	<<"    total_intensity_min="<<    total_intensity_min 	<<"    total_intensity_max="<<    total_intensity_max
        <<endl;

    if(gaussian_intensity_min<=0.0)  gaussian_intensity_min=1.e-20;// avoid negative logs
    if(     ics_intensity_min<=0.0)       ics_intensity_min=1.e-20;// avoid negative logs
    if(   dirbe_intensity_min<=0.0)     dirbe_intensity_min=1.e-20;// avoid negative logs


   double shift=1.0; // tiny shift to make points visible and not obscuring each other

   // the Gaussian has the 511 keV line where the energy is exact so no shift
   spectrum=new TGraph(2);
   spectrum   ->SetPoint(0,E[i], gaussian_intensity_min * pow(E[i],2) ); 
   spectrum   ->SetPoint(1,E[i], gaussian_intensity_max * pow(E[i],2) );

   spectrum->SetMarkerColor(kMagenta  );// 
   spectrum->SetMarkerStyle(22); // triangles
   spectrum->SetMarkerSize (0.5);//
   spectrum->SetLineColor(kMagenta);// kCyan kMagenta
   spectrum->SetLineWidth(1     );
   spectrum->SetLineStyle(1      );//1=solid 2=dash 3=dot 4=dash-dot
   if(plot_gaussian==1)spectrum->Draw("L"); 
 
   shift=0.97;
   spectrum=new TGraph(2);
   spectrum   ->SetPoint(0,E[i]*shift, dirbe_intensity_min * pow(E[i],2) ); 
   spectrum   ->SetPoint(1,E[i]*shift, dirbe_intensity_max * pow(E[i],2) );

   spectrum->SetMarkerColor(kBlue  );// 
   spectrum->SetMarkerStyle(22); // triangles
   spectrum->SetMarkerSize (0.5);//
   spectrum->SetLineColor(kBlue);// kCyan kMagenta
   spectrum->SetLineWidth(1     );
   spectrum->SetLineStyle(1      );//1=solid 2=dash 3=dot 4=dash-dot
   if(plot_dirbe==1)spectrum->Draw("L"); 

   shift=0.98;
   spectrum=new TGraph(2);
   spectrum   ->SetPoint(0,E[i]*shift, ics_intensity_min * pow(E[i],2) ); 
   spectrum   ->SetPoint(1,E[i]*shift, ics_intensity_max * pow(E[i],2) );

   spectrum->SetMarkerColor(kBlack  );// 
   spectrum->SetMarkerStyle(22); // triangles
   spectrum->SetMarkerSize (0.5);//
   spectrum->SetLineColor(kGreen);// kCyan kMagenta
   spectrum->SetLineWidth(1     );
   spectrum->SetLineStyle(1      );//1=solid 2=dash 3=dot 4=dash-dot
   if(plot_ics==1)spectrum->Draw("L");  

   shift=1.00;
   spectrum=new TGraph(2);
   spectrum   ->SetPoint(0,E[i]*shift, line_intensity_min * pow(E[i],2) ); 
   spectrum   ->SetPoint(1,E[i]*shift, line_intensity_max * pow(E[i],2) );

   spectrum->SetMarkerColor(kBlack  );// 
   spectrum->SetMarkerStyle(22); // triangles
   spectrum->SetMarkerSize (0.5);//
   spectrum->SetLineColor(kBlue);// kCyan kMagenta
   spectrum->SetLineWidth(1     );
   spectrum->SetLineStyle(1      );//1=solid 2=dash 3=dot 4=dash-dot
   if(plot_lines==1)spectrum->Draw("L");  


   shift=0.99; // tiny shift to make points visible and not obscuring each other
   spectrum=new TGraph(2);
   spectrum   ->SetPoint(0,E[i]*shift, total_intensity_min * pow(E[i],2) ); 
   spectrum   ->SetPoint(1,E[i]*shift, total_intensity_max * pow(E[i],2) );

   spectrum->SetMarkerColor(kMagenta );              //AWS20100924 
   spectrum->SetMarkerStyle(22); // triangles
   spectrum->SetMarkerSize (0.5);//
   spectrum->SetLineColor(kMagenta);// kCyan kMagenta  AWS20100924
   spectrum->SetLineWidth(2     );
   spectrum->SetLineStyle(1      );//1=solid 2=dash 3=dot 4=dash-dot
   if(plot_total==1)spectrum->Draw("L"); 

   }// i



   }// mode=2012


  cout<<" <<<< plot_SPI_spectrum_bouchet    "<<endl;
   return status;
}

