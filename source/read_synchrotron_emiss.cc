

#include"Galplot.h"                  
#include"galprop_classes.h"
#include"galprop.h"

#include "fitsio.h" 

int Galplot::read_synchrotron_emiss()
{
int stat;

 char   comment[100];

  cout<<" >>>> read_synchrotron_emiss"<<endl;

stat=0;
   fitsfile *fptr;       /* pointer to the FITS file; defined in fitsio.h */
    int status, ii, jj;
    long  fpixel = 1, naxis = 4, nelements, exposure;
    long naxes[5]  ; 

  
     naxis=3;

    if (galaxy.n_spatial_dimensions==2)    naxes[0] = galaxy.n_rgrid;
    if (galaxy.n_spatial_dimensions==3)    naxes[0] = galaxy.n_xgrid;

                                           naxes[1] = galaxy.n_zgrid;   
                                           naxes[2]=  galaxy.n_nu_synchgrid;
   

     nelements=naxes[0]*naxes[1]*naxes[2];
    
     cout<<"read_synchrotron_emiss: nelements="<<nelements<<endl;
  
    
    float *array;          
    array=new float[nelements];

    char  infile[100];
  
    strcpy( infile,configure.fits_directory);
 
   strcat( infile,"synchrotron_emiss_"); 

   
    strcat( infile,galdef.galdef_ID);
    cout<<"  reading  synchrotron emiss  from file "<<infile<<endl;

    status = 0;         /* initialize status before calling fitsio routines */
    fits_open_file(&fptr,  infile,READONLY, &status);   
    cout<<"  fits open status = "<<status<<endl;



    /* Read the array of floats  */
    float nulval=0;
    int anynul;
    fits_read_img(fptr, TFLOAT, fpixel, nelements, &nulval,array, &anynul,&status);
 
  

  

   int i=0; 
   for (int ip       =0;        ip<naxes[2];       ip++)
   for (int iz       =0;        iz<naxes[1];       iz++)
   for (int ir       =0;        ir<naxes[0];       ir++)
   {             
     //    cout<<array[i]<<" ir="<<ir<<" iz="<<iz<<" ip="<<ip<<endl;
     // see galprop store_synch_emiss.cc

	         if (galaxy.n_spatial_dimensions==2)   galaxy.synchrotron_emiss.d2[ir]   [iz].s[ip]=array[i];
	         if (galaxy.n_spatial_dimensions==3)   galaxy.synchrotron_emiss.d3[ir][0][iz].s[ip]=array[i]; 
                      
     i++;
   }


   //      galaxy.synchrotron_emiss .print();

    fits_close_file(fptr, &status);            /* close the file */

    fits_report_error(stderr, status);  /* print out any error messages */


    delete[] array; 






 








  cout<<" <<<< read_synchrotron_emiss"<<endl;

    return( status );
return stat;
}
