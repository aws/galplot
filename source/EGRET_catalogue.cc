

using namespace std;
#include<iostream>  
#include<fstream>
#include<string>    
#include <cstring>  //AWS20111010 for sles11 gcc 4.3.4

#include"EGRET_catalogue.h"

//////////////////////////////////////////////////////////////////////////
int EGRET_catalogue::read(int debug)
{

   cout<<" >>>>  EGRET_catalogue.read "<<endl;

  
   int stat=0;
   char  filename[100];
   char input[1000];
   FILE *ft;
   char *flag;
   int  found;
   char dumstring1[1000],dumstring2[11  ];
   int i_source;
   
   
   double F_,dF_,counts_,sqrtTS_;
   char vp_[10];




   n_source=271;
   ra    = new double[n_source];
   dec   = new double[n_source];
   l     = new double[n_source];
   b     = new double[n_source];
   theta = new double[n_source];
   F     = new double[n_source];
   dF    = new double[n_source];
   index = new double[n_source];
   counts= new double[n_source];
   sqrtTS= new double[n_source];
   
   name   =new char*[n_source]; // array of pointer to char
   vp     =new char*[n_source];
   ID    = new char*[n_source];

   for(i_source=0; i_source<n_source;i_source++)
   {
    name[i_source]=new char[10];
    vp  [i_source]=new char[10];
    ID  [i_source]=new char[ 2];
   }

   selected= new int[n_source];

   //--------------------------------------------------------------

   strcpy(filename,"../FITS/3EG_catalogue.txt");

 
   ft=fopen(filename,"r");
   if(ft==NULL)
   {
      cout<<"no  file called "<<filename<<endl; return -1;
   }

   flag=" ";// initialize to non-NULL

   i_source=-1;

   while(flag!=NULL)
   {
      flag=fgets(input,1000,ft); // read string until newline (Schildt p.222)
      //      cout<<"+ "<<input<<endl;

      // find data lines by searching for comma

      if(strstr(input,",")!=NULL )
      {


       sscanf(input,"%s"  ,dumstring1);
       found=strcmp(dumstring1,"3EG"); 

//     cout<<"dumstring1= "<<dumstring1<<endl;
//     cout<<" found="<<found<<endl;

	      	if(found==0)
	  {  
	    //      cout<<"found "<<input<<endl;

            i_source++;

	    //                                                       V             V  these blanks required to account for blank before comma in input 
	    //	    sscanf(input,"%4c%10c,              %le, %le, %le,%le,%le , %le,%le,%le , %le,     %le,   %6c,%1c"  ,
	    //          dumstring1,dumstring2, &ra,&dec,&l, &b, &theta,&F,&dF,&index,&counts,&sqrtTS,vp,ID);  // le for double

	    // better method, more control over format:
	    sscanf(input+ 4,"%s" ,name   [i_source] ); name[i_source][10]='\0'; // since if ra>=100 string includes ra in name
	    sscanf(input+15,"%le",&ra    [i_source]   );
	    sscanf(input+22,"%le",&dec   [i_source]   );
	    sscanf(input+29,"%le",&l     [i_source]     );
	    sscanf(input+36,"%le",&b     [i_source]     );
	    sscanf(input+43,"%le",&theta [i_source] );
	    sscanf(input+49,"%le",&F     [i_source] );
	    sscanf(input+56,"%le",&dF    [i_source] );
	    sscanf(input+61,"%le",&index [i_source] );
	    sscanf(input+70,"%le",&counts[i_source] );
	    sscanf(input+77,"%le",&sqrtTS[i_source] );
	    sscanf(input+85,"%s" , vp    [i_source]);
	    sscanf(input+93,"%1c" ,ID    [i_source]);    ID[i_source][1]='\0';

	    // eg %4c reads exactly 4 characters, no termination
	    // dumstring1[ 4]='\0'; // termination so that cout can print it correctly
	 
	   
 
	    if(debug==1)
	      {
	    cout<<"name="<<name[i_source]<<" ra="<<ra[i_source] <<" dec="<<dec[i_source]<<" l="<<l[i_source]<<" b="<<b[i_source]
		<<" theta="<<theta[i_source]<<" F="<<F[i_source]<<" dF="<<dF[i_source]
		<<" index="<<index[i_source]<<" counts="<<counts[i_source]<<" sqrtTS="<<sqrtTS[i_source]
		<<"    vp="<<vp[i_source]   <<"     ID="<<ID[i_source] <<endl;

	    if(strstr(vp[i_source],"P1234")!=NULL) cout<<name[i_source]<<  " P1234 found"<<endl;
           
	      }//if debug
	  }
		
			     
	      	if(found!=0)
	  {
	    //    cout<<"found# "<<input<<endl;
          
	    sscanf(input+49,"%le",&F_);
	    sscanf(input+56,"%le",&dF_);
	    sscanf(input+70,"%le",&counts_);
	    sscanf(input+77,"%le",&sqrtTS_);
	    sscanf(input+85,"%s" , vp_);

	  
	    if (debug==1)
	    cout<<"----------------------------------------------------------------"
                <<" F="<<F_<<" dF="<<dF_    
		<<" counts="<<counts_<<" sqrtTS="<<sqrtTS_
		<<"    vp="<<vp_
           <<endl;


	    if(strstr(vp_,"P1234")!=NULL)
	    {
             if(debug==1)cout<<name[i_source]<<": P1234 found"<<endl;
              F[i_source]= F_;
             dF[i_source]=dF_;
             strcpy(vp[i_source],vp_);
	    }
	  }
		
	}//if strstr(input,",")

      } // while flag

   fclose(ft);

   // deselect solar flare and LMC
   for(i_source=0; i_source<n_source;i_source++)
    {
                                          selected[i_source]=1;
     if(strstr(ID[i_source],"S")!=NULL)   selected[i_source]=0;// solar flare
     if(strstr(ID[i_source],"G")!=NULL)   selected[i_source]=0;// LMC
    }

   if(debug==1)
   {
    cout<<"number of sources read ="<<i_source + 1<<endl;
    cout<<"Catalogue for P1234 when available (all except 4 sources):"<<endl;
    for(i_source=0; i_source<n_source;i_source++)
    {
	    cout<<"Name="<<name[i_source]<<" ra="<<ra[i_source] <<" dec="<<dec[i_source]<<" l="<<l[i_source]<<" b="<<b[i_source]
		<<" theta="<<theta[i_source]<<" F="<<F[i_source]<<" dF="<<dF[i_source]
		<<" index="<<index[i_source]<<" counts="<<counts[i_source]<<" sqrtTS="<<sqrtTS[i_source]
		<<"    vp="<<vp[i_source]   <<"     ID="<<ID[i_source]<<" selected="<<selected[i_source] <<endl;
    }
   }// if debug

   cout<<" <<<<  EGRET_catalogue.read  "<<endl;
   return stat;
}
///////////////////////////////////////////////////////
int EGRET_catalogue::print(ofstream &txt_stream)
{
  int i_source;
 txt_stream<<" >>>>  EGRET_catalogue.print "<<endl;
 txt_stream<<"number of sources read ="<<n_source <<endl;
 txt_stream<<"Catalogue for P1234 when available (all except 4 sources):"<<endl;
 for(i_source=0; i_source<n_source;i_source++)
   {
     txt_stream<<"Name="<<name[i_source]<<" ra="<<ra[i_source] <<" dec="<<dec[i_source]<<" l="<<l[i_source]<<" b="<<b[i_source]
		<<" theta="<<theta[i_source]<<" F="<<F[i_source]<<" dF="<<dF[i_source]
		<<" index="<<index[i_source]<<" counts="<<counts[i_source]<<" sqrtTS="<<sqrtTS[i_source]
		<<"    vp="<<vp[i_source]   <<"     ID="<<ID[i_source]<<" selected="<<selected[i_source] <<endl;
   }
   txt_stream<<" <<<<  EGRET_catalogue.print  "<<endl;
   return 0;
}

///////////////////////////////////////////////////////
int EGRET_catalogue::select(int options)
{
  int i_source;
  int n_selected;

 cout<<" >>>>  EGRET_catalogue.select "<<endl;
 cout<<"number of sources read ="<<n_source <<endl;
 

  // deselect solar flare and LMC
   for(i_source=0; i_source<n_source;i_source++)
    {
                                          selected[i_source]=1;
     if(strstr(ID[i_source],"S")!=NULL)   selected[i_source]=0;// deselect solar flare
     if(strstr(ID[i_source],"G")!=NULL)   selected[i_source]=0;// deselect LMC

     if(options==1)
     if(strstr(ID[i_source],"A")!=NULL)   selected[i_source]=0;// deselect active galaxy definite detections

     if(options==2)
     if(strstr(ID[i_source],"A")==NULL)   selected[i_source]=0;// select   active galaxy definite detections
    }

   n_selected=0;
   for(i_source=0; i_source<n_source;i_source++)if (selected[i_source] ==1)n_selected++;

   if(options==0)cout<<"options="<<options<<" :   selecting  all sources (apart from solar flare and LMC):";
   if(options==1)cout<<"options="<<options<<" : deselecting  active galaxy definite detections:";
   if(options==2)cout<<"options="<<options<<" :   selecting  active galaxy definite detections only:";
   cout<<"number of sources selected = "<<n_selected<<endl;

   /*
 for(i_source=0; i_source<n_source;i_source++)
   {
	    cout<<"Name="<<name[i_source]<<" ra="<<ra[i_source] <<" dec="<<dec[i_source]<<" l="<<l[i_source]<<" b="<<b[i_source]
		<<" theta="<<theta[i_source]<<" F="<<F[i_source]<<" dF="<<dF[i_source]
		<<" index="<<index[i_source]<<" counts="<<counts[i_source]<<" sqrtTS="<<sqrtTS[i_source]
		<<"    vp="<<vp[i_source]   <<"     ID="<<ID[i_source] <<endl;
   }
   */
   cout<<" <<<<  EGRET_catalogue.select  "<<endl;
   return 0;
}


/*
///////////////////////////////////////////////////////
int main()
{
  EGRET_catalogue EGRET_catalogue_;
  EGRET_catalogue_.read(2);
  
  ofstream txt_stream;
//  txt_stream=new ofstream;
  txt_stream.open("testoutput.txt");
  txt_stream<<"egret catalogue"<<endl;
  
  EGRET_catalogue_.print(txt_stream);
  
return 0;
}
*/
