#include "Sources.h"
#include <string>
#include <algorithm>
#include <valarray>
#include "Skymap.h"
#include <cmath>
#include <CCfits/CCfits>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_multiroots.h>
#include <healpix_base.h>
#include <healpix_map.h>
#ifdef  HAVE_ALM_HEALPIX_TOOLS_H           //AWS20131128 healpix 2.20a
   #include <lsconstants.h> 
#endif
#include "Psf.h"
#include "Exposure.h"
#include <iostream>
#include <time.h>
#include "Utils.h"
#include <ostream>
#include <sstream>

static long tinit = time(NULL);
long tlast = time(NULL);
void printTime(const std::string &text){
	long now = time(NULL);
	std::cout<<text<<std::endl;
	std::cout<<"Time since last: "<<now-tlast<<std::endl;
	std::cout<<"Time since beginning: "<<now-tinit<<std::endl;
	tlast = now;
}

Sources::Sources(const std::string &fileName){
	read(fileName);
}

void Sources::read(const std::string &fileName){
	readFromFile(fileName);
	sort();
	//Create the name, index database
	fnameIndex.clear();
	for (int i = 0; i < fSources.size(); ++i){
		fnameIndex[fSources[i].name] = i;
	}
}

void Sources::write(const std::string &fileName){
	//Open the fits file, overwrite
	CCfits::FITS fits("!"+fileName, CCfits::Write);

	//Find the maximum length for the names
	int length = 0;
	for (int i = 0; i < fSources.size(); ++i){
		length = std::max(length,int(fSources[i].name.size()));
	}
	//Create the columns needed
	std::ostringstream strForm;
	strForm << length << "A";
	std::vector<std::string> colNames(11), colForm(11), colUnit(11);

        int             number_of_columns = 12;        //AWS20110831
	                number_of_columns = 18;        //AWS20120618 for RA, DEC, flux10000 e_break gamma1 gamma2
        colNames.resize(number_of_columns);            //AWS20110831
        colForm .resize(number_of_columns);            //AWS20110831
        colUnit .resize(number_of_columns);            //AWS20110831

	colNames[0] = "NickName";
	colForm[0] = strForm.str();
	colNames[1] = "GLON";
	colForm[1] = "E";
	colUnit[1] = "deg";
	colNames[2] = "GLAT";
	colForm[2] = "E";
	colUnit[2] = "deg";
	colNames[3] = "Flux100";
	colForm[3] = "E";
	colUnit[3] = "photon/cm**2/s";
	colNames[4] = "Flux_Density";
	colForm[4] = "E";
	colUnit[4] = "photon/cm**2/MeV/s";
	colNames[5] = "Pivot_Energy";
	colForm[5] = "E";
	colUnit[5] = "MeV";
	colNames[6] = "Spectral_Index";
	colForm[6] = "E";
	colNames[7] = "SP_TYPE";
	colForm[7] = "24A";
	//Find the maximum length for the spectral parameters
	length = 0;
	for (int i = 0; i < fSources.size(); ++i){
		length = std::max(length,int(fSources[i].spParameters.size()));
	}
	strForm.str("");
	strForm<< length << "E";
	colNames[8] = "SP_PARS";
	colForm[8] = strForm.str();
	//Find the maximum length for the spectral parameters uncertainty
	length = 0;
	for (int i = 0; i < fSources.size(); ++i){
		length = std::max(length,int(fSources[i].spParametersUnc.size()));
	}
	strForm.str("");
	strForm<< length << "E";
	colNames[9] = "SP_PARS_UNC";
	colForm[9] = strForm.str();
	colNames[10] = "FIT_TYPE";
	colForm[10] = "24A";

	colNames[11] = "Flux1000";            //AWS20110831
	colForm [11] = "E";                   //AWS20110831
	colUnit [11] = "photon/cm**2/s";      //AWS20110831


	colNames[12] = "Flux10000";           //AWS20120615
	colForm [12] = "E";                   //AWS20120615
	colUnit [12] = "photon/cm**2/s";      //AWS20120615

	colNames[13] = "RA";                  //AWS20120612
	colForm [13] = "E";                   //AWS20120612
	colUnit [13] = "deg";                 //AWS20120612

	colNames[14] = "DEC";                 //AWS20120612
	colForm [14] = "E";                   //AWS20120612
	colUnit [14] = "deg";                 //AWS20120612

	colNames[15] = "e_break";             //AWS20120618
	colForm [15] = "E";                   //AWS20120618
	colUnit [15] = "MeV";                 //AWS20120618

	colNames[16] = "gamma1" ;             //AWS20120618
	colForm [16] = "E";                   //AWS20120618
	colUnit [16] = "   ";                 //AWS20120618

	colNames[17] = "gamma2" ;             //AWS20120618
	colForm [17] = "E";                   //AWS20120618
	colUnit [17] = "   ";                 //AWS20120618


	//Create the table
	CCfits::Table *table = fits.addTable("CATALOG", fSources.size(), colNames, colForm, colUnit);

	//Convert the information to acceptable arrays
	std::vector<std::string> names(fSources.size());
	std::valarray<double> l(fSources.size()), b(fSources.size()), pivot(fSources.size());
	std::valarray<double> ra(fSources.size()), dec(fSources.size());                                     //AWS20120612
	std::valarray<double> flux100(fSources.size()), prefactor(fSources.size()), index(fSources.size());
	std::valarray<double> flux1000(fSources.size());                                                     //AWS20110831
	std::valarray<double> flux10000(fSources.size());                                                    //AWS20120530

	std::valarray<double> e_break  (fSources.size());                                                    //AWS20120618
	std::valarray<double> gamma1   (fSources.size());                                                    //AWS20120618
	std::valarray<double> gamma2   (fSources.size());                                                    //AWS20120618

	std::vector<std::string> fitTypes(fSources.size());
	std::vector<std::string> spTypes(fSources.size());
	std::vector<std::valarray<double> > spParameters(fSources.size());
	std::vector<std::valarray<double> > spParametersUnc(fSources.size());
	
	//Get the information from the sources vector
	for (int i=0; i < fSources.size(); ++i){
		names[i] = fSources[i].name;
		l[i] = fSources[i].l;
		b[i] = fSources[i].b;

               	ra [i] = fSources[i].ra;                                                                     //AWS20120612
		dec[i] = fSources[i].dec;                                                                    //AWS20120612

	      

		prefactor[i] = fSources[i].prefactor;
		flux100  [i] = fSources[i].flux100;
		flux1000 [i] = fSources[i].flux1000;                                                          //AWS20110831
		flux10000[i] = fSources[i].flux10000;                                                         //AWS20120615
 
		// convention used by SourcePopulation.cc 
                gamma1   [i] = fSources[i].spParameters[0];                                                   //AWS20120618
                e_break  [i] = fSources[i].spParameters[1];                                                   //AWS20120618
                gamma2   [i] = fSources[i].spParameters[2];                                                   //AWS20120618

		index[i] = fSources[i].index;
		pivot[i] = fSources[i].pivot;
		fitTypes[i] = fitTypeToStr(fSources[i].fitType);
		spTypes[i] = fitTypeToStr(fSources[i].spType);
		spParameters[i].resize(fSources[i].spParameters.size());
		spParameters[i] = fSources[i].spParameters;
		spParametersUnc[i].resize(fSources[i].spParametersUnc.size());
		spParametersUnc[i] = fSources[i].spParametersUnc;
	}

	//Write it to the columns
	table->column("NickName").write(names,1);
	table->column("GLON").write(l,1);
	table->column("GLAT").write(b,1);

	table->column("RA") .write(ra,1);                                                                     //AWS20120612
	table->column("DEC").write(dec,1);                                                                   //AWS20120612

	table->column("Flux_Density").write(prefactor,1);
	table->column("Flux100")  .write(flux100,1);
	table->column("Flux1000") .write(flux1000,1);                                                         //AWS20110831
	table->column("Flux10000").write(flux10000,1);                                                        //AWS20120615

	table->column("e_break"  ).write(e_break  ,1);                                                        //AWS20120618
	table->column("gamma1"   ).write(gamma1   ,1);                                                        //AWS20120618
	table->column("gamma2"   ).write(gamma2   ,1);                                                        //AWS20120618

	table->column("Spectral_Index").write(index,1);
	table->column("Pivot_Energy").write(pivot,1);
	table->column("SP_TYPE").write(spTypes,1);
	table->column("SP_PARS").writeArrays(spParameters,1);
	table->column("SP_PARS_UNC").writeArrays(spParametersUnc,1);
	table->column("FIT_TYPE").write(fitTypes,1);
}

Sources & Sources::operator = (const Sources &old) {
  //Avoid self-assignment
	if (this != &old) {
		//This has to be done differently, since valarray has such a bizarre
		//assignment operator
		fSources = old.fSources;
		//Fix the valarrays for spectral parameters.
		for (int i = 0; i < fSources.size(); ++i) {
			fSources[i].spParameters.resize(old.fSources[i].spParameters.size());
			fSources[i].spParameters = old.fSources[i].spParameters;
			fSources[i].spParametersUnc.resize(old.fSources[i].spParametersUnc.size());
			fSources[i].spParametersUnc = old.fSources[i].spParametersUnc;
		}
		fnameIndex = old.fnameIndex;
	}
	return (*this);
}

std::valarray<double> Sources::genSpectra(int sourceNumber, const std::valarray<double> & energies) const{
   //We must be within bounds
   if (sourceNumber < 0 || sourceNumber > fSources.size()) {
      std::cerr<<"Source number out of bounds when generating spectra"<<std::endl;
      return std::valarray<double>();
   }

   //Get the source
   const Source &source = fSources[sourceNumber];

   //Create the spectra, depending on the spectral type
   std::valarray<double> spectra(energies.size());
   switch(source.spType) {
      case POWERLAWEXPCUTOFF:
	 for (size_t i = 0; i < energies.size(); ++i){
	    spectra[i] = source.spParameters[0]*pow(energies[i]/source.pivot, source.spParameters[1])*exp(-energies[i]/source.spParameters[2]);
	 }
	 break;
      case POWERLAW:
	 for (size_t i = 0; i < energies.size(); ++i){
	    spectra[i] = source.spParameters[0]*pow(energies[i]/source.pivot, source.spParameters[1]);
	 }
	 break;
      case BROKENPOWERLAW:
	 for (size_t i = 0; i < energies.size(); ++i){
	    if (energies[i] < source.spParameters[3]) {
	       spectra[i] = source.spParameters[0]*pow(energies[i]/source.spParameters[3], source.spParameters[1]);
	    } else {
	       spectra[i] = source.spParameters[0]*pow(energies[i]/source.spParameters[3], source.spParameters[2]);
	    }
	 }
	 break;
      case FREE:
	 //Begin by creating the reference power law spectra (this might change
	 //in future versions of the source catalog definition)
	 for (size_t i = 0; i < energies.size(); ++i) {
	    spectra[i] = source.prefactor*pow(energies[i]/source.pivot, source.index);
	 }

	 //Create two arrays, one for energies and another for prefactors
	 std::valarray<double> senergies(source.spParameters.size()/2);
	 std::valarray<double> prefactors(source.spParameters.size()/2);
	 for (int i = 0; i < source.spParameters.size()/2; ++i) {
	    senergies[i] = source.spParameters[2*i];
	    //We should have a minimum of 1e-10 for the prefactors
	    prefactors[i] = std::max(1e-10, source.spParameters[2*i+1]);
	    //The prefactor should also not be smaller than 1/4th of the uncertainty
	    //prefactors[i] = std::max(source.spParametersUnc[2*i+1]/4., prefactors[i]);
	 }
	 //If there is only one pair, use that for all energies
	 if (energies.size() == 1) {
	    spectra *= prefactors[0];
	 } else {
	    for (size_t i = 0; i < energies.size(); ++i) {
	       double scale;
	       //Handle extrapolation by using the source power law, joined to
	       //the last point.  Should be more stable than extrapolating from
	       //last two points
	       if (energies[i] < senergies[0]){
		  /*
		  //Old method of fitting a few points
		  size_t ul = std::min(size_t(2),senergies.size());
		  //Calculate the fit in log scale
		  std::valarray<double> logen(ul), logpre(ul);
		  logen = log(static_cast<std::valarray<double> >(senergies[std::slice(0,ul,1)]));
		  logpre = log(static_cast<std::valarray<double> >(prefactors[std::slice(0,ul,1)]));
		  double sumx = logen.sum();
		  double sumx2 = pow(logen, 2.0).sum();
		  double sumy = logpre.sum();
		  double sumxy = (logen*logpre).sum();
		  double pref = exp( (sumx*sumxy - sumy*sumx2) / (sumx*sumx - ul*sumx2));
		  double ind = (sumy*sumx - ul*sumxy) / (sumx*sumx - ul*sumx2);
		  */
		  /*
		  std::cout<<"prefactor: "<<pref<<", index: "<<ind<<", Pairs: ";
		  for (int j = 0; j < ul; ++j) {
		     std::cout<<"("<<logen[j]<<","<<logpre[j]<<") ";
		  }
		  std::cout<<std::endl;
		  */
		  //scale = pref*pow(energies[i],ind);
		  scale = prefactors[0];
	       } else if (energies[i] > senergies[senergies.size()-1]) {
		  /*
		  //Old method of fitting a few points
		  size_t ll = std::max(size_t(senergies.size()-2),size_t(0));
		  size_t len = std::min(size_t(2),senergies.size());
		  //Calculate the fit in log scale
		  std::valarray<double> logen(len), logpre(len);
		  logen = log(static_cast<std::valarray<double> >(senergies[std::slice(ll,len,1)]));
		  logpre = log(static_cast<std::valarray<double> >(prefactors[std::slice(ll,len,1)]));
		  double sumx = logen.sum();
		  double sumx2 = pow(logen, 2.0).sum();
		  double sumy = logpre.sum();
		  double sumxy = (logen*logpre).sum();
		  double pref = exp( (sumx*sumxy - sumy*sumx2) / (sumx*sumx - len*sumx2));
		  double ind = (sumy*sumx - len*sumxy) / (sumx*sumx - len*sumx2);
		  */
		  /*
		  std::cout<<"prefactor: "<<pref<<", index: "<<ind<<", Pairs: ";
		  for (int j = 0; j < len; ++j) {
		     std::cout<<"("<<logen[j]<<","<<logpre[j]<<") ";
		  }
		  std::cout<<std::endl;
		  */
		  //scale = pref*pow(energies[i],ind);
		  scale = prefactors[prefactors.size()-1];
	       } else {
		  //Use power law interpolation between points.
		  int ilow, iupp;
		  Utils::findIndexRange(senergies, energies[i], energies[i], ilow, iupp);
		  //Calculate the interpolation
		  double tmpInd = log(prefactors[iupp]/prefactors[ilow])/log(senergies[iupp]/senergies[ilow]);
		  double pref = pow(energies[i]/senergies[ilow], tmpInd);
		  scale = pref*prefactors[ilow];
	       }
#ifdef __APPLE__
	       if (std::isnan(scale) || std::isinf(scale)) 
#else
	       if (isnan(scale) || isinf(scale)) 
#endif
		  std::cerr<<"not a number in source spectra"<<std::endl;
	       //Just to keep within sane range
	       scale = std::max(1e-10,scale);
	       scale = std::min(1e+10,scale);
	       spectra[i] *= scale;
	    }
	 }
	 break;
   }

   return spectra;
}

void Sources::genFilterMaps(double lowFlux, Skymap<char> &filter, double fraction, const Psf &psf) const{
	int i = 0;
	while (i < fSources.size() && fSources[i].flux100 > lowFlux) i++;
	genFilterMaps(i, filter, fraction, psf);
}

void Sources::genFilterMaps(int numberOfSources, Skymap<char> &filter, double fraction, const Psf &psf) const{
	//Keep numbers within bounds
	if (numberOfSources > fSources.size()){
		numberOfSources = fSources.size();
	}
	//Create an array of sources to generate
	std::valarray<int> srcNumbers(numberOfSources);
	std::valarray<double> prCorr(1.0,numberOfSources), indCorr(0.0,numberOfSources);
	for (int i = 0; i < numberOfSources; ++i){
		srcNumbers[i] = i;
	}
	//Call genCountMaps with the array and get the actual map
	genFilterMaps(srcNumbers, filter, fraction, psf);
}

void Sources::genFilterMaps(const std::valarray<int> &sourceNumbers, Skymap<char> &filter, double fraction, const Psf &psf) const{
	//Status indicator
	Utils::StatusIndicator status("Filter map", sourceNumbers.size());
	for (int i = 0; i < sourceNumbers.size(); ++i){
		if (sourceNumbers[i] >= 0 && sourceNumbers[i] < fSources.size()){
			genFilterMap(sourceNumbers[i], filter, fraction, psf);
		}
		status.refresh();
	}
}

void Sources::genFilterMap(int sourceNumber, Skymap<char> &filter, double fraction, const Psf &psf) const{
	//Return if number out of bound
	if (sourceNumber < 0 || sourceNumber > fSources.size()) return;

	//Find the coordinates of the burst
	SM::Coordinate co(fSources[sourceNumber].l, fSources[sourceNumber].b);

	//Loop over the spectra of the filter and filter out the width of the
	//psf.  Use the lower boundary of the energies, if possible
	std::valarray<double> energy, eMax;
	if ( ! filter.getBoundaries(energy, eMax) ){
	   energy = filter.getSpectra();
	}
	std::vector<int> listpix;
	for (int i = 0; i < filter.nSpectra(); ++i){
	   double width = psf.getPsfWidth(energy[i], fraction)*degr2rad;
	   filter.query_disc_inclusive(co.healpixAng(), width, listpix);
	   for (int j = 0; j < listpix.size(); ++j) {
	      filter[listpix[j]][i] = 0;
	   }
	}
}

void Sources::genFluxMap(double lowFlux, Skymap<double> &skymap) const{
	int i = 0;
	while (i < fSources.size() && fSources[i].flux100 > lowFlux) i++;
	genFluxMap(i, skymap);
}

void Sources::genFluxMap(int numberOfSources, Skymap<double> &skymap) const{
	//0 or lower means all and we check that the upper boundary is properly set.
	if (numberOfSources <= 0 || numberOfSources > fSources.size()){
		numberOfSources = fSources.size();
	}
	for (int i = 0; i < numberOfSources; ++i){
		SM::Coordinate co(fSources[i].l, fSources[i].b);
		skymap[co] += genSpectra(i, skymap.getSpectra());
	}
}

std::valarray<double> Sources::flux2int(const std::valarray<double> &eMin, const std::valarray<double> &eMax, const std::valarray<double> &flux){
   //Start with an initial guess of the index of -2
   std::valarray<double> ind(2.0,eMin.size()-1);
   std::valarray<double> emean(eMin.size());
   std::valarray<double> output(eMin.size());
   std::valarray<double> old(eMin.size());
   
   emean = sqrt(eMin*eMax);
   //Calculate first approximation using the initial index estimate
   output[0] = flux[0]/emean[0]*(ind[0]-1)/(pow(eMin[0]/emean[0],1-ind[0])-pow(eMax[0]/emean[0],1-ind[0]));
   //std::cout<<"Iteration 0: "<<output[0]<<", ";
   for (int i = 1; i < eMin.size()-1; ++i){
      output[i] = flux[i]/emean[i]/((pow(eMin[i]/emean[i],1-ind[i-1])-1)/(ind[i-1]-1) + (1 - pow(eMax[i]/emean[i],1-ind[i]))/(ind[i]-1));
      //std::cout<<output[i]<<", ";
   }
   output[eMin.size()-1] = flux[eMin.size()-1]/emean[eMin.size()-1]*(ind[ind.size()-1]-1)/(pow(eMin[eMin.size()-1]/emean[eMin.size()-1],1-ind[ind.size()-1])-pow(eMax[eMin.size()-1]/emean[eMin.size()-1],1-ind[ind.size()-1]));
   //std::cout<<output[eMin.size()-1]<<std::endl;

   //Loop until convergence slows down enough
   for (int j = 0; j < 10000; ++j) {
      old = output;
      for (int i = 0; i < ind.size(); ++i){
	 ind[i] = -log(output[i+1]/output[i])/log(emean[i+1]/emean[i]);
      }
      if (fabs(ind[0]-1) > 1e-10) {
	 output[0] = flux[0]/emean[0]*(ind[0]-1)/(pow(eMin[0]/emean[0],1-ind[0])-pow(eMax[0]/emean[0],1-ind[0]));
      } else {
	 output[0] = flux[0]/emean[0]/log(eMax[0]/eMin[0]);
      }
      //std::cout<<"Iteration "<<j+1<<": "<<output[0]<<":"<<old[0]<<", ";
      for (int i = 1; i < eMin.size()-1; ++i){
	 double pre, post;
	 if (fabs(ind[i-1]-1) > 1e-10) {
	    pre = (pow(eMin[i]/emean[i],1-ind[i-1])-1)/(ind[i-1]-1);
	 } else {
	    pre = log(emean[i]/eMin[i]);
	 }
	 if (fabs(ind[i]-1) > 1e-10) {
	    post = (1 - pow(eMax[i]/emean[i],1-ind[i]))/(ind[i]-1);
	 } else {
	    post = log(eMax[i]/emean[i]);
	 }
	 output[i] = flux[i]/emean[i]/(pre + post);
	// std::cout<<output[i]<<":"<<old[i]<<", ";
      }
      if (fabs(ind[ind.size()-1]-1) > 1e-10) {
	 output[eMin.size()-1] = flux[eMin.size()-1]/emean[eMin.size()-1]*(ind[ind.size()-1]-1)/(pow(eMin[eMin.size()-1]/emean[eMin.size()-1],1-ind[ind.size()-1])-pow(eMax[eMin.size()-1]/emean[eMin.size()-1],1-ind[ind.size()-1]));
      } else {
	 output[eMin.size()-1] = flux[eMin.size()-1]/emean[eMin.size()-1]/log(eMax[eMin.size()-1]/eMin[eMin.size()-1]);
      }
      //std::cout<<output[eMin.size()-1]<<":"<<old[eMin.size()-1]<<std::endl;
      bool stop = true;
      for (int i = 0; i < flux.size(); ++i){
	 if (fabs(output[i]-old[i])/old[i] > 1e-5){
	    stop = false;
	    break;
	 }
      }
      if (stop) break;
   }

   return output;

   /* The old method, not stable
   //The root finder
   const gsl_multiroot_fsolver_type *T;
   gsl_multiroot_fsolver *s;

   //Set up the parameters
   fparams par;
   par.eMin.resize(eMin.size());
   par.eMin = eMin;
   par.eMax.resize(eMax.size());
   par.eMax = eMax;
   par.flux.resize(flux.size());
   par.flux = flux;

   const size_t n = 2*flux.size()-2;
   gsl_multiroot_function f = {&minFunc, n, &par};

   //The initial values
   gsl_vector *x = gsl_vector_alloc (n);
   //Calculate the values, assuming the intensity at central point is equal to
   //the flux/binsize
   for (int i = 0; i < flux.size() - 1; ++i) {
      double p0 = flux[i]/(eMax[i]-eMin[i]);
      gsl_vector_set(x,2*i,p0);
      double g0 = log(flux[i+1]/(eMax[i+1]-eMin[i+1])/p0)/log(sqrt(eMax[i+1]*eMin[i+1]/(eMax[i]*eMin[i])));
      gsl_vector_set(x,2*i+1,g0);
   }

   T = gsl_multiroot_fsolver_hybrids;
   s = gsl_multiroot_fsolver_alloc (T, n);
   gsl_multiroot_fsolver_set (s, &f, x);

   //We store only positive prefactors, for obvious reasons.  Should still be
   //better than the initial approximation
   //Convenient views of indexes and prefactores
   gsl_vector_view xpre = gsl_vector_subvector_with_stride(s->x,0,2,n/2);
   gsl_vector_view xind = gsl_vector_subvector_with_stride(s->x,1,2,n/2);
   //Storage for best root value with positive prefactors
   gsl_vector *xstore = gsl_vector_alloc (n);
   double fstore;
   //Begin by copying the initial values
   double fval = 0;
   for (int i = 0; i < n; ++i){
      fval += gsl_vector_get(s->f,i);
   }
   fstore = fval;
   gsl_vector_memcpy(xstore, s->x);
		      
   int iter = 0;
   int status;
   do
   {
      iter++;
      status = gsl_multiroot_fsolver_iterate (s);

      //Store the new root if all prefactors are positive
      if (gsl_vector_isnonneg(&xpre.vector) ){
	 fval = 0;
	 for (int i = 0; i < n; ++i){
	    fval += gsl_vector_get(s->f,i);
	 }
	 fstore = fval;
	 gsl_vector_memcpy(xstore, s->x);
      }


      if (status)   // check if solver is stuck
	 break;
      //std::cout<<"iter: "<<iter<<", ";
      //for (int i = 0; i < n/2; ++i){
//	 std::cout<<"("<<gsl_vector_get(s->x,2*i)<<","<<gsl_vector_get(s->x,2*i+1)<<")";
      //}
      //std::cout<<std::endl;
     
      status = gsl_multiroot_test_residual (s->f, 1e-9);
   }
   while (status == GSL_CONTINUE && iter < 1000);

   //std::cout<<"status = "<< gsl_strerror(status) <<std::endl;

   //populate the output
   size_t N = eMin.size();
   std::valarray<double> output(N);
   for (int i = 0; i < N-1; ++i) {
      output[i] = gsl_vector_get(xstore,2*i);
   }
   output[N-1] = output[N-2]*pow(sqrt(eMin[N-1]*eMax[N-1]/(eMin[N-2]*eMax[N-2])), gsl_vector_get(xstore,2*(N-1)-1));
     
   gsl_multiroot_fsolver_free (s);
   gsl_vector_free (x);
   gsl_vector_free (xstore);

   return output;
   */
}

int Sources::minFunc(const gsl_vector *x, void *params, gsl_vector *f){
   //Get the struct with the parameters
   fparams *p = static_cast<fparams*>(params);
   //Should assert the sizes of the valarrays.  Too lazy.
   int N = p->eMin.size();
   int n = 2*N-2;
   //Create the functions, we always have the endpoints
   double fv, p0, g0, p1, g1, e0, e1;
   p0 = gsl_vector_get(x,0);
   g0 = gsl_vector_get(x,1);
   e0 = sqrt(p->eMax[0]*p->eMin[0]);
   if (fabs(g0 + 1) > 1e-10) {
      fv = p0*e0/(g0+1)*(pow(p->eMax[0]/e0, g0+1) - pow(p->eMin[0]/e0, g0+1)) - p->flux[0];
   } else {
      fv = p0*e0*log(p->eMax[0]/p->eMin[0]) - p->flux[0];
   }
   gsl_vector_set(f,0,fv);
   p0 = gsl_vector_get(x,n-2);
   g0 = gsl_vector_get(x,n-1);
   e0 = sqrt(p->eMin[N-2]*p->eMax[N-2]);
   if (fabs(g0 + 1) > 1e-10) {
      fv = p0*e0/(g0+1)*(pow(p->eMax[N-1]/e0, g0+1) - pow(p->eMin[N-1]/e0, g0+1)) - p->flux[N-1];
   } else {
      fv = p0*e0*log(p->eMax[N-1]/p->eMin[N-1]) - p->flux[N-1];
   }
   gsl_vector_set(f,n-1,fv);
   //Now we loop the parts in between, if there are any
   for (int i = 2; i < n; i+=2) {
      //Start by reading both i-1 and i data
      p0 = gsl_vector_get(x,i-2);
      g0 = gsl_vector_get(x,i-1);
      p1 = gsl_vector_get(x,i);
      g1 = gsl_vector_get(x,i+1);
      e0 = sqrt(p->eMin[i/2-1]*p->eMax[i/2-1]);
      e1 = sqrt(p->eMin[i/2]*p->eMax[i/2]);
      //Creating the continuity function
      fv = p1 - p0*pow(e1/e0, g0);
      gsl_vector_set(f,i-1,fv);
      //And the integration function
      if ( fabs(g0+1) > 1e-10 ) {
	 fv = p0*e0/(g0+1)*(pow(e1/e0, g0+1) - pow(p->eMin[i/2]/e0, g0+1));
      } else {
	 fv = p0*e0*log(e1/p->eMin[i/2]);
      }
      if ( fabs(g1+1) > 1e-10 ) {
	 fv += p1*e1/(g1+1)*(pow(p->eMax[i/2]/e1, g1+1) - 1) - p->flux[i/2];
      } else {
	 fv += p1*e1*log(p->eMax[i/2]/e1) - p->flux[i/2];
      }
      gsl_vector_set(f,i,fv);
   }
   return GSL_SUCCESS;
}

bool Sources::SourceCompare::operator () (const Source &a, const Source &b){
	return a.flux100 > b.flux100;
}

void Sources::sort(){
	std::sort(fSources.begin(), fSources.end(), SourceCompare());
}

void Sources::readFromFile(const std::string & fileName){
	//Open the fits file, readonly
	CCfits::FITS fits(fileName);

	//Get the LAT point source catalog HDU
	CCfits::ExtHDU &table = fits.extension(1);

	//Read specific keywords to tell length
	int nRows;
	table.readKey("NAXIS2", nRows);

	std::vector<std::string> names(nRows);
	std::valarray<double> l(nRows), b(nRows), pivot(nRows);
	std::valarray<double> flux100(nRows), prefactor(nRows), index(nRows);
	std::valarray<double> flux1000(nRows); //AWS20110806
	std::valarray<double> flux10000(nRows);//AWS20120530
	std::valarray<double> ra(nRows), dec(nRows); //AWS20120612

        std::cout<<" starting reading first set of values"<<std::endl;

	//Try to guess the old or new format.  The SOURCE_NAME and NickName
	//columns should be the indicator
	try {
	     std::cout<<" trying SOURCE_NAME"<<std::endl;
   	   //Read in source names
   	   table.column("SOURCE_NAME").read(names, 1, nRows);
   	   //And then longitude and latitude
   	   table.column("L").read(l,1,nRows);
   	   table.column("B").read(b,1,nRows);
   	   //flux > 100 MeV, prefactor and spectral index next
   	   table.column("FLUX100").read(flux100,1,nRows);
   	   table.column("PREFACTOR").read(prefactor,1,nRows);
   	   table.column("SPECTRAL_INDEX").read(index,1,nRows);
	   //Set the pivot point to 100 MeV
	   pivot = 100;
	}catch (CCfits::Table::NoSuchColumn){           
	   try {
	     std::cout<<" trying Source_Name for 4FGL"<<std::endl; // 4FGL  AWS20200113
	      //Read in source names
	      table.column("Source_Name").read(names, 1, nRows);
	      //And then longitude and latitude
	      table.column("GLON").read(l,1,nRows);
	      table.column("GLAT").read(b,1,nRows);
	      //flux > 100 MeV, prefactor and spectral index next
	      //	      table.column("Flux100").read(flux100,1,nRows);  //AWS20140109 NB not in final 2FGL. Set later from energy ranges.
	      table.column("Flux1000").read(flux1000,1,nRows);//AWS20110806 2FGL
	      std::cout<<"read flux1000: first entry="<<flux1000[1]<<std::endl;

	      table.column("PL_Flux_Density").read(prefactor,1,nRows); //4FGL
	      table.column("PL_Index").read(index,1,nRows);            //4FGL
	      table.column("Pivot_Energy").read(pivot,1,nRows);        //4FGL

	      //	      table.column("Flux_Density").read(prefactor,1,nRows); //3FGL
	      //	      table.column("Spectral_Index").read(index,1,nRows);
	      //	      table.column("Pivot_Energy").read(pivot,1,nRows);
	   }catch (CCfits::Table::NoSuchColumn){
	      try {
		   std::cout<<" trying Source_Name for 3FGL"<<std::endl; // final official FSSC 2,3,4FGL
		 //Read in source names
		 table.column("Source_Name").read(names, 1, nRows);
		 //And then longitude and latitude
		 table.column("GLON").read(l,1,nRows);
		 table.column("GLAT").read(b,1,nRows);
		 //flux > 100 MeV, prefactor and spectral index next
		 table.column("Signif_Avg")    .read(flux100, 1,nRows);  // AWS20140109 dummy values, set later from energy ranges. Obsolete.
	         table.column("Flux1000")      .read(flux1000,1,nRows);  // AWS20140109 final 2FGL
		 table.column("Flux_Density")  .read(prefactor,1,nRows);
    //		 table.column("Spectral_Index").read(index,1,nRows); // AWS20140927 index wrong for SpectralType not powerlaw
		 table.column("PowerLaw_Index").read(index,1,nRows); // AWS20140927 use best-fit index for all spectral types if using powerlaw to compute flux
		 table.column("Pivot_Energy")  .read(pivot,1,nRows);
	      }catch (CCfits::Table::NoSuchColumn){
		 std::cerr<<"Source file format not supported"<<std::endl;
		 throw (CCfits::Table::NoSuchColumn("Source file format not supported"));
	      }
	   }
	}

        std::cout<<" completed reading first set of values"<<std::endl;


       std::cout<<" starting reading second set of values"<<std::endl;
	//Spectral and fit type
	std::vector<std::string> fitTypes(nRows, "FIXED");
	std::vector<std::string> spTypes(nRows, "POWERLAW");
	try{
		table.column("FIT_TYPE").read(fitTypes,1,nRows);
	}catch (CCfits::Table::NoSuchColumn){ }
	try{
		table.column("SP_TYPE").read(spTypes,1,nRows);
	}catch (CCfits::Table::NoSuchColumn){}

        std::cout<<" completed reading second set of values"<<std::endl;

      std::cout<<" starting reading third set of values"<<std::endl;

	//Spectral parameters and their uncertainty
	std::vector<std::valarray<double> > spParameters(nRows), spParametersUnc(nRows);
	try {
		   std::cout<<" trying SP_PARS"<<std::endl;
	   table.column("SP_PARS").readArrays(spParameters,1,nRows);
	   //Now try to read the uncertainties separately, we don't want to
	   //screw up the already assigned spectra if uncertainties don't exist
	   try {
		   std::cout<<" trying SP_PARS_UNC"<<std::endl;
	      table.column("SP_PARS_UNC").readArrays(spParametersUnc,1,nRows);
	   } catch (CCfits::Table::NoSuchColumn) {}
	} catch (CCfits::Table::NoSuchColumn) {
	   //Try to read the Flux values from the catalog group
	   std::vector<double> flux100_300, flux300_1000, flux1000_3000, flux3000_10000, flux10000_100000;
	   std::vector<double> fluxU100_300, fluxU300_1000, fluxU1000_3000, fluxU3000_10000, fluxU10000_100000;
	   double enlow[] = {100,300,1000,3000,10000};
	   double enupp[] = {300,1000,3000,10000,100000};
	   bool catalogFlux = true;
	   try {
		std::cout<<" trying Flux100_300"<<std::endl;
	      table.column("Flux100_300").read(flux100_300,1,nRows);
	      table.column("Flux300_1000").read(flux300_1000,1,nRows);
	      table.column("Flux1000_3000").read(flux1000_3000,1,nRows);
	      table.column("Flux3000_10000").read(flux3000_10000,1,nRows);
	      table.column("Flux10000_100000").read(flux10000_100000,1,nRows);

	      /* 3FGL has format 2D for Unc_Flux, for lower and upper bounds. Cannot read with this routine, but not needed anyway in galplot AWS20140929
	      table.column("Unc_Flux100_300").read(fluxU100_300,1,nRows);
	      table.column("Unc_Flux300_1000").read(fluxU300_1000,1,nRows);
	      table.column("Unc_Flux1000_3000").read(fluxU1000_3000,1,nRows);
	      table.column("Unc_Flux3000_10000").read(fluxU3000_10000,1,nRows);
	      table.column("Unc_Flux10000_100000").read(fluxU10000_100000,1,nRows);
	      */
	      // need to have the arrays as  used below, so read from Flux, but NB these are not the correct Unc values!
	      table.column("Flux100_300").read(fluxU100_300,1,nRows);
	      table.column("Flux300_1000").read(fluxU300_1000,1,nRows);
	      table.column("Flux1000_3000").read(fluxU1000_3000,1,nRows);
	      table.column("Flux3000_10000").read(fluxU3000_10000,1,nRows);
	      table.column("Flux10000_100000").read(fluxU10000_100000,1,nRows);

              for(int i=0;i<nRows;i++)flux100[i] = flux100_300[i]+flux300_1000[i]+flux1000_3000[i]+flux3000_10000[i]+flux10000_100000[i]  ;//AWS20100810 NB "=" flux100=flux100_300... does not work since flux100 is valarray, flux100_300 is vector

              for(int i=0;i<nRows;i++)flux10000[i]=flux10000_100000[i];//AWS20120530


	   } catch (CCfits::Table::NoSuchColumn) {
	      try {
		   std::cout<<" trying FLUX100_300"<<std::endl;
		 table.column("FLUX100_300").read(flux100_300,1,nRows);
		 table.column("FLUX300_1000").read(flux300_1000,1,nRows);
		 table.column("FLUX1000_3000").read(flux1000_3000,1,nRows);
		 table.column("FLUX3000_10000").read(flux3000_10000,1,nRows);
		 table.column("FLUX10000_100000").read(flux10000_100000,1,nRows);
		 table.column("UNC_FLUX100_300").read(fluxU100_300,1,nRows);
		 table.column("UNC_FLUX300_1000").read(fluxU300_1000,1,nRows);
		 table.column("UNC_FLUX1000_3000").read(fluxU1000_3000,1,nRows);
		 table.column("UNC_FLUX3000_10000").read(fluxU3000_10000,1,nRows);
		 table.column("UNC_FLUX10000_100000").read(fluxU10000_100000,1,nRows);

              for(int i=0;i<nRows;i++)flux100[i] = flux100_300[i]+flux300_1000[i]+flux1000_3000[i]+flux3000_10000[i]+flux10000_100000[i]  ;//AWS20120530

              for(int i=0;i<nRows;i++)flux10000[i]=flux10000_100000[i];//AWS20120530

	      } catch (CCfits::Table::NoSuchColumn) { 
		 //Nothing found
		 catalogFlux = false;
	      }
	   }
	   //Check to see if we actually read something
	   if (catalogFlux) {
	      //Now we loop the vectors and set the spectral parameters and
	      //spectral type
	      std::vector<double> plFlux(5);
	      for (size_t i = 0; i < nRows; ++i) {
		 spTypes[i] = "FREE";
		 spParameters[i].resize(10);
		 spParametersUnc[i].resize(10);
		 //Calculate the correct factors, since we assume correction to
		 //the already assigned power law
		 for (size_t j = 0; j < 5; ++j) {
		    //Calculate the flux from the power law approximation to
		    //get the scale correct
		    if (fabs(index[i]+1) > 1e-10) {
		       plFlux[j] = pivot[i]*prefactor[i]/(index[i]+1)*(pow(enupp[j]/pivot[i],index[i]+1)-pow(enlow[j]/pivot[i],index[i]+1));
		    }else{
		       plFlux[j] = prefactor[i]/pivot[i]*(log(enupp[j])-log(enlow[j]));
		    }
		    //The energy is the geometrical mean energy of the bin
		    spParameters[i][2*j] = sqrt(enlow[j]*enupp[j]);
		    //And the uncertainty is half the width
		    spParametersUnc[i][2*j] = 0.5*(enupp[j]-enlow[j]);
		 }
		 //Calculate the intensities at the geometrical mean
		 std::valarray<double> flux(5);
		 flux[0] = flux100_300[i];
		 flux[1] = flux300_1000[i];
		 flux[2] = flux1000_3000[i];
		 flux[3] = flux3000_10000[i];
		 flux[4] = flux10000_100000[i];
		 std::valarray<double> intens = flux2int(std::valarray<double>(enlow,5),std::valarray<double>(enupp,5),flux);
		 for (int j = 0; j < intens.size(); ++j) {
		    if (index[i] < 0) {
		       spParameters[i][2*j+1] = intens[j]/prefactor[i]/pow(sqrt(enlow[j]*enupp[j])/pivot[i], index[i]);
		    }else {
		       spParameters[i][2*j+1] = intens[j]/prefactor[i]/pow(sqrt(enlow[j]*enupp[j])/pivot[i], -index[i]);
		    }
		 }
	      	 spParametersUnc[i][1] = fluxU100_300[i]/flux100_300[i]*spParameters[i][1];
	      	 spParametersUnc[i][3] = fluxU300_1000[i]/flux300_1000[i]*spParameters[i][3];
	      	 spParametersUnc[i][5] = fluxU1000_3000[i]/flux1000_3000[i]*spParameters[i][5];
	      	 spParametersUnc[i][7] = fluxU3000_10000[i]/flux3000_10000[i]*spParameters[i][7];
	      	 spParametersUnc[i][9] = fluxU10000_100000[i]/flux10000_100000[i]*spParameters[i][9];
	      }
	   }
	}
	
	//Put the information into the source vector
	fSources.resize(nRows);
	for (int i=0; i < nRows; ++i){
		fSources[i].name = names[i];
		fSources[i].l = l[i];
		fSources[i].b = b[i];
		fSources[i].prefactor = prefactor[i];
		fSources[i].flux100  = flux100 [i];
		fSources[i].flux1000 = flux1000[i];//AWS20110806
		fSources[i].flux10000= flux10000[i];//AWS20120530

		//Crude check for index convention
		if (index[i] < 0) {
		   fSources[i].index = index[i];
		} else {
		   fSources[i].index = -index[i];
		}
		fSources[i].pivot = pivot[i];
		fSources[i].fitType = strToFitType(fitTypes[i]);
		fSources[i].spType = strToFitType(spTypes[i]);
		//Check for validity of the spParameters array.
		switch(fSources[i].spType) {
			case POWERLAW:
				if ( spParameters[i].size() < 2 ) {
					spParameters[i].resize(2);
					spParametersUnc[i].resize(2);
					spParameters[i][0] = fSources[i].prefactor;
					spParameters[i][1] = fSources[i].index;
					//Add arbitrary values
					spParametersUnc[i][0] = fSources[i].prefactor*1e-3;
					spParametersUnc[i][1] = fabs(fSources[i].index)*1e-3;
				} 
				if ( spParametersUnc[i].size() < 2 ) {
				   spParametersUnc[i].resize(2);
				   spParametersUnc[i][0] = spParameters[i][0]*1e-3;
				   spParametersUnc[i][1] = fabs(spParameters[i][1])*1e-3;
				}
				break;
			case POWERLAWEXPCUTOFF:
				switch (fSources[i].fitType) {
				   case BROKENPOWERLAW:
				      std::cout<<"Source name: "<<fSources[i].name<<std::endl;
		      		      std::cout<<"Fit type incompatible with spectral type"<<std::endl;
      				      std::cout<<"Changing fit type to a power law with an exponential cut off"<<std::endl;
				      fSources[i].fitType = POWERLAWEXPCUTOFF;
				      break;
				}
				if ( spParameters[i].size() < 3 ) {
					spParameters[i].resize(3);
					spParametersUnc[i].resize(3);
					spParameters[i][0] = fSources[i].prefactor;
					spParameters[i][1] = fSources[i].index;
					spParameters[i][2] = 1e4;
					spParametersUnc[i][0] = spParameters[i][0]*1e-3;
					spParametersUnc[i][1] = fabs(spParameters[i][1])*1e-3;
					spParametersUnc[i][2] = spParameters[i][2]*1e-3;
				}
				if ( spParametersUnc[i].size() < 3 ) {
					spParametersUnc[i].resize(3);
					spParametersUnc[i][0] = spParameters[i][0]*1e-3;
					spParametersUnc[i][1] = fabs(spParameters[i][1])*1e-3;
					spParametersUnc[i][2] = spParameters[i][2]*1e-3;
				}
				break;
			case BROKENPOWERLAW:
				switch (fSources[i].fitType) {
					case PREFACTOR:
					case INDEX:
					case POWERLAW:
					case POWERLAWEXPCUTOFF:
						std::cout<<"Source name: "<<fSources[i].name<<std::endl;
						std::cout<<"Fit type incompatible with spectral type"<<std::endl;
						std::cout<<"Changing fit type to a broken power law"<<std::endl;
						fSources[i].fitType = BROKENPOWERLAW;
				}
				if ( spParameters[i].size() < 4 ) {
					spParameters[i].resize(4);
					spParametersUnc[i].resize(4);
					spParameters[i][0] = fSources[i].prefactor;
					spParameters[i][1] = fSources[i].index;
					spParameters[i][2] = fSources[i].index;
					spParameters[i][3] = fSources[i].pivot;
					spParametersUnc[i][0] = spParameters[i][0]*1e-3;
					spParametersUnc[i][1] = fabs(spParameters[i][1])*1e-3;
					spParametersUnc[i][2] = fabs(spParameters[i][2])*1e-3;
					spParametersUnc[i][3] = spParameters[i][3]*1e-3;
				}
				if ( spParametersUnc[i].size() < 4 ) {
					spParametersUnc[i].resize(4);
					spParametersUnc[i][0] = spParameters[i][0]*1e-3;
					spParametersUnc[i][1] = fabs(spParameters[i][1])*1e-3;
					spParametersUnc[i][2] = fabs(spParameters[i][2])*1e-3;
					spParametersUnc[i][3] = spParameters[i][3]*1e-3;
				}
				break;
			case FREE:
				switch (fSources[i].fitType) {
					case PREFACTOR:
					case INDEX:
					case POWERLAW:
					case POWERLAWEXPCUTOFF:
					case BROKENPOWERLAW:
						std::cout<<"Source name: "<<fSources[i].name<<std::endl;
						std::cout<<"Fit type incompatible with spectral type"<<std::endl;
						std::cout<<"Changing fit type to FREE"<<std::endl;
						fSources[i].fitType = FREE;
				}
				if ( spParameters[i].size() < 2 ) {
					spParameters[i].resize(2);
					spParameters[i][0] = 1;
					spParameters[i][1] = 1;
					spParametersUnc[i].resize(2);
					spParametersUnc[i][0] = 1e-3;
					spParametersUnc[i][1] = 1e-3;
				}
				//We must remove 0 from the end of the energy parameters array.  
				//The sizes of the arrays are the same as the sizes of the largest one.  
				//We have to take care of that
				for (int j = 0; j < spParameters[i].size(); j+=2){
				   if (spParameters[i][j] == 0) {
				      //The resize function destroys the array so we need temporary storage
				      std::valarray<double> temparr(j);
				      temparr = spParameters[i]; //The assignment function doesn't change the array size.
				      spParameters[i].resize(j);
				      spParameters[i] = temparr;
				      //We do not have to handle the uncertainties, since the additional elements will simply be ignored in that case
				      break;
				   }
				}
				if ( spParametersUnc[i].size() < spParameters[i].size() ) {
				   spParametersUnc[i].resize(spParameters[i].size());
				   //The factor uncertainty is just 0.1 percent
				   for (size_t j = 1; j < spParameters[i].size(); j+=2) {
				      spParametersUnc[i][j] = spParameters[i][j]*1e-3;
				   }
				   //We must estimate the size of the energy bins,
				   //assuming a uniformely logarithmically
				   //distributed bins
				   if ( spParameters[i].size() <= 2 ) {
				      //Only one energy bin, nothing we can do
				      //now
				      spParametersUnc[i][0] = 1;
				   } else {
				      //We must have at least two energy bins,
				      //we can start working
				      double c = spParameters[i][2]/spParameters[i][0]; //The ratio of the bin size
				      double E1 = 2*spParameters[i][0]/(1+c); //The minimum energy
				      //Now we can easily calculate the width
				      for (size_t j = 0; j < spParameters[i].size(); j+=2){
					 spParametersUnc[i][j] = E1/2*(pow(c,j/2+1)-pow(c,j/2));
				      }
				   }
				}
				break;
		}
		fSources[i].spParameters.resize(spParameters[i].size());
		fSources[i].spParameters = spParameters[i];
		fSources[i].spParametersUnc.resize(spParameters[i].size());
		fSources[i].spParametersUnc = spParametersUnc[i];
	}
}

int Sources::sourceNumber(const std::string & name) const{
	std::map<std::string, int>::const_iterator it = fnameIndex.find(name);
	if ( it != fnameIndex.end() ){
		return it->second;
	} else {
		return -1;
	}
}

std::string Sources::fitTypeToStr(const Sources::FitType &type){
	switch (type) {
		case FIXED:
			return "FIXED";
			break;
		case PREFACTOR:
			return "PREFACTOR";
			break;
		case INDEX:
			return "INDEX";
			break;
		case POWERLAW:
			return "POWERLAW";
			break;
		case POWERLAWEXPCUTOFF:
			return "POWERLAWEXPCUTOFF";
			break;
		case BROKENPOWERLAW:
			return "BROKENPOWERLAW";
			break;
		case FREE:
			return "FREE";
			break;
		default:
			return "NONE";
	}
}

Sources::FitType Sources::strToFitType(const std::string & type){
	// Convert to upper case
	std::string upper(type);
	// explicit cast needed to resolve ambiguity
	std::transform(upper.begin(), upper.end(), upper.begin(), (int(*)(int)) std::toupper);
	if (upper == "FIXED"){
		return FIXED;
	}else if (upper == "PREFACTOR"){
		return PREFACTOR;
	}else if (upper == "INDEX"){
		return INDEX;
	}else if (upper == "POWERLAW"){
		return POWERLAW;
	}else if (upper == "POWERLAWEXPCUTOFF"){
		return POWERLAWEXPCUTOFF;
	}else if (upper == "BROKENPOWERLAW"){
		return BROKENPOWERLAW;
	}else if (upper == "FREE"){
		return FREE;
	}
        if (upper != "NONE") std::cout<<"Fit type "<<type<<" not supported, defaulting to none"<<std::endl;
	//Defaults to NONE, don't include the source.  This should be a safe default
	return NONE;
}

////////////////////////////////////////////////////////////
int Sources::add_source(Source source)           //AWS20110829
{

  fSources.push_back(source);

  /* debugging
  std::cout<<"Sources: adding source"<<std::endl;
  std::cout<<"Sources:  number of sources = "<<fSources.size()<<std::endl;

  int i=fSources.size();
  std::cout<<"Sources:  fSources[i-1].l       = "<< fSources[i-1].l       <<std::endl;
  std::cout<<"Sources:  fSources[i-1].b       = "<< fSources[i-1].b       <<std::endl;
  std::cout<<"Sources:  fSources[i-1].flux100 = "<< fSources[i-1].flux100 <<std::endl;
  std::cout<<"Sources:  fSources[i-1].flux1000= "<< fSources[i-1].flux1000<<std::endl;
  */

  return 0;
}
