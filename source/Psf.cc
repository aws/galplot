#include "Psf.h"
#include "Exposure.h"
#include <healpix_base.h>
#include <healpix_map.h>
#include <alm.h>
#ifdef  HAVE_ALM_MAP_TOOLS_H               //AWS20131128 healpix 2.01
   #include <alm_map_tools.h>
#endif
#include <xcomplex.h>
#include <alm_powspec_tools.h>
#ifdef  HAVE_ALM_HEALPIX_TOOLS_H           //AWS20131128 healpix 2.20a
   #include <alm_healpix_tools.h>
   #include <lsconstants.h>
#endif
#include <CCfits/CCfits>
#include "PhysicalConstants.h"
#include "Utils.h"
#include <cmath>
#include <algorithm>
#include <set>

Psf::Psf(const std::string &fileName) {
	ReadPsfFits(fileName);
	CalculateInterpolation();
}

Psf::Psf(const std::vector< std::vector<double> > & psf, const std::vector<double> & energies, const std::vector<double> &theta){
	fPsf = psf;
	fEnergies = energies;
	fTheta = theta;
	CalculateInterpolation();
}

const std::vector< std::vector<double> > &Psf::getPsfVector() const{
	return fPsf;
}

const std::vector<double> &Psf::getTheta() const{
	return fTheta;
}

const std::vector<double> &Psf::getEnergies() const{
	return fEnergies;
}

double Psf::getPsfWidth(double energy, double fraction) const{
	//Find the energy index
	int enindex = 0;
	while(enindex < fEnergies.size() && energy > fEnergies[enindex]) enindex++;
	if (enindex > 0) --enindex;

	//Get the width
	return getPsfWidth(fPsf[enindex], fraction);
}

void Psf::CalculateInterpolation(){
   //Use power law interpolation
   std::valarray<double> coeff(2);
   std::vector< std::valarray<double> > tmp(fTheta.size(), coeff);
   fLagrange.resize(fEnergies.size(), tmp);
   for (int i = 0; i < fEnergies.size()-1; ++i){
      double eDenom = 1./log(fEnergies[i+1]/fEnergies[i]);
      for (int j = 0; j < fTheta.size(); ++j){
	 fLagrange[i][j][0] = log(fPsf[i+1][j]/fPsf[i][j])*eDenom;
	 fLagrange[i][j][1] = fPsf[i][j]/pow(fEnergies[i],fLagrange[i][j][0]);
      }
   }
}

std::vector<double> Psf::getEffectivePsf(double eMin, double eMax, double index) const{
   //Output vector for the psf
   std::vector<double> psf(fTheta.size(),0);
   //First find the index range
   int minIndex, maxIndex;
   Utils::findIndexRange(fEnergies, eMin, eMax, minIndex, maxIndex);
   //Do the psf integration assuming polynomial dependance
   for (int i = minIndex; i<maxIndex; ++i){
      for (int j = 0; j<fTheta.size(); ++j){
	 double ind = fLagrange[i][j][0];
	 double pref = fLagrange[i][j][1];
	 if (fabs(index + 1 + ind) > 1e-10) {
	    psf[j] += pref*(pow(std::min(fEnergies[i+1], eMax), index+1+ind) - pow(std::max(fEnergies[i], eMin), index+1+ind))/(index+1+ind);
	 } else {
	    psf[j] += pref*log(std::min(fEnergies[i+1], eMax)) - log(std::max(fEnergies[i], eMin));
	 }
      }
   }
   //Normalize the psf
   double integral = 0;
   for (int j = 0; j < fTheta.size()-1; ++j){
      double thj = fTheta[j]*utl::kConvertDegreesToRadians;
      double thj1 = fTheta[j+1]*utl::kConvertDegreesToRadians;
      integral += 0.5*(psf[j]*cos(thj)+psf[j+1]*cos(thj1))*(thj1-thj);
   }
   if (integral == 0) std::cout<<"Error creating effective psf map"<<std::endl;
   for (int j = 0; j < fTheta.size(); ++j){
      psf[j] /= integral;
   }

   return psf;
}

Healpix_Map<double> Psf::getEffectiveMap(double eMin, double eMax, double index, int order, const pointing &center, double fraction) const{
	//If order not given, find the appropriate order for the psf
	if (order == 0) {
		//Find the resolution of theta, assume linear.  The resolution of the map is
		//based on this value
		double resolution = fabs(fTheta.back() - fTheta.front())/fTheta.size();
		//Calculate the order of the healpix map
		order = int(log(sqrt(3./pi)*60/resolution)/log(2))+1;
	}
	std::vector<double> psf = getEffectivePsf(eMin, eMax, index);

	return createMap(center, psf, order, fraction);
}

Skymap<double> Psf::convolveBruteForce(const Skymap<double> & skyMap, double fraction) const {
   //Status indicator
   Utils::StatusIndicator status("Brute force convolution", skyMap.nSpectra());
   //Calculate the approximate spectral index to calculate the effective PSF
   //Take the average of the index to the left and right, if they are
   //available
   std::valarray<double> index(skyMap.nSpectra());
   //We need to handle the special case when we have only a single number for
   //the spectra.  Simply put the index to 0, no correction at all.
   if (skyMap.nSpectra() == 1){
      index[0] = 0;
   } else {
      for (int i = 0; i < skyMap.nSpectra(); ++i){
	 if ( i == 0 ) {
	    index[i] = log(skyMap.sum(i+1)/skyMap.sum(i))/log(skyMap.getSpectra()[i+1]/skyMap.getSpectra()[i]);
	 } else if ( i == skyMap.nSpectra() - 1 ) {
	    index[i] = log(skyMap.sum(i-1)/skyMap.sum(i))/log(skyMap.getSpectra()[i-1]/skyMap.getSpectra()[i]);
	 } else {
	    index[i] = (log(skyMap.sum(i+1)/skyMap.sum(i))/log(skyMap.getSpectra()[i+1]/skyMap.getSpectra()[i]) +
		  log(skyMap.sum(i-1)/skyMap.sum(i))/log(skyMap.getSpectra()[i-1]/skyMap.getSpectra()[i]))/2.;
	 }
      }
   }
   //Get energy boundaries
   std::valarray<double> eMin, eMax;
   skyMap.getBoundaries(eMin, eMax);
   //Create the output map
   Skymap<double> outMap(skyMap);
   outMap = 0;
   Healpix_Map<double> hppsf;
   //Loop over all the pixels, create the psf map and add it scaled to the output
#pragma omp parallel for schedule(dynamic) default(shared) private(hppsf)
   for (int i = 0; i < skyMap.nSpectra(); ++i){
      for (int pi = 0; pi < skyMap.Npix(); ++pi) {
	 pointing pnt = skyMap.pix2ang(pi);
	 hppsf = getEffectiveMap(eMin[i], eMax[i], index[i], skyMap.Order(), pnt, fraction);
	 for (int po = 0; po < outMap.Npix(); ++po) {
	    outMap[po][i] += hppsf[i]*skyMap[pi][i];
	 }
      }
      status.refresh();
   }
   return outMap;
}

Skymap<double> Psf::convolve(const Skymap<double> & skyMap, bool deconvolve) const {
   //Status indicator
   Utils::StatusIndicator status("Convolution", skyMap.nSpectra());
   //Calculate the approximate spectral index to calculate the effective PSF
   //Take the average of the index to the left and right, if they are
   //available
   std::valarray<double> index(skyMap.nSpectra());
   //We need to handle the special case when we have only a single number for
   //the spectra.  Simply put the index to 0, no correction at all.
   if (skyMap.nSpectra() == 1){
      index[0] = 0;
   } else {
      for (int i = 0; i < skyMap.nSpectra(); ++i){
	 if ( i == 0 ) {
	    index[i] = log(skyMap.sum(i+1)/skyMap.sum(i))/log(skyMap.getSpectra()[i+1]/skyMap.getSpectra()[i]);
	 } else if ( i == skyMap.nSpectra() - 1 ) {
	    index[i] = log(skyMap.sum(i-1)/skyMap.sum(i))/log(skyMap.getSpectra()[i-1]/skyMap.getSpectra()[i]);
	 } else {
	    index[i] = (log(skyMap.sum(i+1)/skyMap.sum(i))/log(skyMap.getSpectra()[i+1]/skyMap.getSpectra()[i]) +
		  log(skyMap.sum(i-1)/skyMap.sum(i))/log(skyMap.getSpectra()[i-1]/skyMap.getSpectra()[i]))/2.;
	 }
      }
   }
   //Create the output map
   Skymap<double> outMap(skyMap);
   //Increase the order of the map by 1
   outMap = outMap.rebin(std::min(outMap.Order()+1,13), false);
   //std::copy(&index[0],&index[0]+index.size(), std::ostream_iterator<double>(std::cout, "\n"));
   //Loop over energies and do the convolution
   const int lmax = 3*outMap.Nside()-1; //3 seems to be the magic number
   std::valarray<double> eMin, eMax;
   skyMap.getBoundaries(eMin, eMax);
   Healpix_Map<double> map, hppsf;
   Alm<xcomplex<double> > psfalm(lmax,0);  //Symmetric so only m = 0 needed.
   Alm<xcomplex<double> > mapalm(lmax,lmax);  //Not symmetric so m <= l needed.
#pragma omp parallel for schedule(dynamic) default(shared) firstprivate(psfalm,mapalm) private(map,hppsf)
   for (int i = 0; i < skyMap.nSpectra(); ++i){
      //Loop if the psf is narrow enough
      if (getPsfWidth(getEffectivePsf(eMin[i], eMax[i], index[i]), 0.999) < outMap.resolution()) {
	 status.refresh();
	 continue;
      }
      //Convert the Skymap to healpix map
      map = outMap.toHealpixMap(i);
      //Average of the map, needed for scaling the convolution
      double avgBefore = map.average();
      //Convert the psf to Healpix, use a higher order to get a better
      //convolution
      hppsf = getEffectiveMap(eMin[i], eMax[i], index[i], map.Order());
      //Number of iterations depends on convolution or deconvolution
      int niter = 3;
      if (deconvolve)
	 niter = 30;
      map2alm_iter(hppsf, psfalm, niter);  //Using iterations requires us to scale the map to preserve counts

      //Harmonic transform map to alm
      map2alm_iter(map, mapalm, niter);

      //Multiply the alms
      if (deconvolve) {
	 //No need for deconvolution when the pixel size is less than the psf
	 //width
	 for (int l = 0; l<=lmax; ++l) {
	    //We need to reduce numerical noise
	    if (psfalm(l,0).real() > 1e-4) {
	       for (int m = 0; m<=l; ++m) {
		  double a = mapalm(l,m).real();
		  double b = mapalm(l,m).imag();
		  double c = psfalm(l,0).real();
		  double d = psfalm(l,0).imag();
		  double r = (a*c + b*d)/(c*c+d*d)/sqrt(4*pi/(2*l+1));
		  double i = (b*c - a*d)/(c*c+d*d)/sqrt(4*pi/(2*l+1));
		  mapalm(l,m) = xcomplex<double>(r,i);
	       }
	    }
	 }
      } else {
	 for (int l = 0; l<=lmax; ++l) {
	    for (int m = 0; m<=l; ++m) {
     	       mapalm(l,m) *= sqrt(4*pi/(2*l+1))*psfalm(l,0);
	    }
	 }
      }
      //Convert alms back to the map
      alm2map(mapalm, map);
      //Scale the map, so number of counts are equal before and after
      double scale = avgBefore/map.average();
      for (int j = 0; j < map.Npix(); ++j){
	 map[j] *= scale;
      }
      outMap.fromHealpixMap(i, map);
      //Status indicator
      status.refresh();
   }
   /*
   std::cout<<"CountsMap before convolution: "<<skyMap.sum()<<" [";
   for (int i = 0; i < skyMap.nSpectra()-1; ++i){
      std::cout<<skyMap.sum(i)<<", ";
   }
   std::cout<<skyMap.sum(skyMap.nSpectra()-1)<<"]"<<std::endl;
   std::cout<<"CountsMap after convolution: "<<outMap.sum()<<" [";
   for (int i = 0; i < outMap.nSpectra()-1; ++i){
      std::cout<<outMap.sum(i)<<", ";
   }
   std::cout<<outMap.sum(outMap.nSpectra()-1)<<"]"<<std::endl;
   std::cout<<"Ratios after convolution: "<<outMap.sum()/skyMap.sum()<<" [";
   for (int i = 0; i < outMap.nSpectra()-1; ++i){
      std::cout<<outMap.sum(i)/skyMap.sum(i)<<", ";
   }
   std::cout<<outMap.sum(outMap.nSpectra()-1)/skyMap.sum(outMap.nSpectra()-1)<<"]"<<std::endl;
   */
   return outMap.rebin(skyMap.Order(), false);
}

Healpix_Map<double> Psf::getPsfMap(double energy, int order, const pointing &center, double fraction) const{
	//If order not given, find the appropriate order for the psf
	if (order == 0) {
		//Find the resolution of theta, assume linear.  The resolution of the map is
		//based on this value
		double resolution = fabs(fTheta.back() - fTheta.front())/fTheta.size();
		//Calculate the order of the healpix map
		order = int(log(sqrt(3./pi)*60/resolution)/log(2))+1;
	}

	//Find the energy index
	int enindex = 0;
	while(enindex < fEnergies.size() && energy > fEnergies[enindex]) enindex++;
	if (enindex > 0) --enindex;

	return createMap(center, fPsf[enindex], order, fraction);
	//std::cout<<"PSF integral after scaling = "<<4*pi*hp.average()<<std::endl;
}

double Psf::getPsfWidth(const std::vector<double> & psf, double fraction) const{
   //Calculate the maximum angle from the psf fraction
   double th = fTheta.back();
   if ( fraction < 1 ) {
      double integral = 0;
      int i;
      for (i = 0; i < fTheta.size()-1; ++i){
	 double thj = fTheta[i]*utl::kConvertDegreesToRadians;
	 double thj1 = fTheta[i+1]*utl::kConvertDegreesToRadians;
	 integral += 0.5*(psf[i]*cos(thj)+psf[i+1]*cos(thj1))*(thj1-thj);
	 if (integral >= fraction) break;
      }
      th = fTheta[i];
   }
   return th;
}

Healpix_Map<double> Psf::createMap(const pointing &center, const std::vector<double> &psf, int order, double fraction) const {
	//Create the healpix map in RING scheme for fast spherical harmonics
	//transformation
	Healpix_Map<double> hp;
	hp.Set(order, RING);
	hp.fill(0);

	//Calculate the maximum angle from the psf fraction
	double th = getPsfWidth(psf, fraction)*degr2rad;

	//If center not at the pole, different method is needed
	//Since this is only for the point sources, accuracy of the psf is not as
	//important
	if (center.theta != 0 && center.phi != 0){
		//Find the pixels associated with the psf function
		std::vector<int> listpix;
		hp.query_disc_inclusive(center, th, listpix);

		//Use a finer grid in the neighboring pixels
		int cp = hp.ang2pix(center);
		fix_arr<int,8> nb;
		hp.neighbors(cp, nb);

		//Create a set of the pixel numbers
		std::set<int> nbset(&nb[0], &nb[0]+8);
		//Remove -1 from the set
		nbset.erase(-1);
		//Set for the total pixels
		std::set<int> listset(listpix.begin(),listpix.end());

		//Exclude the center pixel and its neighbors from the total
		//pixels.  We also only use the intersection of the neighbor
		//pixels and the total pixels, in case we don't want to include
		//all of the neighbors. (For a very narrow psf or small
		//fraction).
		listset.erase(cp);
		std::set<int> listReduced, nbReduced;
		std::set_difference(listset.begin(), listset.end(), nbset.begin(), nbset.end(), std::insert_iterator<std::set<int> >(listReduced,listReduced.begin()));
		std::set_intersection(listset.begin(), listset.end(), nbset.begin(), nbset.end(), std::insert_iterator<std::set<int> >(nbReduced,nbReduced.begin()));

		//Build the psf map
		vec3 cvec = center.to_vec3();
		std::set<int>::iterator it = listReduced.begin();
		for ( ; it != listReduced.end() ; ++it){
			double cdist = acos(dotprod(cvec, hp.pix2ang(*it).to_vec3()))*rad2degr;
			hp[*it] = std::max(Utils::linearInterpolation(cdist, fTheta, psf),0.0);
		}
		//Neighbors are next, increase the order by 3
		it = nbReduced.begin();
		for ( ; it != nbReduced.end(); ++it) {
		   const int dOrder = std::min(13-order, 3);
   		   const int nPix = (1<<dOrder)*(1<<dOrder);
		   Healpix_Base hpf(order+dOrder, NEST);
		   const int sp = hp.ring2nest(*it);
		   double sum = 0;
		   for (int i = sp*nPix; i < (sp+1)*nPix; ++i) {
		      double cdist = acos(dotprod(cvec, hpf.pix2ang(i).to_vec3()))*rad2degr;
      		      sum += std::max(Utils::linearInterpolation(cdist, fTheta, psf),0.0);
		   }
		   hp[*it] = sum/nPix;
		}
		//And the center pixel, increase the order by 6
		const int dOrder = std::min(13-order, 6);
		const int nPix = (1<<dOrder)*(1<<dOrder);
		Healpix_Base hpf(order+dOrder, NEST);
		const int sp = hp.ring2nest(cp);
		double sum = 0;
		for (int i = sp*nPix; i < (sp+1)*nPix; ++i) {
			double cdist = acos(dotprod(cvec, hpf.pix2ang(i).to_vec3()))*rad2degr;
			sum += std::max(Utils::linearInterpolation(cdist, fTheta, psf),0.0);
		}
		hp[cp] = sum/nPix;

	}else{
		//Loop the rings of the map until we reach the boundary of the psf
		int nRing = 1;
		int startPix, nPixRing;
		double theta(0);
		bool shifted;
		while ( theta < th ){
			hp.get_ring_info2(nRing, startPix, nPixRing, theta, shifted);
			//Difference in order to get the finer structure of the psf
			const int dOrder = std::max(std::min(13-order, 6 - 2*(nRing-1)),1);
			const int nPix = (1<<dOrder)*(1<<dOrder);
			//Create a finer healpix grid to get a better representation for the psf
			Healpix_Base hpf(order+dOrder, NEST);
			//Loop over the relevant pixels in the finer grid and set their value with
			//interpolation
			int sp = hp.ring2nest(startPix);
			double sum = 0;
			for (int i = sp*nPix; i < (sp+1)*nPix; ++i){
				double cdist = hpf.pix2ang(i).theta*rad2degr;
				//Do linear interpolation although it isn't linear.  Finer binning in the
				//input psf should solve that.
				sum += std::max(Utils::linearInterpolation(cdist, fTheta, psf),0.0);
			}
			sum /= nPix;
			//Add the value to all the pixels in a ring
			for (int i = startPix; i < startPix+nPixRing; ++i){
				hp[i] = sum;
			}
			++nRing;
		}
	}
	//Integrate over the psf to assure proper normalization
	double integral = 4*utl::kPi*hp.average();
	//std::cout<<"PSF integral = "<<integral<<std::endl;
	//Scale the map with the integral
	for (int i=0; i<hp.Npix(); ++i){
		hp[i] /= integral;
	}
	return hp;
}

void Psf::ReadPsfFits(const std::string &fileName){
	//Open file in read only mode
	CCfits::FITS fits(fileName);

	//Get the THETA HDU
	CCfits::ExtHDU &thTable = fits.extension("THETA");

	//Read proper keywords
	int nTheta;
	thTable.readKey("NAXIS2", nTheta);

	//Resize the array and read in theta values
	thTable.column("Theta").read(fTheta,1,nTheta);

	//Get the PSF HDU
	CCfits::ExtHDU &psfTable = fits.extension("PSF");
	
	//Read specific keywords to tell data length
	int nEnergy;
	psfTable.readKey("NAXIS2", nEnergy);

	//Resize the array and read in energy values
	psfTable.column("Energy").read(fEnergies,1,nEnergy);

	//Now we read in the psf
	std::vector< std::valarray<double> > array;
	psfTable.column("Psf").readArrays(array,1,nEnergy);

	fPsf.resize(nEnergy,fTheta);
	for (int i = 0; i < nEnergy; ++i){
		for (int j = 0; j < nTheta; ++j){
			fPsf[i][j] = array[i][j];
		}
	}
}
