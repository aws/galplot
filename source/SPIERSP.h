// SPI energy response

#include"RMF.h"
#include"IRF.h"

class SPIERSP
{
 public:

  RMF rmf;
  IRF irf;

  


  double *E_in, *E_out_lo,*E_out_hi;       // interpolated response energies
  int   n_E_in,n_E_out;                    // interpolated response energies
  double *response;                        // interpolated response grid

  int read (char *filename,int idet1,int idet2,
            double *E_in_,                      int n_E_in_,
            double *E_out_lo_,double *E_out_hi_,int n_E_out_);

  void print();
  

  double R(double E_in, double E_out);
};
