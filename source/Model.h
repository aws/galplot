#ifndef Model_h
#define Model_h

#include "Exposure.h"
#include "Counts.h"
#include "Psf.h"
#include "Sources.h"
#include "Variables.h"
#include "Skymap.h"
#include "SparseSkymap.h"
#include "Parameters.h"
#include <string>
#include <exception>
#include <vector>

/** \brief A virtual base class for all the models.
 *
 * This class defines the basic interface for all the models, as well as
 * provide some general purpose functions for exposure correction and
 * convolution.
 */
class BaseModel {
	public:
		/** \brief Defines the type returned by the gradient and second derivative
		 * calculations of the models
		 */
		typedef std::map<std::string, Skymap<double> > gradMap;

		/** \brief The only constructor available.
		 *
		 * \param counts is a reference to a counts map, needed as a reference map
		 * when doing exposure corrections.
		 * \param exposure is a reference to an exposure object, needed for
		 * exposure corrections.
		 * \param psf is a point spread function, needed for convolution
		 * \param sources is a reference for source objects.  Only
		 * really used by the SourceModel, but necessary to store here
		 * for the implementation of source retrieval in the ArrayModel
		 * \param pars is a reference to a Parameters object, so models can easily be
		 * configured.
		 * \param filter is the filter applied to the fit.  Every pixel
		 * where filter == 0 is excluded.
		 * \param configure is a bitmask controlling the model
		 * behavior.  There are two bits defined in the base model that
		 * all model should obey
		 *  - EXPOSURE_CORRECT
		 *  - CONVOLVE
		 * By default, both of them are set.  These modify the behavior
		 * of the built in convolution and exposure correction routine
		 * in BaseModel.  The value of these two are 1 and 2
		 * respectively.  Other models can use this variable, but
		 * cannot use these two values for the bitmask.
		 */
		BaseModel(const CountsMap & counts, const Exposure &exposure, const Psf &psf, const Sources &sources, const Parameters &pars, const Skymap<char> &filter, unsigned int configure = 3);

		const unsigned int EXPOSURE_CORRECT, CONVOLVE;
		
		/** \brief A virtual function returning the model count skymap given variable values.
		 *
		 * \param vars defines the variable values of the model.
		 * \param map is a double precision Skymap which is of the same
		 * size as counts map.  The results should be added to this
		 * map.
		 *
		 * It is advised that models cache their last values, since the parameters
		 * need not change between function calls.  This will speed up minimization
		 * considerably.
		 *
		 * This method must be implemented in a child object.
		 */
		virtual void getMap(const Variables & vars, Skymap<double> &map) = 0;
		
		/** \brief A virtual function returning the different components count skymaps given variable values.
		 *
		 * This function can also be used to output files required by
		 * the model, but please use the prefix for all filenames.
		 *
		 * \param vars defines the variable values of the model.
		 * \return a map of strings and skymaps, where the
		 * strings are the name of the component and the skymap
		 * is its counts map
		 * \param prefix is a string that should be prefixed to all
		 * output filenames by the model.  If no files are written in
		 * this method, it can be safely ignored.
		 *
		 * This method must be implemented in a child object.
		 */
		virtual gradMap getComponents(const Variables & vars, const std::string &prefix) = 0;
		
		/** \brief A virtual function returning a skymap of the first derivatives
		 * of the model with respect to the given variable name
		 *
		 * \param vars defines the variable values of the model.
		 * \param varName is the name of the variable which derivative map should be
		 * returned
		 * \param map is a double precision Skymap which is of the same
		 * size as counts map.  The results should be added to this
		 * map.
		 *
		 * It is advised that models cache their last values, since the parameters
		 * need not change between function calls.  This will speed up minimization
		 * considerably.
		 *
		 * This method must be implemented in a child object.
		 */
		virtual void getGrads(const Variables & vars, const std::string & varName, Skymap<double> &map) = 0;
		
		/** \brief A virtual function returning a skymap of the second derivatives
		 * of the model with respect to the given variable names
		 *
		 * \param vars defines the variable values of the model.
		 * \param varName1 and
		 * \param varName2 are the names of the variables which derivative map should be
		 * returned
		 * \return a map with a single varName1 and Skymap pair.  If the derivative is 0 in all
		 * pixels, return an empty map.
		 *
		 * It is advised that models cache their last values, since the parameters
		 * need not change between function calls.  This will speed up minimization
		 * considerably.
		 *
		 * This method must be implemented in a child object.
		 */
		virtual gradMap getSecDer(const Variables & vars, const std::string & varName1, const std::string & varName2) = 0;
		
		/** \brief Return a reference to the variables */
		Variables & getVariables();
		
		/** \brief Return a constant reference to the variables */
		const Variables & getVariables() const;
		
		/** \brief Set the values of the models of the variables from a variables object
		 *
		 * Only changes the value and error of the variables currently available in
		 * the model, does not set new variables.  Throws an error if one of the
		 * variables in the model is not defined in the new Variables object.
		 */
		void setVariables(const Variables &vars);
		
		/** \brief Return a constant reference to the CountsMap object */
		const CountsMap & getCounts() const;

		//! Return a constant reference to the filter skymap
		const Skymap<char> & getFilter() const;
		
		//! Return a copy of the source object with model modifications
		Sources getSources(const Variables & vars) const;
		
		/** \brief Modify the sources object before return.
		 *
		 * Mechanism needed for models doing spectral fitting to get the
		 * information back into the source catalog.  Definately not optimal, but
		 * works.
		 *
		 * This function needs to be public for the JoinModel to work properly.  This method 
		 * depends on the sources to be the same as the model was initialized with.  getSources takes
		 * care of that.
		 */
		virtual void modifySources(const Variables &, Sources &) const{};
		
		/** \brief Models must be able to throw errors if an exception occurs */
		class ModelError : public std::exception {
			private:
				std::string eMessage;
			public:
				/** \brief Construct an error with the default message "Error in Model class" */
				ModelError() : eMessage("Error in Model class"){}
				/** \brief Construct an error with a specialized error message
				 *
				 * \param message is the specialized message
				 */
				ModelError(const std::string & message) : eMessage(message){}
				~ModelError() throw(){}
				/** \brief Return the error message */
				virtual const char* what () const throw(){
					return(eMessage.c_str());
				}
		};
	protected:
		/** \brief Converts flux maps to counts.
		 *
		 * \param fluxMap is the skymap to be converted to counts.  This should be
		 * in units of 1/Energy/cm^2/s/sr, where energy is the units of energy in the
		 * skymap's spectra.  Note that the energy unit of the flux map and counts
		 * map have to conform.  The energy dependence of the flux is approximated
		 * with a power-law between spectral points.  Finer energy binning in the
		 * input flux map should be used if this is a bad approximation.
		 *
		 * \param psfRebin is a boolean that determines if the return
		 * map should be binned more finely to get a better accuracy
		 * for the convolution.
		 *
		 * \return a double precision Skymap with the same size and energy
		 * structure as the counts map unless psfRebin is given, where
		 * the energy bin structure follows that of the psf.
		 *
		 * \note This does not take into account the solid angle of the pixels.
		 */
		Skymap<double> fluxToCounts(const Skymap<double> &fluxMap, bool psfRebin=false) const;
		/** \brief Convolve a skymap with the stored psf
		 *
		 * \param skyMap is the input skymap to be convolved.  It should be in RING
		 * scheme to make the computation faster.  Output from fluxMap are in that
		 * scheme.
		 *
		 * \param psfRebin is a boolean that determines if the input
		 * map is more finely binned to get a better accuracy
		 * for the convolution.  The output map will then be rebinned
		 * to the same binning as the counts map.
		 *
		 * \return the convolved skymap, which has the same structure as the
		 * original one.
		 *
		 * Uses the fast spherical harmonics decomposition of healpix to do the
		 * convolution.  Assumes a homogenous spherically symmetric psf, since that
		 * is the only thing that can be handled by this method.  In the case of
		 * GLAST, this should not matter much.
		 */
		Skymap<double> convolve(const Skymap<double> & skyMap, bool psfRebin=false) const;
		/** \brief Exposure correct a powerlaw spectra, given a prefactor, index
		 * and a pivot point.
		 *
		 * The formula used for the flux is \f$ F(E) = P_0 (E/E_0)^{\\gamma} \f$,
		 * where \f$ P_0 \f$ is the prefactor, \f$ E_0 \f$ is the pivot point and
		 * \f$ \\gamma \f$ is the index.
		 *
		 * \param prefactor \f$ P_0 \f$
		 * \param index \f$ \\gamma \f$
		 * \param pivot \f$ E_0 \f$
		 * \param co the coordinate we wish to evaluate the exposure
		 *
		 * This function does not take the PSF into account, i.e. we assume the
		 * exposure is uniform enough that it does not matter.  This is not true at
		 * low energies.
		 */
		std::valarray<double> powerLawCounts(double prefactor, double index, double pivot, const SM::Coordinate &co, bool psfRebin=false, std::valarray<double> *eMin=0, std::valarray<double> *eMax=0) const;
		/** \brief Convert a spectra (units of cm^-2 s^-1
		 * MeV^-1, given that exposure energy is MeV) at a given
		 * coordinate to counts.
		 *
		 * This method uses a semi-analytical integration, using a
		 * power law interpolation on the input spectra.
		 *
		 * \param intensities is the intensity spectra in units of  cm^-2 s^-1 MeV^-1, given that exposure energy is MeV
		 * \param energies is the energies corresponding to the
		 * intensities.  Has to be the same size as intensities.
		 * \param co is the coordinate of the flux location
		 * \param eMinArr gives the minimum energy boundary on return
		 * \param eMaxArr gives the maximum energy boundary on return
		 *
		 * \return a double precision valarray of the expected counts of
		 * the spectra
		 */
		std::valarray<double> spectraToCounts(std::valarray<double> &intensities, const std::valarray<double> &energies, const SM::Coordinate &co, bool psfRebin=false, std::valarray<double> *eMinArr=0, std::valarray<double> *eMaxArr=0) const;
		const Exposure &fexposure; //!< constant reference to the Exposure object
		const CountsMap &fcounts; //!< constant reference to the CountsMap object
		const Psf &fpsf; //!< constant reference to the Psf object
		const Sources &fsources; //!< constant reference to the Sources object
		const Parameters &fparameters; //!< constant reference to the Parameters object
		const Skymap<char> &ffilter; //!< constant reference to a filter object.  0 means exclude, 1 include.  Models should take that into account if needed.
		Variables fvariables; //!< The variables of the model

		unsigned int fconfigure; //!< a bitmask controlling the models behavior

		//Return energy boundaries, compatible with the counts map, but
		//as finely binned as the psf.
		void getPsfEnergyBoundaries(std::valarray<double> &eMin, std::valarray<double> &eMax) const;
		//Rebin a map generated by psf boundaries given above to count
		//map boundaries.  Does not check spatial binning.
		void rebinPsfBoundariesToCounts(Skymap<double> & inMap) const;
};
/**\brief a model to join other models in a combined one
 *
 * Stores a vector of pointers to models.  Sums up all the skymaps from the models
 * as a total model map.  Handles all the functions from basemodel gracefully
 * so all models are included.
 */
class ArrayModel : public BaseModel{
	public:
		ArrayModel(const Exposure &exposure, const CountsMap &counts, const Psf &psf, const Sources &sources, const Parameters &pars, const Skymap<char> &filter, unsigned int configure = 3);
		void getMap(const Variables &vars, Skymap<double>&map);
		gradMap getComponents(const Variables & vars, const std::string &prefix);
		void getGrads(const Variables &vars, const std::string & varName, Skymap<double>&map);
		gradMap getSecDer(const Variables & vars, const std::string & varName1, const std::string & varName2);
		void setModels(const std::vector<BaseModel*> &);
	protected:
		void modifySources(const Variables &vars, Sources &sources) const;
		std::vector<BaseModel*> fmodels; //!< A vector of model pointers to store the used models
};


/**\brief A model to join other models in a combined one.
 *
 * The constructor has a list of models it accepts.  To extend this object,
 * just add your model to the list in the constructor.
 *
 * The models are selected with the parameter
 *  - modelNames: a space separated list of model names.
 */
class JoinModel : public ArrayModel {
	public:
		/**\brief The constructor takes the same parameters as the BaseModel constructor.
		 *
		 * Initializes the BaseModel as well as all the models given in the modelNames parameter.
		 */
		JoinModel(const Exposure &exposure, const CountsMap &counts, const Psf &psf, const Sources &sources, const Parameters &pars, const Skymap<char> &filter, unsigned int configure = 3);
		~JoinModel();
};

/**\brief A spatially constant power law model for the Extragalactic
 * background.
 *
 * This model has two variables
 *  - EGBPrefactor
 *  - EGBSlope
 *
 * Uses the BaseModel::powerLawCounts function, so no convolution done.
 */
class EGBModel : public BaseModel {
	public:
		/** \brief The constructor takes the same parameters as the BaseModel constructor.
		 *
		 * Initializes the BaseModel and calculates the EGB skymap from the initial values of the model variables.
		 * Subsequent calls to the model do not perform exposure correction, but rather just scale the cached
		 * map to fit the new variables.  There is a slight error involved, but it should be far less than the systematic
		 * errors of the effective area.  Refitting with better initial values minimizes this error.
		 */
		EGBModel(const Exposure &exposure, const CountsMap &counts, const Psf &psf, const Sources &sources, const Parameters &pars, const Skymap<char> &filter, unsigned int configure = 3);
		void getMap(const Variables &vars, Skymap<double> &map);
		gradMap getComponents(const Variables & vars, const std::string &prefix);
		void getGrads(const Variables &vars, const std::string & varName, Skymap<double> &map);
		gradMap getSecDer(const Variables & vars, const std::string & varName1, const std::string & varName2);
	private:
		double flastPrefactor, flastSlope, fcompareSlope; //!< Cache the previous variable values.  If the final values are very different from the initial values, do the exposure correction again.
		Skymap<double> fcacheMap;  //!< The cached skymap
		void exposureCorrectCacheMap(const Variables &variables); //!< A helper routine to calculate the skymap using accurate exposure correction
		const std::string fprefName, findName; //!< The names of the variables of the model
};

/**\brief A model which handles the point sources from the catalog.
 *
 * Currently it sorts the sources by their flux >100MeV.
 * The only available spectral model for the sources is a power law.
 * This model is controlled by 3 parameters:
 *  - numberOfSources: maximum number of sources to include in the map
 *  - numberOfSourcesPrefactor: number of sources where the prefactor is fitted
 *  - numberOfSourcesIndex: number of sources where the index is fitted
 *
 *  Number of sources never exceed the value given by numberOfSources.  If the
 *  number of fitted sources in the other cases exceeds that, then they are
 *  truncated.  In all cases, only the brightest sources are selected, so it is
 *  not yet possible to select which sources to fit.
 */
class SourceModel : public BaseModel {
	public:
		/** \brief The constructor takes the same parameters as the BaseModel constructor.
		 *
		 * Creates the fixed source maps, as well as the necessary sparse skymaps for sources that need to be fitted.
		 */
		SourceModel(const Exposure &exposure, const CountsMap &counts, const Psf &psf, const Sources &sources, const Parameters &pars, const Skymap<char> &filter, unsigned int configure = 3);
		void getMap(const Variables &vars, Skymap<double> &map);
		gradMap getComponents(const Variables & vars, const std::string &prefix);
		void getGrads(const Variables &vars, const std::string & varName, Skymap<double> &map);
		gradMap getSecDer(const Variables & vars, const std::string & varName1, const std::string & varName2);
		/** \brief Enum for defining the type of variable.
		 *
		 * - Pre: pre factor correction
		 * - Ind: index correction
		 * - Unk: unknown parameter type
		 */
		enum varType { Pre, Ind, Unk };
	private:
		/** \brief Add the number i as a string to the prefix.
		 *
		 * \param i the number to append
		 * \param prefix is the prefix
		 * \return the joined string
		 */
		std::string prependNumber (int i, const std::string &prefix) const;
		/** \brief Given a source number i, find the source corrections.
		 *
		 * \param i the source number
		 * \param prefCorr is the prefix correction
		 * \param indCorr is the index correction
		 */
		void getCorr(int i, double &prefCorr, double &indCorr, const Variables & variables) const;
		/** \brief Find the type of variables.
		 *
		 * \param varName is the name of the variable
		 * \return Ind or Pre for index and prefactor, respectively, or Unk if the parameter is unknown.
		 */
		varType getType(const std::string & varName) const;
		/** Find the index of a variable name.
		 *
		 * \param varName is the name of the variable, and it is assumed it is of a known type
		 * \return the index for the parameter or -1 if an error occurs.
		 */
		int getIndex(const std::string & varName) const;
		Skymap<double> ffixedSkymap; //<! Cache for the fixed sources.  They take a long time to compute
		std::vector< SparseSkymap<double> > fvariableSkymaps;  //<! We store the skymaps for the variable sources separately in a sparse skymap
		int fnPrefactor,fnIndex; //!< Number of sources to fit
		std::vector<double> fprevIndCorr, fprevPrefCorr; //!< We cache the last known values for the corrections to speed up calculations when they don't change
};

/**\brief Basic fitting class that varies the electron and proton
 * normalization.
 *
 * Uses healpix GALPROP output and scales the electron and proton normalization parameters.  The variables of the model are
 *
 * - ElectronNormalization
 * - ProtonNormalization
 *
 * Additionally, one also has to point the model to the GALPROP output files with the parameters
 *  - galdefID: The galdef ID
 *  - galpropFitsDirectory: The directory which holds the GALPROP output
 */
class ElectronProton : public BaseModel {
	public:
		/** \brief The constructor takes the same parameters as the BaseModel constructor.
		 *
		 * Loads the skymaps from GALPROP, exposure corrects them, convolves them and adds the inverse Compton and bremsstrahlung 
		 * components together for electron normalization.
		 */
		ElectronProton(const Exposure &exposure, const CountsMap &counts, const Psf &psf, const Sources &sources, const Parameters &pars, const Skymap<char> &filter, unsigned int configure = 3);
		void getMap(const Variables &vars, Skymap<double> &map);
		gradMap getComponents(const Variables & vars, const std::string &prefix);
		void getGrads(const Variables &vars, const std::string & varName, Skymap<double> &map);
		gradMap getSecDer(const Variables & vars, const std::string & varName1, const std::string & varName2);
	private:
		Skymap<double> felectronSkymap, fpionSkymap;
};

#endif
