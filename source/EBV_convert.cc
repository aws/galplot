using namespace std;
#include <iostream>
#include <string>
#include <vector>
#include <cstring>  //AWS20111010 for sles11 gcc 4.3.4

#include "Skymap.h"
#include "FITS.h"

int EBV_convert(int verbose)
{

  // convert E(B-V) maps from Schlegel et al. 1998 ApJ 500, 525
  // Skymap healpix routines from gardian

  cout<<">>>> EBV_convert"<<endl;

 double pi=acos(-1.0); //AWS20110425 before was from healpix constants.h

  /////////////////////////////////////////////// parameters

  int    Ts               = 125; //AWS20100401
         Ts               = 150;
         Ts               = 150;

  double EBV_max_use      = 5.0; // use EBV only for EBV less than this value.          AWS20090915 
         EBV_max_use      = 2.0; //                                                     AWS20100225 
         EBV_max_use      = 5.0; //                                                     AWS20100225 
         EBV_max_use      = 1e6; // i.e. no EBV limit                                   AWS20100503


	 int  distribute       = 0;//AWS10100429 0= assign excess to local rings, 1=distribute excess over rings
              distribute       = 1;

  int    pos_excess_only  = 1;   // 1 = use positive excesses only, 0 = pos and neg     AWS20090915   
         pos_excess_only  = 1;   //AWS20100429


  /////////////////////////////////////


  cout<<"Ts= "<<Ts<<" EBV_max_use= "<<EBV_max_use<<endl;



  int i,j,il,ib,ir;
  double l,b;

  string mapname;

  mapname="../FITS/schlegel_E_B-V.fits"; // converted from original to Healpix by Markus Ackermann
  cout<<mapname<<endl;
  
  Skymap<double> EBV;
  EBV.load(mapname);
  Skymap<double> NH;

  SM::Coordinate coord; // from Skymap.h


  FITS NHI,COR;

  cout<<"energies:"<<endl;// there are 2 entries, seem to be equal

  for(i=0;i<EBV.getSpectra().size();i++)    cout<<EBV.getSpectra()[i]<<endl; // the energies are called 'Spectra'

  //   if (verbose==1)  EBV.print(cout);AWS20091204
  
   cout<<"EBV max="<<EBV.max()<<" sum="<<EBV.sum()<<endl;

   l=10.; b=0.0;

  coord=SM::Coordinate(l,b);

  /* problem with new gardian routine so remove to test AWS20091204
  valarray<double> ebv = EBV[coord]; // note the argument in [] in this case
  cout<<"EBV at coord=";coord.print(cout);cout<<" : "<<ebv[0]<<" "<<ebv[1]<<endl; // to check the values are the same (not obvious why there are two)

  */
  
  //  valarray<double> ebv;// to satisfy rest of code test AWS20091204  not needed AWS20100225

    /*
  ArraySlice<double> ebv = EBV[coord]; // note the argument in [] in this case. new gardian need this but print() does not exist  AWS20100225
  cout<<"EBV at coord=";coord.print(cout);cout<<" : "<<ebv[0]<<" "<<ebv[1]<<endl; // to check the values are the same (not obvious why there are two)
  */

  double rtd=180./pi;        // rad to deg

  double NH_EBV = 5.80e21;       // canonical value of Bohlin, Savage and Drake 1978
         NH_EBV = 6.17e21;       // from high-latitude HI/EBV correlation Grenier, Casandjian and Terrier 2005 Science 307, 1292: text and note 13
  double    EBV_offset = 9.4e-3; //  ibid  note 13: the offset is to be added to EBV to compute NH; corresponds to NH=0.56e20 cm-2

  NH = EBV;
  NH*= NH_EBV;
  cout<<"NH max="<<NH.max()<<" sum="<<NH.sum()<<endl;

  int ipix,ip;
  ip=0;

  for (ipix=0;ipix< EBV.Npix() ;ipix++)
  {

   l =          EBV.pix2ang(ipix).phi   * rtd;
   b =   90.0 - EBV.pix2ang(ipix).theta * rtd;

  if(verbose==1)  cout<<"ipix="<<ipix<<" l = "<<l<<" b ="<<b<<" EBV="<<EBV[ipix][ip]<<" NH="<<NH[ipix][ip]<<endl; // l,b in deg


  }


  char HIR_filename[200];

  if(Ts==125)strcpy(HIR_filename,"../FITS/rbands_hi12_v2_qdeg_zmax1_Ts125.fits"   );//AWS20100401
  if(Ts==150)strcpy(HIR_filename,"../FITS/rbands_hi12_v2_qdeg_zmax1_Ts150.fits"   );//AWS20100401
  if(Ts==200)strcpy(HIR_filename,"../FITS/rbands_hi12_v2_qdeg_zmax1_Ts200.fits"   );//AWS20100401

  cout<<"Ts= "<<Ts<<"  reading HIR from "<<HIR_filename<<endl;

  NHI.read(HIR_filename);
  cout<<"NHI max="<<NHI.max()<<" sum="<<NHI.sum()<<endl;
  if(verbose==1){cout<<"input NHI:"<<endl; NHI.print();}

  // 17 rings, ring 13=8.0-10.0 kpc is the local ring
  // 720*360 0.5 deg bins
  il=1;ib=360;//b=0
  cout<<"NHI test: il= "<<il<<" ib="<<ib<<" : ";for (ir=0;ir<17;ir++){cout<<NHI(il,ib,ir)<<" ";};cout<<endl;
  il=1;ib=460;//b=50
  cout<<"NHI test: il= "<<il<<" ib="<<ib<<" : ";for (ir=0;ir<17;ir++){cout<<NHI(il,ib,ir)<<" ";};cout<<endl;
  if(verbose==1)
  for(ib=0;ib<720;ib++){cout<<"NHI test: il= "<<il<<" ib="<<ib<<" : ";for (ir=0;ir<17;ir++){cout<<NHI(il,ib,ir)<<" ";};cout<<endl; }


  char COR_filename[200];
  strcpy(COR_filename,"../FITS/rbands_co10mm_2001_qdeg.fits"   );
  cout<<"  reading COR from "<<COR_filename<<endl;
  COR.read(COR_filename);
  cout<<"COR max="<<COR.max()<<" sum="<<COR.sum()<<endl;
  if(verbose==1){cout<<"input COR:"<<endl; COR.print();}


  int n_used   =0;
  int n_changed=0;
  double NHI_av=0.;
  double NH2_av=0.;
  double NH_HI_CO_av=0;
  double NH_from_EBV_av=0;
  double NHI_from_EBV_CO=0;

  double bmin=10.; // lower abs latitude limit for using EBV
         bmin=10.; // to get gas-to-dust avoiding dark gas
         bmin= 5.; // in combination with EBV_max_use                                   AWS20090915
         bmin= 0.; // avoid b cut                                                       AWS20100225

  double R=8.5;    // solar position in kpc
  double aco=-0.4, bco=0.066; // parameters for Xco model 10 in galprop
  double Xco = 1.0e20* pow(10,aco +bco*R)       ; // must be consistent with the CO maps used

  //        Xco = 0.1e20;//AWS20100408 test
  cout<<" adopted local ring  Xco = "<<Xco<<endl;






  double NH2_from_CO;
  double NHI_from_HI;
  double NH_from_EBV;
  double NH_HI_CO;

  // the local ring is 13 with index 12, but some gas is in neighbouring rings. Include them all to be sure.
  int ir_min =  0; // ring  1
  int ir_max = 16; // ring 17

  for(il=0;il<NHI.NAXES[0];il++)
  for(ib=0;ib<NHI.NAXES[1];ib++)
  {

 
   ir=12;// index for ring 13

   l = NHI.CRVAL[0] + NHI.CDELT[0]*il;
   b = NHI.CRVAL[1] + NHI.CDELT[1]*ib;

   if(fabs(b)>=bmin)
   {
     n_used++; 

   coord=SM::Coordinate(l,b);
   //   valarray<double> ebv = EBV[coord]; // note the argument in [] in this case
   //   valarray<double> ebv; //AWS20091204 test fix
   ArraySlice<double> ebv = EBV[coord];// new gardian requires this AWS20102025

  NHI_from_HI=0.;
  NH2_from_CO=0.;
  NH_HI_CO   =0.;

   for(ir=ir_min;ir<=ir_max;ir++)
   {
    NHI_from_HI +=  NHI(il,ib,ir);
    NH2_from_CO +=  COR(il,ib,ir)*2.0*Xco/1.e20; // NB atoms not molecules here
 
   }     

    NH_HI_CO     =  NHI_from_HI + NH2_from_CO;

 

    NHI_av      +=  NHI_from_HI;
    NH2_av      +=  NH2_from_CO;
    NH_HI_CO_av +=  NH_HI_CO;

   

   NH_from_EBV = (ebv[0]+EBV_offset) * NH_EBV/1.0e20; // units of FITS NHI are 1e20 cm-2
   NH_from_EBV_av +=NH_from_EBV;

   NHI_from_EBV_CO=0.;
   if (ebv[0]<EBV_max_use && (NH_from_EBV> NH_HI_CO  || pos_excess_only==0)   )
   NHI_from_EBV_CO= NH_from_EBV - NH2_from_CO; //AWS20100408

   if((verbose==1 || NHI_from_EBV_CO < 0.) || verbose==2) //AWS20100429
   {
    cout<<"NHI using EBV: il= "<<il<<" l="<<l<<" ib="<<ib<<" b="<<b<<" ";
    cout<<" EBV="<<ebv[0]<<" NHI 21cm="<<NHI_from_HI<<" "
        <<" Xco="<<Xco
        <<" NH2_from_CO ="<<NH2_from_CO 
        <<" NH_from_EBV="<<NH_from_EBV
	<<" NH_HI_CO ="<<NH_HI_CO
        <<" ratio ="<<NH_from_EBV/NH_HI_CO  
        <<" NHI_from_EBV_CO ="<< NHI_from_EBV_CO
        <<" NHI 21cm local ring  ="<<NHI(il,ib,12)
        <<endl;
       cout<<"NHI using EBV: il= "<<il<<" l="<<l<<" ib="<<ib<<" b="<<b<<" "
	   <<" NHI 21cm rings =     " ;     for(int irr=ir_min;irr<=ir_max;irr++)cout<<" "<<	NHI(il,ib,irr);
       cout<<endl; 
   }

   // assign excess NH to HI in local ring

   ir=12;// index for ring 13

   //   if (NH_from_EBV> NH_HI_CO     )
   if (ebv[0]<EBV_max_use && (NH_from_EBV> NH_HI_CO  || pos_excess_only==0)   ) //AWS20090915
   {
    if(distribute==0) //AWS20100429
    NHI(il,ib,ir)= NH_from_EBV - NH2_from_CO;
   
    if(distribute==1) //AWS20100429
    {
      for(int irr=ir_min;irr<=ir_max;irr++)
	NHI(il,ib,irr)= (NH_from_EBV - NH2_from_CO) *	NHI(il,ib,irr)/NHI_from_HI ;
    }
	  
    n_changed++;
   

   if((verbose==1 || NHI_from_EBV_CO < 0.) || verbose==2) //AWS20100429
   {
        cout<<"NHI using EBV: il= "<<il<<" l="<<l<<" ib="<<ib<<" b="<<b<<" "
            <<" replaced NHI rings = ";      for(int irr=ir_min;irr<=ir_max;irr++)cout<<" "<<	NHI(il,ib,irr);
        cout<<endl;
   }

   }// if ebv....

   }// if fabs(b)

   }//for il ib

   NHI_av         /=n_used;
   NH2_av         /=n_used;
   NH_HI_CO_av    /=n_used;
   NH_from_EBV_av /=n_used;

   // re-estimate  gas-to-dust from HI and CO (only valid if avoiding the dark gas)
   double gas_to_dust = NHI_av / NH_from_EBV_av  * NH_EBV;

   cout<<"  average for all pixels used : NHI_HI_CO = "<<NH_HI_CO_av <<" NH  from EBV = "<<NH_from_EBV_av
       <<" NHI = "<<NHI_av <<" NH2 = "<<NH2_av
       <<" ratio="<<NH_from_EBV_av/NH_HI_CO_av <<" revised gas-to-dust="<<gas_to_dust <<endl;
   cout<<" number of pixels with EBV estimate replacing NHI ="<<n_changed<< " out of "<<n_used<< " pixels cf total map pixels="<< NHI.NAXES[0]*NHI.NAXES[1]<<endl;

  strcpy(HIR_filename,"../FITS/rbands_hi12_v2_qdeg_zmax1_Ts125_EBV_localring_2.fits"   );
  strcpy(HIR_filename,"../FITS/rbands_hi12_v2_qdeg_zmax1_Ts125_EBV_localring_3.fits"   );//AWS20090915


  strcpy(HIR_filename,"junk.fits");                                                                               //AWS20100225
  // without b cut
  if(EBV_max_use==2.0)  strcpy(HIR_filename,"../FITS/rbands_hi12_v2_qdeg_zmax1_Ts125_EBV2.0_localring_4.fits"   );//AWS20100225
  if(EBV_max_use==5.0)  strcpy(HIR_filename,"../FITS/rbands_hi12_v2_qdeg_zmax1_Ts125_EBV5.0_localring_5.fits"   );//AWS20100225

  strcpy(HIR_filename,"junk.fits");   

 if(distribute==0)                                                                                                           //AWS20100429
 {
  if(pos_excess_only==1)                                                                                                     //AWS20100429
  {
  if( Ts==125 && EBV_max_use==2.0 )  strcpy(HIR_filename,"../FITS/rbands_hi12_v2_qdeg_zmax1_Ts125_EBV2.0_localring.fits"   );//AWS20100401
  if( Ts==125 && EBV_max_use==5.0 )  strcpy(HIR_filename,"../FITS/rbands_hi12_v2_qdeg_zmax1_Ts125_EBV5.0_localring.fits"   );//AWS20100401
  if( Ts==150 && EBV_max_use==2.0 )  strcpy(HIR_filename,"../FITS/rbands_hi12_v2_qdeg_zmax1_Ts150_EBV2.0_localring.fits"   );//AWS20100401
  if( Ts==150 && EBV_max_use==5.0  ) strcpy(HIR_filename,"../FITS/rbands_hi12_v2_qdeg_zmax1_Ts150_EBV5.0_localring.fits"   );//AWS20100401
  }
 }



 if(distribute==1)
 {
  if(pos_excess_only==0)                                                                                                                      //AWS20100429
  {
  if( Ts==125 && EBV_max_use==2.0 )  strcpy(HIR_filename,"../FITS/rbands_hi12_v2_qdeg_zmax1_Ts125_EBV2.0_pos_neg_excess_distributed.fits"   );//AWS20100429
  if( Ts==125 && EBV_max_use==5.0 )  strcpy(HIR_filename,"../FITS/rbands_hi12_v2_qdeg_zmax1_Ts125_EBV5.0_pos_neg_excess_distributed.fits"   );//AWS20100430
  if( Ts==150 && EBV_max_use==2.0 )  strcpy(HIR_filename,"../FITS/rbands_hi12_v2_qdeg_zmax1_Ts150_EBV2.0_pos_neg_excess_distributed.fits"   );//AWS20100430
  if( Ts==150 && EBV_max_use==5.0 )  strcpy(HIR_filename,"../FITS/rbands_hi12_v2_qdeg_zmax1_Ts150_EBV5.0_pos_neg_excess_distributed.fits"   );//AWS20100430
  if( Ts==150 && EBV_max_use==1e6 )  strcpy(HIR_filename,"../FITS/rbands_hi12_v2_qdeg_zmax1_Ts150_EBV1e6_pos_neg_excess_distributed.fits"   );//AWS20100430
  }

  if(pos_excess_only==1)                                                                                                                     //AWS20100429
  {
  if( Ts==125 && EBV_max_use==2.0 )  strcpy(HIR_filename,"../FITS/rbands_hi12_v2_qdeg_zmax1_Ts125_EBV2.0_pos_excess_distributed.fits"   );   //AWS20100430
  if( Ts==125 && EBV_max_use==5.0 )  strcpy(HIR_filename,"../FITS/rbands_hi12_v2_qdeg_zmax1_Ts125_EBV5.0_pos_excess_distributed.fits"   );   //AWS20100430
  if( Ts==150 && EBV_max_use==2.0 )  strcpy(HIR_filename,"../FITS/rbands_hi12_v2_qdeg_zmax1_Ts150_EBV2.0_pos_excess_distributed.fits"   );   //AWS20100430
  if( Ts==150 && EBV_max_use==5.0 )  strcpy(HIR_filename,"../FITS/rbands_hi12_v2_qdeg_zmax1_Ts150_EBV5.0_pos_excess_distributed.fits"   );   //AWS20100430
  if( Ts==150 && EBV_max_use==1e6 )  strcpy(HIR_filename,"../FITS/rbands_hi12_v2_qdeg_zmax1_Ts150_EBV1e6_pos_excess_distributed.fits"   );   //AWS20100430
  }
 }

  NHI.write(HIR_filename);

  /*
  strcpy(COR_filename,"../FITS/rbands_co10mm_2001_qdeg_EBV_localring_zeroed.fits"   );
  COR.write(COR_filename);
  */


  cout<<endl<<"****** remember to add the BINS extension for the R bins definition !"<<endl;

  cout<<"<<<< EBV_convert"<<endl;

  return 0;
}
