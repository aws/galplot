
//**.****|****.****|****.****|****.****|****.****|****.****|****.****|****.****|
// * Configure.cc *                                galprop package * 4/14/2000 
//**"****!****"****!****"****!****"****!****"****!****"****!****"****!****"****|
using namespace std;//AWS20050919
#include<iostream>  //AWS20050919
#include <cstring>  //AWS20111010 for sles11 gcc 4.3.4

#include"Configure.h"

int Configure::init()
{
   cout<<">>>>Configure"<<endl;
   directory_length=100;
   galdef_directory=new char[directory_length];
   fits_directory=new char[directory_length];
   adjunct_directory=new char[directory_length];
   strcpy(galdef_directory, "../GALDEF/");
   strcpy(  fits_directory, "../FITS/"  );
   strcpy(adjunct_directory,"../adjunct/"  );
   cout<<"Configure: galdef_directory:  "<< galdef_directory<<endl;
   cout<<"Configure:   fits_directory:  "<<   fits_directory<<endl;
   cout<<"Configure:adjunct_directory:  "<<adjunct_directory<<endl;
   cout<<"<<<<Configure"<<endl;
   return 0;
}
