

using namespace std;
#include"Galplot.h"                    //AWS20050920
#include"galprop_classes.h"
#include"galprop.h"
#include"constants.h"
#include"fitsio.h"
#include"SPIERSP.h"
//#include"CAT.h"                      //AWS20050921        

/////////////////////////////////////////////////////////////////////////
namespace SPI_spiskymax//  to protect local variables and share with local routines
{
   int status=0;
   char  infile[1000];
   int id,hdunum;

   FITS fits_map;

int longprof,latprof;

int i_lat,i_long;
int ii_long,ii_lat;
int il_min,il_max,ib_min,ib_max;
int il_min1,il_max1,ib_min1,ib_max1;
int il_min2,il_max2,ib_min2,ib_max2;
int il_temp;

double long_min,long_max,lat_min,lat_max;
double l,b,l_off;

double e_min,e_max;

char name[100],canvastitle[100], workstring1[100],workstring2[100],workstring3[100],workstring4[100];
char psfile[400];
}



/////////////////////////////////////////////////////////////
int Galplot::  plot_SPI_spiskymax_profile(int longprof,int latprof,int mode)
///////////////////////////////////////////////////////////
{
  using namespace SPI_spiskymax;

   cout<<" >>>> plot_SPI_spiskymax_profile    "<<endl;



 id=2612;
 strcpy( infile,configure.fits_directory);
 strcat( infile,"spiskymax_image."   );   
 strcat( infile,galplotdef.spiskymax_image_ID   );   
 strcat( infile,".fits"   );   


// skip primary header and grouping extension
hdunum=galplotdef.spiskymax_iteration+2;
fits_map.read(infile,hdunum);
//fits_map.print();



fits_map.read_keyword_double(infile,hdunum,"E_MIN",e_min);
fits_map.read_keyword_double(infile,hdunum,"E_MAX",e_max);

// energies are keV in spiskymax, we need MeV
e_min*=1e-3;
e_max*=1e-3;

cout<<"e_min= "<<e_min<< " MeV " ;
cout<<"e_max= "<<e_max<< " MeV "<<endl;

 

TCanvas *c1;
TH2F *map;
TH1D *profile;



int ncolors=1; int *colors=0;
gStyle->SetPalette(ncolors,colors);

  //====== see HowTo Style: and do before creating canvas
 
 
   TStyle *plain  = new TStyle("Plain","Plain Style (no colors/fill areas)"); 

   
   plain->SetCanvasBorderMode(0);
   plain->SetPadBorderMode(0);
   plain->SetPadColor(0);
   plain->SetCanvasColor(0);
   plain->SetTitleColor(0);
   plain->SetStatColor(0);
   gROOT->SetStyle("Plain");

 // names must be different or canvas disappears

  
  sprintf(name," %5.3f - %5.3f MeV",e_min,e_max);
  strcpy(canvastitle,"galdef_");
  strcat(canvastitle,galdef.galdef_ID);
  strcat(canvastitle,name);



  /*  
  map=new TH2F("SPI skymap",canvastitle,
                  galaxy.n_long,       -180,           180.,
                  galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1));




	 for (i_lat =0;  i_lat <galaxy.n_lat;  i_lat++ )
         {
          for(i_long=0;  i_long<galaxy.n_long; i_long++)
          {

           l=galaxy.long_min+galaxy.d_long*i_long;
           b=galaxy. lat_min+galaxy. d_lat*i_lat;



            l_off=l-180.;
            if(l_off<0.)l_off+=360.0;
            ii_long= int(galaxy.n_long-l_off/galaxy.d_long);
	    //cout<<"l l_off ii_long"<<l<<" "<<l_off<<" "<<ii_long<<endl;

              
               map->SetBinContent(ii_long,i_lat,fits_map(i_long,i_lat)); 
 

            }  //  i_long
           }   //  i_lat


  */

  long_min=  fits_map.CRVAL[0] -   (fits_map.CRPIX[0]-1)*fits_map.CDELT[0];
  long_max=  long_min             +(fits_map.NAXES[0]-1)*fits_map.CDELT[0];

   lat_min=  fits_map.CRVAL[1] -   (fits_map.CRPIX[1]-1)*fits_map.CDELT[1];
   lat_max=   lat_min             +(fits_map.NAXES[1]-1)*fits_map.CDELT[1];



  cout<<"NAXES[0]="<<   fits_map.NAXES[0]<<" ";
  cout<<"CRVAL[0]="<<   fits_map.CRVAL[0]<<" ";
  cout<<"CRPIX[0]="<<   fits_map.CRPIX[0]<<endl;
  cout<<"NAXES[1]="<<   fits_map.NAXES[1]<<" ";
  cout<<"CRVAL[1]="<<   fits_map.CRVAL[1]<<" ";
  cout<<"CRPIX[1]="<<   fits_map.CRPIX[1]<<endl;
  cout<<"long_min="<<  long_min         <<" long_max="<< long_max                                                 <<endl;
  cout<<" lat_min="<<   lat_min         <<"  lat_max="<<  lat_max                                                 <<endl;



  map=new TH2F("SPI skymap",canvastitle,
	       fits_map.NAXES[0],   long_max,long_min,                 // incorrect sense, root restriction, will have to make axes separately
               fits_map.NAXES[1],    lat_min, lat_max) ;               // as for longitude plot                                                                    




	 for (i_lat =0;  i_lat <fits_map.NAXES[1];  i_lat++ )
         {
          for(i_long=0;  i_long<fits_map.NAXES[0]; i_long++)
          {

           l=galaxy.long_min+galaxy.d_long*i_long;
           b=galaxy. lat_min+galaxy. d_lat*i_lat;



            l_off=l-180.;
            if(l_off<0.)l_off+=360.0;
            ii_long= int(galaxy.n_long-l_off/galaxy.d_long);
	    //cout<<"l l_off ii_long"<<l<<" "<<l_off<<" "<<ii_long<<endl;

              
               map->SetBinContent(i_long,i_lat,fits_map(i_long,i_lat)); 
 

            }  //  i_long
           }   //  i_lat

  if(mode==1)
  {
   c1=new TCanvas(name,canvastitle,300,150,600,600);
   map->Draw("lego"); 
  }

  // --------------------------------- profiles

//------------------------------     axes      -------------------------
  TF1 *f1=new TF1("f1","-x",   0,180); // root manual p. 149
  TF1 *f2=new TF1("f2","-x", 180,340); // root manual p. 149

  //                      xmin ymin xmax ymax, funcname   ndiv chopt gridlength
  TGaxis *axis1=new TGaxis(-180,-90 ,  0.,-90  , "f1",       9,""                );
  axis1->SetLabelSize(0.03);
  TGaxis *axis2=new TGaxis(  20,-90 ,180.,-90  , "f2",       8,""                );
  axis2->SetLabelSize(0.03);
  axis2->SetTitle("Galactic longitude");
  // fill in 0-340 between two axis segments, unlabelled
  // at present has no visibile effect but could be useful if more minor tickmarks added
  //                         xmin ymin xmax ymax   wmin wmax ndiv chopt gridlength
  TGaxis *axis3=new TGaxis(  0,  -90.  , 20., -90., 0., 20. ,0, "U"               );

  // construct conventional Galactic longitude axis in two segments, 180-0 and 340-180
  f1=new TF1("f1","-x",   0,180); // root manual p. 149
  f2=new TF1("f2","-x", 180,340); // root manual p. 149

  //                      xmin ymin xmax ymax, funcname   ndiv chopt gridlength
          axis1=new TGaxis(-180,0.  ,  0.,0.   , "f1",       9,""                );
  axis1->SetLabelSize(0.02);
          axis2=new TGaxis(  20,0.  ,180.,0.   , "f2",       8,""                );
  axis2->SetLabelSize(0.02);
  axis2->SetTitle("Galactic longitude");

  
 //////////////////////////////////////////////////////////////////////////////////////////////





  // ----------------------------------longitude profile

  if(longprof==1)
  {
  ib_min1=int((galplotdef.lat_min1-lat_min+.0001)/ fits_map.CDELT[1]);
  ib_max1=int((galplotdef.lat_max1-lat_min+.0001)/ fits_map.CDELT[1]);
  ib_min2=int((galplotdef.lat_min2-lat_min+.0001)/ fits_map.CDELT[1]);
  ib_max2=int((galplotdef.lat_max2-lat_min+.0001)/ fits_map.CDELT[1]);

  cout<<"ib_min1 ibmax1:"<<ib_min1<<" "<<ib_max1<<endl;
  cout<<"ib_min2 ibmax2:"<<ib_min2<<" "<<ib_max2<<endl;

  profile=     map->ProjectionX("profile1" ,ib_min1,ib_max1); // here name must be distinct
  profile->Add(map->ProjectionX("dummy1" ,  ib_min2,ib_max2),1.0);

  profile->Scale(1.0/(ib_max1-ib_min1+1 + ib_max2-ib_min2+1));
 
  profile->GetXaxis()->SetTitle("Galactic longitude");
  profile->GetXaxis()->SetNdivisions(0); // since axis drawn separately
  profile->GetYaxis()->SetTitle("intensity, cm^{-2} sr^{-1} s^{-1}  ");
  profile->GetYaxis()->SetTitleOffset(1.2);
  profile->GetYaxis()->SetLabelSize  (0.030);

  profile->SetTitle(""         ); //removes text written in box on plot
  //  profile->SetMaximum(profile->GetMaximum()*1.5); // to allow for data with larger range
 
  if(galplotdef.lat_log_scale==0)
   profile->SetMaximum(profile->GetMaximum()*1.5); // to allow for data with larger range
  if(galplotdef.lat_log_scale==1)
   profile->SetMaximum(profile->GetMaximum()*2.0); 
  if(galplotdef.lat_log_scale==1)                     
   profile->SetMinimum(profile->GetMaximum()/1e5);   



  TGaxis::SetMaxDigits(4);  // see root #1329 and mail root 20 Aug 2002

  profile->SetStats(kFALSE);
  profile->SetLineStyle(1     );// 1=solid 2=dash
  profile->SetLineWidth(3     );
  profile->SetLineColor(kBlack);


  strcpy(name,"longitude profile");
   strcpy(canvastitle,name);
 

  c1=new TCanvas(name,canvastitle,400,250,600,600);
  if(galplotdef.long_log_scale==1)c1->SetLogy(); //a TCanvas is a TPad, TPad has this function.  
  profile->Draw("L");

  

//------------------------------     axes      -------------------------
  

  axis1->Draw();   axis2->Draw();   axis3->Draw();

  }// longprof

  // --------------------------- latitude profile


  if(latprof==1)
  {
  // should handle cases CDELT > or < 0 but only <0 tested !

  l_off=galplotdef.long_max1-long_min; if(l_off>360.)l_off-=360.; if(l_off<  0.)l_off+=360.;
  il_max1=int(abs(l_off/ fits_map.CDELT[0]));

  l_off=galplotdef.long_min1-long_min; if(l_off>360.)l_off-=360.; if(l_off<  0.)l_off+=360.;
  il_min1=int(abs(l_off/ fits_map.CDELT[0]));

  if(il_min1>il_max1)
    {
      il_temp=il_min1;
      il_min1=il_max1;
      il_max1=il_temp;
    }

  

  l_off=galplotdef.long_max2-long_min; if(l_off>360.)l_off-=360.; if(l_off<  0.)l_off+=360.;
  il_max2=int(abs(l_off/ fits_map.CDELT[0]));

  l_off=galplotdef.long_min2-long_min; if(l_off>360.)l_off-=360.; if(l_off<  0.)l_off+=360.;
  il_min2=int(abs(l_off/ fits_map.CDELT[0]));

  if(il_min2>il_max2)
    {
      il_temp=il_min2;
      il_min2=il_max2;
      il_max2=il_temp;
    }

  cout<<"il_min1 ilmax1:"<<il_min1<<" "<<il_max1<<endl;
  cout<<"il_min2 ilmax2:"<<il_min2<<" "<<il_max2<<endl;

  profile=     map->ProjectionY("profile2" ,il_min1,il_max1 );   // here name must be distinct
  profile->Add(map->ProjectionY("dummy2" ,  il_min2,il_max2),1.0);

  profile->Scale(1.0/(il_max1-il_min1+1 + il_max2-il_min2+1));

  profile->GetXaxis()->SetTitle("Galactic  latitude");
  profile->GetYaxis()->SetTitle("intensity, cm^{-2} sr^{-1} s^{-1}  ");
  profile->GetYaxis()->SetTitleOffset(1.2);
  profile->GetYaxis()->SetLabelSize  (0.030);

  profile->SetTitle(""         ); // removes text written in box on plot
  //  profile->SetMaximum(profile->GetMaximum()*1.5); // to allow for data with larger range
   if(galplotdef.lat_log_scale==0)
   profile->SetMaximum(profile->GetMaximum()*1.5); // to allow for data with larger range
  if(galplotdef.lat_log_scale==1)
   profile->SetMaximum(profile->GetMaximum()*2.0); 
  if(galplotdef.lat_log_scale==1)                     
   profile->SetMinimum(profile->GetMaximum()/1e5);    



  TGaxis::SetMaxDigits(4);  // see root #1329 and mail root 20 Aug 2002

  profile->SetStats(kFALSE);
  profile->SetLineStyle(1     );// 1=solid 2=dash
  profile->SetLineWidth(3     );
  profile->SetLineColor(kBlack);

  strcpy(name,"latitude profile");
  strcpy(canvastitle,name);
  c1=new TCanvas(name,canvastitle,500,350,600,600);
  if(galplotdef.lat_log_scale==1)c1->SetLogy();                //a TCanvas is a TPad, TPad has this function
  profile->Draw("L");
 



  
  }

  // parameters labelling

  sprintf(workstring1,"  %5.2f<l<%5.2f , %5.2f<l<%5.2f",                 //AWS20040315
                          galplotdef.long_min1,galplotdef.long_max1,
                          galplotdef.long_min2,galplotdef.long_max2);
 
  sprintf(workstring2,"  %5.2f<b<%5.2f , %5.2f<b<%5.2f",                 //AWS20040315
                         galplotdef.lat_min1,galplotdef.lat_max1,
                         galplotdef.lat_min2,galplotdef.lat_max2);
  
  sprintf(workstring3," %5.3f - %5.3f MeV",e_min,e_max);


  strcpy(workstring4," galdef ID ");
  strcat(workstring4,galdef.galdef_ID);


  TText *text=new TText();
  text->SetTextFont(62);
  text->SetTextColor(kBlack);
  text->SetTextSize(0.025 );
  text->SetTextAlign(12);

  if(longprof)
  text->DrawTextNDC(.56 ,.85 ,workstring2);// NDC=normalized coord system  AWS20040315
  if(latprof)
  text->DrawTextNDC(.52 ,.85 ,workstring1);// NDC=normalized coord system  AWS20040315
  text->DrawTextNDC(.58 ,.88 ,workstring3);// NDC=normalized coord system
  text->DrawTextNDC(.20 ,.92 ,workstring4);// NDC=normalized coord system





  //  plot_SPI_model_profile( longprof,  latprof, e_min,e_max, ic, bremss, total);
 plot_SPI_model_profile( longprof,  latprof, e_min , e_max ,    1     , 1  ,1  );

  //============== postscript output

  sprintf(workstring1,"l_%.2f_%.2f_%.2f_%.2f",                          
                          galplotdef.long_min1,galplotdef.long_max1,
                          galplotdef.long_min2,galplotdef.long_max2);

  sprintf(workstring2,"b_%.2f_%.2f_%.2f_%.2f",                         
                          galplotdef.lat_min1,galplotdef.lat_max1,
                          galplotdef.lat_min2,galplotdef.lat_max2);

  sprintf(workstring3,"%.3f-%.3f_MeV",e_min,e_max);



  strcpy(psfile,"plots/");
  if(longprof==1)
  strcat(psfile,"longitude_profile_");
  if(latprof==1)
  strcat(psfile,"latitude_profile_");
  if(   mode==1)
  strcat(psfile,"lego_"  
          );
  strcat(psfile,galdef.galdef_ID);
  strcat(psfile,"_"        );
  strcat(psfile,workstring3);
  strcat(psfile,"_"        );
  if(longprof==1)
  strcat(psfile,workstring2);
  if( latprof==1)
  strcat(psfile,workstring1);

  strcat(psfile,"_spiskymax_");



  strcat(psfile,galplotdef.psfile_tag);

  strcat(psfile,".eps");

  cout<<"postscript file="<<psfile<<endl;
  c1->Print(psfile       );
  //==============







   cout<<" <<<< plot_SPI_spiskymax_profile    "<<endl;
 
 return 0;
}
///////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////
/// enable this for stand-alone test

/*
int main(int argc,char*argv[])   
{
  cout<<"testing plot_SPI_spiskymax_profile"<<endl;
     char version[]="48";  
 
 
   cout<<">>>>galplot version "<<version<<endl;
 
   if(argc!=2){cout<<"no galdef file specified!"<<endl;return -1;}
   cout<<argc<<" "<<argv[0]<<" "<<argv[1]<<endl;
 
   if(configure.init() !=0)return 1;
   cout<<configure.galdef_directory<<endl;// just a test
 
   if(galdef.read  (version,argv[1],configure.galdef_directory) !=0) return 1;
 
   if(galplotdef.read  (version,argv[1],configure.galdef_directory) !=0) return 1;

  configure.init();
  

  txtFILE=fopen("junk","w");


  extern void InitGui();
   VoidFuncPtr_t initFuncs[] = {InitGui, 0};
   TROOT root ("ISDC", "galplot", initFuncs);
   //int argc=0; char * argv[]={" "};
   TApplication theApp("galplot", &argc, argv);

  

  plot_SPI_spiskymax_profile(1,0,0);
  plot_SPI_spiskymax_profile(0,1,0);
  plot_SPI_spiskymax_profile(0,0,1);

   theApp.Run();
  return 0;

}


*/
