
#include"Galplot.h"                    //AWS20050920
#include"galprop_classes.h"
#include"galprop.h"
#include"constants.h"
#include"fitsio.h"


int  Galplot::plot_gcr_spectra_data(int Z, int A, int index_mult, int Ek_or_momentum) //AWS20130201
{
   cout<<" >>>> plot_gcr_spectra_data    "<<endl;
   int status=0;

GCR_data gcr_data;

char *database_file=new char[400];
char *area_units  =new char[4];
char *energy_units=new char[4];
char *Y_name      =new char[20];
int n_ZA_numerator_select,   *Z_numerator_select,  *A_numerator_select;
int n_ZA_denominator_select, *Z_denominator_select,*A_denominator_select;

// for kinematic
 double *p_mean,*p_low,*p_high; double Ekin,Etot,beta, gamma,  rigidity; char species[10]; int test_kinematic=0; //AWS20130201

int ip;
int i_ZA;                 // index of species in galplotdef parameters
int color;                //AWS20060320
Style_t markerstyle;      //AWS20060619

char name[100],canvastitle[100], workstring1[100],workstring2[100];


TGraphAsymmErrors *spectrum;
TText *text;



 
 n_ZA_numerator_select=3;
 Z_numerator_select=new int[n_ZA_numerator_select];
 A_numerator_select=new int[n_ZA_numerator_select];
 n_ZA_denominator_select=3;
 Z_denominator_select=new int[n_ZA_denominator_select];
 A_denominator_select=new int[n_ZA_denominator_select];

strcpy(Y_name,       "flux");
strcpy(area_units,   "cm2");
strcpy(energy_units, "MeV");
//strcpy(database_file,"GCR_data_1.dat");           //AWS20090220
strcpy(database_file,galplotdef.gcr_database_file); //AWS20090220


    for(i_ZA=0;i_ZA<n_ZA_numerator_select;i_ZA++)
    {
      Z_numerator_select[i_ZA]=0;
      A_numerator_select[i_ZA]=0;
    }
    for(i_ZA=0;i_ZA<n_ZA_denominator_select;i_ZA++)
    {
      Z_denominator_select[i_ZA]=0;
      A_denominator_select[i_ZA]=0;
    }




      i_ZA=0;
      Z_denominator_select[i_ZA]= Z;
      A_denominator_select[i_ZA]= A;
     


    /* == test case
 Z_numerator_select[0]=5;
 A_numerator_select[0]=10;
 Z_numerator_select[1]=5;
 A_numerator_select[1]=11;
 Z_numerator_select[2]=0;
 A_numerator_select[2]=0; 

 Z_denominator_select[0]=6;
 A_denominator_select[0]=12;
 Z_denominator_select[1]=6;
 A_denominator_select[1]=13;
 Z_denominator_select[2]=0;
 A_denominator_select[2]=0;
    */

   

 gcr_data.read(database_file ,       area_units ,       energy_units ,
	       Y_name,
	       n_ZA_numerator_select,       Z_numerator_select,       A_numerator_select,
               n_ZA_denominator_select,     Z_denominator_select,     A_denominator_select);

 gcr_data.print();

 


 // modified to enable different marker colours, shapes per point  //AWS20060321

 for  (ip    =0;  ip    <gcr_data.n    ;ip    ++)                                    //AWS20060324                              
   if(gcr_data. E_mean[ip]==0.0)                                                     //AWS20060324
      gcr_data. E_mean[ip]=     (gcr_data. E_low  [ip]+gcr_data. E_high [ip])/2.0;   //AWS20060324






/*
 kinematic(int Z, int A, char *species, 
              double &p, double &Ekin,double &Etot, 
              double &beta, double &gamma, double &rigidity,int test) 
 */


         strcpy(species,"nucleus"); 
 if(A==0)strcpy(species,"electron"); 

 p_mean=new double[gcr_data.n];
 p_low =new double[gcr_data.n];
 p_high=new double[gcr_data.n];

 for  (ip=0;ip<gcr_data.n;ip++)
 {
  p_mean[ip] = -1; // kinematic will then compute p from input Ekin
  p_low [ip] = -1;
  p_high[ip] = -1;

  Ekin =  gcr_data.E_mean[ip];
  
  kinematic(Z, A, species, p_mean[ip],  gcr_data.E_mean[ip]  , Etot, beta, gamma, rigidity,test_kinematic) ;
  kinematic(Z, A, species, p_low [ip],  gcr_data.E_low [ip]  , Etot, beta, gamma, rigidity,test_kinematic) ;
  kinematic(Z, A, species, p_high[ip],  gcr_data.E_high[ip]  , Etot, beta, gamma, rigidity,test_kinematic) ;
  
  cout<<"plot_gcr_spectra_data: Z="<<Z<<" A="<<A<<" ip="<<ip<<" p_mean="<<p_mean[ip]<<" computed from  gcr_data.E_mean="<<gcr_data.E_mean[ip]<<endl;
  cout<<"plot_gcr_spectra_data: Z="<<Z<<" A="<<A<<" ip="<<ip<<" p_low ="<<p_low [ip]<<" computed from  gcr_data.E_low ="<<gcr_data.E_low [ip]<<endl;
  cout<<"plot_gcr_spectra_data: Z="<<Z<<" A="<<A<<" ip="<<ip<<" p_high="<<p_high[ip]<<" computed from  gcr_data.E_high="<<gcr_data.E_high[ip]<<endl;

 }
  cout<<"plot_gcr_spectra_data: calculation of momentum  using kinematic complete"<<endl;


  for  (ip    =0;  ip    <gcr_data.n    ;ip    ++)
  {
   spectrum=new TGraphAsymmErrors(1    );


   if(Ek_or_momentum==1) // Ekin
   {
   spectrum->SetPoint     (0,gcr_data. E_mean[ip],  gcr_data.value [ip]*pow(gcr_data. E_mean[ip],index_mult)    ); //AWS20091009

   spectrum->SetPointError(0, gcr_data. E_mean  [ip]-      gcr_data. E_low [ip],
                              gcr_data. E_high  [ip]-      gcr_data. E_mean[ip],
			      gcr_data.err_minus[ip]  *pow(gcr_data. E_mean[ip],index_mult),                       //AWS20091009
                              gcr_data.err_plus [ip]  *pow(gcr_data. E_mean[ip],index_mult)   );                   //AWS20091009
   }


  if(Ek_or_momentum==2) // momentum
  {
   double value    = gcr_data.    value [ip]/A;              // I(Ekin)->n(p)
   double err_minus= gcr_data.err_minus [ip]/A;
   double err_plus = gcr_data.err_plus  [ip]/A;

   spectrum->SetPoint     (0,               p_mean[ip],           value     *pow(               p_mean[ip],index_mult)    ); 

   spectrum->SetPointError(0,           p_mean  [ip]-                p_low [ip],
                                        p_high  [ip]-                p_mean[ip],
			               err_minus      *pow(          p_mean[ip],index_mult),                    
                                       err_plus       *pow(          p_mean[ip],index_mult)   );                  
   }

    

  //Gtypes.h:enum EColor { kWhite, kBlack, kRed, kGreen, kBlue, kYellow, kMagenta, kCyan };

     // TAttMarker
     //*-*-*-*-*-*-*-*-*-*-*-*Marker Attributes class*-*-*-*-*-*-*-*-*-*-*-*-*-*
     //*                      =======================
     //*  Marker attributes are:
     //*    Marker Color
     //*    Marker style
     //*    Marker Size
     //*
     //*  This class is used (in general by secondary inheritance)
     //*  by many other classes (graphics, histograms).
     //*
     //*  List of the currently supported markers (screen and PostScript)
     //*  ===============================================================
     //*      1 : dot                     kDot
     //*      2 : +                       kPlus
     //*      3 : *                       kStar
     //*      4 : o                       kCircle
     //*      5 : x                       kMultiply
     //*      6 : small scalable dot      kFullDotSmall
     //*      7 : medium scalable dot     kFullDotMedium
     //*      8 : large scalable dot      kFullDotLarge
     //*      9 -->15 : dot
     //*     16 : open triangle down      kOpenTriangleDown
     //*     18 : full cross              kFullCross
     //*     20 : full circle             kFullCircle
     //*     21 : full square             kFullSquare
     //*     22 : full triangle up        kFullTriangleUp
     //*     23 : full triangle down      kFullTriangleDown
     //*     24 : open circle             kOpenCircle
     //*     25 : open square             kOpenSquare
     //*     26 : open triangle up        kOpenTriangleUp
     //*     27 : open diamond            kOpenDiamond
     //*     28 : open cross              kOpenCross
     //*     29 : open star               kOpenStar
     //*     30 : full star               kFullStar
     //*

     /*-----------------------------------------------
     // default marker
  color=kBlack;                                                   //AWS20060320
  markerstyle=kOpenCircle;                                        //AWS20060619



  // all experiments in GCR_data_1.dat

  if(strncmp(gcr_data.experiment[ip],"AMS01"   ,5)==0){ color=kMagenta;markerstyle=kOpenCircle;  } //AWS20090227
  if(strncmp(gcr_data.experiment[ip],"JACEE"   ,5)==0){ color=kRed ;   markerstyle=kOpenSquare;  }                  
  if(strncmp(gcr_data.experiment[ip],"BESS"    ,4)==0){ color=kRed ;   markerstyle=kOpenTriangleUp  ;}            
  if(strncmp(gcr_data.experiment[ip],"IMP"     ,3)==0){ color=kRed ;   markerstyle=kOpenTriangleDown;}
  if(strncmp(gcr_data.experiment[ip],"CAPRICE" ,7)==0){ color=kRed  ;  markerstyle=kOpenStar  ;  } 
  if(strncmp(gcr_data.experiment[ip],"RUNJOB"  ,6)==0){ color=kRed ;   markerstyle=kOpenCross ;  } 

  if(strncmp(gcr_data.experiment[ip],"HEAT"    ,4)==0){ color=kBlue ;  markerstyle=kFullCircle;  } 
  if(strncmp(gcr_data.experiment[ip],"HEAO3"   ,5)==0){ color=kBlue;   markerstyle=kFullSquare;  }
  if(strncmp(gcr_data.experiment[ip],"SOKOL"   ,5)==0){ color=kBlue;   markerstyle=kFullTriangleUp  ; }
  if(strncmp(gcr_data.experiment[ip],"SANRIKU" ,7)==0){ color=kBlue;   markerstyle=kFullTriangleDown;}
  if(strncmp(gcr_data.experiment[ip],"IMAX"    ,4)==0){ color=kBlue;   markerstyle=kFullStar ;  } 
  if(strncmp(gcr_data.experiment[ip],"CRN"     ,3)==0){ color=kBlue;   markerstyle=kOpenDiamond; } // kFullCross:no symbol plotted

  if(strncmp(gcr_data.experiment[ip],"ACE"     ,3)==0){ color=kCyan;   markerstyle=kPlus      ;  }
  if(strncmp(gcr_data.experiment[ip],"Voyager" ,7)==0){ color=kCyan;   markerstyle=kMultiply  ;  } 
  if(strncmp(gcr_data.experiment[ip],"MUBEE"   ,5)==0){ color=kCyan;   markerstyle=kOpenDiamond     ; } 

 ---------------    */

     /*AWS20110124
  cout<<"Z,A ="<<Z<<","<<A<< "  experiment: "<<  gcr_data.experiment[ip]<<" colour= "<<color<<" markerstyle="<<markerstyle
      <<" colour= "<<gcr_data.color[ip]<<" style="<<gcr_data.style[ip]<<" size="<<gcr_data.size[ip]
      <<" E_mean="<<gcr_data. E_mean[ip]<<" plot value="<<  gcr_data.value [ip]*pow(gcr_data. E_mean[ip],2) <<endl;          //AWS20060320
     */

  /*
  spectrum->SetMarkerColor(color);                                //AWS20060320
  spectrum->SetMarkerStyle(markerstyle); // see TAttMarker        //AWS20060619

  //  spectrum->SetMarkerStyle(22); // triangles
  spectrum->SetMarkerSize (1.0);// 
  spectrum->SetLineColor(color );                                 //AWS20060320
  */

  cout<<"plot_gcr_spectra_data: Z,A ="<<Z<<","<<A<< "  experiment: "<<  gcr_data.experiment[ip]
      <<" colour= "<<gcr_data.color[ip]<<" style="<<gcr_data.style[ip]<<" size="<<gcr_data.size[ip]
      <<" E_mean="<<gcr_data. E_mean[ip]<<" plot value="<<  gcr_data.value [ip]*pow(gcr_data. E_mean[ip],2) <<endl; //AWS20110124      

  spectrum->SetMarkerColor(gcr_data.color[ip]);                          //AWS20060621
  spectrum->SetMarkerStyle(gcr_data.style[ip]); // see TAttMarker        //AWS20060621
  spectrum->SetMarkerSize (gcr_data.size [ip]);                          //AWS20060621
  spectrum->SetLineColor  (gcr_data.color[ip] );                         //AWS20060621

  spectrum->Draw(" P ");  // points as markers  
     
  }// for

 /*                                                      //AWS20060321
  spectrum=new TGraphAsymmErrors(gcr_data.n    );

 
  
  for  (ip    =0;  ip    <gcr_data.n    ;ip    ++)
    {
   spectrum->SetPoint     (ip,gcr_data. E_mean[ip],  gcr_data.value [ip]*pow(gcr_data. E_mean[ip],2)    );

   spectrum->SetPointError(ip,gcr_data. E_mean  [ip]-     gcr_data. E_low  [ip],
                              gcr_data. E_high  [ip]-      gcr_data. E_mean[ip],
			      gcr_data.err_minus[ip]  *pow(gcr_data. E_mean[ip],2), 
                              gcr_data.err_plus [ip]  *pow(gcr_data. E_mean[ip],2)   );

    }

  //Gtypes.h:enum EColor { kWhite, kBlack, kRed, kGreen, kBlue, kYellow, kMagenta, kCyan };

  color=kBlack;                                                  //AWS20060320
  cout<<"experiment: "<<  gcr_data.experiment[ip]<<endl;                         //AWS20060320
  if(strncmp(gcr_data.experiment[ip],"BESS",4)==0) color=kBlue;  //AWS20060320
  if(strncmp(gcr_data.experiment[ip],"BESS",4)==0) color=kBlue;  //AWS20060320

  spectrum->SetMarkerColor(color);                            //AWS20060320
//spectrum->SetMarkerStyle(21); 
//spectrum->SetMarkerStyle(22); // triangles
  spectrum->SetMarkerStyle(markerstyle);                      //AWS20060619 
  spectrum->SetMarkerSize (1.0);// 
  spectrum->SetLineColor(color );                             //AWS20060320

 
  spectrum->Draw(" P ");  // points as markers                       
 */


   cout<<" <<<< plot_gcr_spectra_data   "<<endl;
   return status;
}


