#ifndef Parameters_h
#define Parameters_h

#include <iostream>
#include <string>
#include <map>
#include <vector>
#include <exception>
#include <sstream>

/** \brief Trim white space from end of string */
std::string TrimWhitespace(std::string & str);

/** \brief Class to handle parsing of simple parameter = value pairs.
 *
 * Uses templated functions and the >> input operator to handle different types
 * of values.
 *
 * For now, parameter can not contain whitespace and there must be a whitespace
 * between the parameter and the = sign.
 */
class Parameters {
	private:
		typedef std::map<std::string, std::string> maptype; //!< Alias typedef for a map of string pairs, used to store the parameter name and value
		typedef maptype::const_iterator cmapiterator; //!< constant interator for the map
		maptype fParameters; //!< Storage for the parmeter name/value pairs
		std::string fCommentString; //!< The comment string is defined in the constructor.
		/** \brief This class holds the neccessary input/output operations for each
		 * line of parameter = value pair.
		 *
		 * It is assumed that the parameter does not contain spaces and there is a
		 * space between the parameter and the equal sign.
		 */
		class Pair {
			/** \brief Output operator for a pair 
			 *
			 * Outputs a parameter = value string
			 */
			friend std::ostream & operator << (std::ostream & os, const Pair &pair);
			/** \brief Input operator for a pair 
			 *
			 * Input must be of the form
			 * \code
			 * ParameterName = value can be a vector with whitespace
			 * \endcode
			 * \note The space between the parameter name and the =-sign is
			 * mandatory.
			 */
			friend std::istream & operator >> (std::istream & is, Pair &pair);
			private:
				std::string fKey, fValue; //!< Store the name and value
			public:
				/** \brief Create the pair from an input stream.
				 *
				 * \param is is the input stream to be parsed.
				 */
				Pair(std::istream & is) { is >> *this;}
				/** \brief Create a pair from two strings.
				 * 
				 * \param key is the parameter name
				 * \param value is it value in string form
				 */
				Pair(const std::string & key, const std::string & value) : fKey(key), fValue(value) {}
				/** \brief Create a pair from a std::pair of two strings.
				 * 
				 * \param pair is the std::pair where the first value is the parameter
				 * name and the second value is the parameter value.
				 */
				Pair(const std::pair<std::string,std::string> &pair) : fKey(pair.first), fValue(pair.second) {}
				/** \brief Return a std::pair with the Pair info.
				 *
				 * \return a std::pair of two string, where the first is the parameter
				 * name and the second is the parameter value.
				 */
				std::pair<std::string, std::string> getPair() const  {return std::pair<std::string,std::string>(fKey,fValue);}
		};
		/** \brief Helper class to print all the parameters.
		 *
		 * Used in conjuction with the for_each algorithm to print the pairs.
		 */
		class Print {
			private:
				std::ostream & fos;
				Print();
			public:
				/** \brief The constructor takes an output stream as an argument */
				Print(std::ostream & os) : fos(os) {}
				/** \brief Default operator works on std::pairs of strings */
				void operator () (const std::pair<const std::string, std::string> & pair){
					fos << Pair(pair);
				}
		};
		/** \brief Strip comment string and everything after it from the input str.
		 *
		 * \param str is the string from which to strip comments.
		 * \return a copy of the original string after comments have been removed.
		 *
		 * This utilises the comment string set in the constructor
		 */
		std::string StripComments(std::string & str);

	public:
		/** \brief Error class for the Parameters class. */
		class ParameterError : public std::exception {
			private:
				std::string eMessage; //!< store the error message
			public:
				/** \brief Construct an error with the default message "Error in
				 * Parmaeters class"
				 */
				ParameterError() : eMessage("Error in Parameters class"){}
				/** \brief Construct an error with a customized error message.
				 *
				 * \param message is the error message to display.
				 */
				ParameterError(const std::string & message) : eMessage(message){}
				~ParameterError() throw(){}
				/** \brief Return the error message of this error */
				virtual const char* what () const throw(){
					return(eMessage.c_str());
				}
		};
		/** \brief Construct an empty Parameters object.
		 *
		 * \param commentString is the comment string; all characters on a line, including and after this string
		 * in the parsed input streams will be discarded.  Defaults to "#".
		 */
		Parameters(std::string commentString="#") : fCommentString(commentString) {}
		/** \brief Construct a Parameters object from an input stream.
		 *
		 * \param is is the input stream to be parsed.  It will be parsed line by
		 * line and all comments stripped off.
		 * \param commentString is the comment string; all characters on a line, including and after this string
		 * in the parsed input streams will be discarded.  Defaults to "#".
		 */
		Parameters(std::istream & is, std::string commentString="#") : fCommentString(commentString) {ParseIstream(is);}
		/** \brief Parse a parameter stream into the Parameters object.
		 *
		 * \param is is the input stream to be parsed.  It will be parsed line by
		 * line and all comments stripped off.
		 */
		void ParseIstream(std::istream & is);
		/** \brief Print all the parameters to a output stream
		 *
		 * \param os is the output stream to which the parameters will be printed.
		 */
		void PrintOstream(std::ostream & os) const;
		/** \brief Return a parameter value given its name.
		 *
		 * This is a templated function and the results depend on the type of the
		 * input parameter.
		 * \param parameter is the name of the parameter
		 * \param value is a reference to a variable which will contain the
		 * parameter value on return.
		 *
		 * The basic input mechanism of c++ is used to parse the value string, so
		 * only the first non-whitespace containing string will be parsed for the
		 * value.
		 */
		template<typename T>
		void getParameter(const std::string & parameter, T & value) const{
			const cmapiterator it = fParameters.find(parameter);
			if (it == fParameters.end()) throw(ParameterError("Parameter \""+parameter+"\" not found"));
			//Create a stream to read the parameter
			std::istringstream iss((*it).second);
			iss >> value;
			if (iss.fail() || iss.bad()) throw(ParameterError("Error reading parameter \""+parameter+"\""));
		}
		/** \brief Return a parameter value as a string given its name.
		 *
		 * \param parameter is the name of the parameter
		 * \param value is a reference to a string which will contain the
		 * parameter value on return.
		 *
		 * The unparsed value string is returned.
		 */
		void getParameter(const std::string & parameter, std::string & value) const {
			const cmapiterator it = fParameters.find(parameter);
			if (it == fParameters.end()) throw(ParameterError("Parameter \""+parameter+"\" not found"));
			value = (*it).second;
		}
		/** \brief Return a parameter value as a vector given its name.
		 *
		 * This is a templated function and the results depend on the type of the
		 * input parameter.
		 * \param parameter is the name of the parameter
		 * \param vec is a reference to a vector which will contain the
		 * parameter values on return.
		 *
		 * The basic input mechanism of c++ is used to parse the value string, so
		 * the value string will be parsed to type T for each white space seperated
		 * object in the value string.
		 */
		template<typename T>
		void getParameter(const std::string & parameter, std::vector<T> & vec) const{
			const cmapiterator it = fParameters.find(parameter);
			if (it == fParameters.end()) throw(ParameterError("Parameter \""+parameter+"\" not found"));
			//Clear the vector
			vec.clear();
			//Create a stream to read the parameter
			std::istringstream iss((*it).second);
			while (iss.good()){
				T value;
				iss >> value;
				if (iss.fail() || iss.bad()) throw(ParameterError("Error reading parameter \""+parameter+"\""));
				vec.push_back(value);
			}
		}

		/** \brief Set the value of a parameter given its name.
		 *
		 * This is a templated function and the results depend on the type of the
		 * input parameter.
		 * \param parameter is the name of the parameter
		 * \param value is a reference to a variable which holds the parameter
		 * value
		 *
		 * The basic output mechanism of c++ is used to convert the value to string.
		 */
		template<typename T>
		void setParameter(const std::string & parameter, const T & value){
			std::istringstream iss;
			iss << value;
			fParameters[parameter] = iss.str();
		}
		/** \brief Set the value of a parameter given its name.
		 *
		 * \param parameter is the name of the parameter
		 * \param value is a reference to a string which holds the parameter
		 * value
		 *
		 * The value of the parameter is set to that of the string.
		 */
		void setParameter(const std::string & parameter, const std::string & value){
			fParameters[parameter] = value;
		}
	friend std::ostream & operator << (std::ostream & os, const Pair &pair);
	friend std::istream & operator >> (std::istream & is, Pair &pair);
};

#endif
