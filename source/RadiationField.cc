#include <algorithm>
#include <cassert>
#include <fstream>
#include <iostream>

#include <PhysicalConstants.h>
#include <ErrorLogger.h>// AWS20100409 for galplot, reinstated

#include <RadiationField.h>

//#include <CLHEP/Vector/ThreeVector.h>

using namespace std;
using namespace rf;
using namespace utl;
//using namespace CLHEP;

RadiationField::RadiationField() : 
  fRebinnedSkymapOrder(1), 
  fNumberOfComponents(0), 
  fLuminosity(0), 
  fDustMass(0) {

}

RadiationField::RadiationField(const string& filename,
			       const valarray<double>& freq,
			       int rebinnedSkymapOrder) :
  fRebinnedSkymapOrder(rebinnedSkymapOrder),
  fNumberOfComponents(0),
  fLuminosity(0),
  fDustMass(0) {

  assert(!filename.empty());

  fFrequency.resize(freq.size());
  fFrequency = freq;
  
  ReadRadiationField(filename);

}

RadiationField::~RadiationField() {

  ClearData();

}

const Skymap<double>
RadiationField::GetSkymap(const ThreeVector& pos,
			  const STELLARCOMPONENT component,
			  const int healpixOrder) {

  assert (component >= TOTAL && component <= THERMAL);

  if (fRebinnedSkymapOrder == healpixOrder)
    return GetSkymap(pos, component);
  else {

    FlushSkymapCache(component);

    const int rebinnedSkymapOrder = fRebinnedSkymapOrder;

    fRebinnedSkymapOrder = healpixOrder;

    Skymap<double> skymap = GetSkymap(pos, component);

    FlushSkymapCache(component);

    fRebinnedSkymapOrder = rebinnedSkymapOrder;

    return skymap;

  }

}

const Skymap<double> 
RadiationField::GetSkymap(const ThreeVector& pos, 
			  const STELLARCOMPONENT component) {

  //cout << "Cache built: " << fCacheBuilt[component] << endl;

  if (!fCacheBuilt[component])
    BuildSkymapCache(component);

  //for (unsigned long i = 0; i < fXData.size(); ++i)
    //cout << i << " " << fXData[i] << endl;

  //for (unsigned long i = 0; i < fZData.size(); ++i)
  //cout << i << " " << fZData[i] << endl;

  //cout << pos << " " << -pos << endl;

  if (fabs(pos.x()) > fXData[fXData.size()-1] || fabs(pos.z()) > fZData[fZData.size()-1]) {

    const Skymap<double>& skymap = *fSkymapOrderedData[0][0][component];

    return ((Skymap<double>() = skymap) = 0); 

  }

  // Bilinear interpolation to form skymap

  const double* xVal = std::lower_bound(&fXData[0], &fXData[fXData.size()-1], fabs(pos.x()));

  const unsigned long xIndex = xVal - &fXData[0];

  const double* zVal = std::lower_bound(&fZData[0], &fZData[fZData.size()-1], fabs(pos.z()));

  const unsigned long zIndex = zVal - &fZData[0];

  double xCoeff = 0;

  if (fabs(pos.x()) <= (fXData[xIndex] + fXRangeData[xIndex]) &&
      fabs(pos.x()) >= (fXData[xIndex] - fXRangeData[xIndex]))  
    xCoeff = 1;
  else if (fabs(pos.x()) < fXData[fXData.size()-1]) {
    
    const double dX = fXData[xIndex] - fXData[xIndex-1];
    
    xCoeff = (fXData[xIndex] - fabs(pos.x()))/dX;
    
  }
  
  double zCoeff = 0;
  
  if (fabs(pos.z()) <= (fZData[zIndex] + fZRangeData[zIndex]) &&
      fabs(pos.z()) >= (fZData[zIndex] - fZRangeData[zIndex]))  
    zCoeff = 1;
  else if (fabs(pos.z()) < fZData[fZData.size()-1]) {
    
    const double dZ = fZData[zIndex] - fZData[zIndex-1];
    
    zCoeff = (fZData[zIndex] - fabs(pos.z()))/dZ;
    
  }

  //cout << xIndex << " " << zIndex << " " << xCoeff << " " << zCoeff << endl;

  //cout << xIndex << " " << zIndex << " " << *xVal << " " << *zVal << " " << (xVal - &fXData[0]) << " " << (zVal - &fZData[0]) << " " << xCoeff << " " << zCoeff << endl;

  // Form the bilinear interpolated skymap -- this is nasty and not optimised 
  // but I need to get it to work first ...

  Skymap<double> skymap;
  
  if (xCoeff >= 1. && zCoeff >= 1.) {

    if (pos.z() >= 0.)
      return *fSkymapOrderedData[xIndex][zIndex][component];
    else {

      const Skymap<double>& skymap1 = *fSkymapOrderedData[xIndex][zIndex][component];

      Skymap<double> skymap1Mirror = skymap1;

      for (unsigned long i = 0; i < skymap1.Npix(); ++i) {

	SM::Coordinate coord = skymap1.pix2coord(i);

	SM::Coordinate mirrorCoord(coord.l(), -coord.b());

	skymap1Mirror[mirrorCoord] = skymap1[coord];

      }

      return skymap1Mirror;

    }

  } else if (xCoeff >= 1.) {

    const Skymap<double>& skymap1 = *fSkymapOrderedData[xIndex][zIndex-1][component];
    
    const Skymap<double>& skymap2 = *fSkymapOrderedData[xIndex][zIndex][component];

    if (pos.z() >= 0) 
      skymap = skymap1*zCoeff + skymap2*(1. - zCoeff);
    else {

      Skymap<double> skymap1Mirror = skymap1;
      Skymap<double> skymap2Mirror = skymap2;

      for (unsigned long i = 0; i < skymap1.Npix(); ++i) {

	SM::Coordinate coord = skymap1.pix2coord(i);

	SM::Coordinate mirrorCoord(coord.l(), -coord.b());

	skymap1Mirror[mirrorCoord] = skymap1[coord];

      }

      for (unsigned long i = 0; i < skymap2.Npix(); ++i) {

	SM::Coordinate coord = skymap2.pix2coord(i);

	SM::Coordinate mirrorCoord(coord.l(), -coord.b());

	skymap2Mirror[mirrorCoord] = skymap2[coord];

      }

      skymap = skymap1Mirror*zCoeff + skymap2Mirror*(1. - zCoeff); 

    }

  } else if (zCoeff >= 1.) {

    const Skymap<double>& skymap1 = *fSkymapOrderedData[xIndex-1][zIndex][component];

    const Skymap<double>& skymap2 = *fSkymapOrderedData[xIndex][zIndex][component];

    if (pos.z() >= 0.)
      skymap = skymap1*xCoeff + skymap2*(1. - xCoeff);
    else {

      Skymap<double> skymap1Mirror = skymap1;
      Skymap<double> skymap2Mirror = skymap2;

      for (unsigned long i = 0; i < skymap1.Npix(); ++i) {

	SM::Coordinate coord = skymap1.pix2coord(i);

	SM::Coordinate mirrorCoord(coord.l(), -coord.b());

	skymap1Mirror[mirrorCoord] = skymap1[coord];

      }

      for (unsigned long i = 0; i < skymap2.Npix(); ++i) {

	SM::Coordinate coord = skymap2.pix2coord(i);

	SM::Coordinate mirrorCoord(coord.l(), -coord.b());

	skymap2Mirror[mirrorCoord] = skymap2[coord];

      }

      skymap = skymap1Mirror*xCoeff + skymap2Mirror*(1. - xCoeff);

    }

  } else {

    const Skymap<double>& skymap1 = *fSkymapOrderedData[xIndex-1][zIndex-1][component];

    const Skymap<double>& skymap2 = *fSkymapOrderedData[xIndex-1][zIndex][component];

    const Skymap<double>& skymap3 = *fSkymapOrderedData[xIndex][zIndex-1][component];

    const Skymap<double>& skymap4 = *fSkymapOrderedData[xIndex][zIndex][component];

    if (pos.z() >= 0.) 
      skymap = skymap1*xCoeff*zCoeff + skymap2*(1. - zCoeff)*xCoeff + skymap3*zCoeff*(1. - xCoeff) + skymap4*(1. - xCoeff)*(1. - zCoeff);
    else {

      Skymap<double> skymap1Mirror = skymap1;
      Skymap<double> skymap2Mirror = skymap2;
      Skymap<double> skymap3Mirror = skymap3;
      Skymap<double> skymap4Mirror = skymap4;

      for (unsigned long i = 0; i < skymap1.Npix(); ++i) {

	SM::Coordinate coord = skymap1.pix2coord(i);

	SM::Coordinate mirrorCoord(coord.l(), -coord.b());

	skymap1Mirror[mirrorCoord] = skymap1[coord];

      }

      for (unsigned long i = 0; i < skymap2.Npix(); ++i) {

	SM::Coordinate coord = skymap2.pix2coord(i);

	SM::Coordinate mirrorCoord(coord.l(), -coord.b());

	skymap2Mirror[mirrorCoord] = skymap2[coord];

      }

      for (unsigned long i = 0; i < skymap3.Npix(); ++i) {

	SM::Coordinate coord = skymap3.pix2coord(i);

	SM::Coordinate mirrorCoord(coord.l(), -coord.b());

	skymap3Mirror[mirrorCoord] = skymap3[coord];

      }

      for (unsigned long i = 0; i < skymap4.Npix(); ++i) {

	SM::Coordinate coord = skymap4.pix2coord(i);

	SM::Coordinate mirrorCoord(coord.l(), -coord.b());

	skymap4Mirror[mirrorCoord] = skymap4[coord];

      }
      
      skymap = skymap1Mirror*xCoeff*zCoeff + skymap2Mirror*(1. - zCoeff)*xCoeff + skymap3Mirror*zCoeff*(1. - xCoeff) + skymap4Mirror*(1. - xCoeff)*(1. - zCoeff); 

    }

  }

  return skymap;

}

void RadiationField::ReadRadiationField(const string& filename) {

  ostringstream buf;
  buf << "Reading from " << filename;
  INFO(buf.str()); //AWS20100409 for galplot reinstated

  ClearData();
  
  const string prefix = filename.substr(0, filename.find_last_of("/")+1);

  //cout << prefix << endl;

  fPrefix = prefix;

  ifstream rfFile(filename.c_str());

  unsigned long volumeElements, wlBins, numFilters;
  double luminosity, dustMass;

  rfFile >> volumeElements >> wlBins >> numFilters;
  rfFile >> luminosity >> dustMass;

  fLuminosity = luminosity;
  fDustMass = dustMass;

  fWavelength.resize(wlBins);
  fWavelength = 0;

  unsigned long stellarComponents;

  rfFile >> stellarComponents;

  fStellarComponentLuminosity.resize(stellarComponents);

  for (unsigned long i = 0; i < stellarComponents; ++i) {

    double componentLuminosity;
    string componentName;

    rfFile >> componentName >> componentLuminosity;

    fStellarComponentLuminosity[i] = componentLuminosity;
    fStellarComponentName.push_back(componentName);

  }

  unsigned long geometry;
  double modelRegionData[6];

  rfFile >> geometry;
  rfFile >> modelRegionData[0] >> modelRegionData[1] >> modelRegionData[2] >> modelRegionData[3] >> modelRegionData[4] >> modelRegionData[5];

  vector<ThreeVector> posVec, rangeVec;

  for (unsigned long i = 0; i < volumeElements; ++i) {

    double index;
    double x, y, z, dX, dY, dZ;

    rfFile >> index;
    rfFile >> x >> y >> z;
    rfFile >> dX >> dY >> dZ;

    ThreeVector pos(x, y, z), range(dX, dY, dZ);

    //cout << pos << endl;

    if (!posVec.size()) {
      
      posVec.push_back(pos);
      rangeVec.push_back(range);

    } else if (fabs(posVec[0].x() - pos.x()) < dX) {
      
      posVec.push_back(pos);
      rangeVec.push_back(range);

    }

    if (fabs(posVec[0].x() - pos.x()) > dX || i == volumeElements - 1) {

      fPositionData.push_back(posVec);
      fRangeData.push_back(rangeVec);

      posVec.clear();
      posVec.push_back(pos);

      rangeVec.clear();
      rangeVec.push_back(range);

    }

    string filterFilename, filterCountFilename, directFilename, scatteredFilename, transientFilename, thermalFilename, totalFilename, filterFluxFilename, fluxFilename;

    rfFile >> filterFilename;
    rfFile >> filterCountFilename;
    rfFile >> directFilename;
    rfFile >> scatteredFilename;
    rfFile >> transientFilename;
    rfFile >> thermalFilename;
    rfFile >> totalFilename;
    rfFile >> filterFluxFilename;
    rfFile >> fluxFilename;

    fNumberOfComponents = 7;

    fCacheBuilt.resize(fNumberOfComponents);
    fCacheBuilt = false;

    fFilenameData[pos].resize(fNumberOfComponents);

    fFilenameData[pos][0] = totalFilename;
    fFilenameData[pos][1] = directFilename;
    fFilenameData[pos][2] = scatteredFilename;
    fFilenameData[pos][3] = transientFilename;
    fFilenameData[pos][4] = thermalFilename;
    fFilenameData[pos][5] = filterFilename;
    fFilenameData[pos][6] = filterCountFilename;

    //Skymap<double> totalSkymap(prefix + totalFilename);

    //newRF.fTotalFlux.push_back(totalSkymap);

    //newRF.fWavelength = newRF.fTotalFlux.back().getSpectra();

    /*    for (unsigned long j = 0; j < numFilters; ++j) {

      unsigned long nF;
      string filter;
      double centralWl, val;

      rfFile >> nF >> filter >> centralWl >> val;

    }

    for (unsigned long j = 0; j < wlBins; ++j) {

      double wl;
      unsigned long count;
      double total, direct, scattered, transient, thermal;

      rfFile >> wl >> count >> total >> direct >> scattered >> transient >> thermal;

    }
    */

    //cout << i << " " << index << " " << x << " " << y << " " << z << " " << totalFilename << endl;

  }

  for (unsigned long i = 0; i < fPositionData.size(); ++i) {

    vector< vector<string>* > pStrData;

    vector< vector<Skymap<double>* > > pSkymapData;

    for (unsigned long j = 0; j < fPositionData[i].size(); ++j) {

      const ThreeVector& pos = fPositionData[i][j];

      map<ThreeVector, vector<string> >::iterator fIt = fFilenameData.find(pos);

      pStrData.push_back(&fIt->second);

      vector<Skymap<double>* > skymaps;
      skymaps.resize(fIt->second.size());

      pSkymapData.push_back(skymaps);

    }

    fFilenameOrderedData.push_back(pStrData);
    fSkymapOrderedData.push_back(pSkymapData);

  }

  fXData.resize(fPositionData.size());
  fYData.resize(fPositionData.size());

  for (unsigned long i = 0; i < fPositionData.size(); ++i) {

    fXData[i] = fPositionData[i][0].x();
    fYData[i] = fPositionData[i][0].y();

  }

  fZData.resize(fPositionData[0].size());

  for (unsigned long i = 0; i < fPositionData[0].size(); ++i)
    fZData[i] = fPositionData[0][i].z();

  fXRangeData.resize(fRangeData.size());
  fYRangeData.resize(fRangeData.size());

  for (unsigned long i = 0; i < fRangeData.size(); ++i) {

    fXRangeData[i] = fRangeData[i][0].x();
    fYRangeData[i] = fRangeData[i][0].y();

  }

  fZRangeData.resize(fRangeData[0].size());

  for (unsigned long i = 0; i < fRangeData[0].size(); ++i)
    fZRangeData[i] = fRangeData[0][i].z();

  /*for (unsigned long i = 0; i < fPositionData.size(); ++i) {

    for (unsigned long j = 0; j < fPositionData[i].size(); ++j) 
      cout << fPositionData[i][j] << " ";

    cout << endl;

    for (unsigned long j = 0; j < fValidRangeData[i].size(); ++j) 
      cout << fValidRangeData[i][j] << " ";

    for (unsigned long j = 0; j < fPositionData[i].size(); ++j)
      cout << (*fFilenameOrderedData[i][j])[0] << " ";

    cout << endl;

  }
  */
}

void RadiationField::BuildSkymapCache(const unsigned long component) {

  assert(component < fNumberOfComponents);
  
  valarray<double> energy(fFrequency.size());
  
  energy = kPlanck_SI/e_SI*fFrequency;
  
  for (unsigned long i = 0; i < fSkymapOrderedData.size(); ++i) {
    
    for (unsigned long j = 0; j < fSkymapOrderedData[i].size(); ++j) {
      
      const string& filename = (*fFilenameOrderedData[i][j])[component];
      
      //cout << filename << endl;
      
      Skymap<double> skymap(fPrefix + filename);
      
      const valarray<double>& wl = skymap.getSpectra();
      
      valarray<double> freq(0., wl.size()), en(0., wl.size());
      
      for (unsigned int iWl = 0; iWl < wl.size(); ++iWl)
	freq[iWl] = kSpeedOfLight_SI*1./(wl[wl.size()-1-iWl]*micron/m);
      
      en = kPlanck_SI/e_SI*freq;

      Skymap<double> skymapRebinned(fRebinnedSkymapOrder, freq);
      
      skymapRebinned = skymap.rebin(fRebinnedSkymapOrder);
      
      fSkymapOrderedData[i][j][component] = new Skymap<double>(fRebinnedSkymapOrder, fFrequency);	
      
      for (unsigned int nPix = 0; nPix < skymapRebinned.Npix(); ++nPix) {
	
	valarray<double> spec(0., fFrequency.size()), binCount(0., fFrequency.size());
	
	for (unsigned int iFreq = 0; iFreq < freq.size(); ++iFreq) {
	  
	  int index = (log10(freq[iFreq]) - log10(fFrequency[0]))/(log10(fFrequency[fFrequency.size()-1]) - log10(fFrequency[0]))*fFrequency.size();
	  
	  if (index >= 0 && index < fFrequency.size()) {
	    
	    const double specVal = skymapRebinned[nPix][freq.size()-1-iFreq];//energyRaw[iFreq]/energyRaw[iFreq];
	    
	    spec[index] += specVal;
	    binCount[index] += 1.;
	    
	  }
	  
	}
	
	spec *= 1./binCount*1./energy*1./energy*1./(kSpeedOfLight_SI*m/cm);
	
	(*fSkymapOrderedData[i][j][component])[nPix] = spec; // eV^-1 cm^-3 -- per pixel
	
      }
       
      /*for (size_t iFreq = 0; iFreq < freq.size(); ++iFreq) {
	
	size_t index = (log10(freq[iFreq]) - log10(fFrequency[0]))/(log10(fFrequency[fFrequency.size()-1]) - log10(fFrequency[0]))*fFrequency.size();

	cout << iFreq << " " 
	     << freq[iFreq] << " " 
	     << en[iFreq] << " " 
	     << index << " " 
	     << (index >= 0 && index < fFrequency.size() ? fFrequency[index] : 0) << " "
	     << (index >= 0 && index < fFrequency.size() ? energy[index] : 0) << " " 
	     << skymap.sum(freq.size()-1-iFreq)*1./(kSpeedOfLight_SI*m/cm) << " " 
	     << skymapRebinned.sum(freq.size()-1-iFreq)*1./(kSpeedOfLight_SI*m/cm) << " "
	     << (index >= 0 && index < fFrequency.size() ? fSkymapOrderedData[i][j][component]->sum(index)*energy[index]*energy[index] : 0) << " "
	     << fFrequency.size() << endl;

      }

      exit(0);
      */
    }     
   
  }

  fCacheBuilt[component] = true;

}
  
void RadiationField::FlushSkymapCache(const unsigned long component) {

  assert(component < fNumberOfComponents);
  
  for (unsigned long i = 0; i < fSkymapOrderedData.size(); ++i) {
    
    for (unsigned long j = 0; j < fSkymapOrderedData[i].size(); ++j) {
      
      delete fSkymapOrderedData[i][j][component];
      fSkymapOrderedData[i][j][component] = 0;
      
    }
    
  }

  fCacheBuilt[component] = false;
  
}

void RadiationField::FlushSkymapCache() {

  for (unsigned long i = 0; i < fNumberOfComponents; ++i)
    FlushSkymapCache(i);
  
}

void RadiationField::ClearData() {

  for (unsigned long i = 0; i < fPositionData.size(); ++i) {
    
    fPositionData[i].clear();
    fRangeData[i].clear();
    
  }
  
  for (unsigned long i = 0; i < fFilenameOrderedData.size(); ++i)
    fFilenameOrderedData[i].clear(); // Don't own the pointers
  
  fFilenameOrderedData.clear();
    
  for (unsigned long i = 0; i < fSkymapOrderedData.size(); ++i) {
    
    for (unsigned long j = 0; j < fSkymapOrderedData[i].size(); ++j) {
      
      for (unsigned long k = 0; k < fSkymapOrderedData[i][j].size(); ++k) 
	delete fSkymapOrderedData[i][j][k];
      
      fSkymapOrderedData[i][j].clear();
      
    }
    
    fSkymapOrderedData[i].clear();
    
  }
  
  fSkymapOrderedData.clear();
  
}
