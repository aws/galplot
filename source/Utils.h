#ifndef Utils_h
#define Utils_h

#include <valarray>
#include <vector>

/** \brief Several utilities common to many classes are stored in this namespace */
namespace Utils{
	/** \brief Assuming array is sorted ascendantly, find the index where the array value is just below the searched for value.
	 *
	 * \param array the ascendantly sorted array
	 * \param value the value to search for
	 *
	 * \note Returns the lowest index in the array if value is lower than all values in the array
	 */
	int indexBelow(const std::valarray<double> &array, double value);
	/** \brief Assuming array is sorted ascendantly, find the index where the array value is just above the searched for value.
	 *
	 * \param array the ascendantly sorted array
	 * \param value the value to search for
	 * 
	 * \note Returns the highest index in the array if value is higher than all values in the array
	 */
	int indexAbove(const std::valarray<double> &array, double value);
	/** \brief Find the index range which brackets the range from lowValue to highValue
	 *
	 * \param array the ascendantly sorted array to search
	 * \param lowValue the lower limit of the range to bracket
	 * \param highValue the upper limit of the range to bracket
	 * \param lowIndex a reference to a integer, which will contain the lower index in the array
	 * \param highIndex a reference to a integer, which will contain the higher index in the array
	 *
	 * If the range is outside the array span, the index will be in the end closer to the range.  The
	 * index will only be the same if the array size is less than 2.
	 */
	void findIndexRange(const std::valarray<double> &array, double lowValue, double highValue, int &lowIndex, int &highIndex);
	/** \overload */
	void findIndexRange(const std::vector<double> &array, double lowValue, double highValue, int &lowIndex, int &highIndex);
	/** Linear interpolation of values 
	 *
	 * \param x the value at which to evaluate the interpolation
	 * \param xValues the vector of x values in the data to interpolate
	 * \param yValues the vector of y values in the data to interpolate
	 *
	 * This method extrapolates silently.
	 */
	double linearInterpolation(double x, const std::vector<double> &xValues, const std::vector<double> &yValues);
	/** \brief Calculate the 3rd order Lagrange Coefficients, given x and y values.
	 *
	 * \param x the x values to use
	 * \param y the y values to use
	 * \param coeff on return, stores the calculated coefficients.  The coefficients are stored in order, so the total
	 * function will be \f$ f = \sum^i coeff_i x^i \f$
	 */
	void lagrangeCoeff4(const std::valarray<double> & x,const std::valarray<double> & y, std::valarray<double> & coeff);

	/** \brief Nicely formed status indicator, showing percent completed in both numbers and as a bar.
	 *
	 * This class is OMP thread safe.
	 */
	class StatusIndicator {
		public:
			/** \brief Construct a status indicator with a string and number of steps
			 *
			 * \param marker is the string to print in front of the status indicator
			 * \param steps the total number of steps expected
			 */
			StatusIndicator(const std::string & marker, int steps);
			/** \brief Increase the number of finished steps by one and reprint. */
			void refresh();
		private:
			std::string fmarker; //!< Store the marker
			int fsteps, fcurrentStep; //!< Number of steps and current step.
			long ftInit; //!< Initial time when counter started, used for time estimates
			double ftEstimate; //!< Estimated time needed to complete the project
	};
}

#endif
