#ifndef SOURCES_H
#define SOURCES_H

#include <string>
#include <valarray>
#include <vector>
#include <gsl/gsl_vector.h>
#include "Skymap.h"
#include "Psf.h"
#include "Exposure.h"

//The value for E0 in the definition of the source spectra
#define E0 100.0

/** \brief Store relevant information from the source catalog.
 *
 * Has the ability to generate counts map from the source catalog, given an Exposure and a Psf.
 */
class Sources {
	public:
		/** \brief Default constructor, does nothing */
		Sources() {}
		/** \brief Construct the object from a catalog in a fits file
		 *
		 * \param fileName is the file name of the fits file containing the catalog.
		 *
		 * The file should contain a table in the first extension with at least the
		 * following columns (formats)
		 *  - SOURCE_NAME (string) the name of the source
		 *  - L (float) the galactic longitude of the source in degrees
		 *  - B (float) the galactic latitude of the source in degrees
		 *  - FLUX100 (float) integrated flux above 100 MeV.  Used for sorting on
		 *  brightness
		 *  - PREFACTOR (float) the power law prefactor associated with the source spectra
		 *  - SPECTRAL_INDEX (float) the power law index of the source spectra.  The
		 *  spectra is assumed to have a pivot point around 100 MeV
		 *
		 * Using the following columns will also work and is
		 * automatically selected
		 *  - NickName (string) the name of the source
		 *  - GLON (float) galactic longitude in degrees
		 *  - GLAT (float) galactic latitude in degrees
		 *  - Flux100 (float) integrated flux above 1000 MeV
		 *  - Flux_Density (float) the prefactor for the power law
		 *  - Spectral_Index (float) the index for the power law
		 *  - Pivot_Energy (float) the pivot energy for the power law
		 *  (F = F_0 (E/E_p)^g
		 *
		 * Additionally, these columns will be read if they exist
		 *  - SP_TYPE (string) the name of the spectral type.  Available types are
		 *    - POWERLAW
		 *    - POWERLAWEXPCUTOFF
		 *    - BROKENPOWERLAW
		 *    - FREE
		 *  - SP_PARS (float array) the parameters for the spectra
		 *    - POWERLAW: prefactor, index (100 MeV pivot point)
		 *    - POWERLAWEXPCUTOFF: prefactor, index, cutoff energy (100 MeV pivot point)
		 *    - BROKENPOWERLAW: prefactor, index1, index2, break energy
		 *    - FREE: energy, scaler pairs, where the scaler is the deviation from
		 *    the power law given in the columns before.  Power law interpolation
		 *    is used if the energy ranges are not compatible.
		 *  - SP_PARS_UNC (float array) the uncertainty of the spectral
		 *  parameters.  Has the same structure as SP_PARS.  For the
		 *  free floating spectra, gives the half-width of the energy
		 *  bin used in the fit.
		 *  - FIT_TYPE (string) how to fit the source in SourceModel2.  \see
		 *  FitType structure for available values.
		 *
		 * The SP_TYPE, SP_PARS, and SP_PARS_UNC columns are ment to be used as output from fits
		 * performed with SourceModel2, but are of course read in in subsequent
		 * calls and can be used manually.
		 *
		 * Also, if the spectral type is not defined in the catalog,
		 * the routine tries to read the following columns
		 * - Flux100_300
		 * - Flux300_1000
		 * - Flux1000_3000
		 * - Flux3000_10000
		 * - Flux10000_100000
		 *
		 * which indicate the integrated flux for those bins.
		 * Subsequently, the spectral type is set to free and these
		 * values used for the spectra.  It also reads the accompaning
		 * uncertainty columns.
		 */
		Sources(const std::string &fileName);
		/** \brief Enum values for available spectral shapes.
		 *
		 * This defines both the shape of the spectra and how we wan't the source to be
		 * fit.  The information is read from the source catalog.  The catalog is
		 * assumed to contain a index and a prefactor for a power law spectral shape,
		 * which is used as a initial value for the fit.  The exponential cut off is
		 * put at 10 GeV as a default and the break energy in the broken power law likewise.
		 *
		 * Currently, the broken power law is not functioning.
		 */
		enum FitType {
		   FIXED, //!< Use the values specified in the catalog
		   PREFACTOR, //!< Fit only the prefactor of the simple power law
		   INDEX, //!< Fit only the index of the simple power law
		   POWERLAW, //!< Simple power law with a prefactor and an index
		   POWERLAWEXPCUTOFF, //!< Simple power law with a high energy cutoff
		   BROKENPOWERLAW, //!< Broken power law, with two indices, a prefactor and a break energy.  The prefactor is the spectral value at the break energy.
		   FREE, //!< One parameter per energy bin
		   NONE //!< Do not include the source
		};

		//! Convert FitType to string.
		static std::string fitTypeToStr(const FitType & type);

		/** Convert strings to FitType.
		 *
		 * Converts strings containing the enum names for FitType into enum
		 * objects.  This routine is case insensitive.
		 */
		static FitType strToFitType(const std::string & type);

		/** \brief Structure to store the source information */
		struct Source {
		   std::string name; //!< Name of the source
		   double l, b, flux100, prefactor, index, pivot;
	           double       flux1000; //AWS20110806
		   double       flux10000;//AWS20120530
		   double ra,dec;         //AWS20120612

		   FitType fitType; //!< How we are supposed to fit
		   FitType spType; //!< What kind of spectra we have from the extended catalog
		   /** \brief The spectral parameters
		    *
		    * The size of this array depends on the spectral Type
		    *  - \e POWERLAW: prefactor, index
		    *  - \e POWERLAWEXPCUTOFF: prefactor, index, cutoff energy
		    *  - \e BROKENPOWERLAW: prefactor, index1, index2, break energy
		    *  - \e FREE: energy, prefactor pairs
		    *
		    * In the case of a free spectra, the prefactor is a correction for the power law given in the catalog for the given energy, which is sqrt(Emin*Emax)
		    */
		   std::valarray<double> spParameters;
		   /** \brief Uncertainty of the spectral parameters
		    *
		    * This is the uncertainty for the parameters given in spParameters.
		    * It has the same shape as the spParameters array.
		    * For the free fit, the uncertainty in energy is half the bin width.
		    */
		   std::valarray<double> spParametersUnc;
		};

		/** \brief Read a new catalog from file
		 *
		 * \see the constructor for details on the catalog format
		 */
		void read(const std::string &fileName);
		/** \brief Write the current catalog to file, including all spectral
		 * types.
		 */
		void write(const std::string &fileName);
		/** \brief Generate the spectra for a given source
		 *
		 * \param sourceNumber is the source for which the spectra is
		 * to be generated
		 * \param energies is an array containing the energies at which
		 * the spectra is to be evaluated.
		 *
		 * \return the spectra in units of cm^-2 s^-1 MeV^-1 at the
		 * required energies.
		 */
		std::valarray<double> genSpectra(int sourceNumber, const std::valarray<double> &energies) const;
		/** \brief Add fluxes of sources brighter than a limit to a skymap
		 *
		 * \param lowFlux is the minimum flux >100MeV a source has to have to be included.  It is given in photon/cm^2/s
		 * \param fluxMap is the skymap to add the sources to in
		 * units of photons/cm^2/MeV/s
		 * 
		 * Does not take the point spread function into account.
		 */
		void genFluxMap(double lowFlux, Skymap<double> &fluxMap) const;
		/** \brief Add fluxes of numberOfSources brightest
		 * sources to a skymap
		 *
		 * \param numberOfSources is the number of sources to include
		 * \param fluxMap is the skymap to add the sources to in
		 * units of photons/cm^2/MeV/s
		 * 
		 * Does not take the point spread function into account.
		 */
		void genFluxMap(int numberOfSources, Skymap<double> &fluxMap) const;
		/** \brief Return a filter map for the sources.
		 *
		 * Uses the fraction of the psf as the region to filter.
		 */
		void genFilterMaps(double lowFlux, Skymap<char> &filter, double fraction, const Psf &psf) const;
		/** overload */
		void genFilterMaps(int numberOfSources, Skymap<char> &filter, double fraction, const Psf &psf) const;
		/** overload */
		void genFilterMaps(const std::valarray<int> &sourceNumbers, Skymap<char> &filter, double fraction, const Psf &psf) const;
		/** overload */
		void genFilterMap(int sourceNumber, Skymap<char> &filter, double fraction, const Psf &psf) const;
		/** \brief Return a constant reference to a source */
		const Source & operator [] (int i) const { return fSources[i]; }
		/** \brief Return a reference to a source */
		Source & operator [] (int i) { return fSources[i]; }
		//! The number of sources
		int size() const { return fSources.size(); }
		//! Given a name, find the source number
		int sourceNumber(const std::string & name) const;
		//! Assignment operator
		Sources & operator = (const Sources &);
		/** \brief Function to convert fluxes in energy bins into
		 * intensities at the geometrical mean energy of the bin
		 *
		 * \parameter eMin, eMax are the boundaries of the bins, must
		 * be in ascending order, but not necessarily adjacent
		 * \parameter flux are the flux values of the bins
		 * \return the intensities at the geometrical
		 * mean energy of the bins
		 */
		static std::valarray<double> flux2int(const std::valarray<double> &eMin, const std::valarray<double> &eMax, const std::valarray<double> &flux);

                int add_source (Source source); //AWS20110829

	private:
		struct fparams {
		   std::valarray<double> eMin, eMax, flux;
		};
		static int minFunc(const gsl_vector *x, void *params, gsl_vector *f);
		/** \brief Sort the sources on their flux > 100MeV value */
		void sort();
		/** \brief Read the source catalog from fits file */
		void readFromFile(const std::string & fileName);
		std::vector<Source> fSources; //!< Store the sources
		std::map<std::string, int> fnameIndex; //!< Store the name, index in a map for fast name lookup
		bool foldFormat; //!< We must know what format we are using
		/** \brief Helper class to compare the sources.  Needed in the sort method */
		class SourceCompare {
			public:
				bool operator () (const Source &a, const Source &b);
		};
};

#endif
