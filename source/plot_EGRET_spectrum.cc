
//**.****|****.****|****.****|****.****|****.****|****.****|****.****|****.****|
// * plot_EGRET_spectrum.cc *                                galprop package * 5/22/2000 
//**"****!****"****!****"****!****"****!****"****!****"****!****"****!****"****|
#include"Galplot.h"                    //AWS20050920
#include"galprop_classes.h"
#include"galprop.h"
#include"constants.h"
#include"fitsio.h"

/*
void   differential_spectrum(double e1,double e2, double intensity, double g,double &intensity_esq1,double &intensity_esq2);
void   differential_spectrum(double *e,    int n, double *intensity,         double *intensity_esq1,double *intensity_esq2,
                             double *Ebar,double *Ibar);
*/

//void Galplot::deconvolve_intensity(TH1D *intensity);

int Galplot::plot_EGRET_spectrum(int mode)
{
   cout<<" >>>> plot_EGRET_spectrum    "<<endl;
   int status=0;


int i_comp,i_lat,i_long,i_E_EGRET;
int ii_long,ii_lat;
int il_min,il_max,ib_min,ib_max;
int il_min1,il_max1,ib_min1,ib_max1;
int il_min2,il_max2,ib_min2,ib_max2;
double l,b,l_off;
double exposure, intensity,*intensity_,*intensity_esq1,*intensity_esq2;
double           counts,      *counts_,stat_err;                             //AWS20060322
double *Ebar,*Ibar;
double long_min,lat_min;
double d_long, d_lat;
double *g;
double segment[2],Ebar_segment[2];

char name[100],canvastitle[100], workstring1[100],workstring2[100];
int errorbar; double errorbarfactor;
double delta_err;//AWS20040312

TCanvas *c1;
TH3F    *egret_map;
TH1D    *profile;  
TH1D *profile11,*profile12,*profile21,*profile22; // four (l,b) intervals
TGraph  *spectrum;

TH3F    *counts_map;                                                              //AWS20060322
TH1D    *counts_profile;                                                          //AWS20060322
TH1D    *counts_profile11,*counts_profile12,*counts_profile21,*counts_profile22;  //AWS20060322

int ncolors=1; int *colors=0;
gStyle->SetPalette(ncolors,colors);


 cout<<data.EGRET_counts.n_rgrid<<" "<<data.EGRET_counts.n_zgrid<<endl;

  
 
 
 g=new double[data.n_E_EGRET];
 g[0]=1.5;
 g[1]=1.5;
 g[2]=1.5;
 g[3]=1.5;
 g[4]=1.6;
 g[5]=1.7;
 g[6]=1.9;
 g[7]=2.0;
 g[8]=2.5;
 g[9]=2.7;

  // EGRET map is 2D Distribution, hence coords are formally r, z

  egret_map=new TH3F(canvastitle,canvastitle,
                     data.EGRET_counts.n_rgrid,       -180,           180.,
		     data.EGRET_counts.n_zgrid,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1),
                     data.EGRET_counts.n_pgrid,data.E_EGRET[0],data.E_EGRET[data.n_E_EGRET]);



  counts_map=new TH3F(canvastitle,canvastitle,                                              //AWS20060322
                     data.EGRET_counts.n_rgrid,       -180,           180.,
		     data.EGRET_counts.n_zgrid,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1),
                     data.EGRET_counts.n_pgrid,data.E_EGRET[0],data.E_EGRET[data.n_E_EGRET]);


  long_min= -179.75;//specific for EGRET data counts.gp1234_30.g001
   lat_min  =-89.75;
  d_long      =0.5;     
  d_lat       =0.5;

        for(i_E_EGRET=0; i_E_EGRET<data.n_E_EGRET                    ; i_E_EGRET++)
        {
	 for (i_lat =0;  i_lat <data.EGRET_counts.n_zgrid            ; i_lat++    )
         {
          for(i_long=0;  i_long<data.EGRET_counts.n_rgrid            ; i_long++   )
          {

            l=       long_min+ d_long   *i_long;
            b=        lat_min+ d_lat    *i_lat;

            exposure =data.EGRET_exposure.d2[i_long][i_lat].s[i_E_EGRET];
            intensity=0.;
            if(exposure>0.)
            intensity=data.EGRET_counts.  d2[i_long][i_lat].s[i_E_EGRET]/exposure;


            intensity*=sabin(lat_min,d_long,d_lat,i_lat); // multiply by solid angle of bin


            counts = data.EGRET_counts.  d2[i_long][i_lat].s[i_E_EGRET];//AWS20060322

            //cout<<i_long<<" "<<i_lat<<" "<<exposure<<" "<<intensity<<endl;
            l_off=l-180.;
            if(l_off<0.)l_off+=360.0;
            ii_long=  data.EGRET_counts.n_rgrid-l_off/d_long;
	    //cout<<"l l_off ii_long"<<l<<" "<<l_off<<" "<<ii_long<<endl;

               egret_map    ->SetBinContent(ii_long,i_lat,i_E_EGRET,intensity);   
  
              counts_map    ->SetBinContent(ii_long,i_lat,i_E_EGRET,counts   ); //AWS20060322

            }// i_long
	   }// i_lat
          }// i_E_EGRET

 


  // NB reversed longitude axis starting at +180 and decreasing

  l_off=180.-galplotdef.long_max1; if(l_off>360.)l_off-=360.; if(l_off<  0.)l_off+=360.;
  il_min1=l_off/d_long;
  l_off=180.-galplotdef.long_min1; if(l_off>360.)l_off-=360.; if(l_off<  0.)l_off+=360.;
  il_max1=l_off/d_long;
  cout<<"il_min1 ilmax1:"<<il_min1<<" "<<il_max1<<endl;

  l_off=180.-galplotdef.long_max2; if(l_off>360.)l_off-=360.; if(l_off<  0.)l_off+=360.;
  il_min2=l_off/d_long;
  l_off=180.-galplotdef.long_min2; if(l_off>360.)l_off-=360.; if(l_off<  0.)l_off+=360.;
  il_max2=l_off/d_long;
  cout<<"il_min2 ilmax2:"<<il_min2<<" "<<il_max2<<endl;




  ib_min1=(galplotdef.lat_min1-lat_min+.0001)/d_lat;//AWS20030911
  ib_max1=(galplotdef.lat_max1-lat_min+.0001)/d_lat;//AWS20030911

  cout<<"ib_min1 ibmax1:"<<ib_min1<<" "<<ib_max1<<endl;

  ib_min2=(galplotdef.lat_min2-lat_min+.0001)/d_lat;//AWS20030911
  ib_max2=(galplotdef.lat_max2-lat_min+.0001)/d_lat;//AWS20030911

  cout<<"ib_min2 ibmax2:"<<ib_min2<<" "<<ib_max2<<endl;

  double sa_norm_11,   sa_norm_12,   sa_norm_21,   sa_norm_22;
         sa_norm_11=0.;sa_norm_12=0.;sa_norm_21=0.;sa_norm_22=0.;


          for (i_long=il_min1;  i_long<=il_max1           ; i_long++   )//AWS20030910
	  for (i_lat =ib_min1;  i_lat <=ib_max1           ; i_lat++    )//AWS20030910
              sa_norm_11+=sabin(lat_min,d_long,d_lat,i_lat); // multiply by solid angle of bin


          for (i_long=il_min1;  i_long<=il_max1           ; i_long++   )//AWS20030910
          for (i_lat =ib_min2;  i_lat <=ib_max2           ; i_lat++    )//AWS20030910
              sa_norm_12+=sabin(lat_min,d_long,d_lat,i_lat); 

          for (i_long=il_min2;  i_long<=il_max2           ; i_long++   )//AWS20030910
	  for (i_lat =ib_min1;  i_lat <=ib_max1           ; i_lat++    )//AWS20030910
              sa_norm_21+=sabin(lat_min,d_long,d_lat,i_lat); // multiply by solid angle of bin


          for (i_long=il_min2;  i_long<=il_max2           ; i_long++   )//AWS20030910
          for (i_lat =ib_min2;  i_lat <=ib_max2           ; i_lat++    )//AWS20030910
              sa_norm_22+=sabin(lat_min,d_long,d_lat,i_lat); 


  profile11=egret_map->ProjectionZ("EGRET_spectrum1" ,il_min1,il_max1, ib_min1,ib_max1);//AWS20040301
  profile12=egret_map->ProjectionZ("EGRET_spectrum2" ,il_min1,il_max1, ib_min2,ib_max2);
  profile21=egret_map->ProjectionZ("EGRET_spectrum3" ,il_min2,il_max2, ib_min1,ib_max1);
  profile22=egret_map->ProjectionZ("EGRET_spectrum4" ,il_min2,il_max2, ib_min2,ib_max2);
  profile11->Scale(1./sa_norm_11);
  profile12->Scale(1./sa_norm_12);
  profile21->Scale(1./sa_norm_21);
  profile22->Scale(1./sa_norm_22);

  /*
  profile11->Scale(1./((il_max1-il_min1+1)*(ib_max1-ib_min1+1)));
  profile12->Scale(1./((il_max1-il_min1+1)*(ib_max2-ib_min2+1)));
  profile21->Scale(1./((il_max2-il_min2+1)*(ib_max1-ib_min1+1)));
  profile22->Scale(1./((il_max2-il_min2+1)*(ib_max2-ib_min2+1)));
  */
  profile  =egret_map->ProjectionZ("EGRET_spectrum5" ,il_min1,il_max1, ib_min1,ib_max1);

  for(i_E_EGRET=0; i_E_EGRET<data.n_E_EGRET                    ; i_E_EGRET++)
   profile->SetBinContent(i_E_EGRET,0);

  profile->Add(profile11,0.25);
  profile->Add(profile12,0.25);
  profile->Add(profile21,0.25);
  profile->Add(profile22,0.25);

  if(galplotdef.convolve_EGRET>=1)
   deconvolve_intensity(profile);

 // profile ->Draw();


 //AWS20060322
  counts_profile11=counts_map->ProjectionZ("EGRET_counts1"   ,il_min1,il_max1, ib_min1,ib_max1);
  counts_profile12=counts_map->ProjectionZ("EGRET_counts2"   ,il_min1,il_max1, ib_min2,ib_max2);
  counts_profile21=counts_map->ProjectionZ("EGRET_counts3"   ,il_min2,il_max2, ib_min1,ib_max1);
  counts_profile22=counts_map->ProjectionZ("EGRET_counts4"   ,il_min2,il_max2, ib_min2,ib_max2);


  
  counts_profile  =counts_map->ProjectionZ("EGRET_counts5"   ,il_min1,il_max1, ib_min1,ib_max1);

  for(i_E_EGRET=0; i_E_EGRET<data.n_E_EGRET                    ; i_E_EGRET++)
   counts_profile->SetBinContent(i_E_EGRET,0);

  // this was an bug:   the counts should not be scaled. only the error bars are affected  AWS20080207
  /*
  counts_profile->Add(counts_profile11,0.25);
  counts_profile->Add(counts_profile12,0.25);
  counts_profile->Add(counts_profile21,0.25);
  counts_profile->Add(counts_profile22,0.25);
  */

  counts_profile->Add(counts_profile11,1.00);
  counts_profile->Add(counts_profile12,1.00);
  counts_profile->Add(counts_profile21,1.00);
  counts_profile->Add(counts_profile22,1.00);

  counts_       =new double[data.n_E_EGRET]; 
 //AWS20060322


  intensity_    =new double[data.n_E_EGRET];
  intensity_esq1=new double[data.n_E_EGRET];
  intensity_esq2=new double[data.n_E_EGRET];


 

         for(i_E_EGRET=0; i_E_EGRET<data.n_E_EGRET                    ; i_E_EGRET++)
        {
	  intensity_[i_E_EGRET]=       profile ->GetBinContent(i_E_EGRET);
	     counts_[i_E_EGRET]=counts_profile ->GetBinContent(i_E_EGRET); //AWS20060322
          
          differential_spectrum(data.E_EGRET[i_E_EGRET],data.E_EGRET[i_E_EGRET+1] ,
                                intensity_[i_E_EGRET], g[i_E_EGRET],
                                intensity_esq1[i_E_EGRET],intensity_esq2[i_E_EGRET]);

	 
	  cout<<"e1,e2="<<data.E_EGRET[i_E_EGRET]<<" "<<data.E_EGRET[i_E_EGRET+1]
              <<" intensity="<<intensity_[i_E_EGRET]<<"intensity_esq1, 2 ="<<intensity_esq1[i_E_EGRET]<<" "<<intensity_esq2[i_E_EGRET]
	      <<" counts="<<counts_[i_E_EGRET]<<endl; //AWS20060322


	  profile11->SetName("axxx");
	  profile12->SetName("bxxx");
	  profile21->SetName("cxxx");
	  profile22->SetName("dxxx");

	  counts_profile11->SetName("exxx"); //AWS20060322
	  counts_profile12->SetName("fxxx");
	  counts_profile21->SetName("gxxx");
	  counts_profile22->SetName("hxxx");


	}//i_E_EGRET

         Ebar=new double[data.n_E_EGRET];
         Ibar=new double[data.n_E_EGRET];

	 // automatic determination of spectral index
	 differential_spectrum(data.E_EGRET, data.n_E_EGRET, intensity_, intensity_esq1, intensity_esq2, Ebar,Ibar);

         cout<<"plot_EGRET_spectrum: E^2*spectrum"<<endl;                            //AWS20040309
	 for(i_E_EGRET=0; i_E_EGRET<data.n_E_EGRET                    ; i_E_EGRET++) //AWS20040309
	   cout<<"EGRET spectrum *E^2: E1 E2 Ebar Ibar "                                                  //AWS20040309
               <<data.E_EGRET[i_E_EGRET]<<" "<<data.E_EGRET[i_E_EGRET+1]             //AWS20040309
               <<" "<<Ebar[i_E_EGRET]<<" "<<Ibar[i_E_EGRET]<<endl;                   //AWS20040309


// tex output  AWS20041119
// txtFILE in global and assigned in plot_spectrum

fprintf(txtFILE,"======================================================\n");
fprintf(txtFILE,"EGRET spectrum                                        \n");   
 fprintf(txtFILE,"     E(MeV)      E^2 * intensity (MeV cm^-2 sr^-1 s^-1)    counts    \n"); //AWS20070906
for(i_E_EGRET=0; i_E_EGRET<data.n_E_EGRET ; i_E_EGRET++)
  {
  fprintf(txtFILE,"%6.0f-%6.0f & %9.5f &                             %9.1f \\\\ \n",data.E_EGRET[i_E_EGRET],data.E_EGRET[i_E_EGRET+1],Ibar[i_E_EGRET],counts_[i_E_EGRET]   );  //AWS20070906
                                   // "\\\\" yields "\\"
  }
fprintf(txtFILE,"======================================================\n");


for(i_E_EGRET=0; i_E_EGRET<data.n_E_EGRET                    ; i_E_EGRET++)
{

 if(galplotdef.spectrum_style_EGRET==1 ||  galplotdef.spectrum_style_EGRET==3)
 {
  for (errorbar=1;errorbar<=2; errorbar++)
    {

          delta_err=0;                                                                  //AWS20040312

         if(galplotdef.sources_EGRET< 100)                          //EGRET only          AWS20060405
	 {
          if(i_E_EGRET>9) delta_err=0.05; // additional error for E>10 GeV                AWS20040312
          if(i_E_EGRET>9 )delta_err=0.20; // additional error for E>10 GeV                AWS20050725         NB need also below
          if(i_E_EGRET>10)delta_err=0.50; // additional error for E>20 GeV                AWS20050725         NB need also below
	 }

          if(errorbar==1)errorbarfactor=    1.0+galplotdef.error_bar_EGRET + delta_err; //AWS20040312
          if(errorbar==2)errorbarfactor=1./(1.0+galplotdef.error_bar_EGRET + delta_err);//AWS20040312

          stat_err=1.0/sqrt(1.0+counts_[i_E_EGRET]);                                    //AWS20060322
          if(errorbar==1)errorbarfactor*= 1.0+stat_err;                                 //AWS20060322
          if(errorbar==2)errorbarfactor/= 1.0+stat_err;                                 //AWS20060322

          cout<<data.E_EGRET[i_E_EGRET]<<" MeV counts="<<counts_[i_E_EGRET] <<" stat_err="<<stat_err<<endl;

	  segment[0]=intensity_esq1[i_E_EGRET]*errorbarfactor;
 	  segment[1]=intensity_esq2[i_E_EGRET]*errorbarfactor;

          

          spectrum=new TGraph(2,&data.E_EGRET[i_E_EGRET],segment);

	  spectrum->SetMarkerColor(kRed  );
	  spectrum->SetMarkerStyle(21); 
	  spectrum->SetMarkerStyle(22); // triangles
	  spectrum->SetMarkerSize (0.5);// 
	  spectrum->SetLineColor(kRed );
	  spectrum->SetLineWidth(3     );
	  spectrum->SetLineStyle(1      );
	  spectrum->SetMarkerStyle(22); // triangles
	  spectrum->SetMarkerSize (0.5);// 
	  spectrum->Draw();       // don't use "same" for TGraph
	  spectrum->Draw(" P ");  // points as markers  

          spectrum->Draw();
     }//errorbar
    }//if


 if(galplotdef.spectrum_style_EGRET==2 ||  galplotdef.spectrum_style_EGRET==3)
 {

          delta_err=0;                                                                  //AWS20040312

         if(galplotdef.sources_EGRET< 100)                          //EGRET only          AWS20060405
	 {
          if(i_E_EGRET>9) delta_err=0.05; // additional error for E>10 GeV                AWS20040312
          if(i_E_EGRET>9 )delta_err=0.20; // additional error for E>10 GeV                AWS20050725         NB need also above
          if(i_E_EGRET>10)delta_err=0.50; // additional error for E>20 GeV                AWS20050725         NB need also above
	 }

         segment[0]=Ibar[i_E_EGRET]*(1.0+galplotdef.error_bar_EGRET + delta_err);       //AWS20040312
         segment[1]=Ibar[i_E_EGRET]/(1.0+galplotdef.error_bar_EGRET + delta_err);       //AWS20040312

         stat_err=1.0/sqrt(1.0+counts_[i_E_EGRET]);                                    //AWS20060322
         segment[0]      *=         (1.0+stat_err);                                    //AWS20060322
         segment[1]      /=         (1.0+stat_err);                                    //AWS20060322

         cout<<data.E_EGRET[i_E_EGRET]<<" MeV counts="<<counts_[i_E_EGRET] <<" stat_err="<<stat_err<<endl;

    Ebar_segment[0]=Ebar[i_E_EGRET];
    Ebar_segment[1]=Ebar[i_E_EGRET];
    spectrum=new TGraph(2,Ebar_segment,segment);

	  spectrum->SetMarkerColor(kRed  );
	  spectrum->SetMarkerStyle(21); 
	  spectrum->SetMarkerStyle(22); // triangles
	  spectrum->SetMarkerSize (0.5);// 
	  spectrum->SetLineColor(kRed );
	  spectrum->SetLineWidth(3     );
	  spectrum->SetLineStyle(1      );
	  spectrum->SetMarkerStyle(22); // triangles
	  spectrum->SetMarkerSize (0.5);// 
	  spectrum->Draw();       // don't use "same" for TGraph
	  spectrum->Draw(" P ");  // points as markers  

          spectrum->Draw();

  }//if

 }//i_EGRET





   cout<<" <<<< plot_EGRET_spectrum    "<<endl;
   return status;
}

//------------------------------------------------------------------------------------------

void Galplot::differential_spectrum(double e1,double e2,double intensity, double g,double &intensity_esq1,double &intensity_esq2)
{
  // converts integral intensity between e1 and e2 to two differential values at e1, e2
  // for a given spectral index g
  // result in form E^2 I(E)
  double A;
  
  A=(-g+1.)/(pow(e2,-g+1.)-pow(e1,-g+1)) * intensity;

  intensity_esq1=A*pow(e1,-g+2);
  intensity_esq2=A*pow(e2,-g+2);

 cout<<"A="<<A<<endl;
} 
//------------------------------------------------------------------------------------------

void Galplot::differential_spectrum(double *e,int n, double *intensity,double *intensity_esq1,double *intensity_esq2,double *Ebar,double *Ibar)
{
  // converts integral intensity between energies
  // computes spectral index from neighouring bins
  // result in form E^2 I(E)

double *I1,*I2,*g,*A;
  I1=new double[n];
  I2=new double[n];
  g=new double[n];
  A=new double[n];



  int i;
  int niter,iter;



  for (i=0;i<n;i++) g[i]=2.00; // start value


 niter=5;
 for(iter=0;iter<niter;iter++)
    {
 cout<<endl;
  for (i=0;i<n;i++)
    {
      A[i]=intensity[i]*(1-g[i])/(pow(e[i+1],1-g[i])- pow(e[i],1-g[i]));
   

     
     
      Ebar[i]=pow(e[i]*e[i+1],0.5);
      Ibar[i]=A[i]*pow(Ebar[i]  ,-g[i]);
 
    }
  
  for (i=0;i<n;i++)
    {
      cout<<"  iter i e[i] e[i+1] A[i] g[i]    "<<iter<<" "<<i<<" "<<e[i]<<" "<<e[i+1]<<" "<<A[i]<<" "<<g[i]<<endl;
    }

  
  cout<<endl;

  for (i=1;i<n-1;i++)
    {
 

      g[i]=- log(Ibar[i+1]/Ibar[i-1])/ log(Ebar[i+1]/Ebar[i-1]);
 
    }
     
  // special case of end points
      g[0]  =- log(Ibar[1  ]/Ibar[0  ])/ log(Ebar[1  ]/Ebar[0  ]);
      g[n-1]=- log(Ibar[n-1]/Ibar[n-2])/ log(Ebar[n-1]/Ebar[n-2]);
     
    }//iter

  cout<<" differential_spectrum: result of automatic index determination"<<endl;
  for (i=0;i<n;i++)
    {
      cout<<"i E[i] E[i+1] A[i] g[i]    "<<i<<" "<<e[i]<<" "<<e[i+1]<<" "<<A[i]<<" "<<g[i]<<endl;
    }
 

  if(galplotdef.spectrum_index_EGRET!=0.0) // do not use  automatic  index determination
  {

   for (i=0;i<n;i++) g[i]=galplotdef.spectrum_index_EGRET; // given value
   for (i=0;i<n;i++) A[i]=intensity[i]*(1-g[i])/(pow(e[i+1],1-g[i])- pow(e[i],1-g[i]));
   cout<<" differential_spectrum: overriding with result of using given spectral index (not automatic)"<<endl;
   for (i=0;i<n;i++)
          cout<<"i E[i] E[i+1] A[i] g[i]    "<<i<<" "<<e[i]<<" "<<e[i+1]<<" "<<A[i]<<" "<<g[i]<<endl;
    

  }// if


  for (i=0;i<n;i++)
  {
  

   intensity_esq1[i]=A[i]*pow(e[i],   -g[i]+2.0);
   intensity_esq2[i]=A[i]*pow(e[i+1], -g[i]+2.0);

   Ebar[i]=pow(e[i]*e[i+1],0.5);                      //AWS20050916
   Ibar[i]          =A[i]*pow(Ebar[i],-g[i]+2.0);
  }
    
}
//------------------------------------------------------------------------------------------


void Galplot::deconvolve_intensity(TH1D *intensity)
{
// for 'deconvolved intensities': the histograms corresponding to
// the model before and after convolution in "Convolved convolved, UnConvolved unconvolved"
TH3F    *work_skymap;
TH1D    *profile;  
TH1D *profile11,*profile12,*profile21,*profile22; // four (l,b) intervals
TH1D *unconv_profile,*conv_profile;
   
int i_lat,i_long;
int ii_long,ii_lat;
int il_min,il_max,ib_min,ib_max;
int il_min1,il_max1,ib_min1,ib_max1;
int il_min2,il_max2,ib_min2,ib_max2;
double l,b,l_off;
int i_E_EGRET;
int unconv_conv;



 cout<<">>deconvolve_intensity"<<endl;

 work_skymap=new TH3F("work","work ",
                  galaxy.n_long,       -180,           180.,
                  galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1),
                  data.EGRET_counts.n_pgrid,  data.E_EGRET[0], data.E_EGRET[data.n_E_EGRET]);




  // NB reversed longitude axis starting at +180 and decreasing

  l_off=180.-galplotdef.long_max1; if(l_off>360.)l_off-=360.; if(l_off<  0.)l_off+=360.;
  il_min1=l_off/galdef.d_long;
  l_off=180.-galplotdef.long_min1; if(l_off>360.)l_off-=360.; if(l_off<  0.)l_off+=360.;
  il_max1=l_off/galdef.d_long;
  cout<<"il_min1 ilmax1:"<<il_min1<<" "<<il_max1<<endl;

  l_off=180.-galplotdef.long_max2; if(l_off>360.)l_off-=360.; if(l_off<  0.)l_off+=360.;
  il_min2=l_off/galdef.d_long;
  l_off=180.-galplotdef.long_min2; if(l_off>360.)l_off-=360.; if(l_off<  0.)l_off+=360.;
  il_max2=l_off/galdef.d_long;
  cout<<"il_min2 ilmax2:"<<il_min2<<" "<<il_max2<<endl;


  ib_min1=(galplotdef.lat_min1-galdef.lat_min+.0001)/galdef.d_lat;
  ib_max1=(galplotdef.lat_max1-galdef.lat_min+.0001)/galdef.d_lat;
  cout<<"ib_min1 ibmax1:"<<ib_min1<<" "<<ib_max1<<endl;

  ib_min2=(galplotdef.lat_min2-galdef.lat_min+.0001)/galdef.d_lat;
  ib_max2=(galplotdef.lat_max2-galdef.lat_min+.0001)/galdef.d_lat;
  cout<<"ib_min2 ibmax2:"<<ib_min2<<" "<<ib_max2<<endl;

  // create these histograms with correct dimensions
 unconv_profile=work_skymap->ProjectionZ("unconv_profile" ,il_min1,il_max1, ib_min1,ib_max1);
   conv_profile=work_skymap->ProjectionZ("  conv_profile" ,il_min1,il_max1, ib_min1,ib_max1);


 for (unconv_conv=1;unconv_conv<=2;unconv_conv++)
   {
 
         for(i_E_EGRET=0; i_E_EGRET<data.n_E_EGRET                    ; i_E_EGRET++)
        {

	 for (i_lat =0;  i_lat <galaxy.n_lat;        i_lat ++)
         {
          for(i_long=0;  i_long<galaxy.n_long;       i_long++)
          {

           l=galaxy.long_min+galaxy.d_long*i_long;
           b=galaxy. lat_min+galaxy. d_lat*i_lat;





            l_off=l-180.;
            if(l_off<0.)l_off+=360.0;
            ii_long= galaxy.n_long-l_off/galaxy.d_long;
	    //cout<<"l l_off ii_long"<<l<<" "<<l_off<<" "<<ii_long<<endl;

            if(unconv_conv==1) 
            work_skymap  ->SetBinContent
                           (ii_long,i_lat,i_E_EGRET,unconvolved.EGRET_total_skymap.d2[i_long][i_lat].s[i_E_EGRET]); 
            if(unconv_conv==2) 
            work_skymap  ->SetBinContent
                           (ii_long,i_lat,i_E_EGRET,  convolved.EGRET_total_skymap.d2[i_long][i_lat].s[i_E_EGRET]); 

            }  //  i_long
           }   //  i_lat

	}//i_E_EGRET


  if(unconv_conv==1)
    {
  profile11=work_skymap->ProjectionZ("EGRET_spectrum16" ,il_min1,il_max1, ib_min1,ib_max1);//AWS20040301
  profile12=work_skymap->ProjectionZ("EGRET_spectrum17" ,il_min1,il_max1, ib_min2,ib_max2);
  profile21=work_skymap->ProjectionZ("EGRET_spectrum18" ,il_min2,il_max2, ib_min1,ib_max1);
  profile22=work_skymap->ProjectionZ("EGRET_spectrum19" ,il_min2,il_max2, ib_min2,ib_max2);
    }

  if(unconv_conv==2)
    {
  profile11=work_skymap->ProjectionZ("EGRET_spectrum26" ,il_min1,il_max1, ib_min1,ib_max1);
  profile12=work_skymap->ProjectionZ("EGRET_spectrum27" ,il_min1,il_max1, ib_min2,ib_max2);
  profile21=work_skymap->ProjectionZ("EGRET_spectrum28" ,il_min2,il_max2, ib_min1,ib_max1);
  profile22=work_skymap->ProjectionZ("EGRET_spectrum29" ,il_min2,il_max2, ib_min2,ib_max2);
    }

  profile11->Scale(1./((il_max1-il_min1+1)*(ib_max1-ib_min1+1)));
  profile12->Scale(1./((il_max1-il_min1+1)*(ib_max2-ib_min2+1)));
  profile21->Scale(1./((il_max2-il_min2+1)*(ib_max1-ib_min1+1)));
  profile22->Scale(1./((il_max2-il_min2+1)*(ib_max2-ib_min2+1)));
  
  if(unconv_conv==1)
    {
  unconv_profile =    profile11;
  unconv_profile->Add(profile12);
  unconv_profile->Add(profile21);
  unconv_profile->Add(profile22);
    }       


  if(unconv_conv==2)
    {
  conv_profile =    profile11;
  conv_profile->Add(profile12);
  conv_profile->Add(profile21);
  conv_profile->Add(profile22);
    }       



   }// unconv_conv 

 cout<<" plot_EGRET_spectrum deconvolve_intensity unconv_profile spectrum= "<<endl;
 for(i_E_EGRET=0; i_E_EGRET<data.n_E_EGRET; i_E_EGRET++)cout<<unconv_profile->GetBinContent(i_E_EGRET)<<" " ;
 cout<<endl;
 cout<<" plot_EGRET_spectrum deconvolve_intensity  conv_profile spectrum="<<endl;
 for(i_E_EGRET=0; i_E_EGRET<data.n_E_EGRET; i_E_EGRET++)cout<<  conv_profile->GetBinContent(i_E_EGRET)<<" " ;
 cout<<endl;

 //  unconv_profile->Draw();
 //   conv_profile->Draw("same");



   intensity->Divide  (  conv_profile);
   intensity->Multiply(unconv_profile);


	  profile11->SetName("1xxx");
	  profile12->SetName("2xxx");
	  profile21->SetName("3xxx");
	  profile22->SetName("4xxx");
       conv_profile->SetName("5xxx");
     unconv_profile->SetName("6xxx");

 cout<<"<<deconvolve_intensity"<<endl;

 return;
}
