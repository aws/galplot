

#include"Galplot.h"              
#include"galprop_classes.h"
#include"galprop.h"
#include"constants.h"
#include"fitsio.h"


int Galplot::plot_GLAST_profile(int longprof, int latprof,int ip,int mode)
{
   cout<<" >>>> plot_GLAST_profile    "<<endl;
   int status=0;


int i_comp,i_lat,i_long;
int ii_long,ii_lat;
int il_min,il_max,ib_min,ib_max;
int il_min1,il_max1,ib_min1,ib_max1;
int il_min2,il_max2,ib_min2,ib_max2;
int n_long_rebin,n_lat_rebin;

double l,b,l_off;
double counts,exposure,intensity;
double long_min,lat_min;
double d_long, d_lat;
char name[100],canvastitle[100], workstring1[100],workstring2[100];

 

TCanvas *c1;
TH2F *egret_map;
TH2F *egret_counts;
TH1D *profile;
TH1D *profile_counts;

 int ncolors=0; int *colors=0;// default palette with 50 colours  AWS20050915
gStyle->SetPalette(ncolors,colors);


 cout<<data.GLAST_counts.n_rgrid<<" "<<data.GLAST_counts.n_zgrid<<endl;

  
 
 
  {
 {
 // names must be different or canvas disappears

  sprintf(name," GLAST energy range %d",ip);
  strcpy(canvastitle," galdef ID");
  strcat(canvastitle,galdef.galdef_ID);
  strcat(canvastitle,name);


  //       map is 2D Distribution, hence coords are formally r, z

  egret_map=  new TH2F(canvastitle,canvastitle,
                       data.GLAST_counts.n_rgrid,       -180,           180.,
                       data.GLAST_counts.n_zgrid,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1));

  egret_counts=new TH2F(canvastitle,canvastitle,
                        data.GLAST_counts.n_rgrid,       -180,           180.,
                        data.GLAST_counts.n_zgrid,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1));


  long_min= -179.75;//specific for EGRET data counts.gp1234_30.g001
   lat_min  =-89.75;
  d_long      =0.5;     
  d_lat       =0.5;

	 for (i_lat =0;  i_lat <data.GLAST_counts.n_zgrid            ;  i_lat++ ){
          for(i_long=0;  i_long<data.GLAST_counts.n_rgrid             ; i_long++){

            l=       long_min+ d_long   *i_long;
            b=        lat_min+ d_lat    *i_lat;

            exposure =data.GLAST_exposure.d2[i_long][i_lat].s[ip];
            counts   =data.GLAST_counts.  d2[i_long][i_lat].s[ip];
            intensity=0.;
            if(exposure>0.)
            intensity=           counts /exposure;

            if(galplotdef.verbose==-1401) // selectable debug                AWS20070710
	      cout<<"plot_GLAST_profile: i_long="<<i_long<<" ilat="<<i_lat<<" exposure="<<exposure<<" intensity="<<intensity<<endl;

            l_off=l-180.;
            if(l_off<0.)l_off+=360.0;
            ii_long= int( data.GLAST_counts.n_rgrid-l_off/d_long);//AWS20031223

            ii_long+=1;                                                    // TH2D binning starts at 1   AWS20080110

            if(galplotdef.verbose==-1401) // selectable debug                AWS20070710
	      cout<<"l l_off ii_long"<<l<<" "<<l_off<<" "<<ii_long<<endl;


	    ii_lat=i_lat+1;                                                // TH2D binning starts at 1   AWS20080110
	    egret_map    ->SetBinContent(ii_long,ii_lat,intensity);        // ii_lat replaces i_lat      AWS20080110
	    egret_counts ->SetBinContent(ii_long,ii_lat,counts   );        // ii_lat replaces i_lat      AWS20080110  

            }  //  i_long
           }   //  i_lat

 

  egret_map       ->GetXaxis()->SetTitle("Galactic longitude");
 

  // construct conventional Galactic longitude axis in two seqments, 180-0 and 340-180
  TF1 *f1=new TF1("f1","-x",   0,180); // root manual p. 149
  TF1 *f2=new TF1("f2","-x", 180,340); // root manual p. 149

  //                      xmin ymin xmax ymax, funcname   ndiv chopt gridlength
  TGaxis *axis1=new TGaxis(-180,-90 ,  0.,-90  , "f1",       9,""                );
  axis1->SetLabelSize(0.03);
  TGaxis *axis2=new TGaxis(  20,-90 ,180.,-90  , "f2",       8,""                );
  axis2->SetLabelSize(0.03);
  axis2->SetTitle("Galactic longitude");
  // fill in 0-340 between two axis segments, unlabelled
  // at present has no visibile effect but could be useful if more minor tickmarks added
  //                         xmin ymin xmax ymax   wmin wmax ndiv chopt gridlength
  TGaxis *axis3=new TGaxis(  0,  -90.  , 20., -90., 0., 20. ,0, "U"               );


  // do the plotting


  
  // c1=new TCanvas(canvastitle,       canvastitle,280+ip*10,200+ip*10,600,400);

  egret_map->GetXaxis()->SetNdivisions(0); //remove axis since longitude axis done separately

  /*
  egret_map       ->Draw("colz");
  axis1->Draw();   axis2->Draw();   axis3->Draw();
    
  */

  


//==============================     Profiles  =============================



  // construct conventional Galactic longitude axis in two seqments, 180-0 and 340-180
       f1=new TF1("f1","-x",   0,180); // root manual p. 149
       f2=new TF1("f2","-x", 180,340); // root manual p. 149

  //                      xmin ymin xmax ymax, funcname   ndiv chopt gridlength
          axis1=new TGaxis(-180,0.  ,  0.,0.   , "f1",       9,""                );
  axis1->SetLabelSize(0.02);
          axis2=new TGaxis(  20,0.  ,180.,0.   , "f2",       8,""                );
  axis2->SetLabelSize(0.02);
  axis2->SetTitle("Galactic longitude");
  // fill in 0-340 between two axis segments, unlabelled
  // at present has no visibile effect but could be useful if more minor tickmarks added
  //                         xmin ymin xmax ymax   wmin wmax ndiv chopt gridlength
          axis3=new TGaxis(  0,    0.  , 20., 0.  , 0., 20. ,0, "U"               );


     gStyle->SetHistLineColor(1);//used to create histogram; 1=black 2=red 3=green 4=blue 5=yellow



  // two l or two b ranges


  l_off=180.-galplotdef.long_max1; if(l_off>360.)l_off-=360.; if(l_off<  0.)l_off+=360.;
  il_min1=int(l_off/       d_long);//AWS20031223
  l_off=180.-galplotdef.long_min1; if(l_off>360.)l_off-=360.; if(l_off<  0.)l_off+=360.;
  il_max1=int(l_off/       d_long);//AWS20031223
  cout<<"il_min1 ilmax1:"<<il_min1<<" "<<il_max1<<endl;

  l_off=180.-galplotdef.long_max2; if(l_off>360.)l_off-=360.; if(l_off<  0.)l_off+=360.;
  il_min2=int(l_off/       d_long);//AWS20031223
  l_off=180.-galplotdef.long_min2; if(l_off>360.)l_off-=360.; if(l_off<  0.)l_off+=360.;
  il_max2=int(l_off/       d_long);//AWS20031223
  cout<<"il_min2 ilmax2:"<<il_min2<<" "<<il_max2<<endl;


  ib_min1=int((galplotdef.lat_min1-galdef.lat_min+.0001)/       d_lat);//AWS20031223
  ib_max1=int((galplotdef.lat_max1-galdef.lat_min+.0001)/       d_lat);//AWS20031223
  cout<<"ib_min1 ibmax1:"<<ib_min1<<" "<<ib_max1<<endl;

  ib_min2=int((galplotdef.lat_min2-galdef.lat_min+.0001)/       d_lat);//AWS20031223
  ib_max2=int((galplotdef.lat_max2-galdef.lat_min+.0001)/       d_lat);//AWS20031223
  cout<<"ib_min2 ibmax2:"<<ib_min2<<" "<<ib_max2<<endl;





	  if(longprof)
	    {

  ib_min=int((galplotdef.lat_min-galdef.lat_min+.0001)/d_lat);//AWS20031223
  ib_max=int((galplotdef.lat_max-galdef.lat_min+.0001)/d_lat);//AWS20031223
  cout<<"ib_min ibmax:"<<ib_min<<" "<<ib_max<<endl;


  sprintf(workstring2,"  %5.1f>b>%5.1f",galplotdef.lat_min,galplotdef.lat_max);
  strcpy(workstring1,canvastitle);
  strcat(workstring1,workstring2);


  strcat(name,"glast_longitude");// to identify it: name is not plotted
  
  if(mode==1)
  c1=new TCanvas(name,canvastitle,300+ip*10,150+ip*10,600,600);

 
 
  cout<<"before egret_map GetNbinsX "<<egret_map->GetNbinsX()<<endl;
  cout<<"before egret_map GetNbinsY "<<egret_map->GetNbinsY()<<endl;
 
  profile=     egret_map->ProjectionX("longplot", ib_min1,ib_max1);
  // profile=     egret_map->ProjectionX(canvastitle, ib_min1,ib_max1);
  cout<<"before Add longplot2 profile GetNbinsX "<<profile->GetNbinsX()<<endl;

   

  profile->Add(egret_map->ProjectionX("longplot2", ib_min2,ib_max2),1.0);
 cout<<"after Add longplot2"<<endl;

  profile->Scale(1./(ib_max1-ib_min1+1  + ib_max2-ib_min2+1 ));
  profile->GetXaxis()->SetTitle("Galactic longitude");
  profile->GetXaxis()->SetNdivisions(0); // removes longitude axis since done separately
  profile->GetYaxis()->SetTitle("intensity");
  profile->SetTitle(workstring1); //written in box on plot

  n_long_rebin=int(galplotdef.long_binsize/d_long+.0001);
  profile->Rebin(    n_long_rebin);
  profile->Scale (1./n_long_rebin);

  profile_counts=     egret_counts->ProjectionX("longplot_counts",  ib_min1,ib_max1);
  profile_counts->Add(egret_counts->ProjectionX("longplot_counts2", ib_min2,ib_max2),1.0);
  profile_counts->Rebin(    n_long_rebin);

 for(i_long=0;i_long<profile->GetNbinsX();i_long++)
  { 
    ii_long=i_long+1; // TH2D bins start at 1                                                                      AWS20080110
    
    counts=profile_counts->GetBinContent(ii_long);                                                                //AWS20080110
    if(counts>=1.0)
     profile->SetBinError(ii_long,profile->GetBinContent(ii_long)*(1./sqrt(counts)+galplotdef.error_bar_EGRET) ); //AWS20080110
    else   
     profile->SetBinError(ii_long,0.0);                                                                           //AWS20080110
     
    //cout<<"profile counts error= "<< profile->GetBinContent(i_long)<<" "<<counts<<" "<< profile->GetBinError(i_long)<<endl;
  }

 

  if (mode==0)
  profile->Draw("e same");

  if (mode==1)
    {
  profile->Draw();
  axis1->Draw();   axis2->Draw();   axis3->Draw();
    }
 
	    }//if


	  //----------------------------------------------------

	  if(latprof)
	    {
	      /*
  il_min=(galplotdef.long_min-       long_min+.0001)/d_long;
  il_max=(galplotdef.long_max-       long_min+.0001)/d_long;
  cout<<"il_min ilmax:"<<il_min<<" "<<il_max<<endl;
	      */
  sprintf(workstring2,"  %5.1f>l>%5.1f",galplotdef.long_min,galplotdef.long_max);
  strcpy(workstring1,canvastitle);
  strcat(workstring1,workstring2);

  // NB reversed longitude axis starting at +180 and decreasing

  l_off=180.-galplotdef.long_max ; if(l_off>360.)l_off-=360.; if(l_off<  0.)l_off+=360.;
  il_min =int(l_off/d_long);//AWS20031223
  l_off=180.-galplotdef.long_min ; if(l_off>360.)l_off-=360.; if(l_off<  0.)l_off+=360.;
  il_max =int(l_off/d_long);//AWS20031223
  cout<<"il_min  ilmax :"<<il_min <<" "<<il_max <<endl;


  strcat(name,"glast_latitude");
  
  if(mode==1)
  c1=new TCanvas(name,canvastitle,350+ip*10,150+ip*10,600,600);

  profile=     egret_map->ProjectionY("latplot "        , il_min1,il_max1);
  profile->Add(egret_map->ProjectionY("latplot2"        , il_min2,il_max2),1.0);
  profile->Scale(1./(il_max1-il_min1+1   +il_max2-il_min2+1));
  profile->GetXaxis()->SetTitle("Galactic latitude");
  profile->GetYaxis()->SetTitle("intensity");
  profile->SetTitle(workstring1); //written in box on plot



  n_lat_rebin=int(galplotdef.lat_binsize/d_lat+.0001);
  profile->Rebin    (n_lat_rebin);
  profile->Scale (1./n_lat_rebin);


  profile_counts=     egret_counts->ProjectionY("latplot_counts",  il_min1,il_max1);
  profile_counts->Add(egret_counts->ProjectionY("latplot_counts2", il_min2,il_max2),1.0);
  profile_counts->Rebin(    n_lat_rebin);

 for(i_lat=0;i_lat<profile->GetNbinsX();i_lat++)
  { 
    ii_lat=i_lat+1; //TH2D bins start at 1 AWS20080110

    counts=profile_counts->GetBinContent(ii_lat);//AWS20080110
    if(counts>=1.0)
     profile->SetBinError(ii_lat,profile->GetBinContent(ii_lat)*(1./sqrt(counts)+galplotdef.error_bar_EGRET) );//AWS20080110
    else   
     profile->SetBinError(ii_lat,0.0);//AWS20080110
     
    // cout<<"profile counts error= "<< profile->GetBinContent(i_lat)<<" "<<counts<<" "<< profile->GetBinError(i_lat)<<endl;
  }

  if (mode==0)
  profile->Draw("e same");

  if (mode==1)
  profile->Draw();


	    }//if

 }//if E_gamma

  
} //ip

//============================================================================

// change name to avoid problem with ProjectionX, Projection Y on subsequent calls
  profile       ->SetName("xxxx");
  profile_counts->SetName("yyyy");
 
 

   cout<<" <<<< plot_GLAST_profile    "<<endl;
   return status;
}


 
