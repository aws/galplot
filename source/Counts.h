#ifndef COUNTSMAP_H
#define COUNTSMAP_H

#include <string>
#include <valarray>
#include "Skymap.h"
#include <exception>
#include "Parameters.h"

/** \brief Counts map serves as a storage for binned counts.
 *
 * It has the ability to read counts from both pre-binned healpix based maps,
 * or automatically bin counts from FT1 files.
 */
class CountsMap {
	public:
		/** \brief Standard constructor, basically does nothing */
		CountsMap(){}
		/** \brief Construct a counts map from a Parameters object.
		 *
		 * \param pars should contain the following parameters
		 *  - countsFile: name of the input file
		 *  - countOrder: the order of the resulting map
		 *  - energyBinning: type of energy binning (log, linear or list)
		 *  - nEnergyBins: number of energy bins
		 *  - eMin: minimum energy in MeV (a space separated list of values if
		 *  energyBinning is list)
		 *  - eMax: maximum energy in MeV (a space separated list of values if
		 *  energyBinning is list)
		 *  - FT1filter: a filter used when opening the FT1 fits file (optional)
		 *  - countsOutFile: name of an output file for the binned FT1 files
		 *  (optional)
		 *
		 * If countOrder is not given, the file is assumed to be of healpix type
		 * and all other parameters are unrelevant.  If it is not given, file
		 * should either be a FT1 fits file or a text file containing a list of FT1
		 * file names, which will all be included in the binning.
		 */
		CountsMap(const Parameters &pars);
		/** \brief Construct an empty counts map of given order with given energy
		 * boundaries.
		 *
		 * \param order gives the order of the healpix map
		 * \param eMin and \param eMax are valarrays containing the lower and upper
		 * boundaries, respecitvely, of the energy bins in MeV.
		 */
		CountsMap(int order, std::valarray<double> & eMin, std::valarray<double> & eMax);
		/** \brief Construct a counts map from a healpix file.
		 *
		 * \param fileName should give the location of a healpix fits file.  The
		 * data should be in an extension name counts, containing a table with a
		 * single vector column, where each row is a single healpix pixel and the
		 * vector contains the count as a function of energy.  There should be
		 * another extension, EBINS, holding a table with information about the
		 * energy bins.  That table should have three columns, Channel for the
		 * channel number, E_Min for the minimum energy and E_Max for the maximum
		 * energy.
		 *
		 * This is the output format when counts map is used to bin FT1 files.
		 */
		CountsMap(const std::string &fileName);
		/** \brief Construct a counts map from a FT1 fits file.
		 *
		 * \param fileName is the name of the fits file, or a name of a text file * containing a list of FT1 filenames, one per line.  
		 * \param order controls * the order of the healpix map and 
		 * \param eMin lower energy boundaries
		 * \param eMax upper energy boundaries
		 * \param filter an optional parameter that filters the table on opening.  This string should follow the cfitsio
		 * convention for extended file syntax, without the brackets. Example: \code filter = "MC_SRC_ID == 13000" \endcode
		 */
		CountsMap(const std::string &fileName, int order, std::valarray<double> & eMin, std::valarray<double> & eMax, const std::string &filter = "");
		/** \brief Copy constructor */
		CountsMap(const CountsMap & oldCountsMap);
		/** \brief Add photons to the countmap from an valarray of coordinates,
		 * energy pairs.
		 *
		 * \param photonArray is a valarray of coordinate, energy pairs, where the
		 * energy is given in MeV.
		 */
		void addPhotons(const std::valarray< std::pair< SM::Coordinate, double > > & photonArray);
		/** \brief Add the contents of an FT1 file, or a text file with a list of
		 * FT1 files to the counts map.
		 *
		 * \param fileName should point to the FT1 file or the text file with a
		 * list of FT1 files, one per line.  An optional parameter \param filter
		 * will be applied when opening the FT1 file(s).  It should follow the extended
		 * file name syntax of cfitsio.
		 */
		void addFile(const std::string &fileName, const std::string & filter = "");
		/** \brief Reset the counts map to different order and energy bins.
		 *
		 * Destroys all data.  \param order gives the healpix order while \param
		 * eMin and \param eMax give energy boundaries.
		 */
		void reset(int order, std::valarray<double> &eMin, std::valarray<double> & eMax);
		/** \brief Return a constant reference to the skymap.
		 *
		 * The energy stored in the skymap object refers to the center value of the
		 * energy bin.
		 */
		//		const Skymap<long> & getCountsMap() const;
		const Skymap<int> & getCountsMap() const;                              //AWS20091002 
		/** \brief Return a const reference to the lower energy boundaries */
		const std::valarray<double> & getEMin() const;
		/** \brief Return a const reference to the upper energy boundaries */
		const std::valarray<double> & getEMax() const;
		/** \brief Return the number of photons lost in binning.
		 *
		 * When binning the data, the energy of the photon can be out of range for
		 * the energy bins.  This returns the numbers of photon that where in the
		 * FT1 files, but did not end in the count map.  This does not take into
		 * account any filter applied, since that is done prior to binning.
		 */
		int lost() const;
		/** \brief Write the counts map to a fits file.
		 *
		 * \param fileName is the name of the output file.  It will be overwritten
		 * without an error message.  The format is the same as described in the
		 * healpix fits file constructor.
		 */
		void write(const std::string &fileName) const;
		/** \brief A general exception class for the object to throw */
		class CountsMapException : public std::exception {
			private:
				std::string eMessage;
			public:
				/** \brief Construct an error with the default message "Error in CountsMap class" */
				CountsMapException() : eMessage("Error in CountsMap class"){}
				/** \brief Construct an error with a specialized error message
				 *
				 * \param message is the specialized message
				 */
				CountsMapException(const std::string & message) : eMessage(message){}
				~CountsMapException() throw(){}
				/** \brief Return the error message */
				virtual const char* what () const throw(){
					return(eMessage.c_str());
				}
		};

		/** \brief The assignment operator */
		CountsMap & operator = (const CountsMap & oldCountsMap);

	private:
		/** \brief Read an FT1 file.
		 *
		 * \param fileName should give the name of file to read and it should be an
		 * FT1 fits file.  If an error occurs in reading the file, the routine
		 * returns a value different from 0.  \param filter is an optional
		 * parameter for filtering the FT1 file.  It should follow the cfitsio
		 * extended file name syntax.
		 */
		void ReadFT1Fits(const std::string &fileName, const std::string & filter = "");
		/** \brief Read an healpix fits file.
		 *
		 * \param fileName should give the name of the file to read and it should
		 * be in a compatible format, \see CountsMap::CountsMap. Uses CCFITS which
		 * throws errors if something goes wrong.
		 */
		void ReadHealpixFits(const std::string &fileName);
		/** \brief Add a coordinate,energy pair to the counts map */
		void Add(const std::pair<SM::Coordinate, double> & photon);
		//		Skymap<long> fDataSkymap;  //!< Stores the counts data
		Skymap<int> fDataSkymap;  //!< Stores the counts data                    AWS20091002
		std::valarray<double> fEMin, fEMax;  //!< The skymap only stores single energy value, so we need to implement the bins
		int fLost; //!< Number of lost photons in the binning, not counting filters
		std::string fEnergyCol; //!< The name of the energy column
};
#endif
