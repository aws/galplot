/* based on osse_kinzer.pro
   
  Kinzer et al. ApJ 515, 251 (1999)
  Table 3 VP 5+16 (l=b=0) and check against Fig 1a 

  Kinzer units are MeV
  flux is per radian for flat longitude distribution
  interpret this as integrated over latitude
  so total flux from inner radian is just this value.

  Also Fig 1 of Kinzer et al. ApJ 559, 282 (2001)


  Factor allows scaling from Galactic centre
*/
#include"Galplot.h"                    //AWS20050920
#include"galprop_classes.h"
#include"galprop.h"
#include"constants.h"
#include"fitsio.h"

int Galplot::plot_OSSE_spectrum(int mode)
{

  // mode=1: all components
  // mode=2: total only

   cout<<" >>>> plot_OSSE_spectrum    "<<endl;
   int status=0;

   int test_comparison;

 
   int n_energy;
   double E, intensity,flux_cr,flux_exp,flux_positronium;
   double Emin,Emax,dE;     
   double solid_angle;             
   int i_energy;
   int component;

   TGraph *spectrum;
  
   Emin=0.02;//MeV
   Emax=1.0;
   dE= 0.01;

   // as used up to 2005412
   solid_angle= (galplotdef. lat_max1-galplotdef. lat_min1 + galplotdef. lat_max2-galplotdef. lat_min2 )
               *(galplotdef.long_max1-galplotdef.long_min1 + galplotdef.long_max2-galplotdef.long_min2 )
                /(57.3*57.3);// sterad;

   // from 20050413
   // since already per radian of longitude, should divide only by radian of latitude
   solid_angle= (galplotdef. lat_max1-galplotdef. lat_min1 + galplotdef. lat_max2-galplotdef. lat_min2 ) //AWS20050413
               /(57.3);


   // Skibo 1993 model
   double Acr=5.25e-2 ;//at 0.1 MeV
   double gcr= 1.63;

   double Aexp=0.335 ;// NB units not 1e-2 as in Table 3 caption
   double gamma1=1.84;
   double Ec=0.124;


   double factor_continuum,factor_positronium;
   double factor_cr,factor_exp;

   factor_continuum  =0.5; // factors as in A&A INTEGRAL Special
   factor_positronium=0.5;

   factor_cr         =0.5;
   factor_exp        =0.5;
   factor_positronium=0.5;

   
   factor_cr         =1.0;
   factor_exp        =1.0;
   factor_positronium=1.0;
   



   n_energy=int((Emax-Emin)/dE);



   for (component=1;component<=4;component++)
  {

   if(mode == 1 || ( mode == 2 && component == 4) ) //AWS20050509
   {

    spectrum=new TGraph(n_energy);// otherwise disappear

   for (i_energy=0;i_energy<n_energy;i_energy++)
   {
     E=Emin+i_energy*dE;
     intensity=1.e-2;

   ;
   flux_cr         = Acr * pow( E/0.1,  -gcr);
   flux_exp        = Aexp* pow( E/0.1 , -gamma1)*exp(-(E-0.1)/Ec);

   flux_positronium= E * 0.1 ;   // from Fig 1a to give 5e-2 at 0.511 MeV
   if(E>.511) flux_positronium=0.;
   /* AWS20050408
   flux_cr         *=factor_continuum;
   flux_exp        *=factor_continuum;
   */
   flux_cr         *=factor_cr;               
   flux_exp        *=factor_exp;              
   flux_positronium*=factor_positronium;

   if(component==1)intensity=flux_cr;                                   
   if(component==2)intensity=          flux_exp;                             
   if(component==3)intensity=                      flux_positronium;
   if(component==4)intensity=flux_cr + flux_exp +  flux_positronium;

   intensity/=solid_angle;
   intensity *= pow(E,2.);

   spectrum->SetPoint(i_energy,E,intensity);


   }//i_energy

  spectrum->SetMarkerColor(kMagenta);// also kCyan
  spectrum->SetMarkerStyle(21);
  spectrum->SetMarkerStyle(22); // triangles
  spectrum->SetMarkerSize (0.5);//
  spectrum->SetLineColor(kMagenta);// also kCyan

  spectrum->SetLineWidth(1     );
  if(component==4)
  spectrum->SetLineWidth(3     );

  spectrum->SetLineStyle(1      );//1=solid 2=dash 3=dot 4=dash-dot
  spectrum->SetMarkerStyle(22); // triangles
  spectrum->SetMarkerSize (0.5);//


  //spectrum->Draw("PL");  // points as markers + line 
  spectrum->Draw("L");                              


  }// if mode

}// component
 

  cout<<" <<<< plot_OSSE_spectrum    "<<endl;
   return status;
}

