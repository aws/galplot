using namespace std;

#include<iostream>

#include"SPIERSP.h"

  int  i_E, j_E;
  int ii_E,jj_E;
  int jjj_E;
  
  double work1,work11,work2,work22,work3,work33,work4,work5;
  double Elo,Ehi;
  double irf_sum1_interp, irf_sum2_interp, irf_sum3_interp;
  double irf_sum1_interp_prime, irf_sum1_interp_prime_lo, irf_sum1_interp_prime_hi ;
  int i,j,k;
  int debug;
////////////////////////////////////////////////////////////////////////////////////////////////////
 int SPIERSP:: read (char *directory,int idet1,int idet2,
                     double *E_in_,int n_E_in_,double *E_out_lo_,double*E_out_hi_,int n_E_out_)

{




  cout<<"SPIERSP"<<endl;

  debug=0;

  rmf.read(directory);
  irf.read(directory,idet1,idet2);
   
  /////////////////// summary of IRF information used /////////////////////////

  double *compton_fraction=new double[irf.n_E];
  for (i_E=0;i_E<irf.n_E;i_E++)
        compton_fraction[i_E] = (               irf.sum2[i_E]+irf.sum3[i_E])
                               /(irf.sum1 [i_E]+irf.sum2[i_E]+irf.sum3[i_E]+1e-10);

  cout<<" SPIERSP:        detectors "<<idet1<<" - "<<idet2<<endl;
  cout<<"IRF matrix sum as function of energy for 3 IRF types:"<<endl;
  for (i_E=0;i_E<irf.n_E;i_E++) 
    cout<<i_E<<"  "<<rmf.E_in[i_E]<<" keV: sum1 2 3 total "
        <<irf.sum1[i_E]<<" "<<irf.sum2[i_E]<<" "<<irf.sum3[i_E]
        <<" "<<irf.sum1[i_E]+irf.sum2[i_E]+irf.sum3[i_E]
        <<"  compton fraction= "<< compton_fraction[i_E]
        <<endl;

  cout<<endl;

  //irf.matrix.print();
  //////////////////////////////////////////////////////////////////////////////////

  n_E_in = n_E_in_;
  n_E_out= n_E_out_;

  E_in     =new double[n_E_in];
  E_out_lo =new double[n_E_out];
  E_out_hi =new double[n_E_out];

  for (ii_E=0;ii_E<n_E_in ;ii_E++)    E_in[ii_E] =  E_in_[ii_E];
  for (jj_E=0;jj_E<n_E_out;jj_E++)
  {
    E_out_lo[jj_E]=E_out_lo_[jj_E];
    E_out_hi[jj_E]=E_out_hi_[jj_E];
   }




  response= new double[n_E_in*n_E_out];

  for (i=0;i<n_E_in*n_E_out;i++)response[i]=0.;


  for (ii_E=0;ii_E<n_E_in;ii_E++)
  {

    for(i_E=0;i_E<rmf.n_E;i_E++)if (rmf.E_in[i_E]>E_in[ii_E] ) break;
    if(i_E>0)i_E--;
    if(debug==1)
    cout<<"E_in= "<<E_in[ii_E]<<" rmf.E_in="<<rmf.E_in[i_E]<<endl;


    

    for (jj_E=0;jj_E<n_E_out;jj_E++)
   {
     if(debug==1)
     cout<<"E_in E_out_lo E_out_hi "<<E_in[ii_E]<<" "<<E_out_lo[jj_E]<<" "<<E_out_hi[jj_E]<<endl;

                                                                     work1=0.0;
       if(E_in[ii_E]>E_out_lo[jj_E] && E_in[ii_E]<=E_out_hi[jj_E]  ) work1=1.0;// probability  for type 1 = photopeak events

       //cout<<" work1="<<work1      ;

      

       //cout<<"### i_E="<<i_E<<" irf.n_E="<<irf.n_E<<" irf.sum1="<<irf.sum1[i_E]<<endl  ;
       //cout<<" work1="<<work1  

      if (E_out_lo[jj_E]<=E_in[ii_E])
      {
      

       work2=0.;work3=0.;

       for (j_E=0;j_E<rmf.n_E;j_E++)
       {


	 if(E_out_lo[jj_E]<rmf.E_hi[j_E]   && E_out_hi[jj_E]>rmf.E_lo[j_E])
	 {

	   //               Eout        Eout
	   //                lo          hi 
	   //                |+++++++++++|
	   //               Elo  Ehi
	   //                |####|<<<< overlap range   
	   //            |        |      
	   //            |        |
	   //          rmf       rmf  
	   //           lo        hi

	  Elo=max(E_out_lo[jj_E],rmf.E_lo[j_E]);
 	  Ehi=min(E_out_hi[jj_E],rmf.E_hi[j_E]);

          if (debug==1)
	  cout<<"E_in E_out_lo E_out_hi rmf_E_low rmf_E_hi Elo Ehi "<<E_in[ii_E]<<" "<<E_out_lo[jj_E]<<" "<<E_out_hi[jj_E]
	      <<" "<<rmf.E_lo[j_E]<<" "<<rmf.E_hi[j_E]<<" "<<Elo<<" "<<Ehi<<endl;

	  if(Ehi>Elo)
	  {
           work22=rmf.matrix2[i_E*rmf.n_E +j_E]*(Ehi-Elo)/(rmf.E_hi[j_E]-rmf.E_lo[j_E] ); // probability per keV for type 2 events
           if(debug==1)cout<<" work22="<<work22      ;
           work2+=work22;

           work33=rmf.matrix3[i_E*rmf.n_E +j_E]*(Ehi-Elo)/(rmf.E_hi[j_E]-rmf.E_lo[j_E] ); // probability per keV for type 3 events
           if(debug==1)cout<<" work33="<<work33      ;
           work3+= work33;

        
	    }//if Ehi>Elo

	  if(i_E<=rmf.n_E-2)
	  {
	  irf_sum1_interp= irf.sum1[i_E] + (E_in[ii_E]-rmf.E_in[i_E]) / ( rmf.E_in[i_E+1]-rmf.E_in[i_E] ) * ( irf.sum1[i_E+1]- irf.sum1[i_E] );
	  irf_sum2_interp= irf.sum2[i_E] + (E_in[ii_E]-rmf.E_in[i_E]) / ( rmf.E_in[i_E+1]-rmf.E_in[i_E] ) * ( irf.sum2[i_E+1]- irf.sum2[i_E] );
	  irf_sum3_interp= irf.sum3[i_E] + (E_in[ii_E]-rmf.E_in[i_E]) / ( rmf.E_in[i_E+1]-rmf.E_in[i_E] ) * ( irf.sum3[i_E+1]- irf.sum3[i_E] );
	  }

	  if(E_in[ii_E]<=rmf.E_in[0])
	  {
	   irf_sum1_interp= irf.sum1[0];
	   irf_sum2_interp= irf.sum2[0];
	   irf_sum3_interp= irf.sum3[0];
	   }

	  if( i_E>rmf.n_E-2 )
	  {
	   irf_sum1_interp= irf.sum1[rmf.n_E-1 ];
	   irf_sum2_interp= irf.sum2[rmf.n_E-1 ];
	   irf_sum3_interp= irf.sum3[rmf.n_E-1 ];
	  }

	  // irf at output energy lo
          for(jjj_E=0;jjj_E<rmf.n_E;jjj_E++)if (rmf.E_in[jjj_E]>E_out_lo[jj_E] ) break;
          if(jjj_E>0)jjj_E--;
 

	  if(jjj_E<=rmf.n_E-2)
          irf_sum1_interp_prime_lo= irf.sum1[jjj_E] + (E_out_lo[jj_E]-rmf.E_in[jjj_E]) / ( rmf.E_in[jjj_E+1]- rmf.E_in[jjj_E] )
                                                                                       * ( irf.sum1[jjj_E+1]- irf.sum1[jjj_E] );

          if(E_out_lo[jj_E]< rmf.E_in[0]) irf_sum1_interp_prime_lo= irf.sum1[0];
	  if(        jjj_E > rmf.n_E-2  ) irf_sum1_interp_prime_lo= irf.sum1[rmf.n_E-1 ];

          if(debug==1)
	    cout<<"interpolating irf at output energy: E_out_lo= "<<E_out_lo[jj_E]<<" rmf.E_in="<<rmf.E_in[jjj_E]
                <<" irf_sum1_interp_prime_lo="<< irf_sum1_interp_prime_lo <<endl;

	  // irf at output energy hi
          for(jjj_E=0;jjj_E<rmf.n_E;jjj_E++)if (rmf.E_in[jjj_E]>E_out_hi[jj_E] ) break;
          if(jjj_E>0)jjj_E--;
 

	  if(jjj_E<=rmf.n_E-2)
          irf_sum1_interp_prime_hi= irf.sum1[jjj_E] + (E_out_hi[jj_E]-rmf.E_in[jjj_E]) / ( rmf.E_in[jjj_E+1]- rmf.E_in[jjj_E] )
                                                                                       * ( irf.sum1[jjj_E+1]- irf.sum1[jjj_E] );

          if(E_out_hi[jj_E]< rmf.E_in[0]) irf_sum1_interp_prime_hi= irf.sum1[0];
	  if(        jjj_E > rmf.n_E-2  ) irf_sum1_interp_prime_hi= irf.sum1[rmf.n_E-1 ];


          if(debug==1)
	    cout<<"interpolating irf at output energy: E_out_hi= "<<E_out_hi[jj_E]<<" rmf.E_in="<<rmf.E_in[jjj_E]
                <<" irf_sum1_interp_prime_hi="<< irf_sum1_interp_prime_hi <<endl;


	  irf_sum1_interp_prime = ( irf_sum1_interp_prime_lo + irf_sum1_interp_prime_hi )/2.0 ;

          if(debug==1)
	    cout<<"interpolated  irf at output energy: E_out_lo= "<<E_out_lo[jj_E]<<" E_out_hi="<<E_out_hi[ jj_E]
                <<" irf_sum1_interp_prime_lo="<< irf_sum1_interp_prime_lo      
                <<" irf_sum1_interp_prime_hi="<< irf_sum1_interp_prime_hi      
                <<" irf_sum1_interp_prime   ="<< irf_sum1_interp_prime    <<endl;

          if(debug==1)
	  cout<<"E_in= "<<E_in[ii_E] <<"  irf_sum1_interp="<<irf_sum1_interp <<" irf_sum2_interp="<<irf_sum2_interp <<" irf_sum3_interp="<<irf_sum3_interp
	      <<"  (2+3)/(1+2+3)="    <<( irf_sum2_interp+irf_sum3_interp)/(irf_sum1_interp+ irf_sum2_interp+ irf_sum3_interp)
              <<"  irf_sum1_interp_prime="<< irf_sum1_interp_prime
              <<endl;



          work4 = work1 * irf_sum1_interp + work2 * irf_sum2_interp + work3 * irf_sum3_interp; // probability * IRF for output bin on interpolated grid
//        work5=          irf_sum1_interp +         irf_sum2_interp +         irf_sum3_interp;

          work5=          irf_sum1_interp_prime; // correct formulation

	  /*
          work4=work1 * irf.sum1[i_E] +  work2 * irf.sum2[i_E] + work3 * irf.sum3[i_E]; // probability * IRF for output bin on interpolated grid

          work5= irf.sum1[i_E]+ irf.sum2[i_E]+ irf.sum3[i_E];
	  */




          response[ii_E * n_E_out + jj_E]=work4/work5;

	  //     cout<<" work4="<<work4<<endl;
 
      

       }// if E_out_lo<rmf.E_hi

      }//if E_out_lo<E_in

      }//j_E

      // cout<<"ii_E="<<ii_E<<" jj_E="<<jj_E<<" response= "<< response[ii_E * n_E_out + jj_E]<<endl;

    }//jj_E


    //print();

    //    for (jj_E=0;jj_E<n_E_out;jj_E++) response[ii_E * n_E_out + jj_E]/=work5; // normalize to 1; not required

    

    }//ii_E


  print();

  return 0;
}
/////////////////////////////////////////////////////////////////////////////
 void SPIERSP::print()
{
  double total;

  cout<<endl<<"============ SPIERSP::print ==============="<<endl<<endl;
  cout<<" E_out_lo:";
  for (jj_E=0;jj_E<n_E_out;jj_E++)    cout<< E_out_lo[jj_E]<<"    "; cout<<endl;
  cout<<" E_out_hi:";
  for (jj_E=0;jj_E<n_E_out;jj_E++)    cout<< E_out_hi[jj_E]<<"    "; cout<<endl;

  cout<<" E_in:"<<endl;

  for (ii_E=0;ii_E<n_E_in;ii_E++)
  {
   
    cout<< E_in[ii_E]<<" keV: ";

    total=0;
    for (jj_E=0;jj_E<n_E_out;jj_E++)
   {

   
     //     if(response[ii_E * n_E_out + jj_E] >0.)
    cout<< response[ii_E * n_E_out + jj_E]<<" ";
    total+= response[ii_E * n_E_out + jj_E];
   }
    cout<<"   total= "<<total<<endl;
  }
  cout<<endl<<"==========================================="<<endl<<endl;
}
