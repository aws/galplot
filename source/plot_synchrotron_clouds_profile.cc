#include"Galplot.h"                  
#include"galprop_classes.h"
#include"galprop.h"
#include"constants.h"
#include"fitsio.h"


int Galplot::plot_synchrotron_clouds_profile(int longprof, int latprof, int frequency)
{
   cout<<" >>>> plot_synchrotron_clouds_profile    "<<endl;
   int status=0;

   //return status;// at first, to remove

   int options=0; // plot galprop models

 int free_free_WMAP_template=0;//AWS20110906 0=use galprop model, 1=use WMAP template


   int ip,ip1,ip2,ip3,ip4,ip5,ip6,ip7,ip8;
   int ips1,ips2,ips3,ips4,ips5,ips6,ips7,ips8,ips9,ips10,ips11,ips12,ips13,ips14,ips15,ips16;//AWS20110426
   int survey_profile_number,index_number;

int i_comp,i_lat,i_long;
int ii_long,ii_lat;
int il_min,il_max,ib_min,ib_max;
int il_min1,il_max1,ib_min1,ib_max1;
int il_min2,il_max2,ib_min2,ib_max2;


double long_min;
double l,b,l_off;
double ic_total;
double ic_aniso_total;//AWS20060907
double isotropic_intensity;

char name[1000],canvastitle[1000], workstring1[100],workstring2[100],workstring3[100],workstring4[100],workstring5[100]; //AWS20070611
char psfile[400];
char giffile[400];

 double pi=acos(-1.0); //AWS20110425 before was from healpix constants.h

 double k=1.38e-16;                // Boltzmann's constant, erg/K
 double I_to_Tb=C*C/ (2.* k);      // intensity to brighness temp: Tb= I_to_Tb * I/nu^2;
 double factor;
 double background1,background2;
 int   I_or_Tb        =2;  // 1=intensity, 2=brightness temp.
 int   background_mode=2;  // 0 no background correction, 1=add to model, 2=subtract from data
//     background_mode=0;  // 0 no background correction, 1=add to model, 2=subtract from data    AWS20100617

 int    n_survey_profiles =12; //AWS20070606
        n_survey_profiles =16; //AWS20110426 added WMAP polarized

 double EGB____10MHz = 0.0;
 double EGB____22MHz = 0.0;

 // Guzman etal 2011 A&A 525 A138
 double EGB____45MHz = 550.0; //AWS20110223

 double EGB___150MHz = 0.0;

  // Reich&Reich 1988 A&A Supp 74, 7 Table VII: Toff=3.7+-0.85K
 double EGB___408MHz = 3.7; 

 //  Reich&Reich 1988 A&A Supp 74, 7,  Table VII: Toff=3.6+-0.15K (used here), cf Lawson et al. 1987 MNRAS 225,307: Toff=4.14K
 double EGB___820MHz = 3.6; //AWS20070604

  // Reich&Reich 1988 A&A Supp 74, 7 Table VII: Toff=2.8+-0.03K 
 double EGB__1420MHz = 2.8; 

 double EGB__2326MHz = 0.0; // seems to have CMB subtracted, needs further study
 
 double EGB_22800MHz = 0.0; // by definition for WMAP datasets
 double EGB_33000MHz = 0.0;
 double EGB_41000MHz = 0.0;
 double EGB_61000MHz = 0.0;
 double EGB_94000MHz = 0.0;



 Distribution *synchrotron_skymap1;     
 Distribution *synchrotron_skymap2;
 Distribution *synchrotron_skymap3;
 Distribution *synchrotron_skymap4;
 Distribution *synchrotron_skymap5;     
 Distribution *synchrotron_skymap6;
 Distribution *synchrotron_skymap7;
 Distribution *synchrotron_skymap8;
 Distribution *synchrotron_skymap9;
 Distribution *synchrotron_skymap10;
 Distribution *synchrotron_skymap11;
 Distribution *synchrotron_skymap12;

 Distribution *synchrotron_skymap13;//AWS20110426
 Distribution *synchrotron_skymap14;//AWS20110426
 Distribution *synchrotron_skymap15;//AWS20110426
 Distribution *synchrotron_skymap16;//AWS20110426

 // assign the survey numbering

 synchrotron_skymap1 = &data.synchrotron_skymap____22MHz;
 synchrotron_skymap2 = &data.synchrotron_skymap____45MHz;
 synchrotron_skymap3 = &data.synchrotron_skymap___150MHz;
 synchrotron_skymap4 = &data.synchrotron_skymap___408MHz;
 synchrotron_skymap5 = &data.synchrotron_skymap___820MHz;   //AWS20070604
 synchrotron_skymap6 = &data.synchrotron_skymap__1420MHz;
 synchrotron_skymap7 = &data.synchrotron_skymap__2326MHz;
 synchrotron_skymap8 = &data.synchrotron_skymap_22800MHz;
 synchrotron_skymap9 = &data.synchrotron_skymap_33000MHz;   //AWS20070606
 synchrotron_skymap10= &data.synchrotron_skymap_41000MHz;   //AWS20070606
 synchrotron_skymap11= &data.synchrotron_skymap_61000MHz;   //AWS20070606
 synchrotron_skymap12= &data.synchrotron_skymap_94000MHz;   //AWS20070606

 synchrotron_skymap13= &data.synchrotron_Q_skymap_22800MHz;//AWS20110426
 synchrotron_skymap14= &data.synchrotron_U_skymap_22800MHz;//AWS20110426
 synchrotron_skymap15= &data.synchrotron_P_skymap_22800MHz;//AWS20110426
 synchrotron_skymap16= &data.synchrotron_I_skymap_22800MHz;//AWS20110426

 double nu1_survey=    22.e6;                           
 double nu2_survey=    45.e6;                            
 double nu3_survey=   150.e6;  
 double nu4_survey=   408.e6; // Haslam et al 408 MHz survey //AWS20070524
 double nu5_survey=   820.e6;                                //AWS20070604
 double nu6_survey=  1420.e6;                                //AWS20070524
 double nu7_survey=  2326.e6;                                //AWS20070524
 double nu8_survey =22800.e6;                                //AWS20070525
 double nu9_survey =33000.e6;                                //AWS20070606
 double nu10_survey=41000.e6;                                //AWS20070606
 double nu11_survey=61000.e6;                                //AWS20070606
 double nu12_survey=94000.e6;                                //AWS20070606

 //WMAP 7 year polarized QUPI
 double nu13_survey =22800.e6;                                //AWS20110426
 double nu14_survey =22800.e6;                                //AWS20110426
 double nu15_survey =22800.e6;                                //AWS20110426
 double nu16_survey =22800.e6;                                //AWS20110426                         

 double EGB1=  EGB____22MHz;
 double EGB2=  EGB____45MHz;
 double EGB3=  EGB___150MHz;
 double EGB4=  EGB___408MHz;                                 //AWS20070529
 double EGB5=  EGB___820MHz;                                 //AWS20070604
 double EGB6=  EGB__1420MHz;
 double EGB7=  EGB__2326MHz;
 double EGB8=  EGB_22800MHz;
 double EGB9=  EGB_33000MHz;                                 //AWS20070606
 double EGB10= EGB_41000MHz;                                 //AWS20070606
 double EGB11= EGB_61000MHz;                                 //AWS20070606
 double EGB12= EGB_94000MHz;                                 //AWS20070606

 double EGB13=  EGB_22800MHz;                                //AWS20110426
 double EGB14=  EGB_22800MHz;                                //AWS20110426
 double EGB15=  EGB_22800MHz;                                //AWS20110426
 double EGB16=  EGB_22800MHz;                                //AWS20110426


 

  status=0;
  if (galplotdef.sync_index_n_nu!=4)
  {cout<<"plot_synchtrotron_profile: need exactly 4 index pairs ! got " <<galplotdef.sync_index_n_nu <<endl; status=-1;return status;}// AWS20070524


 // use values from galplotdef (given in MHz)                  AWS20070523
 double nu1_spectral_index= galplotdef.sync_index_nu[0]*1e6; //AWS20070523
 double nu2_spectral_index= galplotdef.sync_index_nu[1]*1e6; //AWS20070523
 double nu3_spectral_index= galplotdef.sync_index_nu[2]*1e6; //AWS20070523
 double nu4_spectral_index= galplotdef.sync_index_nu[3]*1e6; //AWS20070523
 double nu5_spectral_index= galplotdef.sync_index_nu[4]*1e6; //AWS20070523
 double nu6_spectral_index= galplotdef.sync_index_nu[5]*1e6; //AWS20070523
 double nu7_spectral_index= galplotdef.sync_index_nu[6]*1e6; //AWS20070523
 double nu8_spectral_index= galplotdef.sync_index_nu[7]*1e6; //AWS20070523

 double spectral_index12;
 double spectral_index34;
 double spectral_index56;
 double spectral_index78;

 double spectral_index_minimum=2.4;// for spectral index plots
 double spectral_index_maximum=3.2;// for spectral index plots

TCanvas *c1    ;

TH2F     *synch_map ;                                         //AWS20070525
TH2F     *synch_map1;                                         //AWS20070524
TH2F     *synch_map2;                                         //AWS20070524
TH2F     *synch_map3;                                         //AWS20070525
TH2F     *synch_map4;                                         //AWS20070525
TH2F     *synch_map5;                                   
TH2F     *synch_map6;                                   
TH2F     *synch_map7;                                   
TH2F     *synch_map8;                                   
TH2F     *synch_map9;                                        //AWS20070606
TH2F     *synch_map10;
TH2F     *synch_map11;
TH2F     *synch_map12;

// WMAP 7 year 23 GHz polarized Q U P I
TH2F     *synch_map13;                                        //AWS20110426
TH2F     *synch_map14;
TH2F     *synch_map15;
TH2F     *synch_map16;


// free-free model                                              AWS20110624
TH2F     *free_free_map;                                      //AWS20110624
TH2F     *free_free_map1;                                     //AWS20110624
TH2F     *free_free_map2;                                     //AWS20110624
TH2F     *free_free_map3;                                     //AWS20110624
TH2F     *free_free_map4;                                     //AWS20110624
TH2F     *free_free_map5;                                     //AWS20110624
TH2F     *free_free_map6;                                     //AWS20110624
TH2F     *free_free_map7;                                     //AWS20110624
TH2F     *free_free_map8;                                     //AWS20110624
TH2F     *free_free_map9;                                     //AWS20110624
TH2F     *free_free_map10;                                    //AWS20110624
TH2F     *free_free_map11;                                    //AWS20110624
TH2F     *free_free_map12;                                    //AWS20110624
TH2F     *free_free_map13;                                    //AWS20110624
TH2F     *free_free_map14;                                    //AWS20110624
TH2F     *free_free_map15;                                    //AWS20110624
TH2F     *free_free_map16;                                    //AWS20110624

// synch plus free-free model                                   AWS20110624
TH2F     *synch_free_free_map;                                //AWS20110624
TH2F     *synch_free_free_map1;                               //AWS20110624
TH2F     *synch_free_free_map2;                               //AWS20110624
TH2F     *synch_free_free_map3;                               //AWS20110624
TH2F     *synch_free_free_map4;                               //AWS20110624
TH2F     *synch_free_free_map5;                               //AWS20110624
TH2F     *synch_free_free_map6;                               //AWS20110624
TH2F     *synch_free_free_map7;                               //AWS20110624
TH2F     *synch_free_free_map8;                               //AWS20110624
TH2F     *synch_free_free_map9;                               //AWS20110624
TH2F     *synch_free_free_map10;                              //AWS20110624
TH2F     *synch_free_free_map11;                              //AWS20110624
TH2F     *synch_free_free_map12;                              //AWS20110624
TH2F     *synch_free_free_map13;                              //AWS20110624
TH2F     *synch_free_free_map14;                              //AWS20110624
TH2F     *synch_free_free_map15;                              //AWS20110624
TH2F     *synch_free_free_map16;                              //AWS20110624

// thermal dust model, only for WMAP frequencies QUPI
TH2F     *thermal_dust_map;                                   //AWS20120629 
TH2F     *thermal_dust_map13;                                 //AWS20120629 
TH2F     *thermal_dust_map14;                                 //AWS20120629 
TH2F     *thermal_dust_map15;                                 //AWS20120629 
TH2F     *thermal_dust_map16;                                 //AWS20120629 

// spinning dust model, only for WMAP I (since unpolarized)
TH2F     *spinning_dust_map;                                  //AWS20120629 
TH2F     *spinning_dust_map16;                                //AWS20120629 

//       survey data
TH2F    *survey_map ;                                         //AWS20070525
TH2F    *survey_map1;                                         //AWS20070524
TH2F    *survey_map2;                                         //AWS20070524
TH2F    *survey_map3;                                         //AWS20070525
TH2F    *survey_map4;                                         //AWS20070525
TH2F    *survey_map5;  
TH2F    *survey_map6;  
TH2F    *survey_map7;  

// WMAP 23-94 GHz, synchrotron based on Miville-Deschenes or WMAP template
TH2F    *survey_map8;  
TH2F    *survey_map9;                                        //AWS20070606
TH2F    *survey_map10;                                       //AWS20070606
TH2F    *survey_map11;                                       //AWS20070606
TH2F    *survey_map12;                                       //AWS20070606

// WMAP 7 year 23 GHz polarized Q U P I
TH2F     *survey_map13;                                      //AWS20110426
TH2F     *survey_map14;
TH2F     *survey_map15;
TH2F     *survey_map16;


TH2F    *index_map;
TH2F    *index_map12;
TH2F    *index_map34;
TH2F    *index_map56;
TH2F    *index_map78;

TH1D *profile;
TH1D *survey_profile;
TH1D *scale_profile; // to choose profile for scaling plot   //AWS20120430
TH1D *free_free_profile,*synch_free_free_profile;            //AWS20110624
TH1D *index_profile;
TH1D *thermal_dust_profile, *spinning_dust_profile;          //AWS20120629


TText *text; //AWS20070525

FITS spectral_index_map12;
FITS spectral_index_map34;
FITS spectral_index_map56;
FITS spectral_index_map78;


 int ncolors=0; int *colors=0;// default palette of 50 colours AWS20050915
gStyle->SetPalette(ncolors,colors);

  //====== see HowTo Style: and do before creating canvas
 
 
   TStyle *plain  = new TStyle("Plain","Plain Style (no colors/fill areas)"); 

   
   plain->SetCanvasBorderMode(0);
   plain->SetPadBorderMode(0);
   plain->SetPadColor(0);
   plain->SetCanvasColor(0);
   plain->SetTitleColor(0);
   plain->SetStatColor(0);
   gROOT->SetStyle("Plain");



  

 // names must be different or canvas disappears

  if(longprof)sprintf(name,"synchrotron_longitude_profile_"); //AWS20070608
  if( latprof)sprintf(name,"synchrotron_latitude_profile_");   //AWS20070608
  strcpy(canvastitle,"galdef_");
  strcat(canvastitle,galdef.galdef_ID);
  strcat(canvastitle,name);

  //----------------------------------------------------------------------------------------


  synch_map1=new TH2F("synchrotron skymap1",canvastitle,                                         //AWS20070524
                  galaxy.n_long,       -180,           180.,
                  galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1));

  survey_map1=new TH2F("survey     skymap1",canvastitle,                                         //AWS20070524
                  galaxy.n_long,       -180,           180.,
                  galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1));

  synch_map2=new TH2F("synchrotron skymap2",canvastitle,                                         //AWS20070524
                  galaxy.n_long,       -180,           180.,
                  galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1));

  survey_map2=new TH2F("survey     skymap2",canvastitle,                                         //AWS20070524
                  galaxy.n_long,       -180,           180.,
                  galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1));


  synch_map3=new TH2F("synchrotron skymap3",canvastitle,                                         //AWS20070524
                  galaxy.n_long,       -180,           180.,
                  galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1));

  survey_map3=new TH2F("survey     skymap3",canvastitle,                                         //AWS20070524
                  galaxy.n_long,       -180,           180.,
                  galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1));

  synch_map4=new TH2F("synchrotron skymap4",canvastitle,                                         //AWS20070524
                  galaxy.n_long,       -180,           180.,
                  galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1));

  survey_map4=new TH2F("survey     skymap4",canvastitle,                                         //AWS20070524
                  galaxy.n_long,       -180,           180.,
                  galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1));


  synch_map5=new TH2F("synchrotron skymap5",canvastitle,                                         //AWS20070524
                  galaxy.n_long,       -180,           180.,
                  galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1));

  survey_map5=new TH2F("survey     skymap5",canvastitle,                                         //AWS20070524
                  galaxy.n_long,       -180,           180.,
                  galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1));



  synch_map6=new TH2F("synchrotron skymap6",canvastitle,                                         //AWS20070524
                  galaxy.n_long,       -180,           180.,
                  galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1));

  survey_map6=new TH2F("survey     skymap6",canvastitle,                                         //AWS20070524
                  galaxy.n_long,       -180,           180.,
                  galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1));


  synch_map7=new TH2F("synchrotron skymap7",canvastitle,                                         //AWS20070524
                  galaxy.n_long,       -180,           180.,
                  galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1));

  survey_map7=new TH2F("survey     skymap7",canvastitle,                                         //AWS20070524
                  galaxy.n_long,       -180,           180.,
                  galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1));

  synch_map8=new TH2F("synchrotron skymap8",canvastitle,                                         //AWS20070524
                  galaxy.n_long,       -180,           180.,
                  galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1));

  survey_map8=new TH2F("survey     skymap8",canvastitle,                                         //AWS20070524
                  galaxy.n_long,       -180,           180.,
                  galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1));


  synch_map9=new TH2F("synchrotron skymap9",canvastitle,                                         //AWS20070524
                  galaxy.n_long,       -180,           180.,
                  galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1));

  survey_map9=new TH2F("survey     skymap9",canvastitle,                                         //AWS20070524
                  galaxy.n_long,       -180,           180.,
                  galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1));

  synch_map10=new TH2F("synchrotron skymap10",canvastitle,                                         //AWS20070524
                  galaxy.n_long,       -180,           180.,
                  galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1));

  survey_map10=new TH2F("survey     skymap10",canvastitle,                                         //AWS20070524
                  galaxy.n_long,       -180,           180.,
                  galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1));


  synch_map11=new TH2F("synchrotron skymap11",canvastitle,                                         //AWS20070524
                  galaxy.n_long,       -180,           180.,
                  galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1));

  survey_map11=new TH2F("survey     skymap11",canvastitle,                                         //AWS20070524
                  galaxy.n_long,       -180,           180.,
                  galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1));


  synch_map12=new TH2F("synchrotron skymap12",canvastitle,                                         //AWS20070524
                  galaxy.n_long,       -180,           180.,
                  galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1));

  survey_map12=new TH2F("survey     skymap12",canvastitle,                                         //AWS20070524
                  galaxy.n_long,       -180,           180.,
                  galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1));




  synch_map13=new TH2F("synchrotron skymap13",canvastitle,                                         //AWS20110426
                  galaxy.n_long,       -180,           180.,
                  galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1));

  survey_map13=new TH2F("survey     skymap13",canvastitle,                                         //AWS20110426
                  galaxy.n_long,       -180,           180.,
                  galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1));

  synch_map14=new TH2F("synchrotron skymap14",canvastitle,                                         //AWS20110426
                  galaxy.n_long,       -180,           180.,
                  galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1));

  survey_map14=new TH2F("survey     skymap14",canvastitle,                                         //AWS20110426
                  galaxy.n_long,       -180,           180.,
                  galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1));


  synch_map15=new TH2F("synchrotron skymap15",canvastitle,                                         //AWS20110426
                  galaxy.n_long,       -180,           180.,
                  galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1));

  survey_map15=new TH2F("survey     skymap15",canvastitle,                                         //AWS20110426
                  galaxy.n_long,       -180,           180.,
                  galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1));


  synch_map16=new TH2F("synchrotron skymap16",canvastitle,                                         //AWS20110426
                  galaxy.n_long,       -180,           180.,
                  galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1));

  survey_map16=new TH2F("survey     skymap16",canvastitle,                                         //AWS20110426
                  galaxy.n_long,       -180,           180.,
                  galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1));


  //--------------------------------------------------------------
  free_free_map1 =new TH2F("free_free skymap1 ",canvastitle,                                         //AWS20110624
                  galaxy.n_long,       -180,           180.,
                  galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1));

  free_free_map2 =new TH2F("free_free skymap2 ",canvastitle,                                         //AWS20110624
                  galaxy.n_long,       -180,           180.,
                  galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1));

  free_free_map3 =new TH2F("free_free skymap3 ",canvastitle,                                         //AWS20110624
                  galaxy.n_long,       -180,           180.,
                  galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1));

  free_free_map4 =new TH2F("free_free skymap4 ",canvastitle,                                         //AWS20110624
                  galaxy.n_long,       -180,           180.,
                  galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1));

  free_free_map5 =new TH2F("free_free skymap5 ",canvastitle,                                         //AWS20110624
                  galaxy.n_long,       -180,           180.,
                  galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1));

  free_free_map6 =new TH2F("free_free skymap6 ",canvastitle,                                         //AWS20110624
                  galaxy.n_long,       -180,           180.,
                  galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1));

  free_free_map7 =new TH2F("free_free skymap7 ",canvastitle,                                         //AWS20110624
                  galaxy.n_long,       -180,           180.,
                  galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1));

  free_free_map8 =new TH2F("free_free skymap8 ",canvastitle,                                         //AWS20110624
                  galaxy.n_long,       -180,           180.,
                  galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1));

  free_free_map9 =new TH2F("free_free skymap9 ",canvastitle,                                         //AWS20110624
                  galaxy.n_long,       -180,           180.,
                  galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1));

  free_free_map10=new TH2F("free_free skymap10",canvastitle,                                         //AWS20110624
                  galaxy.n_long,       -180,           180.,
                  galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1));

  free_free_map11=new TH2F("free_free skymap11",canvastitle,                                         //AWS20110624
                  galaxy.n_long,       -180,           180.,
                  galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1));

  free_free_map12=new TH2F("free_free skymap12",canvastitle,                                         //AWS20110624
                  galaxy.n_long,       -180,           180.,
                  galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1));

  free_free_map13=new TH2F("free_free skymap13",canvastitle,                                         //AWS20110624
                  galaxy.n_long,       -180,           180.,
                  galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1));

  free_free_map14=new TH2F("free_free skymap14",canvastitle,                                         //AWS20110624
                  galaxy.n_long,       -180,           180.,
                  galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1));

  free_free_map15=new TH2F("free_free skymap15",canvastitle,                                         //AWS20110624
                  galaxy.n_long,       -180,           180.,
                  galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1));

  free_free_map16=new TH2F("free_free skymap16",canvastitle,                                         //AWS20110624
                  galaxy.n_long,       -180,           180.,
                  galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1));



  synch_free_free_map1 =new TH2F("synch_free_free skymap1 ",canvastitle,                                         //AWS20110624
                  galaxy.n_long,       -180,           180.,
                  galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1));

  synch_free_free_map2 =new TH2F("synch_free_free skymap2 ",canvastitle,                                         //AWS20110624
                  galaxy.n_long,       -180,           180.,
                  galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1));

  synch_free_free_map3 =new TH2F("synch_free_free skymap3 ",canvastitle,                                         //AWS20110624
                  galaxy.n_long,       -180,           180.,
                  galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1));

  synch_free_free_map4 =new TH2F("synch_free_free skymap4 ",canvastitle,                                         //AWS20110624
                  galaxy.n_long,       -180,           180.,
                  galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1));

  synch_free_free_map5 =new TH2F("synch_free_free skymap5 ",canvastitle,                                         //AWS20110624
                  galaxy.n_long,       -180,           180.,
                  galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1));

  synch_free_free_map6 =new TH2F("synch_free_free skymap6 ",canvastitle,                                         //AWS20110624
                  galaxy.n_long,       -180,           180.,
                  galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1));

  synch_free_free_map7 =new TH2F("synch_free_free skymap7 ",canvastitle,                                         //AWS20110624
                  galaxy.n_long,       -180,           180.,
                  galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1));

  synch_free_free_map8 =new TH2F("synch_free_free skymap8 ",canvastitle,                                         //AWS20110624
                  galaxy.n_long,       -180,           180.,
                  galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1));

  synch_free_free_map9 =new TH2F("synch_free_free skymap9 ",canvastitle,                                         //AWS20110624
                  galaxy.n_long,       -180,           180.,
                  galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1));

  synch_free_free_map10=new TH2F("synch_free_free skymap10",canvastitle,                                         //AWS20110624
                  galaxy.n_long,       -180,           180.,
                  galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1));

  synch_free_free_map11=new TH2F("synch_free_free skymap11",canvastitle,                                         //AWS20110624
                  galaxy.n_long,       -180,           180.,
                  galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1));

  synch_free_free_map12=new TH2F("synch_free_free skymap12",canvastitle,                                         //AWS20110624
                  galaxy.n_long,       -180,           180.,
                  galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1));

  synch_free_free_map13=new TH2F("synch_free_free skymap13",canvastitle,                                         //AWS20110624
                  galaxy.n_long,       -180,           180.,
                  galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1));

  synch_free_free_map14=new TH2F("synch_free_free skymap14",canvastitle,                                         //AWS20110624
                  galaxy.n_long,       -180,           180.,
                  galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1));

  synch_free_free_map15=new TH2F("synch_free_free skymap15",canvastitle,                                         //AWS20110624
                  galaxy.n_long,       -180,           180.,
                  galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1));

  synch_free_free_map16=new TH2F("synch_free_free skymap16",canvastitle,                                         //AWS20110624
                  galaxy.n_long,       -180,           180.,
                  galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1));




     thermal_dust_map13=new TH2F("thermal dust    skymap13",canvastitle,                                       //AWS20120629
                  galaxy.n_long,       -180,           180.,
                  galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1));


     thermal_dust_map14=new TH2F("thermal dust    skymap14",canvastitle,                                       //AWS20120629
                  galaxy.n_long,       -180,           180.,
                  galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1));

     thermal_dust_map15=new TH2F("thermal dust    skymap15",canvastitle,                                       //AWS20120629
                  galaxy.n_long,       -180,           180.,
                  galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1));



     thermal_dust_map16=new TH2F("thermal dust    skymap16",canvastitle,                                       //AWS20120629
                  galaxy.n_long,       -180,           180.,
                  galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1));


   spinning_dust_map16=new TH2F("spinning dust   skymap16",canvastitle,                                       //AWS20120629
                  galaxy.n_long,       -180,           180.,
                  galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1));




  //--------------------------------------------------------------

    index_map=new TH2F("spectral index skymap",canvastitle,
                  galaxy.n_long,       -180,           180.,
                  galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1));

    index_map12=new TH2F("spectral index skymap12",canvastitle,                                   //AWS20070608
                  galaxy.n_long,       -180,           180.,
                  galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1));

    index_map34=new TH2F("spectral index skymap34",canvastitle,                                   //AWS20070608
                  galaxy.n_long,       -180,           180.,
                  galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1));


    index_map56=new TH2F("spectral index skymap56",canvastitle,                                   //AWS20070608
                  galaxy.n_long,       -180,           180.,
                  galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1));

    index_map78=new TH2F("spectral index skymap78",canvastitle,                                   //AWS20070608
                  galaxy.n_long,       -180,           180.,
                  galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1));

    //---------------------------------------------------------------

    spectral_index_map12.init(galaxy.n_long,galaxy.n_lat); 
    spectral_index_map12.CRVAL[0]=180.;
    spectral_index_map12.CRVAL[1]=galaxy.lat_min;
    spectral_index_map12.CDELT[0]=-galaxy.d_long;    
    spectral_index_map12.CDELT[1]=galaxy.d_lat;      
    spectral_index_map12.CRPIX[0]=0.;                
    spectral_index_map12.CRPIX[1]=0.;             

    spectral_index_map34.init(galaxy.n_long,galaxy.n_lat); 
    spectral_index_map34.CRVAL[0]=180.;
    spectral_index_map34.CRVAL[1]=galaxy.lat_min;
    spectral_index_map34.CDELT[0]=-galaxy.d_long;    
    spectral_index_map34.CDELT[1]=galaxy.d_lat;      
    spectral_index_map34.CRPIX[0]=0.;                
    spectral_index_map34.CRPIX[1]=0.;     


    spectral_index_map56.init(galaxy.n_long,galaxy.n_lat); 
    spectral_index_map56.CRVAL[0]=180.;
    spectral_index_map56.CRVAL[1]=galaxy.lat_min;
    spectral_index_map56.CDELT[0]=-galaxy.d_long;    
    spectral_index_map56.CDELT[1]=galaxy.d_lat;      
    spectral_index_map56.CRPIX[0]=0.;                
    spectral_index_map56.CRPIX[1]=0.;     

    spectral_index_map78.init(galaxy.n_long,galaxy.n_lat); 
    spectral_index_map78.CRVAL[0]=180.;
    spectral_index_map78.CRVAL[1]=galaxy.lat_min;
    spectral_index_map78.CDELT[0]=-galaxy.d_long;    
    spectral_index_map78.CDELT[1]=galaxy.d_lat;      
    spectral_index_map78.CRPIX[0]=0.;                
    spectral_index_map78.CRPIX[1]=0.;     


  ips1  =log10(nu1_survey        /galaxy.nu_synch[0])/log10(galaxy.nu_synch_factor);//AWS20070524
  ips2  =log10(nu2_survey        /galaxy.nu_synch[0])/log10(galaxy.nu_synch_factor);//AWS20070524
  ips3  =log10(nu3_survey        /galaxy.nu_synch[0])/log10(galaxy.nu_synch_factor);//AWS20070524
  ips4  =log10(nu4_survey        /galaxy.nu_synch[0])/log10(galaxy.nu_synch_factor);//AWS20070524
  ips5  =log10(nu5_survey        /galaxy.nu_synch[0])/log10(galaxy.nu_synch_factor);
  ips6  =log10(nu6_survey        /galaxy.nu_synch[0])/log10(galaxy.nu_synch_factor);
  ips7  =log10(nu7_survey        /galaxy.nu_synch[0])/log10(galaxy.nu_synch_factor);
  ips8  =log10(nu8_survey        /galaxy.nu_synch[0])/log10(galaxy.nu_synch_factor);
  ips9  =log10(nu9_survey        /galaxy.nu_synch[0])/log10(galaxy.nu_synch_factor);//AWS20070606
  ips10 =log10(nu10_survey       /galaxy.nu_synch[0])/log10(galaxy.nu_synch_factor);//AWS20070606
  ips11 =log10(nu11_survey       /galaxy.nu_synch[0])/log10(galaxy.nu_synch_factor);//AWS20070606
  ips12 =log10(nu12_survey       /galaxy.nu_synch[0])/log10(galaxy.nu_synch_factor);//AWS20070606

  ips13 =log10(nu13_survey       /galaxy.nu_synch[0])/log10(galaxy.nu_synch_factor);//AWS20110426
  ips14 =log10(nu14_survey       /galaxy.nu_synch[0])/log10(galaxy.nu_synch_factor);//AWS20110426
  ips15 =log10(nu15_survey       /galaxy.nu_synch[0])/log10(galaxy.nu_synch_factor);//AWS20110426
  ips16 =log10(nu16_survey       /galaxy.nu_synch[0])/log10(galaxy.nu_synch_factor);//AWS20110426

  // find the nearest frequency AWS20110427
  if( abs(nu1_survey -galaxy.nu_synch[ips1 +1]) < abs(nu1_survey -galaxy.nu_synch[ips1 ]) ) ips1 ++;
  if( abs(nu2_survey -galaxy.nu_synch[ips2 +1]) < abs(nu2_survey -galaxy.nu_synch[ips2 ]) ) ips2 ++;
  if( abs(nu3_survey -galaxy.nu_synch[ips3 +1]) < abs(nu3_survey -galaxy.nu_synch[ips3 ]) ) ips3 ++;
  if( abs(nu4_survey -galaxy.nu_synch[ips4 +1]) < abs(nu4_survey -galaxy.nu_synch[ips4 ]) ) ips4 ++;
  if( abs(nu5_survey -galaxy.nu_synch[ips5 +1]) < abs(nu5_survey -galaxy.nu_synch[ips5 ]) ) ips5 ++;
  if( abs(nu6_survey -galaxy.nu_synch[ips6 +1]) < abs(nu6_survey -galaxy.nu_synch[ips6 ]) ) ips6 ++;
  if( abs(nu7_survey -galaxy.nu_synch[ips7 +1]) < abs(nu7_survey -galaxy.nu_synch[ips7 ]) ) ips7 ++;
  if( abs(nu8_survey -galaxy.nu_synch[ips8 +1]) < abs(nu8_survey -galaxy.nu_synch[ips8 ]) ) ips8 ++;
  if( abs(nu9_survey -galaxy.nu_synch[ips9 +1]) < abs(nu9_survey -galaxy.nu_synch[ips9 ]) ) ips9 ++;
  if( abs(nu10_survey-galaxy.nu_synch[ips10+1]) < abs(nu10_survey-galaxy.nu_synch[ips10]) ) ips10++;
  if( abs(nu11_survey-galaxy.nu_synch[ips11+1]) < abs(nu11_survey-galaxy.nu_synch[ips11]) ) ips11++;
  if( abs(nu12_survey-galaxy.nu_synch[ips12+1]) < abs(nu12_survey-galaxy.nu_synch[ips12]) ) ips12++;
  if( abs(nu13_survey-galaxy.nu_synch[ips13+1]) < abs(nu13_survey-galaxy.nu_synch[ips13]) ) ips13++;
  if( abs(nu14_survey-galaxy.nu_synch[ips14+1]) < abs(nu14_survey-galaxy.nu_synch[ips14]) ) ips14++;
  if( abs(nu15_survey-galaxy.nu_synch[ips15+1]) < abs(nu15_survey-galaxy.nu_synch[ips15]) ) ips15++;
  if( abs(nu16_survey-galaxy.nu_synch[ips16+1]) < abs(nu16_survey-galaxy.nu_synch[ips16]) ) ips16++;

  ip1 =log10(nu1_spectral_index/galaxy.nu_synch[0])/log10(galaxy.nu_synch_factor);
  ip2 =log10(nu2_spectral_index/galaxy.nu_synch[0])/log10(galaxy.nu_synch_factor);
  ip3 =log10(nu3_spectral_index/galaxy.nu_synch[0])/log10(galaxy.nu_synch_factor);
  ip4 =log10(nu4_spectral_index/galaxy.nu_synch[0])/log10(galaxy.nu_synch_factor);
  ip5 =log10(nu5_spectral_index/galaxy.nu_synch[0])/log10(galaxy.nu_synch_factor);
  ip6 =log10(nu6_spectral_index/galaxy.nu_synch[0])/log10(galaxy.nu_synch_factor);
  ip7 =log10(nu7_spectral_index/galaxy.nu_synch[0])/log10(galaxy.nu_synch_factor);
  ip8 =log10(nu8_spectral_index/galaxy.nu_synch[0])/log10(galaxy.nu_synch_factor);


  // find the nearest frequency AWS20110427
  if( abs(nu1_spectral_index -galaxy.nu_synch[ip1 +1]) < abs(nu1_spectral_index -galaxy.nu_synch[ip1 ]) ) ip1 ++;
  if( abs(nu2_spectral_index -galaxy.nu_synch[ip2 +1]) < abs(nu2_spectral_index -galaxy.nu_synch[ip2 ]) ) ip2 ++;
  if( abs(nu3_spectral_index -galaxy.nu_synch[ip3 +1]) < abs(nu3_spectral_index -galaxy.nu_synch[ip3 ]) ) ip3 ++;
  if( abs(nu4_spectral_index -galaxy.nu_synch[ip4 +1]) < abs(nu4_spectral_index -galaxy.nu_synch[ip4 ]) ) ip4 ++;
  if( abs(nu5_spectral_index -galaxy.nu_synch[ip5 +1]) < abs(nu5_spectral_index -galaxy.nu_synch[ip5 ]) ) ip5 ++;
  if( abs(nu6_spectral_index -galaxy.nu_synch[ip6 +1]) < abs(nu6_spectral_index -galaxy.nu_synch[ip6 ]) ) ip6 ++;
  if( abs(nu7_spectral_index -galaxy.nu_synch[ip7 +1]) < abs(nu7_spectral_index -galaxy.nu_synch[ip7 ]) ) ip7 ++;
  if( abs(nu8_spectral_index -galaxy.nu_synch[ip8 +1]) < abs(nu8_spectral_index -galaxy.nu_synch[ip8 ]) ) ip8 ++;

  cout<<"nearest synch frequency to  "<<nu1_survey/1.e6<<" MHz survey in galprop skymap = "<<galaxy.nu_synch[ips1]/1.e6<<" MHz "<<endl;//AWS20070524
  cout<<"nearest synch frequency to  "<<nu2_survey/1.e6<<" MHz survey in galprop skymap = "<<galaxy.nu_synch[ips2]/1.e6<<" MHz "<<endl;//AWS20070524
  cout<<"nearest synch frequency to  "<<nu3_survey/1.e6<<" MHz survey in galprop skymap = "<<galaxy.nu_synch[ips3]/1.e6<<" MHz "<<endl;//AWS20070524
  cout<<"nearest synch frequency to  "<<nu4_survey/1.e6<<" MHz survey in galprop skymap = "<<galaxy.nu_synch[ips4]/1.e6<<" MHz "<<endl;//AWS20070525
  cout<<"nearest synch frequency to  "<<nu5_survey/1.e6<<" MHz survey in galprop skymap = "<<galaxy.nu_synch[ips5]/1.e6<<" MHz "<<endl;
  cout<<"nearest synch frequency to  "<<nu6_survey/1.e6<<" MHz survey in galprop skymap = "<<galaxy.nu_synch[ips6]/1.e6<<" MHz "<<endl;
  cout<<"nearest synch frequency to  "<<nu7_survey/1.e6<<" MHz survey in galprop skymap = "<<galaxy.nu_synch[ips7]/1.e6<<" MHz "<<endl;
  cout<<"nearest synch frequency to  "<<nu8_survey/1.e6<<" MHz survey in galprop skymap = "<<galaxy.nu_synch[ips8]/1.e6<<" MHz "<<endl;
  cout<<"nearest synch frequency to  "<<nu9_survey/1.e6<<" MHz survey in galprop skymap = "<<galaxy.nu_synch[ips9]/1.e6<<" MHz "<<endl;//AWS20070606
  cout<<"nearest synch frequency to "<<nu10_survey/1.e6<<" MHz survey in galprop skymap = "<<galaxy.nu_synch[ips10]/1.e6<<" MHz "<<endl;//AWS20070606
  cout<<"nearest synch frequency to "<<nu11_survey/1.e6<<" MHz survey in galprop skymap = "<<galaxy.nu_synch[ips11]/1.e6<<" MHz "<<endl;//AWS20070606
  cout<<"nearest synch frequency to "<<nu12_survey/1.e6<<" MHz survey in galprop skymap = "<<galaxy.nu_synch[ips12]/1.e6<<" MHz "<<endl;//AWS20070606

  cout<<"nearest synch frequency to "<<nu13_survey/1.e6<<" MHz survey in galprop skymap = "<<galaxy.nu_synch[ips13]/1.e6<<" MHz "<<endl;//AWS20110426
  cout<<"nearest synch frequency to "<<nu14_survey/1.e6<<" MHz survey in galprop skymap = "<<galaxy.nu_synch[ips14]/1.e6<<" MHz "<<endl;//AWS20110426
  cout<<"nearest synch frequency to "<<nu15_survey/1.e6<<" MHz survey in galprop skymap = "<<galaxy.nu_synch[ips15]/1.e6<<" MHz "<<endl;//AWS20110426
  cout<<"nearest synch frequency to "<<nu16_survey/1.e6<<" MHz survey in galprop skymap = "<<galaxy.nu_synch[ips16]/1.e6<<" MHz "<<endl;//AWS20110426


  cout<<"nearest synch frequencies for spectral index in galprop skymap = "<<galaxy.nu_synch[ip1]/1.e6<<" - "<<galaxy.nu_synch[ip2]/1.e6   <<" MHz "<<endl;
  cout<<"nearest synch frequencies for spectral index in galprop skymap = "<<galaxy.nu_synch[ip3]/1.e6<<" - "<<galaxy.nu_synch[ip4]/1.e6   <<" MHz "<<endl;
  cout<<"nearest synch frequencies for spectral index in galprop skymap = "<<galaxy.nu_synch[ip5]/1.e6<<" - "<<galaxy.nu_synch[ip6]/1.e6   <<" MHz "<<endl;
  cout<<"nearest synch frequencies for spectral index in galprop skymap = "<<galaxy.nu_synch[ip7]/1.e6<<" - "<<galaxy.nu_synch[ip8]/1.e6   <<" MHz "<<endl;

  if(I_or_Tb==1) factor  =    1.0;                                          // intensity




  if(background_mode==0)// no background correction
    {
      background1=0.;
      background2=0.;
    }


  if(background_mode==1)// add background to model
    {
      //      background1=EGB_408MHz;  if(I_or_Tb==1) background1 /= ( I_to_Tb / pow(galaxy.nu_synch[ips1],2)  );
      background2=0.;
    }

  if(background_mode==2)// subtract background from data
    {
      background1=0.;
      //      background2=EGB_408MHz;  if(I_or_Tb==1) background2 /= ( I_to_Tb / pow(galaxy.nu_synch[ips1],2)  );
    }


  ///////////////////// survey maps

	 for (i_lat =0;  i_lat <galaxy.n_lat;  i_lat++ ){
          for(i_long=0;  i_long<galaxy.n_long; i_long++){

           l=galaxy.long_min+galaxy.d_long*i_long;
           b=galaxy. lat_min+galaxy. d_lat*i_lat;



            l_off=l-180.;
            if(l_off<0.)l_off+=360.0;
            ii_long= galaxy.n_long-l_off/galaxy.d_long;
	    ii_long+=1;                                 //TH1D bins start at 1 AWS20080125
            ii_lat = i_lat+1;                           //TH1D bins start at 1 AWS20080128

	    //	    cout<<"l l_off ii_long"<<l<<" "<<l_off<<" "<<ii_long<<endl;

               
              if(I_or_Tb==2) factor  =    I_to_Tb / pow(galaxy.nu_synch[ips1],2)  ;     // brightness temperature
	      if(background_mode==1){background1=EGB1      ;  if(I_or_Tb==1) background1 /= ( I_to_Tb / pow(galaxy.nu_synch[ips1],2)  );}     //AWS20070529
              if(background_mode==2){background2=EGB1      ;  if(I_or_Tb==1) background2 /= ( I_to_Tb / pow(galaxy.nu_synch[ips1],2)  );}     //AWS20070529
             
              survey_map1->SetBinContent(ii_long,ii_lat,       synchrotron_skymap1        ->d2[i_long][i_lat].s[0]    * factor - background2); //AWS20080128 and below
	     
              if(I_or_Tb==2) factor  =    I_to_Tb / pow(galaxy.nu_synch[ips2],2)  ;     // brightness temperature
	      if(background_mode==1){background1=EGB2       ;  if(I_or_Tb==1) background1 /= ( I_to_Tb / pow(galaxy.nu_synch[ips2],2)  );}     //AWS20070529
              if(background_mode==2){background2=EGB2       ;  if(I_or_Tb==1) background2 /= ( I_to_Tb / pow(galaxy.nu_synch[ips2],2)  );}     //AWS20070529
            
              survey_map2->SetBinContent(ii_long,ii_lat,       synchrotron_skymap2        ->d2[i_long][i_lat].s[0]    * factor - background2); //AWS20070529

              if(I_or_Tb==2) factor  =    I_to_Tb / pow(galaxy.nu_synch[ips3],2)  ;     // brightness temperature
	      if(background_mode==1){background1=EGB3       ;  if(I_or_Tb==1) background1 /= ( I_to_Tb / pow(galaxy.nu_synch[ips3],2)  );}     //AWS20070529
              if(background_mode==2){background2=EGB3       ;  if(I_or_Tb==1) background2 /= ( I_to_Tb / pow(galaxy.nu_synch[ips3],2)  );}     //AWS20070529
              
              survey_map3->SetBinContent(ii_long,ii_lat,       synchrotron_skymap3        ->d2[i_long][i_lat].s[0]    * factor - background2); //AWS20070529

	      //	      cout<<data.synchrotron_skymap__2326MHz   .d2[i_long][i_lat].s[0]    * factor - background2<<"  " <<survey_map3->GetBinContent(ii_long,i_lat)<<endl;

              if(I_or_Tb==2) factor  =    I_to_Tb / pow(galaxy.nu_synch[ips4],2)  ;     // brightness temperature
	      if(background_mode==1){background1=EGB4        ; if(I_or_Tb==1) background1 /= ( I_to_Tb / pow(galaxy.nu_synch[ips4],2)  );}     //AWS20070529
              if(background_mode==2){background2=EGB4        ; if(I_or_Tb==1) background2 /= ( I_to_Tb / pow(galaxy.nu_synch[ips4],2)  );}     //AWS20070529
              
              survey_map4->SetBinContent(ii_long,ii_lat,       synchrotron_skymap4        ->d2[i_long][i_lat].s[0]    * factor - background2); //AWS20070529


             if(I_or_Tb==2) factor  =    I_to_Tb / pow(galaxy.nu_synch[ips5],2)  ;     // brightness temperature
	      if(background_mode==1){background1=EGB5        ; if(I_or_Tb==1) background1 /= ( I_to_Tb / pow(galaxy.nu_synch[ips5],2)  );}     
              if(background_mode==2){background2=EGB5        ; if(I_or_Tb==1) background2 /= ( I_to_Tb / pow(galaxy.nu_synch[ips5],2)  );}   
            
              survey_map5->SetBinContent(ii_long,ii_lat,       synchrotron_skymap5        ->d2[i_long][i_lat].s[0]    * factor - background2); 

             if(I_or_Tb==2) factor  =    I_to_Tb / pow(galaxy.nu_synch[ips6],2)  ;     // brightness temperature
	      if(background_mode==1){background1=EGB6        ; if(I_or_Tb==1) background1 /= ( I_to_Tb / pow(galaxy.nu_synch[ips6],2)  );}     
              if(background_mode==2){background2=EGB6        ; if(I_or_Tb==1) background2 /= ( I_to_Tb / pow(galaxy.nu_synch[ips6],2)  );}   
           
              survey_map6->SetBinContent(ii_long,ii_lat,       synchrotron_skymap6        ->d2[i_long][i_lat].s[0]    * factor - background2); 

             if(I_or_Tb==2) factor  =    I_to_Tb / pow(galaxy.nu_synch[ips7],2)  ;     // brightness temperature
	      if(background_mode==1){background1=EGB7        ; if(I_or_Tb==1) background1 /= ( I_to_Tb / pow(galaxy.nu_synch[ips7],2)  );}     
              if(background_mode==2){background2=EGB7        ; if(I_or_Tb==1) background2 /= ( I_to_Tb / pow(galaxy.nu_synch[ips7],2)  );}   
             
              survey_map7->SetBinContent(ii_long,ii_lat,       synchrotron_skymap7        ->d2[i_long][i_lat].s[0]    * factor - background2); 


             if(I_or_Tb==2) factor  =    I_to_Tb / pow(galaxy.nu_synch[ips8],2)  ;     // brightness temperature
	      if(background_mode==1){background1=EGB8        ; if(I_or_Tb==1) background1 /= ( I_to_Tb / pow(galaxy.nu_synch[ips8],2)  );}     
              if(background_mode==2){background2=EGB8        ; if(I_or_Tb==1) background2 /= ( I_to_Tb / pow(galaxy.nu_synch[ips8],2)  );}   
           
              survey_map8->SetBinContent(ii_long,ii_lat,       synchrotron_skymap8        ->d2[i_long][i_lat].s[0]    * factor - background2); 


             if(I_or_Tb==2) factor  =    I_to_Tb / pow(galaxy.nu_synch[ips9],2)  ;     // brightness temperature
	      if(background_mode==1){background1=EGB9        ; if(I_or_Tb==1) background1 /= ( I_to_Tb / pow(galaxy.nu_synch[ips9],2)  );}     
              if(background_mode==2){background2=EGB9        ; if(I_or_Tb==1) background2 /= ( I_to_Tb / pow(galaxy.nu_synch[ips9],2)  );}   
              
              survey_map9->SetBinContent(ii_long,ii_lat,       synchrotron_skymap9        ->d2[i_long][i_lat].s[0]    * factor - background2); 



             if(I_or_Tb==2) factor  =    I_to_Tb / pow(galaxy.nu_synch[ips10],2)  ;     // brightness temperature
	      if(background_mode==1){background1=EGB10        ; if(I_or_Tb==1) background1 /= ( I_to_Tb / pow(galaxy.nu_synch[ips10],2)  );}     
              if(background_mode==2){background2=EGB10        ; if(I_or_Tb==1) background2 /= ( I_to_Tb / pow(galaxy.nu_synch[ips10],2)  );}   
              
              survey_map10->SetBinContent(ii_long,ii_lat,       synchrotron_skymap10       ->d2[i_long][i_lat].s[0]     * factor - background2); 


             if(I_or_Tb==2) factor  =    I_to_Tb / pow(galaxy.nu_synch[ips11],2)  ;     // brightness temperature
	      if(background_mode==1){background1=EGB11        ; if(I_or_Tb==1) background1 /= ( I_to_Tb / pow(galaxy.nu_synch[ips11],2)  );}     
              if(background_mode==2){background2=EGB11        ; if(I_or_Tb==1) background2 /= ( I_to_Tb / pow(galaxy.nu_synch[ips11],2)  );}   
           
              survey_map11->SetBinContent(ii_long,ii_lat,       synchrotron_skymap11       ->d2[i_long][i_lat].s[0]     * factor - background2);


             if(I_or_Tb==2) factor  =    I_to_Tb / pow(galaxy.nu_synch[ips12],2)  ;     // brightness temperature
	      if(background_mode==1){background1=EGB12        ; if(I_or_Tb==1) background1 /= ( I_to_Tb / pow(galaxy.nu_synch[ips12],2)  );}     
              if(background_mode==2){background2=EGB12        ; if(I_or_Tb==1) background2 /= ( I_to_Tb / pow(galaxy.nu_synch[ips12],2)  );}   
             
              survey_map12->SetBinContent(ii_long,ii_lat,       synchrotron_skymap12       ->d2[i_long][i_lat].s[0]     * factor - background2);


	      // WMAP polarized maps AWS20110426
	      // not yet available as l,b so use existing 23 GHz map 9  as place holder for now. Now available so used

            if(I_or_Tb==2) factor  =    I_to_Tb / pow(galaxy.nu_synch[ips13],2)  ;     // brightness temperature
	      if(background_mode==1){background1=EGB13        ; if(I_or_Tb==1) background1 /= ( I_to_Tb / pow(galaxy.nu_synch[ips13],2)  );}     
              if(background_mode==2){background2=EGB13        ; if(I_or_Tb==1) background2 /= ( I_to_Tb / pow(galaxy.nu_synch[ips13],2)  );}   
	      //                                                                  V
              survey_map13->SetBinContent(ii_long,ii_lat,       synchrotron_skymap13       ->d2[i_long][i_lat].s[0]     * factor - background2);

            if(I_or_Tb==2) factor  =    I_to_Tb / pow(galaxy.nu_synch[ips14],2)  ;     // brightness temperature
	      if(background_mode==1){background1=EGB14        ; if(I_or_Tb==1) background1 /= ( I_to_Tb / pow(galaxy.nu_synch[ips14],2)  );}     
              if(background_mode==2){background2=EGB14        ; if(I_or_Tb==1) background2 /= ( I_to_Tb / pow(galaxy.nu_synch[ips14],2)  );}   
	      //                                                                  V
              survey_map14->SetBinContent(ii_long,ii_lat,       synchrotron_skymap14       ->d2[i_long][i_lat].s[0]     * factor - background2);

            if(I_or_Tb==2) factor  =    I_to_Tb / pow(galaxy.nu_synch[ips15],2)  ;     // brightness temperature
	      if(background_mode==1){background1=EGB15        ; if(I_or_Tb==1) background1 /= ( I_to_Tb / pow(galaxy.nu_synch[ips15],2)  );}     
              if(background_mode==2){background2=EGB15        ; if(I_or_Tb==1) background2 /= ( I_to_Tb / pow(galaxy.nu_synch[ips15],2)  );}   
	      //                                                                  V
              survey_map15->SetBinContent(ii_long,ii_lat,       synchrotron_skymap15       ->d2[i_long][i_lat].s[0]     * factor - background2);

            if(I_or_Tb==2) factor  =    I_to_Tb / pow(galaxy.nu_synch[ips16],2)  ;     // brightness temperature
	      if(background_mode==1){background1=EGB16        ; if(I_or_Tb==1) background1 /= ( I_to_Tb / pow(galaxy.nu_synch[ips16],2)  );}     
              if(background_mode==2){background2=EGB16        ; if(I_or_Tb==1) background2 /= ( I_to_Tb / pow(galaxy.nu_synch[ips16],2)  );}   
	      //                                                                  V
              survey_map16->SetBinContent(ii_long,ii_lat,       synchrotron_skymap16       ->d2[i_long][i_lat].s[0]     * factor - background2);


	      //             cout<<"spectral index="<<spectral_index12<<" "<<spectral_index34    <<endl;

            }  //  i_long
           }   //  i_lat


    //////////////////



  // format = 0
  if (galdef.skymap_format==0) //AWS20100616
  {
	 for (i_lat =0;  i_lat <galaxy.n_lat;  i_lat++ ){
          for(i_long=0;  i_long<galaxy.n_long; i_long++){

           l=galaxy.long_min+galaxy.d_long*i_long;
           b=galaxy. lat_min+galaxy. d_lat*i_lat;



            l_off=l-180.;
            if(l_off<0.)l_off+=360.0;
            ii_long= galaxy.n_long-l_off/galaxy.d_long;
	    ii_long+=1;                                 //TH1D bins start at 1 AWS20080125
            ii_lat = i_lat+1;                           //TH1D bins start at 1 AWS20080128

	    //	    cout<<"l l_off ii_long"<<l<<" "<<l_off<<" "<<ii_long<<endl;

               
              if(I_or_Tb==2) factor  =    I_to_Tb / pow(galaxy.nu_synch[ips1],2)  ;     // brightness temperature
	      if(background_mode==1){background1=EGB1      ;  if(I_or_Tb==1) background1 /= ( I_to_Tb / pow(galaxy.nu_synch[ips1],2)  );}     //AWS20070529
              if(background_mode==2){background2=EGB1      ;  if(I_or_Tb==1) background2 /= ( I_to_Tb / pow(galaxy.nu_synch[ips1],2)  );}     //AWS20070529
               synch_map1->SetBinContent(ii_long,ii_lat,galaxy.synchrotron_skymap          .d2[i_long][i_lat].s[ips1] * factor + background1); //AWS20080128
	    
	     
              if(I_or_Tb==2) factor  =    I_to_Tb / pow(galaxy.nu_synch[ips2],2)  ;     // brightness temperature
	      if(background_mode==1){background1=EGB2       ;  if(I_or_Tb==1) background1 /= ( I_to_Tb / pow(galaxy.nu_synch[ips2],2)  );}     //AWS20070529
              if(background_mode==2){background2=EGB2       ;  if(I_or_Tb==1) background2 /= ( I_to_Tb / pow(galaxy.nu_synch[ips2],2)  );}     //AWS20070529
               synch_map2->SetBinContent(ii_long,ii_lat,galaxy.synchrotron_skymap          .d2[i_long][i_lat].s[ips2] * factor + background1); 
	     

              if(I_or_Tb==2) factor  =    I_to_Tb / pow(galaxy.nu_synch[ips3],2)  ;     // brightness temperature
	      if(background_mode==1){background1=EGB3       ;  if(I_or_Tb==1) background1 /= ( I_to_Tb / pow(galaxy.nu_synch[ips3],2)  );}     //AWS20070529
              if(background_mode==2){background2=EGB3       ;  if(I_or_Tb==1) background2 /= ( I_to_Tb / pow(galaxy.nu_synch[ips3],2)  );}     //AWS20070529
               synch_map3->SetBinContent(ii_long,ii_lat,galaxy.synchrotron_skymap          .d2[i_long][i_lat].s[ips3] * factor + background1); 
	     

	      //	      cout<<data.synchrotron_skymap__2326MHz   .d2[i_long][i_lat].s[0]    * factor - background2<<"  " <<survey_map3->GetBinContent(ii_long,i_lat)<<endl;

              if(I_or_Tb==2) factor  =    I_to_Tb / pow(galaxy.nu_synch[ips4],2)  ;     // brightness temperature
	      if(background_mode==1){background1=EGB4        ; if(I_or_Tb==1) background1 /= ( I_to_Tb / pow(galaxy.nu_synch[ips4],2)  );}     //AWS20070529
              if(background_mode==2){background2=EGB4        ; if(I_or_Tb==1) background2 /= ( I_to_Tb / pow(galaxy.nu_synch[ips4],2)  );}     //AWS20070529
               synch_map4->SetBinContent(ii_long,ii_lat,galaxy.synchrotron_skymap          .d2[i_long][i_lat].s[ips4] * factor + background1); 
             


             if(I_or_Tb==2) factor  =    I_to_Tb / pow(galaxy.nu_synch[ips5],2)  ;     // brightness temperature
	      if(background_mode==1){background1=EGB5        ; if(I_or_Tb==1) background1 /= ( I_to_Tb / pow(galaxy.nu_synch[ips5],2)  );}     
              if(background_mode==2){background2=EGB5        ; if(I_or_Tb==1) background2 /= ( I_to_Tb / pow(galaxy.nu_synch[ips5],2)  );}   
               synch_map5->SetBinContent(ii_long,ii_lat,galaxy.synchrotron_skymap          .d2[i_long][i_lat].s[ips5] * factor + background1); 
            

             if(I_or_Tb==2) factor  =    I_to_Tb / pow(galaxy.nu_synch[ips6],2)  ;     // brightness temperature
	      if(background_mode==1){background1=EGB6        ; if(I_or_Tb==1) background1 /= ( I_to_Tb / pow(galaxy.nu_synch[ips6],2)  );}     
              if(background_mode==2){background2=EGB6        ; if(I_or_Tb==1) background2 /= ( I_to_Tb / pow(galaxy.nu_synch[ips6],2)  );}   
               synch_map6->SetBinContent(ii_long,ii_lat,galaxy.synchrotron_skymap          .d2[i_long][i_lat].s[ips6] * factor + background1); 
            

             if(I_or_Tb==2) factor  =    I_to_Tb / pow(galaxy.nu_synch[ips7],2)  ;     // brightness temperature
	      if(background_mode==1){background1=EGB7        ; if(I_or_Tb==1) background1 /= ( I_to_Tb / pow(galaxy.nu_synch[ips7],2)  );}     
              if(background_mode==2){background2=EGB7        ; if(I_or_Tb==1) background2 /= ( I_to_Tb / pow(galaxy.nu_synch[ips7],2)  );}   
               synch_map7->SetBinContent(ii_long,ii_lat,galaxy.synchrotron_skymap          .d2[i_long][i_lat].s[ips7] * factor + background1); 
            


             if(I_or_Tb==2) factor  =    I_to_Tb / pow(galaxy.nu_synch[ips8],2)  ;     // brightness temperature
	      if(background_mode==1){background1=EGB8        ; if(I_or_Tb==1) background1 /= ( I_to_Tb / pow(galaxy.nu_synch[ips8],2)  );}     
              if(background_mode==2){background2=EGB8        ; if(I_or_Tb==1) background2 /= ( I_to_Tb / pow(galaxy.nu_synch[ips8],2)  );}   
               synch_map8->SetBinContent(ii_long,ii_lat,galaxy.synchrotron_skymap          .d2[i_long][i_lat].s[ips8] * factor + background1); 
             


             if(I_or_Tb==2) factor  =    I_to_Tb / pow(galaxy.nu_synch[ips9],2)  ;     // brightness temperature
	      if(background_mode==1){background1=EGB9        ; if(I_or_Tb==1) background1 /= ( I_to_Tb / pow(galaxy.nu_synch[ips9],2)  );}     
              if(background_mode==2){background2=EGB9        ; if(I_or_Tb==1) background2 /= ( I_to_Tb / pow(galaxy.nu_synch[ips9],2)  );}   
               synch_map9->SetBinContent(ii_long,ii_lat,galaxy.synchrotron_skymap          .d2[i_long][i_lat].s[ips9] * factor + background1); 
            



             if(I_or_Tb==2) factor  =    I_to_Tb / pow(galaxy.nu_synch[ips10],2)  ;     // brightness temperature
	      if(background_mode==1){background1=EGB10        ; if(I_or_Tb==1) background1 /= ( I_to_Tb / pow(galaxy.nu_synch[ips10],2)  );}     
              if(background_mode==2){background2=EGB10        ; if(I_or_Tb==1) background2 /= ( I_to_Tb / pow(galaxy.nu_synch[ips10],2)  );}   
               synch_map10->SetBinContent(ii_long,ii_lat,galaxy.synchrotron_skymap          .d2[i_long][i_lat].s[ips10] * factor + background1); 
            


             if(I_or_Tb==2) factor  =    I_to_Tb / pow(galaxy.nu_synch[ips11],2)  ;     // brightness temperature
	      if(background_mode==1){background1=EGB11        ; if(I_or_Tb==1) background1 /= ( I_to_Tb / pow(galaxy.nu_synch[ips11],2)  );}     
              if(background_mode==2){background2=EGB11        ; if(I_or_Tb==1) background2 /= ( I_to_Tb / pow(galaxy.nu_synch[ips11],2)  );}   
               synch_map11->SetBinContent(ii_long,ii_lat,galaxy.synchrotron_skymap          .d2[i_long][i_lat].s[ips11] * factor + background1); 
            


             if(I_or_Tb==2) factor  =    I_to_Tb / pow(galaxy.nu_synch[ips12],2)  ;     // brightness temperature
	      if(background_mode==1){background1=EGB12        ; if(I_or_Tb==1) background1 /= ( I_to_Tb / pow(galaxy.nu_synch[ips12],2)  );}     
              if(background_mode==2){background2=EGB12        ; if(I_or_Tb==1) background2 /= ( I_to_Tb / pow(galaxy.nu_synch[ips12],2)  );}   
               synch_map12->SetBinContent(ii_long,ii_lat,galaxy.synchrotron_skymap          .d2[i_long][i_lat].s[ips12] * factor + background1); 


	       // WMAP polarized AWS20110426

                         if(I_or_Tb==2) factor  =    I_to_Tb / pow(galaxy.nu_synch[ips13],2)  ;     // brightness temperature
	      if(background_mode==1){background1=EGB13        ; if(I_or_Tb==1) background1 /= ( I_to_Tb / pow(galaxy.nu_synch[ips13],2)  );}     
              if(background_mode==2){background2=EGB13        ; if(I_or_Tb==1) background2 /= ( I_to_Tb / pow(galaxy.nu_synch[ips13],2)  );}   
               synch_map13->SetBinContent(ii_long,ii_lat,galaxy.synchrotron_Q_skymap          .d2[i_long][i_lat].s[ips13] * factor + background1); 

                         if(I_or_Tb==2) factor  =    I_to_Tb / pow(galaxy.nu_synch[ips14],2)  ;     // brightness temperature
	      if(background_mode==1){background1=EGB14        ; if(I_or_Tb==1) background1 /= ( I_to_Tb / pow(galaxy.nu_synch[ips14],2)  );}     
              if(background_mode==2){background2=EGB14        ; if(I_or_Tb==1) background2 /= ( I_to_Tb / pow(galaxy.nu_synch[ips14],2)  );}   
               synch_map14->SetBinContent(ii_long,ii_lat,galaxy.synchrotron_U_skymap          .d2[i_long][i_lat].s[ips14] * factor + background1); 

                         if(I_or_Tb==2) factor  =    I_to_Tb / pow(galaxy.nu_synch[ips15],2)  ;     // brightness temperature
	      if(background_mode==1){background1=EGB15        ; if(I_or_Tb==1) background1 /= ( I_to_Tb / pow(galaxy.nu_synch[ips15],2)  );}     
              if(background_mode==2){background2=EGB15        ; if(I_or_Tb==1) background2 /= ( I_to_Tb / pow(galaxy.nu_synch[ips15],2)  );}   
               synch_map15->SetBinContent(ii_long,ii_lat,galaxy.synchrotron_P_skymap          .d2[i_long][i_lat].s[ips15] * factor + background1); 

                         if(I_or_Tb==2) factor  =    I_to_Tb / pow(galaxy.nu_synch[ips16],2)  ;     // brightness temperature
	      if(background_mode==1){background1=EGB16        ; if(I_or_Tb==1) background1 /= ( I_to_Tb / pow(galaxy.nu_synch[ips16],2)  );}     
              if(background_mode==2){background2=EGB16        ; if(I_or_Tb==1) background2 /= ( I_to_Tb / pow(galaxy.nu_synch[ips16],2)  );}   
               synch_map16->SetBinContent(ii_long,ii_lat,galaxy.synchrotron_skymap          .d2[i_long][i_lat].s[ips16] * factor + background1); 






            }  //  i_long
           }   //  i_lat

  }// skymap_format==0


 ////////////////// skymap format = 3 

	if(galdef.skymap_format==3)
	{
          double rtd=180./pi;        // rad to deg




	 for (i_lat =0;  i_lat <galaxy.n_lat;  i_lat++ ){
          for(i_long=0;  i_long<galaxy.n_long; i_long++){

           l=galaxy.long_min+galaxy.d_long*i_long;
           b=galaxy. lat_min+galaxy. d_lat*i_lat;



            l_off=l-180.;
            if(l_off<0.)l_off+=360.0;
            ii_long= galaxy.n_long-l_off/galaxy.d_long;
	    ii_long+=1;                                 //TH1D bins start at 1 AWS20080125
            ii_lat = i_lat+1;                           //TH1D bins start at 1 AWS20080128

	    //	    cout<<"l l_off ii_long"<<l<<" "<<l_off<<" "<<ii_long<<endl;
               
            int ipix;
            pointing ang;                                    // healpix base class
            ang.theta = (90.0 - b)/rtd;
            ang.phi   =         l /rtd;
            ipix = galaxy.synchrotron_hp_skymap.ang2pix(ang);// healpix base class

              if(I_or_Tb==2) factor  =    I_to_Tb / pow(galaxy.nu_synch[ips1],2)  ;     // brightness temperature
	      if(background_mode==1){background1=EGB1      ;  if(I_or_Tb==1) background1 /= ( I_to_Tb / pow(galaxy.nu_synch[ips1],2)  );}     //AWS20070529
              if(background_mode==2){background2=EGB1      ;  if(I_or_Tb==1) background2 /= ( I_to_Tb / pow(galaxy.nu_synch[ips1],2)  );}     //AWS20070529
               synch_map1->SetBinContent(ii_long,ii_lat,galaxy.synchrotron_hp_skymap[ipix][ips1] * factor + background1); //AWS20100615
	       
             
	     
              if(I_or_Tb==2) factor  =    I_to_Tb / pow(galaxy.nu_synch[ips2],2)  ;     // brightness temperature
	      if(background_mode==1){background1=EGB2       ;  if(I_or_Tb==1) background1 /= ( I_to_Tb / pow(galaxy.nu_synch[ips2],2)  );}     //AWS20070529
              if(background_mode==2){background2=EGB2       ;  if(I_or_Tb==1) background2 /= ( I_to_Tb / pow(galaxy.nu_synch[ips2],2)  );}     //AWS20070529
               synch_map2->SetBinContent(ii_long,ii_lat,galaxy.synchrotron_hp_skymap[ipix][ips2] * factor + background1); 
	   
             

              if(I_or_Tb==2) factor  =    I_to_Tb / pow(galaxy.nu_synch[ips3],2)  ;     // brightness temperature
	      if(background_mode==1){background1=EGB3       ;  if(I_or_Tb==1) background1 /= ( I_to_Tb / pow(galaxy.nu_synch[ips3],2)  );}     //AWS20070529
              if(background_mode==2){background2=EGB3       ;  if(I_or_Tb==1) background2 /= ( I_to_Tb / pow(galaxy.nu_synch[ips3],2)  );}     //AWS20070529
               synch_map3->SetBinContent(ii_long,ii_lat,galaxy.synchrotron_hp_skymap[ipix][ips3] * factor + background1); 
	      
            

	     

              if(I_or_Tb==2) factor  =    I_to_Tb / pow(galaxy.nu_synch[ips4],2)  ;     // brightness temperature
	      if(background_mode==1){background1=EGB4        ; if(I_or_Tb==1) background1 /= ( I_to_Tb / pow(galaxy.nu_synch[ips4],2)  );}     //AWS20070529
              if(background_mode==2){background2=EGB4        ; if(I_or_Tb==1) background2 /= ( I_to_Tb / pow(galaxy.nu_synch[ips4],2)  );}     //AWS20070529
               synch_map4->SetBinContent(ii_long,ii_lat,galaxy.synchrotron_hp_skymap[ipix][ips4] * factor + background1); 
           


             if(I_or_Tb==2) factor  =    I_to_Tb / pow(galaxy.nu_synch[ips5],2)  ;     // brightness temperature
	      if(background_mode==1){background1=EGB5        ; if(I_or_Tb==1) background1 /= ( I_to_Tb / pow(galaxy.nu_synch[ips5],2)  );}     
              if(background_mode==2){background2=EGB5        ; if(I_or_Tb==1) background2 /= ( I_to_Tb / pow(galaxy.nu_synch[ips5],2)  );}   
               synch_map5->SetBinContent(ii_long,ii_lat,galaxy.synchrotron_hp_skymap[ipix][ips5] * factor + background1); 
        

             if(I_or_Tb==2) factor  =    I_to_Tb / pow(galaxy.nu_synch[ips6],2)  ;     // brightness temperature
	      if(background_mode==1){background1=EGB6        ; if(I_or_Tb==1) background1 /= ( I_to_Tb / pow(galaxy.nu_synch[ips6],2)  );}     
              if(background_mode==2){background2=EGB6        ; if(I_or_Tb==1) background2 /= ( I_to_Tb / pow(galaxy.nu_synch[ips6],2)  );}   
               synch_map6->SetBinContent(ii_long,ii_lat,galaxy.synchrotron_hp_skymap[ipix][ips6] * factor + background1); 
            

             if(I_or_Tb==2) factor  =    I_to_Tb / pow(galaxy.nu_synch[ips7],2)  ;     // brightness temperature
	      if(background_mode==1){background1=EGB7        ; if(I_or_Tb==1) background1 /= ( I_to_Tb / pow(galaxy.nu_synch[ips7],2)  );}     
              if(background_mode==2){background2=EGB7        ; if(I_or_Tb==1) background2 /= ( I_to_Tb / pow(galaxy.nu_synch[ips7],2)  );}   
               synch_map7->SetBinContent(ii_long,ii_lat,galaxy.synchrotron_hp_skymap[ipix][ips7] * factor + background1); 
              


             if(I_or_Tb==2) factor  =    I_to_Tb / pow(galaxy.nu_synch[ips8],2)  ;     // brightness temperature
	      if(background_mode==1){background1=EGB8        ; if(I_or_Tb==1) background1 /= ( I_to_Tb / pow(galaxy.nu_synch[ips8],2)  );}     
              if(background_mode==2){background2=EGB8        ; if(I_or_Tb==1) background2 /= ( I_to_Tb / pow(galaxy.nu_synch[ips8],2)  );}   
               synch_map8->SetBinContent(ii_long,ii_lat,galaxy.synchrotron_hp_skymap[ipix][ips8] * factor + background1); 
         


             if(I_or_Tb==2) factor  =    I_to_Tb / pow(galaxy.nu_synch[ips9],2)  ;     // brightness temperature
	      if(background_mode==1){background1=EGB9        ; if(I_or_Tb==1) background1 /= ( I_to_Tb / pow(galaxy.nu_synch[ips9],2)  );}     
              if(background_mode==2){background2=EGB9        ; if(I_or_Tb==1) background2 /= ( I_to_Tb / pow(galaxy.nu_synch[ips9],2)  );}   
               synch_map9->SetBinContent(ii_long,ii_lat,galaxy.synchrotron_hp_skymap[ipix][ips9] * factor + background1); 
           



             if(I_or_Tb==2) factor  =    I_to_Tb / pow(galaxy.nu_synch[ips10],2)  ;     // brightness temperature
	      if(background_mode==1){background1=EGB10        ; if(I_or_Tb==1) background1 /= ( I_to_Tb / pow(galaxy.nu_synch[ips10],2)  );}     
              if(background_mode==2){background2=EGB10        ; if(I_or_Tb==1) background2 /= ( I_to_Tb / pow(galaxy.nu_synch[ips10],2)  );}   
               synch_map10->SetBinContent(ii_long,ii_lat,galaxy.synchrotron_hp_skymap[ipix][ips10] * factor + background1); 
         


             if(I_or_Tb==2) factor  =    I_to_Tb / pow(galaxy.nu_synch[ips11],2)  ;     // brightness temperature
	      if(background_mode==1){background1=EGB11        ; if(I_or_Tb==1) background1 /= ( I_to_Tb / pow(galaxy.nu_synch[ips11],2)  );}     
              if(background_mode==2){background2=EGB11        ; if(I_or_Tb==1) background2 /= ( I_to_Tb / pow(galaxy.nu_synch[ips11],2)  );}   
               synch_map11->SetBinContent(ii_long,ii_lat,galaxy.synchrotron_hp_skymap[ipix][ips11] * factor + background1); 
            


             if(I_or_Tb==2) factor  =    I_to_Tb / pow(galaxy.nu_synch[ips12],2)  ;     // brightness temperature
	      if(background_mode==1){background1=EGB12        ; if(I_or_Tb==1) background1 /= ( I_to_Tb / pow(galaxy.nu_synch[ips12],2)  );}     
              if(background_mode==2){background2=EGB12        ; if(I_or_Tb==1) background2 /= ( I_to_Tb / pow(galaxy.nu_synch[ips12],2)  );}   
               synch_map12->SetBinContent(ii_long,ii_lat,galaxy.synchrotron_hp_skymap[ipix][ips12] * factor + background1); 
            

	       //AWS20110426 : polarized maps

             if(I_or_Tb==2) factor  =    I_to_Tb / pow(galaxy.nu_synch[ips13],2)  ;     // brightness temperature
	      if(background_mode==1){background1=EGB13        ; if(I_or_Tb==1) background1 /= ( I_to_Tb / pow(galaxy.nu_synch[ips13],2)  );}     
              if(background_mode==2){background2=EGB13        ; if(I_or_Tb==1) background2 /= ( I_to_Tb / pow(galaxy.nu_synch[ips13],2)  );}   
               synch_map13->SetBinContent(ii_long,ii_lat,galaxy.synchrotron_Q_hp_skymap[ipix][ips13] * factor + background1); 

             if(I_or_Tb==2) factor  =    I_to_Tb / pow(galaxy.nu_synch[ips14],2)  ;     // brightness temperature
	      if(background_mode==1){background1=EGB14        ; if(I_or_Tb==1) background1 /= ( I_to_Tb / pow(galaxy.nu_synch[ips14],2)  );}     
              if(background_mode==2){background2=EGB14        ; if(I_or_Tb==1) background2 /= ( I_to_Tb / pow(galaxy.nu_synch[ips14],2)  );}   
               synch_map14->SetBinContent(ii_long,ii_lat,galaxy.synchrotron_U_hp_skymap[ipix][ips14] * factor + background1); 

             if(I_or_Tb==2) factor  =    I_to_Tb / pow(galaxy.nu_synch[ips15],2)  ;     // brightness temperature
	      if(background_mode==1){background1=EGB15        ; if(I_or_Tb==1) background1 /= ( I_to_Tb / pow(galaxy.nu_synch[ips15],2)  );}     
              if(background_mode==2){background2=EGB15        ; if(I_or_Tb==1) background2 /= ( I_to_Tb / pow(galaxy.nu_synch[ips15],2)  );}   
               synch_map15->SetBinContent(ii_long,ii_lat,galaxy.synchrotron_P_hp_skymap[ipix][ips15] * factor + background1); 

             if(I_or_Tb==2) factor  =    I_to_Tb / pow(galaxy.nu_synch[ips16],2)  ;     // brightness temperature
	      if(background_mode==1){background1=EGB16        ; if(I_or_Tb==1) background1 /= ( I_to_Tb / pow(galaxy.nu_synch[ips16],2)  );}     
              if(background_mode==2){background2=EGB16        ; if(I_or_Tb==1) background2 /= ( I_to_Tb / pow(galaxy.nu_synch[ips16],2)  );}   
               synch_map16->SetBinContent(ii_long,ii_lat,galaxy.synchrotron_hp_skymap[ipix][ips16] * factor + background1); 


	       //AWS20110624 : free-free and synch+free-free maps for appropriate cases: only unpolarized  surveys or WMAP I


//           if(free_free_WMAP_template==0)                                              //AWS20110906

	     if(galplotdef.free_free_options==1)                                         //AWS20111222
	     {

              if(I_or_Tb==2) factor  =    I_to_Tb / pow(galaxy.nu_synch[ips1 ],2)  ;     // brightness temperature
	      if(background_mode==1){background1=EGB1         ; if(I_or_Tb==1) background1 /= ( I_to_Tb / pow(galaxy.nu_synch[ips1 ],2)  );}     
              if(background_mode==2){background2=EGB1         ; if(I_or_Tb==1) background2 /= ( I_to_Tb / pow(galaxy.nu_synch[ips1 ],2)  );}   
                     free_free_map1 ->SetBinContent(ii_long,ii_lat, galaxy.free_free_hp_skymap[ipix][ips1 ]                                            * factor + background1); 
               synch_free_free_map1 ->SetBinContent(ii_long,ii_lat,(galaxy.free_free_hp_skymap[ipix][ips1 ]+galaxy.synchrotron_hp_skymap[ipix][ips1 ]) * factor + background1);

              if(I_or_Tb==2) factor  =    I_to_Tb / pow(galaxy.nu_synch[ips2 ],2)  ;     // brightness temperature
	      if(background_mode==1){background1=EGB2         ; if(I_or_Tb==1) background1 /= ( I_to_Tb / pow(galaxy.nu_synch[ips2 ],2)  );}     
              if(background_mode==2){background2=EGB2         ; if(I_or_Tb==1) background2 /= ( I_to_Tb / pow(galaxy.nu_synch[ips2 ],2)  );}   
                     free_free_map2 ->SetBinContent(ii_long,ii_lat, galaxy.free_free_hp_skymap[ipix][ips2 ]                                            * factor + background1); 
               synch_free_free_map2 ->SetBinContent(ii_long,ii_lat,(galaxy.free_free_hp_skymap[ipix][ips2 ]+galaxy.synchrotron_hp_skymap[ipix][ips2 ]) * factor + background1);

              if(I_or_Tb==2) factor  =    I_to_Tb / pow(galaxy.nu_synch[ips3 ],2)  ;     // brightness temperature
	      if(background_mode==1){background1=EGB3         ; if(I_or_Tb==1) background1 /= ( I_to_Tb / pow(galaxy.nu_synch[ips3 ],2)  );}     
              if(background_mode==2){background2=EGB3         ; if(I_or_Tb==1) background2 /= ( I_to_Tb / pow(galaxy.nu_synch[ips3 ],2)  );}   
                     free_free_map3 ->SetBinContent(ii_long,ii_lat, galaxy.free_free_hp_skymap[ipix][ips3 ]                                            * factor + background1); 
               synch_free_free_map3 ->SetBinContent(ii_long,ii_lat,(galaxy.free_free_hp_skymap[ipix][ips3 ]+galaxy.synchrotron_hp_skymap[ipix][ips3 ]) * factor + background1);

              if(I_or_Tb==2) factor  =    I_to_Tb / pow(galaxy.nu_synch[ips4 ],2)  ;     // brightness temperature
	      if(background_mode==1){background1=EGB4         ; if(I_or_Tb==1) background1 /= ( I_to_Tb / pow(galaxy.nu_synch[ips4 ],2)  );}     
              if(background_mode==2){background2=EGB4         ; if(I_or_Tb==1) background2 /= ( I_to_Tb / pow(galaxy.nu_synch[ips4 ],2)  );}   
                     free_free_map4 ->SetBinContent(ii_long,ii_lat, galaxy.free_free_hp_skymap[ipix][ips4 ]                                            * factor + background1); 
               synch_free_free_map4 ->SetBinContent(ii_long,ii_lat,(galaxy.free_free_hp_skymap[ipix][ips4 ]+galaxy.synchrotron_hp_skymap[ipix][ips4 ]) * factor + background1);

              if(I_or_Tb==2) factor  =    I_to_Tb / pow(galaxy.nu_synch[ips5 ],2)  ;     // brightness temperature
	      if(background_mode==1){background1=EGB5         ; if(I_or_Tb==1) background1 /= ( I_to_Tb / pow(galaxy.nu_synch[ips5 ],2)  );}     
              if(background_mode==2){background2=EGB5         ; if(I_or_Tb==1) background2 /= ( I_to_Tb / pow(galaxy.nu_synch[ips5 ],2)  );}   
                     free_free_map5 ->SetBinContent(ii_long,ii_lat, galaxy.free_free_hp_skymap[ipix][ips5 ]                                            * factor + background1); 
               synch_free_free_map5 ->SetBinContent(ii_long,ii_lat,(galaxy.free_free_hp_skymap[ipix][ips5 ]+galaxy.synchrotron_hp_skymap[ipix][ips5 ]) * factor + background1);

              if(I_or_Tb==2) factor  =    I_to_Tb / pow(galaxy.nu_synch[ips6 ],2)  ;     // brightness temperature
	      if(background_mode==1){background1=EGB6         ; if(I_or_Tb==1) background1 /= ( I_to_Tb / pow(galaxy.nu_synch[ips6 ],2)  );}     
              if(background_mode==2){background2=EGB6         ; if(I_or_Tb==1) background2 /= ( I_to_Tb / pow(galaxy.nu_synch[ips6 ],2)  );}   
                     free_free_map6 ->SetBinContent(ii_long,ii_lat, galaxy.free_free_hp_skymap[ipix][ips6 ]                                            * factor + background1); 
               synch_free_free_map6 ->SetBinContent(ii_long,ii_lat,(galaxy.free_free_hp_skymap[ipix][ips6 ]+galaxy.synchrotron_hp_skymap[ipix][ips6 ]) * factor + background1);

              if(I_or_Tb==2) factor  =    I_to_Tb / pow(galaxy.nu_synch[ips7 ],2)  ;     // brightness temperature
	      if(background_mode==1){background1=EGB7         ; if(I_or_Tb==1) background1 /= ( I_to_Tb / pow(galaxy.nu_synch[ips7 ],2)  );}     
              if(background_mode==2){background2=EGB7         ; if(I_or_Tb==1) background2 /= ( I_to_Tb / pow(galaxy.nu_synch[ips7 ],2)  );}   
                     free_free_map7 ->SetBinContent(ii_long,ii_lat, galaxy.free_free_hp_skymap[ipix][ips7 ]                                            * factor + background1); 
               synch_free_free_map7 ->SetBinContent(ii_long,ii_lat,(galaxy.free_free_hp_skymap[ipix][ips7 ]+galaxy.synchrotron_hp_skymap[ipix][ips7 ]) * factor + background1);

	       // cases 8-16 are synchrotron estimates or polarized QUPI

              if(I_or_Tb==2) factor  =    I_to_Tb / pow(galaxy.nu_synch[ips16],2)  ;     // brightness temperature
	      if(background_mode==1){background1=EGB16        ; if(I_or_Tb==1) background1 /= ( I_to_Tb / pow(galaxy.nu_synch[ips16],2)  );}     
              if(background_mode==2){background2=EGB16        ; if(I_or_Tb==1) background2 /= ( I_to_Tb / pow(galaxy.nu_synch[ips16],2)  );}   
                     free_free_map16->SetBinContent(ii_long,ii_lat, galaxy.free_free_hp_skymap[ipix][ips16]                                            * factor + background1); 
               synch_free_free_map16->SetBinContent(ii_long,ii_lat,(galaxy.free_free_hp_skymap[ipix][ips16]+galaxy.synchrotron_hp_skymap[ipix][ips16]) * factor + background1);


	    }//if(galplotdef.free_free_options==1)                                                //AWS20130122


	     ///////////////////////////

 //          if(free_free_WMAP_template==1)                                              //AWS20110906

	     if(galplotdef.free_free_options==2)                                         //AWS20111222
	     {

              if(I_or_Tb==2) factor  =    I_to_Tb / pow(galaxy.nu_synch[ips1 ],2)  ;     // brightness temperature
	      if(background_mode==1){background1=EGB1         ; if(I_or_Tb==1) background1 /= ( I_to_Tb / pow(galaxy.nu_synch[ips1 ],2)  );}     
              if(background_mode==2){background2=EGB1         ; if(I_or_Tb==1) background2 /= ( I_to_Tb / pow(galaxy.nu_synch[ips1 ],2)  );}   
                     free_free_map1 ->SetBinContent(ii_long,ii_lat, data.free_free_WMAP_MEM_hp_skymap[ipix][ips1 ]                                            * factor + background1); 
               synch_free_free_map1 ->SetBinContent(ii_long,ii_lat,(data.free_free_WMAP_MEM_hp_skymap[ipix][ips1 ]+galaxy.synchrotron_hp_skymap[ipix][ips1 ]) * factor + background1);

              if(I_or_Tb==2) factor  =    I_to_Tb / pow(galaxy.nu_synch[ips2 ],2)  ;     // brightness temperature
	      if(background_mode==1){background1=EGB2         ; if(I_or_Tb==1) background1 /= ( I_to_Tb / pow(galaxy.nu_synch[ips2 ],2)  );}     
              if(background_mode==2){background2=EGB2         ; if(I_or_Tb==1) background2 /= ( I_to_Tb / pow(galaxy.nu_synch[ips2 ],2)  );}   
                     free_free_map2 ->SetBinContent(ii_long,ii_lat, data.free_free_WMAP_MEM_hp_skymap[ipix][ips2 ]                                            * factor + background1); 
               synch_free_free_map2 ->SetBinContent(ii_long,ii_lat,(data.free_free_WMAP_MEM_hp_skymap[ipix][ips2 ]+galaxy.synchrotron_hp_skymap[ipix][ips2 ]) * factor + background1);

              if(I_or_Tb==2) factor  =    I_to_Tb / pow(galaxy.nu_synch[ips3 ],2)  ;     // brightness temperature
	      if(background_mode==1){background1=EGB3         ; if(I_or_Tb==1) background1 /= ( I_to_Tb / pow(galaxy.nu_synch[ips3 ],2)  );}     
              if(background_mode==2){background2=EGB3         ; if(I_or_Tb==1) background2 /= ( I_to_Tb / pow(galaxy.nu_synch[ips3 ],2)  );}   
                     free_free_map3 ->SetBinContent(ii_long,ii_lat, data.free_free_WMAP_MEM_hp_skymap[ipix][ips3 ]                                            * factor + background1); 
               synch_free_free_map3 ->SetBinContent(ii_long,ii_lat,(data.free_free_WMAP_MEM_hp_skymap[ipix][ips3 ]+galaxy.synchrotron_hp_skymap[ipix][ips3 ]) * factor + background1);

              if(I_or_Tb==2) factor  =    I_to_Tb / pow(galaxy.nu_synch[ips4 ],2)  ;     // brightness temperature
	      if(background_mode==1){background1=EGB4         ; if(I_or_Tb==1) background1 /= ( I_to_Tb / pow(galaxy.nu_synch[ips4 ],2)  );}     
              if(background_mode==2){background2=EGB4         ; if(I_or_Tb==1) background2 /= ( I_to_Tb / pow(galaxy.nu_synch[ips4 ],2)  );}   
                     free_free_map4 ->SetBinContent(ii_long,ii_lat, data.free_free_WMAP_MEM_hp_skymap[ipix][ips4 ]                                            * factor + background1); 
               synch_free_free_map4 ->SetBinContent(ii_long,ii_lat,(data.free_free_WMAP_MEM_hp_skymap[ipix][ips4 ]+galaxy.synchrotron_hp_skymap[ipix][ips4 ]) * factor + background1);

              if(I_or_Tb==2) factor  =    I_to_Tb / pow(galaxy.nu_synch[ips5 ],2)  ;     // brightness temperature
	      if(background_mode==1){background1=EGB5         ; if(I_or_Tb==1) background1 /= ( I_to_Tb / pow(galaxy.nu_synch[ips5 ],2)  );}     
              if(background_mode==2){background2=EGB5         ; if(I_or_Tb==1) background2 /= ( I_to_Tb / pow(galaxy.nu_synch[ips5 ],2)  );}   
                     free_free_map5 ->SetBinContent(ii_long,ii_lat, data.free_free_WMAP_MEM_hp_skymap[ipix][ips5 ]                                            * factor + background1); 
               synch_free_free_map5 ->SetBinContent(ii_long,ii_lat,(data.free_free_WMAP_MEM_hp_skymap[ipix][ips5 ]+galaxy.synchrotron_hp_skymap[ipix][ips5 ]) * factor + background1);

              if(I_or_Tb==2) factor  =    I_to_Tb / pow(galaxy.nu_synch[ips6 ],2)  ;     // brightness temperature
	      if(background_mode==1){background1=EGB6         ; if(I_or_Tb==1) background1 /= ( I_to_Tb / pow(galaxy.nu_synch[ips6 ],2)  );}     
              if(background_mode==2){background2=EGB6         ; if(I_or_Tb==1) background2 /= ( I_to_Tb / pow(galaxy.nu_synch[ips6 ],2)  );}   
                     free_free_map6 ->SetBinContent(ii_long,ii_lat, data.free_free_WMAP_MEM_hp_skymap[ipix][ips6 ]                                            * factor + background1); 
               synch_free_free_map6 ->SetBinContent(ii_long,ii_lat,(data.free_free_WMAP_MEM_hp_skymap[ipix][ips6 ]+galaxy.synchrotron_hp_skymap[ipix][ips6 ]) * factor + background1);

              if(I_or_Tb==2) factor  =    I_to_Tb / pow(galaxy.nu_synch[ips7 ],2)  ;     // brightness temperature
	      if(background_mode==1){background1=EGB7         ; if(I_or_Tb==1) background1 /= ( I_to_Tb / pow(galaxy.nu_synch[ips7 ],2)  );}     
              if(background_mode==2){background2=EGB7         ; if(I_or_Tb==1) background2 /= ( I_to_Tb / pow(galaxy.nu_synch[ips7 ],2)  );}   
                     free_free_map7 ->SetBinContent(ii_long,ii_lat, data.free_free_WMAP_MEM_hp_skymap[ipix][ips7 ]                                            * factor + background1); 
               synch_free_free_map7 ->SetBinContent(ii_long,ii_lat,(data.free_free_WMAP_MEM_hp_skymap[ipix][ips7 ]+galaxy.synchrotron_hp_skymap[ipix][ips7 ]) * factor + background1);

	       // cases 8-16 are synchrotron estimates or polarized QUPI

              if(I_or_Tb==2) factor  =    I_to_Tb / pow(galaxy.nu_synch[ips16],2)  ;     // brightness temperature
	      if(background_mode==1){background1=EGB16        ; if(I_or_Tb==1) background1 /= ( I_to_Tb / pow(galaxy.nu_synch[ips16],2)  );}     
              if(background_mode==2){background2=EGB16        ; if(I_or_Tb==1) background2 /= ( I_to_Tb / pow(galaxy.nu_synch[ips16],2)  );}   
                     free_free_map16->SetBinContent(ii_long,ii_lat, data.free_free_WMAP_MEM_hp_skymap[ipix][ips16]                                            * factor + background1); 
               synch_free_free_map16->SetBinContent(ii_long,ii_lat,(data.free_free_WMAP_MEM_hp_skymap[ipix][ips16]+galaxy.synchrotron_hp_skymap[ipix][ips16]) * factor + background1);


	    }//if(galplotdef.free_free_options==2)                                                //AWS20130122

	     ////////////////////////////////////////////////////////////////////////

// thermal dust

             if(I_or_Tb==2) factor  =    I_to_Tb / pow(galaxy.nu_synch[ips13],2)  ;     // brightness temperature
	      if(background_mode==1){background1=EGB13        ; if(I_or_Tb==1) background1 /= ( I_to_Tb / pow(galaxy.nu_synch[ips13],2)  );}     
              if(background_mode==2){background2=EGB13        ; if(I_or_Tb==1) background2 /= ( I_to_Tb / pow(galaxy.nu_synch[ips13],2)  );}   
                  thermal_dust_map13->SetBinContent(ii_long,ii_lat, data.dust_Q_WMAP_MCMC_SD_hp_skymap[ipix][ips13]                                            * factor + background1); 

             if(I_or_Tb==2) factor  =    I_to_Tb / pow(galaxy.nu_synch[ips14],2)  ;     // brightness temperature
	      if(background_mode==1){background1=EGB14        ; if(I_or_Tb==1) background1 /= ( I_to_Tb / pow(galaxy.nu_synch[ips14],2)  );}     
              if(background_mode==2){background2=EGB14        ; if(I_or_Tb==1) background2 /= ( I_to_Tb / pow(galaxy.nu_synch[ips14],2)  );}   
                  thermal_dust_map14->SetBinContent(ii_long,ii_lat, data.dust_U_WMAP_MCMC_SD_hp_skymap[ipix][ips14]                                            * factor + background1); 

             if(I_or_Tb==2) factor  =    I_to_Tb / pow(galaxy.nu_synch[ips15],2)  ;     // brightness temperature
	      if(background_mode==1){background1=EGB15        ; if(I_or_Tb==1) background1 /= ( I_to_Tb / pow(galaxy.nu_synch[ips15],2)  );}     
              if(background_mode==2){background2=EGB15        ; if(I_or_Tb==1) background2 /= ( I_to_Tb / pow(galaxy.nu_synch[ips15],2)  );}   
                  thermal_dust_map15->SetBinContent(ii_long,ii_lat, data.dust_P_WMAP_MCMC_SD_hp_skymap[ipix][ips15]                                            * factor + background1); 

             if(I_or_Tb==2) factor  =    I_to_Tb / pow(galaxy.nu_synch[ips16],2)  ;     // brightness temperature
	      if(background_mode==1){background1=EGB16        ; if(I_or_Tb==1) background1 /= ( I_to_Tb / pow(galaxy.nu_synch[ips16],2)  );}     
              if(background_mode==2){background2=EGB16        ; if(I_or_Tb==1) background2 /= ( I_to_Tb / pow(galaxy.nu_synch[ips16],2)  );}   
                  thermal_dust_map16->SetBinContent(ii_long,ii_lat, data.dust_WMAP_MCMC_SD_hp_skymap[ipix][ips16]                                            * factor + background1); 

		  //		  cout<<" data.dust_WMAP_MCMC_SD_hp_skymap[ipix][ips16]*factor="<< data.dust_WMAP_MCMC_SD_hp_skymap[ipix][ips16]*factor<<endl;

// spinning dust

            if(I_or_Tb==2) factor  =    I_to_Tb / pow(galaxy.nu_synch[ips16],2)  ;     // brightness temperature
	      if(background_mode==1){background1=EGB16        ; if(I_or_Tb==1) background1 /= ( I_to_Tb / pow(galaxy.nu_synch[ips16],2)  );}     
              if(background_mode==2){background2=EGB16        ; if(I_or_Tb==1) background2 /= ( I_to_Tb / pow(galaxy.nu_synch[ips16],2)  );}   
                 spinning_dust_map16->SetBinContent(ii_long,ii_lat, data.spin_dust_WMAP_MCMC_SD_hp_skymap[ipix][ips16]                                            * factor + background1); 

		 

	//-----------------------------------------------------------------------------------------------------------------



           }  //  i_long
           }   //  i_lat

	 //            }  //  i_pix
         


}// skymap_format==3	 




	//////////////////////////////////////////////////////////
	// synchrotron from molecular clouds
	//////////////////////////////////////////////////////////


       
	cout<<"synchrotron_clouds_profile:      COR:"<<endl;        
	//	galaxy.COR.print();


        TH2F     *synch_clouds_map;
        synch_clouds_map=new TH2F("synchrotron clouds skymap",canvastitle,                                      
                  galaxy.n_long,       -180,           180.,
                  galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1));


        TH2F     *synch_HI_map;
        synch_HI_map=new TH2F("synchrotron HI skymap",canvastitle,                                      
                  galaxy.n_long,       -180,           180.,
                  galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1));



	 for (i_lat =0;  i_lat <galaxy.n_lat;  i_lat++ ){
          for(i_long=0;  i_long<galaxy.n_long; i_long++){

           l=galaxy.long_min+galaxy.d_long*i_long;
           b=galaxy. lat_min+galaxy. d_lat*i_lat;



            l_off=l-180.;
            if(l_off<0.)l_off+=360.0;
            ii_long= galaxy.n_long-l_off/galaxy.d_long;
	    ii_long+=1;                                 //TH1D bins start at 1 
            ii_lat = i_lat+1;                           //TH1D bins start at 1



               

            // integrate over rings, z-axis of dataset
	    double value=0.;

	    double Xco=1.5e20;

	    double   clouds_scaling_factor = 1.0e-20; // NH->K(408 MHz)
            double   clouds_extra_factor   = 2.5;
                     clouds_extra_factor   = 1.7;
            
            for(int ir=0;ir<galaxy.COR.n_zgrid;ir++)	    value+= galaxy.COR .d3[i_long][i_lat][ir].s[0] *  2*Xco ; 

            value *= clouds_scaling_factor * clouds_extra_factor;

            synch_clouds_map->SetBinContent(ii_long,ii_lat,value  ); 
            
            value=0.;
            for(int ir=0;ir<galaxy.HIR.n_zgrid;ir++)	    value+= galaxy.HIR .d3[i_long][i_lat][ir].s[0]; 
            value *= clouds_scaling_factor;

            synch_HI_map->SetBinContent(ii_long,ii_lat,value  ); 

	    //cout<<"l b l_off ii_long "<<l<<" "<<b<<" "<<l_off<<" "<<ii_long<<" integrated HI+ 2*Xco*CO value="<<value<<endl;





              
	  }
	 }

       






 /////////////////   do the plotting
  





  // construct conventional Galactic longitude axis in two seqments, 180-0 and 340-180
  TF1 *f1=new TF1("f1","-x",   0,180); // root manual p. 149
  TF1 *f2=new TF1("f2","-x", 180,340); // root manual p. 149

  //                      xmin ymin xmax ymax, funcname   ndiv chopt gridlength
  TGaxis *axis1=new TGaxis(-180,-90 ,  0.,-90  , "f1",       9,""                );
  axis1->SetLabelSize(0.03);
  TGaxis *axis2=new TGaxis(  20,-90 ,180.,-90  , "f2",       8,""                );
  axis2->SetLabelSize(0.03);
  axis2->SetTitle("Galactic longitude");
  // fill in 0-340 between two axis segments, unlabelled
  // at present has no visibile effect but could be useful if more minor tickmarks added
  //                         xmin ymin xmax ymax   wmin wmax ndiv chopt gridlength
  TGaxis *axis3=new TGaxis(  0,  -90.  , 20., -90., 0., 20. ,0, "U"               );


  


  int linewidth = 1; //AWS20110706
 


//==============================     Profiles  =============================

//------------------------------     axes      -------------------------

  // construct conventional Galactic longitude axis in two seqments, 180-0 and 340-180

  f1=new TF1("f1","-x",   0,180); // root manual p. 149
  f2=new TF1("f2","-x", 180,340); // root manual p. 149

  // See root PaintAxis

  //                      xmin ymin xmax ymax, funcname   ndiv chopt gridlength
          axis1=new TGaxis(-180,0.  ,  0.,0.   , "f1",       9,""                );
          axis1=new TGaxis(-180,1.e-3, 0.,0.   , "f1",       9,""                );//test
          axis1->SetLabelSize(0.02);
          axis1->SetTitleSize(0.03); //AWS20121129 added, same size as for latitude, but redefined later, here is obsolete 

          axis2=new TGaxis(  20,0.  ,180.,0.   , "f2",       8,""                );
          axis2->SetLabelSize(0.02);
          axis2->SetTitle("Galactic longitude");
          axis2->SetTitleSize(0.03); //AWS20121129 added, same size as for latitude, but redefined later, here is obsolete

  // fill in 0-340 between two axis segments, unlabelled
  // at present has no visibile effect but could be useful if more minor tickmarks added
  //                         xmin ymin xmax ymax   wmin wmax ndiv chopt gridlength
          axis3=new TGaxis(  0,    0.  , 20., 0.  , 0., 20. ,0, "U"               );


//----------------------------------------------------------------------
  // NB reversed longitude axis starting at +180 and decreasing

  l_off=180.-galplotdef.long_max ; if(l_off>360.)l_off-=360.; if(l_off<  0.)l_off+=360.;
  il_min =l_off/galdef.d_long;
  l_off=180.-galplotdef.long_min ; if(l_off>360.)l_off-=360.; if(l_off<  0.)l_off+=360.;
  il_max =l_off/galdef.d_long;
  cout<<"il_min  ilmax :"<<il_min <<" "<<il_max <<endl;



  ib_min=(galplotdef.lat_min-galdef.lat_min+.0001)/galdef.d_lat;
  ib_max=(galplotdef.lat_max-galdef.lat_min+.0001)/galdef.d_lat;
  cout<<"ib_min ibmax:"<<ib_min<<" "<<ib_max<<endl;




  // two l or two b ranges


  l_off=180.-galplotdef.long_max1; if(l_off>360.)l_off-=360.; if(l_off<  0.)l_off+=360.;
  il_min1=l_off/galdef.d_long;
  l_off=180.-galplotdef.long_min1; if(l_off>360.)l_off-=360.; if(l_off<  0.)l_off+=360.;
  il_max1=l_off/galdef.d_long;
  cout<<"il_min1 ilmax1:"<<il_min1<<" "<<il_max1<<endl;

  l_off=180.-galplotdef.long_max2; if(l_off>360.)l_off-=360.; if(l_off<  0.)l_off+=360.;
  il_min2=l_off/galdef.d_long;
  l_off=180.-galplotdef.long_min2; if(l_off>360.)l_off-=360.; if(l_off<  0.)l_off+=360.;
  il_max2=l_off/galdef.d_long;
  cout<<"il_min2 ilmax2:"<<il_min2<<" "<<il_max2<<endl;


  ib_min1=(galplotdef.lat_min1-galdef.lat_min+.0001)/galdef.d_lat;
  ib_max1=(galplotdef.lat_max1-galdef.lat_min+.0001)/galdef.d_lat;
  cout<<"ib_min1 ibmax1:"<<ib_min1<<" "<<ib_max1<<endl;

  ib_min2=(galplotdef.lat_min2-galdef.lat_min+.0001)/galdef.d_lat;
  ib_max2=(galplotdef.lat_max2-galdef.lat_min+.0001)/galdef.d_lat;
  cout<<"ib_min2 ibmax2:"<<ib_min2<<" "<<ib_max2<<endl;






  long_min=-180.; // ic_map etc have longitude centred at 0.0



            

 
     gStyle->SetHistLineColor(kBlue);//1=black 2=red 3=green 4=blue

   

 for (survey_profile_number=1 ; survey_profile_number<=n_survey_profiles ; survey_profile_number++) //AWS20070525
 {

   if(survey_profile_number== 1){synch_map=synch_map1;  survey_map=survey_map1;} // pointers
   if(survey_profile_number== 2){synch_map=synch_map2;  survey_map=survey_map2;} 
   if(survey_profile_number== 3){synch_map=synch_map3;  survey_map=survey_map3;} 
   if(survey_profile_number== 4){synch_map=synch_map4;  survey_map=survey_map4;} 
   if(survey_profile_number== 5){synch_map=synch_map5;  survey_map=survey_map5;} 
   if(survey_profile_number== 6){synch_map=synch_map6;  survey_map=survey_map6;} 
   if(survey_profile_number== 7){synch_map=synch_map7;  survey_map=survey_map7;}

// WMAP 23-94 GHz synchrotron based on Miville or WMAP 7-yr template  
   if(survey_profile_number== 8){synch_map=synch_map8;  survey_map=survey_map8;} 
   if(survey_profile_number== 9){synch_map=synch_map9;  survey_map=survey_map9;} //AWS20070606
   if(survey_profile_number==10){synch_map=synch_map10; survey_map=survey_map10;} //AWS20070606
   if(survey_profile_number==11){synch_map=synch_map11; survey_map=survey_map11;} //AWS20070606
   if(survey_profile_number==12){synch_map=synch_map12; survey_map=survey_map12;} //AWS20070606

// WMAP 7 year 23 GHz polarized Q U P I
   if(survey_profile_number==13){synch_map=synch_map13; survey_map=survey_map13;} //AWS20110426
   if(survey_profile_number==14){synch_map=synch_map14; survey_map=survey_map14;} //AWS20110426
   if(survey_profile_number==15){synch_map=synch_map15; survey_map=survey_map15;} //AWS20110426
   if(survey_profile_number==16){synch_map=synch_map16; survey_map=survey_map16;} //AWS20110426

   if(survey_profile_number== 1){free_free_map=free_free_map1 ;synch_free_free_map=synch_free_free_map1 ; } //AWS20110624
   if(survey_profile_number== 2){free_free_map=free_free_map2 ;synch_free_free_map=synch_free_free_map2 ; } //AWS20110624
   if(survey_profile_number== 3){free_free_map=free_free_map3 ;synch_free_free_map=synch_free_free_map3 ; } //AWS20110624
   if(survey_profile_number== 4){free_free_map=free_free_map4 ;synch_free_free_map=synch_free_free_map4 ; } //AWS20110624
   if(survey_profile_number== 5){free_free_map=free_free_map5 ;synch_free_free_map=synch_free_free_map5 ; } //AWS20110624
   if(survey_profile_number== 6){free_free_map=free_free_map6 ;synch_free_free_map=synch_free_free_map6 ; } //AWS20110624
   if(survey_profile_number== 7){free_free_map=free_free_map7 ;synch_free_free_map=synch_free_free_map7 ; } //AWS20110624
   if(survey_profile_number== 8){free_free_map=free_free_map8 ;synch_free_free_map=synch_free_free_map8 ; } //AWS20110624
   if(survey_profile_number== 9){free_free_map=free_free_map9 ;synch_free_free_map=synch_free_free_map9 ; } //AWS20110624
   if(survey_profile_number==10){free_free_map=free_free_map10;synch_free_free_map=synch_free_free_map10; } //AWS20110624
   if(survey_profile_number==11){free_free_map=free_free_map11;synch_free_free_map=synch_free_free_map11; } //AWS20110624
   if(survey_profile_number==12){free_free_map=free_free_map12;synch_free_free_map=synch_free_free_map12; } //AWS20110624
   if(survey_profile_number==13){free_free_map=free_free_map13;synch_free_free_map=synch_free_free_map13; } //AWS20110624
   if(survey_profile_number==14){free_free_map=free_free_map14;synch_free_free_map=synch_free_free_map14; } //AWS20110624
   if(survey_profile_number==15){free_free_map=free_free_map15;synch_free_free_map=synch_free_free_map15; } //AWS20110624
   if(survey_profile_number==16){free_free_map=free_free_map16;synch_free_free_map=synch_free_free_map16; } //AWS20110624


   if(survey_profile_number==13){thermal_dust_map = thermal_dust_map13; }                                   //AWS20120629
   if(survey_profile_number==14){thermal_dust_map = thermal_dust_map14; }                                   //AWS20120629
   if(survey_profile_number==15){thermal_dust_map = thermal_dust_map15; }                                   //AWS20120629
   if(survey_profile_number==16){thermal_dust_map = thermal_dust_map16; }                                   //AWS20120629


   if(survey_profile_number==16){spinning_dust_map=spinning_dust_map16; }                                   //AWS20120629

  ////////////////// plot survey (moved from after model plots except Draw) AWS20120430


     // -------------------------- longitude profile

 if(longprof)
 {
  survey_profile=     survey_map ->ProjectionX("survey " , ib_min1,ib_max1);        //AWS20070525
  survey_profile->Add(survey_map ->ProjectionX("survey2" , ib_min2,ib_max2),1.0);   //AWS20070525

  survey_profile->Scale(1.0/(ib_max1-ib_min1+1 + ib_max2-ib_min2+1));

  survey_profile->SetStats(kFALSE);
  survey_profile->SetLineStyle(1     );// 1=solid 2=dash
  survey_profile->SetLineWidth(linewidth     );
  survey_profile->SetLineColor(kBlack);//AWS20110624 was kRed

  

  }// if longprof

     // -------------------------- latitude  profile

  if(latprof )
  {
  survey_profile=     survey_map ->ProjectionY("survey3"     , il_min1,il_max1);       //AWS20070525
  survey_profile->Add(survey_map ->ProjectionY("survey4"     , il_min2,il_max2),1.0);  //AWS20070525

  survey_profile->Scale(1./(il_max1-il_min1+1 +il_max2-il_min2+1 ));

  //fix to avoid last bin being zero and spoiling the plot
  survey_profile->SetBinContent(                              survey_profile->GetNbinsX(),         //AWS20110221
                                survey_profile->GetBinContent(survey_profile->GetNbinsX()-1) );    //AWS20110221
  
  survey_profile->SetStats(kFALSE);
  survey_profile->SetLineStyle(1     );// 1=solid 2=dash
  survey_profile->SetLineWidth(linewidth     );
  survey_profile->SetLineColor(kBlack);//AWS20110624 was kRed
  

  }// if latprof





     // -------------------------- longitude profile 

  if(longprof)
    {

  sprintf(workstring2,"  %5.1f>b>%5.1f,%5.1f>b>%5.1f      ",
                         galplotdef.lat_min1,galplotdef.lat_max1,
                         galplotdef.lat_min2,galplotdef.lat_max2);
  strcpy(workstring1,canvastitle);
  strcat(workstring1,workstring2);

  strcat(name,"total_longitude");// to identify it: name is not plotted
  sprintf(workstring5,"%d",survey_profile_number);  //AWS20070608
  strcat(name,       workstring5);                  //AWS20070608
  


  c1=new TCanvas(name,canvastitle, 50+survey_profile_number*20,150+survey_profile_number*20,600,600); //AWS20070608

  if(galplotdef.long_log_scale==1)c1->SetLogy(); //AWS20081204            

  sprintf(workstring5,"longitude profile total1 %d",survey_profile_number);                                  //AWS20070608
  
  profile=     synch_map ->ProjectionX(workstring5 , ib_min1,ib_max1);                                   //AWS20070608

  //  cout<<"synch           map profile "<<survey_profile_number<<" first bin="<<synch_map->GetBinContent(0,0)<<endl;
  //  cout<<"synch longitude profile     "<<survey_profile_number<<" first bin="<<profile  ->GetBinContent(0)  <<endl;


  sprintf(workstring5,"longitude profile total2 %d",survey_profile_number);                                  //AWS20070608

  profile->Add(synch_map ->ProjectionX(workstring5 , ib_min2,ib_max2),1.0);                              //AWS20070608

  profile->Scale(1.0/(ib_max1-ib_min1+1 + ib_max2-ib_min2+1));
  profile->GetXaxis()->SetTitle("Galactic longitude");       //AWS20090609 since done separately
  profile->GetXaxis()->SetNdivisions(0); 

  if(I_or_Tb==1)
  profile->GetYaxis()->SetTitle("intensity, erg cm^{-2} sr^{-1} s^{-1} Hz^{-1} ");
  if(I_or_Tb==2)
  profile->GetYaxis()->SetTitle("brightness temperature, K");

  profile->GetYaxis()->SetTitleOffset(1.30); //AWS20120628 was 1.2, move left due to more decimals AWS20121130 was 1.45, move right for larger size
  profile->GetYaxis()->SetTitleSize  (0.040);// seems to be problematic AWS20121130 reinstate
  profile->GetYaxis()->SetLabelSize  (0.030);

  profile->SetTitle(""         ); //written in box on plot

 


  // scale the plots
  //AWS20120430 choose profile for scaling plots. model was original choice 

  int scale_plots_option=1; // 0=use model, 1=use survey
 
  if(scale_plots_option==0)scale_profile = profile;
  if(scale_plots_option==1)scale_profile = survey_profile; //AWS20120430

  double scale_profile_min; //AWS20120628
  
  if(galplotdef.long_log_scale==0)                 //AWS20081218
  {
                                      profile->SetMaximum(scale_profile->GetMaximum()* 1.5); // to allow for data with larger range increased from 1.5  AWS20110428 scale_profile AWS20120430 3.0->1.5 using survey

   if(survey_profile_number==14)      profile->SetMaximum(scale_profile->GetMaximum()* 2.0);// U: WMAP data has large fluctuations AWS20120627 was 40.0

   if(survey_profile_number==16)      profile->SetMaximum(scale_profile->GetMaximum()* 1.5);// allow for free-free 5.0->1.5 using survey


   /*
   if(survey_profile_number==13) 
                     if(scale_profile->GetMinimum()<0.0)
    profile->SetMinimum(scale_profile->GetMinimum()* 2.0);// Q: in case it goes -ve  AWS20120627 was 20.0
   */

   scale_profile_min=0.;

   if(survey_profile_number==13) scale_profile_min=scale_profile->GetMinimum()* 2.0; // Q goes -ve AWS20120628
   if(survey_profile_number==14) scale_profile_min=scale_profile->GetMinimum()* 2.0; // U goes -ve AWS20120628

   profile->SetMinimum(scale_profile_min); //AWS20120628

   //   if(survey_profile_number==14)      profile->SetMinimum(scale_profile->GetMinimum()* 2.0);// U: goes -ve, and data has large fluctations  AWS20120627 was 40.0  //AWS20120628
  }



 

  if(galplotdef.long_log_scale==1)
  {
                                     profile->SetMaximum(scale_profile->GetMaximum()* 3.0);// scale_profile AWS20120430
   if(survey_profile_number==14)     profile->SetMaximum(scale_profile->GetMaximum()*100.0);// U: WMAP data has large fluctuations
   if(survey_profile_number==16)     profile->SetMaximum(scale_profile->GetMaximum()* 5.0);// allow for free-free
                                                     
                                     profile->SetMinimum(scale_profile->GetMaximum()/1e3);   
  }


  TGaxis::SetMaxDigits(4);  // see root #1329 and mail root 20 Aug 2002

  profile->SetStats(kFALSE);
  profile->SetLineStyle(1     );// 1=solid 2=dash
  profile->SetLineWidth(linewidth     );//AWS20110706
  profile->SetLineColor(kBlue);//AWS20050916

  if(options==0) profile->Fill(0.);   // since Draw is required to get plot ... does not work, no change
  if(options==0) profile->SetLineColor(kWhite);// since Draw is required to get plot
  profile->Draw("L");//L=line

  // longitude axis position takes -ve profile into account, for linear scales  AWS20120628

  if(galplotdef.long_log_scale==0)                 //AWS20081218
  {
   axis1=new TGaxis(-180, scale_profile_min,  0., scale_profile_min, "f1",       9,""                );
   axis1->SetLabelSize(0.02);
   axis1->SetTitleSize(0.040); //AWS20121130 added, consistent with latitude

   axis2=new TGaxis(  20, scale_profile_min,180., scale_profile_min, "f2",       8,""                );
   axis2->SetLabelSize(0.02);
   axis2->SetTitle("Galactic longitude");
   axis2->SetTitleSize(0.040); //AWS20121130 added, consistent with latitude

  }

  axis1->Draw();   axis2->Draw(); 
  //axis3->Draw(); AWS20120628

         
  if(options==1)
  {

  // free-free        
  if(survey_profile_number <8 ||survey_profile_number==16) // only total, not polarized
  {                                                              //AWS20110624
  free_free_profile=     free_free_map ->ProjectionX("freefree" , ib_min1,ib_max1);       
  free_free_profile->Add(free_free_map ->ProjectionX("freefree2" ,ib_min2,ib_max2),1.0); 

  free_free_profile->Scale(1.0/(ib_max1-ib_min1+1 + ib_max2-ib_min2+1));

  free_free_profile->SetStats(kFALSE);
  free_free_profile->SetLineStyle(1     );// 1=solid 2=dash
  free_free_profile->SetLineWidth(linewidth     );//AWS20110706
  free_free_profile->SetLineColor(kGreen);
  free_free_profile->Draw("L same");//L=line

  synch_free_free_profile=     synch_free_free_map ->ProjectionX("synch_freefree" , ib_min1,ib_max1);       
  synch_free_free_profile->Add(synch_free_free_map ->ProjectionX("synch_freefree2" ,ib_min2,ib_max2),1.0); 

  synch_free_free_profile->Scale(1.0/(ib_max1-ib_min1+1 + ib_max2-ib_min2+1));

  synch_free_free_profile->SetStats(kFALSE);
  synch_free_free_profile->SetLineStyle(1     );// 1=solid 2=dash
  synch_free_free_profile->SetLineWidth(linewidth     );
  synch_free_free_profile->SetLineColor(kRed);
  synch_free_free_profile->Draw("L same");//L=line

 }

  // thermal dust
    if(survey_profile_number >=13 && survey_profile_number<=16) // polarized and totel
  {                                                             
  thermal_dust_profile=     thermal_dust_map ->ProjectionX("thermal dust" , ib_min1,ib_max1);       
  thermal_dust_profile->Add(thermal_dust_map ->ProjectionX("thermal dust2" ,ib_min2,ib_max2),1.0); 

  thermal_dust_profile->Scale(1.0/(ib_max1-ib_min1+1 + ib_max2-ib_min2+1));

  thermal_dust_profile->SetStats(kFALSE);
  thermal_dust_profile->SetLineStyle(1     );// 1=solid 2=dash
  thermal_dust_profile->SetLineWidth(linewidth        );
  thermal_dust_profile->SetLineColor(kOrange);
  thermal_dust_profile->Draw("L same");//L=line



 }

    // spinning dust
  if(survey_profile_number == 16 ) // not polarized so only I                          
  {                                                             
  spinning_dust_profile=     spinning_dust_map ->ProjectionX("spinning dust" , ib_min1,ib_max1);       
  spinning_dust_profile->Add(spinning_dust_map ->ProjectionX("spinning dust2" ,ib_min2,ib_max2),1.0); 

  spinning_dust_profile->Scale(1.0/(ib_max1-ib_min1+1 + ib_max2-ib_min2+1));

  spinning_dust_profile->SetStats(kFALSE);
  spinning_dust_profile->SetLineStyle(2     );// 1=solid 2=dash
  spinning_dust_profile->SetLineWidth(linewidth     );
  spinning_dust_profile->SetLineColor(kOrange);
  spinning_dust_profile->Draw("L same");//L=line
  }

  }// options==1



  //                                          synchrotron from clouds


  // replace profile name!
 {                                                             
  spinning_dust_profile=     synch_clouds_map ->ProjectionX("synch clouds" , ib_min1,ib_max1);       
  spinning_dust_profile->Add(synch_clouds_map ->ProjectionX("synch clouds2" ,ib_min2,ib_max2),1.0); 

  spinning_dust_profile->Scale(1.0/(ib_max1-ib_min1+1 + ib_max2-ib_min2+1));

  spinning_dust_profile->SetStats(kFALSE);
  spinning_dust_profile->SetLineStyle(1     );// 1=solid 2=dash
  linewidth=2;
  spinning_dust_profile->SetLineWidth(linewidth     );
  spinning_dust_profile->SetLineColor(kRed);
  spinning_dust_profile->Draw("L same");//L=line



  spinning_dust_profile=     synch_HI_map ->ProjectionX("synch HI" , ib_min1,ib_max1);       
  spinning_dust_profile->Add(synch_HI_map ->ProjectionX("synch HI2" ,ib_min2,ib_max2),1.0); 

  spinning_dust_profile->Scale(1.0/(ib_max1-ib_min1+1 + ib_max2-ib_min2+1));

  spinning_dust_profile->SetStats(kFALSE);
  spinning_dust_profile->SetLineStyle(1     );// 1=solid 2=dash
  linewidth=2;
  spinning_dust_profile->SetLineWidth(linewidth     );
  spinning_dust_profile->SetLineColor(kBlue);
  spinning_dust_profile->Draw("L same");//L=line



  }


    }// if longprof

  // -------------------------- latitude profile

  if(latprof)
    {
     
  sprintf(workstring2,"  %5.1f>l>%5.1f,%5.1f>l>%5.1f       ",
                 	  galplotdef.long_min1,galplotdef.long_max1,
                          galplotdef.long_min2,galplotdef.long_max2);
  strcpy(workstring1,canvastitle);
  strcat(workstring1,workstring2);



  strcat(name,"total_latitude");
  c1=new TCanvas(name,canvastitle,600+survey_profile_number*20,150+survey_profile_number*20,600,600); //AWS20070525
  if(galplotdef.lat_log_scale==1)c1->SetLogy();                //a TCanvas is a TPad, TPad has this function

  profile=     synch_map ->ProjectionY("total3"     , il_min1,il_max1);                               //AWS20070525
  profile->Add(synch_map ->ProjectionY("total4"     , il_min2,il_max2),1.0);                          //AWS20070525
  profile->Scale(1./(il_max1-il_min1+1 +il_max2-il_min2+1 ));

  profile->GetXaxis()->SetTitle("Galactic latitude");
  profile->GetXaxis()->SetTitleSize(0.040); //AWS20120628 like longitude AWS20121130
  profile->GetXaxis()->SetTitleFont(62)  ; //AWS20120628 like longitude

  profile->GetXaxis()->SetLabelSize(0.03); //AWS20120628 


  if(I_or_Tb==1)
  profile->GetYaxis()->SetTitle("intensity,  erg cm^{-2} sr^{-1} s^{-1} Hz^{-1} ");
  if(I_or_Tb==2)
  profile->GetYaxis()->SetTitle("brightness temperature, K");

  profile->GetYaxis()->SetTitleOffset(1.30); //AWS20120628 was 1.2, move left AWS20121130 was 1.4, move right for larger size
  profile->GetYaxis()->SetTitleSize  (0.040);// seems to be problematic AWS20121130 reinstated
  profile->GetYaxis()->SetLabelSize  (0.030);
  profile->SetTitle(""         ); //written in box on plot

  // scale the plots
  //AWS20120430 choose profile for scaling plots. model was original choice 

  int scale_plots_option=1; // 0=use model, 1=use survey     AWS20120528
 
  if(scale_plots_option==0)scale_profile = profile;        //AWS20120528
  if(scale_plots_option==1)scale_profile = survey_profile; //AWS20120528


  if(galplotdef.lat_log_scale==0)
  {
                                     profile->SetMaximum(scale_profile->GetMaximum()*2.0); // to allow for data with larger range  AWS20120627 was 3.0
   if(survey_profile_number==14)     profile->SetMaximum(scale_profile->GetMaximum()*2.0  );// U: WMAP data has large fluctuations AWS20120627 was 100.0
   if(survey_profile_number==16)     profile->SetMaximum(scale_profile->GetMaximum()*5.0);// allow for free-free

   if(survey_profile_number==13)  
                     if(scale_profile->GetMinimum()<0.0)
                              profile->SetMinimum(scale_profile->GetMinimum()* 2.0);             // Q: in case it goes -ve  AWS20120627 was  20.0

   if(survey_profile_number==14)     profile->SetMinimum(scale_profile->GetMinimum()* 2.0);// U: goes -ve, and data has large fluctations  AWS20120627 was  50.0
  }

  /*                                               //AWS20110502 since Q,U include -ve  values
  if(galplotdef.lat_log_scale==0) //AWS20090609
   profile->SetMinimum(0.0);      //AWS20090609
  */

  if(galplotdef.lat_log_scale==1)
  {
                                     profile->SetMaximum(scale_profile->GetMaximum()* 5.0);//AWS20110624 changed from 3.0
   if(survey_profile_number==14)     profile->SetMaximum(scale_profile->GetMaximum()*50.0);// U: WMAP data has large fluctuations
   if(survey_profile_number==16)     profile->SetMaximum(scale_profile->GetMaximum()* 5.0);// allow for free-free
                                     profile->SetMinimum(scale_profile->GetMaximum() /1e3);//AWS20110624 changed from 1e2
  }


  TGaxis::SetMaxDigits(4); // see root #1329 and mail root 20 Aug 2002

  profile->SetStats(kFALSE);
  profile->SetLineStyle(1     );// 1=solid 2=dash
  profile->SetLineWidth(linewidth     );
  profile->SetLineColor(kBlue);//AWS20050916
  profile->Draw("L");//L=line

  // free-free        
  if(survey_profile_number <8 ||survey_profile_number==16) // only total, not polarized
  {                                                              //AWS20110624
  free_free_profile=     free_free_map ->ProjectionY("freefree lat" , il_min1,il_max1);      //note title change to avoid problems 
  free_free_profile->Add(free_free_map ->ProjectionY("freefree2 lat" ,il_min2,il_max2),1.0); 

  free_free_profile->Scale(1.0/(il_max1-il_min1+1 + il_max2-il_min2+1));

  free_free_profile->SetStats(kFALSE);
  free_free_profile->SetLineStyle(1     );// 1=solid 2=dash
  free_free_profile->SetLineWidth(linewidth     );
  free_free_profile->SetLineColor(kGreen);
  free_free_profile->Draw("L same");//L=line

  synch_free_free_profile=     synch_free_free_map ->ProjectionY("synch_freefree lat" , il_min1,il_max1);       
  synch_free_free_profile->Add(synch_free_free_map ->ProjectionY("synch_freefree2 lat" ,il_min2,il_max2),1.0); 

  synch_free_free_profile->Scale(1.0/(il_max1-il_min1+1 + il_max2-il_min2+1));

  synch_free_free_profile->SetStats(kFALSE);
  synch_free_free_profile->SetLineStyle(1     );// 1=solid 2=dash
  synch_free_free_profile->SetLineWidth(linewidth     );
  synch_free_free_profile->SetLineColor(kRed);
  synch_free_free_profile->Draw("L same");//L=line

 }



  // thermal dust
    if(survey_profile_number >=13 && survey_profile_number<=16) // polarized and totel
  {                                                             
  thermal_dust_profile=     thermal_dust_map ->ProjectionY("thermal dust lat" , il_min1,il_max1);       
  thermal_dust_profile->Add(thermal_dust_map ->ProjectionY("thermal dust2 lat" ,il_min2,il_max2),1.0); 

  thermal_dust_profile->Scale(1.0/(il_max1-il_min1+1 + il_max2-il_min2+1));

  thermal_dust_profile->SetStats(kFALSE);
  thermal_dust_profile->SetLineStyle(1     );// 1=solid 2=dash
  thermal_dust_profile->SetLineWidth(linewidth        );
  thermal_dust_profile->SetLineColor(kOrange);
  thermal_dust_profile->Draw("L same");//L=line



 }

    // spinning dust
  if(survey_profile_number == 16 ) // not polarized so only I                          
  {                                                             
  spinning_dust_profile=     spinning_dust_map ->ProjectionY("spinning dust lat" , il_min1,il_max1);       
  spinning_dust_profile->Add(spinning_dust_map ->ProjectionY("spinning dust2 lat" ,il_min2,il_max2),1.0); 

  spinning_dust_profile->Scale(1.0/(il_max1-il_min1+1 + il_max2-il_min2+1));

  spinning_dust_profile->SetStats(kFALSE);
  spinning_dust_profile->SetLineStyle(2     );// 1=solid 2=dash
  spinning_dust_profile->SetLineWidth(linewidth     );
  spinning_dust_profile->SetLineColor(kOrange);
  spinning_dust_profile->Draw("L same");//L=line
  }



 //                                          synchrotron from clouds


  // replace profile name!
                                                             
  

  {                                                             
  spinning_dust_profile=     synch_clouds_map ->ProjectionY("synch clouds  lat" ,il_min1,il_max1);       
  spinning_dust_profile->Add(synch_clouds_map ->ProjectionY("synch clouds2 lat" ,il_min2,il_max2),1.0); 

  spinning_dust_profile->Scale(1.0/(il_max1-il_min1+1 + il_max2-il_min2+1));

  spinning_dust_profile->SetStats(kFALSE);
  spinning_dust_profile->SetLineStyle(1     );// 1=solid 2=dash
                                      linewidth=2;
  spinning_dust_profile->SetLineWidth(linewidth     );
  spinning_dust_profile->SetLineColor(kRed);
  spinning_dust_profile->Draw("L same");//L=line
  }






    }//if latprof





  // moved to before models, except for Draw AWS20120430

    survey_profile->Draw("L same");//L=line

  
  ////////////////// plot survey

    /*

     // -------------------------- longitude profile

 if(longprof)
 {
  survey_profile=     survey_map ->ProjectionX("survey " , ib_min1,ib_max1);        //AWS20070525
  survey_profile->Add(survey_map ->ProjectionX("survey2" , ib_min2,ib_max2),1.0);   //AWS20070525

  survey_profile->Scale(1.0/(ib_max1-ib_min1+1 + ib_max2-ib_min2+1));

  survey_profile->SetStats(kFALSE);
  survey_profile->SetLineStyle(1     );// 1=solid 2=dash
  survey_profile->SetLineWidth(linewidth     );
  survey_profile->SetLineColor(kBlack);//AWS20110624 was kRed
  survey_profile->Draw("L same");//L=line

  }// if longprof

     // -------------------------- latitude  profile

  if(latprof )
  {
  survey_profile=     survey_map ->ProjectionY("survey3"     , il_min1,il_max1);       //AWS20070525
  survey_profile->Add(survey_map ->ProjectionY("survey4"     , il_min2,il_max2),1.0);  //AWS20070525

  survey_profile->Scale(1./(il_max1-il_min1+1 +il_max2-il_min2+1 ));

  //fix to avoid last bin being zero and spoiling the plot
  survey_profile->SetBinContent(                              survey_profile->GetNbinsX(),         //AWS20110221
                                survey_profile->GetBinContent(survey_profile->GetNbinsX()-1) );    //AWS20110221
  
  survey_profile->SetStats(kFALSE);
  survey_profile->SetLineStyle(1     );// 1=solid 2=dash
  survey_profile->SetLineWidth(linewidth     );
  survey_profile->SetLineColor(kBlack);//AWS20110624 was kRed
  survey_profile->Draw("L same");//L=line

  }// if latprof

    */

  //////////////////////////////////////////////////////////////////////////////////////////////

  // parameters labelling

  sprintf(workstring1,"  %5.2f<l<%5.2f , %5.2f<l<%5.2f",               
                          galplotdef.long_min1,galplotdef.long_max1,
                          galplotdef.long_min2,galplotdef.long_max2);
 
  sprintf(workstring2,"  %5.2f<b<%5.2f , %5.2f<b<%5.2f",                
                         galplotdef.lat_min1,galplotdef.lat_max1,
                         galplotdef.lat_min2,galplotdef.lat_max2);
  
  if(survey_profile_number== 1)sprintf(workstring3," %5.0f MHz",galaxy.nu_synch[ips1]/1.e6); //AWS20070525
  if(survey_profile_number== 2)sprintf(workstring3," %5.0f MHz",galaxy.nu_synch[ips2]/1.e6); //AWS20070525
  if(survey_profile_number== 3)sprintf(workstring3," %5.0f MHz",galaxy.nu_synch[ips3]/1.e6); //AWS20070525
  if(survey_profile_number== 4)sprintf(workstring3," %5.0f MHz",galaxy.nu_synch[ips4]/1.e6); //AWS20070525
  if(survey_profile_number== 5)sprintf(workstring3," %5.0f MHz",galaxy.nu_synch[ips5]/1.e6);
  if(survey_profile_number== 6)sprintf(workstring3," %5.0f MHz",galaxy.nu_synch[ips6]/1.e6);
  if(survey_profile_number== 7)sprintf(workstring3," %5.0f MHz",galaxy.nu_synch[ips7]/1.e6);
  if(survey_profile_number== 8)sprintf(workstring3," %5.0f MHz",galaxy.nu_synch[ips8]/1.e6);
  if(survey_profile_number== 9)sprintf(workstring3," %5.0f MHz",galaxy.nu_synch[ips9]/1.e6);
  if(survey_profile_number==10)sprintf(workstring3," %5.0f MHz",galaxy.nu_synch[ips10]/1.e6);
  if(survey_profile_number==11)sprintf(workstring3," %5.0f MHz",galaxy.nu_synch[ips11]/1.e6);
  if(survey_profile_number==12)sprintf(workstring3," %5.0f MHz",galaxy.nu_synch[ips12]/1.e6);
  if(survey_profile_number==13)sprintf(workstring3," %5.0f MHz",galaxy.nu_synch[ips13]/1.e6);//AWS20110426
  if(survey_profile_number==14)sprintf(workstring3," %5.0f MHz",galaxy.nu_synch[ips14]/1.e6);//AWS20110426
  if(survey_profile_number==15)sprintf(workstring3," %5.0f MHz",galaxy.nu_synch[ips15]/1.e6);//AWS20110426
  if(survey_profile_number==16)sprintf(workstring3," %5.0f MHz",galaxy.nu_synch[ips16]/1.e6);//AWS20110426

  strcpy(workstring4," galdef ID ");
  strcat(workstring4,galdef.galdef_ID);


  text=new TText();                          //AWS20070525
  text->SetTextFont(62);
  text->SetTextColor(kBlack);
  text->SetTextSize(0.025 ); //AWS20121130 was 0.025
  text->SetTextAlign(12);

  if(longprof)
  text->DrawTextNDC(.52 ,.85 ,workstring2);// NDC=normalized coord system   latitude range   //AWS20121130 was .50
  if(latprof)
  text->DrawTextNDC(.54 ,.85 ,workstring1);// NDC=normalized coord system  longitude range   //AWS20121130 was .52

  text->DrawTextNDC(.62 ,.88 ,workstring3);// NDC=normalized coord system  MHz               //AWS20121130 was .58

  text->SetTextSize(0.025 );                                            //AWS20121130 was 0.034 as    spectra GALDEF ID outside box
  text->DrawTextNDC(.18 ,.92 ,workstring4);// NDC=normalized coord system AWS20121130 was 0.20, 0.93



  // WMAP 7-year Q U P I
 
  text->SetTextSize(0.040 );//AWS20121130

  if(survey_profile_number>= 8 && survey_profile_number<= 12  )
                               text->DrawTextNDC(.15 ,.86 ,"synchrotron" );// NDC=normalized coord system AWS20121130
  if(survey_profile_number==13)text->DrawTextNDC(.15 ,.86 ,"Stokes Q"    );// NDC=normalized coord system AWS20121130 aws .20,.86
  if(survey_profile_number==14)text->DrawTextNDC(.15 ,.86 ,"Stokes U"    );// NDC=normalized coord system
  if(survey_profile_number==15)text->DrawTextNDC(.15 ,.86 ,"Stokes P"    );// NDC=normalized coord system
  if(survey_profile_number==16)text->DrawTextNDC(.15 ,.86 ,"Stokes I"    );// NDC=normalized coord system

 

 


//============================================================================

  //============== postscript output

  sprintf(workstring1,"l_%.2f_%.2f_%.2f_%.2f",                           //AWS20040315
                          galplotdef.long_min1,galplotdef.long_max1,
                          galplotdef.long_min2,galplotdef.long_max2);

  sprintf(workstring2,"b_%.2f_%.2f_%.2f_%.2f",                           //AWS20040315
                          galplotdef.lat_min1,galplotdef.lat_max1,
                          galplotdef.lat_min2,galplotdef.lat_max2);

  if(survey_profile_number==1)   sprintf(workstring3,"%.0f_MHz",galaxy.nu_synch[ips1]/1.e6);           //AWS20070525
  if(survey_profile_number==2)   sprintf(workstring3,"%.0f_MHz",galaxy.nu_synch[ips2]/1.e6);           //AWS20070525
  if(survey_profile_number==3)   sprintf(workstring3,"%.0f_MHz",galaxy.nu_synch[ips3]/1.e6);           //AWS20070525
  if(survey_profile_number==4)   sprintf(workstring3,"%.0f_MHz",galaxy.nu_synch[ips4]/1.e6);           //AWS20070525
  if(survey_profile_number==5)   sprintf(workstring3,"%.0f_MHz",galaxy.nu_synch[ips5]/1.e6);           //AWS20070529
  if(survey_profile_number==6)   sprintf(workstring3,"%.0f_MHz",galaxy.nu_synch[ips6]/1.e6);           //AWS20070529
  if(survey_profile_number==7)   sprintf(workstring3,"%.0f_MHz",galaxy.nu_synch[ips7]/1.e6);           //AWS20070529
  if(survey_profile_number==8)   sprintf(workstring3,"%.0f_MHz",galaxy.nu_synch[ips8]/1.e6);           //AWS20070529
  if(survey_profile_number==9)   sprintf(workstring3,"%.0f_MHz",galaxy.nu_synch[ips9]/1.e6);           //AWS20070608
  if(survey_profile_number==10)  sprintf(workstring3,"%.0f_MHz",galaxy.nu_synch[ips10]/1.e6);          //AWS20070608
  if(survey_profile_number==11)  sprintf(workstring3,"%.0f_MHz",galaxy.nu_synch[ips11]/1.e6);          //AWS20070608
  if(survey_profile_number==12)  sprintf(workstring3,"%.0f_MHz",galaxy.nu_synch[ips12]/1.e6);          //AWS20070608
  if(survey_profile_number==13)  sprintf(workstring3,"%.0f_MHz",galaxy.nu_synch[ips13]/1.e6);          //AWS20110426
  if(survey_profile_number==14)  sprintf(workstring3,"%.0f_MHz",galaxy.nu_synch[ips14]/1.e6);          //AWS20110426
  if(survey_profile_number==15)  sprintf(workstring3,"%.0f_MHz",galaxy.nu_synch[ips15]/1.e6);          //AWS20110426
  if(survey_profile_number==16)  sprintf(workstring3,"%.0f_MHz",galaxy.nu_synch[ips16]/1.e6);          //AWS20110426


                                 strcpy(psfile,"plots/");
                                 strcat(psfile,"synchrotron_");
  if(survey_profile_number==13)  strcat(psfile,"Q_");                                            //AWS20110428
  if(survey_profile_number==14)  strcat(psfile,"U_");                                            //AWS20110428
  if(survey_profile_number==15)  strcat(psfile,"P_");                                            //AWS20110428
  if(survey_profile_number==16)  strcat(psfile,"I_");                                            //AWS20110428

                                 strcat(psfile,"clouds_");

  if(longprof==1)                strcat(psfile,"longitude_profile_");
  if( latprof==1)                strcat(psfile,"latitude_profile_" );

  strcat(psfile,galdef.galdef_ID);
  strcat(psfile,"_"        );
  strcat(psfile,workstring3);
  strcat(psfile,"_"        );
  if(longprof==1)
  strcat(psfile,workstring2);
  if( latprof==1)
  strcat(psfile,workstring1);

  
  strcat(psfile,"_"        );
  strcat(psfile,galplotdef.psfile_tag);
  strcpy(giffile,psfile);

  strcat(psfile,".eps");

  cout<<"postscript file="<<psfile<<endl;
  c1->Print(psfile,"eps");
  strcat(giffile,".gif");
  cout<<"gif        file="<<giffile<<endl;
  c1->Print(giffile,"gif");
  //==============


 } // loop over survey profiles AWS20070525


 

  

 



   cout<<" <<<< plot_synchrotron_clouds_profile   "<<endl;
   return status;
}


 
