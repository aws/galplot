using namespace std;

#include <cmath>
#include<iostream>
#include <cstring>  //AWS20111010 for sles11 gcc 4.3.4
#include"fitsio.h"



#include"Isotropic.h"



//////////////////////////////////////////////////////
  
  
///////////////////////////////////////////////////////
  int Isotropic:: read (char *directory,char *filename) 
{
  char infile[500];

  char err_text[100];
  char comment[100];

  fitsfile *fptr;
  int status;
  int hdunum,hdutype,total_hdus,colnum;
  int n_columns,n_rows;
  double nulval=0.;
  int anynul   =0;
  char nulstr[100];strcpy(nulstr,"null");
  
  int i;

  cout<< "Isotropic::read  (" << directory<<" , "<<filename<<")" <<endl;
  
  strcpy(infile,directory);
  strcat(infile,filename);
  
  cout<<"Isotropic::read   infile= "<<infile<<endl;

  status=0; // needed
  fits_open_file(&fptr,infile,READONLY,&status) ;
  cout<<"FITS read open status= "<<status<<" "<<infile<<endl;

  if(status!=0){cout<<"isotropic background file "<<infile << " does not exist ! please provide a valid one. Exiting. "<<endl; exit(0); }

  fits_get_errstatus(status,err_text);
  cout<<err_text<<endl;

  fits_get_num_hdus(fptr,&total_hdus,&status);
  cout<<"total number of header units="<<total_hdus<<endl;

  

  fits_movnam_hdu(fptr,ANY_HDU,"ISOTROPIC",0,&status);
  cout<<"FITS movnam_hdu:"<<" status= "<<status<<   endl  ;


   fits_read_key(fptr,TLONG,"TFIELDS",&n_columns,comment,&status);
   cout<<" n_columns ="<<n_columns ;
   fits_read_key(fptr,TLONG,"NAXIS2",&n_rows  ,comment,&status);
   cout<<" n_rows    ="<<n_rows    <<endl;

  energy    =new double[n_rows];
  spectrum  =new double[n_rows];

  // fixup since this caused crash; string literal problem?
  /*
  fits_get_colnum(fptr,CASEINSEN,"energy",&colnum,&status);
  cout<<"energy    column ="<< colnum<<endl;
  */
  colnum=1; //AWS20191231  
  cout<<"set energy    column ="<< colnum<<endl;

  cout<<"skipping read for test"<<endl;
  /*
  for (i=0;i<n_rows;i++)
  fits_read_col(fptr, TDOUBLE, colnum,i+ 1,1,1,&nulval,&energy[i],   &anynul,&status);
  */

  // fixup since this caused crash; string literal problem?
  /*
  fits_get_colnum(fptr,CASEINSEN,"intensity",&colnum,&status);
  cout<<"intensity column ="<< colnum<<endl;
  */
  colnum=2; //AWS20191231

  n_energy=n_rows;

  cout<<"skipping read for test"<<endl;
  /*
  for (i=0;i<n_rows;i++)
    fits_read_col(fptr, TDOUBLE, colnum,i+ 1,1,1,&nulval,&spectrum[i],&anynul,&status);
  */
  for (i=0;i<n_rows;i++){cout<<" energy="<<energy[i]<<" spectrum = "<< spectrum[i]<<endl;}

  cout<<"<<< Isotropic.read"<<endl;
  return 0;
}

////////////////////////////////////////////////////////////////

void Isotropic:: print ()
{

  int i;
  cout<<"Isotropic::print"<<endl;
  for (i=0;i<n_energy;i++){cout<<" energy="<<energy[i]<<" MeV, intensity spectrum = "<< spectrum[i]<<" cm-2 sr-1 s-1 MeV-1"<<endl;}
  cout<<"end Isotropic::print"<<endl;
}

////////////////////////////////////////////////////////////////

double Isotropic:: interpolated(double energy_,int debug=0)
{
  double interpolated_;

 double A;
 int i;
 int found=0;
   


  for (i=0;i<n_energy-1;i++)
  {
    if(energy_ >= energy[i] && energy_ <= energy[i+1])
      {
        
	  if(debug==2)
          cout   <<  i<<" "  <<energy_<<" "<<energy  [i]<<" "<<  energy[i+1]
                                      <<" "<<spectrum[i]<<" "<<spectrum[i+1]<<endl;
          found=1;
          break;
      }
  }

  // logarithmic interpolation

  if (found==0) interpolated_= 0.;

  if (found==1)
  {
   interpolated_ = log(spectrum[i])
                  + (log(energy_)-log(energy[i]))/( log(energy  [i+1])-  log(energy  [i]) ) 
                                                 *( log(spectrum[i+1]) - log(spectrum[i]) )  ;

   interpolated_ = exp(interpolated_);
  }

  if(debug==1)
    cout<< "Isotropic::interpolated"  <<  i<<" "  <<energy_<<" "<<energy  [i]<<" "<<  energy[i+1]
                                               <<" "<<spectrum[i]<<" "<<spectrum[i+1]
	 <<" interpolated isotropic spectrum ="<<interpolated_<<endl;


  return interpolated_;
}

////////////////////////////////////////////////////////////////////////

double Isotropic:: integrated  (double energy_1, double energy_2, int debug=0) //AWS20081202
{
  // integration over energy interval using power-law approximation

  double integrated_;
  double s1,s2,A,g;

  s1=interpolated(energy_1);
  s2=interpolated(energy_2);
  integrated_=0.;

  if (s1>0.0 && s2>0.0)
  {
   g=log(s1/s2)/log(energy_1/energy_2);
   A=s1/pow(energy_1,g);

   if(g!= -1.0)   integrated_ = A * (pow(energy_2,g+1.) - pow(energy_1,g+1.)) / (g+1.);

   if(g== -1.0)   integrated_ = A * log(energy_2/energy_1);
  }

  if(debug==1)
  cout<<"Isotropic::integrated: energy_1= "<<energy_1<<" energy_2= "<<energy_2<<" s1= "<<s1<<" s2= "<<s2<<" g= "<<g<<" A="<<A<<" integrated_= "<<integrated_<<endl;





  return integrated_;
}
