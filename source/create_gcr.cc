
//**.****|****.****|****.****|****.****|****.****|****.****|****.****|****.****|
// * create_gcr.cc *                               galprop package * 08/16/2001 
//**"****!****"****!****"****!****"****!****"****!****"****!****"****!****"****|
#include"Galplot.h"                    //AWS20050920
#include"galprop_classes.h"
#include"galprop.h"
#include"constants.h"

int Galplot::create_gcr(int mode)              //AWS20130131
{
   cout<<" >>>> create_gcr mode="<<mode<<endl;
   galdef.print();
  
   int i=0,j,Z,A,Z1,A1, stat=0;
   char name[100];
   char *element[]=
      {
        "Hydrogen",    "Helium",   "Lithium", "Beryllium",     "Boron",
          "Carbon",  "Nitrogen",    "Oxygen",  "Fluorine",      "Neon",
	  "Sodium", "Magnesium", "Aluminium",   "Silicon","Phosphorus",
        "Sulfphur",  "Chlorine",     "Argon", "Potassium",   "Calcium",
        "Scandium",  "Titanium",  "Vanadium",  "Chromium", "Manganese",
            "Iron",    "Cobalt",    "Nickel",    "Copper",      "Zinc"
      };
   double t_half[2],branching_ratio;                                   // IMOS20010816
   int galdef_network_par=0;         // imos network, use everywhere
   int K_electron;                                                     // AWS20010731

// calculate the number of species
   if(!galdef.secondary_antiprotons) galdef.tertiary_antiprotons=0;    // IMOS20000802




   cout<<endl<<"Number of species to create: n_species= "<<n_species<<endl<<endl;
   if(!n_species) {cout<<"create_gcr.cc: No particles specified -exit"<<endl; exit(1);}


  if(mode==0)  // case species not yet defined                          //AWS20130131
  {

// create a Particle array
   gcr=new Particle[n_species];




   K_electron=0; // for all secondaries and non-nuclei                    AWS20010731


 
   for(i=0;i<n_species;i++)
   {
    

      gcr[i].init();                                            // arrays_assigned=0; AWS20130131

      strcpy(name,"no name");
      Z=1;  A=0;  t_half[0]=0.0;                                                

      gcr[i].primary_abundance=0.0;

      if(galdef.n_spatial_dimensions==2)
         gcr[i].init(name,Z,A,K_electron,t_half[0],                              
            galdef.r_min,  galdef.r_max, galdef.dr,  
            galdef.z_min,  galdef.z_max, galdef.dz,
            galdef.   p_min,  galdef.   p_max, galdef.   p_factor,
            galdef.Ekin_min,  galdef.Ekin_max, galdef.Ekin_factor,
            galdef.p_Ekin_grid);  

      if(galdef.n_spatial_dimensions==3)
         gcr[i].init(name,Z,A,K_electron,t_half[0],                               
            galdef.x_min,  galdef.x_max, galdef.dx,  
            galdef.y_min,  galdef.y_max, galdef.dy,
            galdef.z_min,  galdef.z_max, galdef.dz,
            galdef.   p_min,  galdef.   p_max, galdef.   p_factor,
            galdef.Ekin_min,  galdef.Ekin_max, galdef.Ekin_factor,
            galdef.p_Ekin_grid); 

      gcr[i].print(); 
      cout<<"============== completed creation of "<<gcr[i].name<<endl<<endl;
    
   }

  }//mode==0                                                                      //AWS20130131



  if(mode==1)  // case species defined from reading data                          //AWS20130131
  {
   for(i=0;i<n_species;i++)
   {
    
      strcpy(name,gcr[i].name);// assumed now defined
      Z=gcr[i].Z;  A=gcr[i].A; // assumed now defined
      t_half[0]=0.0;                                                

      gcr[i].primary_abundance=0.0;

      if(galdef.n_spatial_dimensions==2)
         gcr[i].init(name,Z,A,K_electron,t_half[0],                            
            galdef.r_min,  galdef.r_max, galdef.dr,  
            galdef.z_min,  galdef.z_max, galdef.dz,
            galdef.   p_min,  galdef.   p_max, galdef.   p_factor,
            galdef.Ekin_min,  galdef.Ekin_max, galdef.Ekin_factor,
            galdef.p_Ekin_grid);  

      if(galdef.n_spatial_dimensions==3)
         gcr[i].init(name,Z,A,K_electron,t_half[0],                            
            galdef.x_min,  galdef.x_max, galdef.dx,  
            galdef.y_min,  galdef.y_max, galdef.dy,
            galdef.z_min,  galdef.z_max, galdef.dz,
            galdef.   p_min,  galdef.   p_max, galdef.   p_factor,
            galdef.Ekin_min,  galdef.Ekin_max, galdef.Ekin_factor,
            galdef.p_Ekin_grid); 

      gcr[i].print(); 
      cout<<"============== completed creation of "<<gcr[i].name<<endl<<endl;
    
   }

  }//mode==1                                                                      //AWS20130131


   cout<<" <<<< create_gcr"<<endl;
   return stat;
}




