/*
!**********************************************************************
!    Title: Compute solid angle in bin in spherical system            |
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!                                                                     !
!                                                                     !
!    Input                                                   Channel  !
!    Y0         psi  centre of pixel 1 in FITS format (deg)  argument |
!    XBINSZ     chi  pixel size                       (deg)  argument |
!    YBINSZ     psi  pixel size                       (deg)  argument |
!    IY         psi bin                                      argument |
!                                                                     !
!    Output                                                  Channel  !
!    SABIN      solid angle of bin in steradians             function |
!                                                                     !
!    Subroutines called:                                              !
!    none                                                             |
!    Description:                                                     !
!     Computes solid angle in bin IY in psi direction.                |
!     Follows FITS conventions to define position of bin.             |
!     (chi,psi) coordinates related to  spherical coords              |
!     by :      chi = phi  , psi = pi/2 = theta                       |
!                                                                     !
!                                                                     !
!    Routine History:                                                 !
!                                                                     !
!    Date    Author     Description of Change           SPR NO.       !
!                                                                     !
! 20000225   A.Strong    first version from fortran                   |
! 20030924   A.Strong    IY starts at 0 for C/C++                     |
! 20060203   A.Strong    treat poles correctly                        |
!***********************************************************************
       FUNCTION SABIN(Y0,XBINSZ,YBINSZ,IY)
!***********************************************************************
*/

#include<math.h>
double sabin(double Y0,double XBINSZ,double YBINSZ,int IY)
{

 


  //    Local variables
      double DTR, A1, A2,pi,SABIN_;
      


 

      pi = acos(-1.0);
      DTR = pi/180.;
    
//    lower bound of bin
//    A1  =    (Y0 + (IY-1.5) * YBINSZ) * DTR;//AWS20030924
      A1  =    (Y0 + (IY-0.5) * YBINSZ) * DTR;//AWS20030924

 

//    upper bound of bin
//    A2  =    (Y0 + (IY-0.5) * YBINSZ) * DTR;//AWS20030924
      A2  =    (Y0 + (IY+0.5) * YBINSZ) * DTR;//AWS20030924

// case of bin centred on pole : solid angle between pole and boundary //AWS20060203
      if(A1 < -pi/2.) A1=  -pi/2.;                                     //AWS20060203
      if(A2 > +pi/2.) A2=  +pi/2.;                                     //AWS20060203

//    integral of  cos(psi)d(psi) from A1 to A2
      SABIN_=      (sin(A2)-sin(A1))*XBINSZ*DTR;



      return SABIN_;
}
