

#include"Galplot.h"                  
#include"galprop_classes.h"
#include"galprop.h"
#include"constants.h"
#include"fitsio.h"

#include <vector>

// plot multi-wavelength ISRF from radio to gamma

int Galplot::plot_mw_isrf()
 {

  cout<<" >>>> plot_mw_isrf    "<<endl;

  if(galdef.n_spatial_dimensions==3) //AWS20081117
  {cout<<" plot_mw_isrf only works at present for 2D"<< endl;
   return 0;
  }

  int ic=1,bremss=1, pi0=1, total=1;

  int status=0;

  int i_comp,i_lat,i_long,ip;
  int ii_long,ii_lat;
  int il_min, il_max, ib_min, ib_max;
  int il_min1,il_max1,ib_min1,ib_max1;
  int il_min2,il_max2,ib_min2,ib_max2;
  double long_min;
  double l,b,l_off;
  double ic_total;
  double ic_aniso_total;                                                                   //AWS20060511
  char name[100],canvastitle[100], workstring1[100],workstring2[100];
  char  psfile[400];
  char giffile[400];
  char txtfile[400];                               //AWS20041118
  char iosfile[400];                               //AWS20052411
  
  TCanvas *c1;
  TH3F* ic_map;
  TH3F* ic_aniso_map;                                                                      //AWS20060511
  TH3F* bremss_map;
  TH3F* pi0_decay_map;
  TH3F* isotropic_map;
  TH3F* total_map;
  TH3F* sourcepop1_map, *sourcepop1_sublimit_map, *sourcepop1_soplimit_map;                  //AWS20051111
  
  TGraph *spectrum;
  TGraph *spectrum_ic, *spectrum_ic_optical, *spectrum_ic_ir, *spectrum_ic_cmb, *spectrum_bremss, *spectrum_pi0, *spectrum_isotropic, *spectrum_total;//AWS20041117
  TGraph *spectrum_ic_aniso;                                                                 //AWS20060511   
  TGraph *spectrum_sourcepop1, *spectrum_sourcepop1_sublimit,*spectrum_sourcepop1_soplimit;  //AWS20051111
 
  TH1D *profile;
  TH1D *profile11,*profile12,*profile21,*profile22; // four (l,b) intervals
  TText *text;
  TLatex *latex;                                   //AWS20041021
  
  TH1F *h;                                         //AWS20040413
  
  double isotropic_intensity;
  double solid_angle;
 
  double x,y,y1,y2;
 
  int ncolors=1; int *colors=0;
  gStyle->SetPalette(ncolors,colors);
 
  // define line styles for SetLineStyle (only 1-4 predefined at root 5.08)       AWS20071005
  gStyle->SetLineStyleString(1," ");
  gStyle->SetLineStyleString(2,"12 12");
  gStyle->SetLineStyleString(3,"4 8");
  gStyle->SetLineStyleString(4,"12 16 4 16");
  gStyle->SetLineStyleString(5,"20 12 4 12");
  gStyle->SetLineStyleString(6,"20 12 4 12 4 12 4 12");
  gStyle->SetLineStyleString(7,"20 20");
  gStyle->SetLineStyleString(8,"20 12 4 12 4 12");
  gStyle->SetLineStyleString(9,"80 20");
  gStyle->SetLineStyleString(10,"80 40 4 40");



  // names must be different or canvas disappears
  
  strcpy(canvastitle,"mw_isrf galdef ID ");
  strcat(canvastitle,galdef.galdef_ID);

  ic_map = 
    new TH3F("IC spectrum", canvastitle,
	     galaxy.n_long, -180, 180.,
	     galaxy.n_lat, 
	     galaxy.lat_min, galaxy.lat_min + galaxy.d_lat*(galaxy.n_lat-1),
	     galaxy.n_E_gammagrid, 
	     log10(galaxy.E_gamma_min), log10(galaxy.E_gamma_max));

  TH3F* ic_optical_map = 
    new TH3F("IC spectrum optical", canvastitle,
	     galaxy.n_long, -180, 180.,
	     galaxy.n_lat, 
	     galaxy.lat_min, galaxy.lat_min + galaxy.d_lat*(galaxy.n_lat-1),
	     galaxy.n_E_gammagrid, 
	     log10(galaxy.E_gamma_min), log10(galaxy.E_gamma_max));

  TH3F* ic_ir_map = 
    new TH3F("IC spectrum infrared", canvastitle,
	     galaxy.n_long, -180, 180.,
	     galaxy.n_lat, 
	     galaxy.lat_min, galaxy.lat_min + galaxy.d_lat*(galaxy.n_lat-1),
	     galaxy.n_E_gammagrid, 
	     log10(galaxy.E_gamma_min), log10(galaxy.E_gamma_max));

  TH3F* ic_cmb_map = 
    new TH3F("IC spectrum CMB", canvastitle,
	     galaxy.n_long, -180, 180.,
	     galaxy.n_lat, 
	     galaxy.lat_min, galaxy.lat_min + galaxy.d_lat*(galaxy.n_lat-1),
	     galaxy.n_E_gammagrid, 
	     log10(galaxy.E_gamma_min), log10(galaxy.E_gamma_max));

  ic_aniso_map = 
    new TH3F("IC anisotropic spectrum", canvastitle,                             //AWS20060511
	     galaxy.n_long, -180, 180.,
	     galaxy.n_lat, 
	     galaxy.lat_min, galaxy.lat_min + galaxy.d_lat*(galaxy.n_lat-1),
	     galaxy.n_E_gammagrid, 
	     log10(galaxy.E_gamma_min), log10(galaxy.E_gamma_max));
    
  bremss_map = 
    new TH3F("bremss skymap",canvastitle,
	     galaxy.n_long,       -180,           180.,
	     galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1),
	     galaxy.n_E_gammagrid,log10(galaxy.E_gamma_min), log10(galaxy.E_gamma_max));
  
  pi0_decay_map = 
    new TH3F("pi0-decay skymap",canvastitle,
	     galaxy.n_long,       -180,           180.,
	     galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1),
	     galaxy.n_E_gammagrid,log10(galaxy.E_gamma_min), log10(galaxy.E_gamma_max));
  

  isotropic_map = 
    new TH3F("isotropic skymap",canvastitle,
	     galaxy.n_long,       -180,           180.,
	     galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1),
	     galaxy.n_E_gammagrid,log10(galaxy.E_gamma_min), log10(galaxy.E_gamma_max));
  
  total_map = 
    new TH3F("total     skymap",canvastitle,
	     galaxy.n_long,       -180,           180.,
	     galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1),
	     galaxy.n_E_gammagrid,log10(galaxy.E_gamma_min), log10(galaxy.E_gamma_max));
  
  
  sourcepop1_map = 
    new TH3F("sourcepop1 skymap",canvastitle,                                                     //AWS20051111 
	     galaxy.n_long,       -180,           180.,
	     galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1),
	     galaxy.n_E_gammagrid,log10(galaxy.E_gamma_min), log10(galaxy.E_gamma_max));
  
  
  sourcepop1_sublimit_map = 
    new TH3F("sourcepop1 skymap sub",canvastitle,                                                     //AWS20051111 AWS20080124
	     galaxy.n_long,       -180,           180.,
	     galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1),
	     galaxy.n_E_gammagrid,log10(galaxy.E_gamma_min), log10(galaxy.E_gamma_max));
  
  sourcepop1_soplimit_map = 
    new TH3F("sourcepop1 skymap sop",canvastitle,                                                     //AWS20051111 AWS20080124
	     galaxy.n_long,       -180,           180.,
	     galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1),
	     galaxy.n_E_gammagrid,log10(galaxy.E_gamma_min), log10(galaxy.E_gamma_max));
  
  //-----------------------------------------------------------------------------------------------------------------------------------
  
  
  for  (ip = 0; ip < galaxy.n_E_gammagrid; ip++) {
         
    if (galplotdef.isotropic_type == 2)
      isotropic_intensity = 
	isotropic_EGRET_from_list(galaxy.E_gamma[ip], galplotdef.isotropic_g);
    
    if (galplotdef.isotropic_type == 1 || isotropic_intensity<0.0 )
      isotropic_intensity = 
	galplotdef.isotropic_const*pow(galaxy.E_gamma[ip], 2.0 - galplotdef.isotropic_g);
    
    cout<<"isotropic_intensity="<<isotropic_intensity<<endl;
    
    for (i_lat = 0; i_lat < galaxy.n_lat; i_lat++) {
      
      for (i_long = 0; i_long < galaxy.n_long; i_long++) {

	l = galaxy.long_min + galaxy.d_long*i_long;
	b = galaxy.lat_min + galaxy.d_lat*i_lat;

	solid_angle = sabin(galaxy.lat_min, galaxy.d_long, galaxy.d_lat, i_lat); // multiply by solid angle of bin
	
	// this puts the bins at the right place but is incremental
	//              ic_map->Fill(l,b,        galaxy.IC_iso_skymap[i_comp].d2[i_long][i_lat].s[ip]);   
	
	// this would require shifting the array:
	ic_total = 0.;
	ic_aniso_total = 0.; //AWS20060511

	// Change -- TAP 24092007, simply select out all components and 
	// assign them to their own sky maps. The individual components
	// will be plotted according to the gamma_IC_selectcomp flag 
	// from the galplotdef file -- if 1 then components are split, if
	// 0 only the total spectrum will be plotted.

	for (i_comp = 0; i_comp < galaxy.n_ISRF_components; ++i_comp) {
	  //if (galplotdef.gamma_IC_selectcomp == 0 || //AWS20060310
	  //  (galplotdef.gamma_IC_selectcomp == 1 && i_comp == 0) || //AWS20060310
		//  (galplotdef.gamma_IC_selectcomp == 2 && i_comp == 1) || //AWS20060310
	  //(galplotdef.gamma_IC_selectcomp == 3 && i_comp == 2)) { //AWS20060310
	    
	  ic_total += galaxy.IC_iso_skymap[i_comp].d2[i_long][i_lat].s[ip];

	  if (galdef.IC_anisotropic > 0) //AWS20060511
	    ic_aniso_total += galaxy.IC_aniso_skymap[i_comp].d2[i_long][i_lat].s[ip]; //AWS20060511
		
	}

	l_off = l-180.;
	//          if(l_off<0.)l_off+=360.0; //AWS20060203 
	if (l_off <= 0.)
	  l_off += 360.0;//AWS20060203
	ii_long = galaxy.n_long - l_off/galaxy.d_long;
	
	if(galplotdef.verbose==-1001)// selectable debug                                                        //AWS20060203
	  cout<<"plot_spectrum:l l_off ii_long "<<l<<" "<<l_off<<" "<<ii_long<<" solid_angle="<<solid_angle<<endl;//AWS20060203
	
	ic_map->SetBinContent(ii_long, i_lat, ip, ic_total*solid_angle);  

	// Components with isotropic approximation (valid for CMB only)

	if (galaxy.n_ISRF_components <= 3)
	  ic_optical_map->SetBinContent(ii_long, i_lat, ip, galaxy.IC_iso_skymap[0].d2[i_long][i_lat].s[ip]*solid_angle);
	
	if (galaxy.n_ISRF_components <= 3)
	  ic_ir_map->SetBinContent(ii_long, i_lat, ip, galaxy.IC_iso_skymap[1].d2[i_long][i_lat].s[ip]*solid_angle);
	
	if (galaxy.n_ISRF_components <= 3)
	  ic_cmb_map->SetBinContent(ii_long, i_lat, ip, galaxy.IC_iso_skymap[2].d2[i_long][i_lat].s[ip]*solid_angle);

	ic_aniso_map->SetBinContent(ii_long, i_lat, ip,
				    ic_aniso_total*solid_angle);  //AWS20060511
	
	bremss_map->SetBinContent(ii_long, i_lat, ip,
				  galaxy.bremss_skymap.d2[i_long][i_lat].s[ip]*solid_angle); 
	
	pi0_decay_map->SetBinContent(ii_long, i_lat, ip, 
				     galaxy.pi0_decay_skymap.d2[i_long][i_lat].s[ip]*solid_angle); 
	
	isotropic_map->SetBinContent(ii_long,i_lat,ip,
				     isotropic_intensity*solid_angle);
	
	if(galplotdef.sourcepop_total   ==1)                                                                                                       //AWS20060118
	  sourcepop1_map         ->SetBinContent(ii_long,i_lat,ip,sourcepop1.skymap_intensity_spectrum         .d2[i_long][i_lat].s[ip]*solid_angle);//AWS20051111
	if(galplotdef.sourcepop_sublimit==1)                                                                                                       //AWS20060118
	  sourcepop1_sublimit_map->SetBinContent(ii_long,i_lat,ip,sourcepop1.skymap_intensity_spectrum_sublimit.d2[i_long][i_lat].s[ip]*solid_angle);//AWS20051111
	if(galplotdef.sourcepop_soplimit==1)                                                                                                       //AWS20060118
	  sourcepop1_soplimit_map->SetBinContent(ii_long,i_lat,ip,sourcepop1.skymap_intensity_spectrum_soplimit.d2[i_long][i_lat].s[ip]*solid_angle);//AWS20051111
	
      }  //  i_long
    }   //  i_lat
  } // ip
  
  // use anisotropic IC for total if available
  if (galdef.IC_anisotropic==0) //AWS20060512
    total_map->Add(ic_map, 1.0);   // TH1 function
  
  if(galdef.IC_anisotropic > 0) //AWS20060512 
    total_map->Add(ic_aniso_map, 1.0);   // TH1 function
  
  total_map->Add(   bremss_map,1.0);
  total_map->Add(pi0_decay_map,1.0);
  
  if(galplotdef.isotropic_use==1)
    total_map->Add(isotropic_map,1.0);
  
  ic_map->GetXaxis()->SetTitle("Galactic longitude");
  ic_aniso_map->GetXaxis()->SetTitle("Galactic longitude");

  ic_optical_map->GetXaxis()->SetTitle("Galactic longitude");
  ic_ir_map->GetXaxis()->SetTitle("Galactic longitude");
  ic_cmb_map->GetXaxis()->SetTitle("Galactic longitude");

  bremss_map->GetXaxis()->SetTitle("Galactic longitude");
  pi0_decay_map->GetXaxis()->SetTitle("Galactic longitude");
  
  if(galplotdef.verbose==-1000)// selectable debug
    {
      cout<<"sourcepop1.skymap_intensity_spectrum"<<endl;
      sourcepop1.skymap_intensity_spectrum.print();
    }
  
  // do the plotting
  //==============================     spectra   =============================
  
  //----------------------------------------------------------------------
  
  ib_min=(galplotdef.lat_min-galdef.lat_min+.0001)/galdef.d_lat;
  ib_max=(galplotdef.lat_max-galdef.lat_min+.0001)/galdef.d_lat;
  cout<<"ib_min ibmax:"<<ib_min<<" "<<ib_max<<endl;
  
  il_min=(galplotdef.long_min-galdef.long_min+.0001)/galdef.d_long;
  il_max=(galplotdef.long_max-galdef.long_min+.0001)/galdef.d_long;
  cout<<"il_min ilmax:"<<il_min<<" "<<il_max<<endl;

  sprintf(workstring1,"  %5.2f<l<%5.2f , %5.2f<l<%5.2f", //AWS20040315
	  galplotdef.long_min1,galplotdef.long_max1,
	  galplotdef.long_min2,galplotdef.long_max2);
  
  sprintf(workstring2,"  %5.2f<b<%5.2f , %5.2f<b<%5.2f", //AWS20040315
	  galplotdef.lat_min1,galplotdef.lat_max1,
	  galplotdef.lat_min2,galplotdef.lat_max2);
  
  // NB reversed longitude axis starting at +180 and decreasing
  
  l_off=180.-galplotdef.long_max1; if(l_off>360.)l_off-=360.; if(l_off<  0.)l_off+=360.;
  il_min1=l_off/galdef.d_long;
  l_off=180.-galplotdef.long_min1; if(l_off>360.)l_off-=360.; if(l_off<  0.)l_off+=360.;
  il_max1=l_off/galdef.d_long;
  cout<<"il_min1 ilmax1:"<<il_min1<<" "<<il_max1<<endl;
  
  l_off=180.-galplotdef.long_max2; if(l_off>360.)l_off-=360.; if(l_off<  0.)l_off+=360.;
  il_min2=l_off/galdef.d_long;
  l_off=180.-galplotdef.long_min2; if(l_off>360.)l_off-=360.; if(l_off<  0.)l_off+=360.;
  il_max2=l_off/galdef.d_long;
  cout<<"il_min2 ilmax2:"<<il_min2<<" "<<il_max2<<endl;
  

  ib_min1=(galplotdef.lat_min1-galdef.lat_min+.0001)/galdef.d_lat;
  ib_max1=(galplotdef.lat_max1-galdef.lat_min+.0001)/galdef.d_lat;
  cout<<"ib_min1 ibmax1:"<<ib_min1<<" "<<ib_max1<<endl;
  
  ib_min2=(galplotdef.lat_min2-galdef.lat_min+.0001)/galdef.d_lat;
  ib_max2=(galplotdef.lat_max2-galdef.lat_min+.0001)/galdef.d_lat;
  cout<<"ib_min2 ibmax2:"<<ib_min2<<" "<<ib_max2<<endl;
  
  double sa_norm_11,   sa_norm_12,   sa_norm_21,   sa_norm_22;
  sa_norm_11=0.;sa_norm_12=0.;sa_norm_21=0.;sa_norm_22=0.;
  
  
  for (i_long=il_min1;  i_long<=il_max1           ; i_long++   ) //AWS20030909
    for (i_lat =ib_min1;  i_lat <=ib_max1           ; i_lat++   ) //AWS20030909
      sa_norm_11+=sabin(galaxy.lat_min,galaxy.d_long,galaxy.d_lat,i_lat); // multiply by solid angle of bin
  
  
  for (i_long=il_min1;  i_long<=il_max1           ; i_long++   )//AWS20030909
    for (i_lat =ib_min2;  i_lat <=ib_max2           ; i_lat++    )//AWS20030909
      sa_norm_12+=sabin(galaxy.lat_min,galaxy.d_long,galaxy.d_lat,i_lat); 
  
  for (i_long=il_min2;  i_long<=il_max2           ; i_long++   )//AWS20030909
    for (i_lat =ib_min1;  i_lat <=ib_max1           ; i_lat++    )//AWS20030909
      sa_norm_21+=sabin(galaxy.lat_min,galaxy.d_long,galaxy.d_lat,i_lat); // multiply by solid angle of bin
  
  
  for (i_long=il_min2;  i_long<=il_max2           ; i_long++   )//AWS20030909
    for (i_lat =ib_min2;  i_lat <=ib_max2           ; i_lat++    )//AWS20030909
      sa_norm_22+=sabin(galaxy.lat_min,galaxy.d_long,galaxy.d_lat,i_lat); 
  
  //====== see HowTo Style: and do before creating canvas
    
  TStyle *plain  = new TStyle("Plain","Plain Style (no colors/fill areas)"); 
    
  plain->SetCanvasBorderMode(0);
  plain->SetPadBorderMode(0);
  plain->SetPadColor(0);
  plain->SetCanvasColor(0);
  plain->SetTitleColor(1,"X"); //AWS20080125  was (0), worked in root 4.08, changed to 1 for 5.08  Default options are (1, "X")
  plain->SetTitleColor(1,"Y"); //AWS20080125
  plain->SetStatColor(0);
  gROOT->SetStyle("Plain");
  
  strcpy(name,"mw_isrf");// to identify it: name is not plotted
  c1=new TCanvas(name,canvastitle,300,150, 600, 600);

  c1->SetLogx();
  c1->SetLogy();
  
  if(galplotdef.gamma_spectrum_Emin<=0.0)
    galplotdef.gamma_spectrum_Emin=galaxy.E_gamma_min;
  if(galplotdef.gamma_spectrum_Emax<=0.0)
    galplotdef.gamma_spectrum_Emax=galaxy.E_gamma_max;
  
  //----------------------------------------- axes
  h=c1->DrawFrame(galplotdef.gamma_spectrum_Emin, galplotdef.gamma_spectrum_Imin,  //AWS20040413
		  galplotdef.gamma_spectrum_Emax ,galplotdef.gamma_spectrum_Imax); 

  
      h->SetXTitle("Energy, MeV");
  //  h->GetXaxis()->SetTitle("here is a very long title to test whether it appears                      ");
      h->SetTitleOffset(1.1, "X");//+ve up  was 1.1 , modify for root 5.08
      h->SetTitleSize(0.035, "X");
      
  h->SetLabelOffset(0.00, "X");//+ve up 
  h->SetLabelSize(0.035,  "X");
  

  h->SetYTitle("E^{2} #times Intensity, cm^{-2} sr^{-1} s^{-1} MeV");  
  h->SetTitleOffset(1.3,  "Y");//+ve left
  h->SetTitleSize(0.035,  "Y");
  h->SetLabelOffset(0.00, "Y");//+ve left
  h->SetLabelSize(0.035,  "Y");
     


  //----------------------------------------- text
  text=new TText();
  text->SetTextFont(62);
  text->SetTextColor(kBlack);
  text->SetTextSize(0.034 );                                              //AWS20040315
  text->SetTextAlign(12);
  
  //if(galplotdef.verbose!=-1002)// selectable debug: no title or l,b ranges  or labels on model   AWS20080125
    if(galplotdef.verbose!=-1003)// selectable debug: no title or l,b ranges                       AWS20080125
    {
      text->DrawTextNDC(.10 ,.93 ,canvastitle);// NDC=normalized coord system   AWS20040413
      text->SetTextSize(0.022 );                                              //AWS20040315
      text->DrawTextNDC(.58 ,.88 ,workstring1);// NDC=normalized coord system   AWS20040413
      text->DrawTextNDC(.58 ,.86 ,workstring2);// NDC=normalized coord system   AWS20040413
    }
  

    //---------------------------------------- using (l,b) format


  if(galdef.skymap_format==0) //AWS20091126
 {

  
  if (galplotdef.gamma_spectrum == 1 || galplotdef.gamma_spectrum == 2) { // 1=plot model 2=model only 3=isotropic only 4=data only
          
    // -------- inverse Compton: isotropic ISRF
      
    gStyle->SetHistLineColor(3);// used in creating histogram   1=black 2=red 3=green 4=blue 5=yellow

    // total IC

    profile11 = 
      ic_map->ProjectionZ("galplot1", il_min1, il_max1, ib_min1, ib_max1);
    profile12 = 
      ic_map->ProjectionZ("galplot2", il_min1, il_max1, ib_min2, ib_max2);
    profile21 = 
      ic_map->ProjectionZ("galplot3", il_min2, il_max2, ib_min1, ib_max1);
    profile22 = 
      ic_map->ProjectionZ("galplot4", il_min2, il_max2, ib_min2, ib_max2);
    
    profile11->Scale(1./sa_norm_11);
    profile12->Scale(1./sa_norm_12);
    profile21->Scale(1./sa_norm_21);
    profile22->Scale(1./sa_norm_22);
    
    profile = 
      ic_map->ProjectionZ("galplot5", il_min1, il_max1, ib_min1, ib_max1);
    
    for(ip = 0; ip < galaxy.n_E_gammagrid; ip++) 
      profile->SetBinContent(ip, 0);
    
    profile->Add(profile11, 0.25);
    profile->Add(profile12, 0.25);
    profile->Add(profile21, 0.25);
    profile->Add(profile22, 0.25);
    
    spectrum = new TGraph(galaxy.n_E_gammagrid);

    for (ip = 0; ip < galaxy.n_E_gammagrid; ip++)
      spectrum->SetPoint(ip, galaxy.E_gamma[ip], profile->GetBinContent(ip));
    
    //Gtypes.h:enum EColor { kWhite, kBlack, kRed, kGreen, kBlue, kYellow, kMagenta, kCyan };
    spectrum->SetMarkerColor(kGreen);
    spectrum->SetMarkerStyle(21); 
    spectrum->SetMarkerStyle(22); // triangles
    spectrum->SetMarkerSize (0.5);// 
    spectrum->SetLineColor(kGreen);
    spectrum->SetLineWidth(2);
    spectrum->SetLineStyle(1);//1=solid 2=dash 3=dot 4=dash-dot  AWS20080121
    spectrum->SetMarkerStyle(22); // triangles
    spectrum->SetMarkerSize(0.5);// 
    //  spectrum->Draw("AP");       // axes: not used
    
    spectrum->Draw("PL");  // axes; points as markers;line  AWS20040408
    
    text=new TText();
    text->SetTextFont(62);
    text->SetTextColor(kGreen);
    text->SetTextSize(0.030);
    text->SetTextAlign(12);
    ip=1;
    spectrum->GetPoint(ip,x,y);
    //text->DrawText(log10(x),log10(y*1.25),   "IC"     );// user coordinates,but log must be taken
    
    if(galplotdef.verbose!=-1002) {// selectable debug: no title or l,b ranges  or labels on model  AWS20060814
      
      if (galdef.IC_anisotropic == 0)               // AWS20060512
	text->DrawText(x, y*1.25, "IC");// user coordinates: no log needed (change in root) AWS20040312
      
      if(galdef.IC_anisotropic > 0)               // AWS20060512
	text->DrawText(x, y*1.25, "iso IC");// AWS20060512
      
    }
      
    spectrum_ic = spectrum;//AWS20041117

    if (galplotdef.gamma_IC_selectcomp) {

      profile11 = 
	ic_optical_map->ProjectionZ("galplot_optical1", il_min1, il_max1, ib_min1, ib_max1);
      profile12 = 
	ic_optical_map->ProjectionZ("galplot_optical2", il_min1, il_max1, ib_min2, ib_max2);
      profile21 = 
	ic_optical_map->ProjectionZ("galplot_optical3", il_min2, il_max2, ib_min1, ib_max1);
      profile22 = 
	ic_optical_map->ProjectionZ("galplot_optical4", il_min2, il_max2, ib_min2, ib_max2);
      
      profile11->Scale(1./sa_norm_11);
      profile12->Scale(1./sa_norm_12);
      profile21->Scale(1./sa_norm_21);
      profile22->Scale(1./sa_norm_22);
      
      profile = 
	ic_optical_map->ProjectionZ("galplot_optical5", il_min1, il_max1, ib_min1, ib_max1);
      
      for(ip = 0; ip < galaxy.n_E_gammagrid; ip++) 
	profile->SetBinContent(ip, 0);
      
      profile->Add(profile11, 0.25);
      profile->Add(profile12, 0.25);
      profile->Add(profile21, 0.25);
      profile->Add(profile22, 0.25);
      
      spectrum = new TGraph(galaxy.n_E_gammagrid);
      
      for (ip = 0; ip < galaxy.n_E_gammagrid; ip++)
	spectrum->SetPoint(ip, galaxy.E_gamma[ip], profile->GetBinContent(ip));
      
      //Gtypes.h:enum EColor { kWhite, kBlack, kRed, kGreen, kBlue, kYellow, kMagenta, kCyan };
      spectrum->SetMarkerColor(kGreen);
      spectrum->SetMarkerStyle(21); 
      spectrum->SetMarkerStyle(22); // triangles
      spectrum->SetMarkerSize(0.5);// 
      spectrum->SetLineColor(kGreen);
      spectrum->SetLineWidth(2);
      //    gStyle->SetLineStyleString(9,"40  20 ");                      // no effect in root version 4.03 at MPE AWS20080122
      spectrum->SetLineStyle(9);//1=solid 2=dash 3=dot 4=dash-dot.   5-9  not in root version 4.03, are in 5.08 AWS20080124
      spectrum->SetMarkerStyle(22); // triangles
      spectrum->SetMarkerSize(0.5);// 
      //  spectrum->Draw("AP");       // axes: not used
      
      spectrum->Draw("L");  // line only  AWS20080121
      
      spectrum_ic_optical = spectrum; //TAP20072409
      
      profile11 = 
	ic_ir_map->ProjectionZ("galplot_ir1", il_min1, il_max1, ib_min1, ib_max1);
      profile12 = 
	ic_ir_map->ProjectionZ("galplot_ir2", il_min1, il_max1, ib_min2, ib_max2);
      profile21 = 
	ic_ir_map->ProjectionZ("galplot_ir3", il_min2, il_max2, ib_min1, ib_max1);
      profile22 = 
	ic_ir_map->ProjectionZ("galplot_ir4", il_min2, il_max2, ib_min2, ib_max2);
      
      profile11->Scale(1./sa_norm_11);
      profile12->Scale(1./sa_norm_12);
      profile21->Scale(1./sa_norm_21);
      profile22->Scale(1./sa_norm_22);
      
      profile = 
	ic_ir_map->ProjectionZ("galplot_ir5", il_min1, il_max1, ib_min1, ib_max1);
      
      for(ip = 0; ip < galaxy.n_E_gammagrid; ip++) 
	profile->SetBinContent(ip, 0);
      
      profile->Add(profile11, 0.25);
      profile->Add(profile12, 0.25);
      profile->Add(profile21, 0.25);
      profile->Add(profile22, 0.25);
      
      spectrum = new TGraph(galaxy.n_E_gammagrid);
      
      for (ip = 0; ip < galaxy.n_E_gammagrid; ip++)
	spectrum->SetPoint(ip, galaxy.E_gamma[ip], profile->GetBinContent(ip));
      
      //Gtypes.h:enum EColor { kWhite, kBlack, kRed, kGreen, kBlue, kYellow, kMagenta, kCyan };
      spectrum->SetMarkerColor(kGreen);
      spectrum->SetMarkerStyle(21); 
      spectrum->SetMarkerStyle(22); // triangles
      spectrum->SetMarkerSize(0.5);// 
      spectrum->SetLineColor(kGreen);
      spectrum->SetLineWidth(2);
      spectrum->SetLineStyle(2);//1=solid 2=dash 3=dot 4=dash-dot  AWS20080123
      spectrum->SetMarkerStyle(22); // triangles
      spectrum->SetMarkerSize(0.5);// 
      //  spectrum->Draw("AP");       // axes: not used
      
      spectrum->Draw("L");  // line only  AWS20080121
      
      spectrum_ic_ir = spectrum; //TAP20072409
      
      profile11 = 
	ic_cmb_map->ProjectionZ("galplot_cmb1", il_min1, il_max1, ib_min1, ib_max1);
      profile12 = 
	ic_cmb_map->ProjectionZ("galplot_cmb2", il_min1, il_max1, ib_min2, ib_max2);
      profile21 = 
	ic_cmb_map->ProjectionZ("galplot_cmb3", il_min2, il_max2, ib_min1, ib_max1);
      profile22 = 
	ic_cmb_map->ProjectionZ("galplot_cmb4", il_min2, il_max2, ib_min2, ib_max2);
      
      profile11->Scale(1./sa_norm_11);
      profile12->Scale(1./sa_norm_12);
      profile21->Scale(1./sa_norm_21);
      profile22->Scale(1./sa_norm_22);
      
      profile = 
	ic_cmb_map->ProjectionZ("galplot_cmb5", il_min1, il_max1, ib_min1, ib_max1);
      
      for(ip = 0; ip < galaxy.n_E_gammagrid; ip++) 
	profile->SetBinContent(ip, 0);
      
      profile->Add(profile11, 0.25);
      profile->Add(profile12, 0.25);
      profile->Add(profile21, 0.25);
      profile->Add(profile22, 0.25);
      
      spectrum = new TGraph(galaxy.n_E_gammagrid);
      
      for (ip = 0; ip < galaxy.n_E_gammagrid; ip++)
	spectrum->SetPoint(ip, galaxy.E_gamma[ip], profile->GetBinContent(ip));
      
      //Gtypes.h:enum EColor { kWhite, kBlack, kRed, kGreen, kBlue, kYellow, kMagenta, kCyan };
      spectrum->SetMarkerColor(kGreen);
      spectrum->SetMarkerStyle(21); 
      spectrum->SetMarkerStyle(22); // triangles
      spectrum->SetMarkerSize(0.5);// 
      spectrum->SetLineColor(kGreen);
      spectrum->SetLineWidth(2);
      spectrum->SetLineStyle(3);//1=solid 2=dash 3=dot 4=dash-dot  AWS20080121
      spectrum->SetMarkerStyle(22); // triangles
      spectrum->SetMarkerSize(0.5);// 
      //  spectrum->Draw("AP");       // axes: not used
      
      spectrum->Draw("L");  // axes; line only  AWS20080121
      
      spectrum_ic_cmb = spectrum; //TAP20072409

    }        

    // inverse Compton: anisotropic ISRF  AWS20060511
    
    if (galdef.IC_anisotropic > 0) {
	  
      profile11=ic_aniso_map->ProjectionZ("galplot101" ,il_min1,il_max1, ib_min1,ib_max1);
      profile12=ic_aniso_map->ProjectionZ("galplot102" ,il_min1,il_max1, ib_min2,ib_max2);
      profile21=ic_aniso_map->ProjectionZ("galplot103" ,il_min2,il_max2, ib_min1,ib_max1);
      profile22=ic_aniso_map->ProjectionZ("galplot104" ,il_min2,il_max2, ib_min2,ib_max2);
      profile11->Scale(1./sa_norm_11);
      profile12->Scale(1./sa_norm_12);
      profile21->Scale(1./sa_norm_21);
      profile22->Scale(1./sa_norm_22);
            
      profile = ic_aniso_map->ProjectionZ("galplot105" ,il_min1,il_max1, ib_min1,ib_max1);
      for  (ip = 0; ip < galaxy.n_E_gammagrid; ip++) 
	profile->SetBinContent(ip,0);
		  
      profile->Add(profile11,0.25);
      profile->Add(profile12,0.25);
      profile->Add(profile21,0.25);
      profile->Add(profile22,0.25);
	  
      spectrum=new TGraph(galaxy.n_E_gammagrid);
      for (ip = 0; ip < galaxy.n_E_gammagrid; ip++)
	spectrum->SetPoint(ip, galaxy.E_gamma[ip], profile->GetBinContent(ip));
      
      //Gtypes.h:enum EColor { kWhite, kBlack, kRed, kGreen, kBlue, kYellow, kMagenta, kCyan };
      spectrum->SetMarkerColor(kGreen);
      spectrum->SetMarkerStyle(21); 
      spectrum->SetMarkerStyle(22); // triangles
      spectrum->SetMarkerSize (0.5);// 
      spectrum->SetLineColor(kGreen);
      spectrum->SetLineWidth(2     );
      spectrum->SetLineStyle(1      );//1=solid 2=dash 3=dot 4=dash-dot
      spectrum->SetMarkerStyle(22); // triangles
      spectrum->SetMarkerSize (0.5);// 
      
      spectrum->Draw("PL");  // axes; points as markers;line  AWS20040408
      
      text=new TText();
      text->SetTextFont(62);
      text->SetTextColor(kGreen);
      text->SetTextSize(0.030 );
      text->SetTextAlign(12);
      ip=1;
      spectrum->GetPoint(ip,x,y);
      
      if(galplotdef.verbose!=-1002)// selectable debug: no title or l,b ranges  or labels on model  AWS20060814
	text->DrawText(x ,y*1.25 ,   "aniso IC"     );// user coordinates: no log needed (change in root) AWS20040312
      
      spectrum_ic_aniso=spectrum;
      
    }
    // ------- bremsstrahlung
    
    profile11=bremss_map->ProjectionZ("galplot6" ,il_min1,il_max1, ib_min1,ib_max1);
    profile12=bremss_map->ProjectionZ("galplot7" ,il_min1,il_max1, ib_min2,ib_max2);
    profile21=bremss_map->ProjectionZ("galplot8" ,il_min2,il_max2, ib_min1,ib_max1);
    profile22=bremss_map->ProjectionZ("galplot9" ,il_min2,il_max2, ib_min2,ib_max2);
    profile11->Scale(1./sa_norm_11);
    profile12->Scale(1./sa_norm_12);
    profile21->Scale(1./sa_norm_21);
    profile22->Scale(1./sa_norm_22);
      /*
	profile11->Scale(1./((il_max1-il_min1+1)*(ib_max1-ib_min1+1)));
	profile12->Scale(1./((il_max1-il_min1+1)*(ib_max2-ib_min2+1)));
	profile21->Scale(1./((il_max2-il_min2+1)*(ib_max1-ib_min1+1)));
	profile22->Scale(1./((il_max2-il_min2+1)*(ib_max2-ib_min2+1)));
      */
    
    profile  =bremss_map->ProjectionZ("galplot10" ,il_min1,il_max1, ib_min1,ib_max1);
    for  (ip    =0;  ip    <galaxy.n_E_gammagrid;ip    ++) profile->SetBinContent(ip,0);
    
    
    profile->Add(profile11,0.25);
    profile->Add(profile12,0.25);
    profile->Add(profile21,0.25);
    profile->Add(profile22,0.25);
    
    
    spectrum=new TGraph(galaxy.n_E_gammagrid);
      for  (ip    =0;  ip    <galaxy.n_E_gammagrid;ip    ++)
	spectrum->SetPoint(ip,galaxy.E_gamma[ip],profile->GetBinContent(ip));
      
      spectrum->SetMarkerColor(kCyan);
      spectrum->SetMarkerStyle(21); 
      spectrum->SetMarkerStyle(22); // triangles
      spectrum->SetMarkerSize (0.5);// 
      spectrum->SetLineColor(kCyan);
      spectrum->SetLineWidth(2     );
      spectrum->SetLineStyle(1      );//1=solid 2=dash 3=dot 4=dash-dot  AWS20080121
      spectrum->SetMarkerStyle(22); // triangles
      spectrum->SetMarkerSize (0.5);// 
            
      spectrum->Draw("PL");  // points as markers  + line  AWS20040408
      
      text=new TText();
      text->SetTextFont(62);
      text->SetTextColor(kCyan);
      text->SetTextSize(0.030 );
      text->SetTextAlign(12);
      
      // find first on-scale point for label 
      ip=0;y=-1.;
      while(y<galplotdef.gamma_spectrum_Imin*2&&ip<galaxy.n_E_gammagrid)    {spectrum->GetPoint(ip,x,y); ip++;}//AWS20030905
      //  text->DrawText(log10(x*1.1),log10(y*0.9),   "bremss"     );// user coordinates,but log must be taken
      
      if(galplotdef.verbose!=-1002)// selectable debug: no title or l,b ranges  or labels on model AWS20060814
	text->DrawText(x*1.1,y*0.9,   "bremss"     );// user coordinates AWS20040312
      spectrum_bremss=spectrum;//AWS20041117
      
      // --------- pi0 decay
      
      profile11=pi0_decay_map->ProjectionZ("galplot11" ,il_min1,il_max1, ib_min1,ib_max1);
      profile12=pi0_decay_map->ProjectionZ("galplot12" ,il_min1,il_max1, ib_min2,ib_max2);
      profile21=pi0_decay_map->ProjectionZ("galplot13" ,il_min2,il_max2, ib_min1,ib_max1);
      profile22=pi0_decay_map->ProjectionZ("galplot14" ,il_min2,il_max2, ib_min2,ib_max2);
      profile11->Scale(1./sa_norm_11);
      profile12->Scale(1./sa_norm_12);
      profile21->Scale(1./sa_norm_21);
      profile22->Scale(1./sa_norm_22);
      /*
	profile11->Scale(1./((il_max1-il_min1+1)*(ib_max1-ib_min1+1)));
	profile12->Scale(1./((il_max1-il_min1+1)*(ib_max2-ib_min2+1)));
	profile21->Scale(1./((il_max2-il_min2+1)*(ib_max1-ib_min1+1)));
	profile22->Scale(1./((il_max2-il_min2+1)*(ib_max2-ib_min2+1)));
      */
      
      profile  =pi0_decay_map->ProjectionZ("galplot15" ,il_min1,il_max1, ib_min1,ib_max1);
      for  (ip    =0;  ip    <galaxy.n_E_gammagrid;ip    ++) profile->SetBinContent(ip,0);
            
      profile->Add(profile11,0.25);
      profile->Add(profile12,0.25);
      profile->Add(profile21,0.25);
      profile->Add(profile22,0.25);

      spectrum=new TGraph(galaxy.n_E_gammagrid);
      for  (ip    =0;  ip    <galaxy.n_E_gammagrid;ip    ++)
	spectrum->SetPoint(ip,galaxy.E_gamma[ip],profile->GetBinContent(ip));
      
      spectrum->SetMarkerColor(kRed);
      spectrum->SetMarkerStyle(21); 
      spectrum->SetMarkerStyle(22); // triangles
      spectrum->SetMarkerSize (0.5);// 
      spectrum->SetLineColor(kRed);
      spectrum->SetLineWidth(2     );
      spectrum->SetLineStyle(1      );//1=solid 2=dash 3=dot 4=dash-dot  AWS20080121
      spectrum->SetMarkerStyle(22); // triangles
      spectrum->SetMarkerSize (0.5);// 
            
      spectrum->Draw("PL");  // points as markers + line AWS20040408
      
      text=new TText();
      text->SetTextFont(122);//Greek
      text->SetTextColor(kRed);
      text->SetTextSize(0.035 );
      text->SetTextAlign(12);
      // find first on-scale point for label 
      ip=0;y=-1.;
      while(y<galplotdef.gamma_spectrum_Imin*2&&ip<galaxy.n_E_gammagrid)    {spectrum->GetPoint(ip,x,y); ip++;}//AWS20030905
      //  text->DrawText(log10(x*1.1),log10(y),"p^o");// user coordinates,but log must be taken
      //  text->DrawText(x*1.1,y,"p^o");// user coordinates AWS20040312
      
      latex=new TLatex();                  //screen and gif symbol support AWS20041021
      latex->SetTextAlign(12);
      latex->SetTextSize(0.035);
      latex->SetTextColor(kRed);
      if(galplotdef.verbose!=-1002)// selectable debug: no title or l,b ranges  or labels on model  AWS20060814
	latex->DrawLatex(x*1.2,y,"#pi^{o}");
            
      spectrum_pi0=spectrum;//AWS20041117
      
      // ------- total
      
      profile11=total_map->ProjectionZ("galplot16" ,il_min1,il_max1, ib_min1,ib_max1);
      profile12=total_map->ProjectionZ("galplot17" ,il_min1,il_max1, ib_min2,ib_max2);
      profile21=total_map->ProjectionZ("galplot18" ,il_min2,il_max2, ib_min1,ib_max1);
      profile22=total_map->ProjectionZ("galplot19" ,il_min2,il_max2, ib_min2,ib_max2);
      profile11->Scale(1./sa_norm_11);
      profile12->Scale(1./sa_norm_12);
      profile21->Scale(1./sa_norm_21);
      profile22->Scale(1./sa_norm_22);
      /*
	profile11->Scale(1./((il_max1-il_min1+1)*(ib_max1-ib_min1+1)));
	profile12->Scale(1./((il_max1-il_min1+1)*(ib_max2-ib_min2+1)));
	profile21->Scale(1./((il_max2-il_min2+1)*(ib_max1-ib_min1+1)));
	profile22->Scale(1./((il_max2-il_min2+1)*(ib_max2-ib_min2+1)));
      */
      
      profile  =total_map->ProjectionZ("galplot20" ,il_min1,il_max1, ib_min1,ib_max1);
      for  (ip    =0;  ip    <galaxy.n_E_gammagrid;ip    ++) profile->SetBinContent(ip,0);
            
      profile->Add(profile11,0.25);
      profile->Add(profile12,0.25);
      profile->Add(profile21,0.25);
      profile->Add(profile22,0.25);
        
      spectrum=new TGraph(galaxy.n_E_gammagrid);
      for  (ip    =0;  ip    <galaxy.n_E_gammagrid;ip    ++)
	spectrum->SetPoint(ip,galaxy.E_gamma[ip],profile->GetBinContent(ip));
            
      spectrum_total=spectrum;//AWS20041117
      
      // trim points outside EGRET range if required AWS20040504
      
      if(galplotdef.spectrum_cut_total==1|| galplotdef.spectrum_cut_total==3)
	{
	  for (ip=0;                 ip<spectrum->GetN() ;ip++)
	    {
	      spectrum->GetPoint(0,x,y);
	      if(x< data.E_EGRET[ 0] ) spectrum->RemovePoint(0 );
	    }
	}
      
      if(galplotdef.spectrum_cut_total==2|| galplotdef.spectrum_cut_total==3)
	{
	  for (ip=spectrum->GetN()-1;ip>0;                ip--)
	    {
	      spectrum->GetPoint(ip,x,y);
	      //  if(x> data.E_EGRET[galplotdef.energies_EGRET] ) spectrum->RemovePoint(ip);
	      if(x> data.E_EGRET[12] ) spectrum->RemovePoint(ip);//remove only E>50 GeV AWS20040507
	    }
 
	}// trim points
            
      spectrum->SetMarkerColor(kBlue);
      spectrum->SetMarkerStyle(21); 
      spectrum->SetMarkerStyle(22); // triangles
      spectrum->SetMarkerSize (0.5);// 
      spectrum->SetLineColor(kBlue);
      spectrum->SetLineWidth(3     );
      spectrum->SetLineStyle(1      );//1=solid 2=dash 3=dot 4=dash-dot
      spectrum->SetMarkerStyle(22); // triangles
      spectrum->SetMarkerSize (0.5);// 
        
      spectrum->Draw("PL");  // points as markers  + line
      
      text=new TText();
      text->SetTextFont(62);
      text->SetTextColor(kBlue);
      text->SetTextSize(0.030 );
      text->SetTextAlign(12);
      ip=1;
      
      spectrum->GetPoint(ip,x,y);
      //  text->DrawText(log10(x),log10(y*1.4),   "total"     );// user coordinates,but log must be taken
  if(galplotdef.verbose!=-1002)// selectable debug: no title or l,b ranges  or labels on model  AWS20060814
    text->DrawText(x,y*1.4,   "total"     );// user coordinates AWS20040312

  // ------- isotropic

  spectrum_isotropic=new TGraph(galaxy.n_E_gammagrid);//AWS20091230

  if(galplotdef.isotropic_use==1 )
    {
      spectrum=new TGraph(galaxy.n_E_gammagrid);
      
      //     spectrum_isotropic=new TGraph(galaxy.n_E_gammagrid);//AWS20041117
      
      // use bin at i_long=i_lat=0 (since isotropic can use any)
      solid_angle=sabin(galaxy.lat_min,galaxy.d_long,galaxy.d_lat,0); // need to remove solid angle factor for i_lat=0
      
      cout<<"plot_spectrum: isotropic: solid angle ="<<solid_angle<<endl;   //AWS20060203
      
      /* replace by segmented plot AWS20040312
	 
	 for  (ip    =0;  ip    <galaxy.n_E_gammagrid;ip    ++)
	 spectrum->SetPoint(ip,galaxy.E_gamma[ip],isotropic_map->GetBinContent(0,0,ip)/solid_angle);
	 
	 spectrum->SetMarkerColor(kBlack);
	 spectrum->SetMarkerStyle(21); 
	 spectrum->SetMarkerStyle(22); // triangles
	 spectrum->SetMarkerSize (0.5);// 
	 spectrum->SetLineColor(kBlack);
	 spectrum->SetLineWidth(1     );
	 spectrum->SetLineStyle(1      );
	 spectrum->SetMarkerStyle(22); // triangles
	 spectrum->SetMarkerSize (0.5);// 
	 spectrum->Draw();
	 spectrum->Draw(" P ");  // points as markers  
	 spectrum->Draw();
	 
      */
      // plot as segments to avoid line joining to zero points AWS20040115
      
      for  (ip    =0;  ip    <galaxy.n_E_gammagrid-1;ip    ++)
	{
	  
	  spectrum=new TGraph(2);    
	  
	  if(isotropic_map->GetBinContent(0,0,ip  )>0 && isotropic_map->GetBinContent(0,0,ip+1)>0)
	    {
	      spectrum->SetPoint(0,galaxy.E_gamma[ip  ],isotropic_map->GetBinContent(0,0,ip  )/solid_angle);
	      spectrum->SetPoint(1,galaxy.E_gamma[ip+1],isotropic_map->GetBinContent(0,0,ip+1)/solid_angle);
	      
	      
	      spectrum->SetLineColor(kBlack);
	      spectrum->SetLineWidth(1     );
	      spectrum->SetLineStyle(1      ); 
	      spectrum->Draw("PL");//AWS20040408
	      
	      spectrum_isotropic->SetPoint(ip,galaxy.E_gamma[ip  ],isotropic_map->GetBinContent(0,0,ip  )/solid_angle);//AWS20041117
	      
	    }//if
	  
	}//ip
      
      text=new TText();
      text->SetTextFont(62);
      text->SetTextColor(kBlack);
      text->SetTextSize(0.030 );
      text->SetTextAlign(12);
      //ip=1;
      //spectrum->GetPoint(ip,x,y);
      
      x=100.; //AWS20040312
      y=9.e-4;//AWS20040312
      
      
      //  text->DrawText(log10(x),log10(y*1.15),   "EB"     );// user coordinates,but log must be taken
      if(galplotdef.verbose!=-1002)// selectable debug: no title or l,b ranges  or labels on model  AWS20060814
	text->DrawText(x,y*1.15,   "EB"     );// user coordinates AWS20040312
    } //if
  
  // --------- source populations                              AWS20051111
  //           all sources
  
  if(galplotdef.sourcepop_total   ==1)
    {
      profile11=sourcepop1_map->ProjectionZ("galplot21" ,il_min1,il_max1, ib_min1,ib_max1);
      profile12=sourcepop1_map->ProjectionZ("galplot22" ,il_min1,il_max1, ib_min2,ib_max2);
      profile21=sourcepop1_map->ProjectionZ("galplot23" ,il_min2,il_max2, ib_min1,ib_max1);
      profile22=sourcepop1_map->ProjectionZ("galplot24" ,il_min2,il_max2, ib_min2,ib_max2);
      profile11->Scale(1./sa_norm_11);
      profile12->Scale(1./sa_norm_12);
      profile21->Scale(1./sa_norm_21);
      profile22->Scale(1./sa_norm_22);
      
      profile  =sourcepop1_map->ProjectionZ("galplot25" ,il_min1,il_max1, ib_min1,ib_max1);

      for  (ip    =0;  ip    <galaxy.n_E_gammagrid;ip    ++) 
	profile->SetBinContent(ip,0);
      
      profile->Add(profile11,0.25);
      profile->Add(profile12,0.25);
      profile->Add(profile21,0.25);
      profile->Add(profile22,0.25);
            
      spectrum=new TGraph(galaxy.n_E_gammagrid);
      for  (ip    =0;  ip    <galaxy.n_E_gammagrid;ip    ++)
	{
	  spectrum->SetPoint(ip,galaxy.E_gamma[ip],profile->GetBinContent(ip));
	  cout<<"sourcepop1 E="<<galaxy.E_gamma[ip]<<"  spectrum="<<profile->GetBinContent(ip)<<endl;
	}
      
      spectrum->SetMarkerColor(kMagenta);
      spectrum->SetMarkerStyle(21); 
      spectrum->SetMarkerStyle(22); // triangles
      spectrum->SetMarkerSize (0.5);// 
      spectrum->SetLineColor(kMagenta);
      spectrum->SetLineWidth(4     );
      spectrum->SetLineStyle(4      );//1=solid 2=dash 3=dot 4=dash-dot
      spectrum->SetMarkerStyle(22); // triangles
      spectrum->SetMarkerSize (0.5);// 
            
      spectrum->Draw("PL");  // points as markers + line AWS20040408
      
      text=new TText();
      text->SetTextFont(122);//Greek
      text->SetTextColor(kMagenta);
      text->SetTextSize(0.035 );
      text->SetTextAlign(12);
      // find first on-scale point for label 
      ip=0;y=-1.;
      while(y<galplotdef.gamma_spectrum_Imin*2&&ip<galaxy.n_E_gammagrid)    {spectrum->GetPoint(ip,x,y); ip++;}//AWS20030905
      //  text->DrawText(log10(x*1.1),log10(y),"p^o");// user coordinates,but log must be taken
      //  text->DrawText(x*1.1,y,"p^o");// user coordinates AWS20040312
      
      latex=new TLatex();                  //screen and gif symbol support AWS20041021
      latex->SetTextAlign(12);
      latex->SetTextSize(0.035);
      latex->SetTextColor(kMagenta);
      latex->DrawLatex(x*1.2,y,"source population");
            
      spectrum_sourcepop1=spectrum;//AWS20051111
      
    }// if sourcepop_total
  
  // --------- source populations                              AWS20051111
  //            sources below detection limit
  
  if(galplotdef.sourcepop_sublimit==1)
    {
      profile11=sourcepop1_sublimit_map->ProjectionZ("galplot26" ,il_min1,il_max1, ib_min1,ib_max1);
      profile12=sourcepop1_sublimit_map->ProjectionZ("galplot27" ,il_min1,il_max1, ib_min2,ib_max2);
      profile21=sourcepop1_sublimit_map->ProjectionZ("galplot28" ,il_min2,il_max2, ib_min1,ib_max1);
      profile22=sourcepop1_sublimit_map->ProjectionZ("galplot29" ,il_min2,il_max2, ib_min2,ib_max2);
      profile11->Scale(1./sa_norm_11);
      profile12->Scale(1./sa_norm_12);
      profile21->Scale(1./sa_norm_21);
      profile22->Scale(1./sa_norm_22);
      
      profile  =sourcepop1_sublimit_map->ProjectionZ("galplot30" ,il_min1,il_max1, ib_min1,ib_max1);
      for  (ip    =0;  ip    <galaxy.n_E_gammagrid;ip    ++) profile->SetBinContent(ip,0);
      
      profile->Add(profile11,0.25);
      profile->Add(profile12,0.25);
      profile->Add(profile21,0.25);
      profile->Add(profile22,0.25);
      
      spectrum=new TGraph(galaxy.n_E_gammagrid);
      for  (ip    =0;  ip    <galaxy.n_E_gammagrid;ip    ++)
	{
	  spectrum->SetPoint(ip,galaxy.E_gamma[ip],profile->GetBinContent(ip));
	  cout<<"sourcepop1 E="<<galaxy.E_gamma[ip]<<"  spectrum below limit="<<profile->GetBinContent(ip)<<endl;
	}
      
      spectrum->SetMarkerColor(kMagenta);
      spectrum->SetMarkerStyle(21); 
      spectrum->SetMarkerStyle(22); // triangles
      spectrum->SetMarkerSize (0.5);// 
      spectrum->SetLineColor(kMagenta);
      spectrum->SetLineWidth(2     );
      spectrum->SetLineStyle(3      );//1=solid 2=dash 3=dot 4=dash-dot
      spectrum->SetMarkerStyle(22); // triangles
      spectrum->SetMarkerSize (0.5);// 
       
      spectrum->Draw(" L");  // points as markers P+ line L
  
      text=new TText();
      text->SetTextFont(122);//Greek
      text->SetTextColor(kMagenta);
      text->SetTextSize(0.035 );
      text->SetTextAlign(12);
      // find first on-scale point for label 
      ip=0;y=-1.;
      while(y<galplotdef.gamma_spectrum_Imin*2&&ip<galaxy.n_E_gammagrid)    {spectrum->GetPoint(ip,x,y); ip++;}//AWS20030905
      //  text->DrawText(log10(x*1.1),log10(y),"p^o");// user coordinates,but log must be taken
      //  text->DrawText(x*1.1,y,"p^o");// user coordinates AWS20040312
      
      latex=new TLatex();                  //screen and gif symbol support AWS20041021
      latex->SetTextAlign(12);
      latex->SetTextSize(0.035);
      latex->SetTextColor(kMagenta);
      //  latex->DrawLatex(x*1.2,y,"source population below limit"); AWS20060724
            
      spectrum_sourcepop1_sublimit=spectrum;

    }//if sublimit
  
  // --------- source populations                              AWS20051111
  //            sources above detection limit
  
  if(galplotdef.sourcepop_soplimit==1)
    {
      profile11=sourcepop1_soplimit_map->ProjectionZ("galplot31" ,il_min1,il_max1, ib_min1,ib_max1);
      profile12=sourcepop1_soplimit_map->ProjectionZ("galplot32" ,il_min1,il_max1, ib_min2,ib_max2);
      profile21=sourcepop1_soplimit_map->ProjectionZ("galplot33" ,il_min2,il_max2, ib_min1,ib_max1);
      profile22=sourcepop1_soplimit_map->ProjectionZ("galplot34" ,il_min2,il_max2, ib_min2,ib_max2);
      profile11->Scale(1./sa_norm_11);
      profile12->Scale(1./sa_norm_12);
      profile21->Scale(1./sa_norm_21);
      profile22->Scale(1./sa_norm_22);
            
      profile  =sourcepop1_soplimit_map->ProjectionZ("galplot35" ,il_min1,il_max1, ib_min1,ib_max1);
      for  (ip    =0;  ip    <galaxy.n_E_gammagrid;ip    ++) profile->SetBinContent(ip,0);
            
      profile->Add(profile11,0.25);
      profile->Add(profile12,0.25);
      profile->Add(profile21,0.25);
      profile->Add(profile22,0.25);
      
      spectrum=new TGraph(galaxy.n_E_gammagrid);
      for  (ip    =0;  ip    <galaxy.n_E_gammagrid;ip    ++)
	{
	  spectrum->SetPoint(ip,galaxy.E_gamma[ip],profile->GetBinContent(ip));
	  cout<<"sourcepop1 E="<<galaxy.E_gamma[ip]<<"  spectrum above limit="<<profile->GetBinContent(ip)<<endl;
	}
      
      spectrum->SetMarkerColor(kMagenta);
      spectrum->SetMarkerStyle(21); 
      spectrum->SetMarkerStyle(22); // triangles
      spectrum->SetMarkerSize (0.5);// 
      spectrum->SetLineColor(kMagenta);
      spectrum->SetLineWidth(2     );
      spectrum->SetLineStyle(2      );//1=solid 2=dash 3=dot 4=dash-dot
      spectrum->SetMarkerStyle(22); // triangles
      spectrum->SetMarkerSize (0.5);// 
            
      spectrum->Draw(" L");  // points as markers P + line L
  
      text=new TText();
      text->SetTextFont(122);//Greek
      text->SetTextColor(kMagenta);
      text->SetTextSize(0.035 );
      text->SetTextAlign(12);
      // find first on-scale point for label 
      ip=0;y=-1.;
      while(y<galplotdef.gamma_spectrum_Imin*2&&ip<galaxy.n_E_gammagrid)    {spectrum->GetPoint(ip,x,y); ip++;}//AWS20030905
      
      
      latex=new TLatex();                  //screen and gif symbol support AWS20041021
      latex->SetTextAlign(12);
      latex->SetTextSize(0.035);
      latex->SetTextColor(kMagenta);
      //  latex->DrawLatex(x*1.2,y,"source population above limit"); AWS20060724
      
      
      spectrum_sourcepop1_soplimit=spectrum;
    }// if soplimit
  
  // total including sources
  if(galplotdef.sourcepop_sublimit==1) //AWS20060118
    {
      spectrum=new TGraph(galaxy.n_E_gammagrid);
      for  (ip    =0;  ip    <galaxy.n_E_gammagrid;ip    ++)
	{
	  spectrum_total              ->GetPoint(ip,x,y1);
	  spectrum_sourcepop1_sublimit->GetPoint(ip,x,y2);
	  spectrum                    ->SetPoint(ip,x,y1+y2);
	}
      spectrum->SetMarkerColor(kRed);
      spectrum->SetMarkerStyle(21); 
      spectrum->SetMarkerStyle(22); // triangles
      spectrum->SetMarkerSize (0.5);// 
      spectrum->SetLineColor(kMagenta );
      spectrum->SetLineWidth(4     );
      spectrum->SetLineStyle(1      );//1=solid 2=dash 3=dot 4=dash-dot
      spectrum->SetMarkerStyle(22); // triangles
      spectrum->SetMarkerSize (0.5);// 
            
      spectrum->Draw("PL");  // points as markers + line 
    }// if sublimit
  
    }// galplotdef.gamma_spectrum==1 || 2
  

 } //if(galdef.skymap_format==0)  AWS20100105

  ////////////////////////////////////////////////



  
  /////////////////////////////////////////////////////////////////////////////////////////////////
  // Porter ISRF
  /////////////////////////////////////////////////////////////////////////////////////////////////

  cout<<"galaxy.ISRF:"<<endl;

   int icomp,ir,iz,ix,iy,inu;
   //for(icomp=0;icomp<=2;icomp++) {   cout<<"ISRF component "<<icomp<<endl; galaxy.ISRF[icomp].print();}


   // at present the Galactic position for the ISRF is ad hoc

   ir=5;
   double r_isrf=8.5; // solar position           AWS20100104
   ir =   r_isrf/galaxy.dr ;                    //AWS20100104
   if (ir>galaxy.n_rgrid-1) ir=galaxy.n_rgrid-1;//AWS20100104   

   ix=int(ir/sqrt(2.)+.1);
   iy=ix;
   iz=40;                   // only valid for 4 kpc halo !
   iz=galaxy.n_zgrid/2 + 1; //AWS20100104
   cout<<"plot_mw_isrf: Porter ISRF:  ir="<<ir <<" ix="<<ix <<" iy="<<iy <<" iz="<<iz; //AWS20100114
   if(galdef.n_spatial_dimensions==2)   cout<<" r="<<galaxy.r[ir]                          <<" z="<<galaxy.z[iz]<<endl;
   if(galdef.n_spatial_dimensions==3)   cout<<" x="<<galaxy.x[ix]  <<" y="<<galaxy.y[iy]   <<" z="<<galaxy.z[iz]<<endl;
   cout<<"galaxy.ISRF[0].n_pgrid="<< galaxy.ISRF[0].n_pgrid  <<endl;

   for(icomp=0;icomp<=2;icomp++)
   {
    cout<<"ISRF component "<<icomp<<endl;

    for(inu=0; inu<galaxy.ISRF[0].n_pgrid ;inu++)
      {
      cout<<" galaxy.nu_ISRF[inu]=" <<galaxy.nu_ISRF[inu]<<" galaxy.ISRF  (eV cm-3)  = ";
      if(galdef.n_spatial_dimensions==2)   cout<< galaxy.ISRF[icomp].d2[ir]    [iz].s[inu]<<endl;
      if(galdef.n_spatial_dimensions==3)   cout<< galaxy.ISRF[icomp].d3[ix][iy][iz].s[inu]<<endl;
      }
    cout<<endl<<endl;
    }


   if(galdef.n_spatial_dimensions==2)   cout<<" r="<<galaxy.r[ir]                          <<" z="<<galaxy.z[iz]<<endl;
   if(galdef.n_spatial_dimensions==3)   cout<< "x="<<galaxy.x[ix]  <<" y="<<galaxy.y[iy]   <<" z="<<galaxy.z[iz]<<endl;

    for(inu=0; inu<galaxy.ISRF[0].n_pgrid ;inu++)
    {
     if(galdef.n_spatial_dimensions==2) 
        cout<< "nu= " <<                galaxy.nu_ISRF[inu]
            <<"  opt=  "<< galaxy.ISRF[0].d2[ir][iz].s[inu]
            <<"  IR= "   <<galaxy.ISRF[1].d2[ir][iz].s[inu]
            <<"  CMB="   <<galaxy.ISRF[2].d2[ir][iz].s[inu]
            <<" eV cm-3"<<endl;

     if(galdef.n_spatial_dimensions==3) 
        cout<< "nu= " <<                galaxy.nu_ISRF[inu]
            <<"  opt=  "<< galaxy.ISRF[0].d3[ix][iy][iz].s[inu]
            <<"  IR= "   <<galaxy.ISRF[1].d3[ix][iy][iz].s[inu]
            <<"  CMB="   <<galaxy.ISRF[2].d3[ix][iy][iz].s[inu]
            <<" eV cm-3"<<endl;
}


    // separate components
    int plot_optical=0;
    int plot_IR     =0;
    int plot_CMB    =1;

 
   for(icomp=0;icomp<=2;icomp++)
   {
   spectrum=new TGraph(galaxy.ISRF[0].n_pgrid   );

    double  E_nu, isrf_intensity;
    double factor= 1e-6 * C /(4.* Pi); // conversion  eV cm-3 to MeV cm-2 sr-1 s-1     using constants.h

    for  (inu    =0;  inu    <galaxy.ISRF[0].n_pgrid      ;inu    ++)
    {
     E_nu=  galaxy.nu_ISRF[inu]* H / MEV2ERG; // energy in MeV.  H is Plancks constant from constants.h

     if(galdef.n_spatial_dimensions==2)   isrf_intensity= factor* galaxy.ISRF[icomp].d2[ir]    [iz].s[inu]; 
     if(galdef.n_spatial_dimensions==3)   isrf_intensity= factor* galaxy.ISRF[icomp].d3[ix][iy][iz].s[inu];

     spectrum->SetPoint(inu , E_nu, isrf_intensity);
     cout<<"isrf E_nu="<< E_nu   <<" MeV  isrf_intensity="<<  isrf_intensity    <<endl;
	}
      
   
      if(icomp==0)spectrum->SetLineColor(kBlue);//optical
      if(icomp==1)spectrum->SetLineColor(kRed);//IR
      if(icomp==2)spectrum->SetLineColor(kGreen);//CMB
      spectrum->SetLineWidth(2     );
      spectrum->SetLineStyle(1      );//1=solid 2=dash 3=dot 4=dash-dot
     
      if((icomp==0&&plot_optical==1)||( icomp==1&&plot_IR==1)||( icomp==2&&plot_CMB==1)  )    
      spectrum->Draw(" L");  // points as markers P   line L

   }


   // opt+IR 

   spectrum=new TGraph(galaxy.ISRF[0].n_pgrid   );

    double  E_nu, isrf_intensity;
    double factor= 1e-6 * C /(4.* Pi); // conversion  eV cm-3 to MeV cm-2 sr-1 s-1     using constants.h

    for  (inu    =0;  inu    <galaxy.ISRF[0].n_pgrid      ;inu    ++)
    {
     E_nu=  galaxy.nu_ISRF[inu]* H / MEV2ERG; // energy in MeV.  H is Plancks constant from constants.h

     if(galdef.n_spatial_dimensions==2)
     isrf_intensity= factor*(galaxy.ISRF[0].d2[ir]    [iz].s[inu] + galaxy.ISRF[1].d2[ir]    [iz].s[inu])  ; 

     if(galdef.n_spatial_dimensions==3)
     isrf_intensity= factor*(galaxy.ISRF[0].d3[ix][iy][iz].s[inu] + galaxy.ISRF[1].d3[ix][iy][iz].s[inu])  ; 



     spectrum->SetPoint(inu , E_nu, isrf_intensity);
     cout<<"isrf E_nu="<< E_nu   <<" MeV  isrf_intensity opt+ir="<<  isrf_intensity    <<endl;
	}
      
   
      spectrum->SetLineColor(kMagenta);
      spectrum->SetLineWidth(2     );
      spectrum->SetLineStyle(1      );//1=solid 2=dash 3=dot 4=dash-dot
     
            
      spectrum->Draw(" L");  // points as markers P   line L





   ////////////////////////////////////////////////////////////////////////////////////////
   // synchrotron
   ////////////////////////////////////////////////////////////////////////////////////////


   cout<<"synchrotron isrf "<<endl;

   read_synchrotron_skymap();
   //  galaxy.synchrotron_skymap.print();

   valarray<double> sync_intensity(galaxy.n_nu_synchgrid);
   sync_intensity=0.;
 
   int n_mean=0;
   double sa_norm=0.0;

   for (int  ib       =0;        ib<galaxy.n_lat;                ib++)
   for (int  il       =0;        il<galaxy.n_long;               il++)
   {
     double l=galaxy.long_min + galaxy.d_long *il;
     double b=galaxy. lat_min + galaxy.d_lat  *ib;

     int select=0;
     // check whether point is in any of the 4 regions
     if(l>=galplotdef.long_min1&&l<=galplotdef.long_max1 && b>=galplotdef.lat_min1 && b<=galplotdef.lat_max1) select=1;
     if(l>=galplotdef.long_min2&&l<=galplotdef.long_max2 && b>=galplotdef.lat_min1 && b<=galplotdef.lat_max1) select=1;
     if(l>=galplotdef.long_min1&&l<=galplotdef.long_max1 && b>=galplotdef.lat_min2 && b<=galplotdef.lat_max2) select=1;
     if(l>=galplotdef.long_min2&&l<=galplotdef.long_max2 && b>=galplotdef.lat_min2 && b<=galplotdef.lat_max2) select=1;

     //    cout<<"l="<<l<<" b="<<b<<endl;

     if(select==1)
     {
       //     cout<<"selected l="<<l<<" b="<<b<<endl;
       i_lat=ib;
      solid_angle=sabin(galaxy.lat_min,galaxy.d_long,galaxy.d_lat,i_lat);

      n_mean++;
      sa_norm += solid_angle;

      for (inu =0; inu<galaxy.n_nu_synchgrid; inu++)
      {             
	sync_intensity[inu]   +=  (galaxy.synchrotron_skymap.d2[il][ib].s[inu] * solid_angle);
      //    cout<<"l="<<l<<" b="<<b<<" sync_intensity[inu]= "<<  sync_intensity[inu]  <<endl;
      }
    
 

     }//if select
   } // for ib, il

   sync_intensity /= sa_norm;      // solid-angle weighted intensity

   //emissivity (erg cm^-3 sr^-1 Hz^-1 s^-1)-> intensity erg cm-2 sr-1 Hz-1 s-1

   spectrum=new TGraph(  galaxy.n_nu_synchgrid  );

   factor= 1./MEV2ERG; // conversion  erg cm-2 sr-1 Hz-1 s-1  to MeV cm-2 sr-1 s-1     using constants.h

    for  (inu    =0;  inu    < galaxy.n_nu_synchgrid     ;inu    ++)
    {
     E_nu=  galaxy.nu_synch[inu]* H / MEV2ERG;                                      // energy in MeV.  H is Plancks constant from constants.h

     sync_intensity[inu] = factor * sync_intensity[inu] *  galaxy.nu_synch[inu]  ; 

     spectrum->SetPoint(inu , E_nu, sync_intensity[inu]);
     cout<<"sync E_nu="<< E_nu   <<" MeV  sync_intensity="<<  sync_intensity[inu]    <<endl;
    }
      
   
      spectrum->SetLineColor(kBlack);
      spectrum->SetLineWidth(2     );
      spectrum->SetLineStyle(1      );//1=solid 2=dash 3=dot 4=dash-dot
     
            
      spectrum->Draw(" L");  // points as markers P   line L



  //============== postscript, gif and txt files
  
  sprintf(workstring1,"l_%.2f_%.2f_%.2f_%.2f",                            //AWS20040315
	  galplotdef.long_min1,galplotdef.long_max1,
	  galplotdef.long_min2,galplotdef.long_max2);
  
  sprintf(workstring2,"b_%.2f_%.2f_%.2f_%.2f",                            //AWS20040315
	  galplotdef.lat_min1,galplotdef.lat_max1,
	  galplotdef.lat_min2,galplotdef.lat_max2);
  
  strcpy(psfile,"plots/");
  strcat(psfile,"mw_isrf_");
  strcat(psfile,galdef.galdef_ID);
  strcat(psfile,"_"        );
  strcat(psfile,workstring1);
  strcat(psfile,"_"        );
  strcat(psfile,workstring2);

  

  strcat(psfile,galplotdef.psfile_tag);

  strcpy(giffile,psfile);
  strcpy(txtfile,psfile);                        //AWS20041118
  strcpy(iosfile,psfile);                        //AWS20051124


  strcat(txtfile,".txt");
  cout<<txtfile<<endl;
  txtFILE=fopen(txtfile,"w");

  

  //============================================================================ 
  // further plots
  // needs txtFILE so put here
  plot_spectrum_healpix(0);    //AWS20091228

 //============== postscript and gif output


  if(galplotdef.output_format==1 || galplotdef.output_format==3)
   {
   strcat(psfile,".eps");
   cout<<"postscript file="<<psfile<<endl;
   c1->Print(psfile,"eps" );
  }

  if(galplotdef.output_format==2 || galplotdef.output_format==3)
   {
   strcat(giffile,".gif");
   cout<<"       gif file="<<giffile<<endl;
   c1->Print(giffile,"gif" );
  }

  //==============


  // ASCII, TeX output                           //AWS20041118
  /*
  cout<<endl<<"inverse Compton spectrum"<<endl;spectrum_ic        ->Print();
  cout<<endl<<"bremsstrahlung  spectrum"<<endl;spectrum_bremss    ->Print();
  cout<<endl<<"pi0-decay       spectrum"<<endl;spectrum_pi0       ->Print();
  cout<<endl<<"isotropic       spectrum"<<endl;spectrum_isotropic ->Print();
  cout<<endl<<"total           spectrum"<<endl;spectrum_total     ->Print();
  */

  cout<<txtfile<<endl;

if(galdef.skymap_format==0) //AWS20091126
 {

  if(galplotdef.gamma_spectrum==1 || galplotdef.gamma_spectrum==2) //AWS20050412
  {

  fprintf(txtFILE,"\n ==== Model spectra ==== ");
  fprintf(txtFILE," galdef ID %s\n", galdef.galdef_ID);
  fprintf(txtFILE,"\n");
  fprintf(txtFILE,"%6.2f < l < %6.2f, ",galplotdef.long_min1,galplotdef.long_max1);
  fprintf(txtFILE,"%6.2f < l < %6.2f\n",galplotdef.long_min2,galplotdef.long_max2);
  fprintf(txtFILE,"%6.2f < b < %6.2f, ",galplotdef. lat_min1,galplotdef. lat_max1);
  fprintf(txtFILE,"%6.2f < b < %6.2f\n",galplotdef. lat_min2,galplotdef. lat_max2);
  fprintf(txtFILE,"\n");

  fprintf(txtFILE,"     E(MeV)        E^2 * intensity (MeV cm^-2 sr^-1 s^-1)     \n");
  fprintf(txtFILE,"                pi0    bremss       IC     isotropic  total \n");
  fprintf(txtFILE,"\n");

   for  (ip    =0;  ip    <galaxy.n_E_gammagrid ;ip    ++) {

     spectrum_pi0   ->GetPoint(ip,x,y);
     fprintf(txtFILE,"%10.2e ",x );// energy
     fprintf(txtFILE,"%9.5f " ,y );// intensity

     spectrum_bremss->GetPoint(ip,x,y);
     fprintf(txtFILE,"%9.5f ",y );

     spectrum_ic->GetPoint(ip,x,y);
     fprintf(txtFILE,"%9.5f ",y );

     if (galplotdef.gamma_IC_selectcomp) {

       spectrum_ic_optical->GetPoint(ip,x,y);
       fprintf(txtFILE,"%9.5f ",y );
       
       spectrum_ic_ir->GetPoint(ip,x,y);
       fprintf(txtFILE,"%9.5f ",y );
       
       spectrum_ic_cmb->GetPoint(ip,x,y);
       fprintf(txtFILE,"%9.5f ",y );

     }

     spectrum_isotropic->GetPoint(ip,x,y);
     fprintf(txtFILE,"%9.5f ",y );

     spectrum_total    ->GetPoint(ip,x,y);
     fprintf(txtFILE,"%9.5f ",y );

     fprintf(txtFILE,"\n");
   }

  fprintf(txtFILE,"===============================================================\n\n");

  }// if


 } // if(galdef.skymap_format==0) AWS20100105
 



   cout<<" <<<< plot_mw_isrf   "<<endl;
   return status;
}

/////////////////////////////////////////////////////////////////
// isotropic intensity using list in EGRET ranges, in form E^2 I(E)
// assuming a power law in the range
// returns -1 if outside range of list

// double Galplot::isotropic_EGRET_from_list(double Egamma,double g)
// defined in plot_spectrum.cc






