// g++ test_gsl_gauss_sampler.cc -I/afs/ipp-garching.mpg.de/home/a/aws/gsl/gsl-1.10/olga/include -L//afs/ipp-garching.mpg.de/home/a/aws/gsl/gsl-1.10/olga/lib -lgsl -lgslcblas


using namespace std;
#include<iostream>
#include<cmath>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>





int main()
{
  cout<<" test_gsl_gauss_sampler"<<endl;

  // see example in gsl User Manual

      const gsl_rng_type * T;
       gsl_rng * r;
         
       /* create a generator chosen by the 
          environment variable GSL_RNG_TYPE */
     
       gsl_rng_env_setup();
     
       T = gsl_rng_default;
       r = gsl_rng_alloc (T);


       // that's the setup, now use it  (will need initializer if in subroutine)

  double sigma=0.1;
  double mean=0, std=0;

  double result;

  int n=10000;

  for (int i=0;i<n;i++)
  {

   result=gsl_ran_gaussian ( r,  sigma);

   mean+=result;
   std+=result*result;

   cout<<result<<endl;
    }

  mean/=n;
  std=sqrt(std/n);

  cout<<"sigma="<<sigma<<" mean="<<mean<<" std="<<std<<endl;
  return 0;
}
