// g++ test_gsl_gamma_function.cc -I/afs/ipp-garching.mpg.de/home/a/aws/gsl/gsl-1.10/olga/include -L//afs/ipp-garching.mpg.de/home/a/aws/gsl/gsl-1.10/olga/lib -lgsl -lgslcblas

using namespace std;
#include<iostream>
#include<cmath>
#include <gsl/gsl_sf.h>

/*
7.19.4 Incomplete Gamma Functions
~ Function: double gsl_sf_gamma_inc (double a, double x)
~ Function: int gsl_sf_gamma_inc_e (double a, double x, gsl_sf_result * result)

    These functions compute the unnormalized incomplete Gamma Function \Gamma(a,x) = \int_x^\infty dt t^{a-1} \exp(-t) for a real and x >= 0. 

~ Function: double gsl_sf_gamma_inc_Q (double a, double x)
~ Function: int gsl_sf_gamma_inc_Q_e (double a, double x, gsl_sf_result * result)

    These routines compute the normalized incomplete Gamma Function Q(a,x) = 1/\Gamma(a) \int_x^\infty dt t^{a-1} \exp(-t) for a > 0, x >= 0. 


*/


double   flux_cutoff_powerlaw(double E                , double g, double Ecutoff,double Emin_ref, double Emax_ref, double flux_ref)
{
  // computes flux at an energy       given the flux over a reference range.
  // for a cutoff powerlaw E^-g exp(-E/Ecutoff)
  // using: integral x_to_inf [t^-g e^-ct] = c^(g-1) integral cx_to_inf [t^-g e^-t] = c^(g-1) \Gamma(a,cx) with -g=a-1 -> a=1-g
  // From gsl manual:
  // double gsl_sf_gamma_inc (double a, double x)
  // is the unnormalized incomplete Gamma Function \Gamma(a,x) = \int_x^\infty dt t^{a-1} \exp(-t) for a real and x >= 0. 

  // tests in test_gsl_gamma_function.cc

  double a=1.0-g;
  double c=1.0/Ecutoff;
  
  double x3=Emin_ref;
  double x4=Emax_ref;

 

  // normalization to reference range
  double s_ref =  pow(c,(g-1)) *gsl_sf_gamma_inc(a,c*x3)
                - pow(c,(g-1)) *gsl_sf_gamma_inc(a,c*x4);

  
  double result = flux_ref / s_ref * pow(E,-g) * exp(-E/Ecutoff);

  return result;
 
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

double   flux_cutoff_powerlaw(double Emin, double Emax, double g, double Ecutoff,double Emin_ref, double Emax_ref, double flux_ref)
{
  // computes flux in an energy range given the flux over a reference range.
  // for a cutoff powerlaw E^-g exp(-E/Ecutoff)
  // using: integral x_to_inf [t^-g e^-ct] = c^(g-1) integral cx_to_inf [t^-g e^-t] = c^(g-1) \Gamma(a,cx) with -g=a-1 -> a=1-g
  // From gsl manual:
  // double gsl_sf_gamma_inc (double a, double x)
  // is the unnormalized incomplete Gamma Function \Gamma(a,x) = \int_x^\infty dt t^{a-1} \exp(-t) for a real and x >= 0. 

  // tests in test_gsl_gamma_function.cc

  double a=1.0-g;
  double c=1.0/Ecutoff;
  double x1=Emin;
  double x2=Emax;
  double x3=Emin_ref;
  double x4=Emax_ref;

  // integral over required range
  double s     =  pow(c,(g-1)) *gsl_sf_gamma_inc(a,c*x1)
                - pow(c,(g-1)) *gsl_sf_gamma_inc(a,c*x2);

  // normalization to reference range
  double s_ref =  pow(c,(g-1)) *gsl_sf_gamma_inc(a,c*x3)
                - pow(c,(g-1)) *gsl_sf_gamma_inc(a,c*x4);

  
  double result = flux_ref * s/s_ref;

  return result;
 
}


int main()
{
  cout<<" test_gsl_gamma function"<<endl;

  // see example in gsl User Manual

  double a,x,result;
  
  if(false)
    {
  for(a=-3;a<3;a+=.1)
  for(x=.01;x<10;x+=.1)
    {
  result=gsl_sf_gamma_inc(a,x);
  
    cout<<"a="<<a<<" x="<<x<<" result="<<result<<endl;
    }
    }
  // check integral x to inf t^-g e^-ct = c^(g-1) integral cx to inf t^-g e^-t = c^(g-1) \Gamma(a,cx) with -g=a-1-> a=1-g

  double c=3;
  double g=0.4;

  a=1-g;
  x=10;

  result=pow(c,(g-1)) *gsl_sf_gamma_inc(a,c*x);

   cout<<"c="<<c<<" x="<<x<<" result="<<result<<endl;


   double s=0;
   double dt=1e-4;
   double xmax=100;
   for(double t=x; t<xmax; t+=dt) s+= pow(t,-g) * exp(-c*t);
   s*=dt;
   cout<<"c="<<c<<" x="<<x<<" result="<<result<<" numerical integration="<<s<<endl;
  

   double Emin= 10.;
   double Emax=1000.;
   g=1.4;
   double Ecutoff= 500;
   double Emin_ref=10 ,Emax_ref=1e5, flux_ref=1;;

   double sum=0.; // over energy ranges, should equal flux_ref at end if ranges are equal

   for(Emin=10 ;Emax<Emax_ref;Emin*=2)
   {
    Emax=Emin*2;

    result= flux_cutoff_powerlaw( Emin,  Emax,g,  Ecutoff, Emin_ref, Emax_ref, flux_ref);

    sum+=result;
    cout<<"Emin="<<Emin<<" Emax="<<Emax<<" g="<<g<<" Ecutoff="<<Ecutoff<<" result="<<result<<" sum="<<sum<<endl;
   }

   double E;
   double integral=0.0;
   double factor=1.1;
   for(E=10 ;E<Emax_ref;E*=factor)
   {
   

    result= flux_cutoff_powerlaw(   E,g,  Ecutoff, Emin_ref, Emax_ref, flux_ref);

    integral += result*E*log(factor);// log integration

    cout<<"E="<<E<<" g="<<g<<" Ecutoff="<<Ecutoff<<" result="<<result<<" integral="<<integral<<endl;
   }


  return 0;
}
