#include "Model.h"
#include "Exposure.h"
#include "Counts.h"
#include "Psf.h"
#include "Sources.h"
#include "Parameters.h"
#include "Variables.h"
#include "Skymap.h"
#include <string>
#include <vector>

//The TestModel
#include "TestModel.h"
#include "SourceModel2.h"

#include "GasIC.h"
#include "HIH2IC.h"               //AWS20080422
#include "HIH2_3rings_IC.h"       //AWS20080423
#include "HIH2_flexirings_IC.h"   //AWS20080424
#include "HIH2Xco_flexirings_IC.h"//AWS20080430


JoinModel::JoinModel(const Exposure &exposure, const CountsMap &counts, const Psf &psf, const Sources &sources, const Parameters &pars) : ArrayModel(exposure,counts,psf,sources,pars){
	//Get the names of the models from the parameters
	std::vector<std::string> modelNames;
	fparameters.getParameter("modelNames", modelNames);

	std::vector<BaseModel*> models;
	
	//Loop the names and add the models to the list
	//To include your model, add it to this if structure
	for (int i = 0; i < modelNames.size(); ++i){
		if ( modelNames[i] == "SourceModel" ) {
			models.push_back( new SourceModel(exposure, counts, psf, sources, pars) );
		}else if (modelNames[i] == "SourceModel2" ) {
			models.push_back( new SourceModel2(exposure, counts, psf, sources, pars) );
		}else if (modelNames[i] == "EGBModel" ) {
			models.push_back( new EGBModel(exposure, counts, psf, sources, pars) );
		}else if (modelNames[i] == "ElectronProton" ) {
			models.push_back( new ElectronProton(exposure, counts, psf, sources, pars) );
		//The test model
		}else if (modelNames[i] == "TestModel" ) {
			models.push_back( new TestModel(exposure, counts, psf, sources, pars) );
		}else if (modelNames[i] == "GasIC" ) {
			models.push_back( new GasIC(exposure, counts, psf, sources, pars) );
		}else if (modelNames[i] ==   "HIH2IC" ) {                                            //AWS20080422
			models.push_back( new HIH2IC(exposure, counts, psf, sources, pars) );
		}else if (modelNames[i] ==   "HIH2_3rings_IC" ) {                                    //AWS20080423
			models.push_back( new HIH2_3rings_IC(exposure, counts, psf, sources, pars) );
		}else if (modelNames[i] ==   "HIH2_flexirings_IC" ) {                                //AWS20080424
			models.push_back( new HIH2_flexirings_IC(exposure, counts, psf, sources, pars) );
		}else if (modelNames[i] ==   "HIH2Xco_flexirings_IC" ) {                             //AWS20080430
			models.push_back( new HIH2Xco_flexirings_IC(exposure, counts, psf, sources, pars) );
		}else{
			std::cout<<"No model named "<<modelNames[i]<<std::endl;
		}
	}

	setModels(models);

}

JoinModel::~JoinModel(){
	//Delete the models
	for (int i = 0; i < fmodels.size(); ++i){
		delete fmodels[i];
	}
}

