#include"Galplot.h"                    //AWS20050920
#include"galprop_classes.h"
#include"galprop.h"
#include"constants.h"
#include"fitsio.h"


int Galplot::plot_convolved_GLAST_profile(int longprof, int latprof,int ip,int ic,int bremss,int pi0,int total)
{
   cout<<" >>>> plot_convolved_GLAST_profile    "<<endl;

   txt_stream<<"Profiles from plot_convolved_GLAST_profile      "<<endl;  //AWS20080206
   txt_stream<<data.E_GLAST[ip]<<" -  "<<data.E_GLAST[ip+1]<<" MeV"<<endl;//AWS20080206


   if(longprof==1)
   {
    txt_stream<<galplotdef. lat_min1<<" < b < "<<galplotdef. lat_max1<< " , ";
    txt_stream<<galplotdef. lat_min2<<" < b < "<<galplotdef. lat_max2<< endl;
   }

   if(latprof==1)
   {
    txt_stream<<galplotdef.long_min1<<" < l < "<<galplotdef.long_max1<< " , ";
    txt_stream<<galplotdef.long_min2<<" < l < "<<galplotdef.long_max2<< endl;
   }
 

   int status=0;


int i_comp,i_lat,i_long;
int ii_long,ii_lat;
int il_min,il_max,ib_min,ib_max;
int il_min1,il_max1,ib_min1,ib_max1;
int il_min2,il_max2,ib_min2,ib_max2;


double long_min;
double l,b,l_off;
double ic_total;
double ic_aniso_total;//AWS20060907
double isotropic_intensity;

char name[100],canvastitle[100], workstring1[100],workstring2[100],workstring3[100],workstring4[100];
char psfile[400];


TCanvas *c1;
TH2F *        ic_map;
TH2F *  ic_aniso_map; //AWS20060907
TH2F *    bremss_map;
TH2F * pi0_decay_map;
TH2F *     total_map;
TH2F * isotropic_map;

TH1D *profile;

double *profile_ic, *profile_ic_aniso, *profile_bremss, *profile_pi0_decay, *profile_total; //AWS20080206 for print output


 int ncolors=0; int *colors=0;// default palette of 50 colours AWS20050915
gStyle->SetPalette(ncolors,colors);

  //====== see HowTo Style: and do before creating canvas
 
 
   TStyle *plain  = new TStyle("Plain","Plain Style (no colors/fill areas)"); 

   
   plain->SetCanvasBorderMode(0);
   plain->SetPadBorderMode(0);
   plain->SetPadColor(0);
   plain->SetCanvasColor(0);
   plain->SetTitleColor(1,"X");//AWS20080125  was (0): changed for root 5.08
   plain->SetTitleColor(1,"Y");//AWS20080125
   plain->SetStatColor(0);
   gROOT->SetStyle("Plain");



  {
 {
 // names must be different or canvas disappears

  sprintf(name," %5.0f - %5.0f MeV",data.E_GLAST[ip],data.E_GLAST[ip+1]);
  strcpy(canvastitle,"galdef_");
  strcat(canvastitle,galdef.galdef_ID);
  strcat(canvastitle,name);




  ic_map=new TH2F("IC skymap",canvastitle,
                  galaxy.n_long,       -180,           180.,
                  galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1));

  ic_aniso_map=new TH2F("IC aniso skymap",canvastitle,                                               //AWS20060907
                  galaxy.n_long,       -180,           180.,
                  galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1));


  bremss_map=new TH2F("bremss skymap",canvastitle,
                  galaxy.n_long,       -180,           180.,
                  galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1));

  pi0_decay_map=new TH2F("pi0-decay skymap",canvastitle,
                  galaxy.n_long,       -180,           180.,
                  galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1));

      total_map=new TH2F("total     skymap",canvastitle,
                  galaxy.n_long,       -180,           180.,
                  galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1));

  isotropic_map=new TH2F("isotropic skymap",canvastitle,
                  galaxy.n_long,       -180,           180.,
                  galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1));




  isotropic_intensity=0.;//

  if(galplotdef.isotropic_type==1) // integrate power law
    isotropic_intensity=galplotdef.isotropic_const
                        *(  pow(data.E_GLAST[ip+1],1.0-galplotdef.isotropic_g)
                           -pow(data.E_GLAST[ip  ],1.0-galplotdef.isotropic_g)  )
                        /                         (1.0-galplotdef.isotropic_g);

  if(galplotdef.isotropic_type==2) // explicit list of values
    isotropic_intensity=galplotdef.isotropic_EGRET[ip];

  cout<<"isotropic_intensity="<<isotropic_intensity<<endl;






	 for (i_lat =0;  i_lat <galaxy.n_lat;  i_lat++ ){
          for(i_long=0;  i_long<galaxy.n_long; i_long++){

           l=galaxy.long_min+galaxy.d_long*i_long;
           b=galaxy. lat_min+galaxy. d_lat*i_lat;

// this puts the bins at the right place but is incremental
//              ic_map->Fill(l,b,        galaxy.IC_iso_skymap[i_comp].d2[i_long][i_lat].s[ip]);   

// this would require shifting the array:
            ic_total      =0;
            ic_aniso_total=0;                                                                      //AWS20060907

            for(i_comp=0;i_comp<galaxy.n_ISRF_components;i_comp++)
	      if(   galplotdef.gamma_IC_selectcomp==0                            //AWS20060310
                 ||(galplotdef.gamma_IC_selectcomp==1 && i_comp==0  )            //AWS20060310
                 ||(galplotdef.gamma_IC_selectcomp==2 && i_comp==1  )            //AWS20060310
                 ||(galplotdef.gamma_IC_selectcomp==3 && i_comp==2  ))           //AWS20060310
		{                                                                                  //AWS20060907
	     
                ic_total      += convolved.GLAST_IC_iso_skymap  [i_comp].d2[i_long][i_lat].s[ip];

		if(galdef.IC_anisotropic > 0)                                                      //AWS20060907
                ic_aniso_total+= convolved.GLAST_IC_aniso_skymap[i_comp].d2[i_long][i_lat].s[ip];  //AWS20060907

		}

            l_off=l-180.;
            if(l_off<0.)l_off+=360.0;
            ii_long= galaxy.n_long-l_off/galaxy.d_long;     // this is at least correct for galprop centred on 0.0,0.5...but needs checking AWS20081001
	    //	    cout<<"l l_off ii_long"<<l<<" "<<l_off<<" "<<ii_long<<endl;

	    // TH2D bins start at 1              
            ii_lat=i_lat+1;                                                                         //AWS20080901

               ic_map      ->SetBinContent(ii_long,ii_lat,ic_total);   
	       if(galdef.IC_anisotropic > 0)                                                        //AWS20060907
               ic_aniso_map->SetBinContent(ii_long,ii_lat,ic_aniso_total);                          //AWS20060907 

               bremss_map->SetBinContent(ii_long,ii_lat,convolved.GLAST_bremss_skymap   .d2[i_long][i_lat].s[ip]); //AWS20080901
            pi0_decay_map->SetBinContent(ii_long,ii_lat,convolved.GLAST_pi0_decay_skymap.d2[i_long][i_lat].s[ip]); //AWS20080901
    //      isotropic_map->SetBinContent(ii_long,ii_lat,isotropic_intensity                                     ); //AWS20080901
            isotropic_map->SetBinContent(ii_long,ii_lat,convolved.GLAST_isotropic_skymap.d2[i_long][i_lat].s[ip]); //AWS20080901
                total_map->SetBinContent(ii_long,ii_lat,convolved.GLAST_total_skymap    .d2[i_long][i_lat].s[ip]); //AWS20080901

            }  //  i_long
           }   //  i_lat

	 /*
  total_map->Add(       ic_map,1.0);   // TH1 function
  total_map->Add(   bremss_map,1.0);
  total_map->Add(pi0_decay_map,1.0);
	 */

	 //if( galplotdef.isotropic_use==1) AWS20020813: this is done in convolve_GLAST
	 //total_map->Add(isotropic_map,1.0);

  ic_map       ->GetXaxis()->SetTitle("Galactic longitude");
  ic_aniso_map ->GetXaxis()->SetTitle("Galactic longitude");                                      //AWS20060907 
  bremss_map   ->GetXaxis()->SetTitle("Galactic longitude");
  pi0_decay_map->GetXaxis()->SetTitle("Galactic longitude");

  // construct conventional Galactic longitude axis in two seqments, 180-0 and 340-180
  TF1 *f1=new TF1("f1","-x",   0,180); // root manual p. 149
  TF1 *f2=new TF1("f2","-x", 180,340); // root manual p. 149

  //                      xmin ymin xmax ymax, funcname   ndiv chopt gridlength
  TGaxis *axis1=new TGaxis(-180,-90 ,  0.,-90  , "f1",       9,""                );
  axis1->SetLabelSize(0.03);
  TGaxis *axis2=new TGaxis(  20,-90 ,180.,-90  , "f2",       8,""                );
  axis2->SetLabelSize(0.03);
  axis2->SetTitle("Galactic longitude");
  // fill in 0-340 between two axis segments, unlabelled
  // at present has no visibile effect but could be useful if more minor tickmarks added
  //                         xmin ymin xmax ymax   wmin wmax ndiv chopt gridlength
  TGaxis *axis3=new TGaxis(  0,  -90.  , 20., -90., 0., 20. ,0, "U"               );


  // do the plotting


  //  total_map->Draw("lego"); 
 


//==============================     Profiles  =============================

//------------------------------     axes      -------------------------

  // construct conventional Galactic longitude axis in two seqments, 180-0 and 340-180
  f1=new TF1("f1","-x",   0,180); // root manual p. 149
  f2=new TF1("f2","-x", 180,340); // root manual p. 149

  //                      xmin ymin xmax ymax, funcname   ndiv chopt gridlength
          axis1=new TGaxis(-180,0.  ,  0.,0.   , "f1",       9,""                );
  axis1->SetLabelSize(0.02);
          axis2=new TGaxis(  20,0.  ,180.,0.   , "f2",       8,""                );
  axis2->SetLabelSize(0.02);
  axis2->SetTitle("Galactic longitude");
  // fill in 0-340 between two axis segments, unlabelled
  // at present has no visibile effect but could be useful if more minor tickmarks added
  //                         xmin ymin xmax ymax   wmin wmax ndiv chopt gridlength
          axis3=new TGaxis(  0,    0.  , 20., 0.  , 0., 20. ,0, "U"               );


//----------------------------------------------------------------------
  // NB reversed longitude axis starting at +180 and decreasing

  l_off=180.-galplotdef.long_max ; if(l_off>360.)l_off-=360.; if(l_off<  0.)l_off+=360.;
  il_min =l_off/galdef.d_long;
  l_off=180.-galplotdef.long_min ; if(l_off>360.)l_off-=360.; if(l_off<  0.)l_off+=360.;
  il_max =l_off/galdef.d_long;
  cout<<"il_min  ilmax :"<<il_min <<" "<<il_max <<endl;



  ib_min=(galplotdef.lat_min-galdef.lat_min+.0001)/galdef.d_lat;
  ib_max=(galplotdef.lat_max-galdef.lat_min+.0001)/galdef.d_lat;
  cout<<"ib_min ibmax:"<<ib_min<<" "<<ib_max<<endl;




  // two l or two b ranges


  l_off=180.-galplotdef.long_max1; if(l_off>360.)l_off-=360.; if(l_off<  0.)l_off+=360.;
  il_min1=l_off/galdef.d_long;
  l_off=180.-galplotdef.long_min1; if(l_off>360.)l_off-=360.; if(l_off<  0.)l_off+=360.;
  il_max1=l_off/galdef.d_long;
  cout<<"il_min1 ilmax1:"<<il_min1<<" "<<il_max1<<endl;

  l_off=180.-galplotdef.long_max2; if(l_off>360.)l_off-=360.; if(l_off<  0.)l_off+=360.;
  il_min2=l_off/galdef.d_long;
  l_off=180.-galplotdef.long_min2; if(l_off>360.)l_off-=360.; if(l_off<  0.)l_off+=360.;
  il_max2=l_off/galdef.d_long;
  cout<<"il_min2 ilmax2:"<<il_min2<<" "<<il_max2<<endl;


  ib_min1=(galplotdef.lat_min1-galdef.lat_min+.0001)/galdef.d_lat;
  ib_max1=(galplotdef.lat_max1-galdef.lat_min+.0001)/galdef.d_lat;
  cout<<"ib_min1 ibmax1:"<<ib_min1<<" "<<ib_max1<<endl;

  ib_min2=(galplotdef.lat_min2-galdef.lat_min+.0001)/galdef.d_lat;
  ib_max2=(galplotdef.lat_max2-galdef.lat_min+.0001)/galdef.d_lat;
  cout<<"ib_min2 ibmax2:"<<ib_min2<<" "<<ib_max2<<endl;






  long_min=-180.; // ic_map etc have longitude centred at 0.0



  //---------- total           

  if(total==1)
    {
     gStyle->SetHistLineColor(kBlue);//1=black 2=red 3=green 4=blue


     // -------------------------- longitude profile

  if(longprof)
    {

  sprintf(workstring2,"  %5.1f>b>%5.1f,%5.1f>b>%5.1f      ",
                         galplotdef.lat_min1,galplotdef.lat_max1,
                         galplotdef.lat_min2,galplotdef.lat_max2);
  strcpy(workstring1,canvastitle);
  strcat(workstring1,workstring2);

  strcat(name,"total_longitude");// to identify it: name is not plotted
  c1=new TCanvas(name,canvastitle,600+ip*10,150+ip*10,600,600);
  profile=     total_map->ProjectionX("total " , ib_min1,ib_max1);


  profile->Add(total_map->ProjectionX("total2" , ib_min2,ib_max2),1.0);
  profile->Scale(1.0/(ib_max1-ib_min1+1 + ib_max2-ib_min2+1));


  //  profile->GetXaxis()->SetTitle("Galactic longitude"); AWS20080208 don't duplicate axis title
  profile->GetXaxis()->SetNdivisions(0); 

  profile->GetYaxis()->SetTitle("intensity, cm^{-2} sr^{-1} s^{-1}  ");
  profile->GetYaxis()->SetTitleOffset(1.2);
//profile->GetYaxis()->SetTitleSize  (0.001); seems to be problematic
  profile->GetYaxis()->SetLabelSize  (0.030);

  profile->SetTitle(""         ); //written in box on plot

  profile->SetMinimum(0.0);                       //AWS20080125
  profile->SetMaximum(profile->GetMaximum()*1.5); // to allow for data with larger range
 

  if(galplotdef.sources_EGRET==300) // special case of MILAGRO data  AWS20080208
  {
   profile->SetMaximum(12.5e-12);
   profile->SetMinimum(-5.0e-12);
   // move axis down to appropriate position 
   //               xmin   ymin    xmax     ymax,   funcname   ndiv chopt gridlength
  axis1=new TGaxis(-180, -5.0e-12, 0.,    -5.0e-12 , "f1",       9,""                );
  axis1->SetLabelSize(0.02);
  axis2=new TGaxis(  20, -5.0e-12 ,180.,  -5.0e-12 , "f2",       8,""                );
  axis2->SetLabelSize(0.02);
  axis2->SetTitle("Galactic longitude");


  }


  TGaxis::SetMaxDigits(4);  // see root #1329 and mail root 20 Aug 2002

  profile->SetStats(kFALSE);
  profile->SetLineStyle(1     );// 1=solid 2=dash
  profile->SetLineWidth(3     );
  profile->SetLineColor(kBlue);//AWS20050916
  
  profile->Draw("L");//L=line
  axis1->Draw();   axis2->Draw();   axis3->Draw();

  profile_total = new double[galaxy.n_long];                                                                  //AWS20080206
  for (i_long=0;    i_long < galaxy.n_long;i_long++)  profile_total[i_long] = profile->GetBinContent(i_long+1);//AWS20080206
         
    }// if longprof

  // -------------------------- latitude profile

  if(latprof)
    {
      /*
  il_min=(galplotdef.long_min-       long_min+.0001)/galdef.d_long;
  il_max=(galplotdef.long_max-       long_min+.0001)/galdef.d_long;
  cout<<"il_min ilmax:"<<il_min<<" "<<il_max<<endl;
      */
  sprintf(workstring2,"  %5.1f>l>%5.1f,%5.1f>l>%5.1f       ",
                 	  galplotdef.long_min1,galplotdef.long_max1,
                          galplotdef.long_min2,galplotdef.long_max2);
  strcpy(workstring1,canvastitle);
  strcat(workstring1,workstring2);



  strcat(name,"total_latitude");
  c1=new TCanvas(name,canvastitle,650+ip*10,150+ip*10,600,600);
  if(galplotdef.lat_log_scale==1)c1->SetLogy();                //a TCanvas is a TPad, TPad has this function

  profile=     total_map->ProjectionY("total3"     , il_min1,il_max1);
  profile->Add(total_map->ProjectionY("total4"     , il_min2,il_max2),1.0);
  profile->Scale(1./(il_max1-il_min1+1 +il_max2-il_min2+1 ));
  profile->GetXaxis()->SetTitle("Galactic latitude");
  profile->GetYaxis()->SetTitle("intensity, cm^{-2} sr^{-1} s^{-1}   ");
  profile->GetYaxis()->SetTitleOffset(1.2);
//profile->GetYaxis()->SetTitleSize  (0.001); seems to be problematic
  profile->GetYaxis()->SetLabelSize  (0.030);
  profile->SetTitle(""         ); //written in box on plot

  if(galplotdef.lat_log_scale==0)
   profile->SetMaximum(profile->GetMaximum()*1.5); // to allow for data with larger range  
  if(galplotdef.lat_log_scale==1)
   profile->SetMaximum(profile->GetMaximum()*2.0); 


  if(galplotdef.lat_log_scale==0)
   profile->SetMinimum(0.0);                         //AWS20080125
  if(galplotdef.lat_log_scale==1)                     //AWS20040301
   profile->SetMinimum(profile->GetMaximum()/1e4);    //AWS20040301

  if(galplotdef.sources_EGRET==300) // special case of MILAGR0 data AWS20080108
  {
   profile->SetMaximum( 5.0e-12);
   profile->SetMinimum(-2.0e-12);
  }

  TGaxis::SetMaxDigits(4); // see root #1329 and mail root 20 Aug 2002

  profile->SetStats(kFALSE);
  profile->SetLineStyle(1     );// 1=solid 2=dash
  profile->SetLineWidth(3     );
  profile->SetLineColor(kBlue);//AWS20050916
  profile->Draw("L");//L=line

  profile_total = new double[galaxy.n_lat];                                                                   //AWS20080206
  for (i_lat=0;      i_lat < galaxy.n_lat;i_lat++)  profile_total[i_lat] = profile->GetBinContent(i_lat+1);   //AWS20080206


    }//if latprof



    }//if total



	  //----- inverse Compton

	  if(ic==1)
	    {
	      gStyle->SetHistLineColor(kGreen);// used to create histogram; 1=black 2=red 3=green 4=blue
	    
  if(longprof)
    {

      //strcat(name,"ic_longitude");// to identify it: name is not plotted
      //c1=new TCanvas(name,canvastitle,300+ip*10,150+ip*10,600,600);


  profile=     ic_map   ->ProjectionX("inverse Compton1",  ib_min1,ib_max1);       //AWS20040301 
  profile->Add(ic_map   ->ProjectionX("inverse Compton2" , ib_min2,ib_max2),1.0);
  profile->Scale(1.0/(ib_max1-ib_min1+1 + ib_max2-ib_min2+1));
  
  profile->GetXaxis()->SetTitle("Galactic longitude");
  profile->GetXaxis()->SetNdivisions(0); // removes longitude axis since done separately
  profile->GetYaxis()->SetTitle("intensity");
  profile->SetTitle(workstring1); //written in box on plot

  profile->SetLineStyle(1     );// 1=solid 2=dash                                                               AWS20080208
  profile->SetLineWidth(3     );
  profile->Draw("same L"); // L=line (instead of histogram)
  profile->SetLineColor(kGreen);//AWS20050916
  axis1->Draw();   axis2->Draw();   axis3->Draw();

  profile_ic    = new double[galaxy.n_long];                                                                   //AWS20080206
  for (i_long=0;    i_long < galaxy.n_long;i_long++)  profile_ic   [i_long] = profile->GetBinContent(i_long+1);//AWS20080206

    }//longprof
 

  if(latprof)
    {
      /*
  il_min=(galplotdef.long_min-       long_min+.0001)/galdef.d_long;
  il_max=(galplotdef.long_max-       long_min+.0001)/galdef.d_long;
  cout<<"il_min ilmax:"<<il_min<<" "<<il_max<<endl;
      */
  sprintf(workstring2,"  %5.1f>l>%5.1f",galplotdef.long_min,galplotdef.long_max);
  strcpy(workstring1,canvastitle);
  strcat(workstring1,workstring2);



  //  strcat(name,"ic_latitude");
  //  c1=new TCanvas(name,canvastitle,350+ip*10,150+ip*10,600,600);
  profile=     ic_map->ProjectionY("inverse Compton3", il_min1,il_max1);              //AWS20040301
  profile->Add(ic_map->ProjectionY("inverse Compton4", il_min2,il_max2),1.0);         //AWS20040301
  profile->Scale(1./(il_max1-il_min1+1 +il_max2-il_min2+1 ));

  profile->GetXaxis()->SetTitle("Galactic latitude");
  profile->GetYaxis()->SetTitle("intensity");
  profile->SetTitle(workstring1); //written in box on plot


  profile->SetLineStyle(1     );// 1=solid 2=dash 3=dot                                                         AWS20080208
  profile->SetLineWidth(3     );
  profile->SetLineColor(kGreen);//AWS20050916
  profile->Draw("same L");//L=line (instead of histogram)

  profile_ic    = new double[galaxy.n_lat];                                                                   //AWS20080206
  for (i_lat=0;      i_lat < galaxy.n_lat;i_lat++)  profile_ic   [i_lat] = profile->GetBinContent(i_lat+1);   //AWS20080206

    }//latprof



  }//if inverse Compton


	  //----- anisotropic inverse Compton              AWS20060907

	  if(ic==1)
	  {
	    if(galdef.IC_anisotropic > 0)                                                       //AWS20060907
	    {
	      gStyle->SetHistLineColor(kGreen);// used to create histogram; 1=black 2=red 3=green 4=blue
	    
  if(longprof)
    {

      //strcat(name,"ic_longitude");// to identify it: name is not plotted
      //c1=new TCanvas(name,canvastitle,300+ip*10,150+ip*10,600,600);


  profile=     ic_aniso_map   ->ProjectionX("inverse Compton5",  ib_min1,ib_max1);       //AWS20040301 
  profile->Add(ic_aniso_map   ->ProjectionX("inverse Compton6" , ib_min2,ib_max2),1.0);
  profile->Scale(1.0/(ib_max1-ib_min1+1 + ib_max2-ib_min2+1));
  
  profile->GetXaxis()->SetTitle("Galactic longitude");
  profile->GetXaxis()->SetNdivisions(0); // removes longitude axis since done separately
  profile->GetYaxis()->SetTitle("intensity");
  profile->SetTitle(workstring1); //written in box on plot

  profile->SetLineStyle(2     );// 1=solid 2=dash
  profile->SetLineWidth(6     );
  profile->Draw("same L"); // L=line (instead of histogram)
  profile->SetLineColor(kGreen);//AWS20050916
  axis1->Draw();   axis2->Draw();   axis3->Draw();


  profile_ic    = new double[galaxy.n_long];                                                                   //AWS20080206
  for (i_long=0;    i_long < galaxy.n_long;i_long++)  profile_ic   [i_long] = profile->GetBinContent(i_long+1);//AWS20080206

    }//longprof
 

  if(latprof)
    {
      /*
  il_min=(galplotdef.long_min-       long_min+.0001)/galdef.d_long;
  il_max=(galplotdef.long_max-       long_min+.0001)/galdef.d_long;
  cout<<"il_min ilmax:"<<il_min<<" "<<il_max<<endl;
      */
  sprintf(workstring2,"  %5.1f>l>%5.1f",galplotdef.long_min,galplotdef.long_max);
  strcpy(workstring1,canvastitle);
  strcat(workstring1,workstring2);



  //  strcat(name,"ic_latitude");
  //  c1=new TCanvas(name,canvastitle,350+ip*10,150+ip*10,600,600);
  profile=     ic_aniso_map->ProjectionY("inverse Compton7", il_min1,il_max1);              //AWS20040301
  profile->Add(ic_aniso_map->ProjectionY("inverse Compton8", il_min2,il_max2),1.0);         //AWS20040301
  profile->Scale(1./(il_max1-il_min1+1 +il_max2-il_min2+1 ));

  profile->GetXaxis()->SetTitle("Galactic latitude");
  profile->GetYaxis()->SetTitle("intensity");
  profile->SetTitle(workstring1); //written in box on plot


  profile->SetLineStyle(2     );// 1=solid 2=dash 3=dot
  profile->SetLineWidth(6     );
  profile->SetLineColor(kGreen);//AWS20050916
  profile->Draw("same L");//L=line (instead of histogram)

  profile_ic    = new double[galaxy.n_lat];                                                                   //AWS20080206
  for (i_lat=0;      i_lat < galaxy.n_lat;i_lat++)  profile_ic   [i_lat] = profile->GetBinContent(i_lat+1);   //AWS20080206


    }//latprof


    }// if aniso IC
  }//if inverse Compton



  //------- bremss

  if(bremss==1)
    {
	      gStyle->SetHistLineColor(kCyan);// used to create histogram; 1=black 2=red 3=green 4=blue 5=yellow
  if(longprof)
    {
      //  strcat(name,"bremss_longitude");// to identify it: name is not plotted
      //  c1=new TCanvas(name,canvastitle,400+ip*10,150+ip*10,600,600);

  profile=     bremss_map->ProjectionX("bremsstrahlung1" , ib_min1,ib_max1);       //AWS20040301
  profile->Add(bremss_map->ProjectionX("bremsstrahlung2" , ib_min2,ib_max2),1.0);
  profile->Scale(1.0/(ib_max1-ib_min1+1 + ib_max2-ib_min2+1));
 
  profile->GetXaxis()->SetTitle("Galactic longitude");
  profile->GetXaxis()->SetNdivisions(0); 
  profile->GetYaxis()->SetTitle("intensity");
  profile->SetTitle(workstring1); //written in box on plot

  profile->SetLineStyle(1     );// 1=solid 2=dash 3=dot 4=dash-dot                                               AWS20080208
  profile->SetLineWidth(3     );
  profile->SetLineColor(kCyan);//AWS20050916
  profile->Draw("same L");
  axis1->Draw();   axis2->Draw();   axis3->Draw();

  
  profile_bremss= new double[galaxy.n_long];                                                                    //AWS20080206
  for (i_long=0;    i_long < galaxy.n_long;i_long++)  profile_bremss[i_long] = profile->GetBinContent(i_long+1);//AWS20080206

    }//if

  if(latprof)
    {
      /*
  il_min=(galplotdef.long_min-       long_min+.0001)/galdef.d_long;
  il_max=(galplotdef.long_max-       long_min+.0001)/galdef.d_long;
  cout<<"il_min ilmax:"<<il_min<<" "<<il_max<<endl;
      */
  sprintf(workstring2,"  %5.1f>l>%5.1f",galplotdef.long_min,galplotdef.long_max);
  strcpy(workstring1,canvastitle);
  strcat(workstring1,workstring2);



  //  strcat(name,"bremss_latitude");
  //  c1=new TCanvas(name,canvastitle,450+ip*10,150+ip*10,600,600);
  profile=     bremss_map->ProjectionY("bremsstrahlung3", il_min1,il_max1);          //AWS20040301
  profile->Add(bremss_map->ProjectionY("bremsstrahlung4", il_min2,il_max2),1.0);     //AWS20040301
  profile->Scale(1./(il_max1-il_min1+1 +il_max2-il_min2+1 ));


  profile->GetXaxis()->SetTitle("Galactic latitude");
  profile->GetYaxis()->SetTitle("intensity");
  profile->SetTitle(workstring1); //written in box on plot

  profile->SetLineStyle(1     );// 1=solid 2=dash 3=dot 4=dash-dot                                               AWS20080208
  profile->SetLineWidth(3     );
  profile->SetLineColor(kCyan);//AWS20050916
  profile->Draw("same L");

  profile_bremss= new double[galaxy.n_lat];                                                                    //AWS20080206
  for (i_lat=0;      i_lat < galaxy.n_lat;i_lat++)  profile_bremss[i_lat] = profile->GetBinContent(i_lat+1);   //AWS20080206

    }//if



  }//if bremsstrahlung


  //---------- pi0-decay
  if(pi0==1)
    {
	      gStyle->SetHistLineColor(kRed);// used to create histogram; 1=black 2=red 3=green 4=blue
  if(longprof)
    {
      //  strcat(name,"pi0_longitude");// to identify it: name is not plotted
      //  c1=new TCanvas(name,canvastitle,500+ip*10,150+ip*10,600,600);

  profile=     pi0_decay_map->ProjectionX("pi0-decay1" , ib_min1,ib_max1);          //AWS20040301
  profile->Add(pi0_decay_map->ProjectionX("pi0-decay2" , ib_min2,ib_max2),1.0);
  profile->Scale(1.0/(ib_max1-ib_min1+1 + ib_max2-ib_min2+1));

  profile->GetXaxis()->SetTitle("Galactic longitude");
  profile->GetXaxis()->SetNdivisions(0); 
  profile->GetYaxis()->SetTitle("intensity");

  profile->SetTitle(workstring1); //written in box on plot

  profile->SetLineStyle(1     );// 1=solid 2=dash 3=dot                                                              AWS20080208
  profile->SetLineWidth(3     );
  profile->SetLineColor(kRed);//AWS20050916
  profile->Draw("same L");
  axis1->Draw();   axis2->Draw();   axis3->Draw();

  profile_pi0_decay = new double[galaxy.n_long];                                                                   //AWS20080206
  for (i_long=0;    i_long < galaxy.n_long;i_long++)  profile_pi0_decay[i_long] = profile->GetBinContent(i_long+1);//AWS20080206

    }

  // -------------------------------------- latitude profile

  if(latprof)
    {
      /*
  il_min=(galplotdef.long_min-       long_min+.0001)/galdef.d_long;
  il_max=(galplotdef.long_max-       long_min+.0001)/galdef.d_long;
  cout<<"il_min ilmax:"<<il_min<<" "<<il_max<<endl;
      */
  sprintf(workstring2,"  %5.1f>l>%5.1f",galplotdef.long_min,galplotdef.long_max);
  strcpy(workstring1,canvastitle);
  strcat(workstring1,workstring2);



  //  strcat(name,"pi0_latitude");
  //  c1=new TCanvas(name,canvastitle,550+ip*10,150+ip*10,600,600);
  profile=     pi0_decay_map->ProjectionY("pi0-decay3", il_min1,il_max1);            //AWS20040301
  profile->Add(pi0_decay_map->ProjectionY("pi0-decay4", il_min2,il_max2),1.0);       //AWS20040301
  profile->Scale(1./(il_max1-il_min1+1 +il_max2-il_min2+1 ));

 
  profile->GetXaxis()->SetTitle("Galactic latitude");
  profile->GetYaxis()->SetTitle("intensity");
  profile->SetTitle(workstring1); //written in box on plot

  profile->SetLineStyle(1     );// 1=solid 2=dash 3=dot                                                            AWS20080208
  profile->SetLineWidth(3     );
  profile->SetLineColor(kRed);//AWS20050916
  profile->Draw("same L");

  profile_pi0_decay = new double[galaxy.n_lat];                                                                   //AWS20080206
  for (i_lat=0;      i_lat < galaxy.n_lat;i_lat++)  profile_pi0_decay[i_lat] = profile->GetBinContent(i_lat+1);   //AWS20080206

    }//if latprof



    }//if pi0-decay

  // isotropic background
  if(galplotdef.isotropic_use==1)
  {
    gStyle->SetHistLineColor(kBlack);

    // -------------------------------------- longitude profile
  if(longprof)
  {
    profile=new TH1D("isotropic","isotropic1",galaxy.n_long,-180.,180.);       //AWS20040301 

    for(i_long=0;  i_long<galaxy.n_long; i_long++)
     profile->SetBinContent(i_long,isotropic_intensity);
  }

  if(latprof)
  {
    profile=new TH1D("isotropic","isotropic2",galaxy.n_lat,-90.,90.);          //AWS20040301
   
    for (i_lat =0;  i_lat <galaxy.n_lat;  i_lat++ )
    {
     ii_lat=i_lat+1;                                                            //AWS20080207
     profile->SetBinContent(ii_lat,isotropic_intensity);                        //AWS20080207
    }

  }

  profile->SetLineColor(kBlack);//AWS20050916
  profile->Draw("same L");





  }// if isotropic

  //////////////////////////////////////////////////////////////////////////////////////////////

  // parameters labelling

  sprintf(workstring1,"  %5.2f<l<%5.2f , %5.2f<l<%5.2f",                 //AWS20040315
                          galplotdef.long_min1,galplotdef.long_max1,
                          galplotdef.long_min2,galplotdef.long_max2);
 
  sprintf(workstring2,"  %5.2f<b<%5.2f , %5.2f<b<%5.2f",                 //AWS20040315
                         galplotdef.lat_min1,galplotdef.lat_max1,
                         galplotdef.lat_min2,galplotdef.lat_max2);
  
  sprintf(workstring3," %5.0f - %5.0f MeV",data.E_GLAST[ip],data.E_GLAST[ip+1]);


  strcpy(workstring4," galdef ID ");
  strcat(workstring4,galdef.galdef_ID);


  TText *text=new TText();
  text->SetTextFont(62);
  text->SetTextColor(kBlack);
  text->SetTextSize(0.025 );
  text->SetTextAlign(12);

  if(longprof)
  text->DrawTextNDC(.56 ,.85 ,workstring2);// NDC=normalized coord system  AWS20040315
  if(latprof)
  text->DrawTextNDC(.52 ,.85 ,workstring1);// NDC=normalized coord system  AWS20040315
  text->DrawTextNDC(.58 ,.88 ,workstring3);// NDC=normalized coord system
  text->DrawTextNDC(.20 ,.92 ,workstring4);// NDC=normalized coord system




  plot_GLAST_profile(longprof,latprof,ip,0);

  plot_GLAST_profile_healpix(longprof,latprof,ip,0); //AWS20080520


  //plot_source_population_profile(longprof,  latprof, ip,  1,1, 1);//AWS20051108
  plot_source_population_profile(longprof,  latprof, ip,  1,1, 5);//AWS20110818 sourcepop5 

  //  if(galplotdef.sources_EGRET==300)plot_MILAGRO_profile(longprof,latprof,ip,0); //AWS20080107 20131113


 }//if E_gamma
} //ip

//============================================================================

  //============== postscript output

  sprintf(workstring1,"l_%.2f_%.2f_%.2f_%.2f",                           //AWS20040315
                          galplotdef.long_min1,galplotdef.long_max1,
                          galplotdef.long_min2,galplotdef.long_max2);

  sprintf(workstring2,"b_%.2f_%.2f_%.2f_%.2f",                           //AWS20040315
                          galplotdef.lat_min1,galplotdef.lat_max1,
                          galplotdef.lat_min2,galplotdef.lat_max2);

  sprintf(workstring3,"%.0f-%.0f_MeV",data.E_GLAST[ip],data.E_GLAST[ip+1]);



  strcpy(psfile,"plots/");
  if(longprof==1)
  strcat(psfile,"longitude_profile_GLAST_");
  if(latprof==1)
  strcat(psfile,"latitude_profile_GLAST_");
  strcat(psfile,galdef.galdef_ID);
  strcat(psfile,"_"        );
  strcat(psfile,workstring3);
  strcat(psfile,"_"        );
  if(longprof==1)
  strcat(psfile,workstring2);
  if( latprof==1)
  strcat(psfile,workstring1);

  if(galplotdef.convolve_GLAST==0)
  strcat(psfile,"_unconv_"  );
  if(galplotdef.convolve_GLAST!=0)
  strcat(psfile,"_conv_"    );

  strcat(psfile,galplotdef.psfile_tag);

  strcat(psfile,".eps");

  cout<<"postscript file="<<psfile<<endl;
  c1->Print(psfile       );
  //==============



  // print output
  txt_stream<<endl<<"gamma-ray intensities cm^-2 sr^-1 s^-1 "<<endl;

 if(longprof==1)
 {
   txt_stream<<" l       pi0          bremss           IC       isotropic    total "<<endl; // same order as in spectrum printout
  for (i_long=0;i_long<galaxy.n_long;i_long++)
  {
 
   l= 180.-  +galaxy. d_long*i_long; //profile has reverse axis
   if(l<0.0)l+=360.0;
   txt_stream <<l<<"    "<< profile_pi0_decay[i_long]<<"  " << profile_bremss[i_long]
                 <<"  " << profile_ic[i_long]<<"  " << isotropic_intensity<<"  " << profile_total[i_long]         <<endl;
  }
 }



 if(latprof==1)
 {
    txt_stream<<" b        pi0          bremss           IC       isotropic    total"<<endl; // same order as in spectrum printout
  for (i_lat=0;i_lat<galaxy.n_lat;i_lat++)
  {

   b=    galaxy. lat_min+galaxy. d_lat*i_lat;
    txt_stream <<b<<"    "<< profile_pi0_decay[i_lat ]<<"  " << profile_bremss[i_lat ]
               <<    "  " << profile_ic       [i_lat ]<<"  " << isotropic_intensity<<"  " << profile_total[i_lat ]         <<endl;
  }
 }


 

   cout<<" <<<< plot_convolved_GLAST_profile   "<<endl;
   return status;
}


 
