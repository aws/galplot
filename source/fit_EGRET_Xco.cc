
// saved before combining E ranges as  fit_EGRET_Xco.cc_beforeEranges

#include"Galplot.h"                    //AWS20050920
#include"galprop_classes.h"
#include"galprop.h"
#include"constants.h"
#include"fitsio.h"


// shared variables with file scope
static int  n_values_fcn;
static double *x,*y,*z,*errorz;
static double *x1,*x2,*x3,*x4,*x5,*x6,*x7,*x8,*x9,*x10,*x11,*x12,*x13,*x14,*x15;
static double *zcounts,*zexposure;

static double func2(double x,          double *par);
static double func3(double x,double y, double *par);
static double func4(double x1,double x2, double x3,double *par);

static void    fcn2(int &npar, double *gin, double &f, double *par, int iflag);
static void    fcn3(int &npar, double *gin, double &f, double *par, int iflag);
static void    fcn4(int &npar, double *gin, double &f, double *par, int iflag);
static void    fcn5(int &npar, double *gin, double &f, double *par, int iflag);
static void    fcn6(int &npar, double *gin, double &f, double *par, int iflag);
static void    fcn7(int &npar, double *gin, double &f, double *par, int iflag);
static void    fcn7poisson
                   (int &npar, double *gin, double &f, double *par, int iflag);
static void    fcn8(int &npar, double *gin, double &f, double *par, int iflag);
static void    fcn8poisson
                   (int &npar, double *gin, double &f, double *par, int iflag);


static void    fcna6(int &npar, double *gin, double &f, double *par, int iflag);
static void    fcna6poisson
                    (int &npar, double *gin, double &f, double *par, int iflag);
static void    fcnb6(int &npar, double *gin, double &f, double *par, int iflag);
static void    fcnb6poisson
                    (int &npar, double *gin, double &f, double *par, int iflag);

static void    fcnq6(int &npar, double *gin, double &f, double *par, int iflag);
static void    fcnq7(int &npar, double *gin, double &f, double *par, int iflag);


Distribution EGRET_exposure_rebinned;
Distribution EGRET_counts_rebinned;
//int rebin_EGRET_data(int longbin, int latbin,double &long_min, double &lat_min, double &d_long, double &d_lat);
//////////////////////////////////////////////////////////////////////////////////////////

int Galplot::fit_EGRET_Xco(int options)                             // AWS20050104
{
  if(options==1)cout<<" >>>> fit_EGRET_Xco  options= "<<options<<" statistical error "                <<endl;
  if(options==2)cout<<" >>>> fit_EGRET_Xco  options= "<<options<<" statistical plus systematic error "<<endl;

   int status=0;

int i_E_EGRET;
int i_E_EGRET1[20],i_E_EGRET2[20];
int i_E_fit_range,n_E_fit_range;

int ivalues,iivalues,nvalues,n_values_rebin,n_values_rebin_  ;


int i_comp,i_long,i_lat,ii_long,ii_long_,ii_lat;
int ii_long_1, ii_long_2,ii_lat_1,ii_lat_2;
int n_ii;
int i_Ring,n_Ring;

int use_EGRET_rebinned_data;
int EGRET_counts_n_rgrid; // value depends on rebinning of data
int EGRET_counts_n_zgrid; // value depends on rebinning of data

double long_min,lat_min;
double d_long, d_lat;

int longbin,latbin;

double l,b,l_off;
double l_1,l_2,b_1,b_2;
double ic_total;
double isotropic_intensity;
double pred,obs,counts,exposure,error_estimate;
double pred_IC,pred_gas;
double  pred_HIT, pred_H2T;
double *pred_HIR,*pred_H2R;
double maximum_intensity;

double parmin,parmax; // bounds on parameters   AWS20070711

 double *background_spectrum,          *background_integral_spectrum;
 double *background_spectrum_errorbar, *background_integral_spectrum_errorbar;

char name[100],canvastitle[100], workstring1[100],workstring2[100],workstring3[100],workstring4[100];
char psfile[400];

 TGraphErrors *pred_obs;
 TGraphErrors *pred_IC_obs;
 TGraphErrors *pred_gas_obs;
 TGraphErrors *pred_HIT_obs;
 TGraphErrors *pred_H2T_obs;
 TGraphErrors **pred_HIR_obs;
 TGraphErrors **pred_H2R_obs;


 TGraphErrors *pred_total_obs;
 TGraphErrors *pred_IC_gas_obs;
 TGraphErrors *counts_obs, *exposure_obs;

 TGraph       *pred_obs_;
 


 TH1D *pred_th1,*obs_th1,*counts_th1,*exposure_th1;
 TH1D *pred_IC_th1,*pred_gas_th1;

 TH1D *pred_HIT_th1,*pred_H2T_th1;
 TH1D **pred_HIR_th1,**pred_H2R_th1;

 TF1 *fitfunction1,*fitfunction2;


 int npar;
 double *par,*par_error;
 int ipar;
 TMinuit *gMinuit;

 double f,*gin;                   //AWS20040413
 int iflag;                       //AWS20040413
 double chisq_input,chisq_fitted; //AWS20040413
 int data_points_cumulative;      //AWS20040420

 n_Ring=galaxy.HIR.n_zgrid;

 use_EGRET_rebinned_data=1;

  long_min= -179.75;//specific for EGRET data counts.gp1234_30.g001
   lat_min  =-89.75;
  d_long      =0.5;     
  d_lat       =0.5;

  EGRET_counts_n_rgrid=data.EGRET_counts.n_rgrid;
  EGRET_counts_n_zgrid=data.EGRET_counts.n_zgrid;


// rebin EGRET data if required
if(use_EGRET_rebinned_data==1)
{


 longbin=(int)(galplotdef.d_long/0.5 + .00001);
  latbin=(int)(galplotdef.d_lat /0.5 + .00001);

 if (rebin_EGRET_data(longbin,latbin, long_min,lat_min, d_long, d_lat)!=0) return 1;

 EGRET_counts_n_rgrid=EGRET_counts_rebinned.n_rgrid;
 EGRET_counts_n_zgrid=EGRET_counts_rebinned.n_zgrid;


 galplotdef.fit_nrebin_EGRET=1; // since rebinning already done

}

 // first compute number of values to fit



 





         nvalues=0;

	 for (i_lat =0;  i_lat <     EGRET_counts_n_zgrid;  i_lat++ )
         {
          for(i_long=0;  i_long<     EGRET_counts_n_rgrid; i_long++)
         {

 


           l=       long_min+ d_long   *i_long;
           if(l<0.0) l+=360.0;
           b=        lat_min+ d_lat    *i_lat;
	   

           if( 
                 ((l>=galplotdef.long_min1 && l<=galplotdef.long_max1) || (l>=galplotdef.long_min2 && l<=galplotdef.long_max2) )  
	      && ((b>=galplotdef. lat_min1 && b<=galplotdef. lat_max1) || (b>=galplotdef. lat_min2 && b<=galplotdef .lat_max2) )
              )
	     {
	       //   cout<<l<<" "<<b<<" "<<" selected"<<endl;
	       nvalues++;
	     }
           
             }  //  i_long
           }   //  i_lat

         cout<<" nvalues for fitting="<<nvalues<<endl;



//////////////////////////////////////////////////////////////////////////////////

      background_spectrum                  =new double[data.n_E_EGRET];
      background_spectrum_errorbar         =new double[data.n_E_EGRET];
      background_integral_spectrum         =new double[data.n_E_EGRET];
      background_integral_spectrum_errorbar=new double[data.n_E_EGRET];
      for(i_E_EGRET=0; i_E_EGRET<data.n_E_EGRET  ; i_E_EGRET++)
      {
	background_integral_spectrum         [i_E_EGRET]=0;
	background_integral_spectrum_errorbar[i_E_EGRET]=0;
      }
//////////////////////////////////////////////////////////////////////////////////

      chisq_input=0.0; chisq_fitted=0.0; data_points_cumulative=0; //AWS20040420

      n_E_fit_range=12;

      i_E_EGRET1[ 0] = 1;       i_E_EGRET2[ 0] = 9;
      i_E_EGRET1[ 1] = 2;       i_E_EGRET2[ 1] = 9;
      i_E_EGRET1[ 2] = 3;       i_E_EGRET2[ 2] = 9;
      i_E_EGRET1[ 3] = 4;       i_E_EGRET2[ 3] = 9;
      i_E_EGRET1[ 4] = 5;       i_E_EGRET2[ 4] = 9;
      i_E_EGRET1[ 5] = 6;       i_E_EGRET2[ 5] = 9;

      i_E_EGRET1[ 6] = 3;       i_E_EGRET2[ 6] = 8;
      i_E_EGRET1[ 7] = 3;       i_E_EGRET2[ 7] = 7;
      i_E_EGRET1[ 8] = 3;       i_E_EGRET2[ 8] = 6;
      i_E_EGRET1[ 9] = 4;       i_E_EGRET2[ 9] = 8;
      i_E_EGRET1[10] = 4;       i_E_EGRET2[10] = 7;
      i_E_EGRET1[11] = 4;       i_E_EGRET2[11] = 6;
      

      //      if(galplotdef.sources_EGRET==200) // GLAST SC2 data: only one energy range       AWS20070711
      if(galplotdef.data_GLAST==200) // GLAST SC2 data: only one energy range       AWS20080508
      {
       n_E_fit_range=1;
       i_E_EGRET1[ 0] = 0;       i_E_EGRET2[ 0] = 0;
      }


      //       for(i_E_EGRET=0; i_E_EGRET<data.n_E_EGRET                    ; i_E_EGRET++)








     //////////////////////////////////////////////////////////////////////////////////

     for (i_E_fit_range=0;i_E_fit_range<n_E_fit_range;i_E_fit_range++)
     {


	 pred_th1=new TH1D("pred",     "pred"      ,nvalues,0.,0.);
      pred_IC_th1=new TH1D("pred_IC",  "pred_IC"   ,nvalues,0.,0.);
     pred_gas_th1=new TH1D("pred_gas", "pred_gas"  ,nvalues,0.,0.);
  	  obs_th1=new TH1D("obs",      "obs"       ,nvalues,0.,0.);
       counts_th1=new TH1D("counts",   "counts"    ,nvalues,0.,0.);
     exposure_th1=new TH1D("exposure", "exposure"  ,nvalues,0.,0.);

     pred_HIT_th1=new TH1D("pred_HIT", "pred_HIT"  ,nvalues,0.,0.);
     pred_H2T_th1=new TH1D("pred_H2T", "pred_H2T"  ,nvalues,0.,0.);

     pred_HIR_th1=new TH1D*[n_Ring]; // array of pointers to TH1D
     pred_H2R_th1=new TH1D*[n_Ring];

     for (i_Ring=0;i_Ring<n_Ring;i_Ring++)
     {
      pred_HIR_th1[i_Ring]=new TH1D("pred_HIR", "pred_HIR"  ,nvalues,0.,0.);
      pred_H2R_th1[i_Ring]=new TH1D("pred_H2R", "pred_H2R"  ,nvalues,0.,0.);
     }

     pred_HIR=new double[n_Ring];
     pred_H2R=new double[n_Ring];

         ivalues=0;


	 // indices of EGRET maps

	 for (i_lat =0;  i_lat <     EGRET_counts_n_zgrid;  i_lat++ )
         {
          for(i_long=0;  i_long<     EGRET_counts_n_rgrid; i_long++)
	    {
 
           l=       long_min+ d_long   *i_long;
           if(l  <0.0) l+=360.0;
	   // boundaries: to avoid problem around l=0 keep l_2 > l_1 and control indexing range in ii_long loop
           l_1=     long_min+ d_long   *i_long - d_long*0.5; // boundary of rebinned EGRET bin
           l_2=     long_min+ d_long   *i_long + d_long*0.5;
       

           b=        lat_min+ d_lat    *i_lat;
           b_1=      lat_min+ d_lat    *i_lat  - d_lat*0.5;  // boundary of rebinned EGRET bin
           b_2=      lat_min+ d_lat    *i_lat  + d_lat*0.5;

           if(galplotdef.verbose== -503) // selectable debug
           cout<<"l l_1 l_2  b b_1 b_2   "<<l<<" "<<l_1<<" "<<l_2<<" "<<b<<" "<<b_1<<" "<<b_2<<endl;

           if( 
                 ((l>=galplotdef.long_min1 && l<=galplotdef.long_max1) || (l>=galplotdef.long_min2 && l<=galplotdef.long_max2) )  
	      && ((b>=galplotdef. lat_min1 && b<=galplotdef. lat_max1) || (b>=galplotdef. lat_min2 && b<=galplotdef. lat_max2  ) )
              )
	     {

	    // indices of galprop maps 
            ii_long  = (int) ((l  -galaxy.long_min)/galaxy.d_long+.0001);
            ii_long_1= (int) ((l_1-galaxy.long_min)/galaxy.d_long+.0001);  // boundary of rebinned EGRET bin in galprop map
            ii_long_2= (int) ((l_2-galaxy.long_min)/galaxy.d_long+.0001);

            ii_lat   = (int) ((b  -galaxy.lat_min )/galaxy.d_lat +.0001);
            ii_lat_1 = (int) ((b_1-galaxy.lat_min )/galaxy.d_lat +.0001);  // boundary of rebinned EGRET bin in galprop map
            ii_lat_2 = (int) ((b_2-galaxy.lat_min )/galaxy.d_lat +.0001);

	    //	        cout<<"l b  ii_long ii_lat  "<<l<<" "<<b<<" "<<ii_long<<" "<<ii_lat<<endl;

            if(galplotdef.verbose== -503) // selectable debug
            {
	     cout<<"  l_1 l_2  ii_long_1 ii_long_2:  "<<l_1<<" "<<l_2<<" "<<ii_long_1<<" "<<ii_long_2      ;
	     cout<<", b_1 b_2  ii_lat_1  ii_lat_2 :  "<<b_1<<" "<<b_2<<" "<<ii_lat_1 <<" "<<ii_lat_2 <<endl;
            }

	    pred     = 0.;
            pred_IC  = 0.;
            pred_gas = 0.;
            pred_HIT = 0.;
            pred_H2T = 0.;


            counts   = 0.;
            exposure = 0.;
            obs      = 0.;

            for (i_Ring=0;i_Ring<n_Ring;i_Ring++)
            {
	     pred_HIR[i_Ring] = 0.;
             pred_H2R[i_Ring] = 0.;
	    }
                 for(i_E_EGRET=i_E_EGRET1[i_E_fit_range]; i_E_EGRET<=i_E_EGRET2[i_E_fit_range]              ; i_E_EGRET++)

       {


         n_ii=0;

	 for   (ii_long=ii_long_1; ii_long<=ii_long_2;  ii_long++)
	 {
	   // put index in correct range
                                                                ii_long_ =  ii_long;
           if(ii_long_ <0 )                                     ii_long_ += convolved.EGRET_bremss_skymap.n_rgrid;
           if(ii_long_ >=convolved.EGRET_bremss_skymap.n_rgrid) ii_long_ -= convolved.EGRET_bremss_skymap.n_rgrid;

	   //  cout<<"ii_long ii_long_ "<<ii_long<<" "<<ii_long_<<endl;

	  for  (ii_lat = ii_lat_1 ; ii_lat<= ii_lat_2 ; ii_lat ++)
	  {

            if(ii_lat>=0 && ii_lat < convolved.EGRET_bremss_skymap.n_zgrid)
	    {

            ic_total=0;
            for(i_comp=0;i_comp<galaxy.n_ISRF_components;i_comp++)
              ic_total+= convolved.EGRET_IC_iso_skymap[i_comp].d2[ii_long_][ii_lat].s[i_E_EGRET];

          
            pred+=       ic_total;
            pred+=       convolved.EGRET_bremss_skymap   .     d2[ii_long_][ii_lat].s[i_E_EGRET];
            pred+=       convolved.EGRET_pi0_decay_skymap.     d2[ii_long_][ii_lat].s[i_E_EGRET]; 

            pred_IC +=   ic_total;
            pred_gas+=   convolved.EGRET_bremss_skymap   .     d2[ii_long_][ii_lat].s[i_E_EGRET];
            pred_gas+=   convolved.EGRET_pi0_decay_skymap.     d2[ii_long_][ii_lat].s[i_E_EGRET]; 

            pred_HIT+=   convolved.EGRET_HIT_skymap      .     d2[ii_long_][ii_lat].s[i_E_EGRET];
            pred_H2T+=   convolved.EGRET_H2T_skymap      .     d2[ii_long_][ii_lat].s[i_E_EGRET];


            if(galplotdef.verbose==-506)// selectable debug AWS20070712
	      {
		cout<<"fit_EGRET_Xco: A"
		    <<" convolved.EGRET_bremss_skymap    "<<convolved.EGRET_bremss_skymap      .     d2[ii_long_][ii_lat].s[i_E_EGRET]
		    <<" convolved.EGRET_pi0_decay_skymap "<<convolved.EGRET_pi0_decay_skymap   .     d2[ii_long_][ii_lat].s[i_E_EGRET]
		    <<" convolved.EGRET_HIT_skymap       "<<convolved.EGRET_HIT_skymap         .     d2[ii_long_][ii_lat].s[i_E_EGRET]
		    <<" convolved.EGRET_H2T_decay_skymap "<<convolved.EGRET_H2T_skymap         .     d2[ii_long_][ii_lat].s[i_E_EGRET]
                    <<" pred_gas "<<pred_gas<<" pred_HIT "<<pred_HIT<< " pred_H2T "<<pred_H2T;
	        if(pred_H2T>0. ) cout<<" * ";
		cout<<endl;

		cout<<"fit_EGRET_Xco: B"
		    <<" convolved.EGRET_bremss_skymap+ convolved.EGRET_pi0_decay_skymap   "
                     <<convolved.EGRET_bremss_skymap      .     d2[ii_long_][ii_lat].s[i_E_EGRET]
		      +convolved.EGRET_pi0_decay_skymap   .     d2[ii_long_][ii_lat].s[i_E_EGRET]
		    <<" convolved.EGRET_HIT_skymap+convolved.EGRET_H2T_decay_skymap "
		    <<  convolved.EGRET_HIT_skymap         .     d2[ii_long_][ii_lat].s[i_E_EGRET]
	               +convolved.EGRET_H2T_skymap         .     d2[ii_long_][ii_lat].s[i_E_EGRET];
                cout<<endl;
	      }


            for (i_Ring=0;i_Ring<n_Ring;i_Ring++)
            {
             pred_HIR[i_Ring]+=   convolved.EGRET_HIR_skymap.  d3[ii_long_][ii_lat][i_Ring].s[i_E_EGRET];
             pred_H2R[i_Ring]+=   convolved.EGRET_H2R_skymap.  d3[ii_long_][ii_lat][i_Ring].s[i_E_EGRET];
	    }

            n_ii++;

	   }//if

          } //ii_lat
	 }// ii_long
       
	 


            if(use_EGRET_rebinned_data==0)
	    {
            counts  +=       data.EGRET_counts.  d2[i_long][i_lat].s[i_E_EGRET];

             obs    +=       data.EGRET_counts.  d2[i_long][i_lat].s[i_E_EGRET]
                            /data.EGRET_exposure.d2[i_long][i_lat].s[i_E_EGRET]; // exposure of EGRET map bin:includes solid angle

            exposure+=       data.EGRET_exposure.d2[i_long][i_lat].s[i_E_EGRET]; 

	    }

            if(use_EGRET_rebinned_data==1)
	    {
            counts  +=            EGRET_counts_rebinned.  d2[i_long][i_lat].s[i_E_EGRET];

             obs    +=            EGRET_counts_rebinned.  d2[i_long][i_lat].s[i_E_EGRET]
                            /     EGRET_exposure_rebinned.d2[i_long][i_lat].s[i_E_EGRET]; // exposure of EGRET map bin:includes solid angle

            exposure+=            EGRET_exposure_rebinned.d2[i_long][i_lat].s[i_E_EGRET]; 

	    }


	}//i_E_EGRET 



	 if(n_ii == 0){cout << "error in fit_EGRET_X_co: n_ii=0"<<endl; return -1;}

	 //cout<<"before/n_ii pred pred_IC pred_gas pred_HIT pred_H2T "<<n_ii<<" "<<pred<<" "<<pred_IC<<" "<<pred_gas<<" "<<pred_HIT<<" "<<pred_H2T<<endl;

	 pred     /=n_ii;
	 pred_IC  /=n_ii;
	 pred_gas /=n_ii;
	 pred_HIT /=n_ii;
	 pred_H2T /=n_ii;

         for (i_Ring=0;i_Ring<n_Ring;i_Ring++)
         {
          pred_HIR[i_Ring] /=n_ii;
          pred_H2R[i_Ring] /=n_ii;
         }
	 
	 if(galplotdef.verbose==-504)         //AWS20070172
	   cout<<"fit_EGRET_Xco: after /n_ii:l b i_long i_lat i_E_EGRET1,2 n_ii pred pred_IC pred_gas pred_HIT pred_H2T "
               <<l<<" "<<b<<" "
	       <<i_long<<" "<<i_lat<<" "<<i_E_EGRET1[i_E_fit_range]<<"-"<<i_E_EGRET2[i_E_fit_range] <<" "
               <<n_ii<<" "<<pred<<" "<<pred_IC<<" "<<pred_gas<<" "<<pred_HIT<<" "<<pred_H2T<<endl;

	    exposure/= (i_E_EGRET2[i_E_fit_range] - i_E_EGRET1[i_E_fit_range] + 1 ); // to compensate multiple energies in sum

            pred_th1    ->SetBinContent(ivalues,pred    );
            pred_IC_th1 ->SetBinContent(ivalues,pred_IC );
            pred_gas_th1->SetBinContent(ivalues,pred_gas);
            pred_HIT_th1->SetBinContent(ivalues,pred_HIT);
            pred_H2T_th1->SetBinContent(ivalues,pred_H2T);

           for (i_Ring=0;i_Ring<n_Ring;i_Ring++)
           {
	    pred_HIR_th1[i_Ring]->SetBinContent(ivalues,pred_HIR[i_Ring]);
	    pred_H2R_th1[i_Ring]->SetBinContent(ivalues,pred_H2R[i_Ring]);
	   }

             obs_th1->SetBinContent(ivalues, obs     );
          counts_th1->SetBinContent(ivalues, counts  );
        exposure_th1->SetBinContent(ivalues, exposure);


	 if(galplotdef.verbose==-505)         //AWS20070172
	       cout<<"l b exposure  counts pred obs "
	  	  <<l<<" "<<b<<" "<<data.EGRET_exposure.d2[i_long][i_lat].s[i_E_EGRET]<<" "<<counts<<" "<<" "<<pred<<" "<<obs<<endl;
           


 
 


            ivalues++;

	  }//if


            }  //  i_long
           }   //  i_lat

   


      

       ////////////////////////////////////////////////////////////////////////////////

	     pred_th1->Rebin(galplotdef.fit_nrebin_EGRET);
          pred_IC_th1->Rebin(galplotdef.fit_nrebin_EGRET);
	 pred_gas_th1->Rebin(galplotdef.fit_nrebin_EGRET);
	 pred_HIT_th1->Rebin(galplotdef.fit_nrebin_EGRET);
	 pred_H2T_th1->Rebin(galplotdef.fit_nrebin_EGRET);

         for (i_Ring=0;i_Ring<n_Ring;i_Ring++)
         {
          pred_HIR_th1[i_Ring]->Rebin(galplotdef.fit_nrebin_EGRET);
          pred_H2R_th1[i_Ring]->Rebin(galplotdef.fit_nrebin_EGRET);
         }

	      obs_th1->Rebin(galplotdef.fit_nrebin_EGRET);
	   counts_th1->Rebin(galplotdef.fit_nrebin_EGRET);
	 exposure_th1->Rebin(galplotdef.fit_nrebin_EGRET);

	   n_values_rebin_= pred_th1->GetNbinsX(); // before excluding low counts

	     // rebinning adds the bins so must rescale pred and obs (but counts and exposure remain summed)
	     pred_th1->Scale(1./galplotdef.fit_nrebin_EGRET);
	  pred_IC_th1->Scale(1./galplotdef.fit_nrebin_EGRET);
         pred_gas_th1->Scale(1./galplotdef.fit_nrebin_EGRET);
         pred_HIT_th1->Scale(1./galplotdef.fit_nrebin_EGRET);
         pred_H2T_th1->Scale(1./galplotdef.fit_nrebin_EGRET);

         for (i_Ring=0;i_Ring<n_Ring;i_Ring++)
         {
          pred_HIR_th1[i_Ring]->Scale(1./galplotdef.fit_nrebin_EGRET);
          pred_H2R_th1[i_Ring]->Scale(1./galplotdef.fit_nrebin_EGRET);
         }


              obs_th1->Scale(1./galplotdef.fit_nrebin_EGRET);

	     cout<<"n_rebin "<<galplotdef.fit_nrebin_EGRET<<" number of values after rebinning="<<n_values_rebin_<<endl;




	     iivalues=0;
	     for (ivalues=0;ivalues<n_values_rebin_;ivalues++)
	     {
                counts=counts_th1 ->GetBinContent(ivalues);
		if(counts<float(galplotdef.fit_counts_min_EGRET))
                {
		  //cout<<"remove point "<<ivalues<< " with counts="<<counts<<endl;
                
                }
                else iivalues++;
	     }

	     n_values_rebin=iivalues; // after excluding low counts

	   counts_obs = new TGraphErrors(n_values_rebin);
	 exposure_obs = new TGraphErrors(n_values_rebin);

	     pred_obs = new TGraphErrors(n_values_rebin);
	  pred_IC_obs = new TGraphErrors(n_values_rebin);
         pred_gas_obs = new TGraphErrors(n_values_rebin);
         pred_HIT_obs = new TGraphErrors(n_values_rebin);
         pred_H2T_obs = new TGraphErrors(n_values_rebin);



         pred_HIR_obs=new TGraphErrors*[n_Ring]; // array of pointers to TGraphErrors
         pred_H2R_obs=new TGraphErrors*[n_Ring]; 

         for (i_Ring=0;i_Ring<n_Ring;i_Ring++)
         {
          pred_HIR_obs[i_Ring] = new TGraphErrors(n_values_rebin);
          pred_H2R_obs[i_Ring] = new TGraphErrors(n_values_rebin);
	 }


	     pred_obs_= new TGraph      (n_values_rebin);



	     cout<<"number of values after removing low counts:"<<pred_obs->GetN()<<endl;


             maximum_intensity=0.;
             iivalues=0;
	     for (ivalues=0;ivalues<n_values_rebin_;ivalues++)
	     {

              counts=counts_th1 ->GetBinContent(ivalues);

		if(counts<galplotdef.fit_counts_min_EGRET)
                {
		  cout<<"remove point "<<ivalues<< " with counts="<<counts<<endl;
                }
                  else
		    {
	          pred_obs    ->SetPoint(iivalues,pred_th1    ->GetBinContent(ivalues),obs_th1->GetBinContent(ivalues) );
 	        counts_obs    ->SetPoint(iivalues,counts_th1  ->GetBinContent(ivalues),obs_th1->GetBinContent(ivalues) );
	      exposure_obs    ->SetPoint(iivalues,exposure_th1->GetBinContent(ivalues),obs_th1->GetBinContent(ivalues) );       	

	          pred_IC_obs ->SetPoint(iivalues,pred_IC_th1 ->GetBinContent(ivalues),obs_th1->GetBinContent(ivalues) );
	          pred_gas_obs->SetPoint(iivalues,pred_gas_th1->GetBinContent(ivalues),obs_th1->GetBinContent(ivalues) );
	          pred_HIT_obs->SetPoint(iivalues,pred_HIT_th1->GetBinContent(ivalues),obs_th1->GetBinContent(ivalues) );
	          pred_H2T_obs->SetPoint(iivalues,pred_H2T_th1->GetBinContent(ivalues),obs_th1->GetBinContent(ivalues) );

                  for (i_Ring=0;i_Ring<n_Ring;i_Ring++)
                  {
                   pred_HIR_obs[i_Ring]->SetPoint(iivalues,pred_HIR_th1[i_Ring]->GetBinContent(ivalues),obs_th1->GetBinContent(ivalues) );
                   pred_H2R_obs[i_Ring]->SetPoint(iivalues,pred_H2R_th1[i_Ring]->GetBinContent(ivalues),obs_th1->GetBinContent(ivalues) );
	          }

	          error_estimate= obs_th1->GetBinContent(ivalues)/sqrt(counts);

		  // add systematic error if required                           AWS20040420
		  if(options==2)error_estimate=sqrt(  pow(error_estimate,2)
						    + pow(obs_th1->GetBinContent(ivalues)*galplotdef.error_bar_EGRET, 2) );

	          pred_obs->SetPointError(iivalues,0.0, error_estimate );

                  if (pred_th1->GetBinContent(ivalues)>maximum_intensity)
                                                       maximum_intensity=pred_th1->GetBinContent(ivalues);
                  if ( obs_th1->GetBinContent(ivalues)>maximum_intensity)
                                                       maximum_intensity= obs_th1->GetBinContent(ivalues);





		  if(kFALSE)
		{
	            cout<<data.E_EGRET[i_E_EGRET]<<"-"<<data.E_EGRET[i_E_EGRET+1]<<" MeV " 
	      	  <<"pred="<<pred_th1->GetBinContent(ivalues)<<" obs="<<obs_th1->GetBinContent(ivalues)
                  <<" counts= "<<counts<<" error estimate="<<error_estimate<<endl;
		}


		  iivalues++;

		    }//else
	     }//ivalues




  cout<<endl<<"==================================== "<<data.E_EGRET[i_E_EGRET1[i_E_fit_range]]<<"-"<<data.E_EGRET[i_E_EGRET2[i_E_fit_range] +1]<<" MeV"<<endl;




  cout<<"           ========fit to total using TGraph    ======================================";
  cout<<" "<<data.E_EGRET[i_E_EGRET1[i_E_fit_range]]<<"-"<<data.E_EGRET[i_E_EGRET2[i_E_fit_range] +1]<<" MeV"<<endl;
  //  pred_obs->Fit("pol1"    );

  fitfunction1  =new TF1("f1","[0] + x");     // only background free, parameter 0
  fitfunction2  =new TF1("f2","[0] + [1]*x"); // scaling and      background free, parameters 0, 1

  if(galplotdef.fit_function_EGRET==1 ||galplotdef.fit_function_EGRET==3)
   pred_obs->Fit("f1"      );
  if(galplotdef.fit_function_EGRET==2 ||galplotdef.fit_function_EGRET==3)
   pred_obs->Fit("f2"      );



  // ----------------------------------------------------   fits using Minuit

  x     =new double[n_values_rebin];
  y     =new double[n_values_rebin];
  z     =new double[n_values_rebin];
  errorz=new double[n_values_rebin];

  parmin=0.0; parmax=100.;     //AWS20070711

  double dummy; // since pred_IC_obs, pred_gas_obs  do not have second value ("obs") set

  Double_t arglist[10];
  Int_t ierflg = 0;
  static Double_t vstart[4] = {0.1e-5, 0.1 ,  0.1, 0.1 };
  static Double_t step[4] =   {1.e-8 , 0.01 , 0.01,0.01};
  Double_t amin,edm,errdef;
  Int_t nvpar,nparx,icstat;


  // fit to total        

  cout<<"            =======fit to total      using TMinuit======================================";
  cout<<" "<<data.E_EGRET[i_E_EGRET1[i_E_fit_range]]<<"-"<<data.E_EGRET[i_E_EGRET2[i_E_fit_range] +1]<<" MeV"<<endl;


  for (ivalues=0;ivalues<n_values_rebin;ivalues++)
    {
      pred_obs    ->GetPoint  (ivalues,x[ivalues],dummy);
   
      pred_obs    ->GetPoint  (ivalues, dummy,    z[ivalues]  );
     errorz[ivalues] = 
      pred_obs    ->GetErrorY (ivalues );

     // cout<<" x y z errorz :"<<x[ivalues]<<" "<<y[ivalues]<<" "<<z[ivalues]<<" "<<errorz[ivalues]<<endl;
    }

  n_values_fcn=n_values_rebin;
  npar=2;
  gMinuit = new TMinuit(npar);  //initialize TMinuit with a maximum of npar params
  gMinuit->SetFCN(fcn2);



   arglist[0] = 1;
   gMinuit->mnexcm("SET ERR", arglist ,1,ierflg);

// Set starting values and step sizes for parameters

   gMinuit->mnparm(0, "background2", vstart[0], step[0], 0,0,ierflg);
   gMinuit->mnparm(1, "total",       vstart[1], step[1], 0,0,ierflg);
 
  

// Now ready for minimization step
   arglist[0] = 500;  //maxcalls
   arglist[1] = 0.1;  //tolerance
   gMinuit->mnexcm("MIGRAD", arglist ,2,ierflg);

// Print results

//   gMinuit->mnstat(amin,edm,errdef,nvpar,nparx,icstat);
//   gMinuit->mnprin(3,amin);


// get fitted parameters
   par      =new double[npar];
   par_error=new double[npar];
   for(ipar=0;ipar<npar;ipar++)gMinuit->GetParameter(ipar,par[ipar],par_error[ipar]);
   double background2=par[0]; // for pred/obs line in plot

   background_spectrum         [i_E_EGRET]           =    par      [0];
   background_spectrum_errorbar[i_E_EGRET]           =    par_error[0];



// plot pred/obs for fitted parameters, but with background=0
   par[0]=0;
   pred_total_obs =new TGraphErrors(n_values_rebin);
   for (ivalues=0;ivalues<n_values_rebin;ivalues++)
    {
      pred_obs       ->GetPoint     (ivalues,dummy,obs);
      pred_total_obs ->SetPoint     (ivalues,func2(x[ivalues],           par),obs);
      pred_total_obs ->SetPointError(ivalues,0.0, pred_obs->GetErrorY(ivalues));
    }
  

 
  // compute chisq for input model with fitted background//AWS20040413

  par[0]=   background_spectrum         [i_E_EGRET]   ;
  gin=new double[npar];
  fcn3(npar, gin, f, par,  iflag);
  chisq_fitted+=f;
  data_points_cumulative+=n_values_fcn; //AWS20040420
  cout<<data.E_EGRET[i_E_EGRET]<<"-"<<data.E_EGRET[i_E_EGRET+1]<<" MeV: chi^2 for fitted model with par[1]="<<par[1]
      <<" : "<<f<<" #data points="<<n_values_fcn
      <<"    cumulative: "<<chisq_fitted<<" #data points="<<data_points_cumulative<<endl;

 
  par[1]=1.0;
  gin=new double[npar];
  fcn3(npar, gin, f, par,  iflag);
  chisq_input+=f;
  cout<<data.E_EGRET[i_E_EGRET]<<"-"<<data.E_EGRET[i_E_EGRET+1]<<" MeV: chi^2 for input  model with par[1]="<<par[1]
      <<"        : "<<f<<" #data points="<<n_values_fcn
      <<"    cumulative: "<<chisq_input <<" #data points="<<data_points_cumulative<<endl;



  // fit to IC and gas 


 cout<<"            =======fit to IC and gas using TMinuit======================================";
 cout<<" "<<data.E_EGRET[i_E_EGRET1[i_E_fit_range]]<<"-"<<data.E_EGRET[i_E_EGRET2[i_E_fit_range] +1]<<" MeV"<<endl;

  for (ivalues=0;ivalues<n_values_rebin;ivalues++)
    {
      pred_IC_obs ->GetPoint  (ivalues,x[ivalues],dummy);
      pred_gas_obs->GetPoint  (ivalues,y[ivalues],dummy);
    

      pred_obs    ->GetPoint  (ivalues, dummy,    z[ivalues]  );
     errorz[ivalues] = 
      pred_obs    ->GetErrorY (ivalues );

     //  cout<<" x y z errorz :"<<x[ivalues]<<" "<<y[ivalues]<<" "<<z[ivalues]<<" "<<errorz[ivalues]<<endl;
    }




  n_values_fcn=n_values_rebin;
  npar=3;
  gMinuit = new TMinuit(npar);  //initialize TMinuit with a maximum of npar params
  gMinuit->SetFCN(fcn3);



   arglist[0] = 1;
   gMinuit->mnexcm("SET ERR", arglist ,1,ierflg);

// Set starting values and step sizes for parameters

   gMinuit->mnparm(0, "background3", vstart[0], step[0], 0,0,ierflg);
   gMinuit->mnparm(1, "IC",          vstart[1], step[1], 0,0,ierflg);
   gMinuit->mnparm(2, "gas",         vstart[2], step[2], 0,0,ierflg);
 

// Now ready for minimization step
   arglist[0] = 500;  //maxcalls
   arglist[1] = 0.1;  //tolerance
   gMinuit->mnexcm("MIGRAD", arglist ,2,ierflg);

// Print results (superfluous since mnexcm prints them in same format)

//   gMinuit->mnstat(amin,edm,errdef,nvpar,nparx,icstat);
//   gMinuit->mnprin(3,amin);


// get fitted parameters
   par      =new double[npar];
   par_error=new double[npar];
   for(ipar=0;ipar<npar;ipar++)gMinuit->GetParameter(ipar,par[ipar],par_error[ipar]);
   double background3=par[0]; // for pred/obs line in plot

// plot pred/obs for fitted parameters, but with background=0
   par[0]=0;
   pred_IC_gas_obs=new TGraphErrors(n_values_rebin);
   for (ivalues=0;ivalues<n_values_rebin;ivalues++)
    {
      pred_obs       ->GetPoint     (ivalues,dummy,obs);
      pred_IC_gas_obs->SetPoint     (ivalues,func3(x[ivalues],y[ivalues],par),obs);
      pred_IC_gas_obs->SetPointError(ivalues,0.0, pred_obs->GetErrorY(ivalues));
    }
  

  cout<<"=========================================================================="<<endl;

 ///////////////////////////////////////////////////////////////////////////////////////////////
 cout<<endl;
 cout<<"            =======fit to IC and HIT using TMinuit======================================";
 cout<<" "<<data.E_EGRET[i_E_EGRET1[i_E_fit_range]]<<"-"<<data.E_EGRET[i_E_EGRET2[i_E_fit_range] +1]<<" MeV"<<endl;

  for (ivalues=0;ivalues<n_values_rebin;ivalues++)
    {
      pred_IC_obs ->GetPoint  (ivalues,x[ivalues],dummy);
      pred_HIT_obs->GetPoint  (ivalues,y[ivalues],dummy);
     

      pred_obs    ->GetPoint  (ivalues, dummy,    z[ivalues]  );
     errorz[ivalues] = 
      pred_obs    ->GetErrorY (ivalues );

     // cout<<" x y z errorz :"<<x[ivalues]<<" "<<y[ivalues]<<" "<<z[ivalues]<<" "<<errorz[ivalues]<<endl;
    }

  n_values_fcn=n_values_rebin;
  npar=3;
  gMinuit = new TMinuit(npar);  //initialize TMinuit with a maximum of npar params
  gMinuit->SetFCN(fcn3);



   arglist[0] = 1;
   gMinuit->mnexcm("SET ERR", arglist ,1,ierflg);

// Set starting values and step sizes for parameters

   gMinuit->mnparm(0, "background3a", vstart[0], step[0], 0,0,ierflg);
   gMinuit->mnparm(1, "ICa",          vstart[1], step[1], 0,0,ierflg);
   gMinuit->mnparm(2, "HITa",         vstart[2], step[2], 0,0,ierflg);

// Now ready for minimization step
   arglist[0] = 500;  //maxcalls
   arglist[1] = 0.1;  //tolerance
   gMinuit->mnexcm("MIGRAD", arglist ,2,ierflg);

// Print results

//   gMinuit->mnstat(amin,edm,errdef,nvpar,nparx,icstat);
//   gMinuit->mnprin(3,amin);


// get fitted parameters
   par      =new double[npar];
   par_error=new double[npar];
   for(ipar=0;ipar<npar;ipar++)gMinuit->GetParameter(ipar,par[ipar],par_error[ipar]);
   background3=par[0]; // for pred/obs line in plot

// plot pred/obs for fitted parameters, but with background=0
   par[0]=0;
   pred_IC_gas_obs=new TGraphErrors(n_values_rebin);
   for (ivalues=0;ivalues<n_values_rebin;ivalues++)
    {
      pred_obs       ->GetPoint     (ivalues,dummy,obs);
      pred_IC_gas_obs->SetPoint     (ivalues,func3(x[ivalues],y[ivalues],par),obs);
      pred_IC_gas_obs->SetPointError(ivalues,0.0, pred_obs->GetErrorY(ivalues));
    }
  

  cout<<"=========================================================================="<<endl;

///////////////////////////////////////////////////////////////////////////////////////////////
 cout<<endl;
 cout<<"            =======fit to IC and H2T using TMinuit======================================";
 cout<<" "<<data.E_EGRET[i_E_EGRET1[i_E_fit_range]]<<"-"<<data.E_EGRET[i_E_EGRET2[i_E_fit_range] +1]<<" MeV"<<endl;

  for (ivalues=0;ivalues<n_values_rebin;ivalues++)
    {
      pred_IC_obs ->GetPoint  (ivalues,x[ivalues],dummy);
      pred_H2T_obs->GetPoint  (ivalues,y[ivalues],dummy);
     

      pred_obs    ->GetPoint  (ivalues, dummy,    z[ivalues]  );
     errorz[ivalues] = 
      pred_obs    ->GetErrorY (ivalues );

     // cout<<" x y z errorz :"<<x[ivalues]<<" "<<y[ivalues]<<" "<<z[ivalues]<<" "<<errorz[ivalues]<<endl;
    }

  n_values_fcn=n_values_rebin;
  npar=3;
  gMinuit = new TMinuit(npar);  //initialize TMinuit with a maximum of npar params
  gMinuit->SetFCN(fcn3);



   arglist[0] = 1;
   gMinuit->mnexcm("SET ERR", arglist ,1,ierflg);

// Set starting values and step sizes for parameters

   gMinuit->mnparm(0, "background3b", vstart[0], step[0], 0,0,ierflg);
   gMinuit->mnparm(1, "ICb",          vstart[1], step[1], 0,0,ierflg);
   gMinuit->mnparm(2, "H2Tb",         vstart[2], step[2], 0,0,ierflg);

// Now ready for minimization step
   arglist[0] = 500;  //maxcalls
   arglist[1] = 0.1;  //tolerance
   gMinuit->mnexcm("MIGRAD", arglist ,2,ierflg);

// Print results

//   gMinuit->mnstat(amin,edm,errdef,nvpar,nparx,icstat);
//   gMinuit->mnprin(3,amin);


// get fitted parameters
   par      =new double[npar];
   par_error=new double[npar];
   for(ipar=0;ipar<npar;ipar++)gMinuit->GetParameter(ipar,par[ipar],par_error[ipar]);
   background3=par[0]; // for pred/obs line in plot

// plot pred/obs for fitted parameters, but with background=0
   par[0]=0;
   pred_IC_gas_obs=new TGraphErrors(n_values_rebin);
   for (ivalues=0;ivalues<n_values_rebin;ivalues++)
    {
      pred_obs       ->GetPoint     (ivalues,dummy,obs);
      pred_IC_gas_obs->SetPoint     (ivalues,func3(x[ivalues],y[ivalues],par),obs);
      pred_IC_gas_obs->SetPointError(ivalues,0.0, pred_obs->GetErrorY(ivalues));
    }
  

  cout<<"=========================================================================="<<endl;


///////////////////////////////////////////////////////////////////////////////////////////////
 cout<<endl;
 cout<<"            =======fit to IC+ HIT+ H2T using TMinuit======================================";
 cout<<" "<<data.E_EGRET[i_E_EGRET1[i_E_fit_range]]<<"-"<<data.E_EGRET[i_E_EGRET2[i_E_fit_range] +1]<<" MeV"<<endl;

  x1    =new double[n_values_rebin];
  x2    =new double[n_values_rebin];
  x3    =new double[n_values_rebin];

  for (ivalues=0;ivalues<n_values_rebin;ivalues++)
    {
      pred_IC_obs ->GetPoint  (ivalues,x1[ivalues],dummy); // sets x1
      pred_HIT_obs->GetPoint  (ivalues,x2[ivalues],dummy); 
      pred_H2T_obs->GetPoint  (ivalues,x3[ivalues],dummy);

      pred_obs    ->GetPoint  (ivalues, dummy,    z[ivalues]  ); 
     errorz[ivalues] = 
      pred_obs    ->GetErrorY (ivalues );

     // cout<<" x y z errorz :"<<x[ivalues]<<" "<<y[ivalues]<<" "<<z[ivalues]<<" "<<errorz[ivalues]<<endl;
    }

  n_values_fcn=n_values_rebin;
  npar=4;
  gMinuit = new TMinuit(npar);  //initialize TMinuit with a maximum of npar params
  gMinuit->SetFCN(fcn4);
 


   arglist[0] = 1;
   gMinuit->mnexcm("SET ERR", arglist ,1,ierflg);

// Set starting values and step sizes for parameters

   gMinuit->mnparm(0, "background4",  vstart[0], step[0], parmin,parmax,ierflg);//AWS20070711 upper bound set since 0,0 means no bounds
   gMinuit->mnparm(1, "IC4",          vstart[1], step[1], parmin,parmax,ierflg);//AWS20070711 upper bound set since 0,0 means no bounds
   gMinuit->mnparm(2, "HIT4",         vstart[2], step[2], parmin,parmax,ierflg);//AWS20070711 upper bound set since 0,0 means no bounds
   gMinuit->mnparm(3, "H2T4",         vstart[2], step[2], parmin,parmax,ierflg);//AWS20070711 upper bound set since 0,0 means no bounds

// Now ready for minimization step
   arglist[0] = 500;  //maxcalls
   arglist[1] = 0.1;  //tolerance
   gMinuit->mnexcm("MIGRAD", arglist ,2,ierflg);

// Print results

//   gMinuit->mnstat(amin,edm,errdef,nvpar,nparx,icstat);
//   gMinuit->mnprin(3,amin);


// get fitted parameters
   par      =new double[npar];
   par_error=new double[npar];
   for(ipar=0;ipar<npar;ipar++)gMinuit->GetParameter(ipar,par[ipar],par_error[ipar]);
   background3=par[0]; // for pred/obs line in plot

// plot pred/obs for fitted parameters, but with background=0
   par[0]=0;
   pred_IC_gas_obs=new TGraphErrors(n_values_rebin);
   for (ivalues=0;ivalues<n_values_rebin;ivalues++)
    {
      pred_obs       ->GetPoint     (ivalues,dummy,obs);
      pred_IC_gas_obs->SetPoint     (ivalues,func4(x1[ivalues],x2[ivalues],x3[ivalues],par),obs);
      pred_IC_gas_obs->SetPointError(ivalues,0.0, pred_obs->GetErrorY(ivalues));
    }
  

  cout<<"=========================================================================="<<endl;



///////////////////////////////////////////////////////////////////////////////////////////////
 cout<<endl;
 cout<<"            =======fit to IC+ HIT+ H2R Rings 1-4 using TMinuit======================================";
 cout<<" "<<data.E_EGRET[i_E_EGRET1[i_E_fit_range]]<<"-"<<data.E_EGRET[i_E_EGRET2[i_E_fit_range] +1]<<" MeV"<<endl;
  x1    =new double[n_values_rebin];
  x2    =new double[n_values_rebin];
  x3    =new double[n_values_rebin];
  x4    =new double[n_values_rebin];
  x5    =new double[n_values_rebin];
  x6    =new double[n_values_rebin];
  x7    =new double[n_values_rebin];
  x8    =new double[n_values_rebin];
  x9    =new double[n_values_rebin];
  x10   =new double[n_values_rebin];
  x11   =new double[n_values_rebin];
  x12   =new double[n_values_rebin];
  x13   =new double[n_values_rebin];
  x14   =new double[n_values_rebin];
  x15   =new double[n_values_rebin];

zcounts   =new double[n_values_rebin];
zexposure =new double[n_values_rebin];

  for (ivalues=0;ivalues<n_values_rebin;ivalues++)
    {
      pred_IC_obs                   ->GetPoint  (ivalues,x1[ivalues],dummy); // sets x1
      pred_HIT_obs                  ->GetPoint  (ivalues,x2[ivalues],dummy); 

      i_Ring=0; pred_H2R_obs[i_Ring]->GetPoint  (ivalues,x3[ivalues],dummy);
      i_Ring=1; pred_H2R_obs[i_Ring]->GetPoint  (ivalues,x4[ivalues],dummy);
      i_Ring=2; pred_H2R_obs[i_Ring]->GetPoint  (ivalues,x5[ivalues],dummy);
      i_Ring=3; pred_H2R_obs[i_Ring]->GetPoint  (ivalues,x6[ivalues],dummy);
      i_Ring=4; pred_H2R_obs[i_Ring]->GetPoint  (ivalues,x7[ivalues],dummy);

      pred_obs    ->GetPoint  (ivalues, dummy,    z[ivalues]  ); 
     errorz[ivalues] = 
      pred_obs    ->GetErrorY (ivalues );

    counts_obs    ->GetPoint  (ivalues,zcounts  [ivalues],dummy  ); 
  exposure_obs    ->GetPoint  (ivalues,zexposure[ivalues],dummy  ); 


   if(galplotdef.verbose== -501) // selectable debug
       cout<<" x1-6 z errorz zcounts zexposure :"<<x1[ivalues]<<" "<<x2[ivalues]<<" "<<x3[ivalues]<<" "<<x4[ivalues]
         	         <<" "<<x5[ivalues]<<" "<<x6[ivalues]<<" "<<z[ivalues]<<" "<<errorz[ivalues]
                         <<" "<<zcounts[ivalues]<<" "<<zexposure[ivalues]<<endl;
    }

  n_values_fcn=n_values_rebin;
  npar=7;
  gMinuit = new TMinuit(npar);  //initialize TMinuit with a maximum of npar params
  gMinuit->SetFCN(fcn7);
  //gMinuit->SetFCN(fcn7poisson);

   arglist[0] = 1;
   gMinuit->mnexcm("SET ERR", arglist ,1,ierflg);

// Set starting values and step sizes for parameters
//                                                      lower,upper bounds
   gMinuit->mnparm(0, "background5",  vstart[0], step[0], 0.,100.,ierflg);
   gMinuit->mnparm(1, "IC5",          vstart[1], step[1], 0.,100.,ierflg);
   gMinuit->mnparm(2, "HIT5",         vstart[2], step[2], 0.,100.,ierflg);
   gMinuit->mnparm(3, "H2R5_ring1",   vstart[2], step[2], 0.,100.,ierflg);
   gMinuit->mnparm(4, "H2R5_ring2",   vstart[2], step[2], 0.,100.,ierflg);
   gMinuit->mnparm(5, "H2R5_ring3",   vstart[2], step[2], 0.,100.,ierflg);
   gMinuit->mnparm(6, "H2R5_ring4",   vstart[2], step[2], 0.,100.,ierflg);

// Now ready for minimization step
   arglist[0] = 5000;  //maxcalls
   arglist[1] = 0.1;  //tolerance
   gMinuit->mnexcm("MIGRAD", arglist ,2,ierflg);

// Print results

//   gMinuit->mnstat(amin,edm,errdef,nvpar,nparx,icstat);
//   gMinuit->mnprin(3,amin);

   ////////////////////////////////////////////////////////////////////////////////////////////////

  cout<<endl;
  cout<<"            =======fit to IC+ HIT+ H2R Rings 1-5 using TMinuit======================================";
  cout<<" "<<data.E_EGRET[i_E_EGRET1[i_E_fit_range]]<<"-"<<data.E_EGRET[i_E_EGRET2[i_E_fit_range] +1]<<" MeV"<<endl;

  npar=8;
  gMinuit = new TMinuit(npar);  //initialize TMinuit with a maximum of npar params
  gMinuit->SetFCN(fcn8);
  //gMinuit->SetFCN(fcn8poisson);

   arglist[0] = 1;
   gMinuit->mnexcm("SET ERR", arglist ,1,ierflg);

// Set starting values and step sizes for parameters
//                                                      lower,upper bounds
   gMinuit->mnparm(0, "background5",  vstart[0], step[0], 0.,100.,ierflg);
   gMinuit->mnparm(1, "IC5",          vstart[1], step[1], 0.,100.,ierflg);
   gMinuit->mnparm(2, "HIT5",         vstart[2], step[2], 0.,100.,ierflg);
   gMinuit->mnparm(3, "H2R5_ring1",   vstart[2], step[2], 0.,100.,ierflg);
   gMinuit->mnparm(4, "H2R5_ring2",   vstart[2], step[2], 0.,100.,ierflg);
   gMinuit->mnparm(5, "H2R5_ring3",   vstart[2], step[2], 0.,100.,ierflg);
   gMinuit->mnparm(6, "H2R5_ring4",   vstart[2], step[2], 0.,100.,ierflg);
   gMinuit->mnparm(7, "H2R5_ring5",   vstart[2], step[2], 0.,100.,ierflg);

// Now ready for minimization step
   arglist[0] = 5000;  //maxcalls
   arglist[1] = 0.1;  //tolerance
   gMinuit->mnexcm("MIGRAD", arglist ,2,ierflg);

// Print results

//   gMinuit->mnstat(amin,edm,errdef,nvpar,nparx,icstat);
//   gMinuit->mnprin(3,amin);

  ////////////////////////////////////////////////////////////////////////////////////////////////

  cout<<endl;
  cout<<"            =======fit to IC+ HIT+ H2R Rings 1-3 4 5  chisq using TMinuit======================================";
  cout<<" "<<data.E_EGRET[i_E_EGRET1[i_E_fit_range]]<<"-"<<data.E_EGRET[i_E_EGRET2[i_E_fit_range] +1]<<" MeV"<<endl;

  npar=6;
  gMinuit = new TMinuit(npar);  //initialize TMinuit with a maximum of npar params
  gMinuit->SetFCN(fcna6);


   arglist[0] = 1;
   gMinuit->mnexcm("SET ERR", arglist ,1,ierflg);

// Set starting values and step sizes for parameters
//                                                      lower,upper bounds
   gMinuit->mnparm(0, "background5",  vstart[0], step[0], 0.,100.,ierflg);
   gMinuit->mnparm(1, "IC5",          vstart[1], step[1], 0.,100.,ierflg);
   gMinuit->mnparm(2, "HIT5",         vstart[2], step[2], 0.,100.,ierflg);
   gMinuit->mnparm(3, "H2R5_ring1-3", vstart[2], step[2], 0.,100.,ierflg);
   gMinuit->mnparm(4, "H2R5_ring4",   vstart[2], step[2], 0.,100.,ierflg);
   gMinuit->mnparm(5, "H2R5_ring5",   vstart[2], step[2], 0.,100.,ierflg);
  

// Now ready for minimization step
   arglist[0] = 5000;  //maxcalls
   arglist[1] = 0.1;  //tolerance
   gMinuit->mnexcm("MIGRAD", arglist ,2,ierflg);

// Print results

//   gMinuit->mnstat(amin,edm,errdef,nvpar,nparx,icstat);
//   gMinuit->mnprin(3,amin);

  cout<<endl;
  cout<<"            =======fit to IC+ HIT+ H2R Rings 1-3 4 5  likelihood  using TMinuit======================================";
  cout<<" "<<data.E_EGRET[i_E_EGRET1[i_E_fit_range]]<<"-"<<data.E_EGRET[i_E_EGRET2[i_E_fit_range] +1]<<" MeV"<<endl;
  gMinuit->SetFCN(fcna6poisson);
  gMinuit->mnexcm("MIGRAD", arglist ,2,ierflg);

  ////////////////////////////////////////////////////////////////////////////////////////////////

  cout<<endl;
  cout<<"            =======fit to IC+ HIT+ H2R Rings 1-2 3-4 5  chisq using TMinuit======================================";
  cout<<" "<<data.E_EGRET[i_E_EGRET1[i_E_fit_range]]<<"-"<<data.E_EGRET[i_E_EGRET2[i_E_fit_range] +1]<<" MeV"<<endl;

  npar=6;
  gMinuit = new TMinuit(npar);  //initialize TMinuit with a maximum of npar params
  gMinuit->SetFCN(fcnb6);
  

   arglist[0] = 1;
   gMinuit->mnexcm("SET ERR", arglist ,1,ierflg);

// Set starting values and step sizes for parameters
//                                                      lower,upper bounds
   gMinuit->mnparm(0, "background5",  vstart[0], step[0], 0.,100.,ierflg);
   gMinuit->mnparm(1, "IC5",          vstart[1], step[1], 0.,100.,ierflg);
   gMinuit->mnparm(2, "HIT5",         vstart[2], step[2], 0.,100.,ierflg);
   gMinuit->mnparm(3, "H2R5_ring1-2", vstart[2], step[2], 0.,100.,ierflg);
   gMinuit->mnparm(4, "H2R5_ring3-4", vstart[2], step[2], 0.,100.,ierflg);
   gMinuit->mnparm(5, "H2R5_ring5",   vstart[2], step[2], 0.,100.,ierflg);
  

// Now ready for minimization step
   arglist[0] = 5000;  //maxcalls
   arglist[1] = 0.1;  //tolerance
   gMinuit->mnexcm("MIGRAD", arglist ,2,ierflg);

// Print results

//   gMinuit->mnstat(amin,edm,errdef,nvpar,nparx,icstat);
//   gMinuit->mnprin(3,amin);

  cout<<endl;
  cout<<"            =======fit to IC+ HIT+ H2R Rings 1-2 3-4 5  likelihood  using TMinuit======================================";
  cout<<" "<<data.E_EGRET[i_E_EGRET1[i_E_fit_range]]<<"-"<<data.E_EGRET[i_E_EGRET2[i_E_fit_range] +1]<<" MeV"<<endl;
  gMinuit->SetFCN(fcnb6poisson);
  gMinuit->mnexcm("MIGRAD", arglist ,2,ierflg);

  ////////////////////////////////////////////////////////////////////////////////////////////////

  cout<<endl;
  cout<<"            =======fit to IC+ gradient Rings 1-2 3-4 5  chisq using TMinuit======================================";
  cout<<" "<<data.E_EGRET[i_E_EGRET1[i_E_fit_range]]<<"-"<<data.E_EGRET[i_E_EGRET2[i_E_fit_range] +1]<<" MeV"<<endl;


  for (ivalues=0;ivalues<n_values_rebin;ivalues++)
    {
      pred_IC_obs                   ->GetPoint  (ivalues,x1 [ivalues],dummy); // sets x1
   
      i_Ring=0; pred_HIR_obs[i_Ring]->GetPoint  (ivalues,x2 [ivalues],dummy);
      i_Ring=1; pred_HIR_obs[i_Ring]->GetPoint  (ivalues,x3 [ivalues],dummy);
      i_Ring=2; pred_HIR_obs[i_Ring]->GetPoint  (ivalues,x4 [ivalues],dummy);
      i_Ring=3; pred_HIR_obs[i_Ring]->GetPoint  (ivalues,x5 [ivalues],dummy);
      i_Ring=4; pred_HIR_obs[i_Ring]->GetPoint  (ivalues,x6 [ivalues],dummy);

      i_Ring=0; pred_H2R_obs[i_Ring]->GetPoint  (ivalues,x7 [ivalues],dummy);
      i_Ring=1; pred_H2R_obs[i_Ring]->GetPoint  (ivalues,x8 [ivalues],dummy);
      i_Ring=2; pred_H2R_obs[i_Ring]->GetPoint  (ivalues,x9 [ivalues],dummy);
      i_Ring=3; pred_H2R_obs[i_Ring]->GetPoint  (ivalues,x10[ivalues],dummy);
      i_Ring=4; pred_H2R_obs[i_Ring]->GetPoint  (ivalues,x11[ivalues],dummy);

      pred_obs    ->GetPoint  (ivalues, dummy,    z[ivalues]  ); 
     errorz[ivalues] = 
      pred_obs    ->GetErrorY (ivalues );

    counts_obs    ->GetPoint  (ivalues,zcounts  [ivalues],dummy  ); 
  exposure_obs    ->GetPoint  (ivalues,zexposure[ivalues],dummy  ); 


   if(galplotdef.verbose== -501) // selectable debug
       cout<<" x1-6 z errorz zcounts zexposure :"<<x1[ivalues]<<" "<<x2[ivalues]<<" "<<x3[ivalues]<<" "<<x4[ivalues]
         	         <<" "<<x5[ivalues]<<" "<<x6[ivalues]<<" "<<z[ivalues]<<" "<<errorz[ivalues]
                         <<" "<<zcounts[ivalues]<<" "<<zexposure[ivalues]<<endl;
    }

  npar=6;
  gMinuit = new TMinuit(npar);  //initialize TMinuit with a maximum of npar params
  gMinuit->SetFCN(fcnq6);
  

   arglist[0] = 1;
   gMinuit->mnexcm("SET ERR", arglist ,1,ierflg);

// Set starting values and step sizes for parameters
//                                                      lower,upper bounds
   gMinuit->mnparm(0, "background5",  vstart[0], step[0], 0.,100.,ierflg);
   gMinuit->mnparm(1, "IC5",          vstart[1], step[1], 0.,100.,ierflg);
   gMinuit->mnparm(2, "Xco" ,         vstart[2], step[2], 0.,100.,ierflg);
   gMinuit->mnparm(3, "q_ring1-2",    vstart[2], step[2], 0.,100.,ierflg);
   gMinuit->mnparm(4, "q_ring3-4",    vstart[2], step[2], 0.,100.,ierflg);
   gMinuit->mnparm(5, "q_ring5",      vstart[2], step[2], 0.,100.,ierflg);
  

// Now ready for minimization step
   arglist[0] = 5000;  //maxcalls
   arglist[1] = 0.1;  //tolerance
   gMinuit->mnexcm("MIGRAD", arglist ,2,ierflg);

  ////////////////////////////////////////////////////////////////////////////////////////////////

  cout<<endl;
  cout<<"            =======fit to IC+ gradient Rings 1-2 3-4 5-6 7-8  chisq using TMinuit======================================";
  cout<<" "<<data.E_EGRET[i_E_EGRET1[i_E_fit_range]]<<"-"<<data.E_EGRET[i_E_EGRET2[i_E_fit_range] +1]<<" MeV"<<endl;


  for (ivalues=0;ivalues<n_values_rebin;ivalues++)
    {
      pred_IC_obs                   ->GetPoint  (ivalues,x1 [ivalues],dummy); // sets x1
   
      i_Ring=0; pred_HIR_obs[i_Ring]->GetPoint  (ivalues,x2 [ivalues],dummy);
      i_Ring=1; pred_HIR_obs[i_Ring]->GetPoint  (ivalues,x3 [ivalues],dummy);
      i_Ring=2; pred_HIR_obs[i_Ring]->GetPoint  (ivalues,x4 [ivalues],dummy);
      i_Ring=3; pred_HIR_obs[i_Ring]->GetPoint  (ivalues,x5 [ivalues],dummy);
      i_Ring=4; pred_HIR_obs[i_Ring]->GetPoint  (ivalues,x6 [ivalues],dummy);
      i_Ring=5; pred_HIR_obs[i_Ring]->GetPoint  (ivalues,x7 [ivalues],dummy);
      i_Ring=6; pred_HIR_obs[i_Ring]->GetPoint  (ivalues,x8 [ivalues],dummy);
      i_Ring=7; pred_HIR_obs[i_Ring]->GetPoint  (ivalues,x9 [ivalues],dummy);


      i_Ring=0; pred_H2R_obs[i_Ring]->GetPoint  (ivalues,x10[ivalues],dummy);
      i_Ring=1; pred_H2R_obs[i_Ring]->GetPoint  (ivalues,x11[ivalues],dummy);
      i_Ring=2; pred_H2R_obs[i_Ring]->GetPoint  (ivalues,x12[ivalues],dummy);
      i_Ring=3; pred_H2R_obs[i_Ring]->GetPoint  (ivalues,x13[ivalues],dummy);
      i_Ring=4; pred_H2R_obs[i_Ring]->GetPoint  (ivalues,x14[ivalues],dummy);
      i_Ring=5; pred_H2R_obs[i_Ring]->GetPoint  (ivalues,x15[ivalues],dummy);//AWS20070801  x15 (was x14)

      pred_obs    ->GetPoint  (ivalues, dummy,    z[ivalues]  ); 
     errorz[ivalues] = 
      pred_obs    ->GetErrorY (ivalues );

    counts_obs    ->GetPoint  (ivalues,zcounts  [ivalues],dummy  ); 
  exposure_obs    ->GetPoint  (ivalues,zexposure[ivalues],dummy  ); 


   if(galplotdef.verbose== -501) // selectable debug
       cout<<" x1-6 z errorz zcounts zexposure :"<<x1[ivalues]<<" "<<x2[ivalues]<<" "<<x3[ivalues]<<" "<<x4[ivalues]
         	         <<" "<<x5[ivalues]<<" "<<x6[ivalues]<<" "<<z[ivalues]<<" "<<errorz[ivalues]
                         <<" "<<zcounts[ivalues]<<" "<<zexposure[ivalues]<<endl;
    }

  npar=7;
  gMinuit = new TMinuit(npar);  //initialize TMinuit with a maximum of npar params
  gMinuit->SetFCN(fcnq7);
  

   arglist[0] = 1;
   gMinuit->mnexcm("SET ERR", arglist ,1,ierflg);

// Set starting values and step sizes for parameters
//                                                      lower,upper bounds
   gMinuit->mnparm(0, "background5",  vstart[0], step[0], 0.,100.,ierflg);
   gMinuit->mnparm(1, "IC5",          vstart[1], step[1], 0.,100.,ierflg);
   gMinuit->mnparm(2, "Xco" ,         vstart[2], step[2], 0.,100.,ierflg);
   gMinuit->mnparm(3, "q_ring1-2",    vstart[2], step[2], 0.,100.,ierflg);
   gMinuit->mnparm(4, "q_ring3-4",    vstart[2], step[2], 0.,100.,ierflg);
   gMinuit->mnparm(5, "q_ring5-6",    vstart[2], step[2], 0.,100.,ierflg);
   gMinuit->mnparm(6, "q_ring7-8",    vstart[2], step[2], 0.,100.,ierflg);

// Now ready for minimization step
   arglist[0] = 5000;  //maxcalls
   arglist[1] = 0.1;  //tolerance
   gMinuit->mnexcm("MIGRAD", arglist ,2,ierflg);



   //////////////////////////////////////////////////////////////////////////////////////////
// get fitted parameters
   par      =new double[npar];
   par_error=new double[npar];
   for(ipar=0;ipar<npar;ipar++)gMinuit->GetParameter(ipar,par[ipar],par_error[ipar]);
   background3=par[0]; // for pred/obs line in plot

// plot pred/obs for fitted parameters, but with background=0
   par[0]=0;
   pred_IC_gas_obs=new TGraphErrors(n_values_rebin);
   for (ivalues=0;ivalues<n_values_rebin;ivalues++)
    {
      pred_obs       ->GetPoint     (ivalues,dummy,obs);
      pred_IC_gas_obs->SetPoint     (ivalues,func4(x1[ivalues],x2[ivalues],x3[ivalues],par),obs);
      pred_IC_gas_obs->SetPointError(ivalues,0.0, pred_obs->GetErrorY(ivalues));
    }
  

  cout<<"=========================================================================="<<endl;



     }//i_E_fit_range


///////////////////////////////////////////////////////////////////////////////////////////////////

  /*-------------------------- skip plotting for now

int ncolors=1; int *colors=0;
gStyle->SetPalette(ncolors,colors);

  
 

  //====== see HowTo Style: and do before creating canvas
 
 
   TStyle *plain  = new TStyle("Plain","Plain Style (no colors/fill areas)"); 

   
   plain->SetCanvasBorderMode(0);
   plain->SetPadBorderMode(0);
   plain->SetPadColor(0);
   plain->SetCanvasColor(0);
   plain->SetTitleColor(0);
   plain->SetStatColor(0);
   gROOT->SetStyle("Plain");

   int fitted; // 1=fitted by TGraph to total 2=fitted by TMinuit to total 3=fitted by TMinuit to IC+gas

   for (fitted=1;fitted<=3;fitted++ )
   {
     if(fitted==2)   pred_obs=pred_total_obs; 
     if(fitted==3)   pred_obs=pred_IC_gas_obs;

  sprintf(name,"fit_EGRET%d_%d",i_E_EGRET,fitted);
  TCanvas *c1=new TCanvas(name,name,100+i_E_EGRET*20,100+i_E_EGRET*10,600,600);

  TGaxis::SetMaxDigits(3); // for well-labelled axes
  c1->DrawFrame(0.0,0.0,maximum_intensity*1.1,maximum_intensity*1.1);
  
  //AFTER DrawFrame or Draw() (else crash)
  pred_obs->GetHistogram()->SetXTitle("predicted intensity, cm^{-2} sr^{-1} s^{-1} ");
  pred_obs->GetHistogram()->SetYTitle("observed  intensity, cm^{-2} sr^{-1} s^{-1} ");
  pred_obs->GetHistogram()->SetLabelOffset(+0.00,"X"); //  +ve down
  pred_obs->GetHistogram()->SetLabelSize  ( 0.03,"Y"); // labels, not titles
  pred_obs->GetHistogram()->SetLabelOffset(+0.00,"X"); //  +ve down
  pred_obs->GetHistogram()->SetLabelSize  ( 0.03,"X"); // labels, not titles

  pred_obs->GetHistogram()->SetTitleOffset(+0.85,"X");  //  +ve down
  pred_obs->GetHistogram()->SetTitleSize   (0.032,"X");
  pred_obs->GetHistogram()->SetTitleOffset(+1.5,"Y");  //  +ve to left
  pred_obs->GetHistogram()->SetTitleSize   (0.032,"Y");

  pred_obs->SetMarkerColor(kBlue);
  pred_obs->SetMarkerStyle(21); 
  pred_obs->SetMarkerStyle(22); // triangles
  pred_obs ->SetMarkerSize (0.5);// 
  pred_obs ->SetLineColor(kBlue);
  pred_obs ->SetLineWidth(1     );
  pred_obs ->SetLineStyle(1      );
  pred_obs ->SetMarkerStyle(22); // triangles
  pred_obs ->SetMarkerSize (0.5);// 
                                                 

  pred_obs ->Draw(" P"); // axes and markers

  if(fitted==2)
    {
      // this case the fit is not in the TGraph so must draw line obs=pred+background
  TGraph *line=new TGraph(2);
  line->SetPoint(0,0.0,    background2);
  line->SetPoint(1,1.0,1.0+background2);
  line->Draw("L");
    }


  if(fitted==3)
    {
      // this case the fit is not in the TGraph so must draw line obs=pred+background
  TGraph *line=new TGraph(2);
  line->SetPoint(0,0.0,    background3);
  line->SetPoint(1,1.0,1.0+background3);
  line->Draw("L");
    }


  sprintf(workstring1,"  %5.1f<l<%5.1f , %5.1f<l<%5.1f",
                          galplotdef.long_min1,galplotdef.long_max1,
                          galplotdef.long_min2,galplotdef.long_max2);
 
  sprintf(workstring2,"  %5.1f<b<%5.1f , %5.1f<b<%5.1f",
                         galplotdef.lat_min1,galplotdef.lat_max1,
                         galplotdef.lat_min2,galplotdef.lat_max2);
  
  sprintf(workstring3," %5.0f - %5.0f MeV",data.E_EGRET[i_E_EGRET],data.E_EGRET[i_E_EGRET+1]);


  strcpy(workstring4," galdef ID ");
  strcat(workstring4,galdef.galdef_ID);


  TText *text=new TText();
  text->SetTextFont(62);
  text->SetTextColor(kBlack);
  text->SetTextSize(0.025 );
  text->SetTextAlign(12);


  text->DrawTextNDC(.13 ,.82 ,workstring1);// NDC=normalized coord system
  text->DrawTextNDC(.13 ,.85 ,workstring2);// NDC=normalized coord system
  text->DrawTextNDC(.13 ,.88 ,workstring3);// NDC=normalized coord system
  text->DrawTextNDC(.20 ,.92 ,workstring4);// NDC=normalized coord system


 



  //============== postscript output

  sprintf(workstring1,"l_%.1f_%.1f_%.1f_%.1f",
                          galplotdef.long_min1,galplotdef.long_max1,
                          galplotdef.long_min2,galplotdef.long_max2);

  sprintf(workstring2,"b_%.1f_%.1f_%.1f_%.1f",
                          galplotdef.lat_min1,galplotdef.lat_max1,
                          galplotdef.lat_min2,galplotdef.lat_max2);

  sprintf(workstring3,"%.0f-%.0f_MeV",data.E_EGRET[i_E_EGRET],data.E_EGRET[i_E_EGRET+1]);

  strcpy(psfile,"plots/");
  strcat(psfile,"gamma_pred_obs_");
  strcat(psfile,galdef.galdef_ID);
  strcat(psfile,"_"        );
  strcat(psfile,workstring3);
  strcat(psfile,"_"        );
  strcat(psfile,workstring1);
  strcat(psfile,"_"        );
  strcat(psfile,workstring2);

  if(galplotdef.convolve_EGRET==0)
  strcat(psfile,"_unconv_"  );
  if(galplotdef.convolve_EGRET!=0)
  strcat(psfile,"_conv_"    );

  if(fitted==1)strcat(psfile,"fitted_total_TGraph_");
  if(fitted==2)strcat(psfile,"fitted_total_" );
  if(fitted==3)strcat(psfile,"fitted_IC_gas_");

  strcat(psfile,galplotdef.psfile_tag);

 

  strcat(psfile,".eps");

  if(options==1)//AWS20040420
  {
  cout<<"postscript file="<<psfile<<endl;
  c1->Print(psfile       );
  }

   }//if fitted
  //==============
     ------------------- skip plotting for now--------------------*/


  ///	}//i_E_EGRET



       /*------------- skip this for now

       //=== print differential and integral background spectra

          for(i_E_EGRET=0; i_E_EGRET<data.n_E_EGRET                    ; i_E_EGRET++)
        {
	  cout<<data.E_EGRET[i_E_EGRET]<<"-"<<data.E_EGRET[i_E_EGRET+1]<<" MeV: differential background spectrum= "
          <<background_spectrum[i_E_EGRET]<<" +-"<<background_spectrum_errorbar[i_E_EGRET]  <<endl;
	}

          for(i_E_EGRET=0;          i_E_EGRET<data.n_E_EGRET          ;  i_E_EGRET++)
        {
	  background_integral_spectrum         [i_E_EGRET]=0.;
          background_integral_spectrum_errorbar[i_E_EGRET]=0.;

          int ii_E_EGRET;
          for(ii_E_EGRET=i_E_EGRET; ii_E_EGRET<data.n_E_EGRET         ; ii_E_EGRET++)
	  {
           background_integral_spectrum         [i_E_EGRET] +=    background_spectrum         [ii_E_EGRET] ;
           background_integral_spectrum_errorbar[i_E_EGRET] +=pow(background_spectrum_errorbar[ii_E_EGRET],2);
          }

	  background_integral_spectrum_errorbar[i_E_EGRET]=sqrt(background_integral_spectrum_errorbar[i_E_EGRET]);

	  cout<<data.E_EGRET[i_E_EGRET]<<" MeV:    integral  background spectrum= "
          <<background_integral_spectrum[i_E_EGRET]<<" +-"<<background_integral_spectrum_errorbar[i_E_EGRET]  <<endl;
	}


	-----------------*/

   cout<<" <<<< fit_EGRET_Xco   "<<endl;
   return status;
}
 
//______________________________________________________________________________
static double func2(double x,          double *par)
{
  double value=par[0] + par[1]*x           ;

 return value;
}
 
//______________________________________________________________________________
static double func3(double x,double y, double *par)
{
  double value=par[0] + par[1]*x + par[2]*y;

 return value;
}
//______________________________________________________________________________
static double func4(double x1,double x2, double x3, double *par)
{
  double value=par[0] + par[1]*x1 + par[2]*x2 + par[3]*x3;

 return value;
}
//______________________________________________________________________________
static void fcn2(int &npar, double *gin, double &f, double *par, int iflag)
{
  //gin=optional input gradients

  
   int i;

//calculate chisquare
   double chisq = 0;
   double delta;
   for (i=0;i<n_values_fcn; i++) {
     delta  = (z[i]-func2(x[i],     par))/errorz[i];
     chisq += delta*delta;
   }
   f = chisq;
}
//______________________________________________________________________________
static void fcn3(int &npar, double *gin, double &f, double *par, int iflag)
{
  //gin=optional input gradients

  
   int i;

//calculate chisquare
   double chisq = 0;
   double delta;
   for (i=0;i<n_values_fcn; i++) {
     delta  = (z[i]-func3(x[i],y[i],par))/errorz[i];
     chisq += delta*delta;
   }
   f = chisq;
}
//______________________________________________________________________________
static void fcn4(int &npar, double *gin, double &f, double *par, int iflag)
{
  //gin=optional input gradients

  
   int i;

//calculate chisquare
   double chisq = 0;
   double delta;
   for (i=0;i<n_values_fcn; i++) {
     delta  = (z[i]-func4(x1[i],x2[i],x3[i],par))/errorz[i];
     chisq += delta*delta;
   }
   f = chisq;
}
//______________________________________________________________________________
static void fcn5(int &npar, double *gin, double &f, double *par, int iflag)
{
  //gin=optional input gradients

  
   int i;

//calculate chisquare
   double chisq = 0;
   double delta;
   double pred;

   for (i=0;i<n_values_fcn; i++) {
     pred=par[0] + par[1]*x1[i] + par[2]*x2[i] + par[3]*x3[i] + par[4]*x4[i];
     delta  = (z[i]- pred)/errorz[i];
     chisq += delta*delta;
   }
   f = chisq;
}
//______________________________________________________________________________
static void fcn6(int &npar, double *gin, double &f, double *par, int iflag)
{
  //gin=optional input gradients

  
   int i;

//calculate chisquare
   double chisq = 0;
   double delta;
   double pred;

   for (i=0;i<n_values_fcn; i++) {
     pred=par[0] + par[1]*x1[i] + par[2]*x2[i] + par[3]*x3[i] + par[4]*x4[i] + par[5]*x5[i] ;
     delta  = (z[i]- pred)/errorz[i];
     chisq += delta*delta;
   }
   f = chisq;
}
//______________________________________________________________________________
static void fcn7(int &npar, double *gin, double &f, double *par, int iflag)
{
  //gin=optional input gradients

  
   int i;

//calculate chisquare
   double chisq = 0;
   double delta;
   double pred;

   for (i=0;i<n_values_fcn; i++) {
     pred=par[0] + par[1]*x1[i] + par[2]*x2[i] + par[3]*x3[i] + par[4]*x4[i] + par[5]*x5[i] + par[6]*x6[i] ;
     delta  = (z[i]- pred)/errorz[i];
     chisq += delta*delta;
   }
   f = chisq;

   //   cout<<f<<endl;
}
//_____________________________________________________________________________________
static void fcn7poisson(int &npar, double *gin, double &f, double *par, int iflag)
{
  //gin=optional input gradients

  
   int i;

//calculate chisquare
   double chisq = 0;
   double delta;
   double pred;
   double logL  = 0;

   for (i=0;i<n_values_fcn; i++) {
     pred=par[0] + par[1]*x1[i] + par[2]*x2[i] + par[3]*x3[i] + par[4]*x4[i] + par[5]*x5[i] + par[6]*x6[i] ;
     pred*=zexposure[i];
     // cout<<"fcn7poissoin: pred, obs counts:"<<pred<<" "<<zcounts[i]<<endl;
     delta  = (zcounts[i]- pred);
     chisq += delta*delta;

     // L= exp^-x x^n    logL=-x + n log x
     delta=  pred - zcounts[i]*log(pred); // -logL
     logL+=delta;


   }
   //   f = chisq;
   f = logL;
}
//______________________________________________________________________________
static void fcn8(int &npar, double *gin, double &f, double *par, int iflag)
{
  //gin=optional input gradients

  
   int i;

//calculate chisquare
   double chisq = 0;
   double delta;
   double pred;

   for (i=0;i<n_values_fcn; i++) {
     pred=par[0] + par[1]*x1[i] + par[2]*x2[i] + par[3]*x3[i] + par[4]*x4[i] + par[5]*x5[i] + par[6]*x6[i]+ par[7]*x7[i] ;
     delta  = (z[i]- pred)/errorz[i];
     chisq += delta*delta;
   }
   f = chisq;

   //   cout<<f<<endl;
}
//_____________________________________________________________________________________
static void fcn8poisson(int &npar, double *gin, double &f, double *par, int iflag)
{
  //gin=optional input gradients

  
   int i;

//calculate chisquare
   double chisq = 0;
   double delta;
   double pred;
   double logL  = 0;

   for (i=0;i<n_values_fcn; i++) {
     pred=par[0] + par[1]*x1[i] + par[2]*x2[i] + par[3]*x3[i] + par[4]*x4[i] + par[5]*x5[i] + par[6]*x6[i]+ par[7]*x7[i] ;
     pred*=zexposure[i];
     // cout<<"fcn7poissoin: pred, obs counts:"<<pred<<" "<<zcounts[i]<<endl;
     delta  = (zcounts[i]- pred);
     chisq += delta*delta;

     // L= exp^-x x^n    logL=-x + n log x
     delta=  pred - zcounts[i]*log(pred); // -logL
     logL+=delta;


   }
   //   f = chisq;
   f = logL;
}
//______________________________________________________________________________
static void fcna6(int &npar, double *gin, double &f, double *par, int iflag)
{
  // rings 1-3 combined
  //gin=optional input gradients

  
   int i;

//calculate chisquare
   double chisq = 0;
   double delta;
   double pred;

   for (i=0;i<n_values_fcn; i++)
   {
     pred=par[0] + par[1]*x1[i]+ par[2]*x2[i]
        + par[3] * ( x3[i] + x4[i] + x5[i] )
        + par[4]*x6[i]  +     par[5]*x7[i] ;
     delta  = (z[i]- pred)/errorz[i];
     chisq += delta*delta;
   }
   f = chisq;

   //   cout<<f<<endl;
}
//_____________________________________________________________________________________
static void fcna6poisson(int &npar, double *gin, double &f, double *par, int iflag)
{
  //gin=optional input gradients

  
   int i;

//calculate chisquare
   double chisq = 0;
   double delta;
   double pred;
   double logL  = 0;

   for (i=0;i<n_values_fcn; i++)
   {
         pred=par[0] + par[1]*x1[i]+ par[2]*x2[i]
        + par[3] * ( x3[i] + x4[i] + x5[i] )
        + par[4]*x6[i]  +     par[5]*x7[i] ;
     pred*=zexposure[i];
     // cout<<"fcn7poissoin: pred, obs counts:"<<pred<<" "<<zcounts[i]<<endl;
     delta  = (zcounts[i]- pred);
     chisq += delta*delta;

     // L= exp^-x x^n    logL=-x + n log x
     delta=  pred - zcounts[i]*log(pred); // -logL
     logL+=delta;


   }
   //   f = chisq;
   f = logL;
}
//--------------------------------------------------------------------------------------------
static void fcnb6(int &npar, double *gin, double &f, double *par, int iflag)
{
  // rings 1-2 combined and 3-4 combined
  //gin=optional input gradients

  
   int i;

//calculate chisquare
   double chisq = 0;
   double delta;
   double pred;

   for (i=0;i<n_values_fcn; i++)
   {
     pred=par[0] + par[1]*x1[i] + par[2]*x2[i]
        + par[3] * ( x3[i] + x4[i] )
        + par[4] *  (x5[i] + x6[i] )
        + par[5]*x7[i] ;
     delta  = (z[i]- pred)/errorz[i];
     chisq += delta*delta;
   }
   f = chisq;

   //   cout<<f<<endl;
}
//_____________________________________________________________________________________
static void fcnb6poisson(int &npar, double *gin, double &f, double *par, int iflag)
{
  //gin=optional input gradients

  
   int i;

//calculate chisquare
   double chisq = 0;
   double delta;
   double pred;
   double logL  = 0;

   for (i=0;i<n_values_fcn; i++)
   {
          pred=par[0] + par[1]*x1[i] + par[2]*x2[i]
        + par[3] * ( x3[i] + x4[i] )
        + par[4] *  (x5[i] + x6[i] )
        + par[5]*x7[i] ;

     pred*=zexposure[i];
     // cout<<"fcn7poissoin: pred, obs counts:"<<pred<<" "<<zcounts[i]<<endl;
     delta  = (zcounts[i]- pred);
     chisq += delta*delta;

     // L= exp^-x x^n    logL=-x + n log x
     delta=  pred - zcounts[i]*log(pred); // -logL
     logL+=delta;


   }
   //   f = chisq;
   f = logL;
}
//--------------------------------------------------------------------------------------------
static void fcnq6(int &npar, double *gin, double &f, double *par, int iflag)
{
  // q gradient
  // rings 1-2 combined and 3-4 combined
  //gin=optional input gradients
  // x2-6=HIR  x7-11=H2R
  // par[2]=Xco
  // par[3]...= q(R)

   int i;

//calculate chisquare
   double chisq = 0;
   double delta;
   double pred;

   for (i=0;i<n_values_fcn; i++)
   {
     pred=par[0]
        + par[1] *    x1[i] 
        + par[3] *  ( x2[i] + x3[i] + par[2]*(  x7 [i] + x8 [i]   ) )
        + par[4] *  ( x4[i] + x5[i] + par[2]*(  x9 [i] + x10[i]    ) )
        + par[5] *  ( x6[i]         + par[2]*(  x11[i]             ) ) ;
     delta  = (z[i]- pred)/errorz[i];
     chisq += delta*delta;
   }
   f = chisq;

   //   cout<<f<<endl;
}
//--------------------------------------------------------------------------------------------
static void fcnq7(int &npar, double *gin, double &f, double *par, int iflag)
{
  // q gradient
  // rings 1-2  3-4 5-6 7-8

  // x2-9=HIR  x10-15=H2R
  // par[2]=Xco
  // par[3]...= q(R)

   int i;

//calculate chisquare
   double chisq = 0;
   double delta;
   double pred;

   for (i=0;i<n_values_fcn; i++)
   {
    pred=par[0]                                                           // background
       + par[1] *    x1[i]                                                // IC
       + par[3] *  ( x2[i] + x3[i] + par[2]*(  x10[i] + x11[i]   ) )      //  rings 1-2
       + par[4] *  ( x4[i] + x5[i] + par[2]*(  x12[i] + x13[i]   ) )      //  ring  3-4
       + par[5] *  ( x6[i] + x7[i] + par[2]*(  x14[i] + x15[i]   ) )      //  rings 5-6
       + par[6] *  ( x8[i] + x9[i]                                 );     //  rings 7-8

     delta  = (z[i]- pred)/errorz[i];
     chisq += delta*delta;
   }
   f = chisq;

   //   cout<<f<<endl;
}




///////////////////////////////////////////////
///////////////////////////////////////////////
int Galplot::rebin_EGRET_data(int longbin, int latbin, double &long_min, double &lat_min, double &d_long, double &d_lat)
{
  int i_long,ii_long,i_lat,ii_lat,i_E_EGRET;

  cout<<"rebin_EGRET_data"<<endl;
  cout<<"rebinning EGRET data with "<<longbin<<" , "<<latbin<<" *0.5 deg longitude, latitude  bins"<<endl;

  // EGRET data are  720*360 so check for exact division
  if(720%longbin !=0){cout<<"longitude bins not divisible into 720 ! quitting"<<endl; return 1;}
  if(360% latbin !=0){cout<<" latitude bins not divisible into 360 ! quitting"<<endl; return 1;}

  EGRET_counts_rebinned.init  (data.EGRET_counts.n_rgrid/longbin,
                               data.EGRET_counts.n_zgrid/latbin,
                               data.EGRET_counts.n_pgrid);

  EGRET_exposure_rebinned.init(data.EGRET_counts.n_rgrid/longbin,
                               data.EGRET_counts.n_zgrid/latbin,
                               data.EGRET_counts.n_pgrid);


	 for (i_lat =0;  i_lat <data.EGRET_counts.n_zgrid;  i_lat++ )
         {
          for(i_long=0;  i_long<data.EGRET_counts.n_rgrid; i_long++)
          {
	    ii_long=i_long/longbin;
	    ii_lat =i_lat / latbin;

            for(i_E_EGRET=0; i_E_EGRET<data.n_E_EGRET                    ; i_E_EGRET++)
	      {
                        EGRET_counts_rebinned  .d2[ii_long][ii_lat].s[i_E_EGRET]
	       +=  data.EGRET_counts           .d2[ i_long][ i_lat].s[i_E_EGRET];

                        EGRET_exposure_rebinned.d2[ii_long][ii_lat].s[i_E_EGRET]
	       +=  data.EGRET_exposure         .d2[ i_long][ i_lat].s[i_E_EGRET];


	      }

	  }
	 }

	 /* original binning
  long_min= -179.75;//specific for EGRET data counts.gp1234_30.g001
   lat_min  =-89.75;
   d_long   =  0.5;     
   d_lat    =  0.5;
	 */

    long_min   = -180.0+longbin*0.25;
    lat_min    =  -90.0+ latbin*0.25;
    d_long     =        longbin*0.5;     
    d_lat      =         latbin*0.5;



    cout<<"long_min="<<long_min<<endl;
    cout<<" lat_min="<< lat_min<<endl;
    cout<<"  d_long="<<  d_long<<endl;
    cout<<"  d_lat ="<<  d_lat <<endl;

    if(galplotdef.verbose==-502)//selectable debug
    {
     cout<<"EGRET_counts_rebinned  "<<endl;
     EGRET_counts_rebinned  .print();
     cout<<"EGRET_exposure_rebinned"<<endl;
     EGRET_exposure_rebinned.print();
    }
 



  return 0;
}
