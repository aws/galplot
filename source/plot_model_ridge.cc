/* 
ad hoc ridge model with thermal, power-law, positronium
*/
#include"Galplot.h"                    //AWS20050920
#include"galprop_classes.h"
#include"galprop.h"
#include"constants.h"
#include"fitsio.h"

int Galplot::plot_model_ridge  (int mode)
{

   cout<<" >>>> plot_model_ridge      "<<endl;
   int status=0;

   int test_comparison;

 
   int n_energy;
   double E, intensity,flux_cr,flux_exp,flux_positronium;
   double Emin,Emax,dE;     
   double solid_angle;             
   int i_energy;
   int component;
   int color;

   TGraph *spectrum;
  
   Emin=0.002;//MeV
   Emax=30.;
   dE= 0.001;

   solid_angle= (galplotdef. lat_max1-galplotdef. lat_min1 + galplotdef. lat_max2-galplotdef. lat_min2 )
               *(galplotdef.long_max1-galplotdef.long_min1 + galplotdef.long_max2-galplotdef.long_min2 )
                /(57.3*57.3);// sterad;



   // 
   // double Acr=15.0e-2 ;//at 0.1 MeV
   double Acr=15.0e-2 ;//at 0.1 MeV
   double gcr= 1.90;

   // double Aexp=0.100 ;
   double Aexp=0.150 ;
   double gamma1=1.84;
   double Ec=0.050;


   double factor_continuum,factor_positronium;

   factor_continuum  =1.0; //
   factor_positronium=0.5;

   if(mode ==2)
     {
       Acr =15.0e-2;
       Aexp=0.300 ;
       Ec=0.01;
     }

   n_energy=(Emax-Emin)/dE;



   for (component=1;component<=4;component++)
  {
    spectrum=new TGraph(n_energy);// otherwise disappear

   for (i_energy=0;i_energy<n_energy;i_energy++)
   {
     E=Emin+i_energy*dE;
     intensity=1.e-2;

   ;
   flux_cr         = Acr * pow( E/0.1,  -gcr);
   // flux_exp        = Aexp* pow( E/0.1 , -gamma1)*exp(-(E-0.1)/Ec);
   flux_exp        = Aexp* pow( E/0.1 , -gamma1)*exp(-E/Ec);

   flux_positronium= E * 0.1 ;   // from Fig 1a to give 5e-2 at 0.511 MeV
   if(E>.511) flux_positronium=0.;

   flux_cr         *=factor_continuum;
   flux_exp        *=factor_continuum;
   flux_positronium*=factor_positronium;

   if(component==1)intensity=flux_cr;                                   
   if(component==2)intensity=          flux_exp;                             
   if(component==3)intensity=                      flux_positronium;
   if(component==4)intensity=flux_cr + flux_exp +  flux_positronium;

   intensity/=solid_angle;
   intensity *= pow(E,2.);

   spectrum->SetPoint(i_energy,E,intensity);


   }//i_energy

  if(  component==1) color=kBlack  ;//  cr                        also kCyan
  if(  component==2) color=kGreen  ;//  exp
  if(  component==3) color=kRed    ;//  positronium
  if(  component==4) color=kMagenta;//  total


  spectrum->SetMarkerColor(color   );// also kCyan
  spectrum->SetMarkerStyle(21);
  spectrum->SetMarkerStyle(22); // triangles
  spectrum->SetMarkerSize (0.5);//
  spectrum->SetLineColor(color   );// also kCyan

  spectrum->SetLineWidth(2     );
  if(component==4)
  spectrum->SetLineWidth(2     );

  spectrum->SetLineStyle(1      );//1=solid 2=dash 3=dot 4=dash-dot
  spectrum->SetMarkerStyle(22); // triangles
  spectrum->SetMarkerSize (0.5);//


  //spectrum->Draw("PL");  // points as markers + line 
  spectrum->Draw("L");                              

}// component
 

  cout<<" <<<< plot_model_ridge      "<<endl;
   return status;
}

