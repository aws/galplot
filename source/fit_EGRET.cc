#include"Galplot.h"                    //AWS20050920
#include"galprop_classes.h"
#include"galprop.h"
#include"constants.h"
#include"fitsio.h"


// shared variables with file scope
int  n_values_fcn;
static double *x,*y,*z,*errorz;
static double func2(double x,          double *par);
static double func3(double x,double y, double *par);
static void    fcn2(int &npar, double *gin, double &f, double *par, int iflag);
static void    fcn3(int &npar, double *gin, double &f, double *par, int iflag);



//////////////////////////////////////////////////////////////////////////////////////////

int Galplot::fit_EGRET(int options)                             // AWS20040420
{
  if(options==1)cout<<" >>>> fit_EGRET  options= "<<options<<" statistical error "                <<endl;
  if(options==2)cout<<" >>>> fit_EGRET  options= "<<options<<" statistical plus systematic error "<<endl;

   int status=0;

int i_E_EGRET;
int ivalues,iivalues,nvalues,n_values_rebin,n_values_rebin_  ;


int i_comp,i_long,i_lat,ii_long,ii_lat;
 

double long_min,lat_min;
double d_long, d_lat;


double l,b,l_off;
double ic_total;
double isotropic_intensity;
double pred,obs,counts,error_estimate;
double pred_IC,pred_gas;
double maximum_intensity;


 double *background_spectrum,          *background_integral_spectrum;
 double *background_spectrum_errorbar, *background_integral_spectrum_errorbar;

char name[100],canvastitle[100], workstring1[100],workstring2[100],workstring3[100],workstring4[100];
char psfile[400];

 TGraphErrors *pred_obs;
 TGraphErrors *pred_IC_obs;
 TGraphErrors *pred_gas_obs;
 TGraphErrors *pred_total_obs;
 TGraphErrors *pred_IC_gas_obs;

 TGraph       *pred_obs_;
 TH1D *pred_th1,*obs_th1,*counts_th1;
 TH1D *pred_IC_th1,*pred_gas_th1;
 TF1 *fitfunction1,*fitfunction2;


 int npar;
 double *par,*par_error;
 int ipar;
 TMinuit *gMinuit;

 double f,*gin;                   //AWS20040413
 int iflag;                       //AWS20040413
 double chisq_input,chisq_fitted; //AWS20040413
 int data_points_cumulative;      //AWS20040420

 // first compute number of values to fit

  long_min= -179.75;//specific for EGRET data counts.gp1234_30.g001
   lat_min  =-89.75;
  d_long      =0.5;     
  d_lat       =0.5;



         nvalues=0;

	 for (i_lat =0;  i_lat <data.EGRET_counts.n_zgrid;  i_lat++ )
         {
          for(i_long=0;  i_long<data.EGRET_counts.n_rgrid; i_long++)
         {

 


           l=       long_min+ d_long   *i_long;
           if(l<0.0) l+=360.0;
           b=        lat_min+ d_lat    *i_lat;
	   

           if( 
                 ((l>=galplotdef.long_min1 && l<=galplotdef.long_max1) || (l>=galplotdef.long_min2 && l<=galplotdef.long_max2) )  
	      && ((b>=galplotdef. lat_min1 && b<=galplotdef. lat_max1) || (b>=galplotdef. lat_min2 && b<=galplotdef .lat_max2) )
              )
	     {
	       //   cout<<l<<" "<<b<<" "<<" selected"<<endl;
	       nvalues++;
	     }
           
             }  //  i_long
           }   //  i_lat

         cout<<" nvalues for fitting="<<nvalues<<endl;



//////////////////////////////////////////////////////////////////////////////////

      background_spectrum                  =new double[data.n_E_EGRET];
      background_spectrum_errorbar         =new double[data.n_E_EGRET];
      background_integral_spectrum         =new double[data.n_E_EGRET];
      background_integral_spectrum_errorbar=new double[data.n_E_EGRET];
      for(i_E_EGRET=0; i_E_EGRET<data.n_E_EGRET  ; i_E_EGRET++)
      {
	background_integral_spectrum         [i_E_EGRET]=0;
	background_integral_spectrum_errorbar[i_E_EGRET]=0;
      }
//////////////////////////////////////////////////////////////////////////////////

      chisq_input=0.0; chisq_fitted=0.0; data_points_cumulative=0; //AWS20040420

       for(i_E_EGRET=0; i_E_EGRET<data.n_E_EGRET                    ; i_E_EGRET++)
        {

	 pred_th1=new TH1D("pred",     "pred"      ,nvalues,0.,0.);
      pred_IC_th1=new TH1D("pred_IC",  "pred_IC"   ,nvalues,0.,0.);
     pred_gas_th1=new TH1D("pred_gas", "pred_gas"  ,nvalues,0.,0.);
  	  obs_th1=new TH1D("obs",      "obs"       ,nvalues,0.,0.);
       counts_th1=new TH1D("counts",   "counts"    ,nvalues,0.,0.);

         ivalues=0;


	 // indices of EGRET maps

	 for (i_lat =0;  i_lat <data.EGRET_counts.n_zgrid;  i_lat++ )
         {
          for(i_long=0;  i_long<data.EGRET_counts.n_rgrid; i_long++)
	    {
 
           l=       long_min+ d_long   *i_long;
           if(l<0.0) l+=360.0;
           b=        lat_min+ d_lat    *i_lat;

           if( 
                 ((l>=galplotdef.long_min1 && l<=galplotdef.long_max1) || (l>=galplotdef.long_min2 && l<=galplotdef.long_max2) )  
	      && ((b>=galplotdef. lat_min1 && b<=galplotdef. lat_max1) || (b>=galplotdef. lat_min2 && b<=galplotdef. lat_max2  ) )
              )
	     {

	    // indices of galprop maps 
            ii_long= (int) ((l-galaxy.long_min)/galaxy.d_long+.0001);
            ii_lat = (int) ((b-galaxy.lat_min )/galaxy.d_lat +.0001);
	    //    cout<<"l b  ii_long ii_lat  "<<l<<" "<<b<<" "<<ii_long<<" "<<ii_lat<<endl;

          
            ic_total=0;
            for(i_comp=0;i_comp<galaxy.n_ISRF_components;i_comp++)
              ic_total+= convolved.EGRET_IC_iso_skymap[i_comp].d2[ii_long][ii_lat].s[i_E_EGRET];

          
            pred=        ic_total;
            pred+=       convolved.EGRET_bremss_skymap   .     d2[ii_long][ii_lat].s[i_E_EGRET];
            pred+=       convolved.EGRET_pi0_decay_skymap.     d2[ii_long][ii_lat].s[i_E_EGRET]; 

            pred_IC  =   ic_total;
            pred_gas =   convolved.EGRET_bremss_skymap   .     d2[ii_long][ii_lat].s[i_E_EGRET];
            pred_gas+=   convolved.EGRET_pi0_decay_skymap.     d2[ii_long][ii_lat].s[i_E_EGRET]; 

            counts=       data.EGRET_counts.  d2[i_long][i_lat].s[i_E_EGRET];


             obs  =counts/data.EGRET_exposure.d2[i_long][i_lat].s[i_E_EGRET]; // exposure of EGRET map bin:includes solid angle


            pred_th1    ->SetBinContent(ivalues,pred    );
            pred_IC_th1 ->SetBinContent(ivalues,pred_IC );
            pred_gas_th1->SetBinContent(ivalues,pred_gas);

             obs_th1->SetBinContent(ivalues, obs   );
          counts_th1->SetBinContent(ivalues, counts);


	  //     cout<<"l b exposure  counts pred obs "
	  //	  <<l<<" "<<b<<" "<<data.EGRET_exposure.d2[i_long][i_lat].s[i_E_EGRET]<<" "<<counts<<" "<<" "<<pred<<" "<<obs<<endl;
           


 
 


            ivalues++;

	  }//if


            }  //  i_long
           }   //  i_lat

   
      

	     pred_th1->Rebin(galplotdef.fit_nrebin_EGRET);
          pred_IC_th1->Rebin(galplotdef.fit_nrebin_EGRET);
	 pred_gas_th1->Rebin(galplotdef.fit_nrebin_EGRET);
	      obs_th1->Rebin(galplotdef.fit_nrebin_EGRET);
	   counts_th1->Rebin(galplotdef.fit_nrebin_EGRET);

	   n_values_rebin_= pred_th1->GetNbinsX(); // before excluding low counts

	     // rebinning adds the bins so must rescale pred and obs (but counts remain summed)
	     pred_th1->Scale(1./galplotdef.fit_nrebin_EGRET);
	  pred_IC_th1->Scale(1./galplotdef.fit_nrebin_EGRET);
         pred_gas_th1->Scale(1./galplotdef.fit_nrebin_EGRET);
              obs_th1->Scale(1./galplotdef.fit_nrebin_EGRET);

	     cout<<"n_rebin "<<galplotdef.fit_nrebin_EGRET<<" number of values after rebinning="<<n_values_rebin_<<endl;




	     iivalues=0;
	     for (ivalues=0;ivalues<n_values_rebin_;ivalues++)
	     {
                counts=counts_th1 ->GetBinContent(ivalues);
		if(counts<float(galplotdef.fit_counts_min_EGRET))
                {
		  //cout<<"remove point "<<ivalues<< " with counts="<<counts<<endl;
                
                }
                else iivalues++;
	     }

	     n_values_rebin=iivalues; // after excluding low counts

	     pred_obs = new TGraphErrors(n_values_rebin);
	  pred_IC_obs = new TGraphErrors(n_values_rebin);
         pred_gas_obs = new TGraphErrors(n_values_rebin);

	     pred_obs_= new TGraph      (n_values_rebin);



	     cout<<"number of values after removing low counts:"<<pred_obs->GetN()<<endl;


             maximum_intensity=0.;
             iivalues=0;
	     for (ivalues=0;ivalues<n_values_rebin_;ivalues++)
	     {

              counts=counts_th1 ->GetBinContent(ivalues);

		if(counts<galplotdef.fit_counts_min_EGRET)
                {
		  cout<<"remove point "<<ivalues<< " with counts="<<counts<<endl;
                }
                  else
		    {
	          pred_obs    ->SetPoint(iivalues,pred_th1    ->GetBinContent(ivalues),obs_th1->GetBinContent(ivalues) );
        	
	          pred_IC_obs ->SetPoint(iivalues,pred_IC_th1 ->GetBinContent(ivalues),obs_th1->GetBinContent(ivalues) );
	          pred_gas_obs->SetPoint(iivalues,pred_gas_th1->GetBinContent(ivalues),obs_th1->GetBinContent(ivalues) );


	          error_estimate= obs_th1->GetBinContent(ivalues)/sqrt(counts);

		  // add systematic error if required                           AWS20040420
		  if(options==2)error_estimate=sqrt(  pow(error_estimate,2)
						    + pow(obs_th1->GetBinContent(ivalues)*galplotdef.error_bar_EGRET, 2) );

	          pred_obs->SetPointError(iivalues,0.0, error_estimate );

                  if (pred_th1->GetBinContent(ivalues)>maximum_intensity)
                                                       maximum_intensity=pred_th1->GetBinContent(ivalues);
                  if ( obs_th1->GetBinContent(ivalues)>maximum_intensity)
                                                       maximum_intensity= obs_th1->GetBinContent(ivalues);





		  if(kFALSE)
		{
	            cout<<data.E_EGRET[i_E_EGRET]<<"-"<<data.E_EGRET[i_E_EGRET+1]<<" MeV " 
	      	  <<"pred="<<pred_th1->GetBinContent(ivalues)<<" obs="<<obs_th1->GetBinContent(ivalues)
                  <<" counts= "<<counts<<" error estimate="<<error_estimate<<endl;
		}


		  iivalues++;

		    }//else
	     }//ivalues




  cout<<endl<<"==================================== "<<data.E_EGRET[i_E_EGRET]<<"-"<<data.E_EGRET[i_E_EGRET+1]<<" MeV"<<endl;




  cout<<"           ========fit to total using TGraph    ======================================"<<endl;
  //  pred_obs->Fit("pol1"    );

  fitfunction1  =new TF1("f1","[0] + x");     // only background free, parameter 0
  fitfunction2  =new TF1("f2","[0] + [1]*x"); // scaling and      background free, parameters 0, 1

  if(galplotdef.fit_function_EGRET==1 ||galplotdef.fit_function_EGRET==3)
   pred_obs->Fit("f1"      );
  if(galplotdef.fit_function_EGRET==2 ||galplotdef.fit_function_EGRET==3)
   pred_obs->Fit("f2"      );



  // fits using Minuit

  x     =new double[n_values_rebin];
  y     =new double[n_values_rebin];
  z     =new double[n_values_rebin];
  errorz=new double[n_values_rebin];

  double dummy; // since pred_IC_obs, pred_gas_obs  do not have second value ("obs") set

  Double_t arglist[10];
  Int_t ierflg = 0;
  static Double_t vstart[4] = {0, 0.1 , 0.1 };
  static Double_t step[4] = {1.e-8 , 0.01 , 0.01};
  Double_t amin,edm,errdef;
  Int_t nvpar,nparx,icstat;


  // fit to total        

  cout<<"            =======fit to total      using TMinuit======================================"<<endl;



  for (ivalues=0;ivalues<n_values_rebin;ivalues++)
    {
      pred_obs    ->GetPoint  (ivalues,x[ivalues],dummy);
   
      pred_obs    ->GetPoint  (ivalues, dummy,    z[ivalues]  );
     errorz[ivalues] = 
      pred_obs    ->GetErrorY (ivalues );

     // cout<<" x y z errorz :"<<x[ivalues]<<" "<<y[ivalues]<<" "<<z[ivalues]<<" "<<errorz[ivalues]<<endl;
    }

  n_values_fcn=n_values_rebin;
  npar=2;
  gMinuit = new TMinuit(npar);  //initialize TMinuit with a maximum of npar params
  gMinuit->SetFCN(fcn2);



   arglist[0] = 1;
   gMinuit->mnexcm("SET ERR", arglist ,1,ierflg);

// Set starting values and step sizes for parameters

   gMinuit->mnparm(0, "background2", vstart[0], step[0], 0,0,ierflg);
   gMinuit->mnparm(1, "total",       vstart[1], step[1], 0,0,ierflg);
 
  

// Now ready for minimization step
   arglist[0] = 500;  //maxcalls
   arglist[1] = 0.1;  //tolerance
   gMinuit->mnexcm("MIGRAD", arglist ,2,ierflg);

// Print results

   gMinuit->mnstat(amin,edm,errdef,nvpar,nparx,icstat);
   gMinuit->mnprin(3,amin);


// get fitted parameters
   par      =new double[npar];
   par_error=new double[npar];
   for(ipar=0;ipar<npar;ipar++)gMinuit->GetParameter(ipar,par[ipar],par_error[ipar]);
   double background2=par[0]; // for pred/obs line in plot

   background_spectrum         [i_E_EGRET]           =    par      [0];
   background_spectrum_errorbar[i_E_EGRET]           =    par_error[0];



// plot pred/obs for fitted parameters, but with background=0
   par[0]=0;
   pred_total_obs =new TGraphErrors(n_values_rebin);
   for (ivalues=0;ivalues<n_values_rebin;ivalues++)
    {
      pred_obs       ->GetPoint     (ivalues,dummy,obs);
      pred_total_obs ->SetPoint     (ivalues,func2(x[ivalues],           par),obs);
      pred_total_obs ->SetPointError(ivalues,0.0, pred_obs->GetErrorY(ivalues));
    }
  

 
  // compute chisq for input model with fitted background//AWS20040413

  par[0]=   background_spectrum         [i_E_EGRET]   ;
  gin=new double[npar];
  fcn3(npar, gin, f, par,  iflag);
  chisq_fitted+=f;
  data_points_cumulative+=n_values_fcn; //AWS20040420
  cout<<data.E_EGRET[i_E_EGRET]<<"-"<<data.E_EGRET[i_E_EGRET+1]<<" MeV: chi^2 for fitted model with par[1]="<<par[1]
      <<" : "<<f<<" #data points="<<n_values_fcn
      <<"    cumulative: "<<chisq_fitted<<" #data points="<<data_points_cumulative<<endl;

 
  par[1]=1.0;
  gin=new double[npar];
  fcn3(npar, gin, f, par,  iflag);
  chisq_input+=f;
  cout<<data.E_EGRET[i_E_EGRET]<<"-"<<data.E_EGRET[i_E_EGRET+1]<<" MeV: chi^2 for input  model with par[1]="<<par[1]
      <<"        : "<<f<<" #data points="<<n_values_fcn
      <<"    cumulative: "<<chisq_input <<" #data points="<<data_points_cumulative<<endl;



  // fit to IC and gas 

  cout<<"            =======fit to IC and gas using TMinuit======================================"<<endl;



  for (ivalues=0;ivalues<n_values_rebin;ivalues++)
    {
      pred_IC_obs ->GetPoint  (ivalues,x[ivalues],dummy);
      pred_gas_obs->GetPoint  (ivalues,y[ivalues],dummy);
      pred_obs    ->GetPoint  (ivalues, dummy,    z[ivalues]  );
     errorz[ivalues] = 
      pred_obs    ->GetErrorY (ivalues );

     // cout<<" x y z errorz :"<<x[ivalues]<<" "<<y[ivalues]<<" "<<z[ivalues]<<" "<<errorz[ivalues]<<endl;
    }

  n_values_fcn=n_values_rebin;
  npar=3;
  gMinuit = new TMinuit(npar);  //initialize TMinuit with a maximum of npar params
  gMinuit->SetFCN(fcn3);



   arglist[0] = 1;
   gMinuit->mnexcm("SET ERR", arglist ,1,ierflg);

// Set starting values and step sizes for parameters

   gMinuit->mnparm(0, "background3", vstart[0], step[0], 0,0,ierflg);
   gMinuit->mnparm(1, "IC",          vstart[1], step[1], 0,0,ierflg);
   gMinuit->mnparm(2, "gas",         vstart[2], step[2], 0,0,ierflg);
  

// Now ready for minimization step
   arglist[0] = 500;  //maxcalls
   arglist[1] = 0.1;  //tolerance
   gMinuit->mnexcm("MIGRAD", arglist ,2,ierflg);

// Print results

   gMinuit->mnstat(amin,edm,errdef,nvpar,nparx,icstat);
   gMinuit->mnprin(3,amin);


// get fitted parameters
   par      =new double[npar];
   par_error=new double[npar];
   for(ipar=0;ipar<npar;ipar++)gMinuit->GetParameter(ipar,par[ipar],par_error[ipar]);
   double background3=par[0]; // for pred/obs line in plot

// plot pred/obs for fitted parameters, but with background=0
   par[0]=0;
   pred_IC_gas_obs=new TGraphErrors(n_values_rebin);
   for (ivalues=0;ivalues<n_values_rebin;ivalues++)
    {
      pred_obs       ->GetPoint     (ivalues,dummy,obs);
      pred_IC_gas_obs->SetPoint     (ivalues,func3(x[ivalues],y[ivalues],par),obs);
      pred_IC_gas_obs->SetPointError(ivalues,0.0, pred_obs->GetErrorY(ivalues));
    }
  

  cout<<"=========================================================================="<<endl;



int ncolors=1; int *colors=0;
gStyle->SetPalette(ncolors,colors);

  
 

  //====== see HowTo Style: and do before creating canvas
 
 
   TStyle *plain  = new TStyle("Plain","Plain Style (no colors/fill areas)"); 

   
   plain->SetCanvasBorderMode(0);
   plain->SetPadBorderMode(0);
   plain->SetPadColor(0);
   plain->SetCanvasColor(0);
   plain->SetTitleColor(0);
   plain->SetStatColor(0);
   gROOT->SetStyle("Plain");

   int fitted; // 1=fitted by TGraph to total 2=fitted by TMinuit to total 3=fitted by TMinuit to IC+gas

   for (fitted=1;fitted<=3;fitted++ )
   {
     if(fitted==2)   pred_obs=pred_total_obs; 
     if(fitted==3)   pred_obs=pred_IC_gas_obs;

  sprintf(name,"fit_EGRET%d_%d",i_E_EGRET,fitted);
  TCanvas *c1=new TCanvas(name,name,100+i_E_EGRET*20,100+i_E_EGRET*10,600,600);

  TGaxis::SetMaxDigits(3); // for well-labelled axes
  c1->DrawFrame(0.0,0.0,maximum_intensity*1.1,maximum_intensity*1.1);
  
  //AFTER DrawFrame or Draw() (else crash)
  pred_obs->GetHistogram()->SetXTitle("predicted intensity, cm^{-2} sr^{-1} s^{-1} ");
  pred_obs->GetHistogram()->SetYTitle("observed  intensity, cm^{-2} sr^{-1} s^{-1} ");
  pred_obs->GetHistogram()->SetLabelOffset(+0.00,"X"); //  +ve down
  pred_obs->GetHistogram()->SetLabelSize  ( 0.03,"Y"); // labels, not titles
  pred_obs->GetHistogram()->SetLabelOffset(+0.00,"X"); //  +ve down
  pred_obs->GetHistogram()->SetLabelSize  ( 0.03,"X"); // labels, not titles

  pred_obs->GetHistogram()->SetTitleOffset(+0.85,"X");  //  +ve down
  pred_obs->GetHistogram()->SetTitleSize   (0.032,"X");
  pred_obs->GetHistogram()->SetTitleOffset(+1.5,"Y");  //  +ve to left
  pred_obs->GetHistogram()->SetTitleSize   (0.032,"Y");

  pred_obs->SetMarkerColor(kBlue);
  pred_obs->SetMarkerStyle(21); 
  pred_obs->SetMarkerStyle(22); // triangles
  pred_obs ->SetMarkerSize (0.5);// 
  pred_obs ->SetLineColor(kBlue);
  pred_obs ->SetLineWidth(1     );
  pred_obs ->SetLineStyle(1      );
  pred_obs ->SetMarkerStyle(22); // triangles
  pred_obs ->SetMarkerSize (0.5);// 
                                                 

  pred_obs ->Draw(" P"); // axes and markers

  if(fitted==2)
    {
      // this case the fit is not in the TGraph so must draw line obs=pred+background
  TGraph *line=new TGraph(2);
  line->SetPoint(0,0.0,    background2);
  line->SetPoint(1,1.0,1.0+background2);
  line->Draw("L");
    }


  if(fitted==3)
    {
      // this case the fit is not in the TGraph so must draw line obs=pred+background
  TGraph *line=new TGraph(2);
  line->SetPoint(0,0.0,    background3);
  line->SetPoint(1,1.0,1.0+background3);
  line->Draw("L");
    }


  sprintf(workstring1,"  %5.1f<l<%5.1f , %5.1f<l<%5.1f",
                          galplotdef.long_min1,galplotdef.long_max1,
                          galplotdef.long_min2,galplotdef.long_max2);
 
  sprintf(workstring2,"  %5.1f<b<%5.1f , %5.1f<b<%5.1f",
                         galplotdef.lat_min1,galplotdef.lat_max1,
                         galplotdef.lat_min2,galplotdef.lat_max2);
  
  sprintf(workstring3," %5.0f - %5.0f MeV",data.E_EGRET[i_E_EGRET],data.E_EGRET[i_E_EGRET+1]);


  strcpy(workstring4," galdef ID ");
  strcat(workstring4,galdef.galdef_ID);


  TText *text=new TText();
  text->SetTextFont(62);
  text->SetTextColor(kBlack);
  text->SetTextSize(0.025 );
  text->SetTextAlign(12);


  text->DrawTextNDC(.13 ,.82 ,workstring1);// NDC=normalized coord system
  text->DrawTextNDC(.13 ,.85 ,workstring2);// NDC=normalized coord system
  text->DrawTextNDC(.13 ,.88 ,workstring3);// NDC=normalized coord system
  text->DrawTextNDC(.20 ,.92 ,workstring4);// NDC=normalized coord system


 



  //============== postscript output

  sprintf(workstring1,"l_%.1f_%.1f_%.1f_%.1f",
                          galplotdef.long_min1,galplotdef.long_max1,
                          galplotdef.long_min2,galplotdef.long_max2);

  sprintf(workstring2,"b_%.1f_%.1f_%.1f_%.1f",
                          galplotdef.lat_min1,galplotdef.lat_max1,
                          galplotdef.lat_min2,galplotdef.lat_max2);

  sprintf(workstring3,"%.0f-%.0f_MeV",data.E_EGRET[i_E_EGRET],data.E_EGRET[i_E_EGRET+1]);

  strcpy(psfile,"plots/");
  strcat(psfile,"gamma_pred_obs_");
  strcat(psfile,galdef.galdef_ID);
  strcat(psfile,"_"        );
  strcat(psfile,workstring3);
  strcat(psfile,"_"        );
  strcat(psfile,workstring1);
  strcat(psfile,"_"        );
  strcat(psfile,workstring2);

  if(galplotdef.convolve_EGRET==0)
  strcat(psfile,"_unconv_"  );
  if(galplotdef.convolve_EGRET!=0)
  strcat(psfile,"_conv_"    );

  if(fitted==1)strcat(psfile,"fitted_total_TGraph_");
  if(fitted==2)strcat(psfile,"fitted_total_" );
  if(fitted==3)strcat(psfile,"fitted_IC_gas_");

  strcat(psfile,galplotdef.psfile_tag);

 

  strcat(psfile,".eps");

  if(options==1)//AWS20040420
  {
  cout<<"postscript file="<<psfile<<endl;
  c1->Print(psfile       );
  }

   }//if fitted
  //==============



 	}//i_E_EGRET


       //=== print differential and integral background spectra

          for(i_E_EGRET=0; i_E_EGRET<data.n_E_EGRET                    ; i_E_EGRET++)
        {
	  cout<<data.E_EGRET[i_E_EGRET]<<"-"<<data.E_EGRET[i_E_EGRET+1]<<" MeV: differential background spectrum= "
          <<background_spectrum[i_E_EGRET]<<" +-"<<background_spectrum_errorbar[i_E_EGRET]  <<endl;
	}

          for(i_E_EGRET=0;          i_E_EGRET<data.n_E_EGRET          ;  i_E_EGRET++)
        {
	  background_integral_spectrum         [i_E_EGRET]=0.;
          background_integral_spectrum_errorbar[i_E_EGRET]=0.;

          int ii_E_EGRET;
          for(ii_E_EGRET=i_E_EGRET; ii_E_EGRET<data.n_E_EGRET         ; ii_E_EGRET++)
	  {
           background_integral_spectrum         [i_E_EGRET] +=    background_spectrum         [ii_E_EGRET] ;
           background_integral_spectrum_errorbar[i_E_EGRET] +=pow(background_spectrum_errorbar[ii_E_EGRET],2);
          }

	  background_integral_spectrum_errorbar[i_E_EGRET]=sqrt(background_integral_spectrum_errorbar[i_E_EGRET]);

	  cout<<data.E_EGRET[i_E_EGRET]<<" MeV:    integral  background spectrum= "
          <<background_integral_spectrum[i_E_EGRET]<<" +-"<<background_integral_spectrum_errorbar[i_E_EGRET]  <<endl;
	}



   cout<<" <<<< fit_EGRET   "<<endl;
   return status;
}
 
//______________________________________________________________________________
static double func2(double x,          double *par)
{
  double value=par[0] + par[1]*x           ;

 return value;
}
 
//______________________________________________________________________________
static double func3(double x,double y, double *par)
{
  double value=par[0] + par[1]*x + par[2]*y;

 return value;
}

//______________________________________________________________________________
static void fcn2(int &npar, double *gin, double &f, double *par, int iflag)
{
  //gin=optional input gradients

  
   int i;

//calculate chisquare
   double chisq = 0;
   double delta;
   for (i=0;i<n_values_fcn; i++) {
     delta  = (z[i]-func2(x[i],     par))/errorz[i];
     chisq += delta*delta;
   }
   f = chisq;
}
//______________________________________________________________________________
static void fcn3(int &npar, double *gin, double &f, double *par, int iflag)
{
  //gin=optional input gradients

  
   int i;

//calculate chisquare
   double chisq = 0;
   double delta;
   for (i=0;i<n_values_fcn; i++) {
     delta  = (z[i]-func3(x[i],y[i],par))/errorz[i];
     chisq += delta*delta;
   }
   f = chisq;
}
