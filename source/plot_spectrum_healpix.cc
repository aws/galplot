
#include"Galplot.h"                    
#include"galprop_classes.h"
#include"galprop.h"
#include"constants.h"
#include"fitsio.h"

#include"Isotropic.h"    //AWS20081119




int Galplot::plot_spectrum_healpix(int mode) 
{

  cout<<" >>>> plot_spectrum_healpix   "<<endl;
  int status=0;

  int i_comp,i_lat,i_long,ip;
  int ii_long,ii_lat;
  int il_min, il_max, ib_min, ib_max;
  int il_min1,il_max1,ib_min1,ib_max1;
  int il_min2,il_max2,ib_min2,ib_max2;
  double long_min;
  double l,b,l_off;
  double ic_total;
  double ic_aniso_total;                                                                   //AWS20060511
  char name[100],canvastitle[100], workstring1[100],workstring2[100];
  char  psfile[400];
  char giffile[400];
  char txtfile[400];                               //AWS20041118
  char iosfile[400];                               //AWS20052411
  
  TCanvas *c1;

  
  TGraph *spectrum;
  TGraph *spectrum_ic, *spectrum_ic_optical, *spectrum_ic_ir, *spectrum_ic_cmb, *spectrum_bremss, *spectrum_pi0, *spectrum_isotropic, *spectrum_total;//AWS20041117
  TGraph *spectrum_ic_aniso;                                                                 //AWS20060511   
  TGraph *spectrum_sourcepop , *spectrum_sourcepop_sublimit,*spectrum_sourcepop_soplimit,
         *spectrum_total_with_sourcepop_sublimit,*spectrum_total_with_sourcepop_sublimit_with_sources ;    //AWS20090904
  TGraph *spectrum_sources, *spectrum_total_with_sources;                                    //AWS20081212
  TGraph *spectrum_solar_IC, *spectrum_solar_disk;                                           //AWS20100215
 
  TH1D *profile;
  TH1D *profile11,*profile12,*profile21,*profile22; // four (l,b) intervals
  TText *text;
  TLatex *latex;                                   //AWS20041021
  
  TH1F *h;                                         //AWS20040413
  

  double solid_angle;
  double sa_norm_11,   sa_norm_12,   sa_norm_21,   sa_norm_22;
  double x,y,y1,y2;
 

  valarray<double>         intensity_pi0       ( galaxy.pi0_decay_hp_skymap.nSpectra() );
  valarray<double>         intensity_bremss    ( galaxy.pi0_decay_hp_skymap.nSpectra() );
  valarray<double>         intensity_ic        ( galaxy.pi0_decay_hp_skymap.nSpectra() );
  valarray<double>         intensity_ic_optical( galaxy.pi0_decay_hp_skymap.nSpectra() );
  valarray<double>         intensity_ic_ir     ( galaxy.pi0_decay_hp_skymap.nSpectra() );
  valarray<double>         intensity_ic_cmb    ( galaxy.pi0_decay_hp_skymap.nSpectra() );
  valarray<double>         intensity_isotropic ( galaxy.pi0_decay_hp_skymap.nSpectra() ); //AWS20081117
  valarray<double>         intensity_total     ( galaxy.pi0_decay_hp_skymap.nSpectra() );
  valarray<double>         intensity_sources   ( galaxy.pi0_decay_hp_skymap.nSpectra() ); //AWS20081212
  valarray<double>         intensity_total_with_sources
                                               ( galaxy.pi0_decay_hp_skymap.nSpectra() ); //AWS20081212

  valarray<double>         intensity_sourcepop ( galaxy.pi0_decay_hp_skymap.nSpectra() ); //AWS20090108
  valarray<double>         intensity_sourcepop_sublimit
                                               ( galaxy.pi0_decay_hp_skymap.nSpectra() ); //AWS20090108
  valarray<double>         intensity_sourcepop_soplimit
                                               ( galaxy.pi0_decay_hp_skymap.nSpectra() ); //AWS20090108

  valarray<double>         intensity_total_with_sourcepop_sublimit
                                               ( galaxy.pi0_decay_hp_skymap.nSpectra() ); //AWS20090109

  valarray<double>         intensity_total_with_sourcepop_sublimit_with_sources
                                               ( galaxy.pi0_decay_hp_skymap.nSpectra() ); //AWS20090904


  valarray<double>         intensity_solar_IC  ( galaxy.pi0_decay_hp_skymap.nSpectra() ); //AWS20102015
  valarray<double>         intensity_solar_disk( galaxy.pi0_decay_hp_skymap.nSpectra() ); //AWS20102015

  valarray<double>         number_of_pixels    ( galaxy.pi0_decay_hp_skymap.nSpectra() );  
  valarray<double>         isotropic_intensity ( galaxy.pi0_decay_hp_skymap.nSpectra() ); //AWS20150215

  Isotropic isotropic;                                                    //AWS20081119
 

  int ipix, select;

  double pi= acos(-1.0);
  double rtd=180./pi;        // rad to deg
  double dtr=pi / 180.;      // deg to rad


  int ncolors=1; int *colors=0;
  gStyle->SetPalette(ncolors,colors);
 
  // define line styles for SetLineStyle (only 1-4 predefined at root 5.08)       AWS20071005
  gStyle->SetLineStyleString(1," ");
  gStyle->SetLineStyleString(2,"12 12");
  gStyle->SetLineStyleString(3,"4 8");
  gStyle->SetLineStyleString(4,"12 16 4 16");
  gStyle->SetLineStyleString(5,"20 12 4 12");
  gStyle->SetLineStyleString(6,"20 12 4 12 4 12 4 12");
  gStyle->SetLineStyleString(7,"20 20");
  gStyle->SetLineStyleString(8,"20 12 4 12 4 12");
  gStyle->SetLineStyleString(9,"80 20");
  gStyle->SetLineStyleString(10,"80 40 4 40");



  
  
  //  strcpy(canvastitle," galdef ID ");
  //  strcat(canvastitle,galdef.galdef_ID);

 
  /*
  galaxy.pi0_decay_hp_skymap.print(cout);
  galaxy.bremss_hp_skymap   .print(cout);
  galaxy.IC_iso_hp_skymap[0] .print(cout);
  */

  intensity_pi0          = 0.0;
  intensity_bremss       = 0.0;
  intensity_ic           = 0.0;
  intensity_ic_optical   = 0.0;
  intensity_ic_ir        = 0.0;
  intensity_ic_cmb       = 0.0;
  intensity_total        = 0.0;
  intensity_sources      = 0.0;       //AWS20081212
  intensity_total_with_sources = 0.0; //AWS20081212
  intensity_sourcepop    = 0.0;       //AWS20090108
  intensity_sourcepop_sublimit = 0.0; //AWS20090108
  intensity_sourcepop_soplimit = 0.0; //AWS20090108
  intensity_solar_IC     = 0.0;       //AWS20100215
  intensity_solar_disk   = 0.0;       //AWS20100215
  number_of_pixels       = 0.0;


   // isotropic component
  if (galplotdef.isotropic_type == 2) isotropic.read(configure.fits_directory,galplotdef.isotropic_bgd_file); 

  for( ip=0;ip<  galaxy.pi0_decay_hp_skymap.nSpectra() ;ip++)  //AWS20150211
  {
    if (galplotdef.isotropic_type == 1)     isotropic_intensity[ip] = galplotdef.isotropic_const*pow(galaxy.E_gamma[ip],  - galplotdef.isotropic_g);

    if (galplotdef.isotropic_type == 2)     isotropic_intensity[ip] = isotropic.interpolated(galaxy.E_gamma[ip] , 1); 
           
    cout<<"plot_spectrum_healpix: isotropic: E_gamma="<<galaxy.E_gamma[ip]<<" isotropic_intensity[ip]=" <<isotropic_intensity[ip]<<endl;
  } 


  // apply energy dispersion if required AWS20140724

 
 if( galplotdef.energy_disp_FermiLAT==1)
 {
  cout<<"plot_spectrum_healpix: applying energy dispersion"<<endl;

  int                           Skymap_Energy_Dispersion_debug=0; 
  if(galplotdef.verbose==-2400) Skymap_Energy_Dispersion_debug=1;  // 1=print spectra before and after dispersion, and details of matrix calculation
  int interpolation=1;
  double   E_interp_factor = galplotdef.energy_disp_factor;

  string exposure_file_name =  string(configure.fits_directory) + string(galplotdef.GLAST_exposure_file); //AWS20150211

  std::size_t found = exposure_file_name.find("37m_pass7.7");
  if (found!=std::string::npos)
  {
   std::cout << "exposure_file_name in galplotdef="<<exposure_file_name<<": substring 37m_pass7.7 found at: " << found <<": this file does not have column format!"<< endl;
   exposure_file_name = "/afs/ipp-garching.mpg.de/home/a/aws/propagate/c/FITS/GLAST/p8_thibaut/exp2_healpix7_andy.fits"; // for testing, case where data does not have column format
   cout<<"replacing exposure_file_name with colum format exposure : "<<exposure_file_name<<endl;
  }

  string exposure_file_type = "healpix";                       //AWS20150211

  //  string parameter_file_name = string(configure.fits_directory) + "/GLAST/p8_thibaut/edisp_P8V1_ULTRACLEAN_EDISP3.fits";              //AWS20150225
  //  string parameter_file_type = "ST_FITS";                                                                                             //AWS20150225

  string parameter_file_name = string(configure.fits_directory) + string(galplotdef.Fermi_edisp_file);      //AWS20150226
  string parameter_file_type =                                    string(galplotdef.Fermi_edisp_file_type); //AWS20150226

  int    use_Aeff         = galplotdef.Fermi_edisp_use_Aeff;         //AWS20150301
  double E_true_threshold = galplotdef.Fermi_edisp_E_true_threshold; //AWS20150301
  double E_meas_threshold = galplotdef.Fermi_edisp_E_meas_threshold; //AWS20150301

  Skymap_Energy_Dispersion( galaxy.pi0_decay_hp_skymap,    E_interp_factor, interpolation, parameter_file_name, parameter_file_type, exposure_file_name, exposure_file_type, use_Aeff, E_true_threshold, E_meas_threshold,  Skymap_Energy_Dispersion_debug );
  Skymap_Energy_Dispersion( galaxy.   bremss_hp_skymap,    E_interp_factor, interpolation, parameter_file_name, parameter_file_type, exposure_file_name, exposure_file_type, use_Aeff, E_true_threshold, E_meas_threshold,Skymap_Energy_Dispersion_debug );

  Skymap_Energy_Dispersion( galaxy.   IC_iso_hp_skymap[0], E_interp_factor, interpolation, parameter_file_name, parameter_file_type, exposure_file_name, exposure_file_type, use_Aeff, E_true_threshold, E_meas_threshold,Skymap_Energy_Dispersion_debug );
  Skymap_Energy_Dispersion( galaxy.   IC_iso_hp_skymap[1], E_interp_factor, interpolation, parameter_file_name, parameter_file_type, exposure_file_name, exposure_file_type, use_Aeff, E_true_threshold, E_meas_threshold,Skymap_Energy_Dispersion_debug );
  Skymap_Energy_Dispersion( galaxy.   IC_iso_hp_skymap[2], E_interp_factor, interpolation, parameter_file_name, parameter_file_type, exposure_file_name, exposure_file_type, use_Aeff, E_true_threshold, E_meas_threshold,Skymap_Energy_Dispersion_debug );


  Skymap_Energy_Dispersion(           unconvolved.GLAST_unconvolved_intensity_sources, E_interp_factor, interpolation, parameter_file_name, parameter_file_type, exposure_file_name, exposure_file_type, 
                                                                                                                                                                             use_Aeff, E_true_threshold, E_meas_threshold,Skymap_Energy_Dispersion_debug );

  
 if(galplotdef.sourcepop_ext_model!=2) // see below
 {
   //Skymap_Energy_Dispersion_debug=1; // for testing 

  Skymap_Energy_Dispersion(           sourcepop5.healpix_skymap_intensity_spectrum          , E_interp_factor, interpolation, parameter_file_name, parameter_file_type, exposure_file_name, exposure_file_type, use_Aeff, E_true_threshold, E_meas_threshold, Skymap_Energy_Dispersion_debug );
  Skymap_Energy_Dispersion(           sourcepop5.healpix_skymap_intensity_spectrum_sublimit , E_interp_factor, interpolation, parameter_file_name, parameter_file_type, exposure_file_name, exposure_file_type, use_Aeff, E_true_threshold, E_meas_threshold, Skymap_Energy_Dispersion_debug );
  Skymap_Energy_Dispersion(           sourcepop5.healpix_skymap_intensity_spectrum_soplimit , E_interp_factor, interpolation, parameter_file_name, parameter_file_type, exposure_file_name, exposure_file_type, use_Aeff, E_true_threshold, E_meas_threshold, Skymap_Energy_Dispersion_debug );
 }

 if(galplotdef.sourcepop_ext_model==2)  // see below
 {
  Skymap_Energy_Dispersion(           sourcepop2.healpix_skymap_intensity_spectrum          , E_interp_factor, interpolation, parameter_file_name, parameter_file_type, exposure_file_name, exposure_file_type, use_Aeff, E_true_threshold, E_meas_threshold, Skymap_Energy_Dispersion_debug );
  Skymap_Energy_Dispersion(           sourcepop2.healpix_skymap_intensity_spectrum_sublimit , E_interp_factor, interpolation, parameter_file_name, parameter_file_type, exposure_file_name, exposure_file_type, use_Aeff, E_true_threshold, E_meas_threshold, Skymap_Energy_Dispersion_debug );
  Skymap_Energy_Dispersion(           sourcepop2.healpix_skymap_intensity_spectrum_soplimit , E_interp_factor, interpolation, parameter_file_name, parameter_file_type, exposure_file_name, exposure_file_type, use_Aeff, E_true_threshold, E_meas_threshold, Skymap_Energy_Dispersion_debug );
 }
  

 // isotropic component
  valarray<double> E; E.resize( galaxy.pi0_decay_hp_skymap.nSpectra());
  for( ip=0;ip<  galaxy.pi0_decay_hp_skymap.nSpectra() ;ip++) E[ip]=galaxy.E_gamma[ip];

  spectrum_Energy_Dispersion( E, isotropic_intensity,                                         E_interp_factor, interpolation, parameter_file_name, parameter_file_type, exposure_file_name, exposure_file_type, use_Aeff, E_true_threshold, E_meas_threshold, Skymap_Energy_Dispersion_debug );

 } // energy dispersion




 //////////////////////////////////////////////////////////////////

 cout<<"plot_spectrum_healpix: starting loop over pixels"<<endl; //AWS20091208

 for (ipix=0;ipix< galaxy.pi0_decay_hp_skymap.Npix() ;ipix++)
   {
    l =        galaxy.pi0_decay_hp_skymap.pix2ang(ipix).phi   * rtd;
    b = 90.0 - galaxy.pi0_decay_hp_skymap.pix2ang(ipix).theta * rtd;

 if(galplotdef.verbose==-2301)
 {
    for( ip=0;ip<  galaxy.pi0_decay_hp_skymap.nSpectra() ;ip++)
      {
        cout<<"ipix="<<ipix<<" l = "<<l<<" b ="<<b<<" ip="<<ip<<" E_gamma[ip]="<<galaxy.E_gamma[ip]
            <<" galaxy.pi0_decay_hp_skymap[ipix][ip]="                     <<galaxy.pi0_decay_hp_skymap[ipix][ip]
	    <<" sourcepop5.healpix_skymap_intensity_spectrum[ipix][ip]= "   <<  sourcepop5.healpix_skymap_intensity_spectrum[ipix][ip]
            <<" unconvolved.GLAST_unconvolved_intensity_sources[ipix][ip]="<<unconvolved.GLAST_unconvolved_intensity_sources[ipix][ip]  <<endl; //AWS20131123
      }
 }

 if(galplotdef.verbose==-2302) cout<<"ipix="<<ipix<<" l = "<<l<<" b ="<<b<<endl; //AWS20131123

     select=0;
    // select if pixel lies in one of the 4 regions
    if(l>=galplotdef.long_min1 && l<=galplotdef.long_max1 && b>=galplotdef.lat_min1 && b<=galplotdef.lat_max1 ) select=1;
    if(l>=galplotdef.long_min2 && l<=galplotdef.long_max2 && b>=galplotdef.lat_min1 && b<=galplotdef.lat_max1 ) select=1;
    if(l>=galplotdef.long_min1 && l<=galplotdef.long_max1 && b>=galplotdef.lat_min2 && b<=galplotdef.lat_max2 ) select=1;
    if(l>=galplotdef.long_min2 && l<=galplotdef.long_max2 && b>=galplotdef.lat_min2 && b<=galplotdef.lat_max2 ) select=1;

    if (select==1)
    {
     for( ip=0;ip<  galaxy.pi0_decay_hp_skymap.nSpectra() ;ip++)
     {
        
       intensity_pi0       [ip] +=    galaxy.pi0_decay_hp_skymap[ipix][ip];
       intensity_bremss    [ip] +=    galaxy.bremss_hp_skymap   [ipix][ip];

       intensity_ic        [ip] +=   (galaxy.IC_iso_hp_skymap[0][ipix][ip] +  galaxy.IC_iso_hp_skymap[1][ipix][ip] +  galaxy.IC_iso_hp_skymap[2][ipix][ip]);
       intensity_ic_optical[ip] +=    galaxy.IC_iso_hp_skymap[0][ipix][ip]                                                                                 ;
       intensity_ic_ir     [ip] +=                                            galaxy.IC_iso_hp_skymap[1][ipix][ip]                                         ;
       intensity_ic_cmb    [ip] +=                                                                                    galaxy.IC_iso_hp_skymap[2][ipix][ip] ;
 
       intensity_sources   [ip] +=    unconvolved.GLAST_unconvolved_intensity_sources[ipix][ip]; //AWS20081212

      if(galplotdef.sourcepop_ext_model!=2) //AWS20120524
      {
        if(galplotdef.verbose==-2303)  
	  {cout<<"debug ip="<<ip<< " intensity_sourcepop="<<intensity_sourcepop[ip]<< endl;cout<<  sourcepop5.healpix_skymap_intensity_spectrum[ipix][ip]<<endl;}//AWS20131123

       intensity_sourcepop [ip] +=    sourcepop5.healpix_skymap_intensity_spectrum            [ipix][ip]; //AWS20110818 was sourcepop1
       intensity_sourcepop_sublimit
                           [ip] +=    sourcepop5.healpix_skymap_intensity_spectrum_sublimit   [ipix][ip]; //AWS20110818 was sourcepop1
       intensity_sourcepop_soplimit
                           [ip] +=    sourcepop5.healpix_skymap_intensity_spectrum_soplimit   [ipix][ip]; //AWS20110818 was sourcepop1
      }

      if(galplotdef.sourcepop_ext_model==2) //AWS20120524 MSPs
      {
       intensity_sourcepop [ip] +=    sourcepop2.healpix_skymap_intensity_spectrum            [ipix][ip]; //AWS20110818 was sourcepop1
       intensity_sourcepop_sublimit
                           [ip] +=    sourcepop2.healpix_skymap_intensity_spectrum_sublimit   [ipix][ip]; //AWS20110818 was sourcepop1
       intensity_sourcepop_soplimit
                           [ip] +=    sourcepop2.healpix_skymap_intensity_spectrum_soplimit   [ipix][ip]; //AWS20110818 was sourcepop1
      }


       intensity_solar_IC  [ip] +=    unconvolved.GLAST_unconvolved_intensity_solar_IC        [ipix][ip]; //AWS20100215

       number_of_pixels    [ip]++;

     } //i_E_GLAST
    } // if select

   } // ipix

   cout<<"plot_spectrum_healpix: completed loop over pixels"<<endl; //AWS20091208

   intensity_total =  intensity_pi0 +  intensity_bremss + intensity_ic;

   intensity_total_with_sources                          = intensity_total                               + intensity_sources;  //AWS20081212

   intensity_total_with_sourcepop_sublimit               = intensity_total + intensity_sourcepop_sublimit; //AWS20090109

   intensity_total_with_sourcepop_sublimit_with_sources = intensity_total + intensity_sourcepop_sublimit + intensity_sources; //AWS20090904

 cout<<"plot_spectrum_healpix: starting loop over ip"<<endl; //AWS20091208

    for( ip=0;ip<  galaxy.pi0_decay_hp_skymap.nSpectra() ;ip++)
    {
     intensity_pi0       [ip]/=number_of_pixels[ip];
     intensity_bremss    [ip]/=number_of_pixels[ip];
     intensity_ic        [ip]/=number_of_pixels[ip];
     intensity_ic_optical[ip]/=number_of_pixels[ip];
     intensity_ic_ir     [ip]/=number_of_pixels[ip];
     intensity_ic_cmb    [ip]/=number_of_pixels[ip];


     intensity_total     [ip]/=number_of_pixels[ip];

     intensity_sources   [ip]/=number_of_pixels[ip];                   //AWS20081212
     intensity_total_with_sources
                         [ip]/=number_of_pixels[ip];                   //AWS20081212

     intensity_sourcepop [ip]/=number_of_pixels[ip];                   //AWS20090108
     intensity_sourcepop_sublimit
                         [ip]/=number_of_pixels[ip];                   //AWS20090108
     intensity_sourcepop_soplimit
                         [ip]/=number_of_pixels[ip];                   //AWS20090108

     intensity_total_with_sourcepop_sublimit
                         [ip]/=number_of_pixels[ip];                   //AWS20090828

     intensity_total_with_sourcepop_sublimit_with_sources
                         [ip]/=number_of_pixels[ip];                   //AWS20090904

     intensity_solar_IC  [ip]/=number_of_pixels[ip];                   //AWS20100215



    }

 


   for( ip=0;ip<  galaxy.pi0_decay_hp_skymap.nSpectra() ;ip++) // AWS20150215 separate from isotropic calculation
   {
    intensity_total                        [ip] += isotropic_intensity [ip] ;       //AWS20080804 AWS20150215

    intensity_isotropic                    [ip]  = isotropic_intensity [ip];        //AWS20081117 AWS20150215

    intensity_total_with_sources           [ip] += isotropic_intensity [ip];        //AWS20081212 AWS20150215

    intensity_total_with_sourcepop_sublimit[ip] += isotropic_intensity [ip];        //AWS20090109 AWS20150215

    intensity_total_with_sourcepop_sublimit_with_sources[ip]
                                                += isotropic_intensity [ip];        //AWS20090904 AWS20150215


  }


 cout<<"plot_spectrum_healpix: completed loop over ip"<<endl; //AWS20091208

    // NB unlike (l,b) maps the healpix files are not multiplied by E^2 in galprop, so do it before plotting

    for( ip=0;ip<  galaxy.pi0_decay_hp_skymap.nSpectra() ;ip++)
      {cout<<"plot_spectrum_healpix: E="<<galaxy.E_gamma[ip]<<" intensity_total="<< intensity_total[ip]<<" number of pixels="<<number_of_pixels[ip]<<endl;}

    







 
        
     spectrum           =new TGraph(galaxy.n_E_gammagrid);
     spectrum_total     =new TGraph(galaxy.n_E_gammagrid);                                                                  //AWS20081117
     spectrum_pi0       =new TGraph(galaxy.n_E_gammagrid);
     spectrum_bremss    =new TGraph(galaxy.n_E_gammagrid);
     spectrum_ic        =new TGraph(galaxy.n_E_gammagrid);
     spectrum_ic_optical=new TGraph(galaxy.n_E_gammagrid);
     spectrum_ic_ir     =new TGraph(galaxy.n_E_gammagrid);
     spectrum_ic_cmb    =new TGraph(galaxy.n_E_gammagrid);
     spectrum_isotropic =new TGraph(galaxy.n_E_gammagrid);                                                                  //AWS20081118
     spectrum_sources   =new TGraph(galaxy.n_E_gammagrid);                                                                  //AWS20081212
     spectrum_total_with_sources 
                        =new TGraph(galaxy.n_E_gammagrid);                                                                  //AWS20081212
     spectrum_sourcepop =new TGraph(galaxy.n_E_gammagrid);                                                                  //AWS20090108
     spectrum_sourcepop_sublimit
                        =new TGraph(galaxy.n_E_gammagrid);                                                                  //AWS20090108 
     spectrum_sourcepop_soplimit
                        =new TGraph(galaxy.n_E_gammagrid);                                                                  //AWS20090108 
     spectrum_total_with_sourcepop_sublimit
                        =new TGraph(galaxy.n_E_gammagrid);                                                                  //AWS20090108 
     spectrum_total_with_sourcepop_sublimit_with_sources
                        =new TGraph(galaxy.n_E_gammagrid);                                                                  //AWS20090904

     spectrum_solar_IC  =new TGraph(galaxy.n_E_gammagrid);                                                                  //AWS20100215
     spectrum_solar_disk=new TGraph(galaxy.n_E_gammagrid);                                                                  //AWS20100215

 cout<<"plot_spectrum_healpix: starting loop over energies"<<endl; //AWS20091208


      for  (ip    =0;  ip    <galaxy.n_E_gammagrid;ip    ++)
	{
	spectrum_pi0       ->SetPoint(ip,  galaxy.E_gamma[ip],      intensity_pi0       [ip] * pow(galaxy.E_gamma[ip],2.));
	spectrum_bremss    ->SetPoint(ip,  galaxy.E_gamma[ip],      intensity_bremss    [ip] * pow(galaxy.E_gamma[ip],2.));
	spectrum_ic        ->SetPoint(ip,  galaxy.E_gamma[ip],      intensity_ic        [ip] * pow(galaxy.E_gamma[ip],2.));
	spectrum_ic_optical->SetPoint(ip,  galaxy.E_gamma[ip],      intensity_ic_optical[ip] * pow(galaxy.E_gamma[ip],2.));
	spectrum_ic_ir     ->SetPoint(ip,  galaxy.E_gamma[ip],      intensity_ic_ir     [ip] * pow(galaxy.E_gamma[ip],2.));
	spectrum_ic_cmb    ->SetPoint(ip,  galaxy.E_gamma[ip],      intensity_ic_cmb    [ip] * pow(galaxy.E_gamma[ip],2.));

	spectrum           ->SetPoint(ip,  galaxy.E_gamma[ip],      intensity_total     [ip] * pow(galaxy.E_gamma[ip],2.));
	spectrum_total     ->SetPoint(ip,  galaxy.E_gamma[ip],      intensity_total     [ip] * pow(galaxy.E_gamma[ip],2.)); //AWS20081117
	spectrum_isotropic ->SetPoint(ip,  galaxy.E_gamma[ip],      intensity_isotropic [ip] * pow(galaxy.E_gamma[ip],2.)); //AWS20081118

	spectrum_sources   ->SetPoint(ip,  galaxy.E_gamma[ip],      intensity_sources   [ip] * pow(galaxy.E_gamma[ip],2.)); //AWS20081212
	spectrum_total_with_sources
                           ->SetPoint(ip,  galaxy.E_gamma[ip],      intensity_total_with_sources
                                                                                        [ip] * pow(galaxy.E_gamma[ip],2.)); //AWS20081212


        spectrum_sourcepop ->SetPoint(ip,  galaxy.E_gamma[ip],      intensity_sourcepop [ip] * pow(galaxy.E_gamma[ip],2.)); //AWS20090108
        spectrum_sourcepop_sublimit
                           ->SetPoint(ip,  galaxy.E_gamma[ip],      intensity_sourcepop_sublimit
                                                                                        [ip] * pow(galaxy.E_gamma[ip],2.)); //AWS20090108
         spectrum_sourcepop_soplimit
                           ->SetPoint(ip,  galaxy.E_gamma[ip],      intensity_sourcepop_soplimit
                                                                                        [ip] * pow(galaxy.E_gamma[ip],2.)); //AWS20090108                                         
         spectrum_total_with_sourcepop_sublimit
                           ->SetPoint(ip,  galaxy.E_gamma[ip],      intensity_total_with_sourcepop_sublimit
                                                                                        [ip] * pow(galaxy.E_gamma[ip],2.)); //AWS20090108 
         spectrum_total_with_sourcepop_sublimit_with_sources
                           ->SetPoint(ip,  galaxy.E_gamma[ip],      intensity_total_with_sourcepop_sublimit_with_sources
                                                                                        [ip] * pow(galaxy.E_gamma[ip],2.)); //AWS20090904

 	 spectrum_solar_IC ->SetPoint(ip,  galaxy.E_gamma[ip],      intensity_solar_IC  [ip] * pow(galaxy.E_gamma[ip],2.)); //AWS20100215

	}


 cout<<"plot_spectrum_healpix: completed loop over energies"<<endl; //AWS20091208

 // remove isotropic points which are zero to avoid on plot

 int npcut=0;
 for  (ip    =0;  ip    < spectrum_isotropic->GetN();ip    ++)
  {
    spectrum_isotropic->GetPoint(ip,x,y);
    if(y>0.) npcut++;
  }
  
 if(npcut>0)
 {

  TGraph *spectrum_isotropic_cut;
  spectrum_isotropic_cut= new TGraph(npcut);
  int ipcut=0;
  for  (ip    =0;  ip    < spectrum_isotropic->GetN();ip    ++)
   {
    spectrum_isotropic->GetPoint(ip,x,y);
    if(y>0.){spectrum_isotropic_cut->SetPoint(ipcut,x,y); ipcut++;}
   }
  spectrum_isotropic=spectrum_isotropic_cut;
 }
 

     spectrum->SetMarkerColor(kBlue);
     spectrum->SetMarkerStyle(21); 
     spectrum->SetMarkerStyle(22); // triangles
     spectrum->SetMarkerSize (0.5);// 
     spectrum->SetLineColor(kBlue);
     spectrum->SetLineWidth(3     );
     spectrum->SetLineStyle(1      );//1=solid 2=dash 3=dot 4=dash-dot
     spectrum->SetMarkerStyle(22); // triangles
     spectrum->SetMarkerSize (0.5);// 
        
     spectrum->Draw(" L ");  // P=points as markers  L=line AWS20091219 "L same"->"L" in all casese

     cout<<"plot_spectrum_healpix: after spectrum->Draw()"<<endl;

     spectrum_pi0->SetLineColor(kRed);
     spectrum_pi0->SetLineWidth(3     );
     spectrum_pi0->SetLineStyle(1      );//1=solid 2=dash 3=dot 4=dash-dot
     spectrum_pi0->Draw(" L ");

     spectrum_bremss->SetLineColor(kCyan);
     spectrum_bremss->SetLineWidth(3     );
     spectrum_bremss->SetLineStyle(1      );//1=solid 2=dash 3=dot 4=dash-dot
     spectrum_bremss->Draw(" L ");

     spectrum_ic->SetLineColor(kGreen);
     spectrum_ic->SetLineWidth(3     );
     spectrum_ic->SetLineStyle(1      );//1=solid 2=dash 3=dot 4=dash-dot
     spectrum_ic->Draw(" L ");


    if(galplotdef.gamma_IC_selectcomp>=1) //AWS20090612
    {

     spectrum_ic_optical->SetLineColor(kGreen);
     spectrum_ic_optical->SetLineWidth(1     );
     spectrum_ic_optical->SetLineStyle(9      );//1=solid 2=dash 3=dot 4=dash-dot 9=
     spectrum_ic_optical->Draw(" L ");


     spectrum_ic_ir->SetLineColor(kGreen);
     spectrum_ic_ir->SetLineWidth(1     );
     spectrum_ic_ir->SetLineStyle(2      );//1=solid 2=dash 3=dot 4=dash-dot
     spectrum_ic_ir->Draw(" L ");


     spectrum_ic_cmb->SetLineColor(kGreen);
     spectrum_ic_cmb->SetLineWidth(1     );
     spectrum_ic_cmb->SetLineStyle(3      );//1=solid 2=dash 3=dot 4=dash-dot
     spectrum_ic_cmb->Draw(" L ");

    }

     spectrum_isotropic->SetLineColor(kBlack);
     spectrum_isotropic->SetLineWidth(3     );
     spectrum_isotropic->SetLineStyle(1      );//1=solid 2=dash 3=dot 4=dash-dot
     spectrum_isotropic->Draw(" L ");


     spectrum_sources  ->SetLineColor(kMagenta);             //AWS20081212
     spectrum_sources  ->SetLineWidth(3     );
     spectrum_sources  ->SetLineStyle(4      );//1=solid 2=dash 3=dot 4=dash-dot
     spectrum_sources  ->Draw(" L ");
 

     spectrum_total_with_sources  ->SetLineColor(kMagenta);  //AWS20081212
     spectrum_total_with_sources  ->SetLineWidth(3     );
     spectrum_total_with_sources  ->SetLineStyle(4      );//1=solid 2=dash 3=dot 4=dash-dot
     spectrum_total_with_sources  ->Draw(" L ");

     if(galplotdef.sourcepop_total==2)                        //AWS20090115
     {
     spectrum_sourcepop ->SetLineColor(kOrange );             //AWS20090108
     spectrum_sourcepop ->SetLineWidth(6     );
     spectrum_sourcepop ->SetLineStyle(1      );//1=solid 2=dash 3=dot 4=dash-dot
     spectrum_sourcepop ->Draw(" L ");
     }

     if(galplotdef.sourcepop_sublimit==3  || galplotdef.sourcepop_sublimit==1)                              //AWS20090831
     {
     spectrum_sourcepop_sublimit ->SetLineColor(kOrange );             //AWS20090108
     if( galplotdef.sourcepop_sublimit==1)
     spectrum_sourcepop_sublimit ->SetLineColor(kBlue   );             //AWS20090831
     spectrum_sourcepop_sublimit ->SetLineWidth(3     );
     spectrum_sourcepop_sublimit ->SetLineStyle(3      );//1=solid 2=dash 3=dot 4=dash-dot
     spectrum_sourcepop_sublimit ->Draw(" L ");
     }

     if(galplotdef.sourcepop_soplimit==3 || galplotdef.sourcepop_soplimit==1)                              //AWS20090831
     {
     spectrum_sourcepop_soplimit ->SetLineColor(kOrange );             //AWS20090108
     if( galplotdef.sourcepop_soplimit==1)
     spectrum_sourcepop_soplimit ->SetLineColor(kBlue );        //AWS20090831
     spectrum_sourcepop_soplimit ->SetLineWidth(3     );
     spectrum_sourcepop_soplimit ->SetLineStyle(2      );//1=solid 2=dash 3=dot 4=dash-dot
     spectrum_sourcepop_soplimit ->Draw(" L ");
     }

     if(galplotdef.sourcepop_total==3    ||galplotdef.sourcepop_total==1  )                                 //AWS20090831
     {
     spectrum_total_with_sourcepop_sublimit ->SetLineColor(kOrange );             //AWS20090108
     if(galplotdef.sourcepop_total==1 )
     spectrum_total_with_sourcepop_sublimit ->SetLineColor(kBlue );   //AWS20090831
     spectrum_total_with_sourcepop_sublimit ->SetLineWidth(3     );
     spectrum_total_with_sourcepop_sublimit ->SetLineStyle(1      );//1=solid 2=dash 3=dot 4=dash-dot
     spectrum_total_with_sourcepop_sublimit ->Draw(" L ");
     }

     if(galplotdef.sourcepop_total==3    ||galplotdef.sourcepop_total==1  )                                 //AWS20090904
     {
     spectrum_total_with_sourcepop_sublimit_with_sources ->SetLineColor(kOrange );             //AWS20090108
     if(galplotdef.sourcepop_total==1 )
     spectrum_total_with_sourcepop_sublimit_with_sources ->SetLineColor(kBlue );   //AWS20090831
     spectrum_total_with_sourcepop_sublimit_with_sources ->SetLineWidth(3     );
     spectrum_total_with_sourcepop_sublimit_with_sources ->SetLineStyle(1      );//1=solid 2=dash 3=dot 4=dash-dot
     spectrum_total_with_sourcepop_sublimit_with_sources ->Draw(" L ");
     }


     spectrum_solar_IC ->SetLineColor(kBlack); //AWS20100215
     spectrum_solar_IC ->SetLineWidth(3     );
     spectrum_solar_IC ->SetLineStyle(2      );//1=solid 2=dash 3=dot 4=dash-dot
     //     spectrum_solar_IC ->Draw(" L "); //AWS20140825 removed since gives strange results, and needs revised input anyway

     //  cout<<txtfile<<endl;

    cout<<"plot_spectrum_healpix: after last spectrum...->Draw()"<<endl;

  if(galplotdef.gamma_spectrum==1 || galplotdef.gamma_spectrum==2) //AWS20050412
  {

  fprintf(txtFILE,"\n ==== Model spectra from Healpix skymaps ==== ");
  fprintf(txtFILE," galdef ID %s\n", galdef.galdef_ID);
  fprintf(txtFILE,"\n");
  fprintf(txtFILE,"%6.2f < l < %6.2f, ",galplotdef.long_min1,galplotdef.long_max1);
  fprintf(txtFILE,"%6.2f < l < %6.2f\n",galplotdef.long_min2,galplotdef.long_max2);
  fprintf(txtFILE,"%6.2f < b < %6.2f, ",galplotdef. lat_min1,galplotdef. lat_max1);
  fprintf(txtFILE,"%6.2f < b < %6.2f\n",galplotdef. lat_min2,galplotdef. lat_max2);
  fprintf(txtFILE,"\n");

  fprintf(txtFILE,"     E(MeV)        E^2 * intensity (MeV cm^-2 sr^-1 s^-1)     \n");
    if (galplotdef.gamma_IC_selectcomp==0)
  fprintf(txtFILE,"                pi0    bremss    IC   isotropic  total \n");
  if (galplotdef.gamma_IC_selectcomp)
  fprintf(txtFILE,"                pi0    bremss    IC:total    optical    FIR     CMB       isotropic  total-diffuse  sources total-with-sources sourcepop-total above-limit below-limit solar IC\n");

  fprintf(txtFILE,"\n");

   for  (ip    =0;  ip    <galaxy.n_E_gammagrid ;ip    ++) {

     spectrum_pi0   ->GetPoint(ip,x,y);
     fprintf(txtFILE,"%10.2e ",x );// energy
     fprintf(txtFILE,"%9.5f " ,y );// intensity

     spectrum_bremss->GetPoint(ip,x,y);
     fprintf(txtFILE,"%9.5f ",y );

     spectrum_ic->GetPoint(ip,x,y);
     fprintf(txtFILE,"%9.5f ",y );

     if (galplotdef.gamma_IC_selectcomp) {

       spectrum_ic_optical->GetPoint(ip,x,y);
       fprintf(txtFILE,"%9.5f ",y );
       
       spectrum_ic_ir->GetPoint(ip,x,y);
       fprintf(txtFILE,"%9.5f ",y );
       
       spectrum_ic_cmb->GetPoint(ip,x,y);
       fprintf(txtFILE,"%9.5f ",y );

     }

    
     spectrum_isotropic->GetPoint(ip,x,y);  //AWS20081118
     fprintf(txtFILE,"%9.5f ",y );
     

     spectrum_total    ->GetPoint(ip,x,y);
     fprintf(txtFILE,"%9.5f ",y );
 

     spectrum_sources  ->GetPoint(ip,x,y);            //AWS20081212
     fprintf(txtFILE,"%9.5f ",y );

     spectrum_total_with_sources  ->GetPoint(ip,x,y); //AWS20081212
     fprintf(txtFILE,"%9.5f ",y );

     spectrum_sourcepop  ->GetPoint(ip,x,y);                     //AWS20090109
     fprintf(txtFILE,"%9.5f ",y );


     spectrum_sourcepop_soplimit  ->GetPoint(ip,x,y);            //AWS20090109
     fprintf(txtFILE,"%9.5f ",y );

     spectrum_sourcepop_sublimit  ->GetPoint(ip,x,y);            //AWS20090109
     fprintf(txtFILE,"%9.5f ",y );


     spectrum_solar_IC            ->GetPoint(ip,x,y);            //AWS20100215
     fprintf(txtFILE,"%12.8f ",y );

     fprintf(txtFILE,"\n");
   }

  fprintf(txtFILE,"===============================================================\n\n");

  }// if




  


   cout<<" <<<< plot_spectrum_healpix   "<<endl;
   return status;
}





















