/* based on       ibis.pro :
   ; IBIS values from Regis Terrier, email 25 March 2004
; E^2 I(E) per radian

color=2
thick=2

e=[20,40]
flux=1.93 & fluxerr=0.67
oplot,e,[flux,flux],color=color,thick=thick
emean=sqrt(e(0)*e(1))
oplot,[emean,emean],[flux-fluxerr,flux+fluxerr],color=color,thick=thick


ulrat=0.1 ; ratio for upper limit line
vrat=.95  ; for plotting upper limit V

e=[40,60]
flux=1.37 
oplot,e,[flux,flux],color=color,thick=thick
emean=sqrt(e(0)*e(1))
oplot,[emean,emean],[flux,flux*ulrat],color=color,thick=thick
xyouts,[emean]*vrat,[flux*ulrat],'V',color=color,charsize=2,charthick=2

e=[60,120]
flux=1.56 
oplot,e,[flux,flux],color=color,thick=thick
emean=sqrt(e(0)*e(1))
oplot,[emean,emean],[flux,flux*ulrat],color=color,thick=thick
xyouts,[emean]*vrat,[flux*ulrat],'V',color=color,charsize=2,charthick=2


e=[120,220]
flux=2.26 
oplot,e,[flux,flux],color=color,thick=thick
emean=sqrt(e(0)*e(1))
oplot,[emean,emean],[flux,flux*ulrat],color=color,thick=thick
xyouts,[emean]*vrat,[flux*ulrat],'V',color=color,charsize=2,charthick=2


*/
#include"Galplot.h"                    //AWS20050920
#include"galprop_classes.h"
#include"galprop.h"
#include"constants.h"
#include"fitsio.h"

int  Galplot::plot_IBIS_spectrum(int mode)
{

   cout<<" >>>> plot_IBIS_spectrum    "<<endl;
   int status=0;


   int n_energy;
   double *emin,*emax,*flux,*fluxerr;
   double emean,ymin,ymax,y;
   double solid_angle;
   int i_energy;
 
   TLine *line;
  
   n_energy=4;
   emin=      new double[ n_energy];
   emax=      new double[ n_energy];
   flux      =new double[ n_energy];
   fluxerr   =new double[ n_energy];


   emin[0]= 20.;   emax[0]= 40.; flux[0]=1.93;fluxerr[0]=0.67;
   emin[1]= 40.;   emax[1]= 60.; flux[1]=1.37;fluxerr[1]=0.00;
   emin[2]= 60.;   emax[2]=120;  flux[2]=1.56;fluxerr[2]=0.00;
   emin[3]=120;    emax[3]=220.; flux[3]=2.26;fluxerr[3]=0.00;

   solid_angle= (galplotdef. lat_max1-galplotdef. lat_min1 + galplotdef. lat_max2-galplotdef. lat_min2 )
               *(galplotdef.long_max1-galplotdef.long_min1 + galplotdef.long_max2-galplotdef.long_min2 )
                /(57.3*57.3);// sterad;
 
 


   //values from Terrier are per radian, and they are for the inner radian, so flux is effectively cm-2 s-1
   // conversion to intensity only valid for same region


   n_energy=1; // plot only detected point

   for (i_energy=0;i_energy<n_energy;i_energy++)
   {
    emean=sqrt(emin[i_energy]*emax[i_energy]) *1e-3;   // keV -> MeV

    ymin= (flux[i_energy]-fluxerr[i_energy])  *1e-3;   // keV -> MeV
    ymax= (flux[i_energy]+fluxerr[i_energy])  *1e-3;   // keV -> MeV
 
    ymin/=solid_angle;
    ymax/=solid_angle;
 
  
 
 
    if(ymin < galplotdef.gamma_spectrum_Imin)ymin = galplotdef.gamma_spectrum_Imin;
    cout<<" plot_IBIS emean ymin max "<<emean<<" "<<ymin<<" "<<ymax<<endl;
 
    line=new TLine(emean ,ymin,emean,ymax);
    line->SetLineColor(kMagenta);
    line->SetLineWidth(4  );
    line->Draw();


    y   =  flux[i_energy]*1e-3; // keV -> MeV
    y/=solid_angle;
    cout<<" plot_IBIS e y "<<emin[i_energy]*1e-3 <<" "<< emax[i_energy]*1e-3<<" "<<y<<endl;

    line=new TLine(emin[i_energy]*1e-3, y, emax[i_energy]*1e-3, y);
    line->SetLineColor(kMagenta);
    line->SetLineWidth(4  );
    line->Draw();

    }
 

 

  cout<<" <<<< plot_IBIS_spectrum    "<<endl;
   return status;
}

