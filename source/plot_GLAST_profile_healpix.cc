#include"Galplot.h"                    
#include"galprop_classes.h"
#include"galprop.h"
#include"constants.h"
#include"fitsio.h"


#include<vector>
#include<valarray>

int Galplot::plot_GLAST_profile_healpix(int longprof, int latprof,int ip,int mode)
{
   cout<<" >>>> plot_GLAST_profile_healpix    "<<endl;
   int status=0;

   int i,j, ipix, i_E_GLAST;
   pointing point;
   double l,l_off,b,b_off;

   double pi= acos(-1.0);
   double rtd=180./pi;        // rad to deg
   double dtr=pi / 180.;      // deg to rad

   int intensity_method=1; //   1=sum(counts/exposure)(preferred)  2=(sum counts)/(sum exposure) 

  TH1D *profile;

  cout<<"GLAST counts energy bounds:"<<endl;
  for( i=0;i<  data.GLAST_counts_healpix .getEMin().size();i++)   
    cout<< data.GLAST_counts_healpix.getEMin()[i]<<" -  " << data.GLAST_counts_healpix.getEMax()[i] <<endl; // returns a valarray<double>

//Skymap<long> counts=data.GLAST_counts_healpix.getCountsMap();   AWS20091210
  Skymap<int>  counts=data.GLAST_counts_healpix.getCountsMap(); //AWS20091210

  //  if(galplotdef.verbose==-1901)  { cout<<"GLAST counts:"<<endl;  counts.print(cout);} AWS20091204

 

  cout<<"GLAST exposure energies :"<<endl;
  
  for( i=0;i< data.GLAST_exposure_healpix.getEnergies().size();i++)    cout<<data.GLAST_exposure_healpix.getEnergies()[i]<<endl;

  Skymap<double> exposure=   data.GLAST_exposure_healpix .getExposureMap();
  //  if(galplotdef.verbose==-1901) {  cout<<"GLAST exposure:"<<endl;  exposure.print(cout);} AWS20091204


  cout<<"GLAST counts    healpix #pixels = "<<  counts.Npix()<<endl;
  cout<<"GLAST exposure  healpix #pixels = "<<exposure.Npix()<<endl;

  cout<<"GLAST counts    healpix solid angle (sr) = "<< 4.*pi/ counts  .Npix()<<endl;
  cout<<"GLAST exposure  healpix solid angle (sr) = "<< 4.*pi/ exposure.Npix()<<endl;


  cout<<"GLAST counts    Skymap solid angle (sr)  = "<<counts  .solidAngle()<<endl; //NB erroneously stated deg^2 in Skymap.h
  cout<<"GLAST exposure  Skymap solid angle (sr)  = "<<exposure.solidAngle()<<endl; //NB erroneously stated deg^2 in Skymap.h

  if(galplotdef.verbose==-1904) //AWS20110719 however crashes at some point
  for (ipix=0;ipix<counts.Npix();ipix++) // Npix: total pixels: healpix_base.h
  {

    cout<<"theta,phi= "<<counts.pix2ang(ipix)<<endl; // (theta, phi)  in radian

    //  cout<<counts.coord[ipix]<<endl; not sure how this works
    l =        counts.pix2ang(ipix).phi   * rtd;
    b = 90.0 - counts.pix2ang(ipix).theta * rtd;
    cout<<"l = "<<l<<" b ="<<b<<endl; // l,b in deg

   for( i_E_GLAST=0;i_E_GLAST< data.GLAST_exposure_healpix.getEnergies().size();i_E_GLAST++)
     cout<<exposure  [ipix][i_E_GLAST]<<" ";   cout<<endl;//AWS20110719

   for( i_E_GLAST=0;i_E_GLAST< data.GLAST_counts_healpix.  getEMin()    .size();i_E_GLAST++)
     cout<<counts[ipix][i_E_GLAST]<<" ";   cout<<endl;    //AWS20110719

   
}
  //---------------------------------------------------------------------------------------------------------------



  
  //  integrate exposure over energy
  cout<<" plot_GLAST_profile_healpix: ip="<<ip<<endl;
  
if (data.GLAST_exposure_integrated_init==0) // only compute once, for all energies
      
{
  cout<<"    integrating GLAST exposure for all data energy bands"<<endl;
  data.GLAST_exposure_integrated_init=1;

  // integrated exposure has same size as counts
  //  Skymap<double>exposure_integrated (counts.Order() ,  data.GLAST_counts_healpix.  getEMin()    .size()  ) ;  // Order() in healpix_base.h
  //  cout  <<"counts.Npix="<<counts.Npix()   <<"   exposure_integrated.Npix="<<exposure_integrated.Npix()<<endl; // check for equality

  data.GLAST_exposure_integrated =Skymap<double>(counts.Order() ,  data.GLAST_counts_healpix.  getEMin()    .size()  ) ;  // Order() in healpix_base.h
  cout  <<"count.Npix="<<counts.Npix()   <<"   data.GLAST_exposure_integrated.Npix="<<data.GLAST_exposure_integrated.Npix()<<endl; // check for equality

   for (ipix=0;ipix<data.GLAST_exposure_integrated.Npix() ;ipix++)
   {
    SM::Coordinate coordinate=counts.pix2coord( ipix);
    //    if(galplotdef.verbose==-1902)      {cout<<"  pixel coordinate from pix2coord:";coordinate.print(cout);cout<<endl;} AWS20091204

    for( i_E_GLAST=0;i_E_GLAST< data.GLAST_counts_healpix.getEMin().size();i_E_GLAST++)
    {
     double index=-2.1;// 2.0 gave nan !!
     double exposure_integrated_;
 
     exposure_integrated_=
      data.GLAST_exposure_healpix .getWeightedExposure
        (coordinate,  data.GLAST_counts_healpix.getEMin()[i_E_GLAST], data.GLAST_counts_healpix.getEMax()[i_E_GLAST],  index) ;

     //              exposure_integrated[ipix][i_E_GLAST]=exposure_integrated_;
     data.GLAST_exposure_integrated[ipix][i_E_GLAST]=exposure_integrated_;

     if(galplotdef.verbose==-1902) 
     cout<<"plot_GLAST_profile_healpix: Emin="<<  data.GLAST_counts_healpix.getEMin()[i_E_GLAST]<<" Emax="<<  data.GLAST_counts_healpix.getEMax()[i_E_GLAST]
         <<" exposure_integrated_="<<exposure_integrated_<<endl;


    }
   }

   //   if(galplotdef.verbose==-1903) {   cout<<"data.GLAST_exposure_integrated:"<<endl; data.GLAST_exposure_integrated.print(cout);} AWS20091204
    } // if

   
   //---------------------------------------------------------------------------------------------------------------
  // do it this way since copy of counts will give long int, but want double
  Skymap<double>flux (counts.Order() ,  data.GLAST_counts_healpix.  getEMin()    .size()  ) ;  // Order() in healpix_base.h
  cout  <<"counts.Npix="<<counts.Npix()   <<"   flux.Npix="<<flux.Npix()<<endl; // check for equality


  // flux skymap 
   for (ipix=0;ipix<flux.Npix() ;ipix++)
        for( i_E_GLAST=0;i_E_GLAST< data.GLAST_counts_healpix.getEMin().size();i_E_GLAST++)
     {
       flux[ipix][i_E_GLAST] = counts[ipix][i_E_GLAST] / data.GLAST_exposure_integrated[ipix][i_E_GLAST]; // not used but could be output for viewing
     } //i_E_GLAST

   flux/=counts.solidAngle();

   //   cout<<"flux=";flux.print(cout);

  //---------------------------------------------------------------------------------------------------------------
  // longitude profile


   if(longprof==1)
   {
 
   int ilong,nlong;
   nlong=int (360./galplotdef.long_binsize + .01) ;

   vector<valarray<double> >           counts_profile(nlong );  // vector of valarrays to take advantage of valarray operations and vector indexing
   vector<valarray<double> >         exposure_profile(nlong );
   vector<valarray<double> >        intensity_profile(nlong );
   vector<valarray<double> >  intensity_error_profile(nlong );
   vector<valarray<double> >  number_of_pixels       (nlong );

   for (ilong=0;ilong<nlong ;ilong++)
   {
             counts_profile[ilong].resize(data.GLAST_counts_healpix.getEMin().size()   );
             counts_profile[ilong]=0.;
           exposure_profile[ilong].resize(data.GLAST_counts_healpix.getEMin().size()   );
           exposure_profile[ilong]=0.;
          intensity_profile[ilong].resize(data.GLAST_counts_healpix.getEMin().size()   );
          intensity_profile[ilong]=0.;
    intensity_error_profile[ilong].resize(data.GLAST_counts_healpix.getEMin().size()   );
    intensity_error_profile[ilong]=0.;
           number_of_pixels[ilong].resize(data.GLAST_counts_healpix.getEMin().size()   );
           number_of_pixels[ilong]=0.;
   }

   for (ipix=0;ipix<flux.Npix() ;ipix++)
   {
    l =        counts.pix2ang(ipix).phi   * rtd;
    b = 90.0 - counts.pix2ang(ipix).theta * rtd;

    if(galplotdef.verbose==-1902)    cout<<"ipix="<<ipix<<" l = "<<l<<" b ="<<b      ; // l,b in deg

    SM::Coordinate coordinate=counts.pix2coord( ipix);
    //    if(galplotdef.verbose==-1902)      {cout<<"  pixel coordinate from pix2coord:";coordinate.print(cout);cout<<endl;} AWS20091204


    int select=0;
    if(b>=galplotdef.lat_min1 && b<=galplotdef.lat_max1) select=1;
    if(b>=galplotdef.lat_min2 && b<=galplotdef.lat_max2) select=1;

    if (select==1)
    {
      //  cout<<"selected ipix="<<ipix<<" l = "<<l<<" b ="<<b<<endl; // l,b in deg
      l_off= l-180.0; // l:0-360, offset l_off relative to l=180 (since plot goes from 180...0..-180)
      if(l_off<  0.0)l_off+=360.;// put offset in range 0-360
      if(l_off>360.0)l_off-=360.;
      ilong= nlong-1 - int(l_off / galplotdef.long_binsize + .01);// reverse scale for plot, goes from nlong-1 to 0
      //     ilong=  int(l_off / galplotdef.long_binsize + .01);// no reverse scale for plot (test)

      if(galplotdef.verbose==-1901)cout<<"plot_GLAST_profile_healpix: l_off="<<l_off<<" ilong="<<ilong<<endl;
      if(ilong<0||ilong>nlong)cout<<"plot_GLAST_profile_healpix:error: ilong out of range:"<<ilong<<endl;

     if(ilong>=0 && ilong<nlong)
     for( i_E_GLAST=0;i_E_GLAST< data.GLAST_counts_healpix.getEMin().size();i_E_GLAST++)
     {
          counts_profile [ilong][i_E_GLAST] +=   counts[ipix][i_E_GLAST];
	exposure_profile [ilong][i_E_GLAST] +=                             data.GLAST_exposure_integrated[ipix][i_E_GLAST]*counts.solidAngle(); 
        intensity_profile[ilong][i_E_GLAST] +=   counts[ipix][i_E_GLAST]/ (data.GLAST_exposure_integrated[ipix][i_E_GLAST]*counts.solidAngle());
	number_of_pixels [ilong][i_E_GLAST] ++;
     } //i_E_GLAST
    } // if select

   } // ipix



  for (ilong=0;ilong<nlong ;ilong++)
  { 
   for( i_E_GLAST=0;i_E_GLAST< data.GLAST_counts_healpix.getEMin().size();i_E_GLAST++)
   {
    if(exposure_profile[ilong][i_E_GLAST]>0.)
    {


      if(intensity_method==1)
      {
      //   average intensity over bin, error estimate from total counts
             intensity_profile[ilong][i_E_GLAST]/= number_of_pixels [ilong][i_E_GLAST];
       intensity_error_profile[ilong][i_E_GLAST]=  intensity_profile[ilong][i_E_GLAST]/ sqrt(counts_profile[ilong][i_E_GLAST]);
      }

      if(intensity_method==2)
      {
      //  method ignoring variations of intensity and exposure
             intensity_profile[ilong][i_E_GLAST]=      counts_profile[ilong][i_E_GLAST] /exposure_profile[ilong][i_E_GLAST] ;
       intensity_error_profile[ilong][i_E_GLAST]= sqrt(counts_profile[ilong][i_E_GLAST])/exposure_profile[ilong][i_E_GLAST] ;
      } 

    }// if exposure

   }// i_E_GLAST
  }// ilong


  if(galplotdef.verbose==-1901)
   for (ilong=0;ilong<nlong ;ilong++)
     {
       cout<<"counts profile ilong= "<<ilong<<" l="<<ilong* galplotdef.long_binsize<<"-"<<( ilong+1)* galplotdef.long_binsize <<" : ";
       for( i_E_GLAST=0;i_E_GLAST< data.GLAST_counts_healpix.getEMin().size();i_E_GLAST++)
	 cout<<  counts_profile[ilong][i_E_GLAST]<< " "      ;
       cout<<endl;

       cout<<"  intensity profile:";
       for( i_E_GLAST=0;i_E_GLAST< data.GLAST_counts_healpix.getEMin().size();i_E_GLAST++)
	 cout<<  intensity_profile[ilong][i_E_GLAST]<< " "      ;
       cout<<endl;

       cout<<"  intensity error profile:";
       for( i_E_GLAST=0;i_E_GLAST< data.GLAST_counts_healpix.getEMin().size();i_E_GLAST++)
	 cout<<  intensity_error_profile[ilong][i_E_GLAST]<< " "      ;
       cout<<endl;


   }


   // generate the root objects
     
   profile=new TH1D("GLAST longitude profile","GLAST longitude  profile",nlong,        -180.,180.);

   int i_long,ii_long;
   i_E_GLAST=ip;

  for(i_long=0;i_long<profile->GetNbinsX();i_long++)
  {
     ii_long=i_long+1; // TH1D bins start at 1
     profile->SetBinContent(ii_long      , intensity_profile      [i_long][i_E_GLAST] );
     profile->SetBinError  (ii_long      , intensity_error_profile[i_long][i_E_GLAST] );

     if(galplotdef.verbose==-1905) //AWS20110826
      {
        double   l =180.0-(i_long+0.5)*galplotdef.long_binsize;
	if(l<0.0)l+=360.0; // put in 0<l<360 

        txt_stream
	    <<"Fermi longitude profile: l = "<<l 
	    <<"  b = "<<galplotdef.lat_min1<<" - "<<galplotdef.lat_max1
	    <<", "    <<galplotdef.lat_min2<<" - "<<galplotdef.lat_max2
	    <<"  E= "<< data.GLAST_counts_healpix.getEMin()[i_E_GLAST]<< " - "
                     << data.GLAST_counts_healpix.getEMax()[i_E_GLAST]<< " MeV "
            <<" intensity = "<< profile->GetBinContent(ii_long)
            <<    " error = "<< profile->GetBinError  (ii_long)
            <<" cm-2 sr-1 s-1"
            <<endl;
      }
  }

  }// longprof


 //---------------------------------------------------------------------------------------------------------------


 // latitude profile


   if(latprof==1)
   {
 
   int ilong,nlong;
   int ilat, nlat;

   nlat = int (180./galplotdef. lat_binsize + .01) ;
  

   vector<valarray<double> >           counts_profile(nlat  );  // vector of valarrays to take advantage of valarray operations and vector indexing
   vector<valarray<double> >         exposure_profile(nlat  );
   vector<valarray<double> >        intensity_profile(nlat  );
   vector<valarray<double> >  intensity_error_profile(nlat  );
   vector<valarray<double> >  number_of_pixels       (nlat  );

   for (ilong=0;ilong<nlat  ;ilong++)
   {
             counts_profile[ilong].resize(data.GLAST_counts_healpix.getEMin().size()   );
             counts_profile[ilong]=0.;
           exposure_profile[ilong].resize(data.GLAST_counts_healpix.getEMin().size()   );
           exposure_profile[ilong]=0.;
          intensity_profile[ilong].resize(data.GLAST_counts_healpix.getEMin().size()   );
          intensity_profile[ilong]=0.;
    intensity_error_profile[ilong].resize(data.GLAST_counts_healpix.getEMin().size()   );
    intensity_error_profile[ilong]=0.;
           number_of_pixels[ilong].resize(data.GLAST_counts_healpix.getEMin().size()   );
           number_of_pixels[ilong]=0.;
   }

   for (ipix=0;ipix<flux.Npix() ;ipix++)
   {
    l =        counts.pix2ang(ipix).phi   * rtd;
    b = 90.0 - counts.pix2ang(ipix).theta * rtd;
    //  cout<<"ipix="<<ipix<<" l = "<<l<<" b ="<<b<<endl; // l,b in deg

    int select=0;
    if(l>=galplotdef.long_min1 && l<=galplotdef.long_max1) select=1;
    if(l>=galplotdef.long_min2 && l<=galplotdef.long_max2) select=1;

    if (select==1)
    {
      //  cout<<"selected ipix="<<ipix<<" l = "<<l<<" b ="<<b<<endl; // l,b in deg
      b_off= b+90.0; // l:0-360, offset l_off relative to b=-90
    
      ilong=           int(b_off / galplotdef. lat_binsize + .01);

      if(galplotdef.verbose==-1901)   cout<<"plot_GLAST_profile_healpix: b_off="<<b_off<<" ilong="<<ilong<<endl;
      if(ilong<0||ilong>nlat )        cout<<"plot_GLAST_profile_healpix:error: ilong out of range:"<<ilong<<endl;

     if(ilong>=0 && ilong<nlat )
     for( i_E_GLAST=0;i_E_GLAST< data.GLAST_counts_healpix.getEMin().size();i_E_GLAST++)
     {
           counts_profile[ilong][i_E_GLAST] +=   counts[ipix][i_E_GLAST];
       	 exposure_profile[ilong][i_E_GLAST] +=                             data.GLAST_exposure_integrated[ipix][i_E_GLAST]*counts.solidAngle(); 
        intensity_profile[ilong][i_E_GLAST] +=   counts[ipix][i_E_GLAST]/ (data.GLAST_exposure_integrated[ipix][i_E_GLAST]*counts.solidAngle());
	number_of_pixels [ilong][i_E_GLAST] ++;

     } //i_E_GLAST
    } // if select

   } // ipix



  for (ilong=0;ilong<nlat  ;ilong++)
  { 
   for( i_E_GLAST=0;i_E_GLAST< data.GLAST_counts_healpix.getEMin().size();i_E_GLAST++)
   {
    if(exposure_profile[ilong][i_E_GLAST]>0.)
    {

     if(intensity_method==1)
      {
      //   average intensity over bin, error estimate from total counts
             intensity_profile[ilong][i_E_GLAST]/= number_of_pixels [ilong][i_E_GLAST];
       intensity_error_profile[ilong][i_E_GLAST]=  intensity_profile[ilong][i_E_GLAST]/ sqrt(counts_profile[ilong][i_E_GLAST]);
      }

      if(intensity_method==2)
      {
     //  method ignoring variations of intensity and exposure
             intensity_profile[ilong][i_E_GLAST]=      counts_profile[ilong][i_E_GLAST] /exposure_profile[ilong][i_E_GLAST] ;
       intensity_error_profile[ilong][i_E_GLAST]= sqrt(counts_profile[ilong][i_E_GLAST])/exposure_profile[ilong][i_E_GLAST] ;
       }
    }

    }
  }


  if(galplotdef.verbose==-1901)
   for (ilong=0;ilong<nlat  ;ilong++)
     {
       cout<<"counts profile ilong= "<<ilong<<" l="<<ilong* galplotdef. lat_binsize<<"-"<<( ilong+1)* galplotdef. lat_binsize <<" : ";
       for( i_E_GLAST=0;i_E_GLAST< data.GLAST_counts_healpix.getEMin().size();i_E_GLAST++)
	 cout<<  counts_profile[ilong][i_E_GLAST]<< " "      ;
       cout<<endl;

       cout<<"  intensity profile:";
       for( i_E_GLAST=0;i_E_GLAST< data.GLAST_counts_healpix.getEMin().size();i_E_GLAST++)
	 cout<<  intensity_profile[ilong][i_E_GLAST]<< " "      ;
       cout<<endl;

       cout<<"  intensity error profile:";
       for( i_E_GLAST=0;i_E_GLAST< data.GLAST_counts_healpix.getEMin().size();i_E_GLAST++)
	 cout<<  intensity_error_profile[ilong][i_E_GLAST]<< " "      ;
       cout<<endl;


   }


   // generate the root objects
     
  
  profile=new TH1D("GLAST latitude  profile","GLAST latitude   profile",nlat , -90., 90. ); //AWS20080806

   int i_long,ii_long;
   i_E_GLAST=ip;

  for(i_long=0;i_long<profile->GetNbinsX();i_long++)
  {
     ii_long=i_long+1; // TH1D bins start at 1
     profile->SetBinContent(ii_long      , intensity_profile      [i_long][i_E_GLAST] );
     profile->SetBinError  (ii_long      , intensity_error_profile[i_long][i_E_GLAST] );

      if(galplotdef.verbose==-1901)
      cout<<"GLAST latitude profile: intensity = "<< profile->GetBinContent(ii_long)<<" error="<< profile->GetBinError(ii_long)<<endl;

      if(galplotdef.verbose==-1905)//AWS20110826 
      {
        double   b=-90.0 + (i_long+0.5)*galplotdef.lat_binsize;
	
            txt_stream
            <<"Fermi latitude  profile: l = "<<galplotdef.long_min1<<" - "<<galplotdef.long_max1
	    <<", "                           <<galplotdef.long_min2<<" - "<<galplotdef.long_max2
            <<"  b = "<<b 
	    <<"  E= "<< data.GLAST_counts_healpix.getEMin()[i_E_GLAST]<< " - "
                     << data.GLAST_counts_healpix.getEMax()[i_E_GLAST]<< " MeV "
            <<" intensity = "<< profile->GetBinContent(ii_long)
            <<    " error = "<< profile->GetBinError  (ii_long)
            <<" cm-2 sr-1 s-1"
            <<endl;
      }

  }

  }// latprof

   //---------------------------------------------------------------------------------------------

   // plot the profile

  profile->SetLineColor(kBlack);
  profile->Draw("e same");

  profile       ->SetName("xxxx");// to avoid problem on subsequent calls








   cout<<" <<<< plot_GLAST_profile_healpix    "<<endl;
   return status;


}


