class Fermi_Aeff
{
 public:
  int nE_true;
  valarray<double> Aeff_average;
  valarray<double> E_true;

  void read(string filename,                  int debug);
  void read(string filename, string filetype, int debug);

  double Aeff_average_interpolated(double E, int debug);

  void test();
  int initialized;
};
