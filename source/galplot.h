
#include <TROOT.h>
#include <TApplication.h>
#include <TGClient.h>

#include <TF1.h>
#include <TH2.h>
#include <TH3.h>
#include <TCanvas.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TGraphAsymmErrors.h>
#include <TPolyLine.h>
#include <TEllipse.h>
#include <TPaveLabel.h>
#include <TStyle.h>
#include <TGaxis.h>
#include <TLine.h>
#include <TMarker.h>
#include <TLatex.h>
#include <TText.h>
#include <TLatex.h> //AWS20041021
#include <TMinuit.h>

#include "Galplotdef.h"
#include "Data.h"
#include "Convolved.h"
#include "UnConvolved.h"
#include "GCR_data.h"
#include "FITS.h"
#include "Skymap.h" //AWS20140721 for energy dispersion routine

int plot_HIR();
int read_IC_skymap(char *IC_type);

int read_bremss_skymap();
int read_bremss_HIR_skymap();   //AWS20041223
int read_bremss_H2R_skymap();   //AWS20041223

int read_pi0_decay_skymap();
int read_pi0_decay_HIR_skymap();//AWS20041223
int read_pi0_decay_H2R_skymap();//AWS20041223

int read_synchrotron_skymap()  ;//AWS20050726
int plot_synchrotron_skymap()  ;//AWS20050726
int read_synchrotron_data  ()  ;//AWS20050728

int plot_skymaps();
int plot_profile(int longprof, int latprof, int ip,int ic,int bremss,int pi0,int total);
int plot_convolved_EGRET_profile(int longprof, int latprof,int ip,int ic,int bremss,int pi0,int total);

int read_EGRET_data();
int read_EGRET_psf();
int plot_EGRET_skymaps(int mode);
int plot_EGRET_profile(int longprof, int latprof,int ip,int mode);
int plot_EGRET_spectrum(int mode);

int read_COMPTEL_data();
int plot_COMPTEL_spectrum(int mode);

int plot_SPI_spectrum           (int mode);// AWS20040405
int plot_SPI_spectrum_spimodfit (int mode);// AWS20050308
int plot_RXTE_spectrum(int mode);// AWS20040407
int plot_OSSE_spectrum(int mode);// AWS20041001   
int plot_IBIS_spectrum(int mode);// AWS20041015 
int plot_GINGA_spectrum(int mode);// AWS20041102 
int plot_model_ridge   (int mode);// AWS20041102 
int plot_MILAGRO_spectrum (int mode);//AWS20050518
int plot_Chandra_spectrum(int mode); //AWS20050818

int plot_spectrum(int ic,int bremss,int pi0,int total);
int plot_isotropic_sreekumar_spectrum();
int plot_isotropic_EGRET_spectrum();
int fit_EGRET(int options);
int fit_EGRET_Xco(int options);//AWS20050104
int plot_hunter_10_50GeV_spectrum();

int plot_gcr_spectra();
int plot_gcr_spectra_data(int Z,int A,int index_mult);//AWS20091009
int plot_gcr_spectra_ratios();
int plot_gcr_spectra_ratios_data(int i_ratio);
int convolve_EGRET();
int convolve_EGRET_HI_H2();//AWS20041223
int convolve(Distribution &map, Distribution psf);
double sabin(double Y0,double XBINSZ,double YBINSZ,int IY);

double energy_integral
(double emin,double emax,double e1,double e2,double intensity_esq1,double intensity_esq2);
int test_energy_integral    ();

void modulate(double *Ekin, float *cr_density, int np, int z, int a,double phi, int key);



int plot_SPI_spiskymax_profile(int longprof,int latprof,int mode);//AWS20050720
int plot_SPI_model_profile(int longprof, int latprof,double e_min,double e_max,int ic,int bremss,int total);//AWS20050720
int convolve_SPI(double emin,double emax);//AWS20050721


int test_Skymap(int verbose); //AWS20080516
int EBV_convert(int verbose); //AWS20090820


int Skymap_Energy_Dispersion(Skymap<double> &skymap, double E_interp_factor, int interpolation, string parameter_file_name, string parameter_file_type, string exposure_file_name, string exposure_file_type, int use_Aeff, double E_true_threshold, double E_meas_threshold,  int debug); //AWS20150301

int spectrum_Energy_Dispersion(valarray<double> E, valarray<double> &spectrum, double E_interp_factor, int interpolation, string parameter_file_name, string parameter_file_type, string exposure_file_name, string exposure_file_type, int use_Aeff, double E_true_threshold, double E_meas_threshold,  int debug); //AWS20150301
