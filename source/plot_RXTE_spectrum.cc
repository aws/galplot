
//**.****|****.****|****.****|****.****|****.****|****.****|****.****|****.****|
// * plot_RXTE_spectrum.cc *                             
//**"****!****"****!****"****!****"****!****"****!****"****!****"****!****"****|
#include"Galplot.h"                    //AWS20050920
#include"galprop_classes.h"
#include"galprop.h"
#include"constants.h"
#include"fitsio.h"

int Galplot::plot_RXTE_spectrum(int mode)
{

   cout<<" >>>> plot_RXTE_spectrum    "<<endl;
   int status=0;

   int test_comparison;

 
   int n_energy;
   double *emin,*emax,*flux,*dflux;
   double emean,ymin,ymax,y;
   double solid_angle;
   int i_energy;

   TLine *line;
  
   n_energy=24;
   emin=      new double[ n_energy];
   emax=      new double[ n_energy];
    flux=new double[ n_energy];
   dflux=new double[ n_energy];

/*
/afs/ipp-garching.mpg.de/mpe/gamma/instruments/integral/data/surveys_3/user_aws/GCDE_v4_continuum/rxte_revnivtsev.pro
; values per degree^2
; refers to |b|>2 degrees

; see mail/RSci 27 Jun 2003 12:59: suggests scaling factor 400 for |l|<20, & extrapolating to b=0

 ##############################################################################
 Mail from Mike Revnivtsev 28 April 2005:
 The total flux in two stripes 20 deg along longitude and from 2 to 10 
 degrees long latitude (~320 sq.degrees in total)
 is ~3.54e-9 erg/s/cm^2  in the energy band 3-20 keV
 ##############################################################################
 
;Table of values
;Model= powerlaw + gauss
;model1=powerlaw
;model2=gauss
e       =a(*,0)
flux    =a(*,2)*factor
flux_err=a(*,3)*factor
 
;             Energy      de          F          dF        model     model1  model2
               keV                cm-2 s-1 keV deg-2
*/

   double a[24][7]=
     {
              3.60162, 0.210023, 3.78344E-4, 1.06686E-5, 3.5713E-4, 3.5713E-4, 0,
              4.02178, 0.210137, 2.87683E-4, 8.58354E-6, 2.81422E-4, 2.81422E-4, 0,
              4.44223, 0.210307, 2.17261E-4, 7.76197E-6, 2.27053E-4, 2.27053E-4, 0,
              4.86301, 0.210478, 1.80983E-4, 7.14359E-6, 1.86762E-4, 1.86762E-4, 1.06142E-11,
              5.28411, 0.21062 ,1.42483E-4, 6.39953E-6, 1.56149E-4, 1.56141E-4, 7.69163E-9,
              5.7055 ,0.210763 ,1.30692E-4, 5.52598E-6, 1.33408E-4, 1.32342E-4, 1.06622E-6,
              6.12719, 0.210933, 1.37522E-4, 5.32688E-6, 1.37425E-4, 1.13485E-4, 2.39398E-5,
              6.54923, 0.211104, 2.02818E-4, 6.88934E-6, 1.92704E-4, 9.832E-5, 9.43841E-5,
              6.97158, 0.211246, 1.52638E-4, 5.76135E-6, 1.55266E-4, 8.59241E-5, 6.93417E-5,
              7.39424, 0.211417, 8.66837E-5, 3.81458E-6, 8.48996E-5, 7.56704E-5, 9.22925E-6,
              7.81722, 0.211559, 6.81742E-5, 3.92839E-6, 6.73514E-5, 6.71378E-5, 2.13583E-7,
              8.24051, 0.21173, 5.76187E-5, 4.27487E-6, 5.99413E-5, 5.99405E-5, 7.71043E-10,
              8.66414, 0.2119, 5.98643E-5, 4.31799E-6, 5.37846E-5, 5.37846E-5, 0,
              9.08808, 0.212043, 4.87499E-5, 4.19656E-6, 4.85371E-5, 4.85371E-5, 0,
              9.51231, 0.212185, 4.36819E-5, 4.08049E-6, 4.39821E-5, 4.39821E-5, 0,
              10.1494, 0.424911, 3.79979E-5, 2.81384E-6, 3.83191E-5, 3.83191E-5, 0,
              10.9998, 0.425536, 3.37251E-5, 2.80666E-6, 3.22059E-5, 3.22059E-5, 0,
              12.0649, 0.639513, 2.57183E-5, 2.36733E-6, 2.64412E-5, 2.64412E-5, 0,
              13.7736, 1.06915, 2.21571E-5, 2.06199E-6, 1.99518E-5, 1.99518E-5, 0,
              15.9159, 1.07324, 1.61233E-5, 2.43524E-6, 1.45907E-5, 1.45907E-5, 0,
              18.0666, 1.07742, 1.13846E-5, 3.06029E-6, 1.10938E-5, 1.10938E-5, 0,
              20.2257, 1.08163, 1.19281E-5, 3.50472E-6, 8.69421E-6, 8.69421E-6, 0,
              22.3932, 1.0859, 8.18335E-6, 4.30628E-6, 6.97849E-6, 6.97849E-6, 0,
              24.1327, 0.653624, -5.01949E-7, 6.97282E-6, 5.92979E-6, 5.92979E-6, 0 

  };



   solid_angle= (galplotdef. lat_max1-galplotdef. lat_min1 + galplotdef. lat_max2-galplotdef. lat_min2 )
               *(galplotdef.long_max1-galplotdef.long_min1 + galplotdef.long_max2-galplotdef.long_min2 )
                /(57.3*57.3);// sterad;

   solid_angle=1.0/(57.3*57.3);// 1 sq deg in sterad;

   // using new normalization of 28 April
   double total=0.;

   for (i_energy=0;i_energy<n_energy;i_energy++)
   {
    total+= a[i_energy][0] * a[i_energy][2] * a[i_energy][1]; // E * F(E) dE
   }

   cout<< " total energy (keV  cm-2 s-1 deg-2) = "<<total<<endl;

   total*= 320.; // 320 square degrees
   cout<< " total energy over 350<l<10, 2<|b|<10  (keV  cm-2 s-1) = "<<total<<endl;

   total*= 1.6e-12 * 1e3;// keV -> erg
   cout<< " total energy (erg  cm-2 s-1) = "<<total<<endl;

   double norm=3.54e-9/total;
   cout<<"norm="<<norm<<endl;

   for (i_energy=0;i_energy<n_energy;i_energy++)
   {
    emean=a[i_energy][0]*1e-3;
    ymin= (a[i_energy][2]-a[i_energy][3])*1e+3;   // keV^-1 -> MeV^-1
    ymax= (a[i_energy][2]+a[i_energy][3])*1e+3;

    ymin/=solid_angle;
    ymax/=solid_angle;
 
    ymin*=emean*emean;
    ymax*=emean*emean;

    ymin*=norm;
    ymax*=norm;


    if(ymin < galplotdef.gamma_spectrum_Imin)ymin = galplotdef.gamma_spectrum_Imin;
    //cout<<" RXTE emean ymin max "<<emean<<" "<<ymin<<" "<<ymax<<endl;

    line=new TLine(emean ,ymin,emean,ymax);// shift energy for visibility
    line->SetLineColor(kCyan);
    line->SetLineWidth(2  );
    line->Draw();
    }



  cout<<" <<<< plot_RXTE_spectrum    "<<endl;
   return status;
}

