

using namespace std;
#include"Galplot.h"                 
#include"galprop_classes.h"
#include"galprop.h"
#include"constants.h"
#include"fitsio.h"
#include"SPIERSP.h"
   

/////////////////////////////////////////////////////////////////////////
namespace COMPTEL//  to protect local variables and share with local routines
{
   int status=0;
   char  infile[1000];
   int id,hdunum;

 

int longprof,latprof;

int i_lat,i_long;
int ii_long,ii_lat;
int il_min,il_max,ib_min,ib_max;
int il_min1,il_max1,ib_min1,ib_max1;
int il_min2,il_max2,ib_min2,ib_max2;
int il_temp;

double long_min,long_max,lat_min,lat_max;
double l,b,l_off;

int    i_E_range;
double e_min,e_max;

char name[100],canvastitle[100], workstring1[100],workstring2[100],workstring3[100],workstring4[100];
char psfile[400];
}



/////////////////////////////////////////////////////////////
int Galplot::  plot_COMPTEL_profile(int longprof,int latprof,int mode)
///////////////////////////////////////////////////////////
{
  using namespace COMPTEL;

   cout<<" >>>> plot_COMPTEL_profile    "<<endl;


 for(i_E_range= 0; i_E_range<=2;i_E_range++)
 {
   // define the COMPTEL energy ranges

  if(i_E_range==0){ e_min= 1.0 ;  e_max= 3.0 ;}
  if(i_E_range==1){ e_min= 3.0 ;  e_max=10.0 ;}
  if(i_E_range==2){ e_min=10.0 ;  e_max=30.0 ;}

  

cout<<"e_min= "<<e_min<< " MeV " ;
cout<<"e_max= "<<e_max<< " MeV "<<endl;

 

TCanvas *c1;

TH1D *profile,*profile_CO,*profile_NIR;



int ncolors=1; int *colors=0;
gStyle->SetPalette(ncolors,colors);

  //====== see HowTo Style: and do before creating canvas
 
 
   TStyle *plain  = new TStyle("Plain","Plain Style (no colors/fill areas)"); 

   
   plain->SetCanvasBorderMode(0);
   plain->SetPadBorderMode(0);
   plain->SetPadColor(0);
   plain->SetCanvasColor(0);
   plain->SetTitleColor(0);
   plain->SetStatColor(0);
   gROOT->SetStyle("Plain");

 // names must be different or canvas disappears

  
  sprintf(name," %5.3f - %5.3f MeV",e_min,e_max);
  strcpy(canvastitle,"galdef_");
  strcat(canvastitle,galdef.galdef_ID);
  strcat(canvastitle,name);


  



  // --------------------------------- profiles

//------------------------------     axes      -------------------------
  TF1 *f1=new TF1("f1","-x",   0,180); // root manual p. 149
  TF1 *f2=new TF1("f2","-x", 180,340); // root manual p. 149

  //                      xmin ymin xmax ymax, funcname   ndiv chopt gridlength
  TGaxis *axis1=new TGaxis(-180,-90 ,  0.,-90  , "f1",       9,""                );
  axis1->SetLabelSize(0.03);
  TGaxis *axis2=new TGaxis(  20,-90 ,180.,-90  , "f2",       8,""                );
  axis2->SetLabelSize(0.03);
  axis2->SetTitle("Galactic longitude");
  // fill in 0-340 between two axis segments, unlabelled
  // at present has no visibile effect but could be useful if more minor tickmarks added
  //                         xmin ymin xmax ymax   wmin wmax ndiv chopt gridlength
  TGaxis *axis3=new TGaxis(  0,  -90.  , 20., -90., 0., 20. ,0, "U"               );

  // construct conventional Galactic longitude axis in two segments, 180-0 and 340-180
  f1=new TF1("f1","-x",   0,180); // root manual p. 149
  f2=new TF1("f2","-x", 180,340); // root manual p. 149

  //                      xmin ymin xmax ymax, funcname   ndiv chopt gridlength
          axis1=new TGaxis(-180,0.  ,  0.,0.   , "f1",       9,""                );
  axis1->SetLabelSize(0.02);
          axis2=new TGaxis(  20,0.  ,180.,0.   , "f2",       8,""                );
  axis2->SetLabelSize(0.02);
  axis2->SetTitle("Galactic longitude");

  
 //////////////////////////////////////////////////////////////////////////////////////////////





  // ----------------------------------longitude profile
  /*
  if(longprof==1)
    // not yet implemented
  {
  ib_min1=int((galplotdef.lat_min1-lat_min+.0001)/ fits_map.CDELT[1]);
  ib_max1=int((galplotdef.lat_max1-lat_min+.0001)/ fits_map.CDELT[1]);
  ib_min2=int((galplotdef.lat_min2-lat_min+.0001)/ fits_map.CDELT[1]);
  ib_max2=int((galplotdef.lat_max2-lat_min+.0001)/ fits_map.CDELT[1]);

  cout<<"ib_min1 ibmax1:"<<ib_min1<<" "<<ib_max1<<endl;
  cout<<"ib_min2 ibmax2:"<<ib_min2<<" "<<ib_max2<<endl;

  profile=     map->ProjectionX("profile1" ,ib_min1,ib_max1); // here name must be distinct
  profile->Add(map->ProjectionX("dummy1" ,  ib_min2,ib_max2),1.0);

  profile->Scale(1.0/(ib_max1-ib_min1+1 + ib_max2-ib_min2+1));
 
  profile->GetXaxis()->SetTitle("Galactic longitude");
  profile->GetXaxis()->SetNdivisions(0); // since axis drawn separately
  profile->GetYaxis()->SetTitle("intensity, cm^{-2} sr^{-1} s^{-1}  ");
  profile->GetYaxis()->SetTitleOffset(1.2);
  profile->GetYaxis()->SetLabelSize  (0.030);

  profile->SetTitle(""         ); //removes text written in box on plot
 
  
  if(galplotdef.lat_log_scale==0)
   profile->SetMaximum(profile->GetMaximum()*1.5); // to allow for data with larger range
  if(galplotdef.lat_log_scale==1)
   profile->SetMaximum(profile->GetMaximum()*2.0); 
  if(galplotdef.lat_log_scale==1)                     
   profile->SetMinimum(profile->GetMaximum()/1e5);   
  

  profile->SetMinimum(0.0  );// since profiles not yet defined
  profile->SetMaximum(1.e-3);

  TGaxis::SetMaxDigits(4);  // see root #1329 and mail root 20 Aug 2002

  profile->SetStats(kFALSE);
  profile->SetLineStyle(1     );// 1=solid 2=dash
  profile->SetLineWidth(3     );
  profile->SetLineColor(kBlack);


  strcpy(name,"longitude profile");
   strcpy(canvastitle,name);
 

  c1=new TCanvas(name,canvastitle,400,250,600,600);
  if(galplotdef.long_log_scale==1)c1->SetLogy(); //a TCanvas is a TPad, TPad has this function.  
  profile->Draw("L");

  

//------------------------------     axes      -------------------------
  

  axis1->Draw();   axis2->Draw();   axis3->Draw();

  }// longprof

  */



  // --------------------------- latitude profile


  if(latprof==1)
  {

 

    int n_lat=17;
    double latitude[n_lat],flux[n_lat],flux_err[n_lat],flux_CO[n_lat],flux_NIR[n_lat];
    double flux_zero_level;

 

    

 


    lat_min= -20.8 - 1.3; // first  bin lower boundary
    lat_max= +20.8 + 1.3; // last   bin upper boundary

    

    profile    =new TH1D("SPI skymap",    canvastitle,  n_lat,    lat_min, lat_max) ; 
    profile_CO =new TH1D("SPI skymap CO", canvastitle,  n_lat,    lat_min, lat_max) ; 
    profile_NIR=new TH1D("SPI skymap NIR",canvastitle,  n_lat,    lat_min, lat_max) ; 

    double dtr=acos(-1.0)/180.; // degrees to radians
    double solid_angle=48.0 * 2.6 *dtr*dtr; // 48*2.6 deg pixels  
  
    double average_intensity     = 0.0;
    double average_intensity_NIR = 0.0;
    double average_intensity_CO  = 0.0;
    
    int n_lat_av=0;

    double fudge_factor=1.00; //  flexible to compare the shapes

    double lat_min_av=-15.7; // check exactly which bins Bouchet used - this choice gives nearly the same total flux as in 17 Jan email
    double lat_max_av=+15.7;

    for(i_lat=0;i_lat<n_lat;i_lat++)
    {
 
     
      double intensity    =   (flux       [i_lat]-flux_zero_level)   /solid_angle;
      double intensity_err=    flux_err   [i_lat]                    /solid_angle;
      double intensity_CO    =(flux_CO    [i_lat] - flux_zero_level )/solid_angle;
      double intensity_NIR   =(flux_NIR   [i_lat] - flux_zero_level )/solid_angle;
     
      cout<<"b="<<latitude[i_lat]<<" COMPTEL flux-zero level="<<flux[i_lat]<<" solid_angle="<<solid_angle<<" intensity="<<intensity<<endl;


     


      ii_lat=i_lat+1; // TH1D bins start at 1

      profile->    SetBinContent(ii_lat,intensity);
      profile->    SetBinError  (ii_lat,intensity_err);

      profile_CO ->SetBinContent(ii_lat,intensity_CO);
      profile_NIR->SetBinContent(ii_lat,intensity_NIR);

      if(latitude[i_lat]>=lat_min_av && latitude[i_lat]<=lat_max_av)
      {
       average_intensity    += intensity;
       average_intensity_NIR+= intensity_NIR;
       average_intensity_CO += intensity_CO ;
       n_lat_av++;
      }
    

    } //for i_lat


    // ---------------- printout

 


   //-------------------------------------------------------------------------------------------------------------------------------------------


  profile->GetXaxis()->SetTitle("Galactic  latitude");
  profile->GetYaxis()->SetTitle("intensity, cm^{-2} sr^{-1} s^{-1}  ");
  profile->GetYaxis()->SetTitleOffset(1.2);
  profile->GetYaxis()->SetLabelSize  (0.030);

  profile->SetTitle(""         ); // removes text written in box on plot

  /*
   if(galplotdef.lat_log_scale==0)
   profile->SetMaximum(profile->GetMaximum()*1.5); // to allow for data with larger range
  if(galplotdef.lat_log_scale==1)
   profile->SetMaximum(profile->GetMaximum()*2.0); 
  if(galplotdef.lat_log_scale==1)                     
   profile->SetMinimum(profile->GetMaximum()/1e5);   
  */
 
  profile->SetMinimum(0.0  );// since profiles not yet defined
  profile->SetMaximum(3.e-3);// since profiles not yet defined


  TGaxis::SetMaxDigits(4);  

  profile->SetStats(kFALSE);
  profile->SetLineStyle(1     );// 1=solid 2=dash
  profile->SetLineWidth(3     );
  profile->SetLineColor(kBlack);

    

  
  sprintf(name,"COMPTEL latitude profile %d",i_E_range); // vary name and title to keep all plots on screen
  strcpy(canvastitle,name);

  c1=new TCanvas(name,canvastitle, 500+i_E_range*100, 350+i_E_range*100, 600,600);

     

 

                                                                     
  
  if(galplotdef.lat_log_scale==1)c1->SetLogy();                //a TCanvas is a TPad, TPad has this function
  profile->Draw("L");

  profile_CO ->SetLineColor(kCyan);
  profile_CO ->Draw("same L");                  // NB this is really NIR

  profile_NIR->SetLineColor(kMagenta);
  profile_NIR->Draw("same L");                  // NB this is really CO

  
  }

  // parameters labelling

  sprintf(workstring1,"  %5.2f<l<%5.2f , %5.2f<l<%5.2f",                 //AWS20040315
                          galplotdef.long_min1,galplotdef.long_max1,
                          galplotdef.long_min2,galplotdef.long_max2);
 
  sprintf(workstring2,"  %5.2f<b<%5.2f , %5.2f<b<%5.2f",                 //AWS20040315
                         galplotdef.lat_min1,galplotdef.lat_max1,
                         galplotdef.lat_min2,galplotdef.lat_max2);
  
  sprintf(workstring3," %5.3f - %5.3f MeV",e_min,e_max);


  strcpy(workstring4," galdef ID ");
  strcat(workstring4,galdef.galdef_ID);


  TText *text=new TText();
  text->SetTextFont(62);
  text->SetTextColor(kBlack);
  text->SetTextSize(0.025 );
  text->SetTextAlign(12);

  if(longprof)
  text->DrawTextNDC(.56 ,.85 ,workstring2);// NDC=normalized coord system  AWS20040315
  if(latprof)
  text->DrawTextNDC(.52 ,.85 ,workstring1);// NDC=normalized coord system  AWS20040315
  text->DrawTextNDC(.58 ,.88 ,workstring3);// NDC=normalized coord system
  text->DrawTextNDC(.20 ,.92 ,workstring4);// NDC=normalized coord system



  plot_COMPTEL_model_profile( longprof,  latprof, e_min , e_max ,    1     , 1  ,1  );


  //============== postscript output

  sprintf(workstring1,"l_%.2f_%.2f_%.2f_%.2f",                          
                          galplotdef.long_min1,galplotdef.long_max1,
                          galplotdef.long_min2,galplotdef.long_max2);

  sprintf(workstring2,"b_%.2f_%.2f_%.2f_%.2f",                         
                          galplotdef.lat_min1,galplotdef.lat_max1,
                          galplotdef.lat_min2,galplotdef.lat_max2);

  sprintf(workstring3,"%.3f-%.3f_MeV",e_min,e_max);



  strcpy(psfile,"plots/");
  if(longprof==1)
  strcat(psfile,"longitude_profile_");
  if(latprof==1)
  strcat(psfile,"latitude_profile_");
  if(   mode==1)
  strcat(psfile,"lego_"  
          );
  strcat(psfile,galdef.galdef_ID);
  strcat(psfile,"_"        );
  strcat(psfile,workstring3);
  strcat(psfile,"_"        );
  if(longprof==1)
  strcat(psfile,workstring2);
  if( latprof==1)
  strcat(psfile,workstring1);

  strcat(psfile,"_COMPTEL_");



  strcat(psfile,galplotdef.psfile_tag);

  strcat(psfile,".eps");

  cout<<"postscript file="<<psfile<<endl;
  c1->Print(psfile       );
  //==============




 }//i_E_range

   cout<<endl;
   cout<<" <<<< plot_COMPTEL_profile    "<<endl;
 
 return 0;
}
