#include"Galplot.h"                    //AWS20050920
#include"galprop_classes.h"
#include"galprop.h"
#include"constants.h"
#include"fitsio.h"


Distribution   COMPTEL_bremss_skymap;
Distribution  *COMPTEL_IC_iso_skymap;
Distribution   COMPTEL_total_skymap ;

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
int Galplot::plot_COMPTEL_model_profile(int longprof, int latprof,double e_min,double e_max, int ic,int bremss,int total)
{
   cout<<" >>>> plot_COMPTEL_model_profile    "<<endl;
   int status=0;

  

int i_comp,i_lat,i_long;
int ii_long,ii_lat;
int il_min,il_max,ib_min,ib_max;
int il_min1,il_max1,ib_min1,ib_max1;
int il_min2,il_max2,ib_min2,ib_max2;


double long_min;
double l,b,l_off;
double ic_total;
double isotropic_intensity;

char name[100],canvastitle[100], workstring1[100],workstring2[100],workstring3[100],workstring4[100];
char psfile[400];


TCanvas *c1;
TH2F *        ic_map;
TH2F *    bremss_map;
TH2F *     total_map;
TH2F * isotropic_map;

TH1D *profile;




  
 
 // names must be different or canvas disappears

  sprintf(name," %5.0f - %5.0f MeV",0,0);
  strcpy(canvastitle,"galdef_");
  strcat(canvastitle,galdef.galdef_ID);
  strcat(canvastitle,name);

  // integrate model over COMPTEL energy range
  convolve_COMPTEL(e_min,e_max);


  ic_map=new TH2F("IC skymap",canvastitle,
                  galaxy.n_long,       -180,           180.,
                  galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1));

  bremss_map=new TH2F("bremss skymap",canvastitle,
                  galaxy.n_long,       -180,           180.,
                  galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1));



      total_map=new TH2F("total     skymap",canvastitle,
                  galaxy.n_long,       -180,           180.,
                  galaxy.n_lat,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1));




	 for (i_lat =0;  i_lat <galaxy.n_lat;  i_lat++ )
         {
          for(i_long=0;  i_long<galaxy.n_long; i_long++)
          {

           l=galaxy.long_min+galaxy.d_long*i_long;
           b=galaxy. lat_min+galaxy. d_lat*i_lat;

  

// this would require shifting the array:
            ic_total=0;
            for(i_comp=0;i_comp<galaxy.n_ISRF_components;i_comp++)
                ic_total+= COMPTEL_IC_iso_skymap[i_comp].d2[i_long][i_lat].s[0];

            l_off=l-180.;
            if(l_off<0.)l_off+=360.0;
            ii_long= galaxy.n_long-l_off/galaxy.d_long;
	    //cout<<"l l_off ii_long"<<l<<" "<<l_off<<" "<<ii_long<<endl;

               ic_map    ->SetBinContent(ii_long,i_lat,ic_total);   
               bremss_map->SetBinContent(ii_long,i_lat,COMPTEL_bremss_skymap   .d2[i_long][i_lat].s[0]); 
                total_map->SetBinContent(ii_long,i_lat,COMPTEL_total_skymap    .d2[i_long][i_lat].s[0]); 

            }  //  i_long
           }   //  i_lat

	

//==============================     Profiles  =============================



  // NB reversed longitude axis starting at +180 and decreasing

  l_off=180.-galplotdef.long_max ; if(l_off>360.)l_off-=360.; if(l_off<  0.)l_off+=360.;
  il_min =l_off/galdef.d_long;
  l_off=180.-galplotdef.long_min ; if(l_off>360.)l_off-=360.; if(l_off<  0.)l_off+=360.;
  il_max =l_off/galdef.d_long;
  cout<<"il_min  ilmax :"<<il_min <<" "<<il_max <<endl;



  ib_min=(galplotdef.lat_min-galdef.lat_min+.0001)/galdef.d_lat;
  ib_max=(galplotdef.lat_max-galdef.lat_min+.0001)/galdef.d_lat;
  cout<<"ib_min ibmax:"<<ib_min<<" "<<ib_max<<endl;




  // two l or two b ranges


  l_off=180.-galplotdef.long_max1; if(l_off>360.)l_off-=360.; if(l_off<  0.)l_off+=360.;
  il_min1=l_off/galdef.d_long;
  l_off=180.-galplotdef.long_min1; if(l_off>360.)l_off-=360.; if(l_off<  0.)l_off+=360.;
  il_max1=l_off/galdef.d_long;
  cout<<"il_min1 ilmax1:"<<il_min1<<" "<<il_max1<<endl;

  l_off=180.-galplotdef.long_max2; if(l_off>360.)l_off-=360.; if(l_off<  0.)l_off+=360.;
  il_min2=l_off/galdef.d_long;
  l_off=180.-galplotdef.long_min2; if(l_off>360.)l_off-=360.; if(l_off<  0.)l_off+=360.;
  il_max2=l_off/galdef.d_long;
  cout<<"il_min2 ilmax2:"<<il_min2<<" "<<il_max2<<endl;


  ib_min1=(galplotdef.lat_min1-galdef.lat_min+.0001)/galdef.d_lat;
  ib_max1=(galplotdef.lat_max1-galdef.lat_min+.0001)/galdef.d_lat;
  cout<<"ib_min1 ibmax1:"<<ib_min1<<" "<<ib_max1<<endl;

  ib_min2=(galplotdef.lat_min2-galdef.lat_min+.0001)/galdef.d_lat;
  ib_max2=(galplotdef.lat_max2-galdef.lat_min+.0001)/galdef.d_lat;
  cout<<"ib_min2 ibmax2:"<<ib_min2<<" "<<ib_max2<<endl;






  long_min=-180.; // ic_map etc have longitude centred at 0.0



  //---------- total           

  if(total==1)
    {
     gStyle->SetHistLineColor(kBlue);//1=black 2=red 3=green 4=blue


     // -------------------------- longitude profile

  if(longprof)
    {

  sprintf(workstring2,"  %5.1f>b>%5.1f,%5.1f>b>%5.1f      ",
                         galplotdef.lat_min1,galplotdef.lat_max1,
                         galplotdef.lat_min2,galplotdef.lat_max2);
  strcpy(workstring1,canvastitle);
  strcat(workstring1,workstring2);

  strcat(name,"total_longitude");// to identify it: name is not plotted
  //  c1=new TCanvas(name,canvastitle,600+ip*10,150+ip*10,600,600);
  profile=     total_map->ProjectionX("total " , ib_min1,ib_max1);


  profile->Add(total_map->ProjectionX("total2" , ib_min2,ib_max2),1.0);
  profile->Scale(1.0/(ib_max1-ib_min1+1 + ib_max2-ib_min2+1));
  profile->GetXaxis()->SetTitle("Galactic longitude");
  profile->GetXaxis()->SetNdivisions(0); 
  profile->GetYaxis()->SetTitle("intensity, cm^{-2} sr^{-1} s^{-1}  ");
  profile->GetYaxis()->SetTitleOffset(1.2);
//profile->GetYaxis()->SetTitleSize  (0.001); seems to be problematic
  profile->GetYaxis()->SetLabelSize  (0.030);

  profile->SetTitle(""         ); //written in box on plot

  profile->SetMaximum(profile->GetMaximum()*1.5); // to allow for data with larger range
 
  TGaxis::SetMaxDigits(4);  // see root #1329 and mail root 20 Aug 2002

  profile->SetStats(kFALSE);
  profile->SetLineStyle(1     );// 1=solid 2=dash 3=dot
  profile->SetLineWidth(3     );
  profile->SetLineColor(kBlue);


  profile->Scale(1.0);//TEST
  profile->Draw("same L");//L=line
  


    }// if longprof



  // -------------------------- latitude profile

  if(latprof)
    {
      /*
  il_min=(galplotdef.long_min-       long_min+.0001)/galdef.d_long;
  il_max=(galplotdef.long_max-       long_min+.0001)/galdef.d_long;
  cout<<"il_min ilmax:"<<il_min<<" "<<il_max<<endl;
      */
  sprintf(workstring2,"  %5.1f>l>%5.1f,%5.1f>l>%5.1f       ",
                 	  galplotdef.long_min1,galplotdef.long_max1,
                          galplotdef.long_min2,galplotdef.long_max2);
  strcpy(workstring1,canvastitle);
  strcat(workstring1,workstring2);



  strcat(name,"total_latitude");
  //  c1=new TCanvas(name,canvastitle,650+ip*10,150+ip*10,600,600);
  //if(galplotdef.lat_log_scale==1)c1->SetLogy();                //a TCanvas is a TPad, TPad has this function

  profile=     total_map->ProjectionY("total3"     , il_min1,il_max1);
  profile->Add(total_map->ProjectionY("total4"     , il_min2,il_max2),1.0);
  profile->Scale(1./(il_max1-il_min1+1 +il_max2-il_min2+1 ));
  profile->GetXaxis()->SetTitle("Galactic latitude");
  profile->GetYaxis()->SetTitle("intensity, cm^{-2} sr^{-1} s^{-1}   ");
  profile->GetYaxis()->SetTitleOffset(1.2);
//profile->GetYaxis()->SetTitleSize  (0.001); seems to be problematic
  profile->GetYaxis()->SetLabelSize  (0.030);
  profile->SetTitle(""         ); //written in box on plot

  if(galplotdef.lat_log_scale==0)
   profile->SetMaximum(profile->GetMaximum()*1.5); // to allow for data with larger range
  if(galplotdef.lat_log_scale==1)
   profile->SetMaximum(profile->GetMaximum()*2.0); 
  if(galplotdef.lat_log_scale==1)                     //AWS20040301
   profile->SetMinimum(profile->GetMaximum()/1e5);    //AWS20040301


  TGaxis::SetMaxDigits(4); // see root #1329 and mail root 20 Aug 2002

  profile->SetStats(kFALSE);
  profile->SetLineStyle(1     );// 1=solid 2=dash
  profile->SetLineWidth(2    );
  profile->SetLineColor(kBlue);

  profile->Scale(1.0);//TEST
  profile->Draw("same L");//L=line




    }//if latprof



    }//if total



	  //----- inverse Compton

	  if(ic==1)
	    {
	      gStyle->SetHistLineColor(kGreen);// used to create histogram; 1=black 2=red 3=green 4=blue
	     
  if(longprof)
    {

      //strcat(name,"ic_longitude");// to identify it: name is not plotted
      //c1=new TCanvas(name,canvastitle,300+ip*10,150+ip*10,600,600);


  profile=     ic_map   ->ProjectionX("inverse Compton1",  ib_min1,ib_max1);       //AWS20040301 
  profile->Add(ic_map   ->ProjectionX("inverse Compton2" , ib_min2,ib_max2),1.0);
  profile->Scale(1.0/(ib_max1-ib_min1+1 + ib_max2-ib_min2+1));
  
  profile->GetXaxis()->SetTitle("Galactic longitude");
  profile->GetXaxis()->SetNdivisions(0); // removes longitude axis since done separately
  profile->GetYaxis()->SetTitle("intensity");
  profile->SetTitle(workstring1); //written in box on plot

  profile->SetLineStyle(2     );// 1=solid 2=dash
  profile->SetLineWidth(3     );
  profile->SetLineColor(kGreen);
 profile->Scale(1.0);//TEST
  profile->Draw("same L"); // L=line (instead of histogram)
  
    }//longprof
 

  if(latprof)
    {
      /*
  il_min=(galplotdef.long_min-       long_min+.0001)/galdef.d_long;
  il_max=(galplotdef.long_max-       long_min+.0001)/galdef.d_long;
  cout<<"il_min ilmax:"<<il_min<<" "<<il_max<<endl;
      */
  sprintf(workstring2,"  %5.1f>l>%5.1f",galplotdef.long_min,galplotdef.long_max);
  strcpy(workstring1,canvastitle);
  strcat(workstring1,workstring2);



  //  strcat(name,"ic_latitude");
  //  c1=new TCanvas(name,canvastitle,350+ip*10,150+ip*10,600,600);
  profile=     ic_map->ProjectionY("inverse Compton3", il_min1,il_max1);              //AWS20040301
  profile->Add(ic_map->ProjectionY("inverse Compton4", il_min2,il_max2),1.0);         //AWS20040301
  profile->Scale(1./(il_max1-il_min1+1 +il_max2-il_min2+1 ));

  profile->GetXaxis()->SetTitle("Galactic latitude");
  profile->GetYaxis()->SetTitle("intensity");
  profile->SetTitle(workstring1); //written in box on plot


  profile->SetLineStyle(1     );// 1=solid 2=dash 3=dot
  profile->SetLineWidth(2     );
  profile->SetLineColor(kGreen);
  profile->Scale(1.0);//TEST
  profile->Draw("same L");//L=line (instead of histogram)
    }//latprof



  }//if inverse Compton


  //------- bremss

  if(bremss==1)
    {
	      gStyle->SetHistLineColor(kCyan);// used to create histogram; 1=black 2=red 3=green 4=blue 5=yellow
  if(longprof==1)
    {
      //  strcat(name,"bremss_longitude");// to identify it: name is not plotted
      //  c1=new TCanvas(name,canvastitle,400+ip*10,150+ip*10,600,600);

  profile=     bremss_map->ProjectionX("bremsstrahlung1" , ib_min1,ib_max1);       //AWS20040301
  profile->Add(bremss_map->ProjectionX("bremsstrahlung2" , ib_min2,ib_max2),1.0);
  profile->Scale(1.0/(ib_max1-ib_min1+1 + ib_max2-ib_min2+1));
 
  profile->GetXaxis()->SetTitle("Galactic longitude");
  profile->GetXaxis()->SetNdivisions(0); 
  profile->GetYaxis()->SetTitle("intensity");
  profile->SetTitle(workstring1); //written in box on plot

  profile->SetLineStyle(4     );// 1=solid 2=dash 3=dot 4=dash-dot
  profile->SetLineWidth(3     );
  profile->SetLineColor(kCyan);
 profile->Scale(1.0);//TEST
  profile->Draw("same L");
  
    }//if longprof



  if(latprof==1)
  {
      /*
  il_min=(galplotdef.long_min-       long_min+.0001)/galdef.d_long;
  il_max=(galplotdef.long_max-       long_min+.0001)/galdef.d_long;
  cout<<"il_min ilmax:"<<il_min<<" "<<il_max<<endl;
      */
  sprintf(workstring2,"  %5.1f>l>%5.1f",galplotdef.long_min,galplotdef.long_max);
  strcpy(workstring1,canvastitle);
  strcat(workstring1,workstring2);



  //  strcat(name,"bremss_latitude");
  //  c1=new TCanvas(name,canvastitle,450+ip*10,150+ip*10,600,600);
  profile=     bremss_map->ProjectionY("bremsstrahlung3", il_min1,il_max1);          //AWS20040301
  profile->Add(bremss_map->ProjectionY("bremsstrahlung4", il_min2,il_max2),1.0);     //AWS20040301
  profile->Scale(1./(il_max1-il_min1+1 +il_max2-il_min2+1 ));


  profile->GetXaxis()->SetTitle("Galactic latitude");
  profile->GetYaxis()->SetTitle("intensity");
  profile->SetTitle(workstring1); //written in box on plot

  profile->SetLineStyle(1     );// 1=solid 2=dash 3=dot 4=dash-dot
  profile->SetLineWidth(2     );
  profile->SetLineColor(kCyan);

 profile->Scale(1.0);//TEST
  profile->Draw("same L");

    }//if latprof



  }//if bremsstrahlung


 

 




   cout<<" <<<< plot_COMPTEL_model_profile   "<<endl;
   return status;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////////////////////////////

 int Galplot::convolve_COMPTEL(double emin,double emax)
{
    
   cout<<">>>>convolve_COMPTEL "<<endl;




   int i_long,i_lat,ip,i_comp;
   double e1,e2, intensity_esq1,  intensity_esq2;
   double delta;
   

   int i_warn=0;

    COMPTEL_bremss_skymap.init             (galaxy.n_long,galaxy.n_lat,1);
  
    COMPTEL_IC_iso_skymap=new Distribution[galaxy.n_ISRF_components];

   for(i_comp=0;i_comp<galaxy.n_ISRF_components;i_comp++)
    COMPTEL_IC_iso_skymap[i_comp].init   (galaxy.n_long,galaxy.n_lat,1);
    
    COMPTEL_total_skymap    .init          (galaxy.n_long,galaxy.n_lat,1);
    

   for  (i_long   =0;  i_long<galaxy.n_long; i_long++)
   {
    cout<<i_long<<" ";
    for (i_lat    =0;  i_lat <galaxy.n_lat ; i_lat++)
    {
 

     //cout<<"i_long i_lat e_min e_max "<<i_long<<" "<<i_lat<<" "<<e_min<<" "<<emax<<endl;

      COMPTEL_bremss_skymap           .d2[i_long][i_lat].s[0]=0;

     for(i_comp=0;i_comp<galaxy.n_ISRF_components;i_comp++)
      COMPTEL_IC_iso_skymap[i_comp]   .d2[i_long][i_lat].s[0]=0;  

     // for (ip=0; ip<galaxy.n_E_gammagrid-1 ; ip++)   avoid last input energy @ [ip+1] since sometimes NULL for bremss

   for (ip=0; ip<galaxy.n_E_gammagrid-1 ; ip++)                    
     {
    
     e1=  galaxy.E_gamma[ip];
     e2=  galaxy.E_gamma[ip+1];
     intensity_esq1=galaxy.bremss_skymap.d2[i_long][i_lat].s[ip  ];
     intensity_esq2=galaxy.bremss_skymap.d2[i_long][i_lat].s[ip+1];

     // -------------------------------------------------- trap error in input map
     if(intensity_esq1<0.0||intensity_esq2<0.0)
     { 
       if(i_warn<10)
	 {
       cout<< " warning: bremsstrahlung intensity -ve ! this warning will not be repeated"<<endl;
   
       cout<<"i_long i_lat emin emax ip e1 e2 intensity_esq1 intensity_esq2 delta bremss "<<i_long<<" "<<i_lat<<" "<<emin<<" "<<emax<<" "
       	 << ip<<" "<<e1<<" "<<e2<<" " <<intensity_esq1<<" "<<intensity_esq2
	   <<" delta=" <<delta<<" bremss="<<COMPTEL_bremss_skymap   .d2[i_long][i_lat].s[0]<<endl;
       i_warn++;
	 }//if
     }

     //----------------------------------------------------------------------------

     delta=energy_integral( emin, emax, e1, e2, intensity_esq1,intensity_esq2);
     
       
     COMPTEL_bremss_skymap   .d2[i_long][i_lat].s[0] += delta;

     //if(i_long==146&&i_lat==198)
     //  cout<<"i_long i_lat emin emax ip e1 e2 intensity_esq1 intensity_esq2 delta bremss "<<i_long<<" "<<i_lat<<" "<<emin<<" "<<emax<<" "
     //  	 << ip<<" "<<e1<<" "<<e2<<" " <<intensity_esq1<<" "<<intensity_esq2
     //	   <<" delta=" <<delta<<" bremss="<<COMPTEL_bremss_skymap   .d2[i_long][i_lat].s[0]<<endl;
     //if(i_long==146)cout<<i_long<<" "<<i_lat<<" "<<COMPTEL_bremss_skymap   .d2[i_long][i_lat].s[0]<<endl;


   for(i_comp=0;i_comp<galaxy.n_ISRF_components;i_comp++)
     {
     intensity_esq1=galaxy.IC_iso_skymap[i_comp].d2[i_long][i_lat].s[ip  ];
     intensity_esq2=galaxy.IC_iso_skymap[i_comp].d2[i_long][i_lat].s[ip+1];

     delta=energy_integral( emin, emax, e1, e2, intensity_esq1,intensity_esq2);
     /*
     cout<<"emin emax e1 e2 intensity_esq1 intensity_esq2"<<" "<<emin<<" "<<emax<<" "
      <<e1<<" "<<e2<<" " <<intensity_esq1<<" "<<intensity_esq2
      <<" delta=" <<delta<<endl;
     */
     COMPTEL_IC_iso_skymap[i_comp]   .d2[i_long][i_lat].s[0] += delta;

     }//i_comp

   

     /*
     cout<<"emin emax e1 e2 intensity_esq1 intensity_esq2"<<" "<<emin<<" "<<emax<<" "
      <<e1<<" "<<e2<<" " <<intensity_esq1<<" "<<intensity_esq2
      <<" delta=" <<delta<<endl;
     */
  
     }//ip
   
   }// i_lat
  }// i_long


   cout<<endl;

 


   // total skymaps


COMPTEL_total_skymap =COMPTEL_bremss_skymap;


for(i_comp=0;i_comp<galaxy.n_ISRF_components;i_comp++)
 COMPTEL_total_skymap+=COMPTEL_IC_iso_skymap[i_comp];






 

 

 cout<<"COMPTEL_bremss_skymap            max="            <<COMPTEL_bremss_skymap.max()         <<endl;
for(i_comp=0;i_comp<galaxy.n_ISRF_components;i_comp++)
 cout<<"COMPTEL_IC_iso_skymap i_comp#"<< i_comp<<"   max="<<COMPTEL_IC_iso_skymap[i_comp].max() <<endl;
 cout<<"COMPTEL_total_skymap             max="            <<COMPTEL_total_skymap. max()         <<endl;

 

 

  cout<<"<<<<convolve_COMPTEL "<<endl;
   return 0;

}

