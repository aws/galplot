#ifndef SourceModel2_h
#define SourceModel2_h
#include "Model.h"
#include "Exposure.h"
#include "Counts.h"
#include "Psf.h"
#include "Sources.h"
#include "Variables.h"
#include "Skymap.h"
#include "SparseSkymap.h"
#include "Parameters.h"
#include <string>
#include <vector>

/**\brief A model which handles the point sources from the catalog.
 *
 * The sources to include in the model, and how, is controlled by a special
 * column in the source catalog, called FIT_TYPE.  It understands the following
 * values:
 *  - \e FIXED Include the source as the power law specified in the catalog.
 *  Do not fit.
 *	- \e PREFACTOR Include the source as a power law and vary the prefactor in
 *	the fit but keep the index at the original value.
 *	- \e INDEX Include the source as a power law and vary the index in the fit
 *	but keep the prefactor at the original value.
 *	- \e POWERLAW Include the source as a power law and vary both the prefactor
 *	and the index.
 *	- \e POWERLAWEXPCUTOFF Include the source as a power law with an
 *	exponential cut off.  Vary all three parameters of the model.  The initial
 *	value for the cut off is 10 GeV.
 *	- \e BROKENPOWERLAW Include the source as a broken power law.  Vary all
 *	four parameters of the model.  The initial value for the break energy is
 *	10 GeV and the initial value for the second index is the same as the first
 *	index.  The gradient calculation for this model has not been done, so it is
 *	disabled for now.
 *	- \e FREE Include the source and vary its spectral profile freely.  It uses
 *	one parameter per energy bin in the fit, so the number of parameters can
 *	grow quickly if the number of energy bins are not kept minimal.
 *
 *	For every other value in this column, the source is not included in the
 *	model.
 */
class SourceModel2 : public BaseModel {
	public:
		/** \brief The constructor takes the same parameters as the BaseModel constructor.
		 *
		 * Creates the fixed source maps, as well as the necessary sparse skymaps for sources that need to be fitted.
		 */
		SourceModel2(const Exposure &exposure, const CountsMap &counts, const Psf &psf, const Sources &sources, const Parameters &pars, const Skymap<char> &filter, const std::string &scalerVariable="FixedSourceScaling", unsigned int configure = 3);
		// Destructor needed to delete all the sparseSkymaps
		// dynamically allocated
		~SourceModel2();
		void getMap(const Variables &vars, Skymap<double> &map);
		gradMap getComponents(const Variables & vars, const std::string &prefix);
		void getGrads(const Variables &vars, const std::string & varName, Skymap<double> &map);
		gradMap getSecDer(const Variables & vars, const std::string & varName1, const std::string & varName2);
		void modifySources(const Variables &vars, Sources &sources) const;
		/** \brief Add skymap to fixed sources Skymap
		 *
		 * Used when bootstrapping all sources to skip the creation of the fixed
		 * source map each time.
		 */
		void addToFixedSources(const Skymap<double>&);
		//! Enum for defining the type of the variable
		enum varType {
			PREF, //!< Prefactor 
			INDX, //!< Index in a power law
			ECUT, //!< Cut off energy
			EBRE, //!< Break energy
			IND1, //!< First index in a broken power law
			IND2, //!< Second index in a broken power law
			SCAL, //!< Free scaling per bin
			UNKN //!< Unknown variable type
		};
	private:
		/** \brief Add a count spectra to a SparseSkymap
		 *
		 * Adds the convolved count spectra of the source with number
		 * SourceNumber to counts.
		 *
		 * \param SourceNumber is the number of the source to add to
		 * the map
		 * \param counts is the sparse skymap which the source is added
		 * to.
		 */
		void genCountMap(int SourceNumber, SparseSkymap<double> *counts) const;
		/** \brief Add count spectras to a SparseSkymap
		 */
		void genCountMaps(const std::valarray<int> &sourceNumbers, SparseSkymap<double> *counts) const;
		/** \brief Add count spectras to a SparseSkymap
		 */
		void genCountMaps(int numberOfSources, SparseSkymap<double> *counts) const;
		/** \brief Add count spectras to a SparseSkymap
		 */
		void genCountMaps(double lowFlux, SparseSkymap<double> *counts) const;
		/** \brief Find the type of the variable and index of a source given a
		 * variable name.
		 *
		 * \param varName is the name of the variable
		 * \param index is the source index
		 * \param type is the varType of the variable
		 */
		void getIndexAndType(const std::string & varName, int &index, varType &type) const;
		std::vector<int> fFixedSources, //!< Store indices of fixed sources
			fPrefactorSources, //!< Store indices of sources with prefactor fitting
			fIndexSources, //!< Store indices of sources with index fitting
			fPowerLawSources, //!< Store indices of sources with power law fitting
			fPowerLawExpCutOffSources, //!< Store indices of sources with power law fitting and exponential cutoff
			fBrokenPowerLawSources, //!< Store indices of sources with broken power law fitting
			fFreeSources; //!< Store indices of sources with one parameter per energy bin
		Skymap<double> ffixedSkymap; //!< Cache for the fixed sources.  They take a long time to compute
		std::map<int, SparseSkymap<double>* > fvariableSkymaps;  //!< We store the skymaps for the variable sources separately in a sparse skymap
		std::map<int, std::vector<double> > flastPars; //!< We cache the last known values for the variables to speed up calculations
		std::string ffixedScalerVar; //!< Name of the fixed sources skymap scaler variable.  Scaler not used if it is an empty string.
		double fpsfFraction; //!< The fraction of psf containment to use for the sources.
		double fscalerMin; //!< The minimum value for the free source scalers.
};

#endif
