#include"Galplot.h"                    //AWS20050920
#include"galprop_classes.h"
#include"galprop.h"





int Galplot::convolve_EGRET_HI_H2()
{
    
   cout<<">>>>convolve_EGRET_HI_H2 "<<endl;





   int i_long,i_lat,ip,i_E_EGRET,i_comp;
   double emin, emax, e1,e2, intensity_esq1,  intensity_esq2;
   double delta;
   double isotropic_intensity;

   int i_warn=0;

   int i_Ring,n_Ring;

   Distribution work1,work2;

   n_Ring=galaxy.HIR.n_zgrid;

     convolved.EGRET_HIR_skymap.init             (galaxy.n_long,galaxy.n_lat,n_Ring,data.n_E_EGRET);
   unconvolved.EGRET_HIR_skymap.init             (galaxy.n_long,galaxy.n_lat,n_Ring,data.n_E_EGRET);

     convolved.EGRET_H2R_skymap.init             (galaxy.n_long,galaxy.n_lat,n_Ring,data.n_E_EGRET);
   unconvolved.EGRET_H2R_skymap.init             (galaxy.n_long,galaxy.n_lat,n_Ring,data.n_E_EGRET);

     convolved.EGRET_HIT_skymap.init             (galaxy.n_long,galaxy.n_lat,       data.n_E_EGRET);//AWS20050103
   unconvolved.EGRET_HIT_skymap.init             (galaxy.n_long,galaxy.n_lat,       data.n_E_EGRET);//AWS20050103

     convolved.EGRET_H2T_skymap.init             (galaxy.n_long,galaxy.n_lat,       data.n_E_EGRET);//AWS20050103
   unconvolved.EGRET_H2T_skymap.init             (galaxy.n_long,galaxy.n_lat,       data.n_E_EGRET);//AWS20050103

                          work1.init             (galaxy.n_long,galaxy.n_lat,       data.n_E_EGRET);//AWS20050103
                          work2.init             (galaxy.n_long,galaxy.n_lat,       data.n_E_EGRET);//AWS20050103

  for (i_Ring=0; i_Ring<n_Ring; i_Ring++)
  { 
    // cout<<endl<<"i_Ring "<<i_Ring<<endl;

   for (i_long   =0;  i_long<galaxy.n_long; i_long++)
   {
   for (i_lat    =0;  i_lat <galaxy.n_lat ; i_lat++)
   {
   for (i_E_EGRET=0;  i_E_EGRET  <data.n_E_EGRET;i_E_EGRET++)
   {

     //cout<<"i_long i_lat i_E_EGRET "<<i_long<<" "<<i_lat<<" "<<i_E_EGRET<<endl;

     convolved.EGRET_HIR_skymap   .d3[i_long][i_lat][i_Ring].s[i_E_EGRET]=0;
     convolved.EGRET_H2R_skymap   .d3[i_long][i_lat][i_Ring].s[i_E_EGRET]=0;

   

    

     emin=data.E_EGRET[i_E_EGRET  ];
     emax=data.E_EGRET[i_E_EGRET+1];

     // for (ip=0; ip<galaxy.n_E_gammagrid-1 ; ip++)   avoid last input energy @ [ip+1] since sometimes NULL for bremss
   for (ip=0; ip<galaxy.n_E_gammagrid-1 ; ip++)                    
     {
    
     e1=  galaxy.E_gamma[ip];
     e2=  galaxy.E_gamma[ip+1];

     intensity_esq1 =galaxy.   bremss_HIR_skymap.d3[i_long][i_lat][i_Ring].s[ip  ];
     intensity_esq2 =galaxy.   bremss_HIR_skymap.d3[i_long][i_lat][i_Ring].s[ip+1];

     intensity_esq1+=galaxy.pi0_decay_HIR_skymap.d3[i_long][i_lat][i_Ring].s[ip  ];
     intensity_esq2+=galaxy.pi0_decay_HIR_skymap.d3[i_long][i_lat][i_Ring].s[ip+1];

     // -------------------------------------------------- trap error in input map
     if(intensity_esq1<0.0||intensity_esq2<0.0)
     { 
       if(i_warn<10)
	 {
       cout<< " warning: bremsstrahlung intensity -ve ! this warning will not be repeated"<<endl;
   
       cout<<"i_long i_lat emin emax ip e1 e2 intensity_esq1 intensity_esq2 delta bremss "<<i_long<<" "<<i_lat<<" "<<emin<<" "<<emax<<" "
       	 << ip<<" "<<e1<<" "<<e2<<" " <<intensity_esq1<<" "<<intensity_esq2
	   <<" delta=" <<delta<<" bremss="<<convolved.EGRET_bremss_skymap   .d2[i_long][i_lat].s[i_E_EGRET]<<endl;
       i_warn++;
	 }//if
     }

     //----------------------------------------------------------------------------

     delta=energy_integral( emin, emax, e1, e2, intensity_esq1,intensity_esq2);
     
       
     convolved.EGRET_HIR_skymap   .d3[i_long][i_lat][i_Ring].s[i_E_EGRET] += delta;

     //if(i_E_EGRET==1&&i_long==146&&i_lat==198)
     //  cout<<"i_long i_lat emin emax ip e1 e2 intensity_esq1 intensity_esq2 delta bremss "<<i_long<<" "<<i_lat<<" "<<emin<<" "<<emax<<" "
     //  	 << ip<<" "<<e1<<" "<<e2<<" " <<intensity_esq1<<" "<<intensity_esq2
     //	   <<" delta=" <<delta<<" bremss="<<convolved.EGRET_bremss_skymap   .d2[i_long][i_lat].s[i_E_EGRET]<<endl;
     //if(i_E_EGRET==1&&i_long==146)cout<<i_long<<" "<<i_lat<<" "<<convolved.EGRET_bremss_skymap   .d2[i_long][i_lat].s[i_E_EGRET]<<endl;

     if(galplotdef.verbose==-407) //selectable debug AWS20070713
      {
       if(i_Ring==4)//local ring
       {
       cout<<"convolve_EGRET_HI_H2: "
	   <<"ilong="<<i_long<<" i_lat="<<i_lat<<" i_Ring="<<i_Ring<<" emin="<<emin<<" emax="<<emax
           <<" ip="<< ip<<" e1="<<e1<<" e2="<<e2
	   <<" pi0_decay_HIR[ip]=  "<<galaxy.pi0_decay_HIR_skymap.d3[i_long][i_lat][i_Ring].s[ip  ]
	   <<" pi0_decay_HIR[ip+1]="<<galaxy.pi0_decay_HIR_skymap.d3[i_long][i_lat][i_Ring].s[ip+1]
           <<" intensity_esq1=" <<intensity_esq1<<" intensity_esq2="<<intensity_esq2
     	   <<" delta=" <<delta<<" EGRET_HIR_skymap   ="<<convolved.EGRET_HIR_skymap   .d3[i_long][i_lat][i_Ring].s[i_E_EGRET];
           if(delta>0.0)cout<<"*";
           cout<<endl;
       }
      }

     intensity_esq1 =galaxy.   bremss_H2R_skymap.d3[i_long][i_lat][i_Ring].s[ip  ];
     intensity_esq2 =galaxy.   bremss_H2R_skymap.d3[i_long][i_lat][i_Ring].s[ip+1];

     intensity_esq1+=galaxy.pi0_decay_H2R_skymap.d3[i_long][i_lat][i_Ring].s[ip  ];
     intensity_esq2+=galaxy.pi0_decay_H2R_skymap.d3[i_long][i_lat][i_Ring].s[ip+1];



     delta=energy_integral( emin, emax, e1, e2, intensity_esq1,intensity_esq2);

     /*
     cout<<"emin emax e1 e2 intensity_esq1 intensity_esq2"<<" "<<emin<<" "<<emax<<" "
      <<e1<<" "<<e2<<" " <<intensity_esq1<<" "<<intensity_esq2
      <<" delta=" <<delta<<endl;
     */
   


     convolved.EGRET_H2R_skymap   .d3[i_long][i_lat][i_Ring].s[i_E_EGRET] += delta;
     }

     }//i_E_EGRET
    }//i_lat
   }//i_long
  }//i_Ring

   cout<<endl;


   if(galplotdef.verbose==-401)//selectable debug
     {
   cout<<"convolved.EGRET_HIR_skymap before convolution:"<<endl; convolved.EGRET_HIR_skymap.print();
   cout<<"convolved.EGRET_H2R_skymap before convolution:"<<endl; convolved.EGRET_H2R_skymap.print();
     }

   // save unconvolved maps before in-place convolution
    unconvolved.EGRET_HIR_skymap   =convolved.EGRET_HIR_skymap;
    unconvolved.EGRET_H2R_skymap   =convolved.EGRET_H2R_skymap;
 

   


   if(galplotdef.convolve_EGRET>=1)
   {

   for (i_Ring=0; i_Ring<n_Ring; i_Ring++) //AWS20050103
  { 
   for (i_long   =0;  i_long<galaxy.n_long; i_long++)
   {
    for (i_lat    =0;  i_lat <galaxy.n_lat ; i_lat++)
    {
     for (i_E_EGRET=0;  i_E_EGRET  <data.n_E_EGRET;i_E_EGRET++)
     {
      work1.d2[i_long][i_lat].s[i_E_EGRET] =unconvolved.EGRET_HIR_skymap.d3[i_long][i_lat][i_Ring].s[i_E_EGRET]  ;
      work2.d2[i_long][i_lat].s[i_E_EGRET] =unconvolved.EGRET_H2R_skymap.d3[i_long][i_lat][i_Ring].s[i_E_EGRET]  ;
     }//i_E_EGRET
    }//i_lat
   }//i_long

      cout<<endl<<"convolve_EGRET_HI_H2: HIR convolving i_Ring "<<i_Ring<<endl;
      convolve(work1,   data.EGRET_psf);

      cout<<endl<<"convolve_EGRET_HI_H2: H2R convolving i_Ring "<<i_Ring<<endl;
      convolve(work2,   data.EGRET_psf);

  for (i_long   =0;  i_long<galaxy.n_long; i_long++)
   {
   for (i_lat    =0;  i_lat <galaxy.n_lat ; i_lat++)
   {
    for (i_E_EGRET=0;  i_E_EGRET  <data.n_E_EGRET;i_E_EGRET++)
    {
     convolved.EGRET_HIR_skymap.d3[i_long][i_lat][i_Ring].s[i_E_EGRET] =  work1.d2[i_long][i_lat].s[i_E_EGRET] ;
     convolved.EGRET_H2R_skymap.d3[i_long][i_lat][i_Ring].s[i_E_EGRET] =  work2.d2[i_long][i_lat].s[i_E_EGRET] ;   
    }//i_E_EGRET
   }//i_lat
  }//i_long



  }//i_Ring


 }//if





   for (i_Ring=0; i_Ring<n_Ring; i_Ring++) //AWS20050103
  { 
    cout<<endl<<"i_Ring "<<i_Ring<<endl;

   for (i_long   =0;  i_long<galaxy.n_long; i_long++)
   {
   for (i_lat    =0;  i_lat <galaxy.n_lat ; i_lat++)
   {
   for (i_E_EGRET=0;  i_E_EGRET  <data.n_E_EGRET;i_E_EGRET++)
   {

    

     unconvolved.EGRET_HIT_skymap.d2[i_long][i_lat].s[i_E_EGRET]+=unconvolved.EGRET_HIR_skymap.d3[i_long][i_lat][i_Ring].s[i_E_EGRET]  ;
     unconvolved.EGRET_H2T_skymap.d2[i_long][i_lat].s[i_E_EGRET]+=unconvolved.EGRET_H2R_skymap.d3[i_long][i_lat][i_Ring].s[i_E_EGRET]  ;

       convolved.EGRET_HIT_skymap.d2[i_long][i_lat].s[i_E_EGRET]+=  convolved.EGRET_HIR_skymap.d3[i_long][i_lat][i_Ring].s[i_E_EGRET]  ;
       convolved.EGRET_H2T_skymap.d2[i_long][i_lat].s[i_E_EGRET]+=  convolved.EGRET_H2R_skymap.d3[i_long][i_lat][i_Ring].s[i_E_EGRET]  ;

    }//i_E_EGRET
    }//i_lat
   }//i_long
  }//i_Ring




// printouts

 if(galplotdef.verbose==-401||galplotdef.verbose==-403 )//selectable debug
 {
   cout<<"unconvolved.EGRET_HIR_skymap:"<<endl; unconvolved.EGRET_HIR_skymap.print();
   cout<<"unconvolved.EGRET_HIT_skymap:"<<endl; unconvolved.EGRET_HIT_skymap.print();

   cout<<"unconvolved.EGRET_H2R_skymap:"<<endl; unconvolved.EGRET_H2R_skymap.print();
   cout<<"unconvolved.EGRET_H2T_skymap:"<<endl; unconvolved.EGRET_H2T_skymap.print();
 }

  if(galplotdef.verbose==-402||galplotdef.verbose==-403 )//selectable debug
  {
   cout<<"  convolved.EGRET_HIR_skymap:"<<endl;   convolved.EGRET_HIR_skymap.print();
   cout<<"  convolved.EGRET_HIT_skymap:"<<endl;   convolved.EGRET_HIT_skymap.print();

   cout<<"  convolved.EGRET_H2R_skymap:"<<endl;   convolved.EGRET_H2R_skymap.print();
   cout<<"  convolved.EGRET_H2T_skymap:"<<endl;   convolved.EGRET_H2T_skymap.print();
  }

 if(galplotdef.verbose==-404                           )//selectable debug
 {
   cout<<"unconvolved.EGRET_pi0_decay_skymap:"<<endl; unconvolved.EGRET_pi0_decay_skymap.print();
   cout<<"unconvolved.EGRET_bremss_skymap   :"<<endl; unconvolved.EGRET_bremss_skymap   .print();
   cout<<"unconvolved.EGRET_HIT_skymap      :"<<endl; unconvolved.EGRET_HIT_skymap      .print();
   cout<<"unconvolved.EGRET_H2T_skymap      :"<<endl; unconvolved.EGRET_H2T_skymap      .print();
 }

 if(galplotdef.verbose==-405                           )//selectable debug
 {
   cout<<"unconvolved.EGRET_HIR_skymap:"<<endl; unconvolved.EGRET_HIR_skymap.print();
   cout<<"unconvolved.EGRET_HIT_skymap:"<<endl; unconvolved.EGRET_HIT_skymap.print();
 }

 if(galplotdef.verbose==-406                           )//selectable debug
 {
   cout<<" galaxy.pi0_decay_HIR_skymap     :"<<endl; galaxy.pi0_decay_HIR_skymap.print();
 }


 cout<<"unconvolved.EGRET_bremss_skymap    max="<<unconvolved.EGRET_bremss_skymap.max()<<endl;
 cout<<"unconvolved.EGRET_pi0_decay_skymap max="<<unconvolved.EGRET_pi0_decay_skymap.max()<<endl;
 cout<<"unconvolved.EGRET_isotropic_skymap max="<<unconvolved.EGRET_isotropic_skymap.max()<<endl;
 cout<<"unconvolved.EGRET_total_skymap     max="<<unconvolved.EGRET_total_skymap.max()<<endl;
 cout<<"  convolved.EGRET_bremss_skymap    max="<<  convolved.EGRET_bremss_skymap.max()<<endl;
 cout<<"  convolved.EGRET_pi0_decay_skymap max="<<  convolved.EGRET_pi0_decay_skymap.max()<<endl;
 cout<<"  convolved.EGRET_total_skymap     max="<<  convolved.EGRET_total_skymap.max()<<endl;

 cout<<"unconvolved.EGRET_HIR_skymap       max="<<unconvolved.EGRET_HIR_skymap.max()<<endl;
 cout<<"unconvolved.EGRET_HIT_skymap       max="<<unconvolved.EGRET_HIT_skymap.max()<<endl;
 cout<<"unconvolved.EGRET_H2R_skymap       max="<<unconvolved.EGRET_H2R_skymap.max()<<endl;
 cout<<"unconvolved.EGRET_H2T_skymap       max="<<unconvolved.EGRET_H2T_skymap.max()<<endl;

 cout<<"  convolved.EGRET_HIR_skymap       max="<<  convolved.EGRET_HIR_skymap.max()<<endl;
 cout<<"  convolved.EGRET_HIT_skymap       max="<<  convolved.EGRET_HIT_skymap.max()<<endl;
 cout<<"  convolved.EGRET_H2R_skymap       max="<<  convolved.EGRET_H2R_skymap.max()<<endl;
 cout<<"  convolved.EGRET_H2T_skymap       max="<<  convolved.EGRET_H2T_skymap.max()<<endl;

 cout<<"<<<<convolve_EGRET_HI_H2 "<<endl;

 return 0;

}
