

#include"Galplot.h"                   
#include"galprop_classes.h"
#include"galprop.h"




int Galplot::set_output()
{
   cout<<" >>>> set_output   "<<endl;
   int status=0;



char name[100],canvastitle[100], workstring1[100],workstring2[100];
                             
//char txt_stream_file[400];                             



  //============== postscript, gif and txt files

  sprintf(workstring1,"l_%.2f_%.2f_%.2f_%.2f",                            //AWS20040315
                          galplotdef.long_min1,galplotdef.long_max1,
                          galplotdef.long_min2,galplotdef.long_max2);

  sprintf(workstring2,"b_%.2f_%.2f_%.2f_%.2f",                            //AWS20040315
                          galplotdef.lat_min1,galplotdef.lat_max1,
                          galplotdef.lat_min2,galplotdef.lat_max2);

  strcpy(txt_stream_file,"plots/");
  strcat(txt_stream_file,"results_");
  strcat(txt_stream_file,galdef.galdef_ID);
  strcat(txt_stream_file,"_"        );
  strcat(txt_stream_file,workstring1);
  strcat(txt_stream_file,"_"        );
  strcat(txt_stream_file,workstring2);

  /* AWS20110721
  if(galplotdef.convolve_EGRET==0)
  strcat(txt_stream_file,"_unconv_"  );
  if(galplotdef.convolve_EGRET!=0)
  strcat(txt_stream_file,"_conv_"    );
  */

  if(galplotdef.convolve_GLAST==0)               //AWS20110721
  strcat(txt_stream_file,"_unconv_"  );
  if(galplotdef.convolve_GLAST!=0)               //AWS20110721
  strcat(txt_stream_file,"_conv_"    );

  strcat(txt_stream_file,galplotdef.psfile_tag);

  
 
  

  strcat(txt_stream_file,".txt");                       //AWS20051124
  cout<<txt_stream_file<<endl;                          //AWS20051124
  txt_stream.open(txt_stream_file);                      //AWS20051124


  if(txt_stream==NULL)
  {
   cout<<"cannot open "<<txt_stream_file<<endl;
   exit(0);
  }

  txt_stream<<" Results from galplot"<<endl;    //AWS20051124
  txt_stream<<txt_stream_file<<endl;


 
  



   cout<<" <<<<set_output   "<<endl;
   return status;
}





















