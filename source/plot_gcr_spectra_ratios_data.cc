#include"Galplot.h"                    //AWS20050920
#include"galprop_classes.h"
#include"galprop.h"
#include"constants.h"
#include"fitsio.h"


int Galplot::plot_gcr_spectra_ratios_data(int i_ratio)
{
   cout<<" >>>> plot_gcr_spectra_ratios_data    "<<endl;
   int status=0;

GCR_data gcr_data;

char *database_file=new char[400];
char *area_units  =new char[4];
char *energy_units=new char[4];
char *Y_name      =new char[20];
int n_ZA_numerator_select,   *Z_numerator_select,  *A_numerator_select;
int n_ZA_denominator_select, *Z_denominator_select,*A_denominator_select;




int ip;
int i_ZA;                 // index of species in galplotdef parameters



char name[100],canvastitle[100], workstring1[100],workstring2[100];


TGraphAsymmErrors *spectrum;
TText *text;



 
 n_ZA_numerator_select=3;
 Z_numerator_select=new int[n_ZA_numerator_select];
 A_numerator_select=new int[n_ZA_numerator_select];
 n_ZA_denominator_select=3;
 Z_denominator_select=new int[n_ZA_denominator_select];
 A_denominator_select=new int[n_ZA_denominator_select];

strcpy(Y_name,       "ratio");
strcpy(area_units,   "cm2");
strcpy(energy_units, "MeV");
//strcpy(database_file,"GCR_data_1.dat");           //AWS20090220
strcpy(database_file,galplotdef.gcr_database_file); //AWS20090220

    for(i_ZA=0;i_ZA<n_ZA_numerator_select;i_ZA++)
    {
      Z_numerator_select[i_ZA]=0;
      A_numerator_select[i_ZA]=0;
    }
    for(i_ZA=0;i_ZA<n_ZA_denominator_select;i_ZA++)
    {
      Z_denominator_select[i_ZA]=0;
      A_denominator_select[i_ZA]=0;
    }
    for(i_ZA=0;i_ZA<galplotdef.gcr_spectra_n_sec_ZA[i_ratio];i_ZA++)
    {
      Z_numerator_select[i_ZA]= galplotdef.gcr_spectra_sec_Z[i_ratio][i_ZA];
      A_numerator_select[i_ZA]= galplotdef.gcr_spectra_sec_A[i_ratio][i_ZA];
     }
    for(i_ZA=0;i_ZA<galplotdef.gcr_spectra_n_pri_ZA[i_ratio];i_ZA++)
    {
      Z_denominator_select[i_ZA]= galplotdef.gcr_spectra_pri_Z[i_ratio][i_ZA];
      A_denominator_select[i_ZA]= galplotdef.gcr_spectra_pri_A[i_ratio][i_ZA];
     }


    /* == test case
 Z_numerator_select[0]=5;
 A_numerator_select[0]=10;
 Z_numerator_select[1]=5;
 A_numerator_select[1]=11;
 Z_numerator_select[2]=0;
 A_numerator_select[2]=0; 

 Z_denominator_select[0]=6;
 A_denominator_select[0]=12;
 Z_denominator_select[1]=6;
 A_denominator_select[1]=13;
 Z_denominator_select[2]=0;
 A_denominator_select[2]=0;
    */

 gcr_data.read(database_file ,       area_units ,       energy_units ,
	       Y_name,
	       n_ZA_numerator_select,       Z_numerator_select,       A_numerator_select,
               n_ZA_denominator_select,     Z_denominator_select,     A_denominator_select);

 gcr_data.print();



 /*
  spectrum=new TGraphAsymmErrors(gcr_data.n    );

 
  
  for  (ip    =0;  ip    <gcr_data.n    ;ip    ++)
    {
   spectrum->SetPoint     (ip,gcr_data. E_mean[ip],  gcr_data.value [ip]    );

   spectrum->SetPointError(ip,gcr_data. E_mean  [ip]-  gcr_data. E_low  [ip],
                              gcr_data. E_high  [ip]-  gcr_data. E_mean[ ip],
			      gcr_data.err_minus[ip],  gcr_data.err_plus[ip]    );

    }


  //Gtypes.h:enum EColor { kWhite, kBlack, kRed, kGreen, kBlue, kYellow, kMagenta, kCyan };
  
  spectrum->SetMarkerColor(kBlue);
  spectrum->SetMarkerStyle(21); 
  spectrum->SetMarkerStyle(22); // triangles
  spectrum->SetMarkerSize (2.0);// 
  spectrum->SetLineColor(kBlue );
  
  //   cout<<"Z,A ="<<Z<<","<<A<< "
  cout<< "ratio: experiment: "<<  gcr_data.experiment[ip]
      <<" colour= "<<gcr_data.color[ip]<<" style="<<gcr_data.style[ip]<<" size="<<gcr_data.size[ip]
      <<" E_mean="<<gcr_data. E_mean[ip]<<" plot value="<<  gcr_data.value [ip] <<endl;       

  spectrum->SetMarkerColor(gcr_data.color[ip]);                          //AWS20060621
  spectrum->SetMarkerStyle(gcr_data.style[ip]); // see TAttMarker        //AWS20060621
  spectrum->SetMarkerSize (gcr_data.size [ip]);                          //AWS20060621
  spectrum->SetLineColor  (gcr_data.color[ip] );                         //AWS20060621

  spectrum->Draw(" P ");  // points as markers                       
*/


 // modified to enable different marker colours, shapes per point  //AWS20060621

 for  (ip    =0;  ip    <gcr_data.n    ;ip    ++)
 {
   spectrum=new TGraphAsymmErrors(1   );
   spectrum->SetPoint     (0 ,gcr_data. E_mean[ip],  gcr_data.value [ip]    );

   spectrum->SetPointError(0 ,gcr_data. E_mean  [ip]-  gcr_data. E_low  [ip],
                              gcr_data. E_high  [ip]-  gcr_data. E_mean[ ip],
			      gcr_data.err_minus[ip],  gcr_data.err_plus[ip]    );

  cout<< "ratio: experiment: "<<  gcr_data.experiment[ip]
      <<" colour= "<<gcr_data.color[ip]<<" style="<<gcr_data.style[ip]<<" size="<<gcr_data.size[ip]
      <<" E_mean="<<gcr_data. E_mean[ip]<<" plot value="<<  gcr_data.value [ip] <<endl;       

  spectrum->SetMarkerColor(gcr_data.color[ip]);                          //AWS20060621
  spectrum->SetMarkerStyle(gcr_data.style[ip]); // see TAttMarker        //AWS20060621
  spectrum->SetMarkerSize (gcr_data.size [ip]);                          //AWS20060621
  spectrum->SetLineColor  (gcr_data.color[ip] );                         //AWS20060621

  spectrum->Draw(" P ");  // points as markers                       

  }


   cout<<" <<<< plot_gcr_spectra_ratios_data   "<<endl;
   return status;
}


