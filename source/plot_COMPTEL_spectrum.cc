
#include"Galplot.h"                    //AWS20050920
#include"galprop_classes.h"
#include"galprop.h"
#include"constants.h"
#include"fitsio.h"


void   differential_spectrum(double e1,double e2, double intensity, double g,double &intensity_esq1,double &intensity_esq2);
void   differential_spectrum(double *e,    int n, double *intensity,         double *intensity_esq1,double *intensity_esq2,
                             double *Ebar,double *Ibar);



int Galplot::plot_COMPTEL_spectrum(int mode)
{
   cout<<" >>>> plot_COMPTEL_spectrum    "<<endl;
   int status=0;


int i_comp,i_lat,i_long,i_E_COMPTEL;
int ii_long,ii_lat;
int il_min,il_max,ib_min,ib_max;
int il_min1,il_max1,ib_min1,ib_max1;
int il_min2,il_max2,ib_min2,ib_max2;
double l,b,l_off;
double exposure, intensity,*intensity_,*intensity_esq1,*intensity_esq2;
double *Ebar,*Ibar;
double long_min,lat_min;
double d_long, d_lat;
double *g;
double *tof_factor;//AWS20041015
double segment[2],Ebar_segment[2];

char name[100],canvastitle[100], workstring1[100],workstring2[100];
int errorbar; double errorbarfactor;
double error_bar_COMPTEL;//AWS20040115

TCanvas *c1;
TH3F    *comptel_map;
TH1D    *profile;  
TH1D *profile11,*profile12,*profile21,*profile22; // four (l,b) intervals

TGraph  *spectrum;



int ncolors=1; int *colors=0;
gStyle->SetPalette(ncolors,colors);


 cout<<data.COMPTEL_intensity.n_rgrid<<" "<<data.COMPTEL_intensity.n_zgrid<<endl;

  
 
 
 g=new double[data.n_E_COMPTEL];
 g[0]=2.0;
 g[1]=2.0;
 g[2]=2.0;
  
// TOF correction from ~aws/diffuse/comptel/intensity_spectra.pro
tof_factor= new double[data.n_E_COMPTEL];//AWS20041015
tof_factor[0]=1.237;
tof_factor[1]=1.307;
tof_factor[2]=1.058;



  // COMPTEL map is 2D Distribution, hence coords are formally r, z

  comptel_map=new TH3F(canvastitle,canvastitle,
                     data.COMPTEL_intensity.n_rgrid,       -180,           180.,
		     data.COMPTEL_intensity.n_zgrid,galaxy. lat_min,galaxy. lat_min+galaxy.d_lat*(galaxy.n_lat-1),
                     data.COMPTEL_intensity.n_pgrid,data.E_COMPTEL[0],data.E_COMPTEL[data.n_E_COMPTEL]);


  long_min=    0.5;//specific for COMPTEL data 
   lat_min  =-89.5;
  d_long      =1.0;     
  d_lat       =1.0;

        for(i_E_COMPTEL=0; i_E_COMPTEL<data.n_E_COMPTEL                    ; i_E_COMPTEL++)
        {
	 for (i_lat =0;  i_lat <data.COMPTEL_intensity.n_zgrid            ; i_lat++    )
         {
          for(i_long=0;  i_long<data.COMPTEL_intensity.n_rgrid            ; i_long++   )
          {

                
            intensity=data.COMPTEL_intensity.  d2[i_long][i_lat].s[i_E_COMPTEL];


            intensity*=sabin(lat_min,d_long,d_lat,i_lat); // multiply by solid angle of bin
           
            intensity*=tof_factor[i_E_COMPTEL];           //AWS20041015
  
            comptel_map    ->SetBinContent( i_long,i_lat,i_E_COMPTEL,intensity);   
  

            }// i_long
	   }// i_lat
          }// i_E_COMPTEL

 
 
  // NB for COMPTEL longitude increasing from 0 - 360

  l_off=     galplotdef.long_min1; if(l_off>360.)l_off-=360.; if(l_off<  0.)l_off+=360.;
  il_min1=l_off/d_long;
  l_off=     galplotdef.long_max1; if(l_off>360.)l_off-=360.; if(l_off<  0.)l_off+=360.;
  il_max1=l_off/d_long;
  cout<<"il_min1 ilmax1:"<<il_min1<<" "<<il_max1<<endl;

  l_off=     galplotdef.long_min2; if(l_off>360.)l_off-=360.; if(l_off<  0.)l_off+=360.;
  il_min2=l_off/d_long;
  l_off=     galplotdef.long_max2; if(l_off>360.)l_off-=360.; if(l_off<  0.)l_off+=360.;
  il_max2=l_off/d_long;
  cout<<"il_min2 ilmax2:"<<il_min2<<" "<<il_max2<<endl;




  ib_min1=(galplotdef.lat_min1-lat_min+.0001)/d_lat;
  ib_max1=(galplotdef.lat_max1-lat_min+.0001)/d_lat;
  cout<<"ib_min1 ibmax1:"<<ib_min1<<" "<<ib_max1<<endl;

  ib_min2=(galplotdef.lat_min2-lat_min+.0001)/d_lat;
  ib_max2=(galplotdef.lat_max2-lat_min+.0001)/d_lat;
  cout<<"ib_min2 ibmax2:"<<ib_min2<<" "<<ib_max2<<endl;

  double sa_norm_11,   sa_norm_12,   sa_norm_21,   sa_norm_22;
         sa_norm_11=0.;sa_norm_12=0.;sa_norm_21=0.;sa_norm_22=0.;


          for (i_long=il_min1;  i_long<=il_max1           ; i_long++   )//AWS20030910
	  for (i_lat =ib_min1;  i_lat <=ib_max1           ; i_lat++    )//AWS20030910
              sa_norm_11+=sabin(lat_min,d_long,d_lat,i_lat); // multiply by solid angle of bin


          for (i_long=il_min1;  i_long<=il_max1           ; i_long++   )//AWS20030910
          for (i_lat =ib_min2;  i_lat <=ib_max2           ; i_lat++    )//AWS20030910
              sa_norm_12+=sabin(lat_min,d_long,d_lat,i_lat); 

          for (i_long=il_min2;  i_long<=il_max2           ; i_long++   )//AWS20030910
	  for (i_lat =ib_min1;  i_lat <=ib_max1           ; i_lat++    )//AWS20030910
              sa_norm_21+=sabin(lat_min,d_long,d_lat,i_lat); // multiply by solid angle of bin


          for (i_long=il_min2;  i_long<=il_max2           ; i_long++   )//AWS20030910
          for (i_lat =ib_min2;  i_lat <=ib_max2           ; i_lat++    )//AWS20030910
              sa_norm_22+=sabin(lat_min,d_long,d_lat,i_lat); 


  profile11=comptel_map->ProjectionZ("galplot_COMPTEL11a" ,il_min1,il_max1, ib_min1,ib_max1);//AWS20040312
  profile12=comptel_map->ProjectionZ("galplot_COMPTEL12a" ,il_min1,il_max1, ib_min2,ib_max2);
  profile21=comptel_map->ProjectionZ("galplot_COMPTEL21a" ,il_min2,il_max2, ib_min1,ib_max1);
  profile22=comptel_map->ProjectionZ("galplot_COMPTEL22a" ,il_min2,il_max2, ib_min2,ib_max2);
  profile11->Scale(1./sa_norm_11);
  profile12->Scale(1./sa_norm_12);
  profile21->Scale(1./sa_norm_21);
  profile22->Scale(1./sa_norm_22);


  profile  =comptel_map->ProjectionZ("galplot_COMPTEL"    ,il_min1,il_max1, ib_min1,ib_max1);

  for(i_E_COMPTEL=0; i_E_COMPTEL<data.n_E_COMPTEL                    ; i_E_COMPTEL++)
   profile->SetBinContent(i_E_COMPTEL,0);

  profile->Add(profile11,0.25);
  profile->Add(profile12,0.25);
  profile->Add(profile21,0.25);
  profile->Add(profile22,0.25);

  
 // background
  double long_min1_back;
  double long_min2_back;
  double long_max1_back;
  double long_max2_back;
  double  lat_min1_back;
  double  lat_min2_back;
  double  lat_max1_back;
  double  lat_max2_back;
  long_min1_back=60.;
  long_max1_back=300;
  long_min2_back=60.;
  long_max2_back=300;
   lat_min1_back=-80.;
   lat_max1_back=-20;
   lat_min2_back=+20;
   lat_max2_back=+80;

  l_off=                long_min1_back; if(l_off>360.)l_off-=360.; if(l_off<  0.)l_off+=360.;
  il_min1=l_off/d_long;
  l_off=                long_max1_back; if(l_off>360.)l_off-=360.; if(l_off<  0.)l_off+=360.;
  il_max1=l_off/d_long;
  cout<<"il_min1 ilmax1:"<<il_min1<<" "<<il_max1<<endl;

  l_off=                long_min2_back; if(l_off>360.)l_off-=360.; if(l_off<  0.)l_off+=360.;
  il_min2=l_off/d_long;
  l_off=                long_max2_back; if(l_off>360.)l_off-=360.; if(l_off<  0.)l_off+=360.;
  il_max2=l_off/d_long;
  cout<<"background:il_min2 ilmax2:"<<il_min2<<" "<<il_max2<<endl;




  ib_min1=(           lat_min1_back-lat_min+.0001)/d_lat;
  ib_max1=(           lat_max1_back-lat_min+.0001)/d_lat;
  cout<<"background:ib_min1 ibmax1:"<<ib_min1<<" "<<ib_max1<<endl;

  ib_min2=(           lat_min2_back-lat_min+.0001)/d_lat;
  ib_max2=(           lat_max2_back-lat_min+.0001)/d_lat;
  cout<<"background:ib_min2 ibmax2:"<<ib_min2<<" "<<ib_max2<<endl;

  
         sa_norm_11=0.;sa_norm_12=0.;sa_norm_21=0.;sa_norm_22=0.;


          for (i_long=il_min1;  i_long<=il_max1           ; i_long++   )//AWS20030910
	  for (i_lat =ib_min1;  i_lat <=ib_max1           ; i_lat++    )//AWS20030910
              sa_norm_11+=sabin(lat_min,d_long,d_lat,i_lat); // multiply by solid angle of bin


          for (i_long=il_min1;  i_long<=il_max1           ; i_long++   )//AWS20030910
          for (i_lat =ib_min2;  i_lat <=ib_max2           ; i_lat++    )//AWS20030910
              sa_norm_12+=sabin(lat_min,d_long,d_lat,i_lat); 

          for (i_long=il_min2;  i_long<=il_max2           ; i_long++   )//AWS20030910
	  for (i_lat =ib_min1;  i_lat <=ib_max1           ; i_lat++    )//AWS20030910
              sa_norm_21+=sabin(lat_min,d_long,d_lat,i_lat); // multiply by solid angle of bin


          for (i_long=il_min2;  i_long<=il_max2           ; i_long++   )//AWS20030910
          for (i_lat =ib_min2;  i_lat <=ib_max2           ; i_lat++    )//AWS20030910
              sa_norm_22+=sabin(lat_min,d_long,d_lat,i_lat); 


  profile11=comptel_map->ProjectionZ("galplot_COMPTEL11b" ,il_min1,il_max1, ib_min1,ib_max1);
  profile12=comptel_map->ProjectionZ("galplot_COMPTEL12b" ,il_min1,il_max1, ib_min2,ib_max2);
  profile21=comptel_map->ProjectionZ("galplot_COMPTEL21b" ,il_min2,il_max2, ib_min1,ib_max1);
  profile22=comptel_map->ProjectionZ("galplot_COMPTEL22b" ,il_min2,il_max2, ib_min2,ib_max2);
  profile11->Scale(-1./sa_norm_11);
  profile12->Scale(-1./sa_norm_12);
  profile21->Scale(-1./sa_norm_21);
  profile22->Scale(-1./sa_norm_22);
  profile->Add(profile11,0.25);
  profile->Add(profile12,0.25);
  profile->Add(profile21,0.25);
  profile->Add(profile22,0.25);


 // profile ->Draw();


  intensity_    =new double[data.n_E_COMPTEL];
  intensity_esq1=new double[data.n_E_COMPTEL];
  intensity_esq2=new double[data.n_E_COMPTEL];

         for(i_E_COMPTEL=0; i_E_COMPTEL<data.n_E_COMPTEL                    ; i_E_COMPTEL++)
        {
	  intensity_[i_E_COMPTEL]=profile ->GetBinContent(i_E_COMPTEL);

          
          differential_spectrum(data.E_COMPTEL[i_E_COMPTEL],data.E_COMPTEL[i_E_COMPTEL+1] ,
                                intensity_[i_E_COMPTEL], g[i_E_COMPTEL],
                                intensity_esq1[i_E_COMPTEL],intensity_esq2[i_E_COMPTEL]);

	 
	  cout<<"e1,e2="<<data.E_COMPTEL[i_E_COMPTEL]<<" "<<data.E_COMPTEL[i_E_COMPTEL+1]
              <<" intensity="<<intensity_[i_E_COMPTEL]<<"intensity_esq1, 2 ="<<intensity_esq1[i_E_COMPTEL]<<" "<<intensity_esq2[i_E_COMPTEL]<<endl;


	}//i_E_COMPTEL

         Ebar=new double[data.n_E_COMPTEL];
         Ibar=new double[data.n_E_COMPTEL];

	 // automatic determination of spectral index
	 differential_spectrum(data.E_COMPTEL, data.n_E_COMPTEL, intensity_, intensity_esq1, intensity_esq2, Ebar,Ibar);


// tex output  AWS20041211
// txtFILE in global and assigned in plot_spectrum

fprintf(txtFILE,"======================================================\n");
fprintf(txtFILE,"COMPTEL spectrum                                        \n");
fprintf(txtFILE,"     E(MeV)      E^2 * intensity (MeV cm^-2 sr^-1 s^-1)     \n");
for(i_E_COMPTEL=0; i_E_COMPTEL<data.n_E_COMPTEL ; i_E_COMPTEL++)
  {
  fprintf(txtFILE,"%6.0f-%6.0f & %9.5f \\\\ \n",data.E_COMPTEL[i_E_COMPTEL],data.E_COMPTEL[i_E_COMPTEL+1],Ibar[i_E_COMPTEL] );
                                   // "\\\\" yields "\\"
  }
fprintf(txtFILE,"======================================================\n");




         error_bar_COMPTEL= 0.30;//AWS20040115


for(i_E_COMPTEL=0; i_E_COMPTEL<data.n_E_COMPTEL                    ; i_E_COMPTEL++)
{

 if(galplotdef.spectrum_style_EGRET==1 ||  galplotdef.spectrum_style_EGRET==3)
 {
  for (errorbar=1;errorbar<=2; errorbar++)
    {

          if(errorbar==1)errorbarfactor=    1.0+error_bar_COMPTEL; //AWS20040115

          if(errorbar==2)errorbarfactor=1./(1.0+error_bar_COMPTEL);//AWS20040115


	  segment[0]=intensity_esq1[i_E_COMPTEL]*errorbarfactor;
 	  segment[1]=intensity_esq2[i_E_COMPTEL]*errorbarfactor;

          

          spectrum=new TGraph(2,&data.E_COMPTEL[i_E_COMPTEL],segment);

	  spectrum->SetMarkerColor(kGreen  );
	  spectrum->SetMarkerStyle(21); 
	  spectrum->SetMarkerStyle(22); // triangles
	  spectrum->SetMarkerSize (0.5);// 
	  spectrum->SetLineColor(kGreen );
	  spectrum->SetLineWidth(4     );
	  spectrum->SetLineStyle(1      );
	  spectrum->SetMarkerStyle(22); // triangles
	  spectrum->SetMarkerSize (0.5);// 
	  spectrum->Draw();       // don't use "same" for TGraph
	  spectrum->Draw(" P ");  // points as markers  

          spectrum->Draw();
     }//errorbar
    }//if


 if(galplotdef.spectrum_style_EGRET==2 ||  galplotdef.spectrum_style_EGRET==3)
 {
         segment[0]=Ibar[i_E_COMPTEL]*(1.0+error_bar_COMPTEL);//AWS20040115

         segment[1]=Ibar[i_E_COMPTEL]/(1.0+error_bar_COMPTEL);//AWS20040115

    Ebar_segment[0]=Ebar[i_E_COMPTEL];
    Ebar_segment[1]=Ebar[i_E_COMPTEL];
    spectrum=new TGraph(2,Ebar_segment,segment);

	  spectrum->SetMarkerColor(kGreen );
	  spectrum->SetMarkerStyle(21); 
	  spectrum->SetMarkerStyle(22); // triangles
	  spectrum->SetMarkerSize (0.5);// 
	  spectrum->SetLineColor(kGreen);
	  spectrum->SetLineWidth(4     );
	  spectrum->SetLineStyle(1      );
	  spectrum->SetMarkerStyle(22); // triangles
	  spectrum->SetMarkerSize (0.5);// 
	  spectrum->Draw();       // don't use "same" for TGraph
	  spectrum->Draw(" P ");  // points as markers  

          spectrum->Draw();

  }//if


 if(mode>=2)// AWS20041019
   {
     if(mode==2)
     {
      // conventional horizontal bar
	  segment[0]=(intensity_esq1[i_E_COMPTEL]+intensity_esq2[i_E_COMPTEL])/2. ;
 	  segment[1]=segment[0];
     }

     if(mode==3)
     {
      // correct line segment
	  segment[0]=intensity_esq1[i_E_COMPTEL];                                ;
 	  segment[1]=intensity_esq2[i_E_COMPTEL];
     }

          spectrum=new TGraph(2,&data.E_COMPTEL[i_E_COMPTEL],segment);

	  spectrum->SetMarkerColor(kGreen  );
	  spectrum->SetMarkerStyle(21); 
	  spectrum->SetMarkerStyle(22); // triangles
	  spectrum->SetMarkerSize (0.5);// 
	  spectrum->SetLineColor(kGreen );
	  spectrum->SetLineWidth(4     );
	  spectrum->SetLineStyle(1      );
	  spectrum->SetMarkerStyle(22); // triangles
	  spectrum->SetMarkerSize (0.5);// 
	  spectrum->Draw();       // don't use "same" for TGraph
	  spectrum->Draw(" P ");  // points as markers  

          spectrum->Draw();

  }//mode==2

 }//iE_COMPTEL





   cout<<" <<<< plot_COMPTEL_spectrum    "<<endl;
   return status;
}

