
using namespace std; //AWS20050919
#include<iostream>   //AWS20050919
#include<string>     //AWS20050919
#include<cstring>    //AWS20130903 for sles11 gcc 4.3.4
#include "fitsio.h"

#include "FITS.h"

FITS::FITS(){ //required in any case to compile
// Also ensure pointer to NULL in case of declaration
// without initialization, e.g. as used by FITS::read
// to delete previously allocated memory 
// e.g. FITS x;  x.read(); x.read();

  array=NULL; 
} 
///////////////////////////////////////////// 
FITS::FITS(int NAXIS1) //AWS20061121
{
  // constructor cannot be applied to array of FITS pointers
  // so generally use init instead
  
 NAXIS=1;
  cout<<"FITS constructor for NAXIS="<<NAXIS<<endl;
 NAXES=new long[NAXIS];
 NAXES[0]=NAXIS1; /* note switch from FITS convention to C convention */
 
 nelements=NAXES[0]                  ;
 array=new float[ nelements ];
 CRVAL=new double[NAXIS];
 CDELT=new double[NAXIS];
 CRPIX=new double[NAXIS]; //AWS20061121
}
///////////////////////////////////////////// 
FITS::FITS(int NAXIS1,int NAXIS2)
{
  // constructor cannot be applied to array of FITS pointers
  // so generally use init instead
  
 NAXIS=2;
  cout<<"FITS constructor for NAXIS="<<NAXIS<<endl;
 NAXES=new long[NAXIS];
 NAXES[0]=NAXIS1; /* note switch from FITS convention to C convention */
 NAXES[1]=NAXIS2;
 nelements=NAXES[0]*NAXES[1]         ;
 array=new float[ nelements ];
 CRVAL=new double[NAXIS];
 CDELT=new double[NAXIS];
 CRPIX=new double[NAXIS]; //AWS20061121
}
///////////////////////////////////////////// 
FITS::FITS(int NAXIS1,int NAXIS2,int NAXIS3)
{
  // constructor cannot be applied to array of FITS pointers
  // so generally use init instead
  
 NAXIS=3;
  cout<<"FITS constructor for NAXIS="<<NAXIS<<endl;
 NAXES=new long[NAXIS];
 NAXES[0]=NAXIS1; /* note switch from FITS convention to C convention */
 NAXES[1]=NAXIS2;
 NAXES[2]=NAXIS3;
 nelements=NAXES[0]*NAXES[1]*NAXES[2];
 array=new float[ nelements ];
 CRVAL=new double[NAXIS];
 CDELT=new double[NAXIS];
 CRPIX=new double[NAXIS]; //AWS20061121
}
/////////////////////////////////////////////////////////////
FITS::FITS(int NAXIS1,int NAXIS2,int NAXIS3,int NAXIS4)
{
  // constructor cannot be applied to array of FITS pointers
  // so generally use init instead
  
 NAXIS=4;
  cout<<"FITS constructor for NAXIS="<<NAXIS<<endl;
 NAXES=new long[NAXIS];
 NAXES[0]=NAXIS1; /* note switch from FITS convention to C convention */
 NAXES[1]=NAXIS2;
 NAXES[2]=NAXIS3;
 NAXES[3]=NAXIS4;
 nelements=NAXES[0]*NAXES[1]*NAXES[2]*NAXES[3];
 array=new float[ nelements ];
 CRVAL=new double[NAXIS];
 CDELT=new double[NAXIS];
 CRPIX=new double[NAXIS]; //AWS20061121
}
/////////////////////////////////////////////
int FITS::init(int NAXIS1           ) //AWS20061121
{
 
 NAXIS=1;
  cout<<"FITS init for NAXIS="<<NAXIS<<endl;
 NAXES=new long[NAXIS];
 NAXES[0]=NAXIS1; /* note switch from FITS convention to C convention */
 
 
 nelements=NAXES[0];
 array=new float[ nelements ];
 CRVAL=new double[NAXIS];
 CDELT=new double[NAXIS];
 CRPIX=new double[NAXIS]; //AWS20061121
 return 0;
}

/////////////////////////////////////////////
int FITS::init(int NAXIS1,int NAXIS2)
{
 
 NAXIS=2;
  cout<<"FITS init for NAXIS="<<NAXIS<<endl;
 NAXES=new long[NAXIS];
 NAXES[0]=NAXIS1; /* note switch from FITS convention to C convention */
 NAXES[1]=NAXIS2;
 
 nelements=NAXES[0]*NAXES[1];
 array=new float[ nelements ];
 CRVAL=new double[NAXIS];
 CDELT=new double[NAXIS];
 CRPIX=new double[NAXIS]; //AWS20061121
 return 0;
}

/////////////////////////////////////////////
int FITS::init(int NAXIS1,int NAXIS2,int NAXIS3)
{
 
 NAXIS=3;
  cout<<"FITS init for NAXIS="<<NAXIS<<endl;
 NAXES=new long[NAXIS];
 NAXES[0]=NAXIS1; /* note switch from FITS convention to C convention */
 NAXES[1]=NAXIS2;
 NAXES[2]=NAXIS3;
 nelements=NAXES[0]*NAXES[1]*NAXES[2];
 array=new float[ nelements ];
 CRVAL=new double[NAXIS];
 CDELT=new double[NAXIS];
 CRPIX=new double[NAXIS]; //AWS20061121
 return 0;
}
/////////////////////////////////////////////
int FITS::init(int NAXIS1,int NAXIS2,int NAXIS3,int NAXIS4)
{
 
 NAXIS=4;
  cout<<"FITS init for NAXIS="<<NAXIS<<endl;
 NAXES=new long[NAXIS];
 NAXES[0]=NAXIS1; /* note switch from FITS convention to C convention */
 NAXES[1]=NAXIS2;
 NAXES[2]=NAXIS3;
 NAXES[3]=NAXIS4;
 nelements=NAXES[0]*NAXES[1]*NAXES[2]*NAXES[3];
 array=new float[ nelements ];
 CRVAL=new double[NAXIS];
 CDELT=new double[NAXIS];
 CRPIX=new double[NAXIS]; //AWS20061121
 return 0;
}
////////////////////////////////////////////
void FITS::setvalues()
{
for (int i=0;i<nelements;i++)array[i]=i;
}

///////////////////////////////////////////
float& FITS::operator()(int i)                                       //AWS20061121
{
 if(i<NAXES[0])                                                      //AWS20061121
  return array[                                                                    i];
 else
  {cout<<"warning from FITS: bounds violation"<<endl;  return junk;} //AWS20061121
}
///////////////////////////////////////////
float& FITS::operator()(int i,int j)
{
 if(i<NAXES[0]&&j<NAXES[1])                                          //AWS20061121
  return array[                                                      NAXES[0] *j + i];
 else
  {cout<<"warning from FITS: bounds violation"<<endl;  return junk;} //AWS20061121
}
///////////////////////////////////////////
float& FITS::operator()(int i,int j,int k)
{
 if(i<NAXES[0]&&j<NAXES[1]&&k<NAXES[2])                              //AWS20061121
  return array[                               NAXES[0]*NAXES[1] *k + NAXES[0] *j + i];
 else
  {cout<<"warning from FITS: bounds violation"<<endl;  return junk;} //AWS20061121
}
///////////////////////////////////////////
float& FITS::operator()(int i,int j,int k,int l)
{
 if(i<NAXES[0]&&j<NAXES[1]&&k<NAXES[2]&&l<NAXES[3] )                 //AWS20061121
  return array[NAXES[0]*NAXES[1]*NAXES[2] *l +NAXES[0]*NAXES[1] *k + NAXES[0] *j + i];
 else
  {cout<<"warning from FITS: bounds violation"<<endl;  return junk;} //AWS20061121
}
///////////////////////////////////////////
float& FITS::operator()(int i,int j,int k,int l,int m)               //AWS20130903
{
 if(i<NAXES[0]&&j<NAXES[1]&&k<NAXES[2]&&l<NAXES[3]&&m<NAXES[4] )                 
  return array[  NAXES[0]*NAXES[1]*NAXES[2]*NAXES[3] *m+  NAXES[0]*NAXES[1]*NAXES[2] *l +NAXES[0]*NAXES[1] *k + NAXES[0] *j + i];
 else
  {cout<<"warning from FITS: bounds violation"<<endl;  return junk;} 
}
/////////////////////////////////////////////
FITS FITS::operator=(double a) 
{
  int i;
  for (i=0;i<nelements;i++)array[i]=a;
  return *this;
}
//////////////////////////////////////////////
FITS FITS::operator+=(double a) 
{
  int i;
  for (i=0;i<nelements;i++)array[i]+=a;
  return *this;
}
//////////////////////////////////////////////
FITS FITS::operator*=(double a) 
{
  int i;
  for (i=0;i<nelements;i++)array[i]*=a;
  return *this;
}
//////////////////////////////////////////////
FITS FITS::operator/=(double a) //AWS20101223
{
  int i;
  for (i=0;i<nelements;i++)array[i]/=a;
  return *this;
}
//////////////////////////////////////////////


//////////////////////////////////////////////
FITS FITS::operator=(FITS a) //AWS20101223
{
  NAXIS=a.NAXIS;
  NAXES=new long[NAXIS];

             NAXES[0]=a.NAXES[0];
  if(NAXIS>1)NAXES[1]=a.NAXES[1];
  if(NAXIS>2)NAXES[2]=a.NAXES[2];
  if(NAXIS>3)NAXES[3]=a.NAXES[3];

  CRVAL=new double[NAXIS];
  CDELT=new double[NAXIS];
  CRPIX=new double[NAXIS];

             CRVAL[0]=a.CRVAL[0];
  if(NAXIS>1)CRVAL[1]=a.CRVAL[1];
  if(NAXIS>2)CRVAL[2]=a.CRVAL[2];
  if(NAXIS>3)CRVAL[3]=a.CRVAL[3];

             CDELT[0]=a.CDELT[0];
  if(NAXIS>1)CDELT[1]=a.CDELT[1];
  if(NAXIS>2)CDELT[2]=a.CDELT[2];
  if(NAXIS>3)CDELT[3]=a.CDELT[3];

             CRPIX[0]=a.CRPIX[0];
  if(NAXIS>1)CRPIX[1]=a.CRPIX[1];
  if(NAXIS>2)CRPIX[2]=a.CRPIX[2];
  if(NAXIS>3)CRPIX[3]=a.CRPIX[3];

  nelements=a.nelements;
  array=new float[nelements];
  int i;
  for (i=0;i<nelements;i++)array[i]=a.array[i];
  strcpy(filename,a.filename);
  return *this;
}
//////////////////////////////////////////////
FITS FITS::operator+=(FITS a) //AWS20101223
{
  int i;
  for (i=0;i<nelements;i++)array[i]+=a.array[i];
  return *this;
}
//////////////////////////////////////////////
FITS FITS::operator-=(FITS a) //AWS20101223
{
  int i;
  for (i=0;i<nelements;i++)array[i]-=a.array[i];
  return *this;
}
//////////////////////////////////////////////
FITS FITS::operator*=(FITS a) //AWS20101223
{
  int i;
  for (i=0;i<nelements;i++)array[i]*=a.array[i];
  return *this;
}
//////////////////////////////////////////////
FITS FITS::operator/=(FITS a) //AWS20101223
{
  int i;
  for (i=0;i<nelements;i++)array[i]/=a.array[i];
  return *this;
}
/////////////////////////////////////////////
double FITS::sum()
{
int i;
double ssum=0.0;
for (i=0;i<nelements;i++)ssum+=array[i];
return ssum;
}
double FITS::max()
{
int i;
double max_=-1.e50;
for (i=0;i<nelements;i++)if (array[i]>max_)max_=array[i];
return max_;
}


//////////////////////////////////////////////
int FITS::smooth(int n_smooth)
{
  // n_smooth must be odd
  // smooth over first 2 dimensions only
  // only   for 2D or 3D arrays      AWS20070223

  cout<<  "FITS::smooth n_smooth="<<n_smooth<<endl;
  if (NAXIS!=2 && NAXIS!=3){cout<<"FITS::smooth: smoothing not implemented for NAXIS="<<NAXIS<<endl;return -1;} //AWS20070223
  if (n_smooth==1) return 0;
  if (n_smooth%2==0){cout<<"FITS::smooth: smoothing defined only for odd n_smooth"    <<endl;return -1;}

int m;
int i,j,k;      
int ii,jj,kk;
int iii,jjj;
int index,indexx;
float *w;


m=(n_smooth-1)/2 ;

w=new float[nelements];

for(index=0;index<nelements;index++)w[index]=0.;

if(NAXIS==2) //AWS20070223
{
for(i =0;i <NAXES[0];i++ )
  {
for(j =0;j <NAXES[1];j++ )
  {
    //   cout<<i<<" "<<j<<endl;
   index =                         NAXES[0] *  j +   i ;

   for(ii =i -m;ii <=i +m;ii ++)
   {
                     iii=ii;
   if(ii <0)         iii=0;
   if(ii >NAXES[0]-1)iii=NAXES[0]-1;

   for(jj =j -m;jj <=j +m;jj ++)
   {
                      jjj=jj;
    if(jj <0)         jjj=0;
    if(jj >NAXES[1]-1)jjj=NAXES[1]-1;

    //      cout<<ii<<" "<<jj<<endl;



       indexx=                         NAXES[0] *jjj + iii;

       w[index]+=array[indexx];           
      
  }//jj 
  }//ii 

   
  }//j
 }//i 


}// NAXIS==2

//------------------------------------------------------------

if(NAXIS==3) //AWS20070223
{

for(k =0;k <NAXES[2];k++ )
  {
for(i =0;i <NAXES[0];i++ )
  {
for(j =0;j <NAXES[1];j++ )
  {
    //   cout<<i<<" "<<j<<endl;
   index =  NAXES[0]*NAXES[1] *k + NAXES[0] *  j +   i ;

   for(ii =i -m;ii <=i +m;ii ++)
   {
                     iii=ii;
   if(ii <0)         iii=0;
   if(ii >NAXES[0]-1)iii=NAXES[0]-1;

   for(jj =j -m;jj <=j +m;jj ++)
   {
                      jjj=jj;
    if(jj <0)         jjj=0;
    if(jj >NAXES[1]-1)jjj=NAXES[1]-1;

    //      cout<<ii<<" "<<jj<<endl;



       indexx=  NAXES[0]*NAXES[1] *k + NAXES[0] *jjj + iii;

       w[index]+=array[indexx];           
      
  }//jj 
  }//ii 

   
  }//j
 }//i 
}//k

}// NAXIS==3

for(index=0;index<nelements;index++)array[index]=w[index]/(n_smooth*n_smooth);
delete[]w;


//print();

return 0;
}

/////////////////////////////////////////////
void FITS::print()
{
  cout<<"==== FITS.print"<<endl;
  cout<<"filename="<<filename<<endl;
  cout<<"NAXIS="<<NAXIS<<endl;
  
  int i;
  cout<<"NAXES=" ;
  for (i=0;i<NAXIS;    i++){cout<<NAXES[i]<<" ";} 
  cout<<endl ;

  cout<<"CRVAL=" ;
  for (i=0;i<NAXIS;    i++){cout<<CRVAL[i]<<" ";} 
  cout<<endl ;

  cout<<"CDELT=" ;
  for (i=0;i<NAXIS;    i++){cout<<CDELT[i]<<" ";} 
  cout<<endl ;

  cout<<"CRPIX=" ;                                 //AWS20050308
  for (i=0;i<NAXIS;    i++){cout<<CRPIX[i]<<" ";}  //AWS20050308
  cout<<endl ;                                     //AWS20050308


  cout<<"array="<<endl ;
  for (i=0;i<nelements;i++){cout<<array[i]<<" ";if((i+1)%NAXES[0]==0)cout<<endl<<endl;} 
  cout<<endl ;
  cout<<"sum="<<sum()<<endl;

  cout<<endl;

}
  /////////////////////////////////////////
  int FITS::read(char *filename_)
{
   int status;
   fitsfile *fptr;

   char comment[400];

  
   strcpy(filename,filename_);  // copy into FITS structure

   status=0; // need this on input
   if( fits_open_file(&fptr,filename_,READONLY,&status) ) cout<<"FITS read open status= "<<status<<" "<<filename<<endl;

   if(status!=0){NAXIS=0; return status;} //NAXIS=0 signals absent data  AWS20071221

   if( fits_read_key(fptr,TLONG,"NAXIS" ,&NAXIS ,  comment,&status) ) cout<<"FITS read NAXIS status= "<<status<<endl;

   // at least NAXIS keyword should be present
   if(status!=0){NAXIS=0; return status;} //NAXIS=0 signals absent data  AWS20071221

   NAXES=new long[NAXIS];

   if( fits_read_key(fptr,TLONG,"NAXIS1",&NAXES[0],comment,&status) ) cout<<"FITS read NAXIS1 status= "<<status<<endl;
   if(NAXIS>1)
   if( fits_read_key(fptr,TLONG,"NAXIS2",&NAXES[1],comment,&status) ) cout<<"FITS read NAXIS2 status= "<<status<<endl;
   if(NAXIS>2)
   if( fits_read_key(fptr,TLONG,"NAXIS3",&NAXES[2],comment,&status) ) cout<<"FITS read NAXIS3 status= "<<status<<endl;
   if(NAXIS>3)
   if( fits_read_key(fptr,TLONG,"NAXIS4",&NAXES[3],comment,&status) ) cout<<"FITS read NAXIS4 status= "<<status<<endl;
   if(NAXIS>4)
   if( fits_read_key(fptr,TLONG,"NAXIS5",&NAXES[4],comment,&status) ) cout<<"FITS read NAXIS5 status= "<<status<<endl;//AWS20130903


   CRVAL=new double[NAXIS];
   if( fits_read_key(fptr,TDOUBLE,"CRVAL1",&CRVAL[0],comment,&status) ) cout<<"FITS read CRVAL1 status= "<<status<<endl;
   if(NAXIS>1)
   if( fits_read_key(fptr,TDOUBLE,"CRVAL2",&CRVAL[1],comment,&status) ) cout<<"FITS read CRVAL2 status= "<<status<<endl;
   if(NAXIS>2)
   if( fits_read_key(fptr,TDOUBLE,"CRVAL3",&CRVAL[2],comment,&status) ) cout<<"FITS read CRVAL3 status= "<<status<<endl;
   if(NAXIS>3)
   if( fits_read_key(fptr,TDOUBLE,"CRVAL4",&CRVAL[3],comment,&status) ) cout<<"FITS read CRVAL4 status= "<<status<<endl;
   if(NAXIS>4)
   if( fits_read_key(fptr,TDOUBLE,"CRVAL5",&CRVAL[4],comment,&status) ) cout<<"FITS read CRVAL5 status= "<<status<<endl;//AWS20130903

   CDELT=new double[NAXIS];
   if( fits_read_key(fptr,TDOUBLE,"CDELT1",&CDELT[0],comment,&status) ) cout<<"FITS read CDELT1 status= "<<status<<endl;
   if(NAXIS>1)
   if( fits_read_key(fptr,TDOUBLE,"CDELT2",&CDELT[1],comment,&status) ) cout<<"FITS read CDELT2 status= "<<status<<endl;
   if(NAXIS>2)
   if( fits_read_key(fptr,TDOUBLE,"CDELT3",&CDELT[2],comment,&status) ) cout<<"FITS read CDELT3 status= "<<status<<endl;
   if(NAXIS>3)
   if( fits_read_key(fptr,TDOUBLE,"CDELT4",&CDELT[3],comment,&status) ) cout<<"FITS read CDELT4 status= "<<status<<endl;
   if(NAXIS>4)
   if( fits_read_key(fptr,TDOUBLE,"CDELT5",&CDELT[4],comment,&status) ) cout<<"FITS read CDELT5 status= "<<status<<endl;//AWS20130903

   CRPIX=new double[NAXIS];
   if( fits_read_key(fptr,TDOUBLE,"CRPIX1",&CRPIX[0],comment,&status) ) cout<<"FITS read CRPIX1 status= "<<status<<endl;// AWS20050308
   if(NAXIS>1)
   if( fits_read_key(fptr,TDOUBLE,"CRPIX2",&CRPIX[1],comment,&status) ) cout<<"FITS read CRPIX2 status= "<<status<<endl;// AWS20050308
   if(NAXIS>2)
   if( fits_read_key(fptr,TDOUBLE,"CRPIX3",&CRPIX[2],comment,&status) ) cout<<"FITS read CRPIX3 status= "<<status<<endl;// AWS20050308
   if(NAXIS>3)
   if( fits_read_key(fptr,TDOUBLE,"CRPIX4",&CRPIX[3],comment,&status) ) cout<<"FITS read CRPIX4 status= "<<status<<endl;// AWS20050308
   if(NAXIS>4)
   if( fits_read_key(fptr,TDOUBLE,"CRPIX5",&CRPIX[4],comment,&status) ) cout<<"FITS read CRPIX5 status= "<<status<<endl;//AWS20130903

   if( fits_read_key(fptr,TDOUBLE,"CHIPOL",&CHIPOL,  comment,&status) ) cout<<"FITS read CHIPOL status= "<<status<<endl;
   if( fits_read_key(fptr,TDOUBLE,"PSIPOL",&PSIPOL,  comment,&status) ) cout<<"FITS read PSIPOL status= "<<status<<endl;
   if( fits_read_key(fptr,TDOUBLE,"CHIORI",&CHIORI,  comment,&status) ) cout<<"FITS read CHIORI status= "<<status<<endl;
   if( fits_read_key(fptr,TDOUBLE,"PSIORI",&PSIORI,  comment,&status) ) cout<<"FITS read PSIORI status= "<<status<<endl;


   if(NAXIS==1)    nelements=NAXES[0];                            
   if(NAXIS==2)    nelements=NAXES[0]*NAXES[1];                       
   if(NAXIS==3)    nelements=NAXES[0]*NAXES[1]*NAXES[2];          
   if(NAXIS==4)    nelements=NAXES[0]*NAXES[1]*NAXES[2]*NAXES[3];  
   if(NAXIS==5)    nelements=NAXES[0]*NAXES[1]*NAXES[2]*NAXES[3]*NAXES[4];                                               //AWS20130903

   // free memory but only if already allocated
   // NULL is ensured by FITS::FITS() in case of declaration
   if (array!=NULL) delete[]array; 
   array=new float[ nelements ];

  long felement=1;
//float nulval=0;     // this suppresses check, result for NULL entry is undefined and wrong, so replace AWS20070222
  float nulval=-1.e10;//                                                                                 AWS20070222 
                      // to be done also elsewhere in this class (not yet)                               AWS20070222
  int anynul;

  status=0; // in case CHIPOL etc. do not exist

   if( fits_read_img(fptr,TFLOAT,felement,nelements,&nulval,array,&anynul,&status) )
   cout<<"FITS read  status= "<<status<<endl;



   fits_close_file(fptr, &status);            /* close the file */

   fits_report_error(stderr, status);  /* print out any error messages */

   /*
  cout<<"==== FITS.read "<<filename_<<endl;
  cout<<"NAXIS="<<NAXIS<<endl;
  
  int i;
  cout<<"NAXES=" ;
  for (i=0;i<NAXIS;    i++){cout<<NAXES[i]<<" ";} 
  cout<<endl ;

  cout<<"CRVAL=" ;
  for (i=0;i<NAXIS;    i++){cout<<CRVAL[i]<<" ";} 
  cout<<endl ;

  cout<<"CDELT=" ;
  for (i=0;i<NAXIS;    i++){cout<<CDELT[i]<<" ";} 
  cout<<endl ;

  cout<<"CHIPOL PSIPOL CHIORI PSIORI:"<<CHIPOL<<" "<<PSIPOL<<" "<< CHIORI<<" "<< PSIORI<<endl;

   */
 
   status=0;
   return status; // if it gets to here return status=0 (even if e.g. keywords missing)
}
  /////////////////////////////////////////
  int FITS::read(char *filename_, int hdunum)    // AWS20040406
{
  // for arrays in FITS extensions
  // and with CD1_1,CD2_2 instead of CDELT1,CDELT2
  // as used in ISDC software

   int status;
   fitsfile *fptr;

   char comment[400];
   int hdutype;                                   // AWS20040406
  
   strcpy(filename,filename_);  // copy into FITS structure

   status=0; // need this on input
   if( fits_open_file(&fptr,filename_,READONLY,&status) ) cout<<"FITS read open status= "<<status<<" "<<filename<<endl;

   fits_movabs_hdu(fptr,hdunum,&hdutype,&status );// AWS20040406

   if( fits_read_key(fptr,TLONG,"NAXIS" ,&NAXIS ,  comment,&status) ) cout<<"FITS read NAXIS status= "<<status<<endl;

   NAXES=new long[NAXIS];

   if( fits_read_key(fptr,TLONG,"NAXIS1",&NAXES[0],comment,&status) ) cout<<"FITS read NAXIS1 status= "<<status<<endl;
   if(NAXIS>1)
   if( fits_read_key(fptr,TLONG,"NAXIS2",&NAXES[1],comment,&status) ) cout<<"FITS read NAXIS2 status= "<<status<<endl;
   if(NAXIS>2)
   if( fits_read_key(fptr,TLONG,"NAXIS3",&NAXES[2],comment,&status) ) cout<<"FITS read NAXIS3 status= "<<status<<endl;
   if(NAXIS>3)
   if( fits_read_key(fptr,TLONG,"NAXIS4",&NAXES[3],comment,&status) ) cout<<"FITS read NAXIS4 status= "<<status<<endl;

   CRVAL=new double[NAXIS];
   if( fits_read_key(fptr,TDOUBLE,"CRVAL1",&CRVAL[0],comment,&status) ) cout<<"FITS read CRVAL1 status= "<<status<<endl;
   if(NAXIS>1)
   if( fits_read_key(fptr,TDOUBLE,"CRVAL2",&CRVAL[1],comment,&status) ) cout<<"FITS read CRVAL2 status= "<<status<<endl;
   if(NAXIS>2)
   if( fits_read_key(fptr,TDOUBLE,"CRVAL3",&CRVAL[2],comment,&status) ) cout<<"FITS read CRVAL3 status= "<<status<<endl;
   if(NAXIS>3)
   if( fits_read_key(fptr,TDOUBLE,"CRVAL4",&CRVAL[3],comment,&status) ) cout<<"FITS read CRVAL4 status= "<<status<<endl;

   CDELT=new double[NAXIS];
   if( fits_read_key(fptr,TDOUBLE,"CD1_1" ,&CDELT[0],comment,&status) ) cout<<"FITS read CD1_1  status= "<<status<<endl;// AWS20071001
   if(NAXIS>1)
   if( fits_read_key(fptr,TDOUBLE,"CD2_2" ,&CDELT[1],comment,&status) ) cout<<"FITS read CD2_2  status= "<<status<<endl;// AWS20071001
   if(NAXIS>2)
   if( fits_read_key(fptr,TDOUBLE,"CDELT3",&CDELT[2],comment,&status) ) cout<<"FITS read CDELT3 status= "<<status<<endl;
   if(NAXIS>3)
   if( fits_read_key(fptr,TDOUBLE,"CDELT4",&CDELT[3],comment,&status) ) cout<<"FITS read CDELT4 status= "<<status<<endl;

   CRPIX=new double[NAXIS];
   if( fits_read_key(fptr,TDOUBLE,"CRPIX1",&CRPIX[0],comment,&status) ) cout<<"FITS read CRPIX1 status= "<<status<<endl;// AWS20050308
   if(NAXIS>1)
   if( fits_read_key(fptr,TDOUBLE,"CRPIX2",&CRPIX[1],comment,&status) ) cout<<"FITS read CRPIX2 status= "<<status<<endl;// AWS20050308
   if(NAXIS>2)
   if( fits_read_key(fptr,TDOUBLE,"CRPIX3",&CRPIX[2],comment,&status) ) cout<<"FITS read CRPIX3 status= "<<status<<endl;// AWS20050308
   if(NAXIS>3)
   if( fits_read_key(fptr,TDOUBLE,"CRPIX4",&CRPIX[3],comment,&status) ) cout<<"FITS read CRPIX4 status= "<<status<<endl;// AWS20050308



   if( fits_read_key(fptr,TDOUBLE,"CHIPOL",&CHIPOL,  comment,&status) ) cout<<"FITS read CHIPOL status= "<<status<<endl;
   if( fits_read_key(fptr,TDOUBLE,"PSIPOL",&PSIPOL,  comment,&status) ) cout<<"FITS read PSIPOL status= "<<status<<endl;
   if( fits_read_key(fptr,TDOUBLE,"CHIORI",&CHIORI,  comment,&status) ) cout<<"FITS read CHIORI status= "<<status<<endl;
   if( fits_read_key(fptr,TDOUBLE,"PSIORI",&PSIORI,  comment,&status) ) cout<<"FITS read PSIORI status= "<<status<<endl;


   if(NAXIS==1)    nelements=NAXES[0];                            
   if(NAXIS==2)    nelements=NAXES[0]*NAXES[1];                       
   if(NAXIS==3)    nelements=NAXES[0]*NAXES[1]*NAXES[2];          
   if(NAXIS==4)    nelements=NAXES[0]*NAXES[1]*NAXES[2]*NAXES[3];     

   // free memory but only if already allocated
   // NULL is ensured by FITS::FITS() in case of declaration
   if (array!=NULL) delete[]array; 
   array=new float[ nelements ];

  long felement=1;
  float nulval=0;
  int anynul;

  status=0; // in case CHIPOL etc. do not exist

   if( fits_read_img(fptr,TFLOAT,felement,nelements,&nulval,array,&anynul,&status) )
   cout<<"FITS read  status= "<<status<<endl;



   fits_close_file(fptr, &status);            /* close the file */

   fits_report_error(stderr, status);  /* print out any error messages */

   /*
  cout<<"==== FITS.read "<<filename_<<endl;
  cout<<"NAXIS="<<NAXIS<<endl;
  
  int i;
  cout<<"NAXES=" ;
  for (i=0;i<NAXIS;    i++){cout<<NAXES[i]<<" ";} 
  cout<<endl ;

  cout<<"CRVAL=" ;
  for (i=0;i<NAXIS;    i++){cout<<CRVAL[i]<<" ";} 
  cout<<endl ;

  cout<<"CDELT=" ;
  for (i=0;i<NAXIS;    i++){cout<<CDELT[i]<<" ";} 
  cout<<endl ;

  cout<<"CHIPOL PSIPOL CHIORI PSIORI:"<<CHIPOL<<" "<<PSIPOL<<" "<< CHIORI<<" "<< PSIORI<<endl;

   */
return 0;
}

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  int FITS::read(char *filename_, int hdunum1, int hdunum2, int options)    // AWS20070928
{
  // for arrays in FITS extensions, combining many into one array, with the range as input
  // and with CD1_1,CD2_2 instead of CDELT1,CDELT2
  // as used in ISDC software
  // but if not present read CDELT1, CDELT2 instead
  // int options was added to distinguish from the other case with same argument types

   int status;
   fitsfile *fptr;

   char comment[400];
   int hdutype;                                   // AWS20040406
   int hdunum;                                    // AWS20071001
   int i_start;                                   // AWS20070928
   int nelements_per_extension;                   // AWS20071001

   strcpy(filename,filename_);  // copy into FITS structure

   status=0; // need this on input
   if( fits_open_file(&fptr,filename_,READONLY,&status) ) cout<<"FITS read open status= "<<status<<" "<<filename<<endl;

   fits_movabs_hdu(fptr,hdunum1,&hdutype,&status );// AWS20040406

   if( fits_read_key(fptr,TLONG,"NAXIS" ,&NAXIS ,  comment,&status) ) cout<<"FITS read NAXIS status= "<<status<<endl;

   // increase dimensionality of array by one
   NAXIS++; // since the arrays are going to be combined into an array with one extra dimension
   NAXES=new long[NAXIS];
   NAXES[NAXIS-1] = hdunum2-hdunum1+1; // the last axis is over the extensions

   if( fits_read_key(fptr,TLONG,"NAXIS1",&NAXES[0],comment,&status) ) cout<<"FITS read NAXIS1 status= "<<status<<endl;
   if(NAXIS-1>1)//since extension has dimensionality one less
   if( fits_read_key(fptr,TLONG,"NAXIS2",&NAXES[1],comment,&status) ) cout<<"FITS read NAXIS2 status= "<<status<<endl;
   if(NAXIS-1>2)
   if( fits_read_key(fptr,TLONG,"NAXIS3",&NAXES[2],comment,&status) ) cout<<"FITS read NAXIS3 status= "<<status<<endl;
   if(NAXIS-1>3)
   if( fits_read_key(fptr,TLONG,"NAXIS4",&NAXES[3],comment,&status) ) cout<<"FITS read NAXIS4 status= "<<status<<endl;

   CRVAL=new double[NAXIS];
   if( fits_read_key(fptr,TDOUBLE,"CRVAL1",&CRVAL[0],comment,&status) ) cout<<"FITS read CRVAL1 status= "<<status<<endl;
   if(NAXIS-1>1)
   if( fits_read_key(fptr,TDOUBLE,"CRVAL2",&CRVAL[1],comment,&status) ) cout<<"FITS read CRVAL2 status= "<<status<<endl;
   if(NAXIS-1>2)
   if( fits_read_key(fptr,TDOUBLE,"CRVAL3",&CRVAL[2],comment,&status) ) cout<<"FITS read CRVAL3 status= "<<status<<endl;
   if(NAXIS-1>3)
   if( fits_read_key(fptr,TDOUBLE,"CRVAL4",&CRVAL[3],comment,&status) ) cout<<"FITS read CRVAL4 status= "<<status<<endl;

   CDELT=new double[NAXIS];
   if( fits_read_key(fptr,TDOUBLE,"CD1_1" ,&CDELT[0],comment,&status) ) cout<<"FITS read CD1_1  status= "<<status<<endl;// AWS20040406
   if(status!=0)// case where old keywords still used
   {status=0;
   if( fits_read_key(fptr,TDOUBLE,"CDELT1",&CDELT[0],comment,&status) ) cout<<"FITS read CDELT1 status= "<<status<<endl;//AWS20071001
   }
   if(NAXIS-1>1)
   {
   if( fits_read_key(fptr,TDOUBLE,"CD2_2" ,&CDELT[1],comment,&status) ) cout<<"FITS read CD2_2   status= "<<status<<endl;// AWS20040406
   if(status!=0)// case where old keywords still used
   {status=0;
   if( fits_read_key(fptr,TDOUBLE,"CDELT2",&CDELT[1],comment,&status) ) cout<<"FITS read CDELT2 status= "<<status<<endl;//AWS20071001
   }}

   if(NAXIS-1>2)
   if( fits_read_key(fptr,TDOUBLE,"CDELT3",&CDELT[2],comment,&status) ) cout<<"FITS read CDELT3 status= "<<status<<endl;
   if(NAXIS-1>3)
   if( fits_read_key(fptr,TDOUBLE,"CDELT4",&CDELT[3],comment,&status) ) cout<<"FITS read CDELT4 status= "<<status<<endl;

   CRPIX=new double[NAXIS];
   if( fits_read_key(fptr,TDOUBLE,"CRPIX1",&CRPIX[0],comment,&status) ) cout<<"FITS read CRPIX1 status= "<<status<<endl;// AWS20050308
   if(NAXIS-1>1)
   if( fits_read_key(fptr,TDOUBLE,"CRPIX2",&CRPIX[1],comment,&status) ) cout<<"FITS read CRPIX2 status= "<<status<<endl;// AWS20050308
   if(NAXIS-1>2)
   if( fits_read_key(fptr,TDOUBLE,"CRPIX3",&CRPIX[2],comment,&status) ) cout<<"FITS read CRPIX3 status= "<<status<<endl;// AWS20050308
   if(NAXIS-1>3)
   if( fits_read_key(fptr,TDOUBLE,"CRPIX4",&CRPIX[3],comment,&status) ) cout<<"FITS read CRPIX4 status= "<<status<<endl;// AWS20050308



   if(NAXIS==1)    nelements=NAXES[0];                            
   if(NAXIS==2)    nelements=NAXES[0]*NAXES[1];                       
   if(NAXIS==3)    nelements=NAXES[0]*NAXES[1]*NAXES[2];          
   if(NAXIS==4)    nelements=NAXES[0]*NAXES[1]*NAXES[2]*NAXES[3];     

   // free memory but only if already allocated
   // NULL is ensured by FITS::FITS() in case of declaration
   if (array!=NULL) delete[]array; 
   array=new float[ nelements ];

  long felement=1;
  float nulval=0;
  int anynul;

  status=0; // in case some keywords did not exist

  i_start=0;
  nelements_per_extension=nelements/(hdunum2-hdunum1+1);

  for (hdunum=hdunum1; hdunum<=hdunum2; hdunum++)
    {
      cout<<"reading array from extension header number "<<hdunum<<endl;

   if(fits_movabs_hdu(fptr,hdunum,&hdutype,&status )) cout<<"fits_movabs_hdu  status= "<<status<<endl;
      //                                                                   V----------- pointer to position in array
   if( fits_read_img(fptr,TFLOAT,felement,nelements_per_extension,&nulval, array+i_start, &anynul,&status) )   cout<<"FITS read  status= "<<status<<endl;
   i_start+= nelements_per_extension;
    }


   fits_close_file(fptr, &status);            /* close the file */

   fits_report_error(stderr, status);  /* print out any error messages */

   
  cout<<"====------- FITS.read "<<filename_<<endl;
  cout<<"NAXIS="<<NAXIS<<endl;
  
  int i;
  cout<<"NAXES=" ;
  for (i=0;i<NAXIS;    i++){cout<<NAXES[i]<<" ";} 
  cout<<endl ;

  cout<<"CRVAL=" ;
  for (i=0;i<NAXIS;    i++){cout<<CRVAL[i]<<" ";} 
  cout<<endl ;

  cout<<"CRPIX=" ;
  for (i=0;i<NAXIS;    i++){cout<<CRPIX[i]<<" ";} 
  cout<<endl ;


  cout<<"CDELT=" ;
  for (i=0;i<NAXIS;    i++){cout<<CDELT[i]<<" ";} 
  cout<<endl ;

  //  cout<<"CHIPOL PSIPOL CHIORI PSIORI:"<<CHIPOL<<" "<<PSIPOL<<" "<< CHIORI<<" "<< PSIORI<<endl;

   
return 0;
}

  /////////////////////////////////////////
  int FITS::read(char *filename_,int naxis_min,int naxis_max)
{
  // reads only part of array between given second (2D) or third (3D) dimension limits
   int status;
   fitsfile *fptr;

   char comment[400];

  
   strcpy(filename,filename_);  // copy into FITS structure

   status=0; // need this on input
   if( fits_open_file(&fptr,filename_,READONLY,&status) ) cout<<"FITS read open status= "<<status<<" "<<filename<<endl;
   if( fits_read_key(fptr,TLONG,"NAXIS" ,&NAXIS ,  comment,&status) ) cout<<"FITS read NAXIS status= "<<status<<endl;

   NAXES=new long[NAXIS];

   if( fits_read_key(fptr,TLONG,"NAXIS1",&NAXES[0],comment,&status) ) cout<<"FITS read NAXIS1 status= "<<status<<endl;
   if(NAXIS>1)
   if( fits_read_key(fptr,TLONG,"NAXIS2",&NAXES[1],comment,&status) ) cout<<"FITS read NAXIS2 status= "<<status<<endl;
   if(NAXIS>2)
   if( fits_read_key(fptr,TLONG,"NAXIS3",&NAXES[2],comment,&status) ) cout<<"FITS read NAXIS3 status= "<<status<<endl;
   if(NAXIS>3)
   if( fits_read_key(fptr,TLONG,"NAXIS4",&NAXES[3],comment,&status) ) cout<<"FITS read NAXIS4 status= "<<status<<endl;

   CRVAL=new double[NAXIS];
   if( fits_read_key(fptr,TDOUBLE,"CRVAL1",&CRVAL[0],comment,&status) ) cout<<"FITS read CRVAL1 status= "<<status<<endl;
   if(NAXIS>1)
   if( fits_read_key(fptr,TDOUBLE,"CRVAL2",&CRVAL[1],comment,&status) ) cout<<"FITS read CRVAL2 status= "<<status<<endl;
   if(NAXIS>2)
   if( fits_read_key(fptr,TDOUBLE,"CRVAL3",&CRVAL[2],comment,&status) ) cout<<"FITS read CRVAL3 status= "<<status<<endl;
   if(NAXIS>3)
   if( fits_read_key(fptr,TDOUBLE,"CRVAL4",&CRVAL[3],comment,&status) ) cout<<"FITS read CRVAL4 status= "<<status<<endl;

   CDELT=new double[NAXIS];
   if( fits_read_key(fptr,TDOUBLE,"CDELT1",&CDELT[0],comment,&status) ) cout<<"FITS read CDELT1 status= "<<status<<endl;
   if(NAXIS>1)
   if( fits_read_key(fptr,TDOUBLE,"CDELT2",&CDELT[1],comment,&status) ) cout<<"FITS read CDELT2 status= "<<status<<endl;
   if(NAXIS>2)
   if( fits_read_key(fptr,TDOUBLE,"CDELT3",&CDELT[2],comment,&status) ) cout<<"FITS read CDELT3 status= "<<status<<endl;
   if(NAXIS>3)
   if( fits_read_key(fptr,TDOUBLE,"CDELT4",&CDELT[3],comment,&status) ) cout<<"FITS read CDELT4 status= "<<status<<endl;

   if( fits_read_key(fptr,TDOUBLE,"CHIPOL",&CHIPOL,  comment,&status) ) cout<<"FITS read CHIPOL status= "<<status<<endl;
   if( fits_read_key(fptr,TDOUBLE,"PSIPOL",&PSIPOL,  comment,&status) ) cout<<"FITS read PSIPOL status= "<<status<<endl;
   if( fits_read_key(fptr,TDOUBLE,"CHIORI",&CHIORI,  comment,&status) ) cout<<"FITS read CHIORI status= "<<status<<endl;
   if( fits_read_key(fptr,TDOUBLE,"PSIORI",&PSIORI,  comment,&status) ) cout<<"FITS read PSIORI status= "<<status<<endl;

   if(NAXIS==2)NAXES[1]=naxis_max-naxis_min + 1;
   if(NAXIS==3)NAXES[2]=naxis_max-naxis_min + 1;
   if(NAXIS==4)NAXES[2]=naxis_max-naxis_min + 1; // treat as 3D since DRG,DRE have NAXIS=4 but only 3 used

   if(NAXIS==1)    nelements=NAXES[0];                            
   if(NAXIS==2)    nelements=NAXES[0]*NAXES[1];                       
   if(NAXIS==3)    nelements=NAXES[0]*NAXES[1]*NAXES[2];          
   if(NAXIS==4)    nelements=NAXES[0]*NAXES[1]*NAXES[2]*NAXES[3];     

   // free memory but only if already allocated
   // NULL is ensured by FITS::FITS() in case of declaration
   if (array!=NULL) delete[]array; 
   array=new float[ nelements ];

  long felement;
  if(NAXIS==1)felement=1;
  if(NAXIS==2)felement=1+(naxis_min-1)*NAXES[0]         ;
  if(NAXIS==3)felement=1+(naxis_min-1)*NAXES[0]*NAXES[1];
  if(NAXIS==4)felement=1+(naxis_min-1)*NAXES[0]*NAXES[1];
  float nulval=0;
  int anynul;

  status=0; // in case CHIPOL etc. do not exist

   if( fits_read_img(fptr,TFLOAT,felement,nelements,&nulval,array,&anynul,&status) )
   cout<<"FITS read  status= "<<status<<endl;



   fits_close_file(fptr, &status);            /* close the file */

   fits_report_error(stderr, status);  /* print out any error messages */

   /*
  cout<<"==== FITS.read "<<filename_<<endl;
  cout<<"NAXIS="<<NAXIS<<endl;
  
  int i;
  cout<<"NAXES=" ;
  for (i=0;i<NAXIS;    i++){cout<<NAXES[i]<<" ";} 
  cout<<endl ;

  cout<<"CRVAL=" ;
  for (i=0;i<NAXIS;    i++){cout<<CRVAL[i]<<" ";} 
  cout<<endl ;

  cout<<"CDELT=" ;
  for (i=0;i<NAXIS;    i++){cout<<CDELT[i]<<" ";} 
  cout<<endl ;

  cout<<"CHIPOL PSIPOL CHIORI PSIORI:"<<CHIPOL<<" "<<PSIPOL<<" "<< CHIORI<<" "<< PSIORI<<endl;

   */
return 0;
}
  /////////////////////////////////////////
  int FITS::write(char *filename_)
{
   int status;
   fitsfile *fptr;

   cout<<"writing FITS file to "<<filename_<<endl;

   char comment[400];
   char filename__[400];

   strcpy(filename__,"!"); // to overwrite if file exists
   strcat(filename__,filename_);

   // put   filename in object
   strcpy(filename,filename_); // AWS20061122

   status=0; // need this on input
   if( fits_create_file(&fptr,filename__,&status) ) 
   cout<<"FITS create status= "<<status<<endl;
   fits_report_error(stderr, status);  /* print out any error messages */

    /* Create the primary array image (32-bit float pixels */
   fits_create_img(fptr, FLOAT_IMG, NAXIS, NAXES, &status);
   
   fits_report_error(stderr, status);  /* print out any error messages */

    fits_update_key(fptr, TDOUBLE, "CRVAL1", &CRVAL[0],"Start of axis 1", &status);
    if(NAXIS>=2)
    fits_update_key(fptr, TDOUBLE, "CRVAL2", &CRVAL[1],"Start of axis 2", &status);
    if(NAXIS>=3)
    fits_update_key(fptr, TDOUBLE, "CRVAL3", &CRVAL[2],"Start of axis 3", &status);
    if(NAXIS>=4)
    fits_update_key(fptr, TDOUBLE, "CRVAL4", &CRVAL[3],"Start of axis 4", &status);

    fits_update_key(fptr, TDOUBLE, "CRPIX1", &CRPIX[0],"Reference pixel of axis 1", &status); //AWS20061121
    if(NAXIS>=2)
    fits_update_key(fptr, TDOUBLE, "CRPIX2", &CRPIX[1],"Reference pixel of axis 1", &status); //AWS20061121
    if(NAXIS>=3)
    fits_update_key(fptr, TDOUBLE, "CRPIX3", &CRPIX[2],"Reference pixel of axis 1", &status); //AWS20061121
    if(NAXIS>=4)
    fits_update_key(fptr, TDOUBLE, "CRPIX4", &CRPIX[3],"Reference pixel of axis 1", &status); //AWS20061121

    fits_update_key(fptr, TDOUBLE, "CDELT1", &CDELT[0],"Increment of axis 1", &status);
    if(NAXIS>=2)
    fits_update_key(fptr, TDOUBLE, "CDELT2", &CDELT[1],"Increment of axis 2", &status);
    if(NAXIS>=3)
    fits_update_key(fptr, TDOUBLE, "CDELT3", &CDELT[2],"Increment of axis 3", &status);
    if(NAXIS>=4)
    fits_update_key(fptr, TDOUBLE, "CDELT4", &CDELT[3],"Increment of axis 4", &status);

 

    // removed, leave as example                                                              //AWS20061121
    /* Write a keyword; must pass the ADDRESS of the value */
    /*
    long  exposure = 1500;
    fits_update_key(fptr, TLONG, "EXPOSURE", &exposure,
         "Total Exposure Time", &status);
    //cout<<"update_key"<<endl;
   fits_report_error(stderr, status);//  print out any error messages 
    */

    /* Write the array of floats to the image */
   long  fpixel = 1;
    fits_write_img(fptr, TFLOAT, fpixel, nelements, array, &status);
    //cout<<"write_img"<<endl;
   fits_report_error(stderr, status);  /* print out any error messages */



   fits_close_file(fptr, &status);            /* close the file */

   fits_report_error(stderr, status);  /* print out any error messages */

   return status;
}
  /////////////////////////////////////////
  int FITS::read_keyword_double(char *filename_, int hdunum,char *keyword,double &value)//AWS20050720
{
 

   int status;
   fitsfile *fptr;

   char comment[400];
   int hdutype;                                   // AWS20040406
  
   strcpy(filename,filename_);  // copy into FITS structure

   status=0; // need this on input
   if( fits_open_file(&fptr,filename_,READONLY,&status) ) cout<<"FITS read_keyword_double open status= "<<status<<" "<<filename<<endl;

   fits_movabs_hdu(fptr,hdunum,&hdutype,&status );// AWS20040406

   if( fits_read_key(fptr,TDOUBLE,keyword ,&value ,  comment,&status) )
       cout<<"FITS read keyword "<<keyword<< " status= "<<status<<endl;

   return status;
}
  /////////////////////////////////////////
  int FITS::write_keyword_double(char *filename_, int hdunum,char *keyword,double value, char *comment)//AWS20061122
{
  // case of filename provided

   int status;
   fitsfile *fptr;
   int hdutype;                               
  
   status=0; // need this on input
   if( fits_open_file(&fptr,filename_,READWRITE,&status) ) cout<<"FITS write_keyword_double open status= "<<status<<" "<<filename<<endl;

   fits_movabs_hdu(fptr,hdunum,&hdutype,&status ); // hdunum=1 for simple file with no extensions

   if( fits_write_key(fptr,TDOUBLE,keyword ,&value ,comment,&status) )cout<<"FITS write keyword "<<keyword<< " status= "<<status<<endl;

   fits_close_file   (fptr, &status);         /* close the file */
   fits_report_error(stderr, status);         /* print out any error messages */

   return status;
}
  /////////////////////////////////////////
  int FITS::write_keyword_double(                 int hdunum,char *keyword,double value, char *comment)//AWS20061122
{
  // case of filename already in object
   int status;
   fitsfile *fptr;
   int hdutype;                               
  
   status=0; // need this on input
   if( fits_open_file(&fptr,filename ,READWRITE,&status) ) cout<<"FITS write_keyword_double open status= "<<status<<" "<<filename<<endl;

   fits_movabs_hdu(fptr,hdunum,&hdutype,&status ); // hdunum=1 for simple file with no extensions

   if( fits_write_key(fptr,TDOUBLE,keyword ,&value ,comment,&status) )cout<<"FITS write keyword "<<keyword<< " status= "<<status<<endl;

   fits_close_file   (fptr, &status);         /* close the file */
   fits_report_error(stderr, status);         /* print out any error messages */

   return status;
}
