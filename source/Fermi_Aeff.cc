
/*

this version:

setenv FITSIO_DIRECTORY /afs/ipp-garching.mpg.de/home/a/aws/propagate/c/cfitsio/3.26/gcc_sles11_olga2/cfitsio
setenv FITS_TOOLS_DIRECTORY /afs/ipp-garching.mpg.de/home/a/aws/propagate/c/galprop/trunk/tools/fits_tools
setenv HEALPIX_PATH /afs/ipp-garching.mpg.de/home/a/aws/Healpix/Healpix_3.11/src/cxx/generic_gcc
g++ Fermi_Aeff.cc $FITS_TOOLS_DIRECTORY/FITS.cc -I$FITSIO_DIRECTORY/include -I$FITS_TOOLS_DIRECTORY -L$FITSIO_DIRECTORY/lib -I$HEALPIX_PATH/include -L$HEALPIX_PATH/lib -lhealpix_cxx -lcxxsupport -lcfitsio -O3

pre-healpix version:

setenv FITSIO_DIRECTORY /afs/ipp-garching.mpg.de/home/a/aws/propagate/c/cfitsio/3.26/gcc_sles11_olga2/cfitsio
setenv FITS_TOOLS_DIRECTORY /afs/ipp-garching.mpg.de/home/a/aws/propagate/c/galprop/trunk/tools/fits_tools
g++ Fermi_Aeff.cc  $FITS_TOOLS_DIRECTORY/FITS.cc -I$FITSIO_DIRECTORY/include -I$FITS_TOOLS_DIRECTORY  -L$FITSIO_DIRECTORY/lib  -lcfitsio -O3

*/

/*
 Averages Fermi exposure cube over sky, to give an estimate of the effective area Aeff as a function of energy. The units are exposure cm^2 sec but will be used only for the relative Aeff(E). 
 the cube is CAR  projection so the averaging is not uniform, but Aeff is presumably anyway hardly dependent on direction. Could restrict range of latitude to be more representative.

 For use with energy dispersion. This assumes that the energy grid of true energy is the same for the RSP and exposure cubes !

A. Strong, MPE 17 Dec 2014
*/

using namespace std;
#include<iostream>
#include<fstream>
#include<cstring>
#include<cstdlib>
#include<vector>
#include<valarray>


#include"Fermi_Aeff.h"
#include"FITS.h"
#include"fitsio.h" // needed although FITS.h has it

// healpix classes
#include "arr.h"
#include "healpix_map.h"
#include "healpix_map_fitsio.h"
#include "fitshandle.h"
//////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////
void Fermi_Aeff::read(string exposure_filename, int debug)
{
 

  cout<<">> Fermi_Aeff::read, string filename"<<endl;

  FITS expcube;

  char exposure_filename_char[1000];
  strcpy (exposure_filename_char, exposure_filename.c_str());

  cout<<"exposure_filename_char: "<<exposure_filename_char<<endl;

  expcube.read(exposure_filename_char);
  cout<<" excube: dimensions "<<expcube.NAXES[0]<<" "<<expcube.NAXES[1]<<" "<<expcube.NAXES[2]<<endl;

  //expcube.print();

  nE_true=expcube.NAXES[2];
  cout<<" expcube number of energy points nE_true = "<<nE_true<<endl;


  cout<<"averaging expcube over full sky"<<endl;

  Aeff_average.resize(nE_true);
  Aeff_average = 0.;

  for (int i=0;i<expcube.NAXES[0];i++)
  for (int j=0;j<expcube.NAXES[1];j++)
  for (int k=0;k<expcube.NAXES[2];k++)
     Aeff_average[k]+=expcube(i,j,k);

  Aeff_average/= expcube.NAXES[0] * expcube.NAXES[1];

  if(debug==1)  for (int k=0;k<nE_true;k++) cout<<"k="<<k<<" Aeff_average="<< Aeff_average[k]<<endl;


  // read energies

  fitsfile *fptr;

   cout<<"opening  FITS file with fitsio: "<< exposure_filename_char <<endl;

   int status=0; // need this on input
        
   if( fits_open_file(&fptr, exposure_filename_char ,READONLY,&status) )   cout<<"FITS open status= "<<status<<endl;

   fits_report_error(stderr, status);  /* print out any error messages */
   int hdunum=2; // 1=primary, 2=1st extension, assumed to be ENERGIES
   int hdutype;

   status=0; 
   fits_movabs_hdu (fptr, hdunum, &hdutype,&status);   cout<<"FITS movabs_hdu status= "<<status<<endl;

   double *E_true_;
   E_true_ = new double[nE_true];
   int datatype=TDOUBLE;
   int colnum=1;
   LONGLONG firstrow=1; 
   LONGLONG firstelem=1;
   LONGLONG nelements=nE_true;
   double nulval=0;
   int    anynul=0;

   fits_read_col       (fptr, datatype, colnum,  firstrow,  firstelem, nelements, &nulval, E_true_, &anynul, &status);

   E_true.resize(nE_true);
   for (int k=0;k<nE_true;k++) E_true[k]=E_true_[k];

   if(debug==1)  for (int k=0;k<nE_true;k++) cout<<"k="<<k<<" E_true="<< E_true[k] <<" Aeff_average="<< Aeff_average[k]  <<endl;
   

   //////////////////

  initialized=1;

  cout<<"<< Fermi_Aeff::read, string filename"<<endl;

  return;
}
/////////////////////////////////////////////////////////////////////
void Fermi_Aeff::read(string exposure_filename, string filetype, int debug)
{
 

  cout<<">> Fermi_Aeff::read: exposure_filename="<<exposure_filename<<"  filetype="<<filetype<<endl;

 if(filetype=="mapcube")
 {

  FITS expcube;

  char exposure_filename_char[1000];
  strcpy (exposure_filename_char, exposure_filename.c_str());

  cout<<"exposure_filename_char: "<<exposure_filename_char<<endl;

  expcube.read(exposure_filename_char);
  cout<<" excube: dimensions "<<expcube.NAXES[0]<<" "<<expcube.NAXES[1]<<" "<<expcube.NAXES[2]<<endl;

  //expcube.print();

  nE_true=expcube.NAXES[2];
  cout<<" expcube number of energy points nE_true = "<<nE_true<<endl;


  cout<<"averaging expcube over full sky"<<endl;

  Aeff_average.resize(nE_true);
  Aeff_average = 0.;

  for (int i=0;i<expcube.NAXES[0];i++)
  for (int j=0;j<expcube.NAXES[1];j++)
  for (int k=0;k<expcube.NAXES[2];k++)
     Aeff_average[k]+=expcube(i,j,k);

  Aeff_average/= expcube.NAXES[0] * expcube.NAXES[1];

  if(debug==1)  for (int k=0;k<nE_true;k++) cout<<"k="<<k<<" Aeff_average="<< Aeff_average[k]<<endl;


  // read energies

  fitsfile *fptr;

   cout<<"opening  FITS file with fitsio: "<< exposure_filename_char <<endl;

   int status=0; // need this on input
        
   if( fits_open_file(&fptr, exposure_filename_char ,READONLY,&status) )   cout<<"FITS open status= "<<status<<endl;

   fits_report_error(stderr, status);  /* print out any error messages */
   int hdunum=2; // 1=primary, 2=1st extension, assumed to be ENERGIES
   int hdutype;

   status=0; 
   fits_movabs_hdu (fptr, hdunum, &hdutype,&status);   cout<<"FITS movabs_hdu status= "<<status<<endl;

   double *E_true_;
   E_true_ = new double[nE_true];
   int datatype=TDOUBLE;
   int colnum=1;
   LONGLONG firstrow=1; 
   LONGLONG firstelem=1;
   LONGLONG nelements=nE_true;
   double nulval=0;
   int    anynul=0;

   fits_read_col       (fptr, datatype, colnum,  firstrow,  firstelem, nelements, &nulval, E_true_, &anynul, &status);

   E_true.resize(nE_true);
   for (int k=0;k<nE_true;k++) E_true[k]=E_true_[k];

   if(debug==1)  for (int k=0;k<nE_true;k++) cout<<"k="<<k<<" E_true="<< E_true[k] <<" Aeff_average="<< Aeff_average[k]  <<endl;
  
 } // filetype=="mapcube"

  /////////////////////////////////// healpix file
   
  // based on /afs/ipp-garching.mpg.de/home/a/aws/volume3/galprop/trunk/tools/healpix_tools/healpix_skymap_convert_bothways.cc 

 if(filetype=="healpix")
 {
   cout<<"reading exposure healpix file "<<exposure_filename<<endl;

 
  int hdu;
    
  fitshandle inputhandle;

  inputhandle.open(exposure_filename);
  hdu=2;
  inputhandle.goto_hdu(hdu);

  int ncolnum=0;
  ncolnum=inputhandle.ncols();
  cout<<" number of columns ="<<ncolnum<<endl;

  int nenergy=ncolnum;

  int NSIDE;                                                                                                                   
  inputhandle.get_key(string("NSIDE"),NSIDE);                                                          
  cout<<"NSIDE="<<NSIDE<<endl;                                                                        

  int npix = 12 * pow(NSIDE,2); // HealPix standard relation for number of pixels as function of NSIDE 

  long ndata   = nenergy*npix;




  ndata=npix*nenergy;  

  cout<<"npix="<<npix<<" nenergy="<<nenergy<<" ndata="<<ndata<<endl;
  arr<double>data; // HealPix array class
  data.alloc(ndata);

  vector<valarray<double> > fulldata;
  fulldata.resize(npix);
  for(int i=0;i<npix;i++)fulldata[i].resize(nenergy);

  Healpix_Map<double> map;

  int j=0;

  for (int colnum=1;colnum<=ncolnum;colnum++)

  {
   if(debug==1) cout<< "reading "<<exposure_filename<<",  column number: "<< colnum;

   read_Healpix_map_from_fits(exposure_filename,map,colnum,2);// HDU 2 (1=primary)

  if(debug==0||debug==1)
  {
   cout<<" Npix  = "<<map.Npix(); 
   cout<<" Nside = "<<map.Nside();
   cout<<" Order = "<<map.Order();
   cout<<" Scheme= "<<map.Scheme()<<endl; // 0 = RING, 1 = NESTED
  }

  

  for(int i=0;i<map.Npix();i++,j++)
  {  
   fulldata[i][colnum-1]=map[i];
  }

  } // colnum


  cout<<" number of data values read ="<<j<<endl;


 // read the energies assuming in HDU 3 as for galprop healpix (header name ENERGIES) or EBOUNDS (then two columns or three with CHANNEL)
  cout<<endl<<"===== processing ENERGIES or EBOUNDS extension"<<endl<<endl;

  hdu=3;
  inputhandle.goto_hdu(hdu);

  int ncols=inputhandle.ncols();
  cout<<"input file energies : hdu="<<hdu<<" number of columns="<<ncols<<endl;

  vector<string> input_colname;
  input_colname.resize(ncols);

  vector<arr<double> > input_energies;
  input_energies.resize(ncols);

  // Fermi Science Tools  EBOUNDS are in HEASARC-standard keV.
  //              gardian EBOUNDS are in                  MeV.

  // Exposure energies are in MeV however, and units may not be present in header (ENERGIES extension, column 1).

  // This program always outputs in MeV, so check input units and convert if required.

  

  string  input_energy_units="MeV";

  if(  inputhandle.key_present(string("TUNIT2")) ) //EBOUNDS extension, column  E_MIN
  {
  string TUNIT2; 
  inputhandle.get_key(string("TUNIT2"),TUNIT2);
  cout<<"TUNIT2="<<TUNIT2<<endl;
  if(TUNIT2=="keV") input_energy_units="keV";
  }
  else
  {
    cout<<"TUNIT2 not in header, assuming MeV units"<<endl;
  }

  cout<<"input energy units="<<input_energy_units<<endl;

  string output_energy_units="MeV";

  for(int colnum=1;colnum<=ncols;colnum++)
  {
   

   input_colname[colnum-1]=inputhandle .colname(colnum);// index starts at 0
   cout<<"column number "<<colnum<<" input column name ="<<input_colname[colnum-1]<<endl;

 
   inputhandle.read_entire_column(colnum,input_energies[colnum-1]); // size set by routine

   for( j=0;j<nenergy;j++) 
   {
     cout<<"input_energies["<<j<<"]="<<input_energies[colnum-1][j];
    if(input_colname[colnum-1]!="CHANNEL")cout<<" "<<input_energy_units;
    cout<<endl;
   }

   if(input_energy_units=="keV") // applies only to EBOUNDS extension
   if(colnum==2||colnum==3)      // EBOUNDS colnum=1 is CHANNEL so don't convert it
   {
       for( j=0;j<nenergy;j++)input_energies[colnum-1][j]*=1.0e-3;
       cout<<"converted to MeV for output"<<endl;
       for( j=0;j<nenergy;j++) {cout<<"input_energies["<<j<<"]="<<input_energies[colnum-1][j]<<" "<<output_energy_units<<endl;}
   }

  } //colnum


  nE_true=nenergy;
  cout<<" exposure number of energy points nE_true = "<<nE_true<<endl;


  cout<<"averaging expcube over full sky"<<endl;

  Aeff_average.resize(nE_true);
  Aeff_average = 0.;

  for (int i=0;i<map.Npix();i++)
  for (int k=0;k<nE_true;k++)
     Aeff_average[k] += fulldata[i][k];

  Aeff_average/= map.Npix();

  if(debug==1)  for (int k=0;k<nE_true;k++) cout<<"k="<<k<<" Aeff_average="<< Aeff_average[k]<<endl;


   E_true.resize(nE_true);
   int colnum=1; // valid for Science Tools exposure healpix, extension name "ENERGIES"
   for (int k=0;k<nE_true;k++) E_true[k]= input_energies[colnum-1][k];

   if(debug==1)  for (int k=0;k<nE_true;k++) cout<<"k="<<k<<" E_true="<< E_true[k] <<" Aeff_average="<< Aeff_average[k]  <<endl;


 } // filetype=="healpix"


   //////////////////

  initialized=1;

  cout<<"<< Fermi_Aeff::read, string filename, filetype="<<filetype<<endl;

  return;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
double  Fermi_Aeff::Aeff_average_interpolated(double E, int debug)
{

  if(debug==1) cout<<">> Fermi_Aeff::Aeff_average_interpolated"<<endl;

  // assume the energy scale is logarithmic
  double dlogE= log(E_true[1] / E_true[0]);
  int i=log(E / E_true[0]) / dlogE + 1e-6;

  // case of E outside range: extrapolate (rather than setting to zero, but beware)
  int out_of_range=0;
  if(i < 0)         {i=0;         out_of_range=1;}
  if(i > nE_true-2) {i=nE_true-2; out_of_range=1;}

  // log interpolation
  double value= log(Aeff_average[i]) +   (log(E)  - log(E_true[i]) ) / (log(E_true[i+1])  - log(E_true[i]) )  *  ( log(Aeff_average[i+1])- log(Aeff_average[i]) ) ;
  value = exp(value);

  if(debug==1) cout<<"Fermi_Aeff::A_eff_average_interpolated: E=" <<E<< " i="<<i<< " E_true[i]=" <<  E_true[i]<< " E_true[i+1]=" <<  E_true[i+1]
                    << " Aeff_average[i]=" <<  Aeff_average[i] << " Aeff_average[i+1]=" <<  Aeff_average[i+1]  <<" value=" << value<<endl;

  if(out_of_range==1) cout<<"Fermi_Aeff:: E is out of range! extrapolating! A_eff_average_interpolated: E=" <<E<< " i="<<i<< " E_true[i]=" <<  E_true[i]<< " E_true[i+1]=" <<  E_true[i+1]
                    << " Aeff_average[i]=" <<  Aeff_average[i] << " Aeff_average[i+1]=" <<  Aeff_average[i+1]  <<" value=" << value<<endl;


  if(debug==1) cout<<"<< Fermi_Aeff::Aeff_average_interpolated"<<endl;
  return value;
}

////////////////////////////////// test routine, uncomment to use
/*

int main()
{
  cout<<">>Fermi_Aeff test"<<endl;
  Fermi_Aeff fermi_Aeff;

 
  int debug=1;

 

   // string filename
   // from Jean-Marc Casandjian, Dec 16 2014. Correponds to analysis for his Fermi emissivity paper. Energies 20 to 204800 MeV, log, 1001 energies. Corresponds to RSP file which he sent, with same E_true binning.

   string exposure_filename="expo_20_204800_1000E.fits.gz";
   fermi_Aeff.read(exposure_filename, debug);
   cout<<"average expcube over full sky"<<endl;

   for (int k=1;k<fermi_Aeff.nE_true;k++) cout<<"k="<<k<<" fermi_Aeff.Aeff_average="<< fermi_Aeff.Aeff_average[k]<<endl;


 
   for(double E=1;E<1e7;E*=1.5)
     cout<<"test mapcube file: E="<<E<<" MeV  Aeff_average_interpolated="<<fermi_Aeff.Aeff_average_interpolated(E,  debug)<<endl;

   string filetype="mapcube";
   fermi_Aeff.read(exposure_filename, filetype, debug);

         filetype="healpix";
   exposure_filename="/afs/ipp-garching.mpg.de/home/a/aws/propagate/c/FITS/GLAST/p8_thibaut/exp2_healpix7_andy.fits";

   fermi_Aeff.read(exposure_filename, filetype, debug);
  for(double E=1;E<1e7;E*=1.5)
     cout<<"test healpix file: E="<<E<<" MeV  Aeff_average_interpolated="<<fermi_Aeff.Aeff_average_interpolated(E,  debug)<<endl;


   cout<<"<<Fermi_Aeff test"<<endl;
   return 0;  
}

*/
