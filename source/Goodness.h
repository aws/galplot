#ifndef Goodness_h
#define Goodness_h

#include "Counts.h"
#include "Skymap.h"
#include "Model.h"
#include "Variables.h"
#include <string>
#include <vector>

/**\brief Base class for all goodness of fit evaluator.  
 *
 * This is a virtual class which has to be extended.  Defines basic routines
 * which have to be implemented by extensions.
 */
class BaseGoodness {
	public:
		/** \brief Construct a goodness object with counts, a model and a filter
		 * map.
		 * 
		 * \param model a reference to the model to fit
		 * \param filter is a skymap defining areas to exclude from the fit.  Every
		 * pixel with a value of 0 in the filter is excluded.  It has to have the
		 * same size as the counts map used when constructing the model.
		 */
		BaseGoodness(BaseModel &model, const Skymap<char> & filter) : fcounts(model.getCounts()), fmodel(model), ffilter(filter){}
		/** \brief The default operator evaluates the goodness of fit.
		 *
		 * \param variables is a Variables object defining the values of the
		 * model variables
		 * \return the goodness of fit
		 */
		virtual double operator () (const Variables &variables) const = 0;
		/** \brief Calculate the gradient
		 *
		 * \param variables is a Variables object defining the values of the model
		 * parameters
		 * \return a map of string double pairs, where the string contains the name
		 * of the variable while the double holds the value of the gradient for
		 * that variable.
		 */
		virtual std::map<std::string,double> gradient(const Variables &variables) const = 0;
		/** \brief Calculate the Hessian matrix.
		 *
		 * \param variables is a Variables object defining the values of the model
		 * parameters
		 * \return a double map where the first and second index is the two
		 * variables the second derivative was evaluated for.  Their order does not
		 * matter, the value is assumed to be the same, independent of order.
		 */
		virtual std::map<std::string, std::map<std::string,double> > hessian(const Variables &variables) const = 0;
		/** \brief Return a Skymap with the goodness of each pixel.
		 *
		 * \param vars is a Variables object defining the model variable values.
		 * \return a double precision Skymap of the goodness.
		 */
		virtual Skymap<double> getMap (const Variables & vars) const = 0;
		/** \brief Return a constant reference to the model */
		const BaseModel & getModel() const {return fmodel;}
	protected:
		const CountsMap &fcounts; //!< Constant reference to the counts
		const Skymap<char> ffilter; //!< The filter Skymap, 0 means exclude, everything else include
		BaseModel &fmodel; //!< The reference to the model is not constant, since we want the models to be able to cache their results for faster computations
};

/**\brief Uses log likelihood to evaluate the goodness of fit
 */
class LogLikelihood : public BaseGoodness {
	public:
		/** Constructor to initialize the base goodness */
		LogLikelihood(BaseModel & model, const Skymap<char> & filter);
		double operator () (const Variables & variables) const;
		std::map<std::string,double> gradient(const Variables & variables) const;
		std::map<std::string, std::map<std::string,double> > hessian(const Variables &variables) const;
		Skymap<double> getMap (const Variables & vars) const;
	private:
		std::vector<double> flogSumInt; //!< Pre-calculate the log of sum of factorials to get the proper likelihood without much CPU cost
};

#endif
