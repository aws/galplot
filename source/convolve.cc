#include"Galplot.h"                    //AWS20050920
#include"galprop_classes.h"
#include"galprop.h"


int Galplot::convolve(Distribution &map, Distribution psf)
{
    
   cout<<">>>>convolve "<<endl;

 int ip,i_long,i_lat,ii_long,ii_lat,i_dlong,i_dlong_max,i_dlat_max,i_theta;
 double theta,*psf_;
 double b1,b2,dl,dtr;   

 double theta_max;
 double solid_angle;
 
 double total_in,total_out;

 Distribution work;
 work.init(map.n_rgrid,map.n_zgrid,map.n_pgrid);

 work=0.;

 psf_=new double[psf.n_rgrid];

 // map.print();
 /*
 // test

 map=0;
      i_long= 80;i_lat=90;
    for (ip=0; ip<data.n_E_EGRET         ; ip++) map.d2[i_long][i_lat].s[ip]=1;
 // end test
 */


 if(galplotdef.verbose== -204)
 {
  cout<<" setting input map to 1.0 for test"<<endl;
  map=1.0;      // selectable debug
   }
 theta_max  =galplotdef.convolve_EGRET_thmax;
//i_dlong_max=theta_max/galaxy.d_long;      AWS20020716
 i_dlong_max=     179./galaxy.d_long;     //AWS20020716
 i_dlat_max =theta_max/galaxy.d_lat;

 dtr=acos(-1.)/180.; // degrees to radians = pi/180

               
   
   

    for (ii_lat=0;ii_lat<  map.n_zgrid; ii_lat++)
    {
     for (i_lat=0; i_lat<  map.n_zgrid;  i_lat++)
     {

       if(abs(ii_lat-i_lat)<=i_dlat_max)
       {

	 //	 cout<<"ip ii_lat i_lat "<<ip<<" "<<ii_lat<<" "<<i_lat<<endl;

	b1=(galaxy.lat_min+ii_lat*galaxy.d_lat)*dtr;
   	b2=(galaxy.lat_min+ i_lat*galaxy.d_lat)*dtr;

        // solid angle of input pixel in steradian
        solid_angle=sabin(galaxy.lat_min,galaxy.d_long,galaxy.d_lat,i_lat);

      for (i_dlong=0; i_dlong<  i_dlong_max;  i_dlong++)
      {

        dl=(i_dlong*galaxy.d_long)*dtr;

	theta=( acos(sin(b1)*sin(b2)+cos(b1)*cos(b2)*cos(dl) ) )/dtr;
	//cout<<"b1= "<<b1/dtr<<" b2="<<b2/dtr<<" dl="<<dl/dtr<<" theta="<<theta<<endl;
       
	if(theta<=theta_max)
	{

        
         i_theta=theta/data.EGRET_psf_dtheta;

         if(i_theta<psf.n_rgrid)
         {
          for (ip=0; ip<data.n_E_EGRET         ; ip++)
          psf_[ip]=psf.d1[i_theta].s[ip]*solid_angle;
	  //  cout<<"b1= "<<b1/dtr<<" b2="<<b2/dtr<<" dl="<<dl/dtr<<" theta="<<theta<<" psf="<<psf_<<endl;

          for (ii_long=0;ii_long<  map.n_rgrid; ii_long++)
          {
           i_long=ii_long+i_dlong;
           if(i_long>=map.n_rgrid)i_long-=map.n_rgrid;
       
         //cout<<ii_lat<<" "<<i_lat<<" "<<i_dlong<<" "<<ii_long<<" "<<i_long<<endl;

	   //  for (ip=0; ip<data.n_E_EGRET         ; ip++)                  AWS20031017
           for (ip=0; ip<galplotdef.convolve_EGRET_iEmax         ; ip++)//   AWS20031017
            work.d2[ii_long][ii_lat].s[ip]+=map.d2[i_long][i_lat].s[ip] * psf_[ip];

           if(i_dlong>0) // to avoid double counting of pixels with same longitude
	   {
           i_long=ii_long-i_dlong;
           if(i_long< 0          )i_long+=map.n_rgrid;

           //for (ip=0; ip<data.n_E_EGRET         ; ip++)                    AWS20031017
           for (ip=0; ip<galplotdef.convolve_EGRET_iEmax         ; ip++)//   AWS20031017
            work.d2[ii_long][ii_lat].s[ip]+=map.d2[i_long][i_lat].s[ip] * psf_[ip];
	   }//if

         //cout<<ii_lat<<" "<<i_lat<<" "<<i_dlong<<" "<<ii_long<<" "<<i_long<<endl;

         }//ii_long

	   }//if i_theta
	}//if theta

      }//id_long

      }//if(abs(ii_lat-i_lat)
     }//i_lat
    }//ii_lat
     
 
    // assign energy ranges which were not convolved                             AWS20031017
    for (ip=galplotdef.convolve_EGRET_iEmax; ip<data.n_E_EGRET         ; ip++)// AWS20031017                    
    for (i_long=0; i_long<  map.n_rgrid;  i_long++)
    for (i_lat =0; i_lat <  map.n_zgrid;  i_lat++ )
     work.d2[i_long][i_lat].s[ip]= map.d2[i_long][i_lat].s[ip];
       
     cout<<" convolution applied only for "
         <<data.E_EGRET[0]<<"-"<<data.E_EGRET[galplotdef.convolve_EGRET_iEmax]<<" MeV "
	 <<"max. angle="<<galplotdef.convolve_EGRET_thmax<<" degrees"<<endl;

    // test and apply normalization

   for (ip=0; ip<data.n_E_EGRET         ; ip++)                    
   {
    total_in =0.;
    total_out=0.;

     for (i_long=0; i_long<  map.n_rgrid;  i_long++)
     for (i_lat =0; i_lat <  map.n_zgrid;  i_lat++ )
       {
	 total_in += map.d2[i_long][i_lat].s[ip];
	 total_out+=work.d2[i_long][i_lat].s[ip];
       }
     
     cout<<" convolution for "<<data.E_EGRET[ip]<<"-"<<data.E_EGRET[ip+1]<<" MeV "
         <<" total before="<<total_in<<" total after="<<total_out<<" ratio="<<total_out/total_in;//AWS20050104

     if(total_out<=0.0)  cout<<"   *** total <=0 so no renormalization"  ;                       //AWS20050104
     cout<<endl;                                                                                 //AWS20050104

     if(total_out>0.0)                   //AWS20050104
     {

     if(galplotdef.convolve_EGRET==2)
       { 
     for (i_long=0; i_long<  map.n_rgrid;  i_long++)
     for (i_lat =0; i_lat <  map.n_zgrid;  i_lat++ )
       {
       
	 work.d2[i_long][i_lat].s[ip]*=total_in/total_out;
       }//for
       }//if 

     // correct only for PSF sampling error at high energies
     if(galplotdef.convolve_EGRET==3)
       { 
     for (i_long=0; i_long<  map.n_rgrid;  i_long++)
     for (i_lat =0; i_lat <  map.n_zgrid;  i_lat++ )
       {
         if(total_out /total_in> 1.0)
	 work.d2[i_long][i_lat].s[ip]*=total_in/total_out;
       }//for
       }//if 

     }//if total_out

   }//ip

     if(galplotdef.convolve_EGRET==1)   cout<< "maps NOT  renormalized to total (if>0) before convolution!"<<endl;
     if(galplotdef.convolve_EGRET==2)   cout<< "maps ARE  renormalized to total (if>0) before convolution for all ratios!"<<endl;
     if(galplotdef.convolve_EGRET==3)   cout<< "maps ARE  renormalized to total (if>0) before convolution but only if ratio> 1 !"<<endl;

   map=work;


   work.delete_array(); //AWS20050104

  cout<<"<<<<convolve "<<endl;
   return 0;

}
