

#include"Galplot.h"                    
#include"galprop_classes.h"
#include"galprop.h"

#include"Isotropic.h" //AWS20081119

#include"SourceModel2.h"//AWS20091211

int Galplot::gen_GLAST_models()        
{
 int status=0;

 double exposure_factor;

 double long_min,lat_min,d_long,d_lat;        

 FITS work;

 char  infile[100];



  cout<<" >>>> gen_GLAST_models"<<endl;



  // here is just a test of Isotropic 
  /* AWS20110803
  cout<<"just testing Isotropic class"<<endl;
  Isotropic isotropic;                                                      //AWS20081119
  isotropic.read(configure.fits_directory,galplotdef.isotropic_bgd_file);   //AWS20081119


  isotropic.print();                                                        //AWS20081119


  isotropic.interpolated(1999.,1);                                          //AWS20081119
  isotropic.interpolated(11.  ,1);                                          //AWS20081119
  isotropic.interpolated(1e6  ,1);                                          //AWS20081119
  isotropic.integrated  (1e3,2e3,1);                                        //AWS20081202
  isotropic.integrated  (1e0,2e3,1);                                        //AWS20081202
  isotropic.integrated  (1e3,2e7,1);                                        //AWS20081202
  isotropic.integrated  (1e4,2e5,1);                                        //AWS20081202
  */
  //////////////////////////////////////////////////////////////////////

  ///  read_solar_skymap();                                                  //AWS20110719 moved to end to avoid problems

  // use gardian Sources class
  cout<<endl<<" Generating source intensity skymaps using gardian Sources class"<<endl;

  Sources sources_input;                                                     //AWS20081209
  cout<<"reading Fermi Source Catalogue"<<endl;
 
  
  
  string Fermi_cat_file=string(configure.fits_directory)+string(galplotdef.Fermi_cat_file);//AWS20081215
  cout<<"reading Fermi catalogue from "<<Fermi_cat_file<<endl;
  sources_input.read(Fermi_cat_file);

 
  // AWS20110719
 if(TRUE) //AWS20110819  AWS20200117      FALSE->TRUE  
 {
  cout<<"test writing Fermi Source Catalogue"<<endl;
  sources_input.write(string(configure.fits_directory)+"/galplot_Fermi_cat_test_write.fits");
 }

  int numberOfSources=sources_input.size();
  cout<<"number of sources in Fermi catalogue="<<numberOfSources<<endl;
  
 
 // intensity maps on galprop healpix and energy grid

  //  int order=data.GLAST_counts_healpix.getCountsMap().Order(); // from healpix_base    AWS20110803 use galprop instead

  int order=galdef.healpix_order; //AWS20110803
  cout<<"creating Sources skymap with healpix order = "<<order<<endl;
  
  valarray<double> energies(galaxy.n_E_gammagrid);
  for (int ip=0;ip<galaxy.n_E_gammagrid;ip++) energies[ip]=galaxy.E_gamma[ip];



  // unconvolved.GLAST_unconvolved_intensity_sources = Skymap<double> (data.GLAST_counts_healpix.getCountsMap().Order(), energies);//AWS20091210 use energies from galaxy grid

 unconvolved.GLAST_unconvolved_intensity_sources = Skymap<double> (order, energies);//AWS20110803 use energies from galaxy grid and order from galprop 

  sources_input.genFluxMap(numberOfSources, unconvolved.GLAST_unconvolved_intensity_sources ); //AWS20091208

  unconvolved.GLAST_unconvolved_intensity_sources/= unconvolved.GLAST_unconvolved_intensity_sources.solidAngle(); // flux to intensity

  // since genFluxMap produces incorrect values below Fermi energies, set these to zero AWS20100104
  double E_Fermi_min=50.;// 100 MeV->50 MeV  AWS20100113 to avoid zero in normal plot range
  for (int ip=0;ip<galaxy.n_E_gammagrid;ip++)
  {
    if(galaxy.E_gamma[ip]<E_Fermi_min)
      for (int ipix=0;ipix<unconvolved.GLAST_unconvolved_intensity_sources.Npix();ipix++) unconvolved.GLAST_unconvolved_intensity_sources[ipix][ip]=0.0;
  }

  cout<<"unconvolved.GLAST_unconvolved_intensity_sources.sum="<<unconvolved.GLAST_unconvolved_intensity_sources.sum()<<endl;

  //  cout<<"  convolved.GLAST_convolved_intensity_sources.sum  ="<<  convolved.GLAST_convolved_intensity_sources  .sum()<<endl;// NB not assigned yet

 if(FALSE) //AWS20110819
   unconvolved.GLAST_unconvolved_intensity_sources.write("../FITS/galplot_GLAST_unconvolved_intensity_sources_skymap.fits");

  cout<<endl<<" Generating source intensity skymaps using gardian Sources class completed"<<endl;


   //
  
  //--------------------------------------------------------------------------------------------------

  // counts from sources using SourceModel2 class
  cout<<endl<<" Generating source counts skymaps using gardian SourceModel2 class"<<endl;

  // now  have to create the Skymap to hold the counts first. Ordering scheme defaults to  RING

   convolved.  GLAST_convolved_counts_sources = Skymap<double> (data.GLAST_counts_healpix.getCountsMap().Order(), data.GLAST_counts_healpix.getEMin(), data.GLAST_counts_healpix.getEMax())  ;//AWS20091208

 unconvolved.GLAST_unconvolved_counts_sources = Skymap<double> (data.GLAST_counts_healpix.getCountsMap().Order(), data.GLAST_counts_healpix.getEMin(), data.GLAST_counts_healpix.getEMax());  //AWS20100219


 Parameters pars;

 // filter Skymap is needed by BaseModel now   AWS20091204
     Skymap<char> filterMap(data.GLAST_counts_healpix.getCountsMap().Order(), data.GLAST_counts_healpix.getEMin(), data.GLAST_counts_healpix.getEMax());
                  filterMap = char(1);

		                   // NB avoid name clash with galplot variable configure 
  unsigned int configuremodel = 3; //AWS20091211 for new gardian    1=EXPOSURE_CORRECT, 2=CONVOLVE, 3=both (default for this optional parameter)



  //AWS20110718
if(galplotdef.convolve_GLAST==0)
{
 cout<<" galplotdef.convolve_GLAST==0 so applying only exposure  in convolved maps"<<endl;
 configuremodel = 1; //AWS20110718
}
  

  // use SourceModel2 of new gardian (in BaseModel) to enable counts and convolution
   string scalerVariable="FixedSourceScaling";

   SourceModel2 sourcemodel2 (  data.GLAST_exposure_healpix,  data.GLAST_counts_healpix,  data.GLAST_psf_healpix, sources_input,pars, filterMap,scalerVariable,configuremodel);//AWS20091210;

  
  
      cout<<endl<<"finished generating sourcemodel2"<<endl;

      Variables source_variables;
      source_variables=sourcemodel2.getVariables();

      sourcemodel2.getMap(source_variables,  convolved.  GLAST_convolved_counts_sources);
      cout<<"finished generating GLAST_convolved_counts_sources"<<endl;

      sourcemodel2.getMap(source_variables,unconvolved.GLAST_unconvolved_counts_sources);//NB unconvolved=convolved!
      cout<<"finished generating GLAST_unconvolved_counts_sources"<<endl;

 if(FALSE) //AWS20110819
      convolved.GLAST_convolved_counts_sources.write(string(configure.fits_directory)+"/galplot_GLAST_convolved_counts_sources_healpix.fits");

  //////////////////////////////////////////////////////////////////




  Sources sources; 
 
  // parameters (read in from parameter file by gardian) need to be supplied 

  string parameter, value;

  parameter="galdefID";                value=galplotdef.galdef_ID;     pars.setParameter(parameter,value);  pars.getParameter(parameter,value    );  cout<< parameter <<"="<<value<<endl;
  parameter="galpropFitsDirectory";    value=configure.fits_directory; pars.setParameter(parameter,value);  pars.getParameter(parameter,value    );  cout<< parameter <<"="<<value<<endl;//AWS20080613

  // in this case each parameter is a pair of values
  parameter="HIR_ModelMapScale";  value="1.0 0.0001"; pars.setParameter(parameter,value);  pars.getParameter(parameter,value    );  cout<< parameter <<"="<<value<<endl;
  parameter="H2R_ModelMapScale";  value="1.0 0.0001"; pars.setParameter(parameter,value);  pars.getParameter(parameter,value    );  cout<< parameter <<"="<<value<<endl;
  parameter="ics_ModelMapScale";  value="1.0 0.0001"; pars.setParameter(parameter,value);  pars.getParameter(parameter,value    );  cout<< parameter <<"="<<value<<endl;

  // this file name is required, the others are constructed in HIH2IC from pars.galdefID
  parameter="ics_ModelMapName";   value="ics_isotropic_healpix_" +  string(galplotdef.galdef_ID) ; 
                                                      pars.setParameter(parameter,value);  pars.getParameter(parameter,value    );  cout<< parameter <<"="<<value<<endl;

						      // added HII AWS20090811
  parameter="HII_ModelMapScale";  value="1.0 0.0001"; pars.setParameter(parameter,value);  pars.getParameter(parameter,value    );  cout<< parameter <<"="<<value<<endl; //AWS20090811

  cout<<"creating    convolved.GLAST_model_HIH2IC"<<endl;




  int nrings_in;  // number of HI, CO rings in galprop output maps

  if(strcmp(galdef.version,"54")  <0  )  nrings_in =  9 ; // till v53: new maps have  9 rings
  if(strcmp(galdef.version,"54") >=0  )  nrings_in = 17;  // from v54: new maps have 17 rings   
                                                     
  cout<<"galprop version "<<galdef.version<< " number of skymap rings = "<< nrings_in<<endl;

 

  cout<< "generating *convolved* model"<<endl;            
                                    




  configuremodel = 3; //AWS20091211 for new gardian    1=EXPOSURE_CORRECT, 2=CONVOLVE, 3=both (default for this optional parameter)

if(galplotdef.convolve_GLAST==0)
{
 cout<<" galplotdef.convolve_GLAST==0 so applying only exposure  in convolved maps"<<endl;
 configuremodel = 1; //AWS20110718
}



  convolved.GLAST_model_HIH2IC = new HIH2IC(  data.GLAST_exposure_healpix,  data.GLAST_counts_healpix,  data.GLAST_psf_healpix, sources,pars, filterMap, nrings_in,configuremodel);//AWS20091210

  
  cout<<"gen_GLAST_models: generated convolved.GLAST_model_HIH2IC"<<endl; //AWS20091207


  Variables variables;
  variables.add(string("HIR_ModelMapScale"), 0.0, 0.0) ;
  variables.add(string("H2R_ModelMapScale"), 0.0, 0.0) ;
  variables.add(string("ics_ModelMapScale"), 0.0, 0.0) ;
  variables.add(string("HII_ModelMapScale"), 0.0, 0.0) ; //AWS20090811

  // use scaling to pick out separate components
  // total 
  variables["HIR_ModelMapScale"]= 1.0 ;   
  variables["H2R_ModelMapScale"]= 1.0 ;  
  variables["ics_ModelMapScale"]= 1.0 ; 
  variables["HII_ModelMapScale"]= 1.0 ;  //AWS20090811
       

  // now  have to create the Skymap to hold the counts first. Ordering scheme defaults to  RING
  convolved.GLAST_convolved_counts = Skymap<double> (data.GLAST_counts_healpix.getCountsMap().Order(), data.GLAST_counts_healpix.getEMin(), data.GLAST_counts_healpix.getEMax());//AWS20091207

  convolved.GLAST_model_HIH2IC->getMap(variables,convolved.GLAST_convolved_counts);                //AWS20091207 new gardian method
               cout  <<"convolved_counts.Npix="<<convolved.GLAST_convolved_counts.Npix()   << endl;//AWS20091207


  //  if(galplotdef.verbose==-2103){cout<<"GLAST_convolved counts:"<<endl;convolved.GLAST_convolved_counts.print(cout);}

  // HI 
  variables["HIR_ModelMapScale"]= 1.0 ;   
  variables["H2R_ModelMapScale"]= 0.0 ;  
  variables["ics_ModelMapScale"]= 0.0 ; 
  variables["HII_ModelMapScale"]= 0.0 ;  //AWS20090811



       convolved.GLAST_convolved_counts_HI = Skymap<double> (data.GLAST_counts_healpix.getCountsMap().Order(), data.GLAST_counts_healpix.getEMin(), data.GLAST_counts_healpix.getEMax());//AWS20091207

  convolved.GLAST_model_HIH2IC->getMap(variables,convolved.GLAST_convolved_counts_HI);                //AWS20091207 new gardian method
            cout  <<"convolved_counts_HI.Npix="<<convolved.GLAST_convolved_counts_HI.Npix()   << endl;//AWS20091207

  //  if(galplotdef.verbose==-2103){cout<<"GLAST_convolved counts_HI:"<<endl;convolved.GLAST_convolved_counts_HI.print(cout);}

  // H2 
  variables["HIR_ModelMapScale"]= 0.0 ;   
  variables["H2R_ModelMapScale"]= 1.0 ;  
  variables["ics_ModelMapScale"]= 0.0 ; 
  variables["HII_ModelMapScale"]= 0.0 ;  //AWS20090811



      convolved.GLAST_convolved_counts_H2 = Skymap<double> (data.GLAST_counts_healpix.getCountsMap().Order(), data.GLAST_counts_healpix.getEMin(), data.GLAST_counts_healpix.getEMax());//AWS20091207

  convolved.GLAST_model_HIH2IC->getMap(variables,convolved.GLAST_convolved_counts_H2);                //AWS20091207 new gardian method

            cout  <<"convolved_counts_H2.Npix="<<convolved.GLAST_convolved_counts_H2.Npix()   << endl;//AWS20091207


  //  if(galplotdef.verbose==-2103){cout<<"GLAST_convolved counts_H2:"<<endl;convolved.GLAST_convolved_counts_H2.print(cout);}


  // IC
  variables["HIR_ModelMapScale"]= 0.0 ;   
  variables["H2R_ModelMapScale"]= 0.0 ;  
  variables["ics_ModelMapScale"]= 1.0 ; 
  variables["HII_ModelMapScale"]= 0.0 ;  //AWS20090811

  


       convolved.GLAST_convolved_counts_IC = Skymap<double> (data.GLAST_counts_healpix.getCountsMap().Order(), data.GLAST_counts_healpix.getEMin(), data.GLAST_counts_healpix.getEMax());//AWS20091207

  convolved.GLAST_model_HIH2IC->getMap(variables,convolved.GLAST_convolved_counts_IC);                //AWS20091207 new gardian method

            cout  <<"convolved_counts_HI.Npix="<<convolved.GLAST_convolved_counts_IC.Npix()   << endl;//AWS20091207

  //  if(galplotdef.verbose==-2103){cout<<"GLAST_convolved counts_IC:"<<endl;convolved.GLAST_convolved_counts_IC.print(cout);}



  // HII                                  AWS20090811
  variables["HIR_ModelMapScale"]= 0.0 ;   
  variables["H2R_ModelMapScale"]= 0.0 ;  
  variables["ics_ModelMapScale"]= 0.0 ; 
  variables["HII_ModelMapScale"]= 1.0 ;                  

  

      convolved.GLAST_convolved_counts_HII = Skymap<double> (data.GLAST_counts_healpix.getCountsMap().Order(), data.GLAST_counts_healpix.getEMin(), data.GLAST_counts_healpix.getEMax());//AWS20091207

  convolved.GLAST_model_HIH2IC->getMap(variables,convolved.GLAST_convolved_counts_HII);                //AWS20091207 new gardian method
            cout  <<"convolved_counts_HI.Npix="<<convolved.GLAST_convolved_counts_HII.Npix()   << endl;//AWS20091207

  //  if(galplotdef.verbose==-2103){cout<<"GLAST_convolved counts_HII:"<<endl;convolved.GLAST_convolved_counts_HII.print(cout);}


  


  //--------------------------------------------------------------------------------------

if (data.GLAST_exposure_integrated_init==0) // only compute once, for all energies

{
  cout<<"    integrating GLAST exposure for all data energy bands"<<endl;
  data.GLAST_exposure_integrated_init=1;

  // integrated exposure must have same size as counts
 

  data.GLAST_exposure_integrated =Skymap<double>( convolved.GLAST_convolved_counts.Order() ,  data.GLAST_counts_healpix.  getEMin()    .size()  ) ;  // Order() in healpix_base.h
 
 cout  <<"convolved_counts.Npix="<<convolved.GLAST_convolved_counts.Npix()   <<"   data.GLAST_exposure_integrated.Npix="<<data.GLAST_exposure_integrated.Npix()<<endl; // check for equality

   for (int ipix=0;ipix<data.GLAST_exposure_integrated.Npix() ;ipix++)
   {
    SM::Coordinate coordinate=convolved.GLAST_convolved_counts.pix2coord( ipix);
    //    if(galplotdef.verbose==-1902)      {cout<<"  pixel coordinate from pix2coord:";coordinate.print(cout);cout<<endl;}

    for(int  i_E_GLAST=0;i_E_GLAST< data.GLAST_counts_healpix.getEMin().size();i_E_GLAST++)
    {
     double index=-2.0;// -2.0 gave nan but OK now
     double exposure_integrated_;

     exposure_integrated_=
      data.GLAST_exposure_healpix .getWeightedExposure
        (coordinate,  data.GLAST_counts_healpix.getEMin()[i_E_GLAST], data.GLAST_counts_healpix.getEMax()[i_E_GLAST],  index) ;

     //              exposure_integrated[ipix][i_E_GLAST]=exposure_integrated_;
     data.GLAST_exposure_integrated[ipix][i_E_GLAST]=exposure_integrated_;

     if(galplotdef.verbose==-1902)
     cout<<"plot_GLAST_profile_healpix: Emin="<<  data.GLAST_counts_healpix.getEMin()[i_E_GLAST]<<" Emax="<<  data.GLAST_counts_healpix.getEMax()[i_E_GLAST]
         <<" exposure_integrated_="<<exposure_integrated_<<endl;

    }
   }

   //   if(galplotdef.verbose==-1903) {   cout<<"data.GLAST_exposure_integrated:"<<endl; data.GLAST_exposure_integrated.print(cout);}
    } // if

  cout<<"    integrating GLAST exposure for all data energy bands completed"<<endl;


  //--------------------------------------------------------------------------------------



 


  Skymap<double> convolved_counts_over_exposure;

  convolved.GLAST_convolved_counts_over_exposure = convolved.GLAST_convolved_counts; // to get correct size

  // check the consistency of Skymap sizes
  cout<<"data.GLAST_exposure_integrated  .Npix()                ="<<data.GLAST_exposure_integrated                  .Npix()      <<endl;
  cout<<"GLAST_convolved_counts                .Npix()          ="<<convolved.GLAST_convolved_counts                .Npix()      <<endl;
  cout<<"GLAST_convolved_counts_over_exposure  .Npix()          ="<<convolved.GLAST_convolved_counts_over_exposure  .Npix()      <<endl;
  cout<<"data.GLAST_counts_healpix.getEMin().size()             ="<<data.GLAST_counts_healpix.getEMin()             .size()      <<endl;
  cout<<"data.GLAST_exposure_integrated  .nSpectra()            ="<<data.GLAST_exposure_integrated                  .nSpectra()  <<endl;
  cout<<"GLAST_convolved_counts                .nSpectra()      ="<<convolved.GLAST_convolved_counts                .nSpectra()  <<endl;
  cout<<"GLAST_convolved_counts_over_exposure  .nSpectra()      ="<<convolved.GLAST_convolved_counts_over_exposure  .nSpectra()  <<endl;
  cout<<"GLAST_convolved_counts.solid_Angle()                   ="<<convolved.GLAST_convolved_counts                .solidAngle()<<endl;


  for ( int ipix=0;     ipix     < data.GLAST_exposure_integrated     .Npix();ipix++)
  for ( int i_E_GLAST=0;i_E_GLAST< data.GLAST_counts_healpix.getEMin().size();i_E_GLAST++)
  {
   convolved.GLAST_convolved_counts_over_exposure[ipix][i_E_GLAST] = convolved.GLAST_convolved_counts[ipix][i_E_GLAST] / data.GLAST_exposure_integrated[ipix][i_E_GLAST];
  } //i_E_GLAST

  //  if(galplotdef.verbose==-2101){   cout<<"convolved_counts_over_exposure before dividing by solid angle: "<<endl;  convolved.GLAST_convolved_counts_over_exposure.print(cout);} AWS20091204 and similar lines

  convolved.GLAST_convolved_counts_over_exposure/=convolved.GLAST_convolved_counts.solidAngle();

  //  if(galplotdef.verbose==-2102){   cout<<"convolved_counts_over_exposure after  dividing by solid angle: "<<endl;  convolved.GLAST_convolved_counts_over_exposure.print(cout);}





  /* dividing Skymap by Skymap  not working, gives just original Skymap
  convolved_counts_over_exposure = convolved_counts;
  convolved_counts_over_exposure  /= data.GLAST_exposure_integrated;

//   if(galplotdef.verbose==-2100){  cout<<"data.GLAST_exposure_integrated:" <<endl;  data.GLAST_exposure_integrated .print(cout);}
//   if(galplotdef.verbose==-2100){  cout<<"convolved_counts_over_exposure: "<<endl;  convolved_counts_over_exposure .print(cout);}




    Skymap<double> testdivide;
    testdivide=  data.GLAST_exposure_integrated;
    testdivide/= data.GLAST_exposure_integrated;
  cout<<"testdivide: "<<endl;  testdivide.print(cout); // proves it is not working
  */






  // ------------------------------------------ unconvolved skymaps --------------------------------------

 

  cout<<endl<<endl<< "generating *unconvolved* model"<<endl;

  



  // use configure to specify  no convolution
  configuremodel = 1; //AWS20091211 for new gardian    1=EXPOSURE_CORRECT, 2=CONVOLVE, 3=both (default for this optional parameter)

  unconvolved.GLAST_model_HIH2IC = new HIH2IC(  data.GLAST_exposure_healpix,  data.GLAST_counts_healpix,  data.GLAST_psf_healpix, sources,pars, filterMap, nrings_in,configuremodel);//AWS20091210

 // use scaling to pick out separate components
  // total 
  variables["HIR_ModelMapScale"]= 1.0 ;   
  variables["H2R_ModelMapScale"]= 1.0 ;  
  variables["ics_ModelMapScale"]= 1.0 ; 
  variables["HII_ModelMapScale"]= 1.0 ;  //AWS20090811


      unconvolved.GLAST_unconvolved_counts = Skymap<double> (data.GLAST_counts_healpix.getCountsMap().Order(), data.GLAST_counts_healpix.getEMin(), data.GLAST_counts_healpix.getEMax());//AWS20091207

      unconvolved.GLAST_model_HIH2IC->getMap(variables,unconvolved.GLAST_unconvolved_counts);                //AWS20091207 new gardian method
                   cout  <<"unconvolved_counts.Npix="<<unconvolved.GLAST_unconvolved_counts.Npix()   << endl;//AWS20091207

  //  if(galplotdef.verbose==-2103){cout<<"GLAST_unconvolved counts:"<<endl;unconvolved.GLAST_unconvolved_counts.print(cout);}

  // HI 
  variables["HIR_ModelMapScale"]= 1.0 ;   
  variables["H2R_ModelMapScale"]= 0.0 ;  
  variables["ics_ModelMapScale"]= 0.0 ; 
  variables["HII_ModelMapScale"]= 0.0 ;  //AWS20090811
  


      unconvolved.GLAST_unconvolved_counts_HI = Skymap<double> (data.GLAST_counts_healpix.getCountsMap().Order(), data.GLAST_counts_healpix.getEMin(), data.GLAST_counts_healpix.getEMax());//AWS20091207

      unconvolved.GLAST_model_HIH2IC->   getMap(variables,unconvolved.GLAST_unconvolved_counts_HI);                //AWS20091207 new gardian method
                   cout  <<"unconvolved_counts_HI.Npix="<<unconvolved.GLAST_unconvolved_counts_HI.Npix()   << endl;//AWS20091207

  //  if(galplotdef.verbose==-2103){cout<<"GLAST_unconvolved counts_HI:"<<endl;unconvolved.GLAST_unconvolved_counts_HI.print(cout);}

  // H2 
  variables["HIR_ModelMapScale"]= 0.0 ;   
  variables["H2R_ModelMapScale"]= 1.0 ;  
  variables["ics_ModelMapScale"]= 0.0 ; 
  variables["HII_ModelMapScale"]= 0.0 ;  //AWS20090811
  


      unconvolved.GLAST_unconvolved_counts_H2 = Skymap<double> (data.GLAST_counts_healpix.getCountsMap().Order(), data.GLAST_counts_healpix.getEMin(), data.GLAST_counts_healpix.getEMax());//AWS20091207

      unconvolved.GLAST_model_HIH2IC->   getMap(variables,unconvolved.GLAST_unconvolved_counts_H2);                //AWS20091207 new gardian method
                   cout  <<"unconvolved_counts_H2.Npix="<<unconvolved.GLAST_unconvolved_counts_H2.Npix()   << endl;//AWS20091207
  //  if(galplotdef.verbose==-2103){cout<<"GLAST_unconvolved counts_H2:"<<endl;unconvolved.GLAST_unconvolved_counts_H2.print(cout);}


  // IC
  variables["HIR_ModelMapScale"]= 0.0 ;   
  variables["H2R_ModelMapScale"]= 0.0 ;  
  variables["ics_ModelMapScale"]= 1.0 ; 
  variables["HII_ModelMapScale"]= 0.0 ;  //AWS20090811
  

       unconvolved.GLAST_unconvolved_counts_IC = Skymap<double> (data.GLAST_counts_healpix.getCountsMap().Order(), data.GLAST_counts_healpix.getEMin(), data.GLAST_counts_healpix.getEMax());//AWS20091207

      unconvolved.GLAST_model_HIH2IC->   getMap(variables,unconvolved.GLAST_unconvolved_counts_IC);                //AWS20091207 new gardian method
                   cout  <<"unconvolved_counts_IC.Npix="<<unconvolved.GLAST_unconvolved_counts_IC.Npix()   << endl;//AWS20091207
  //  if(galplotdef.verbose==-2103){cout<<"GLAST_unconvolved counts_IC:"<<endl;unconvolved.GLAST_unconvolved_counts_IC.print(cout);}


  // HII                                  AWS20090811
  variables["HIR_ModelMapScale"]= 0.0 ;   
  variables["H2R_ModelMapScale"]= 0.0 ;  
  variables["ics_ModelMapScale"]= 0.0 ; 
  variables["HII_ModelMapScale"]= 1.0 ;                  

  


      unconvolved.GLAST_unconvolved_counts_HII = Skymap<double> (data.GLAST_counts_healpix.getCountsMap().Order(), data.GLAST_counts_healpix.getEMin(), data.GLAST_counts_healpix.getEMax());//AWS20091207

      unconvolved.GLAST_model_HIH2IC->   getMap(variables,unconvolved.GLAST_unconvolved_counts_HII);                //AWS20091207 new gardian method
                   cout  <<"unconvolved_counts_HII.Npix="<<unconvolved.GLAST_unconvolved_counts_HII.Npix()   << endl;//AWS20091207

  //  if(galplotdef.verbose==-2103){cout<<"GLAST_unconvolved counts_HII:"<<endl;unconvolved.GLAST_unconvolved_counts_HII.print(cout);}

  //--------------------------------------------------------------------------------------



 


  Skymap<double> unconvolved_counts_over_exposure;

  unconvolved.GLAST_unconvolved_counts_over_exposure = unconvolved.GLAST_unconvolved_counts; // to get correct size

  // check the consistency of Skymap sizes
  cout<<"data.GLAST_exposure_integrated  .Npix()                ="<<data.GLAST_exposure_integrated                  .Npix()      <<endl;
  cout<<"GLAST_unconvolved_counts                .Npix()          ="<<unconvolved.GLAST_unconvolved_counts                .Npix()      <<endl;
  cout<<"GLAST_unconvolved_counts_over_exposure  .Npix()          ="<<unconvolved.GLAST_unconvolved_counts_over_exposure  .Npix()      <<endl;
  cout<<"data.GLAST_counts_healpix.getEMin().size()             ="<<data.GLAST_counts_healpix.getEMin()             .size()      <<endl;
  cout<<"data.GLAST_exposure_integrated  .nSpectra()            ="<<data.GLAST_exposure_integrated                  .nSpectra()  <<endl;
  cout<<"GLAST_unconvolved_counts                .nSpectra()      ="<<unconvolved.GLAST_unconvolved_counts                .nSpectra()  <<endl;
  cout<<"GLAST_unconvolved_counts_over_exposure  .nSpectra()      ="<<unconvolved.GLAST_unconvolved_counts_over_exposure  .nSpectra()  <<endl;
  cout<<"GLAST_unconvolved_counts.solid_Angle()                   ="<<unconvolved.GLAST_unconvolved_counts                .solidAngle()<<endl;


  for ( int ipix=0;     ipix     < data.GLAST_exposure_integrated     .Npix();ipix++)
  for ( int i_E_GLAST=0;i_E_GLAST< data.GLAST_counts_healpix.getEMin().size();i_E_GLAST++)
  {
   unconvolved.GLAST_unconvolved_counts_over_exposure[ipix][i_E_GLAST] = unconvolved.GLAST_unconvolved_counts[ipix][i_E_GLAST] / data.GLAST_exposure_integrated[ipix][i_E_GLAST];
  } //i_E_GLAST

  //  if(galplotdef.verbose==-2101){   cout<<"unconvolved_counts_over_exposure before dividing by solid angle: "<<endl;  unconvolved.GLAST_unconvolved_counts_over_exposure.print(cout);}

  unconvolved.GLAST_unconvolved_counts_over_exposure/=unconvolved.GLAST_unconvolved_counts.solidAngle();

  // if(galplotdef.verbose==-2102){   cout<<"unconvolved_counts_over_exposure after  dividing by solid angle: "<<endl;  unconvolved.GLAST_unconvolved_counts_over_exposure.print(cout);}



///////////////////////////
// apply energy dispersion if required
  /*

 does not work here since the skymaps are in energy bands for the data
 while dispersion requires the model energies per MeV.
  int Skymap_Energy_Dispersion_debug=0;

  Skymap_Energy_Dispersion( convolved.GLAST_convolved_counts_HI ,Skymap_Energy_Dispersion_debug );
  Skymap_Energy_Dispersion( convolved.GLAST_convolved_counts_H2 ,Skymap_Energy_Dispersion_debug );
  Skymap_Energy_Dispersion( convolved.GLAST_convolved_counts_IC ,Skymap_Energy_Dispersion_debug );
  Skymap_Energy_Dispersion( convolved.GLAST_convolved_counts_HII,Skymap_Energy_Dispersion_debug );

  */
  ////////////////////////////////////////////////////////////////////////////////////



 read_solar_skymap();                                                      //AWS20110719 moved here from start


 ///////////////////////////////////////////////////////////////////////////////////////


  cout<<" <<<< gen_GLAST_models"<<endl;

  
return status;
}
