#include "Exposure.h"
#include "Skymap.h"
#include "PhysicalConstants.h"
#include "CCfits/CCfits"
#include "Utils.h"
#include <valarray>
#include <vector>
#include <string>
#include <sstream>
#include <cmath>
#include <stdexcept>

#define ASSERT(a,b,routine) \
   if (a < b) { \
      std::cerr<<"a should be equal to b in routine"<<std::endl; \
      std::cerr<<"Their sizes are "<<a<<" and "<<b<<", respectively"<<std::endl; \
      throw ("Assertion failed"); \
   }

Exposure::Exposure(const std::string &fileName){
	readFile(fileName);
}

void Exposure::readFile(const std::string &fileName){
	fDataSkymap.load(fileName);
	CalculateInterpolation();
}

const Skymap<double> & Exposure::getExposureMap() const{
	return fDataSkymap;
}

const std::valarray<double> & Exposure::getEnergies() const{
	return fDataSkymap.getSpectra();
}

void Exposure::writeFile(const std::string & filename){
	fDataSkymap.write(filename);
}

double Exposure::getWeightedExposure(SM::Coordinate &co, double eMin, double eMax, double index) const{
	//First find the index range
	int minIndex, maxIndex;
	const std::valarray<double> &energies = getEnergies();
	Utils::findIndexRange(energies, eMin, eMax, minIndex, maxIndex);
	//Then do the integration
	double exposure = 0; //The integral
	double norm = 0; //The normalization integral
	for (int i = minIndex; i < maxIndex; ++i){
	   if (fabs(fIndex[co][i]+index+1) > 1e-10){
	      exposure += fPrefactor[co][i]*(pow(std::min(energies[i+1], eMax), fIndex[co][i]+index+1) - pow(std::max(energies[i], eMin), fIndex[co][i]+index+1))/(fIndex[co][i]+index+1);
	   }else{
	      exposure += fPrefactor[co][i]*(log(std::min(energies[i+1], eMax)) - log(std::max(energies[i], eMin)));
	   }
	   if (fabs(index+1) > 1e-10){
	      norm += (pow(std::min(energies[i+1], eMax), index+1) - pow(std::max(energies[i], eMin), index+1))/(index+1);
	   }else{
	      norm += log(std::min(energies[i+1], eMax)) - log(std::max(energies[i], eMin));
	   }
	}
	//Normalize with a power law integral
	exposure /= norm;
	return exposure;
}

Skymap<double> Exposure::mapToCounts(Skymap<double> inMap, const std::valarray<double> &eMin, const std::valarray<double> &eMax, int order) const{
   //Make sure the sizes fit
   ASSERT(eMin.size(),eMax.size(),mapToCounts)
   //If order is the default -1, set to the order of inMap
   if (order == -1){
      order = inMap.Order();
   }
   //If order is less than the order of inMap, we must rebin
   if (inMap.Order() > order){
      inMap = inMap.rebin(order);
   }

   //Create the output map, of order
   Skymap<double> countMap(order, eMin, eMax);

   //calculate the index order, preparation for spectral integration
   std::vector<std::vector<int> > fluxIndex, expIndex;
   std::vector<std::vector<double> > energies = indexSort(eMin, eMax, inMap.getSpectra(), fluxIndex, expIndex);

   //Status indicator
   Utils::StatusIndicator status("Exposure correction", countMap.Npix()/3000+1);

#pragma omp parallel for schedule(static) default(shared)
   for(int i = 0; i < countMap.Npix(); ++i){
      SM::Coordinate co(countMap.pix2ang(i));
      countMap[i] = convertSpectra(energies, inMap[co], inMap.getSpectra(), fluxIndex, expIndex, co);
      countMap[i] *= countMap.solidAngle();
      if (i % 3000 == 0) {
	 status.refresh();
      }
   }
   return countMap;
}

std::valarray<double> Exposure::spectraToCounts(const std::valarray<double> &intensities, const std::valarray<double> &energies, const std::valarray<double> &eMin, const std::valarray<double> &eMax, const SM::Coordinate &co) const {
   //Make sure the sizes fit
   ASSERT(eMin.size(),eMax.size(),mapToCounts)

   //calculate the index order, preparation for spectral integration
   std::vector<std::vector<int> > fluxIndex, expIndex;
   std::vector<std::vector<double> > sortEn = indexSort(eMin, eMax, energies, fluxIndex, expIndex);

   return convertSpectra(sortEn, intensities, energies, fluxIndex, expIndex, co);
}

std::valarray<double> Exposure::spectraToCounts(double index, double prefactor, double pivotEnergy, const std::valarray<double> &eMin, const std::valarray<double> &eMax, const SM::Coordinate &co) const{
   //Make sure the sizes fit
   ASSERT(eMin.size(),eMax.size(),mapToCounts)

   //Create the output
   std::valarray<double> counts(eMin.size());

   //Normalize the prefactor to 1 MeV 
   prefactor = prefactor/pow(pivotEnergy,index);

   //Integrate the spectra
   for (int i = 0; i < eMin.size(); ++i){
      int expMinIndex, expMaxIndex;
      Utils::findIndexRange(getEnergies(), eMin[i], eMax[i], expMinIndex, expMaxIndex);
      counts[i] = 0;
      for (int j = expMinIndex; j<expMaxIndex; ++j){
	 double Emin = std::max(eMin[i],getEnergies()[j]);
	 double Emax = std::min(eMax[i],getEnergies()[j+1]);
	 if (fabs(index + fIndex[co][j] + 1) > 1e-10) {
	    counts[i] += prefactor*fPrefactor[co][j]/(fIndex[co][j]+index+1)*(pow(Emax, fIndex[co][j]+index+1) - pow(Emin, fIndex[co][j]+index+1));
	 } else {
	    counts[i] += prefactor*fPrefactor[co][j]*(log(Emax) - log(Emin));
	 }
      }
   }
   return counts;
}

void Exposure::createTestMap(int order, const std::valarray<double> & energies, const std::valarray<double> & functionPars, const std::string &function){
	//Begin by creating the energy dependent exposure
	std::valarray<double> exp(energies.size());
	if ( function == "Linear" ) {
		if ( functionPars.size() == 2 ) {
			for ( int i = 0; i < energies.size(); ++i ) {
				exp[i] = functionPars[0]*energies[i] + functionPars[1];
			}
		}else{
			throw std::invalid_argument("Expected functionPars to be of size 2 for Linear function");
		}
	} else if ( function == "Powerlaw" ) {
		if ( functionPars.size() == 2 ) {
			for ( int i = 0; i < energies.size(); ++i ) {
				exp[i] = functionPars[1]*pow(energies[i],functionPars[0]);
			}
		}else{
			throw std::invalid_argument("Expected functionPars to be of size 2 for Powerlaw function");
		}
	} else if ( function == "Array" ) {
		if ( functionPars.size() == energies.size() ) {
			for ( int i = 0; i < energies.size(); ++i ) {
				exp[i] = functionPars[i];
			}
		}else{
			throw std::invalid_argument("Expected the size of functionPars to be equal to the size of energies for Array function");
		}
	} else {
		throw std::invalid_argument("Function parameter unknown, try: Linear, Powerlaw or Array (case sensitive)");
	}
	//Create the skymap and assign the exposure to all pixels
	fDataSkymap.Resize(order, energies, RING);
	for ( int i = 0; i < fDataSkymap.Npix(); ++i ) {
		fDataSkymap[i] = exp;
	}
	CalculateInterpolation();
}

std::vector<std::vector<double> > Exposure::indexSort(const std::valarray<double> &eMin, const std::valarray<double> &eMax, const std::valarray<double> &energies, std::vector<std::vector<int> > &fluxIndex, std::vector<std::vector<int> > &expIndex) const{
   ASSERT(eMin.size(),eMax.size(),mapToCounts)
   //Resize the output
   fluxIndex.resize(eMin.size());
   expIndex.resize(eMin.size());
   std::vector<std::vector<double> > energiesOut(eMin.size());

   //Loop over the energy bins and sort for each
   for (size_t i = 0; i < eMin.size(); ++i){
      //First find the indexes in the spectra of exposure and fluxes, corresponding to energies just
      //below and just above the bin boundaries
      int fluxMinIndex, fluxMaxIndex, expMinIndex, expMaxIndex;
      Utils::findIndexRange(energies, eMin[i], eMax[i], fluxMinIndex, fluxMaxIndex);
      Utils::findIndexRange(getEnergies(), eMin[i], eMax[i], expMinIndex, expMaxIndex);

      //The first one is simple
      energiesOut[i].clear();
      fluxIndex[i].clear();
      expIndex[i].clear();
      energiesOut[i].push_back(eMin[i]);
      int j = fluxMinIndex;
      int k = expMinIndex;
      fluxIndex[i].push_back(j);
      expIndex[i].push_back(k);
      //We loop over the index range, increasing the index of the exposure or
      //flux, depending on which one has the lowest energy.  We must be aware
      //that we may need to extrapolate, if the energy range of either is not
      //large enough.
      while (j < fluxMaxIndex || k < expMaxIndex){ 
	 //If neither has reached the end, we must see which energy is lower
	 if (j < fluxMaxIndex && k < expMaxIndex){ 
	    //If they have both reached the energy limit, we break
	    if (energies[j+1] > eMax[i] && getEnergies()[k+1] > eMax[i]){
	       break;
	    }
	    if (energies[j+1] == getEnergies()[k+1]){
	       ++j;
	       ++k;
	       energiesOut[i].push_back(energies[j]);
	    }else if (energies[j+1] < getEnergies()[k+1]){
	       ++j;
	       energiesOut[i].push_back(energies[j]);
	    }else{
	       ++k;
	       energiesOut[i].push_back(getEnergies()[k]);
	    }
	 }else if (j < fluxMaxIndex){
	    //if next energy is greater than the boundaries, we break
	    if (energies[j+1] > eMax[i]){
	       break;
	    }
	    ++j;
	    energiesOut[i].push_back(energies[j]);
	 }else{
	    //if next energy is greater than the boundaries, we break
	    if (getEnergies()[k+1] > eMax[i]){
	       break;
	    }
	    ++k;
	    energiesOut[i].push_back(getEnergies()[k]);
	 }
	 fluxIndex[i].push_back(j);
	 expIndex[i].push_back(k);
      }
      //Finally we push the boundary energy
      energiesOut[i].push_back(eMax[i]);
   }
   return energiesOut;
}


std::valarray<double> Exposure::convertSpectra(const std::vector<std::vector<double> > &energies, const std::valarray<double> &flux, const std::valarray<double> &enFlux, const std::vector<std::vector<int> > &fluxIndex, std::vector<std::vector<int> > &expIndex, const SM::Coordinate &co) const {
   ArraySlice<double> sl(&flux[0], flux.size());
   return convertSpectra(energies, sl, enFlux, fluxIndex, expIndex, co);
}

std::valarray<double> Exposure::convertSpectra(const std::vector<std::vector<double> > &energies, const ArraySlice<double> &flux, const std::valarray<double> &enFlux, const std::vector<std::vector<int> > &fluxIndex, std::vector<std::vector<int> > &expIndex, const SM::Coordinate &co) const {
   //Make sure everything is of the right size, at least self consistent.
   ASSERT(energies.size(),fluxIndex.size(),convertSpectra)
   ASSERT(energies.size(),expIndex.size(),convertSpectra)

   std::valarray<double> counts(energies.size());

   //Loop over all the energy bins
   for (size_t i = 0; i < energies.size(); ++i){
      //Calculate the power law interpolation for the input spectra
      std::vector<double> fluxPowerLawIndex, fluxPowerLawPrefactor;
      fluxPowerLawIndex.reserve(fluxIndex[i].size());
      fluxPowerLawPrefactor.reserve(fluxIndex[i].size());
      for (int j = fluxIndex[i].front(); j <= fluxIndex[i].back(); ++j){
	 //To handle extrapolation on the higher end
	 if ( j < enFlux.size() -1 ){
	    double index = 0;
	    double prefactor = 0;
	    if (isnan(flux[j]) || isinf(flux[j])){
	       std::cerr<<"NaNs or INFs found in the flux in convertSpectra in Exposure"<<std::endl;
	    }else{
	       if(flux[j] > 0 && flux[j+1] > 0 ){
		  index = log(flux[j+1]/flux[j])/log(enFlux[j+1]/enFlux[j]);
	       }
	       if (fabs(index) > 10) {
		  //std::cerr<<"Great index at: ";
		  //std::cerr<<co;
		  //std::cerr<<" energy index: "<<j<<", flux values: "<<flux[j]<<" and "<<flux[j+1]<<std::endl;
		  index = index/fabs(index)*10;
		  //std::cerr<<"Index reset to "<<index<<std::endl;
	       }
	       prefactor = flux[j]/pow(enFlux[j],index);
	    }
	    fluxPowerLawIndex.push_back(index);
	    fluxPowerLawPrefactor.push_back(prefactor);
	 }else{
	    fluxPowerLawIndex.push_back(fluxPowerLawIndex.back());
	    fluxPowerLawPrefactor.push_back(fluxPowerLawPrefactor.back());
	 }
      }

      //Perform the integration
      double sum = 0;
      for (int j = 0; j< energies[i].size()-1; ++j){
	 //Power law index and prefactor for intensity
	 double gamma = fluxPowerLawIndex[fluxIndex[i][j]-fluxIndex[i][0]];
	 double prefactor = fluxPowerLawPrefactor[fluxIndex[i][j]-fluxIndex[i][0]];
	 //Power law index and prefactor for exposure
	 double egamma = fIndex[co][expIndex[i][j]];
	 double eprefactor = fPrefactor[co][expIndex[i][j]];
	 if (fabs(gamma+egamma+1) > 1e-10) {
	    sum += prefactor*eprefactor/(gamma+egamma+1)*(pow(energies[i][j+1], gamma+egamma+1) - pow(energies[i][j], gamma+egamma+1));
	 } else {
	    sum += prefactor*eprefactor*(log(energies[i][j+1]/energies[i][j]));
	 }
      }
      counts[i] = sum;
   }
   return counts;
}

void Exposure::CalculateInterpolation(){
	if (fDataSkymap.nSpectra() < 2){
		std::cerr<<"Exposure map not suitable for power law interpolation"<<std::endl;
		return;
	}
	//Use power law interpolation
	fIndex.Resize(fDataSkymap.Order(), fDataSkymap.getSpectra());
	fPrefactor.Resize(fDataSkymap.Order(), fDataSkymap.getSpectra());
	//Calculate the energy factor in the power law interpolation
	std::valarray<double> enfac(fDataSkymap.nSpectra()-1);
	for (size_t i = 0; i < enfac.size(); ++i){
	   enfac[i] = 1./log(fDataSkymap.getSpectra()[i+1]/fDataSkymap.getSpectra()[i]);
	}
	//Loop over the map and calculate the prefactor and index
	for (int i = 0; i < fDataSkymap.Npix(); ++i){
	   for (int j = 0; j < enfac.size(); ++j){
	      if (fDataSkymap[i][j] > 0 && fDataSkymap[i][j+1] > 0){
	       fIndex[i][j] = log(fDataSkymap[i][j+1]/fDataSkymap[i][j])*enfac[j];
	      }
	      fPrefactor[i][j] = fDataSkymap[i][j]/pow(fDataSkymap.getSpectra()[j], fIndex[i][j]);
	   }
	   fIndex[i][enfac.size()] = fIndex[i][enfac.size()-1];
	   fPrefactor[i][enfac.size()] = fPrefactor[i][enfac.size()-1];
	}
}

