#include"Galplot.h"                    //AWS20050920
#include"galprop_classes.h"
#include"galprop.h"
#include"constants.h"
#include"fitsio.h"

Distribution gcr_modulated; // used by gcr_modulate
//static void gcr_modulate(int i_species,double phi,int key);

int Galplot::plot_gcr_spectra_ratios()
{
   cout<<" >>>> plot_gcr_spectra_ratios    "<<endl;
   int status=0;


int i_species;            // index of species in nuclei file
int ir,ix,iy,iz,ip;
int i_ZA;                 // index of species in galplotdef parameters
int i_ratio;
int i_mod_phi;
double r;
Distribution gcr_ratio_sec;
Distribution gcr_ratio_pri;
Distribution gcr_ratio    ;


char name[100],canvastitle[100], workstring1[100],workstring2[100];
char Ytitle[100];
char psfile[400];

TCanvas *c1;
TGraph *spectrum;
TText *text;



  // generate ratios

if(gcr[0].n_spatial_dimensions==2)
{
 gcr_ratio_sec.init(gcr[0].n_rgrid,gcr[0].n_zgrid,               gcr[0].n_pgrid);
 gcr_ratio_pri.init(gcr[0].n_rgrid,gcr[0].n_zgrid,               gcr[0].n_pgrid);
 gcr_ratio.    init(gcr[0].n_rgrid,gcr[0].n_zgrid,               gcr[0].n_pgrid);
 gcr_modulated.init(gcr[0].n_rgrid,gcr[0].n_zgrid,               gcr[0].n_pgrid);
}

if(gcr[0].n_spatial_dimensions==3)
 { 
gcr_ratio_sec.init(gcr[0].n_xgrid,gcr[0].n_ygrid,gcr[0].n_zgrid,gcr[0].n_pgrid);
gcr_ratio_pri.init(gcr[0].n_xgrid,gcr[0].n_ygrid,gcr[0].n_zgrid,gcr[0].n_pgrid);
gcr_ratio.    init(gcr[0].n_xgrid,gcr[0].n_ygrid,gcr[0].n_zgrid,gcr[0].n_pgrid);
gcr_modulated.init(gcr[0].n_xgrid,gcr[0].n_ygrid,gcr[0].n_zgrid,gcr[0].n_pgrid);
 }
  

 i_mod_phi=0; // only first modulation phi: later use all


for(i_ratio=0;i_ratio<galplotdef.gcr_spectra_n_ratio;i_ratio++)
 {

  gcr_ratio_sec=0.;
  gcr_ratio_pri=0.;


  for(i_species=0;i_species<n_species;i_species++)
   {

    for(i_ZA=0;i_ZA<galplotdef.gcr_spectra_n_sec_ZA[i_ratio];i_ZA++)
     {
    if(  gcr[i_species].Z==galplotdef.gcr_spectra_sec_Z[i_ratio][i_ZA]
       &&gcr[i_species].A==galplotdef.gcr_spectra_sec_A[i_ratio][i_ZA]

       ||gcr[i_species].Z==galplotdef.gcr_spectra_sec_Z[i_ratio][i_ZA]
       &&              -1==galplotdef.gcr_spectra_sec_A[i_ratio][i_ZA]) // accept all A for this Z
     {
      cout<<"----- found secondary Z A ="<<gcr[i_species].Z<<" "<<gcr[i_species].A<<endl;
      //     gcr_ratio_sec+=gcr[i_species].cr_density;

      gcr_modulate_ratios(i_species, galplotdef.gcr_spectra_mod_phi[i_mod_phi]   ,0);//AWS20050926
      gcr_ratio_sec+=gcr_modulated;

     }//if
     }//i_ZA sec

   
    for(i_ZA=0;i_ZA<galplotdef.gcr_spectra_n_pri_ZA[i_ratio];i_ZA++)
     {
      if(  gcr[i_species].Z==galplotdef.gcr_spectra_pri_Z[i_ratio][i_ZA]
         &&gcr[i_species].A==galplotdef.gcr_spectra_pri_A[i_ratio][i_ZA]

         ||gcr[i_species].Z==galplotdef.gcr_spectra_pri_Z[i_ratio][i_ZA]
         &&              -1==galplotdef.gcr_spectra_pri_A[i_ratio][i_ZA]) // accept all A for this Z
       {
      cout<<"----- found primary   Z A ="<<gcr[i_species].Z<<" "<<gcr[i_species].A<<endl;
      //    gcr_ratio_pri+=gcr[i_species].cr_density;

      gcr_modulate_ratios(i_species,galplotdef.gcr_spectra_mod_phi[i_mod_phi],     0);//AWS20050926
      gcr_ratio_pri+=gcr_modulated;

       }//if
     }//i_ZA pri

   }//i_species
  

  gcr_ratio=gcr_ratio_sec/gcr_ratio_pri;



int ncolors=1; int *colors=0;
gStyle->SetPalette(ncolors,colors);






 // names must be different or canvas disappears

  
  strcpy(canvastitle," galdef ID ");
  strcat(canvastitle,galdef.galdef_ID);
 


 



  //====== see HowTo Style: and do before creating canvas
 
 
   TStyle *plain  = new TStyle("Plain","Plain Style (no colors/fill areas)"); 

   
   plain->SetCanvasBorderMode(0);
   plain->SetPadBorderMode(0);
   plain->SetPadColor(0);
   plain->SetCanvasColor(0);
   plain->SetTitleColor(0);
   plain->SetStatColor(0);
   gROOT->SetStyle("Plain");
  


  strcpy(name,"gcr_spectra_ratios");// to identify it: name is not plotted



   
  sprintf(name,"gcr_spectra_ratio%d",i_ratio);
  c1=new TCanvas(name,name       ,300+i_ratio  *20 ,150+i_ratio  *20,600,600);

 

  if(galplotdef.gcr_spectra_Emin              ==0.0)
    c1->DrawFrame( gcr[0].Ekin[0],                galplotdef.gcr_spectra_ratio_min[i_ratio],
                   gcr[0].Ekin[gcr[0].n_pgrid-1] ,galplotdef.gcr_spectra_ratio_max[i_ratio]);

  if( galplotdef.gcr_spectra_Emin              >0)
    c1->DrawFrame( galplotdef.gcr_spectra_Emin ,galplotdef.gcr_spectra_ratio_min[i_ratio],
                   galplotdef.gcr_spectra_Emax ,galplotdef.gcr_spectra_ratio_max[i_ratio]);

  c1->SetLogx(); 
  
  text=new TText();
  text->SetTextFont(62);
  text->SetTextColor(36);
  text->SetTextSize(0.035 );
  text->SetTextAlign(12);

  text->DrawTextNDC(.12 ,.93 ,canvastitle);// NDC=normalized coord system
  text->SetTextSize(0.025 );
  

  
  strcpy(workstring1,"(Z,A) ");
  text->DrawTextNDC(.43 ,.855  ,workstring1);// NDC=normalized coord system

  strcpy(workstring1,"");
  for(i_ZA=0;i_ZA<galplotdef.gcr_spectra_n_sec_ZA[i_ratio];i_ZA++)
  {
    sprintf(workstring2," (%d,%d)",galplotdef.gcr_spectra_sec_Z[i_ratio][i_ZA],galplotdef.gcr_spectra_sec_A[i_ratio][i_ZA]);
    strcat(workstring1,workstring2);
  }
   text->DrawTextNDC(.50 ,.88 ,workstring1);// NDC=normalized coord system

   strcpy(Ytitle,workstring1);

  strcpy(workstring1,"");
  for(i_ZA=0;i_ZA<galplotdef.gcr_spectra_n_pri_ZA[i_ratio];i_ZA++)
  {
    sprintf(workstring2," (%d,%d)",galplotdef.gcr_spectra_pri_Z[i_ratio][i_ZA],galplotdef.gcr_spectra_pri_A[i_ratio][i_ZA]);
    strcat(workstring1,workstring2);
  }
   text->DrawTextNDC(.50 ,.83 ,workstring1);// NDC=normalized coord system
   strcat(Ytitle," / "      );
   strcat(Ytitle,workstring1);

   // see User Manual 3.02b Ch 9 p 166: `:goto Greek, #: end of Greek. Symbols only in ps file
   sprintf(workstring1,"`F# = %.0f MV",galplotdef.gcr_spectra_mod_phi[i_mod_phi]   );
   text->DrawTextNDC(.15 ,.88 ,workstring1);// NDC=normalized coord system


    if(gcr[0].n_spatial_dimensions==2)
    {
    cout<<"2D"<<endl;
    for(iz=0;iz<gcr[0].n_zgrid;iz++)
      {
    for(ir=0;ir<gcr[0].n_rgrid;ir++)
      {
       if(   gcr[0].z[iz]>=galplotdef.gcr_spectra_z_min && gcr[0].z[iz]<=galplotdef.gcr_spectra_z_max
	   &&gcr[0].r[ir]>=galplotdef.gcr_spectra_r_min && gcr[0].r[ir]<=galplotdef.gcr_spectra_r_max)
	 {
	    cout<<Ytitle    <<"  r ="<<gcr[0].r[ir]<<" z="<<gcr[0].z[iz]             
                <<" phi="<<galplotdef.gcr_spectra_mod_phi[i_mod_phi]<<endl;

         spectrum=new TGraph(gcr[0].n_pgrid);

 
  
  for  (ip    =0;  ip    <gcr[0].n_pgrid;ip    ++)
  spectrum->SetPoint(ip,gcr[0].Ekin[ip],gcr_ratio.d2[ir][iz].s[ip]);

  spectrum->GetHistogram()->SetXTitle("kinetic energy, MeV/nucleon                 ");  
  spectrum->GetHistogram()->SetYTitle(Ytitle); 
  spectrum->GetHistogram()->SetLabelOffset(+0.00,"X"); //  +ve down
  spectrum->GetHistogram()->SetLabelSize  ( 0.03,"Y"); // labels, not titles
  spectrum->GetHistogram()->SetTitleOffset(+1.1,"X");  //  +ve down
  spectrum->GetHistogram()->SetTitleOffset(+1.2,"Y");  //  +ve to left
  spectrum->GetHistogram()->SetTitleSize   (0.04 ,"Y");// titles, not labels
  //Gtypes.h:enum EColor { kWhite, kBlack, kRed, kGreen, kBlue, kYellow, kMagenta, kCyan };
  spectrum->SetMarkerColor(kBlack); //AWS20100510
  spectrum->SetMarkerStyle(21); 
  spectrum->SetMarkerStyle(22); // triangles
  spectrum->SetMarkerSize (0.5);// 
  spectrum->SetLineColor(kBlack);//AWS20100510
  spectrum->SetLineWidth(1     );
  spectrum->SetLineStyle(1      );//AWS20100510
  spectrum->SetMarkerStyle(22); // triangles
  spectrum->SetMarkerSize (0.5);// 
  spectrum->Draw();       // don't use "same" for TGraph
  //  spectrum->Draw(" P ");  // points as markers           AWS20100510 
  
 
	}//if z,r
       }//ir
      }//iz

  }//2D


   if(gcr[0].n_spatial_dimensions==3)
    {
    cout<<"3D"<<endl;
    for(iz=0;iz<gcr[0].n_zgrid;iz++)
      {
    for(ix=0;ix<gcr[0].n_xgrid;ix++)
      {
    for(iy=0;iy<gcr[0].n_ygrid;iy++)
      {
	r=sqrt(gcr[0].x[ix]*gcr[0].x[ix]+ gcr[0].y[iy]*gcr[0].y[iy]);

       if(  gcr[0].z[iz]>=galplotdef.gcr_spectra_z_min && gcr[0].z[iz]<=galplotdef.gcr_spectra_z_max
	  &&           r>=galplotdef.gcr_spectra_r_min &&            r<=galplotdef.gcr_spectra_r_max)
	 {
	    cout <<  Ytitle
                 <<" x="<<gcr[0].x[ix]
                 <<" y="<<gcr[0].y[iy]
                 <<" r="<<       r         
                 <<" z="<<gcr[0].z[iz]
                 <<" phi="<<galplotdef.gcr_spectra_mod_phi[i_mod_phi]
                 <<endl;

         spectrum=new TGraph(gcr[0].n_pgrid);

 
  
  for  (ip    =0;  ip    <gcr[0].n_pgrid;ip    ++)
  spectrum->SetPoint(ip,gcr[0].Ekin[ip],gcr_ratio.d3[ix][iy][iz].s[ip]);

  spectrum->GetHistogram()->SetXTitle("kinetic energy, MeV.nucleon    ");  
  spectrum->GetHistogram()->SetYTitle(Ytitle);
  spectrum->GetHistogram()->SetLabelOffset(+0.00,"X"); //  +ve down
  spectrum->GetHistogram()->SetLabelSize  ( 0.03,"Y"); // labels, not titles
  spectrum->GetHistogram()->SetTitleOffset(+1.1,"X");  //  +ve down
  spectrum->GetHistogram()->SetTitleOffset(+1.2,"Y");  //  +ve to left
  spectrum->GetHistogram()->SetTitleSize   (0.04 ,"Y");// titles, not labels
  //Gtypes.h:enum EColor { kWhite, kBlack, kRed, kGreen, kBlue, kYellow, kMagenta, kCyan };
  spectrum->SetMarkerColor(kBlack);//AWS20100510
  spectrum->SetMarkerStyle(21); 
  spectrum->SetMarkerStyle(22); // triangles
  spectrum->SetMarkerSize (0.5);// 
  spectrum->SetLineColor(kBlack);//AWS20100510
  spectrum->SetLineWidth(1     );
  spectrum->SetLineStyle(1      );//AWS20100510
  spectrum->SetMarkerStyle(22); // triangles
  spectrum->SetMarkerSize (0.5);// 
  spectrum->Draw();       // don't use "same" for TGraph
  //  spectrum->Draw(" P ");  // points as markers  //AWS20100510
  
 
	 }//if z,x,y
        }//iy
       }//ix
      }//iz

  }//3D


  //============= plot data

   plot_gcr_spectra_ratios_data(i_ratio);

  //============== postscript output
   strcpy(workstring1,"secZA");
for(i_ZA=0;i_ZA<galplotdef.gcr_spectra_n_sec_ZA[i_ratio];i_ZA++)
  {
    sprintf(workstring2,"_%d_%d",galplotdef.gcr_spectra_sec_Z[i_ratio][i_ZA],galplotdef.gcr_spectra_sec_A[i_ratio][i_ZA]);
    strcat(workstring1,workstring2);
 }
    strcat(workstring1,"_priZA");

 for(i_ZA=0;i_ZA<galplotdef.gcr_spectra_n_pri_ZA[i_ratio];i_ZA++)
  {
    sprintf(workstring2,"_%d_%d",galplotdef.gcr_spectra_pri_Z[i_ratio][i_ZA],galplotdef.gcr_spectra_pri_A[i_ratio][i_ZA]);
    strcat(workstring1,workstring2);
 }                                                                         



  strcpy(psfile,"plots/");
  strcat(psfile,"gcr_spectra_");
  strcat(psfile,galdef.galdef_ID);
  strcat(psfile,"_"        );
  strcat(psfile,workstring1);
  // strcat(psfile,"_"        );
  // strcat(psfile,workstring2);
  strcat(psfile,".eps");

  cout<<"postscript file="<<psfile<<endl;
  c1->Print(psfile       );




  
  }// for i_ratio






  //==============


   cout<<" <<<< plot_gcr_spectra_ratios   "<<endl;
   return status;
}

// declare static to avoid conflict with function in plot_gcr_spectra.cc
//AWS20050926
//static
// now put in Galplot class so change name to distinguish (but is there a difference - check when this routine is put into use)

 void Galplot::gcr_modulate_ratios(int i_species,double phi,int key)
{

  int iz,ir,ix,iy,ip;

  gcr_modulated=gcr[i_species].cr_density;

  if(phi<1.)return;

  if(gcr[0].n_spatial_dimensions==2)
    {

    for(iz=0;iz<gcr[0].n_zgrid;iz++)
    {
    for(ir=0;ir<gcr[0].n_rgrid;ir++)
     {

       
	//   cout<<"modulate before: ir iz "<<ir<<" "<<iz<<endl;
	//  for(ip=0;ip<gcr[0].n_pgrid;ip++)cout<<gcr_modulated.d2[ir][iz].s[ip]<<endl;

        modulate(gcr[0].Ekin,gcr_modulated.d2[ir][iz].s,
                 gcr[0].n_pgrid,gcr[i_species].Z,gcr[i_species].A,phi,key);

        //  cout<<"modulate after : ir iz "<<ir<<" "<<iz<<endl;
	// for(ip=0;ip<gcr[0].n_pgrid;ip++)cout<<gcr_modulated.d2[ir][iz].s[ip]<<endl;
        
      }//ir
    }//iz


    }//if 2D


  if(gcr[0].n_spatial_dimensions==3)
    {

    for(iz=0;iz<gcr[0].n_zgrid;iz++)
    {
     for(ix=0;ix<gcr[0].n_xgrid;ix++)
     {
     for(iy=0;iy<gcr[0].n_ygrid;iy++)
      {
       


      modulate(gcr[0].Ekin,gcr_modulated.d3[ix][iy][iz].s,
               gcr[0].n_pgrid,gcr[i_species].Z,gcr[i_species].A,phi,key);

        

      }//iy
     }//ix
    }//iz


    }//if 3D



  //  gcr_modulated.print(1.0);


  cout<<" gcr_modulate: phi="<<phi<<endl;


  return;
}
