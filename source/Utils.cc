#include <valarray>
#include <vector>
#include <algorithm>
#include <iostream>
#include <iomanip>
#include <time.h>
#include "Utils.h"

int Utils::indexBelow(const std::valarray<double> &array, double value){
	int index = 0;
	while (array[index] <= value && index < array.size()) ++index;
	if(index > 0) --index;
	return index;
}

int Utils::indexAbove(const std::valarray<double> &array, double value){
	int index = 0;
	while (array[index] < value && index < array.size()-1) ++index;
	return index;
}

void Utils::findIndexRange(const std::vector<double> &array, double lowValue, double highValue, int &lowIndex, int &highIndex){
	std::valarray<double> valarr(&array[0], array.size());
	findIndexRange(valarr,lowValue, highValue, lowIndex, highIndex);
}

void Utils::findIndexRange(const std::valarray<double> &array, double lowValue, double highValue, int &lowIndex, int &highIndex){
	//lower value must be lower than higher value
	double lv = std::min(lowValue,highValue);
	double hv = std::max(lowValue,highValue);
	if (array.size() == 1){
		lowIndex = highIndex = 0;
		return;
	}
	lowIndex = indexBelow(array, lv);
	highIndex = indexAbove(array, hv);
	if (lowIndex == highIndex){
		if (lowIndex == 0){
			++highIndex;
		}else{
			--lowIndex;
		}
	}
}

double Utils::linearInterpolation(double x, const std::vector<double> &xValues, const std::vector<double> &yValues){
	int index = indexAbove(std::valarray<double>(&xValues[0], xValues.size()), x);
	if (index == 0){
		return yValues[index] + (x-xValues[index])*(yValues[index+1]-yValues[index])/(xValues[index+1]-xValues[index]);
	}else{
		return yValues[index-1] + (x-xValues[index-1])*(yValues[index]-yValues[index-1])/(xValues[index]-xValues[index-1]);
	}
}

void Utils::lagrangeCoeff4(const std::valarray<double> & x,const std::valarray<double> & y, std::valarray<double> & coeff){
	if (x.size() != 4 || y.size() != 4) {
		std::cout<<"Cannot compute Lagrange coefficient unless sizes are == 4"<<std::endl;
		return;
	}
	coeff.resize(4);
	double tmp = y[0]/((x[0]-x[1])*(x[0]-x[2])*(x[0]-x[3]));
	coeff[0] = tmp * (- ( x[1] * x[2] * x[3] ));
	coeff[1] = tmp * (x[1]*x[2] + x[1]*x[3] + x[2]*x[3]);
	coeff[2] = tmp * (-x[1] - x[2] - x[3]);
	coeff[3] = tmp;
	tmp = y[1]/((x[1]-x[0])*(x[1]-x[2])*(x[1]-x[3]));
	coeff[0] += tmp * (- ( x[0] * x[2] * x[3]));
	coeff[1] += tmp * (x[0]*x[2] + x[0]*x[3] + x[2]*x[3]);
	coeff[2] += tmp * (-x[0] - x[2] - x[3]);
	coeff[3] += tmp;
	tmp = y[2]/((x[2]-x[0])*(x[2]-x[1])*(x[2]-x[3]));
	coeff[0] += tmp * (- ( x[0] * x[1] * x[3]));
	coeff[1] += tmp * (x[0]*x[1] + x[0]*x[3] + x[1]*x[3]);
	coeff[2] += tmp * (-x[0] - x[1] - x[3]);
	coeff[3] += tmp;
	tmp = y[3]/((x[3]-x[0])*(x[3]-x[1])*(x[3]-x[2]));
	coeff[0] += tmp * (- ( x[0] * x[1] * x[2]));
	coeff[1] += tmp * (x[0]*x[1] + x[0]*x[2] + x[1]*x[2]);
	coeff[2] += tmp * (-x[0] - x[1] - x[2]);
	coeff[3] += tmp;
}

//Status indicator
Utils::StatusIndicator::StatusIndicator(const std::string & marker, int steps) : 
	fmarker(marker), 
	fsteps(steps), 
	fcurrentStep(0), 
	ftEstimate(0) 
{
	ftInit = time(NULL);
	refresh();
}
void Utils::StatusIndicator::refresh(){
#pragma omp critical
	{
	//rotary at the end needs these signs
	const std::string rot = "\\|/-"; 
	std::cout<<"\r"<<fmarker<<" ";
	if (fsteps != 0) {
		//The fraction finished
		double fraction = double(fcurrentStep)/double(fsteps);
		//Calculate the percent (integer)
		int percent = int(fraction*100);
		//Build the string
		std::string output = "";
		for (int i = 0; i < 20; ++i){
			if (i < percent/5) {
				output += "#";
			}else{
				output += "-";
			}
		}
		std::cout<<output<<" ("<<std::setw(3)<<percent<<"%) ";
		//Calculate the estimate time left, basically time from start divided by
		//the fraction finished
		if (fraction > 0){
			if ( ftEstimate == 0 ) {
				ftEstimate = double(time(NULL)-ftInit)/fraction;
			} else {
				ftEstimate = (ftEstimate + 3*double(time(NULL)-ftInit)/fraction)/4.;
			}
			int timeLeftSeconds = int(ftEstimate - (time(NULL)-ftInit));
			int timeLeftHours = timeLeftSeconds/3600;
			int timeLeftMinutes = (timeLeftSeconds - timeLeftHours*3600)/60;
			timeLeftSeconds = (timeLeftSeconds - timeLeftHours*3600 - timeLeftMinutes*60);
			std::cout<<"[Estimated time left: ";
			if (timeLeftHours){
				std::cout<<std::setw(3)<<timeLeftHours<<"h";
			} else {
				std::cout<<std::setw(4)<<"";
			}
			if (timeLeftMinutes || timeLeftHours){
				std::cout<<std::setw(2)<<timeLeftMinutes<<"m";
			} else {
				std::cout<<std::setw(3)<<"";
			}
			std::cout<<std::setw(2)<<timeLeftSeconds<<"s] ";
		}
	}
	std::cout<<rot[fcurrentStep % 4]<<std::flush;
	//Increase the step
	++fcurrentStep;
	}
}
