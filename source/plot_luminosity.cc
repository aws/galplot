#include"Galplot.h"
#include"galprop_classes.h"
#include"galprop.h"
#include"constants.h"
#include"fitsio.h"

#include <vector>

// interfaces to nH.cc
double nHI (double Rkpc, double Zkpc);
double nH2 (double Rkpc, double Zkpc);
double nH2 (double Rkpc, double Zkpc,double H2toCO);
double nHII(double Rkpc, double Zkpc);

// plot luminosity of Galaxy

int Galplot::plot_luminosity()
 {

  cout<<" >>>> plot_luminosity   "<<endl;

  if(galdef.n_spatial_dimensions==3){cout<<"plot_luminosity not working in 3D!";return 0;}//AWS20100430

  valarray<double> gamma_bremss_luminosity_spectrum   (galaxy.n_E_gammagrid); 
  valarray<double> gamma_pi0_decay_luminosity_spectrum(galaxy.n_E_gammagrid); 
  valarray<double> gamma_IC_luminosity_spectrum       (galaxy.n_E_gammagrid); 
  valarray<double> gamma_total_luminosity_spectrum    (galaxy.n_E_gammagrid); 

  valarray<double> gamma_total_integral_luminosity    (galaxy.n_E_gammagrid); 
  valarray<double> gamma_bremss_integral_luminosity   (galaxy.n_E_gammagrid); 
  valarray<double> gamma_pi0_decay_integral_luminosity(galaxy.n_E_gammagrid); 
  valarray<double> gamma_IC_integral_luminosity       (galaxy.n_E_gammagrid); 

  // integral downwards in energy
  valarray<double> gamma_total_integraldown_luminosity    (galaxy.n_E_gammagrid); 
  valarray<double> gamma_bremss_integraldown_luminosity   (galaxy.n_E_gammagrid); 
  valarray<double> gamma_pi0_decay_integraldown_luminosity(galaxy.n_E_gammagrid); 
  valarray<double> gamma_IC_integraldown_luminosity       (galaxy.n_E_gammagrid); 



  valarray<double> gamma_total_integral_photons       (galaxy.n_E_gammagrid); 
  valarray<double> gamma_bremss_integral_photons      (galaxy.n_E_gammagrid); 
  valarray<double> gamma_pi0_decay_integral_photons   (galaxy.n_E_gammagrid); 
  valarray<double> gamma_IC_integral_photons          (galaxy.n_E_gammagrid); 

  valarray<double> synchrotron_luminosity_spectrum    (galaxy.n_nu_synchgrid); 
  valarray<double> synchrotron_integral_luminosity    (galaxy.n_nu_synchgrid); 

  valarray<double> opt_luminosity_spectrum    (galaxy.ISRF[0].n_pgrid); 
  valarray<double> opt_integral_luminosity    (galaxy.ISRF[0].n_pgrid); 
  valarray<double>  IR_luminosity_spectrum    (galaxy.ISRF[0].n_pgrid); 
  valarray<double>  IR_integral_luminosity    (galaxy.ISRF[0].n_pgrid); 

  valarray<double> opt_luminosity_spectrum2   (galaxy.ISRF[0].n_pgrid); //AWS20100304
  valarray<double> opt_integral_luminosity2   (galaxy.ISRF[0].n_pgrid); //AWS20100304 
  valarray<double>  IR_luminosity_spectrum2   (galaxy.ISRF[0].n_pgrid); //AWS20100304 
  valarray<double>  IR_integral_luminosity2   (galaxy.ISRF[0].n_pgrid); //AWS20100304 


  
    cout<<"plot_luminosity: n_species="<<n_species<<" gcr[0].n_pgrid="<< gcr[0].n_pgrid <<endl;
  // using propagated CR
  valarray<double> gcr_proton_luminosity_spectrum (gcr[0].n_pgrid);
  valarray<double> gcr_helium_luminosity_spectrum (gcr[0].n_pgrid);
  valarray<double> gcr_lepton_luminosity_spectrum (gcr[0].n_pgrid);
  valarray<double> gcr_proton_integral_luminosity (gcr[0].n_pgrid);
  valarray<double> gcr_helium_integral_luminosity (gcr[0].n_pgrid);
  valarray<double> gcr_lepton_integral_luminosity (gcr[0].n_pgrid);

  // using CR source spectrum from galdef parameters
  valarray<double> gcr_proton_luminosity_spectrum2(gcr[0].n_pgrid);
  valarray<double> gcr_helium_luminosity_spectrum2(gcr[0].n_pgrid);
  valarray<double> gcr_lepton_luminosity_spectrum2(gcr[0].n_pgrid);
  valarray<double> gcr_proton_integral_luminosity2(gcr[0].n_pgrid);
  valarray<double> gcr_helium_integral_luminosity2(gcr[0].n_pgrid);
  valarray<double> gcr_lepton_integral_luminosity2(gcr[0].n_pgrid);

  // using CR source function from galprop FITS  
  valarray<double>             gcr_proton_luminosity_spectrum3(gcr[0].n_pgrid);
  valarray<double>             gcr_helium_luminosity_spectrum3(gcr[0].n_pgrid);
  valarray<double>             gcr_lepton_luminosity_spectrum3(gcr[0].n_pgrid);
  valarray<double>   gcr_primary_electron_luminosity_spectrum3(gcr[0].n_pgrid);
  valarray<double>   gcr_primary_positron_luminosity_spectrum3(gcr[0].n_pgrid);
  valarray<double> gcr_secondary_electron_luminosity_spectrum3(gcr[0].n_pgrid);
  valarray<double> gcr_secondary_positron_luminosity_spectrum3(gcr[0].n_pgrid);

  valarray<double>             gcr_proton_integral_luminosity3(gcr[0].n_pgrid);
  valarray<double>             gcr_helium_integral_luminosity3(gcr[0].n_pgrid);
  valarray<double>             gcr_lepton_integral_luminosity3(gcr[0].n_pgrid);
  valarray<double>   gcr_primary_electron_integral_luminosity3(gcr[0].n_pgrid);
  valarray<double>   gcr_primary_positron_integral_luminosity3(gcr[0].n_pgrid);
  valarray<double> gcr_secondary_electron_integral_luminosity3(gcr[0].n_pgrid);
  valarray<double> gcr_secondary_positron_integral_luminosity3(gcr[0].n_pgrid);


  valarray<double> nHI_column_density                 (galaxy.n_rgrid);
  valarray<double> nH2_column_density                 (galaxy.n_rgrid);
  valarray<double> nHII_column_density                (galaxy.n_rgrid);

  gamma_bremss_luminosity_spectrum = 0.;
  gamma_pi0_decay_luminosity_spectrum = 0.;
  gamma_IC_luminosity_spectrum = 0.;

  synchrotron_luminosity_spectrum = 0.;

  opt_luminosity_spectrum = 0.;
   IR_luminosity_spectrum = 0.;
  opt_luminosity_spectrum2= 0.; //AWS20100304
   IR_luminosity_spectrum2= 0.; //AWS20100304
   
  gcr_proton_luminosity_spectrum =0.;
  gcr_helium_luminosity_spectrum =0.;
  gcr_lepton_luminosity_spectrum =0.;
   

  nHI_column_density  = 0.;
  nH2_column_density  = 0.;
  nHII_column_density = 0.;

  double nHI_total =0.;
  double nH2_total =0.;
  double nHII_total=0.;
  double nH_total  =0.;
  double volume_total = 0.;

  double pi=acos(-1.);


  int irmax=galaxy.n_rgrid-1; // NB last point
  //  irmax=6; // for testing

  cout<<"maximum radius for luminosity = "<<galaxy.r[irmax]<<endl;



   if(galaxy.n_spatial_dimensions==2)
   {
    for(int ir=0;ir<irmax         ;ir++)
    for(int iz=0;iz<galaxy.n_zgrid;iz++)
    {

      double volume=pi * (pow(galaxy.r[ir+1],2.0) - pow(galaxy.r[ir],2.0)) * galaxy.dz ; // not exact of course since emissivity is at r[ir]. volume in kpc^3
      volume *= pow(kpc2cm,3.0); // from constants.h   volume now in cm^3

      double nHI_ =nHI (galaxy.r[ir],galaxy.z[iz]);
      double nH2_ =nH2 (galaxy.r[ir],galaxy.z[iz])*2.0; // using hard-coded Xco in this case. NB two atoms per molecule!
      double nHII_=nHII(galaxy.r[ir],galaxy.z[iz]);
      double nH  =nHI_ + nH2_ + nHII_;

     
      nHI_total+=nHI_*volume; nH2_total+=nH2_*volume;nHII_total+=nHII_*volume;nH_total+=nH*volume;

      nHI_column_density [ir] += nHI_  * galaxy.dz * kpc2cm;
      nH2_column_density [ir] += nH2_  * galaxy.dz * kpc2cm;
      nHII_column_density[ir] += nHII_ * galaxy.dz * kpc2cm;

      volume_total+=volume;		// cout<<volume<<" volume "<<volume_total<<endl;
      


     for(int ip=0;ip<galaxy.n_E_gammagrid;ip++)
     {
                                                                                                      // emissivity is per sr so need 4pi factor
      gamma_bremss_luminosity_spectrum   [ip]+=galaxy.bremss_emiss       .d2[ir][iz].s[ip] * volume * 4.*pi * nH;
      gamma_pi0_decay_luminosity_spectrum[ip]+=galaxy.pi0_decay_emiss    .d2[ir][iz].s[ip] * volume * 4.*pi * nH;

      for(int icomp=0;icomp<galaxy.n_ISRF_components  ;icomp++)
      gamma_IC_luminosity_spectrum       [ip]+=galaxy.IC_iso_emiss[icomp].d2[ir][iz].s[ip] * volume * 4.*pi;

     }

     for(int inu=0;inu<galaxy.n_nu_synchgrid;inu++)
     {
      synchrotron_luminosity_spectrum[inu] += galaxy.synchrotron_emiss.d2[ir][iz].s[inu] * volume * 4.*pi;
     }


     for(int inu=0;inu<galaxy.ISRF[0].n_pgrid;inu++)
     {
       // ISRF in eV cm-3  
       opt_luminosity_spectrum[inu] += galaxy.ISRF[0].d2[ir][iz].s[inu]/galaxy.nu_ISRF[inu] * volume ; // total energy in Galaxy in eV Hz-1
        IR_luminosity_spectrum[inu] += galaxy.ISRF[1].d2[ir][iz].s[inu]/galaxy.nu_ISRF[inu] * volume ;
     }


    } // ir,iz


    // surface integration AWS20100304

    // top and bottom surfaces (assuming symmetry so just double area of top)

    //   for(int ir=0;ir<irmax         ;ir++)
    //    for(int ir=0;ir<galaxy.ISRF[0].n_rgrid-1         ;ir++)//AWS20100415 using ISRF value since will differ from Galaxy.h

    double z_min_ISRF=-31.; //ISRF as read in. this is unfortunately hard coded to agree with read_isrf!

    int irmax_ISRF=int(( galplotdef.ISRF_luminosity_R              +.0001) / galdef.dr);//AWS20100421
    int izmin_ISRF=int((-galplotdef.ISRF_luminosity_z - z_min_ISRF +.0001) / galdef.dz);//AWS20100421
    int izmax_ISRF=int(( galplotdef.ISRF_luminosity_z - z_min_ISRF +.0001) / galdef.dz);//AWS20100421

    cout<<"plot_luminosity: irmax_ISRF="<< irmax_ISRF<<" izmin_ISRF="<<  izmin_ISRF<<" izmax_ISRF="<<  izmax_ISRF<<    endl;
    cout<<"plot_luminosity: checking ISRF bounds and correcting if necessary"<<endl;
    if (irmax_ISRF>galaxy.ISRF[0].n_rgrid-1)irmax_ISRF=galaxy.ISRF[0].n_rgrid-1;
    if (izmin_ISRF<0                       )izmin_ISRF=0;                        
    if (izmax_ISRF>galaxy.ISRF[0].n_zgrid-1)izmax_ISRF=galaxy.ISRF[0].n_zgrid-1;
    cout<<"plot_luminosity: irmax_ISRF="<< irmax_ISRF<<" izmin_ISRF="<<  izmin_ISRF<<" izmax_ISRF="<<  izmax_ISRF<<    endl;

    txt_stream<<"plot_luminosity: boundary for ISRF surface integration: ISRF_luminosity_R="<< galplotdef.ISRF_luminosity_R 
              <<" ISRF_luminosity_z  ="<<  galplotdef.ISRF_luminosity_z   <<   endl;
    txt_stream<<"plot_luminosity: irmax_ISRF="<< irmax_ISRF<<" izmin_ISRF="<<  izmin_ISRF<<" izmax_ISRF="<<  izmax_ISRF<<    endl;

    for(int ir=0;ir<=irmax_ISRF         ;ir++)//AWS20100421
     {
       //     double area  = pi * (pow(galaxy.r[ir+1],2.0) - pow(galaxy.r[ir],2.0)) * 2.0            ; // not exact of course since emissivity is at r[ir]. volume in kpc^3
       double r1= ir   *galaxy.dr; //AWS20100416 since ISRF dimensions differ from Galaxy.h
       double r2=(ir+1)*galaxy.dr; //AWS20100416 since ISRF dimensions differ from Galaxy.h
       double area  = pi * (pow(r2,2.0) - pow(r1,2.0)) * 2.0            ; // not exact of course since emissivity is at r[ir]. volume in kpc^3
              area *= pow(kpc2cm,2.0); // from constants.h   area   now in cm^2
	  
    
	      //	int iz=galaxy.ISRF[0].n_zgrid-1;//AWS20100409 seems to be OK AWS20100415 use ISRF value since will differ from Galaxy.h

	int iz=izmax_ISRF;//AWS20100421
   
     for(int inu=0;inu<galaxy.ISRF[0].n_pgrid;inu++)
     {
       // ISRF in eV cm-3 
       opt_luminosity_spectrum2[inu] += galaxy.ISRF[0].d2[ir][iz].s[inu] * area   ; // eV  cm^-3 * cm^2  = eV cm-1                      
        IR_luminosity_spectrum2[inu] += galaxy.ISRF[1].d2[ir][iz].s[inu] * area   ;


       if(galplotdef.verbose==-3000)
	if(galaxy.ISRF[0].d2[ir][iz].s[inu]>0.0||galaxy.ISRF[1].d2[ir][iz].s[inu]>0)
	  cout<<"surface integration top and botton: ir="<<ir<<" r="<<galaxy.r[ir]<<" iz="<<iz<<" z="<<galaxy.z[iz]
          <<" area="<<area
	  <<" nu="<<galaxy.nu_ISRF[inu] 
          <<" optical ISRF="<< galaxy.ISRF[0].d2[ir][iz].s[inu]
          <<" IR ISRF="<< galaxy.ISRF[1].d2[ir][iz].s[inu]
	  <<" cumulative sum (w*area, eV cm-1): opt=" <<   opt_luminosity_spectrum2[inu]    
   	  <<                                  "  IR=" <<    IR_luminosity_spectrum2[inu]   
          <<endl;
     }


     } // ir


   // perimeter
    //     for(int iz=0;iz<galaxy.ISRF[0].n_zgrid;iz++)//AWS20100415 using ISRF value since will differ from Galaxy.h

     for(int iz=izmin_ISRF; iz<=izmax_ISRF; iz++)//AWS20100421
     {
       //      int ir=irmax;
       //     double area=2.0*pi*galaxy.r[ir] * galaxy.dz;

       //       int ir  = galaxy.ISRF[0].n_rgrid-1;
       //      double r=(galaxy.ISRF[0].n_rgrid)*galaxy.dr;//AWS20100416

       int   ir= irmax_ISRF;     //AWS20100421
       double r= ir*galaxy.dr;   //AWS20100421

       double area=2.0*pi*r * galaxy.dz;           //AWS20100416
        area *= pow(kpc2cm,2.0); // from constants.h   area   now in cm^2

             for(int inu=0;inu<galaxy.ISRF[0].n_pgrid;inu++)
     {
       // ISRF in eV cm-3 
       opt_luminosity_spectrum2[inu] += galaxy.ISRF[0].d2[ir][iz].s[inu] * area   ; // eV  cm^-3 * cm^2  = eV cm-1                      
        IR_luminosity_spectrum2[inu] += galaxy.ISRF[1].d2[ir][iz].s[inu] * area   ;

        if(galplotdef.verbose==-3000)
         if(galaxy.ISRF[0].d2[ir][iz].s[inu]>0.0||galaxy.ISRF[1].d2[ir][iz].s[inu]>0)
	  cout<<"surface integration perimeter: ir="<<ir<<" r="<<r<<" iz="<<iz<<" z="<<-31.+iz*galaxy.dz  // start value hard coded from read_isrf !
          <<" area="<<area
	  <<" nu="<<galaxy.nu_ISRF[inu] 
          <<" optical ISRF="<< galaxy.ISRF[0].d2[ir][iz].s[inu]
          <<" IR ISRF="<< galaxy.ISRF[1].d2[ir][iz].s[inu]
	  <<" cumulative sum (w*area, eV cm-1): opt=" <<   opt_luminosity_spectrum2[inu]    
   	  <<                                  "  IR=" <<    IR_luminosity_spectrum2[inu]   
          <<endl;
     }


     }



       opt_luminosity_spectrum2*= 1.e-6*MEV2ERG * C;// erg cm-1 s-1 * cm s-1 = erg s-1
        IR_luminosity_spectrum2*= 1.e-6*MEV2ERG * C;

 } // if 2D      AWS20100304


   ////////////////////////////////////////////////



  gamma_total_luminosity_spectrum  = gamma_bremss_luminosity_spectrum +  gamma_pi0_decay_luminosity_spectrum + gamma_IC_luminosity_spectrum ;

  // since emissivity is MeV^2 cm-3 sr-1 MeV-1 s-1 
  // the units are  MeV s-1  as in Strong etal 2000 ApJ 537, 763 Fig 20.

  gamma_total_integral_luminosity     = 0.;
  gamma_bremss_integral_luminosity    = 0.;
  gamma_pi0_decay_integral_luminosity = 0.;
  gamma_IC_integral_luminosity        = 0.;


  gamma_total_integraldown_luminosity     = 0.;
  gamma_bremss_integraldown_luminosity    = 0.;
  gamma_pi0_decay_integraldown_luminosity = 0.;
  gamma_IC_integraldown_luminosity        = 0.;


  gamma_total_integral_photons        = 0.;
  gamma_bremss_integral_photons       = 0.;
  gamma_pi0_decay_integral_photons    = 0.;
  gamma_IC_integral_photons           = 0.;




  for(int ip =0 ;ip <galaxy.n_E_gammagrid;ip++ )
  {
  for(int ipp=ip;ipp<galaxy.n_E_gammagrid;ipp++)
   {
    gamma_total_integral_luminosity    [ip]+=  gamma_total_luminosity_spectrum    [ipp];
    gamma_bremss_integral_luminosity   [ip]+=  gamma_bremss_luminosity_spectrum   [ipp];
    gamma_pi0_decay_integral_luminosity[ip]+=  gamma_pi0_decay_luminosity_spectrum[ipp];
    gamma_IC_integral_luminosity       [ip]+=  gamma_IC_luminosity_spectrum       [ipp];

    gamma_total_integral_photons       [ip]+=  gamma_total_luminosity_spectrum    [ipp]/ galaxy.E_gamma[ipp];  // *E instead of *E^2
    gamma_bremss_integral_photons      [ip]+=  gamma_bremss_luminosity_spectrum   [ipp]/ galaxy.E_gamma[ipp];
    gamma_pi0_decay_integral_photons   [ip]+=  gamma_pi0_decay_luminosity_spectrum[ipp]/ galaxy.E_gamma[ipp];
    gamma_IC_integral_photons          [ip]+=  gamma_IC_luminosity_spectrum       [ipp]/ galaxy.E_gamma[ipp];
   }

  for(int ipp=0;ipp<ip;ipp++)
   {
    gamma_total_integraldown_luminosity    [ip]+=  gamma_total_luminosity_spectrum    [ipp];
    gamma_bremss_integraldown_luminosity   [ip]+=  gamma_bremss_luminosity_spectrum   [ipp];
    gamma_pi0_decay_integraldown_luminosity[ip]+=  gamma_pi0_decay_luminosity_spectrum[ipp];
    gamma_IC_integraldown_luminosity       [ip]+=  gamma_IC_luminosity_spectrum       [ipp];
   }

  }

  // integral Ef(E)dE = integral E^2f(E) dlog(E) = sum E^2 f(E) delta logE.  delta logE = log(E factor)
   gamma_total_integral_luminosity *= log(galaxy.E_gamma_factor);
   gamma_total_integral_luminosity *= MEV2ERG;// from constants.h

   gamma_bremss_integral_luminosity *= log(galaxy.E_gamma_factor);
   gamma_bremss_integral_luminosity *= MEV2ERG;// from constants.h

   gamma_pi0_decay_integral_luminosity *= log(galaxy.E_gamma_factor);
   gamma_pi0_decay_integral_luminosity *= MEV2ERG;// from constants.h

   gamma_IC_integral_luminosity *= log(galaxy.E_gamma_factor);
   gamma_IC_integral_luminosity *= MEV2ERG;// from constants.h



   gamma_total_integraldown_luminosity *= log(galaxy.E_gamma_factor);
   gamma_total_integraldown_luminosity *= MEV2ERG;// from constants.h

   gamma_bremss_integraldown_luminosity *= log(galaxy.E_gamma_factor);
   gamma_bremss_integraldown_luminosity *= MEV2ERG;// from constants.h

   gamma_pi0_decay_integraldown_luminosity *= log(galaxy.E_gamma_factor);
   gamma_pi0_decay_integraldown_luminosity *= MEV2ERG;// from constants.h

   gamma_IC_integraldown_luminosity *= log(galaxy.E_gamma_factor);
   gamma_IC_integraldown_luminosity *= MEV2ERG;// from constants.h





  // integral  f(E)dE = integral Ef(E) dlog(E) = sum E f(E) delta logE.  delta logE = log(E factor)
   gamma_total_integral_photons    *= log(galaxy.E_gamma_factor);
   gamma_bremss_integral_photons   *= log(galaxy.E_gamma_factor);
   gamma_pi0_decay_integral_photons*= log(galaxy.E_gamma_factor);
   gamma_IC_integral_photons       *= log(galaxy.E_gamma_factor);


   synchrotron_integral_luminosity = 0.;

    for(int  inu=0;   inu<galaxy.n_nu_synchgrid; inu++)
    for(int iinu=inu;iinu<galaxy.n_nu_synchgrid;iinu++)
     synchrotron_integral_luminosity[inu] += synchrotron_luminosity_spectrum[iinu] * galaxy.nu_synch[iinu];
     
    synchrotron_integral_luminosity *= log(galaxy.nu_synch_factor);

    double isrf_size=10.; //  kpc, very rough estimate of time for photons to escape Galaxy
    opt_luminosity_spectrum *= 1.e-6*MEV2ERG  / (isrf_size *kpc2cm/C); // eV Hz-1 to erg Hz-1 s-1
     IR_luminosity_spectrum *= 1.e-6*MEV2ERG  / (isrf_size *kpc2cm/C); // eV Hz-1 to erg Hz-1 s-1

     opt_integral_luminosity = 0.;
      IR_integral_luminosity = 0.;
     opt_integral_luminosity2= 0.;
      IR_integral_luminosity2= 0.;


    for(int  inu=0;   inu<galaxy.ISRF[0].n_pgrid; inu++)
    for(int iinu=inu;iinu<galaxy.ISRF[0].n_pgrid;iinu++)
    {
     opt_integral_luminosity[inu] += opt_luminosity_spectrum[iinu] * galaxy.nu_ISRF[iinu];
      IR_integral_luminosity[inu] +=  IR_luminosity_spectrum[iinu] * galaxy.nu_ISRF[iinu];

     opt_integral_luminosity2[inu] += opt_luminosity_spectrum2[iinu] ;
      IR_integral_luminosity2[inu] +=  IR_luminosity_spectrum2[iinu] ;

    }
    opt_integral_luminosity *= log(galaxy.nu_ISRF[1]/galaxy.nu_ISRF[0]);
     IR_integral_luminosity *= log(galaxy.nu_ISRF[1]/galaxy.nu_ISRF[0]);

    opt_integral_luminosity2*= log(galaxy.nu_ISRF[1]/galaxy.nu_ISRF[0]);
     IR_integral_luminosity2*= log(galaxy.nu_ISRF[1]/galaxy.nu_ISRF[0]);


 /////////////////////////////////////////////////////////////////////////////////////////////////
  // Porter ISRF
  /////////////////////////////////////////////////////////////////////////////////////////////////

  cout<<"galaxy.ISRF:"<<endl;

   int icomp,ir,iz,ix,iy,inu;
   //for(icomp=0;icomp<=2;icomp++) {   cout<<"ISRF component "<<icomp<<endl; galaxy.ISRF[icomp].print();}


   // at present the Galactic position for the ISRF is ad hoc

   
   double r_isrf=8.5; // solar position           AWS20100104
   ir =   r_isrf/galaxy.dr ;                    //AWS20100104
   if (ir>galaxy.n_rgrid-1) ir=galaxy.n_rgrid-1;//AWS20100104   

   ix=int(ir/sqrt(2.)+.1);
   iy=ix;
   
   iz=galaxy.n_zgrid/2 + 1; //AWS20100104
   cout<<"plot_luminosity: Porter ISRF:  ir="<<ir <<" ix="<<ix <<" iy="<<iy <<" iz="<<iz; //AWS20100114
   if(galdef.n_spatial_dimensions==2)   cout<<" r="<<galaxy.r[ir]                          <<" z="<<galaxy.z[iz]<<endl;
   if(galdef.n_spatial_dimensions==3)   cout<<" x="<<galaxy.x[ix]  <<" y="<<galaxy.y[iy]   <<" z="<<galaxy.z[iz]<<endl;
   cout<<"galaxy.ISRF[0].n_pgrid="<< galaxy.ISRF[0].n_pgrid  <<endl;



  for(icomp=0;icomp<=2;icomp++)
   {
    cout<<"ISRF component "<<icomp<<endl;

    for(inu=0; inu<galaxy.ISRF[0].n_pgrid ;inu++)
      {
      cout<<"plot_luminosity: galaxy.nu_ISRF[inu]=" <<galaxy.nu_ISRF[inu]<<" galaxy.ISRF  (eV cm-3)  = ";
      if(galdef.n_spatial_dimensions==2)   cout<< galaxy.ISRF[icomp].d2[ir]    [iz].s[inu]<<endl;
      if(galdef.n_spatial_dimensions==3)   cout<< galaxy.ISRF[icomp].d3[ix][iy][iz].s[inu]<<endl;
      }
    cout<<endl<<endl;
    }


   if(galdef.n_spatial_dimensions==2)   cout<<" r="<<galaxy.r[ir]                          <<" z="<<galaxy.z[iz]<<endl;
   if(galdef.n_spatial_dimensions==3)   cout<< "x="<<galaxy.x[ix]  <<" y="<<galaxy.y[iy]   <<" z="<<galaxy.z[iz]<<endl;


    for(inu=0; inu<galaxy.ISRF[0].n_pgrid ;inu++)
    {
     if(galdef.n_spatial_dimensions==2) 
        cout<< "plot_luminosity ISRF: nu= " <<                galaxy.nu_ISRF[inu]
            <<"  opt=  "<< galaxy.ISRF[0].d2[ir][iz].s[inu]
            <<"  IR= "   <<galaxy.ISRF[1].d2[ir][iz].s[inu]
            <<"  CMB="   <<galaxy.ISRF[2].d2[ir][iz].s[inu]
            <<" eV cm-3"<<endl;

     if(galdef.n_spatial_dimensions==3) 
        cout<< "plot_luminosity ISRF: nu= " <<                galaxy.nu_ISRF[inu]
            <<"  opt=  "<< galaxy.ISRF[0].d3[ix][iy][iz].s[inu]
            <<"  IR= "   <<galaxy.ISRF[1].d3[ix][iy][iz].s[inu]
            <<"  CMB="   <<galaxy.ISRF[2].d3[ix][iy][iz].s[inu]
            <<" eV cm-3"<<endl;
}

   //////////////////////////////////////////////////////////////////

    
    // -- cosmic rays

    cout<<"plot_luminosity: n_species="<<n_species<<" gcr[0].n_pgrid="<< gcr[0].n_pgrid <<endl;
    ir=5;
    iz=galaxy.n_zgrid/2;

    // see galprop create_transport_arrays for procedure below
   double spec_shape;
   double g_0=0., rigid_br0=0., rigid_br=galdef.nuc_rigid_br,g_1=galdef.nuc_g_1, g_2=galdef.nuc_g_2;
   Particle particle;

    // read_gcr.cc keeps *E^2 as in FITS file

                gcr_lepton_luminosity_spectrum3=0.; //AWS20100319 since primary and secondary e- have same Z,A, initialize here to avoid double counting
      gcr_primary_electron_luminosity_spectrum3=0.;
    gcr_secondary_electron_luminosity_spectrum3=0.;
      gcr_primary_positron_luminosity_spectrum3=0.;
    gcr_secondary_positron_luminosity_spectrum3=0.;



  for(int i_species=0;i_species<n_species;i_species++)
   {

     //---------- protons

    if(  gcr[i_species].Z==1 &&gcr[i_species].A==1)
    {
      cout<<"plot_luminosity: i_species= "<<i_species<<" Z,A:"<<gcr[i_species].Z<<","<<gcr[i_species].A <<endl;
      for(int ip=0;ip<gcr[0].n_pgrid;ip++) gcr_proton_luminosity_spectrum[ip]=gcr[i_species].cr_density.d2[ir][iz].s[ip] ;
                                 
      rigid_br=galdef.nuc_rigid_br;g_1=galdef.nuc_g_1;  g_2=galdef.nuc_g_2;
      particle=gcr[i_species]; 
     
     for(int ip=0;ip<gcr[0].n_pgrid;ip++)
     {
          if(particle.rigidity[ip]< rigid_br0)                                 
               spec_shape =pow(particle.rigidity[ip]/rigid_br0,-g_0) *pow(rigid_br0/rigid_br,-g_1);
            if(rigid_br0<= particle.rigidity[ip] && particle.rigidity[ip]< rigid_br)
               spec_shape =pow(particle.rigidity[ip]/rigid_br, -g_1);
            if(rigid_br <= particle.rigidity[ip])
               spec_shape =pow(particle.rigidity[ip]/rigid_br, -g_2);

	    gcr_proton_luminosity_spectrum2[ip]=spec_shape*pow(particle.Ekin[ip],2);
     }


     read_gcr_source_functions(particle);//AWS20100312

     gcr_proton_luminosity_spectrum3=0.; //AWS20100319

   if(galaxy.n_spatial_dimensions==2)
   {
    for(int ip=0;ip<gcr[0].n_pgrid;ip++)
    for(int ir=0;ir<irmax         ;ir++)
    for(int iz=0;iz<galaxy.n_zgrid;iz++)
    {

      double volume=pi * (pow(galaxy.r[ir+1],2.0) - pow(galaxy.r[ir],2.0)) * galaxy.dz ; // not exact of course since emissivity is at r[ir]. volume in kpc^3
      volume *= pow(kpc2cm,3.0); // from constants.h   volume now in cm^3

      gcr_proton_luminosity_spectrum3[ip]+=particle.primary_source_function.d2[ir][iz].s[ip]  * volume / particle.beta[ip] * pow(particle.Ekin[ip],2);
     
    }//ir iz
   }//if

   gcr_proton_luminosity_spectrum3*= 4.0*Pi/c; // see galprop convention for source functions

    } //A==1

    
  //------------ Helium 


   if(  gcr[i_species].Z==2 &&gcr[i_species].A==4)
    {
      cout<<"plot_luminosity: i_species= "<<i_species<<" Z,A:"<<gcr[i_species].Z<<","<<gcr[i_species].A <<endl;
      for(int ip=0;ip<gcr[0].n_pgrid;ip++) gcr_helium_luminosity_spectrum[ip]=gcr[i_species].cr_density.d2[ir][iz].s[ip] * 4.;  //NB 4 nucleons

      rigid_br=galdef.nuc_rigid_br;g_1=galdef.nuc_g_1;  g_2=galdef.nuc_g_2;
      particle=gcr[i_species]; 
     
     for(int ip=0;ip<gcr[0].n_pgrid;ip++)
     {
          if(particle.rigidity[ip]< rigid_br0)                                 
               spec_shape =pow(particle.rigidity[ip]/rigid_br0,-g_0) *pow(rigid_br0/rigid_br,-g_1);
            if(rigid_br0<= particle.rigidity[ip] && particle.rigidity[ip]< rigid_br)
               spec_shape =pow(particle.rigidity[ip]/rigid_br, -g_1);
            if(rigid_br <= particle.rigidity[ip])
               spec_shape =pow(particle.rigidity[ip]/rigid_br, -g_2);

	    gcr_helium_luminosity_spectrum2[ip]=spec_shape*pow(particle.Ekin[ip],2);;
     }

     read_gcr_source_functions(particle);//AWS20100312


     gcr_helium_luminosity_spectrum3=0.; //AWS20100319

   if(galaxy.n_spatial_dimensions==2)
   {
    for(int ip=0;ip<gcr[0].n_pgrid;ip++)
    for(int ir=0;ir<irmax         ;ir++)
    for(int iz=0;iz<galaxy.n_zgrid;iz++)
    {

      double volume=pi * (pow(galaxy.r[ir+1],2.0) - pow(galaxy.r[ir],2.0)) * galaxy.dz ; // not exact of course since emissivity is at r[ir]. volume in kpc^3
      volume *= pow(kpc2cm,3.0); // from constants.h   volume now in cm^3

      gcr_helium_luminosity_spectrum3[ip]+=particle.primary_source_function.d2[ir][iz].s[ip]  * volume / particle.beta[ip] * pow(particle.Ekin[ip],2) *4.;// 4 nucleons
     
    }//ir iz
   }//if

   gcr_helium_luminosity_spectrum3*= 4.0*Pi/c; // see galprop convention for source functions

    }// A==4

   

   //-------------- electrons



   if(  gcr[i_species].Z==-1&&gcr[i_species].A==0) // NB includes secondary electrons
    {
      cout<<"plot_luminosity: i_species= "<<i_species<<" Z,A:"<<gcr[i_species].Z<<","<<gcr[i_species].A <<endl;
      for(int ip=0;ip<gcr[0].n_pgrid;ip++) gcr_lepton_luminosity_spectrum[ip]+=gcr[i_species].cr_density.d2[ir][iz].s[ip];
      
     rigid_br0=galdef.electron_rigid_br0; rigid_br=galdef.electron_rigid_br;
     g_0=galdef.electron_g_0;   g_1=galdef.electron_g_1;  g_2=galdef.electron_g_2;
      particle=gcr[i_species]; 
     
     for(int ip=0;ip<gcr[0].n_pgrid;ip++)
     {
          if(particle.rigidity[ip]< rigid_br0)                                 
               spec_shape =pow(particle.rigidity[ip]/rigid_br0,-g_0) *pow(rigid_br0/rigid_br,-g_1);
            if(rigid_br0<= particle.rigidity[ip] && particle.rigidity[ip]< rigid_br)
               spec_shape =pow(particle.rigidity[ip]/rigid_br, -g_1);
            if(rigid_br <= particle.rigidity[ip])
               spec_shape =pow(particle.rigidity[ip]/rigid_br, -g_2);

	    if(strcmp(particle.name,"primary_electrons")==0) // primary electrons only by this method. obsolete now that source functions are used for spectrum3
	    gcr_lepton_luminosity_spectrum2[ip]=spec_shape*pow(particle.Ekin[ip],2);

     }
      
     read_gcr_source_functions(particle);// primary and secondary electrons separate here//AWS20100312 

 

   if(galaxy.n_spatial_dimensions==2)
   {
    for(int ip=0;ip<gcr[0].n_pgrid;ip++)
    for(int ir=0;ir<irmax         ;ir++)
    for(int iz=0;iz<galaxy.n_zgrid;iz++)
    {

      double volume=pi * (pow(galaxy.r[ir+1],2.0) - pow(galaxy.r[ir],2.0)) * galaxy.dz ; // not exact of course since emissivity is at r[ir]. volume in kpc^3
      volume *= pow(kpc2cm,3.0); // from constants.h   volume now in cm^3

      if(strcmp(particle.name,"primary_electrons")==0)                                                                                                              //AWS20100901
      gcr_lepton_luminosity_spectrum3[ip]+=particle.primary_source_function  .d2[ir][iz].s[ip]  * volume / particle.beta[ip] * pow(particle.Ekin[ip],2) *  4.0*Pi/c;

      if(strcmp(particle.name,"secondary_electrons")==0)                                                                                                            //AWS20100901
      gcr_lepton_luminosity_spectrum3[ip]+=particle.secondary_source_function.d2[ir][iz].s[ip]  * volume / particle.beta[ip] * pow(particle.Ekin[ip],2) *  4.0*Pi/c;//AWS20100901

      
      if(strcmp(particle.name,"primary_electrons")==0)
      gcr_primary_electron_luminosity_spectrum3[ip]+=particle.primary_source_function.d2[ir][iz].s[ip]  * volume / particle.beta[ip] * pow(particle.Ekin[ip],2) *  4.0*Pi/c;

      if(strcmp(particle.name,"secondary_electrons")==0)
      gcr_secondary_electron_luminosity_spectrum3[ip]+=particle.secondary_source_function.d2[ir][iz].s[ip]  * volume / particle.beta[ip] * pow(particle.Ekin[ip],2) *  4.0*Pi/c;

     
    }//ir iz
   }//if

   

    }//A==0 Z=-1



   //----------- positrons

   if(  gcr[i_species].Z==+1&&gcr[i_species].A==0) //secondary positrons AWS20100312 
    {
      cout<<"plot_luminosity: i_species= "<<i_species<<" Z,A:"<<gcr[i_species].Z<<","<<gcr[i_species].A <<endl;
      for(int ip=0;ip<gcr[0].n_pgrid;ip++) gcr_lepton_luminosity_spectrum[ip]+=gcr[i_species].cr_density.d2[ir][iz].s[ip];
      
     rigid_br0=galdef.electron_rigid_br0; rigid_br=galdef.electron_rigid_br;
     g_0=galdef.electron_g_0;   g_1=galdef.electron_g_1;  g_2=galdef.electron_g_2;
      particle=gcr[i_species]; 
     
     for(int ip=0;ip<gcr[0].n_pgrid;ip++)
     {
          if(particle.rigidity[ip]< rigid_br0)                                 
               spec_shape =pow(particle.rigidity[ip]/rigid_br0,-g_0) *pow(rigid_br0/rigid_br,-g_1);
            if(rigid_br0<= particle.rigidity[ip] && particle.rigidity[ip]< rigid_br)
               spec_shape =pow(particle.rigidity[ip]/rigid_br, -g_1);
            if(rigid_br <= particle.rigidity[ip])
               spec_shape =pow(particle.rigidity[ip]/rigid_br, -g_2);

	    gcr_lepton_luminosity_spectrum2[ip]=spec_shape*pow(particle.Ekin[ip],2);

     }
      
     read_gcr_source_functions(particle);


   gcr_secondary_positron_luminosity_spectrum3=0.;

   if(galaxy.n_spatial_dimensions==2)
   {
    for(int ip=0;ip<gcr[0].n_pgrid;ip++)
    for(int ir=0;ir<irmax         ;ir++)
    for(int iz=0;iz<galaxy.n_zgrid;iz++)
    {

      double volume=pi * (pow(galaxy.r[ir+1],2.0) - pow(galaxy.r[ir],2.0)) * galaxy.dz ; // not exact of course since emissivity is at r[ir]. volume in kpc^3
      volume *= pow(kpc2cm,3.0); // from constants.h   volume now in cm^3

      gcr_secondary_positron_luminosity_spectrum3[ip]+=particle.secondary_source_function.d2[ir][iz].s[ip]  * volume / particle.beta[ip] * pow(particle.Ekin[ip],2) *  4.0*Pi/c;

                  gcr_lepton_luminosity_spectrum3[ip]+=particle.secondary_source_function.d2[ir][iz].s[ip]  * volume / particle.beta[ip] * pow(particle.Ekin[ip],2) *  4.0*Pi/c;
     
    }//ir iz
   }//if



    }//A==0 Z=+1


   } //i_species
   

 

   gcr_proton_integral_luminosity=0.;
   gcr_helium_integral_luminosity=0.;
   gcr_lepton_integral_luminosity=0.;

   gcr_proton_integral_luminosity2=0.;
   gcr_helium_integral_luminosity2=0.;
   gcr_lepton_integral_luminosity2=0.;

               gcr_proton_integral_luminosity3=0.;
               gcr_helium_integral_luminosity3=0.;
               gcr_lepton_integral_luminosity3=0.;
     gcr_primary_electron_integral_luminosity3=0.;
     gcr_primary_positron_integral_luminosity3=0.;
   gcr_secondary_electron_integral_luminosity3=0.;
   gcr_secondary_positron_integral_luminosity3=0.;

   for(int ip =0; ip <gcr[0].n_pgrid;ip++)
   for(int ipp=ip;ipp<gcr[0].n_pgrid;ipp++)
   {
    gcr_proton_integral_luminosity[ip]+= gcr_proton_luminosity_spectrum[ipp];
    gcr_helium_integral_luminosity[ip]+= gcr_helium_luminosity_spectrum[ipp];
    gcr_lepton_integral_luminosity[ip]+= gcr_lepton_luminosity_spectrum[ipp];

    gcr_proton_integral_luminosity2[ip]+= gcr_proton_luminosity_spectrum2[ipp];
    gcr_helium_integral_luminosity2[ip]+= gcr_helium_luminosity_spectrum2[ipp];
    gcr_lepton_integral_luminosity2[ip]+= gcr_lepton_luminosity_spectrum2[ipp];

                gcr_proton_integral_luminosity3[ip]+=             gcr_proton_luminosity_spectrum3[ipp];
                gcr_helium_integral_luminosity3[ip]+=             gcr_helium_luminosity_spectrum3[ipp];
                gcr_lepton_integral_luminosity3[ip]+=             gcr_lepton_luminosity_spectrum3[ipp];
      gcr_primary_electron_integral_luminosity3[ip]+=   gcr_primary_electron_luminosity_spectrum3[ipp];
      gcr_primary_positron_integral_luminosity3[ip]+=   gcr_primary_positron_luminosity_spectrum3[ipp];
    gcr_secondary_electron_integral_luminosity3[ip]+= gcr_secondary_electron_luminosity_spectrum3[ipp];
    gcr_secondary_positron_integral_luminosity3[ip]+= gcr_secondary_positron_luminosity_spectrum3[ipp];
   }

   gcr_proton_integral_luminosity*=log(gcr[0].Ekin[1]/gcr[0].Ekin[0]);   // all particles have same KE/nucleon grid
   gcr_helium_integral_luminosity*=log(gcr[0].Ekin[1]/gcr[0].Ekin[0]); 
   gcr_lepton_integral_luminosity*=log(gcr[0].Ekin[1]/gcr[0].Ekin[0]); 

   gcr_proton_integral_luminosity2*=log(gcr[0].Ekin[1]/gcr[0].Ekin[0]);  
   gcr_helium_integral_luminosity2*=log(gcr[0].Ekin[1]/gcr[0].Ekin[0]); 
   gcr_lepton_integral_luminosity2*=log(gcr[0].Ekin[1]/gcr[0].Ekin[0]); 

               gcr_proton_integral_luminosity3  *=log(gcr[0].Ekin[1]/gcr[0].Ekin[0]);  
               gcr_helium_integral_luminosity3  *=log(gcr[0].Ekin[1]/gcr[0].Ekin[0]); 
               gcr_lepton_integral_luminosity3  *=log(gcr[0].Ekin[1]/gcr[0].Ekin[0]); 
     gcr_primary_electron_integral_luminosity3  *=log(gcr[0].Ekin[1]/gcr[0].Ekin[0]); 
     gcr_primary_positron_integral_luminosity3  *=log(gcr[0].Ekin[1]/gcr[0].Ekin[0]); 
   gcr_secondary_electron_integral_luminosity3  *=log(gcr[0].Ekin[1]/gcr[0].Ekin[0]); 
   gcr_secondary_positron_integral_luminosity3  *=log(gcr[0].Ekin[1]/gcr[0].Ekin[0]); 

   // normalize to luminosity from galprop printout
   /*
 grep lumin logz10G4c5BS3
 CR_luminosity=8.25896e+40 erg s^-1
 CR_electron_luminosity=1.80029e+39 erg s^-1
 CR_positron_luminosity=5.68774e+41 positrons s^-1

    */
   double galprop_nuclei_luminosity = 8.26e40; // erg s-1
   double galprop_lepton_luminosity = 1.80e39; // erg s-1

   // gcr are in MeV so need luminosity   MeV s-1
   galprop_nuclei_luminosity/=MEV2ERG; // MeV s-1
   galprop_lepton_luminosity/=MEV2ERG; // MeV s-1


   // since at present we have only the p+He luminosity from galprop, assign the protons and helium accordingly
   double galprop_proton_luminosity=galprop_nuclei_luminosity*gcr_proton_integral_luminosity[0]/(gcr_proton_integral_luminosity[0]+gcr_helium_integral_luminosity[0]);
   double galprop_helium_luminosity=galprop_nuclei_luminosity*gcr_helium_integral_luminosity[0]/(gcr_proton_integral_luminosity[0]+gcr_helium_integral_luminosity[0]);



   gcr_proton_luminosity_spectrum*= galprop_proton_luminosity/gcr_proton_integral_luminosity[0] ;
   gcr_helium_luminosity_spectrum*= galprop_helium_luminosity/gcr_helium_integral_luminosity[0] ;
   gcr_lepton_luminosity_spectrum*= galprop_lepton_luminosity/gcr_lepton_integral_luminosity[0] ;


   
   gcr_proton_integral_luminosity *= galprop_proton_luminosity/gcr_proton_integral_luminosity[0] ;
   gcr_helium_integral_luminosity *= galprop_helium_luminosity/gcr_helium_integral_luminosity[0] ;
   gcr_lepton_integral_luminosity *= galprop_lepton_luminosity/gcr_lepton_integral_luminosity[0] ;

   gcr_proton_luminosity_spectrum2*= galprop_proton_luminosity/gcr_proton_integral_luminosity2[0] ;
   gcr_helium_luminosity_spectrum2*= galprop_helium_luminosity/gcr_helium_integral_luminosity2[0] ;
   gcr_lepton_luminosity_spectrum2*= galprop_lepton_luminosity/gcr_lepton_integral_luminosity2[0] ;

       
   gcr_proton_integral_luminosity2 *= galprop_proton_luminosity/gcr_proton_integral_luminosity2[0] ;
   gcr_helium_integral_luminosity2 *= galprop_helium_luminosity/gcr_helium_integral_luminosity2[0] ;
   gcr_lepton_integral_luminosity2 *= galprop_lepton_luminosity/gcr_lepton_integral_luminosity2[0] ;


   // 


   // ======================== print out the results =====================================

   txt_stream<<"Output from plot_luminosity"<<endl;

   // integral properties of Galaxy

  txt_stream<<"for luminosity: total atoms HI="<<nHI_total<<" H2="<<nH2_total<<" HII="<<nHII_total<<" total gas="<<nH_total<<endl;
  txt_stream<<"for luminosity: total Msun  HI="<<nHI_total*1.67e-24/2.0e33<<" H2="<<nH2_total*1.67e-24/2.0e33<<" HII="<<nHII_total*1.67e-24/2.0e33<<" total gas="<<nH_total*1.67e-24/2.0e33<<endl;

  txt_stream<<endl;

  double factor=1.67e-24/2.0e33*pow(kpc2cm,2)*1.0e-6; // atoms cm-2 to Msun/pc^2

  for(int ir=0; ir<galaxy.n_rgrid-1; ir++)
   txt_stream <<"for luminosity:z-column density. R="<<galaxy.r[ir]<<" kpc.  atoms cm^-2:  HI: "
         <<nHI_column_density [ir] <<" H2: "<<   nH2_column_density [ir]<< " HII: " <<    nHII_column_density [ir]
  	 <<"  /  Msun/pc^2:  HI: "
         <<nHI_column_density [ir]*factor <<" H2: "<<   nH2_column_density [ir]*factor<< " HII: " <<    nHII_column_density [ir]*factor
         <<endl;


  txt_stream<<"for luminosity: total volume = "<<volume_total<<" cm^3"<<endl;

  txt_stream<<endl<<" gamma-ray luminosity of Galaxy "<<endl<<endl;

  for(int ip=0;ip<galaxy.n_E_gammagrid;ip++)
  {
    int iz=galaxy.n_zgrid/2;
    cout<<"pi0-decay emissivity spectrum at z=0: "
        <<  "E="               <<galaxy.E_gamma[ip]<<" "; 
    for(int ir=0;ir<galaxy.n_rgrid;ir++)        cout   <<galaxy.pi0_decay_emiss    .d2[ir][iz].s[ip]<<" ";
     cout   << endl;
    }

  for(int ip=0;ip<galaxy.n_E_gammagrid;ip++)
    txt_stream<<"luminosity spectrum: "
        <<  "E="               <<galaxy.E_gamma[ip]<<" "  

        << " bremss: "<< gamma_bremss_luminosity_spectrum   [ip]<<" "
        << " pi0: "   << gamma_pi0_decay_luminosity_spectrum[ip]<<" "
        << " IC: "    << gamma_IC_luminosity_spectrum       [ip]<<" "
        << " total: " << gamma_total_luminosity_spectrum    [ip]<<" "

        << " integral luminosity <E erg s-1 :"
        << " bremss: "<< gamma_bremss_integraldown_luminosity   [ip]<<" "
        <<  " pi0: "  <<gamma_pi0_decay_integraldown_luminosity [ip]<<" "
        <<  " IC: "   <<gamma_IC_integraldown_luminosity        [ip]<<" "
        <<  " total: "<<gamma_total_integraldown_luminosity     [ip]<<" "

        << " integral luminosity >E erg s-1 :"
        << " bremss: "<< gamma_bremss_integral_luminosity   [ip]<<" "
        <<  " pi0: "  <<gamma_pi0_decay_integral_luminosity [ip]<<" "
        <<  " IC: "   <<gamma_IC_integral_luminosity        [ip]<<" "
        <<  " total: "<<gamma_total_integral_luminosity     [ip]<<" "

        << " integral photons s-1:"            
        << " bremss: "<< gamma_bremss_integral_photons      [ip]<<" "
        <<  " pi0: "  <<gamma_pi0_decay_integral_photons    [ip]<<" "
        <<  " IC: "   <<gamma_IC_integral_photons           [ip]<<" "
        <<  " total: "<<gamma_total_integral_photons        [ip]<<" "

        <<endl;







  txt_stream<<endl<<" synchrotron luminosity of Galaxy "<<endl<<endl;

     for(int inu=0;inu<galaxy.n_nu_synchgrid;inu++)
     {
       txt_stream<<"nu ="<<galaxy.nu_synch[inu]<<" synchrotron luminosity erg Hz-1 s-1: "<< synchrotron_luminosity_spectrum[inu]
           <<" integral luminosity erg s-1: "<< synchrotron_integral_luminosity[inu]
           <<endl;
     }



  txt_stream<<endl<<" optical and IR luminosity of Galaxy "<<endl<<endl;
  double solar_luminosity = 3.826e33;// erg s-1
  double Galactic_bolometric_luminosity = 3.30e10 * solar_luminosity; // Binney and Merrifield Galactic Astronomy page 614. 'Best estimate using Tully-Fisher'
         Galactic_bolometric_luminosity = 5.14e10 * solar_luminosity; // bolometric luminosity from Troy as input to his model. see mail 20100305
      
  //normalize to this, but not obvious how IR is included
  factor =  Galactic_bolometric_luminosity/( opt_integral_luminosity[0]+ IR_integral_luminosity[0]);
  txt_stream<< "plot_luminosity: factor normalizing to Galactic bolometric luminosity = "<<factor<<endl;


  opt_luminosity_spectrum*=factor;
   IR_luminosity_spectrum*=factor;
  opt_integral_luminosity*=factor;
   IR_integral_luminosity*=factor;

     for(int inu=0;inu<galaxy.ISRF[0].n_pgrid;inu++)
     {
       txt_stream<<" nu ="<<galaxy.nu_ISRF[inu]<<" luminosity estimate1 erg Hz-1 s-1 optical: "<< opt_luminosity_spectrum[inu]
                                        <<                                        " IR: "<<  IR_luminosity_spectrum[inu]
           <<" integral  luminosity erg s-1 optical: "<< opt_integral_luminosity[inu]
           <<                                  " IR: "<<  IR_integral_luminosity[inu]
           <<" integral  luminosity/Lsun    optical: "<< opt_integral_luminosity[inu]/solar_luminosity
           <<                                  " IR: "<<  IR_integral_luminosity[inu]/solar_luminosity
           <<endl;
     }


     for(int inu=0;inu<galaxy.ISRF[0].n_pgrid;inu++)
     {
       txt_stream<<" nu ="<<galaxy.nu_ISRF[inu]<<" luminosity estimate2 erg Hz-1 s-1 optical: "<< opt_luminosity_spectrum2[inu]
	   <<                                        " IR: "<<  IR_luminosity_spectrum2[inu]
           <<" integral  luminosity erg s-1 optical: "<< opt_integral_luminosity2[inu]
           <<                                  " IR: "<<  IR_integral_luminosity2[inu]
           <<" integral  luminosity/Lsun    optical: "<< opt_integral_luminosity2[inu]/solar_luminosity
           <<                                  " IR: "<<  IR_integral_luminosity2[inu]/solar_luminosity
           <<endl;
     }


     // cosmic rays
     // output in MeV

      txt_stream<<"plot_luminosity: cosmic rays gcr"<<endl;
     for(int ip=0;ip<gcr[0].n_pgrid;ip++)
        txt_stream<<"gcr Ekin/nucleon="         <<gcr[0].Ekin[ip]
           <<   " luminosity spectrum from propagated gcr MeV^2 MeV-1 s-1: "
           <<   " protons: "<< gcr_proton_luminosity_spectrum[ip]
           <<   " helium : "<< gcr_helium_luminosity_spectrum[ip]
           <<   " leptons: "<< gcr_lepton_luminosity_spectrum[ip]
           <<   " integral luminosity MeV s-1: "
           <<   " protons: "<< gcr_proton_integral_luminosity[ip]
           <<   " helium : "<< gcr_helium_integral_luminosity[ip]
           <<   " leptons: "<< gcr_lepton_integral_luminosity[ip]
	   <<endl;

     for(int ip=0;ip<gcr[0].n_pgrid;ip++)
        txt_stream<<"gcr Ekin/nucleon="          <<gcr[0].Ekin[ip]
           <<   " luminosity spectrum2 injection MeV^2 MeV-1 s-1: "
           <<   " protons: "<< gcr_proton_luminosity_spectrum2[ip]
           <<   " helium : "<< gcr_helium_luminosity_spectrum2[ip]
           <<   " primary electrons:"<< gcr_lepton_luminosity_spectrum2[ip]
           <<   " integral luminosity2 MeV s-1: "
           <<   " protons: "<< gcr_proton_integral_luminosity2[ip]
           <<   " helium : "<< gcr_helium_integral_luminosity2[ip]
           <<   " primary electrons :"<< gcr_lepton_integral_luminosity2[ip]
	   <<endl;


     for(int ip=0;ip<gcr[0].n_pgrid;ip++)
        txt_stream<<"gcr Ekin/nucleon="          <<gcr[0].Ekin[ip]
           <<   " luminosity spectrum3 injection MeV^2 MeV-1 s-1: "
           <<   " protons: "<< gcr_proton_luminosity_spectrum3[ip]
           <<   " helium : "<< gcr_helium_luminosity_spectrum3[ip]
           <<  "  primary electrons :"     << gcr_primary_electron_luminosity_spectrum3[ip]
          <<  "   secondary electrons :" << gcr_secondary_electron_luminosity_spectrum3[ip]
           <<  " positrons:"             << gcr_secondary_positron_luminosity_spectrum3[ip]
          <<  "  total leptons :"<< gcr_lepton_luminosity_spectrum3[ip]
           <<   " integral luminosity3 MeV s-1: "
           <<   " protons: "<< gcr_proton_integral_luminosity3[ip]
           <<   " helium : "<< gcr_helium_integral_luminosity3[ip]
           <<  "  primary electrons :"     <<  gcr_primary_electron_integral_luminosity3[ip]
          <<  "   secondary electrons :"  << gcr_secondary_electron_integral_luminosity3[ip]
          <<  " positrons:"               << gcr_secondary_positron_integral_luminosity3[ip]
           <<  "  total leptons :     "               << gcr_lepton_integral_luminosity3[ip]
	   <<endl;


     // output in erg
      txt_stream<<"plot_luminosity: cosmic rays gcr"<<endl;
     for(int ip=0;ip<gcr[0].n_pgrid;ip++)
        txt_stream<<"gcr Ekin/nucleon="         <<gcr[0].Ekin[ip]
           <<   " luminosity spectrum from propagated gcr erg s-1 : "            
           <<   " protons: "<< gcr_proton_luminosity_spectrum[ip]*MEV2ERG
           <<   " helium : "<< gcr_helium_luminosity_spectrum[ip]*MEV2ERG
           <<   " leptons: "<< gcr_lepton_luminosity_spectrum[ip]*MEV2ERG
           <<   " integral luminosity erg s-1: "
           <<   " protons: "<< gcr_proton_integral_luminosity[ip]*MEV2ERG
           <<   " helium : "<< gcr_helium_integral_luminosity[ip]*MEV2ERG
           <<   " leptons: "<< gcr_lepton_integral_luminosity[ip]*MEV2ERG
	   <<endl;

     for(int ip=0;ip<gcr[0].n_pgrid;ip++)
        txt_stream<<"gcr Ekin/nucleon="          <<gcr[0].Ekin[ip]
           <<   " luminosity spectrum2 injection erg s-1 : "        
           <<   " protons: "<< gcr_proton_luminosity_spectrum2[ip]          *MEV2ERG
           <<   " helium : "<< gcr_helium_luminosity_spectrum2[ip]          *MEV2ERG
           <<   " primary electrons:"<< gcr_lepton_luminosity_spectrum2[ip] *MEV2ERG
           <<   " integral luminosity2 erg s-1: "
           <<   " protons: "<< gcr_proton_integral_luminosity2[ip]          *MEV2ERG
           <<   " helium : "<< gcr_helium_integral_luminosity2[ip]          *MEV2ERG
           <<   " primary electrons :"<< gcr_lepton_integral_luminosity2[ip]*MEV2ERG
	   <<endl;


     for(int ip=0;ip<gcr[0].n_pgrid;ip++)
        txt_stream<<"gcr Ekin/nucleon="          <<gcr[0].Ekin[ip]
           <<   " luminosity spectrum3 injection erg s-1: "
           <<   " protons: "<< gcr_proton_luminosity_spectrum3[ip]                          *MEV2ERG
           <<   " helium : "<< gcr_helium_luminosity_spectrum3[ip]                          *MEV2ERG
           <<  "  primary electrons :"     << gcr_primary_electron_luminosity_spectrum3[ip] *MEV2ERG
          <<  "   secondary electrons :" << gcr_secondary_electron_luminosity_spectrum3[ip] *MEV2ERG
           <<  " positrons:"             << gcr_secondary_positron_luminosity_spectrum3[ip] *MEV2ERG
          <<  "  total leptons :"<< gcr_lepton_luminosity_spectrum3[ip]                     *MEV2ERG
           <<   " integral luminosity3 erg s-1: "
           <<   " protons: "<< gcr_proton_integral_luminosity3[ip]                          *MEV2ERG
           <<   " helium : "<< gcr_helium_integral_luminosity3[ip]                          *MEV2ERG
           <<  "  primary electrons :"     <<  gcr_primary_electron_integral_luminosity3[ip]*MEV2ERG
          <<  "   secondary electrons :"  << gcr_secondary_electron_integral_luminosity3[ip]*MEV2ERG
          <<  " positrons:"               << gcr_secondary_positron_integral_luminosity3[ip]*MEV2ERG
           <<  "  total leptons :     "               << gcr_lepton_integral_luminosity3[ip]*MEV2ERG
	   <<endl;




  ////////////////////////// tables for paper


  int width = 9;
  txt_stream.width   (width); // mininum width to get aligned columns
  txt_stream.fill    (' ');// fill character
  txt_stream.precision(2); //number of digits after decimal point
  txt_stream.setf(ios::scientific);

  int n_columns;



  txt_stream<<endl<<"synchrotron luminosity spectrum table start: "<<endl;
  txt_stream<<endl<<"GALPROP model ID: "<<galdef.galdef_ID<<endl;
  txt_stream<<endl<<"++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ "<<endl;
  width = 9;
  n_columns=3;
  for(int i_column=1;i_column<=n_columns;i_column++){ txt_stream.width   (width-4); txt_stream<<i_column<<"     ";}; txt_stream<<endl;

  txt_stream.width   (width); // mininum width to get aligned columns
  txt_stream.fill    (' ');// fill character
  txt_stream.precision(2); //number of digits after decimal point
  txt_stream.setf(ios::scientific);

    for(int inu=0;inu<galaxy.n_nu_synchgrid;inu++)
    {
  
      //     txt_stream              <<"luminosity spectrum table: ";

     txt_stream.width   (width); // mininum width to get aligned columns. Have to apply before each field output, else defaults back.
     txt_stream      <<           galaxy.nu_synch[inu]                                       <<" ";
  
     txt_stream.width   (width);    txt_stream     << synchrotron_luminosity_spectrum[inu] <<" ";
     txt_stream.width   (width);    txt_stream     << synchrotron_integral_luminosity[inu] <<" ";
     txt_stream    <<endl;
    }
  txt_stream<<endl<<"++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ "<<endl;
  txt_stream<<endl<<"synchrotron luminosity spectrum table end "<<endl;

  txt_stream<<endl<<"gamma luminosity spectrum table start: "<<endl;
  txt_stream<<endl<<"GALPROP model ID: "<<galdef.galdef_ID<<endl;
  txt_stream<<endl<<"++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ "<<endl;
  width=9;
  n_columns=17;
  for(int i_column=1;i_column<=n_columns;i_column++){ txt_stream.width   (width-4); txt_stream<<i_column<<"     ";}; txt_stream<<endl;

  for(int ip=0;ip<galaxy.n_E_gammagrid;ip++)
    {
  
      //     txt_stream              <<"luminosity spectrum table: ";

     txt_stream.width   (width); // mininum width to get aligned columns. Have to apply before each field output, else defaults back.
     txt_stream      <<galaxy.E_gamma[ip]                                                    <<" ";
  

     txt_stream.width   (width);    txt_stream        << gamma_pi0_decay_luminosity_spectrum[ip]<<" ";
     txt_stream.width   (width);    txt_stream        << gamma_IC_luminosity_spectrum       [ip]<<" ";
     txt_stream.width   (width);    txt_stream        << gamma_bremss_luminosity_spectrum   [ip]<<" ";
     txt_stream.width   (width);    txt_stream        << gamma_total_luminosity_spectrum    [ip]<<" ";

        

     txt_stream.width   (width);    txt_stream        << gamma_pi0_decay_integraldown_luminosity [ip]<<" ";
     txt_stream.width   (width);    txt_stream        << gamma_IC_integraldown_luminosity        [ip]<<" ";
     txt_stream.width   (width);    txt_stream        << gamma_bremss_integraldown_luminosity    [ip]<<" ";
     txt_stream.width   (width);    txt_stream        << gamma_total_integraldown_luminosity     [ip]<<" ";

       

     txt_stream.width   (width);    txt_stream        << gamma_pi0_decay_integral_luminosity [ip]<<" ";
     txt_stream.width   (width);    txt_stream        << gamma_IC_integral_luminosity        [ip]<<" ";
     txt_stream.width   (width);    txt_stream        << gamma_bremss_integral_luminosity    [ip]<<" ";
     txt_stream.width   (width);    txt_stream        << gamma_total_integral_luminosity     [ip]<<" ";

                 
 
     txt_stream.width   (width);    txt_stream        << gamma_pi0_decay_integral_photons    [ip]<<" ";
     txt_stream.width   (width);    txt_stream        << gamma_IC_integral_photons           [ip]<<" ";
     txt_stream.width   (width);    txt_stream        << gamma_bremss_integral_photons       [ip]<<" ";
     txt_stream.width   (width);    txt_stream        << gamma_total_integral_photons        [ip]<<" ";

     txt_stream    <<endl;
    }
  txt_stream<<endl<<"++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ "<<endl;
  txt_stream<<endl<<"gamma luminosity spectrum table end "<<endl;


  txt_stream<<endl<<"IR + optical luminosity spectrum table start: "<<endl;
  txt_stream<<endl<<"GALPROP model ID: "<<galdef.galdef_ID<<endl;
  txt_stream<<endl<<"++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ "<<endl;
  width = 9;
  n_columns=5;
  for(int i_column=1;i_column<=n_columns;i_column++){ txt_stream.width   (width-4); txt_stream<<i_column<<"     ";}; txt_stream<<endl;
  txt_stream.width   (width); // mininum width to get aligned columns
  txt_stream.fill    (' ');// fill character
  txt_stream.precision(2); //number of digits after decimal point
  txt_stream.setf(ios::scientific);

  for(int inu=0;inu<galaxy.ISRF[0].n_pgrid;inu++)
    {
  
      //     txt_stream              <<"luminosity spectrum table: ";

     txt_stream.width   (width); // mininum width to get aligned columns. Have to apply before each field output, else defaults back.
     txt_stream      <<   galaxy.nu_ISRF[inu]                                      <<" ";
   
     txt_stream.width   (width);    txt_stream     <<       IR_luminosity_spectrum2[inu] <<" ";
     txt_stream.width   (width);    txt_stream     <<      opt_luminosity_spectrum2[inu]  <<" ";
     txt_stream.width   (width);    txt_stream     <<       IR_integral_luminosity2[inu] <<" ";
     txt_stream.width   (width);    txt_stream     <<      opt_integral_luminosity2[inu] <<" ";
    
     txt_stream    <<endl;
    }
  txt_stream<<endl<<"++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ "<<endl;
  txt_stream<<endl<<"IR + optical luminosity spectrum table end "<<endl;




  txt_stream<<endl<<"Cosmic-ray luminosity spectrum table start: "<<endl;
  txt_stream<<endl<<"GALPROP model ID: "<<galdef.galdef_ID<<endl;
  txt_stream<<endl<<"++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ "<<endl;
  width = 9;
  n_columns=13;
  for(int i_column=1;i_column<=n_columns;i_column++){ txt_stream.width   (width-4); txt_stream<<i_column<<"     ";}; txt_stream<<endl;
  txt_stream.width   (width); // mininum width to get aligned columns
  txt_stream.fill    (' ');// fill character
  txt_stream.precision(2); //number of digits after decimal point
  txt_stream.setf(ios::scientific);

      for(int ip=0;ip<gcr[0].n_pgrid;ip++)
    {
  
      //     txt_stream              <<"luminosity spectrum table: ";

     txt_stream.width   (width); // mininum width to get aligned columns. Have to apply before each field output, else defaults back.
     txt_stream      <<        gcr[0].Ekin[ip]                           <<" ";
   
     txt_stream.width   (width);    txt_stream     <<  gcr_proton_luminosity_spectrum3[ip]                          *MEV2ERG       <<" ";
     txt_stream.width   (width);    txt_stream     <<  gcr_helium_luminosity_spectrum3[ip]                          *MEV2ERG     <<" ";
     txt_stream.width   (width);    txt_stream     <<  gcr_primary_electron_luminosity_spectrum3[ip]                *MEV2ERG      <<" ";
     txt_stream.width   (width);    txt_stream     <<  gcr_secondary_electron_luminosity_spectrum3[ip]              *MEV2ERG   <<" ";
     txt_stream.width   (width);    txt_stream     <<  gcr_secondary_positron_luminosity_spectrum3[ip]              *MEV2ERG   <<" ";
     txt_stream.width   (width);    txt_stream     <<  gcr_lepton_luminosity_spectrum3[ip]                          *MEV2ERG   <<" ";
    
     txt_stream.width   (width);    txt_stream     <<  gcr_proton_integral_luminosity3[ip]                          *MEV2ERG       <<" ";
     txt_stream.width   (width);    txt_stream     <<  gcr_helium_integral_luminosity3[ip]                          *MEV2ERG     <<" ";
     txt_stream.width   (width);    txt_stream     <<  gcr_primary_electron_integral_luminosity3[ip]                *MEV2ERG      <<" ";
     txt_stream.width   (width);    txt_stream     <<  gcr_secondary_electron_integral_luminosity3[ip]              *MEV2ERG   <<" ";
     txt_stream.width   (width);    txt_stream     <<  gcr_secondary_positron_integral_luminosity3[ip]              *MEV2ERG   <<" ";
     txt_stream.width   (width);    txt_stream     <<  gcr_lepton_integral_luminosity3[ip]                          *MEV2ERG   <<" ";
     txt_stream    <<endl;
    }
  txt_stream<<endl<<"++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ "<<endl;
  txt_stream<<endl<<"Cosmic-ray luminosity spectrum table end "<<endl;


 ///////////////////////////////////////////////// do the plotting
  





  TText  *text;
  TLatex *latex;                               
  TH1F   *h;                                       
  TGraph *spectrum;

  char name[100],canvastitle[100], workstring1[100],workstring2[100];
  char  psfile[400];
  char giffile[400];
  char txtfile[400];                               
  char iosfile[400];                               
 
  int ncolors=1; int *colors=0;
  gStyle->SetPalette(ncolors,colors);
 
  // define line styles for SetLineStyle (only 1-4 predefined at root 5.08)       
  gStyle->SetLineStyleString(1," ");
  gStyle->SetLineStyleString(2,"12 12");
  gStyle->SetLineStyleString(3,"4 8");
  gStyle->SetLineStyleString(4,"12 16 4 16");
  gStyle->SetLineStyleString(5,"20 12 4 12");
  gStyle->SetLineStyleString(6,"20 12 4 12 4 12 4 12");
  gStyle->SetLineStyleString(7,"20 20");
  gStyle->SetLineStyleString(8,"20 12 4 12 4 12");
  gStyle->SetLineStyleString(9,"80 20");
  gStyle->SetLineStyleString(10,"80 40 4 40");



  // names must be different or canvas disappears
  
  strcpy(canvastitle,"galdef ID ");
  strcat(canvastitle,galdef.galdef_ID);

  //====== see HowTo Style: and do before creating canvas
   TCanvas *c1;    

  TStyle *plain  = new TStyle("Plain","Plain Style (no colors/fill areas)"); 
    
  plain->SetCanvasBorderMode(0);
  plain->SetPadBorderMode(0);
  plain->SetPadColor(0);
  plain->SetCanvasColor(0);
  plain->SetTitleColor(1,"X"); //AWS20080125  was (0), worked in root 4.08, changed to 1 for 5.08  Default options are (1, "X")
  plain->SetTitleColor(1,"Y"); //AWS20080125
  plain->SetStatColor(0);
  gROOT->SetStyle("Plain");


  //-----------  MeV units
  double luminosity_min ;//AWS20100609
  double luminosity_max ;//AWS20100609
  int cosmic_ray_mode=3; // 0=none 1=from propagated gcr, 2=injection 3=source functions     4=all AWS20100609

  for (int plot_ISRF=0; plot_ISRF<=2; plot_ISRF++) //AWS20100609
  {
  
  if(plot_ISRF==0)strcpy(name,"luminosity_MeV_without_ISRF");// to identify it: name is not plotted  AWS20100609
  if(plot_ISRF==1)strcpy(name,"luminosity_MeV_with_ISRF"   );// to identify it: name is not plotted  AWS20100609
  if(plot_ISRF==2)strcpy(name,"luminosity_MeV_with_ISRF_scaled"   );// to identify it: name is not plotted  AWS20100610

  c1=new TCanvas(name,canvastitle,300,150, 600, 600);

  //  c1->SetLogx(); clearer to take log10
  c1->SetLogy();
  
  if(galplotdef.gamma_spectrum_Emin<=0.0)
    galplotdef.gamma_spectrum_Emin=galaxy.E_gamma_min;
  if(galplotdef.gamma_spectrum_Emax<=0.0)
    galplotdef.gamma_spectrum_Emax=galaxy.E_gamma_max;
  
  //----------------------------------------- axes

                    luminosity_min = 1e42;//AWS20100609
   if(plot_ISRF==0) luminosity_max = 1e46;//AWS20100609 no   ISRF
   if(plot_ISRF==1) luminosity_max = 1e50;//AWS20100609 with ISRF normal scaling
   if(plot_ISRF==2) luminosity_max = 1e46;//AWS20100610 with ISRF scaled down

  h=c1->DrawFrame(log10(galplotdef.gamma_spectrum_Emin), luminosity_min,  
		  log10(galplotdef.gamma_spectrum_Emax),luminosity_max); 



  
      h->SetXTitle("log (Energy, MeV)");
  //  h->GetXaxis()->SetTitle("here is a very long title to test whether it appears                      ");
      h->SetTitleOffset(1.1, "X");//+ve up  was 1.1 , modify for root 5.08
      h->SetTitleSize(0.035, "X");
      
  h->SetLabelOffset(0.00, "X");//+ve up 
  h->SetLabelSize(0.035,  "X");
  

  h->SetYTitle("Galactic luminosity, MeV^{2} s^{-1} MeV^{-1}");  
  h->SetTitleOffset(1.3,  "Y");//+ve left
  h->SetTitleSize(0.035,  "Y");
  h->SetLabelOffset(0.00, "Y");//+ve left
  h->SetLabelSize(0.035,  "Y");
     


  //----------------------------------------- text
  text=new TText();
  text->SetTextFont(62);
  text->SetTextColor(kBlack);
  text->SetTextSize(0.034 );                                             
  text->SetTextAlign(12);
  
  
    if(galplotdef.verbose!=-1003)// selectable debug: no title or l,b ranges                      
    {
      text->DrawTextNDC(.10 ,.93 ,canvastitle);// NDC=normalized coord system   
      text->SetTextSize(0.022 );                                              
      //     text->DrawTextNDC(.58 ,.88 ,workstring1);// NDC=normalized coord system   
      //     text->DrawTextNDC(.58 ,.86 ,workstring2);// NDC=normalized coord system   
    }
  




    spectrum = new TGraph(galaxy.n_E_gammagrid);

    for (int ip = 0; ip < galaxy.n_E_gammagrid; ip++)
    {
      //      cout<<" setting E="<< galaxy.E_gamma[ip]<<" luminosity="<<gamma_bremss_luminosity_spectrum[ip]<<endl;
      spectrum->SetPoint(ip, log10(galaxy.E_gamma[ip]), gamma_bremss_luminosity_spectrum[ip] );
    }

    //Gtypes.h:enum EColor { kWhite, kBlack, kRed, kGreen, kBlue, kYellow, kMagenta, kCyan };
 
    spectrum->SetLineColor(kCyan);
    spectrum->SetLineWidth(2);
    spectrum->SetLineStyle(1);//1=solid 2=dash 3=dot 4=dash-dot 
    spectrum->Draw("L");


   spectrum = new TGraph(galaxy.n_E_gammagrid);

    for (int ip = 0; ip < galaxy.n_E_gammagrid; ip++)
    {
      //      cout<<" setting E="<< galaxy.E_gamma[ip]<<" luminosity="<<gamma_pi0_decay_luminosity_spectrum[ip]<<endl;
      spectrum->SetPoint(ip, log10(galaxy.E_gamma[ip]), gamma_pi0_decay_luminosity_spectrum[ip] );
    }

    //Gtypes.h:enum EColor { kWhite, kBlack, kRed, kGreen, kBlue, kYellow, kMagenta, kCyan };
 
    spectrum->SetLineColor(kRed);
    spectrum->SetLineWidth(2);
    spectrum->SetLineStyle(1);//1=solid 2=dash 3=dot 4=dash-dot 
    spectrum->Draw("L");

   spectrum = new TGraph(galaxy.n_E_gammagrid);

    for (int ip = 0; ip < galaxy.n_E_gammagrid; ip++)
    {
      cout<<" setting E="<< galaxy.E_gamma[ip]<<" luminosity="<<gamma_IC_luminosity_spectrum[ip]<<endl;
      spectrum->SetPoint(ip, log10(galaxy.E_gamma[ip]), gamma_IC_luminosity_spectrum[ip] );
    }

    //Gtypes.h:enum EColor { kWhite, kBlack, kRed, kGreen, kBlue, kYellow, kMagenta, kCyan };
 
    spectrum->SetLineColor(kGreen);
    spectrum->SetLineWidth(2);
    spectrum->SetLineStyle(1);//1=solid 2=dash 3=dot 4=dash-dot 
    spectrum->Draw("L");

   spectrum = new TGraph(galaxy.n_E_gammagrid);

    for (int ip = 0; ip < galaxy.n_E_gammagrid; ip++)
    {
      //      cout<<" setting E="<< galaxy.E_gamma[ip]<<" luminosity="<<gamma_total_luminosity_spectrum[ip]<<endl;
      spectrum->SetPoint(ip, log10(galaxy.E_gamma[ip]), gamma_total_luminosity_spectrum[ip] );
    }

    //Gtypes.h:enum EColor { kWhite, kBlack, kRed, kGreen, kBlue, kYellow, kMagenta, kCyan };
 
    spectrum->SetLineColor(kBlack); //AWS20100514
    spectrum->SetLineWidth(2);
    spectrum->SetLineStyle(1);//1=solid 2=dash 3=dot 4=dash-dot 
    spectrum->Draw("L");

    // -- synchrotron

   spectrum = new TGraph(galaxy.n_nu_synchgrid);

    for (int inu = 0; inu < galaxy.n_nu_synchgrid; inu++)
    {
     double factor= 1./MEV2ERG; // conversion  erg  s-1   to MeV  s-1     using constants.h
     double E_nu=  galaxy.nu_synch[inu]* H / MEV2ERG;                                      // energy in MeV.  H is Plancks constant from constants.h

           cout<<" setting synch E="<< E_nu<<" luminosity="<<synchrotron_luminosity_spectrum[inu]*galaxy.nu_synch[inu]*factor<<endl;
           spectrum->SetPoint(inu, log10(E_nu), synchrotron_luminosity_spectrum[inu]*galaxy.nu_synch[inu]*factor );
    }

    //Gtypes.h:enum EColor { kWhite, kBlack, kRed, kGreen, kBlue, kYellow, kMagenta, kCyan };
 
    spectrum->SetLineColor(kBlack);
    spectrum->SetLineWidth(2);
    spectrum->SetLineStyle(1);//1=solid 2=dash 3=dot 4=dash-dot 
    spectrum->Draw("L");


    // --------------- cosmic rays

    // protons

     cosmic_ray_mode=3; // 0=none 1=from propagated gcr, 2=injection 3=source functions     4=all   AWS20100609

    if(cosmic_ray_mode==1 || cosmic_ray_mode==4)
    {

   spectrum = new TGraph(gcr[0].n_pgrid);

    for (int ip = 0; ip < gcr[0].n_pgrid ; ip++)
    {
    
     
      spectrum->SetPoint(ip , log10(gcr[0].Ekin[ip]), gcr_proton_luminosity_spectrum[ip] );
    }

    //Gtypes.h:enum EColor { kWhite, kBlack, kRed, kGreen, kBlue, kYellow, kMagenta, kCyan };
 
    spectrum->SetLineColor(kRed  );
    spectrum->SetLineWidth(2);
    spectrum->SetLineStyle(2);//1=solid 2=dash 3=dot 4=dash-dot 
    spectrum->Draw("L");

    //--
 



    // helium
  spectrum = new TGraph(gcr[0].n_pgrid);

    for (int ip = 0; ip < gcr[0].n_pgrid ; ip++)
    {
    
     
      spectrum->SetPoint(ip , log10(gcr[0].Ekin[ip]), gcr_helium_luminosity_spectrum[ip] );
    }

    //Gtypes.h:enum EColor { kWhite, kBlack, kRed, kGreen, kBlue, kYellow, kMagenta, kCyan };
 
    spectrum->SetLineColor(kBlue );
    spectrum->SetLineWidth(2);
    spectrum->SetLineStyle(2);//1=solid 2=dash 3=dot 4=dash-dot 
    spectrum->Draw("L");


 


    // electrons
  spectrum = new TGraph(gcr[0].n_pgrid);

    for (int ip = 0; ip < gcr[0].n_pgrid ; ip++)
    {
    
     
      spectrum->SetPoint(ip , log10(gcr[0].Ekin[ip]), gcr_lepton_luminosity_spectrum[ip] );
    }

    //Gtypes.h:enum EColor { kWhite, kBlack, kRed, kGreen, kBlue, kYellow, kMagenta, kCyan };
 
    spectrum->SetLineColor(kGreen);
    spectrum->SetLineWidth(2);
    spectrum->SetLineStyle(2);//1=solid 2=dash 3=dot 4=dash-dot 
    spectrum->Draw("L");

    } // if cosmic_ray_mode==1 || 4

    ////

    if(cosmic_ray_mode==2 || cosmic_ray_mode==4)
    {
     spectrum = new TGraph(gcr[0].n_pgrid);

    for (int ip = 0; ip < gcr[0].n_pgrid ; ip++)
    {
    
     
      spectrum->SetPoint(ip , log10(gcr[0].Ekin[ip]), gcr_proton_luminosity_spectrum2[ip] );
    }

    //Gtypes.h:enum EColor { kWhite, kBlack, kRed, kGreen, kBlue, kYellow, kMagenta, kCyan };
 
    spectrum->SetLineColor(kRed  );
    spectrum->SetLineWidth(2);
    spectrum->SetLineStyle(2);//1=solid 2=dash 3=dot 4=dash-dot 
    spectrum->Draw("L");

   //--
  spectrum = new TGraph(gcr[0].n_pgrid);

    for (int ip = 0; ip < gcr[0].n_pgrid ; ip++)
    {
    
     
      spectrum->SetPoint(ip , log10(gcr[0].Ekin[ip]), gcr_helium_luminosity_spectrum2[ip] );
    }

    //Gtypes.h:enum EColor { kWhite, kBlack, kRed, kGreen, kBlue, kYellow, kMagenta, kCyan };
 
    spectrum->SetLineColor(kBlue );
    spectrum->SetLineWidth(2);
    spectrum->SetLineStyle(2);//1=solid 2=dash 3=dot 4=dash-dot 
    spectrum->Draw("L");

    // electrons
  spectrum = new TGraph(gcr[0].n_pgrid);

    for (int ip = 0; ip < gcr[0].n_pgrid ; ip++)
    {
    
     
      spectrum->SetPoint(ip , log10(gcr[0].Ekin[ip]), gcr_lepton_luminosity_spectrum2[ip] );
    }

    //Gtypes.h:enum EColor { kWhite, kBlack, kRed, kGreen, kBlue, kYellow, kMagenta, kCyan };
 
    spectrum->SetLineColor(kGreen);
    spectrum->SetLineWidth(2);
    spectrum->SetLineStyle(2);//1=solid 2=dash 3=dot 4=dash-dot 
    spectrum->Draw("L");

    //--

   } // if cosmic_ray_mode==2||4



   if(cosmic_ray_mode==3 || cosmic_ray_mode==4)
    {

   spectrum = new TGraph(gcr[0].n_pgrid);

    for (int ip = 0; ip < gcr[0].n_pgrid ; ip++)
    {
    
     
      spectrum->SetPoint(ip , log10(gcr[0].Ekin[ip]), gcr_proton_luminosity_spectrum3[ip] );
    }

    //Gtypes.h:enum EColor { kWhite, kBlack, kRed, kGreen, kBlue, kYellow, kMagenta, kCyan };
 
    spectrum->SetLineColor(kRed  );
    spectrum->SetLineWidth(2);
    spectrum->SetLineStyle(3);//1=solid 2=dash 3=dot 4=dash-dot 
    spectrum->Draw("L");

    //--
 



    // helium
  spectrum = new TGraph(gcr[0].n_pgrid);

    for (int ip = 0; ip < gcr[0].n_pgrid ; ip++)
    {
    
     
      spectrum->SetPoint(ip , log10(gcr[0].Ekin[ip]), gcr_helium_luminosity_spectrum3[ip] );
    }

    //Gtypes.h:enum EColor { kWhite, kBlack, kRed, kGreen, kBlue, kYellow, kMagenta, kCyan };
 
    spectrum->SetLineColor(kBlue );
    spectrum->SetLineWidth(2);
    spectrum->SetLineStyle(3);//1=solid 2=dash 3=dot 4=dash-dot 
    spectrum->Draw("L");


 


    // primary electrons
  spectrum = new TGraph(gcr[0].n_pgrid);

    for (int ip = 0; ip < gcr[0].n_pgrid ; ip++)
    {
    
     
      spectrum->SetPoint(ip , log10(gcr[0].Ekin[ip]), gcr_primary_electron_luminosity_spectrum3[ip] );
    }

    //Gtypes.h:enum EColor { kWhite, kBlack, kRed, kGreen, kBlue, kYellow, kMagenta, kCyan };
 
    spectrum->SetLineColor(kGreen);
    spectrum->SetLineWidth(2);
    spectrum->SetLineStyle(3);//1=solid 2=dash 3=dot 4=dash-dot 
    spectrum->Draw("L");

   // secondary electrons
  spectrum = new TGraph(gcr[0].n_pgrid);

    for (int ip = 0; ip < gcr[0].n_pgrid ; ip++)
    {
    
     
      spectrum->SetPoint(ip , log10(gcr[0].Ekin[ip]), gcr_secondary_electron_luminosity_spectrum3[ip] );
    }

    //Gtypes.h:enum EColor { kWhite, kBlack, kRed, kGreen, kBlue, kYellow, kMagenta, kCyan };
 
    spectrum->SetLineColor(kCyan);
    spectrum->SetLineWidth(2);
    spectrum->SetLineStyle(3);//1=solid 2=dash 3=dot 4=dash-dot 
    spectrum->Draw("L");

   // secondary positrons
  spectrum = new TGraph(gcr[0].n_pgrid);

    for (int ip = 0; ip < gcr[0].n_pgrid ; ip++)
    {
    
     
      spectrum->SetPoint(ip , log10(gcr[0].Ekin[ip]), gcr_secondary_positron_luminosity_spectrum3[ip] );
    }

    //Gtypes.h:enum EColor { kWhite, kBlack, kRed, kGreen, kBlue, kYellow, kMagenta, kCyan };
 
    spectrum->SetLineColor(kMagenta);
    spectrum->SetLineWidth(2);
    spectrum->SetLineStyle(3);//1=solid 2=dash 3=dot 4=dash-dot 
    spectrum->Draw("L");



    } // if cosmic_ray_mode==3 || 4


   //AWS20100609 plot surface integrated ISRF 
   if(plot_ISRF==1||plot_ISRF==2)
   {
     // spectrum = new TGraph(galaxy.ISRF[0].n_pgrid);
   spectrum = new TGraph();//AWS20100609 automatic assignment to select points

   int iinu=0;
    for (int inu = 0; inu < galaxy.ISRF[0].n_pgrid ; inu++)
    {
    
     double E_nu=  galaxy.nu_ISRF[inu] * H / MEV2ERG; // conversion frequency to  erg     to MeV       using constants.h;                                    

     cout<<" setting ISRF for MeV plot: E="<< E_nu<<" opt +IR luminosity2="<< (opt_luminosity_spectrum2[inu] + IR_luminosity_spectrum2[inu])/MEV2ERG  <<endl; //  erg s-1 to MeV s-1

     //   spectrum->SetPoint(inu,  log10(E_nu), 1.); // dummy value 

     if(E_nu>0.7e-9) // avoid big fluctuations at low energies
     {
                 double ISRF_scaling=1.0;
       if(plot_ISRF==2) ISRF_scaling=1.0e-4;
      
      spectrum->SetPoint(iinu, log10(E_nu), (opt_luminosity_spectrum2[inu] + IR_luminosity_spectrum2[inu])/MEV2ERG * ISRF_scaling );//  erg s-1 to MeV s-1
      iinu++;
      }
    }

    //Gtypes.h:enum EColor { kWhite, kBlack, kRed, kGreen, kBlue, kYellow, kMagenta, kCyan };
 
    spectrum->SetLineColor(kMagenta);
    spectrum->SetLineWidth(2);
    spectrum->SetLineStyle(1);//1=solid 2=dash 3=dot 4=dash-dot 
    spectrum->Draw("L");
   }

  if(plot_ISRF==2)
  {
   text=new TText();
   text->SetTextFont(62);
   text->SetTextColor(kMagenta);
   text->SetTextSize(0.034 );                                             
   text->SetTextAlign(12);
   strcpy(workstring1,"X 10");                                                                                                                         
   text->DrawTextNDC(.50 ,.60 ,workstring1);// NDC=normalized coord system  
   strcpy(workstring1,"-4");
   text->DrawTextNDC(.56 ,.61 ,workstring1);// NDC=normalized coord system  
  }                                                                                  
    





 //============== postscript, gif and txt files
  
  sprintf(workstring1,"l_%.2f_%.2f_%.2f_%.2f",                       
	  galplotdef.long_min1,galplotdef.long_max1,
	  galplotdef.long_min2,galplotdef.long_max2);
  
  sprintf(workstring2,"b_%.2f_%.2f_%.2f_%.2f",                        
	  galplotdef.lat_min1,galplotdef.lat_max1,
	  galplotdef.lat_min2,galplotdef.lat_max2);
  
  strcpy(psfile,"plots/");
  if(plot_ISRF==0)strcat(psfile,"luminosity_MeV_without_ISRF_")       ;//AWS20100609
  if(plot_ISRF==1)strcat(psfile,"luminosity_MeV_with_ISRF_"   )       ;//AWS20100609
  if(plot_ISRF==2)strcat(psfile,"luminosity_MeV_with_ISRF_scaled_"   );//AWS20100609
  strcat(psfile,galdef.galdef_ID);
  strcat(psfile,"_"        );
  strcat(psfile,workstring1);
  strcat(psfile,"_"        );
  strcat(psfile,workstring2);
  strcat(psfile,"_");    //AWS20100308
  

  strcat(psfile,galplotdef.psfile_tag);

  strcpy(giffile,psfile);
  strcpy(txtfile,psfile);                        
  strcpy(iosfile,psfile);                      







 //============== postscript and gif output


  if(galplotdef.output_format==1 || galplotdef.output_format==3)
   {
   strcat(psfile,".eps");
   cout<<"postscript file="<<psfile<<endl;
   c1->Print(psfile,"eps" );
  }

  if(galplotdef.output_format==2 || galplotdef.output_format==3)
   {
   strcat(giffile,".gif");
   cout<<"       gif file="<<giffile<<endl;
   c1->Print(giffile,"gif" );
  }


  } //plot_ISRF   AWS20100609














  //-------------- Hz units


  strcpy(name,"luminosity_Hz");// to identify it: name is not plotted
  c1=new TCanvas(name,canvastitle,300,150, 600, 600);

  //  c1->SetLogx(); // take log10 to make axis labelling better
  c1->SetLogy();
  
  if(galplotdef.gamma_spectrum_Emin<=0.0)
    galplotdef.gamma_spectrum_Emin=galaxy.E_gamma_min;
  if(galplotdef.gamma_spectrum_Emax<=0.0)
    galplotdef.gamma_spectrum_Emax=galaxy.E_gamma_max;
  
  //----------------------------------------- axes


  // for radio and gammas
         luminosity_min = 1e42*MEV2ERG;
         luminosity_max = 1e45*MEV2ERG;

  // for radio and gammas and optical/IR
         luminosity_min = 1e42*MEV2ERG;
         luminosity_max = 1e50*MEV2ERG * 1.5;//AWS20100421


                             
 //	                                      frequency, Hz
	 h=c1->DrawFrame(log10(galplotdef.gamma_spectrum_Emin*MEV2ERG/H), luminosity_min,  
			 log10(galplotdef.gamma_spectrum_Emax*MEV2ERG/H) ,luminosity_max); 



  
      h->SetXTitle("log (Frequency, Hz)");
  //  h->GetXaxis()->SetTitle("here is a very long title to test whether it appears                      ");
      h->SetTitleOffset(1.1, "X");//+ve up  was 1.1 , modify for root 5.08
      h->SetTitleSize(0.035, "X");
      
  h->SetLabelOffset(0.00, "X");//+ve up 
  h->SetLabelSize(0.035,  "X");
  

  h->SetYTitle("Galactic luminosity, erg Hz s^{-1} Hz^{-1}  ");  
  h->SetTitleOffset(1.3,  "Y");//+ve left
  h->SetTitleSize(0.035,  "Y");
  h->SetLabelOffset(0.00, "Y");//+ve left
  h->SetLabelSize(0.035,  "Y");
     


  //----------------------------------------- text
  text=new TText();
  text->SetTextFont(62);
  text->SetTextColor(kBlack);
  text->SetTextSize(0.034 );                                             
  text->SetTextAlign(12);
  
  
    if(galplotdef.verbose!=-1003)// selectable debug: no title or l,b ranges                      
    {
      text->DrawTextNDC(.10 ,.93 ,canvastitle);// NDC=normalized coord system   
      text->SetTextSize(0.022 );                                              
      //     text->DrawTextNDC(.58 ,.88 ,workstring1);// NDC=normalized coord system   
      //     text->DrawTextNDC(.58 ,.86 ,workstring2);// NDC=normalized coord system   
    }
  




    spectrum = new TGraph(galaxy.n_E_gammagrid);

    for (int ip = 0; ip < galaxy.n_E_gammagrid; ip++)
    {
      //      cout<<" setting E="<< galaxy.E_gamma[ip]<<" luminosity="<<gamma_bremss_luminosity_spectrum[ip]<<endl;
      spectrum->SetPoint(ip, log10(galaxy.E_gamma[ip]*MEV2ERG/H), gamma_bremss_luminosity_spectrum[ip]*MEV2ERG );
    }

    //Gtypes.h:enum EColor { kWhite, kBlack, kRed, kGreen, kBlue, kYellow, kMagenta, kCyan };
 
    spectrum->SetLineColor(kCyan);
    spectrum->SetLineWidth(2);
    spectrum->SetLineStyle(1);//1=solid 2=dash 3=dot 4=dash-dot 
    spectrum->Draw("L");


   spectrum = new TGraph(galaxy.n_E_gammagrid);

    for (int ip = 0; ip < galaxy.n_E_gammagrid; ip++)
    {
      //      cout<<" setting E="<< galaxy.E_gamma[ip]<<" luminosity="<<gamma_pi0_decay_luminosity_spectrum[ip]<<endl;
      spectrum->SetPoint(ip, log10(galaxy.E_gamma[ip]*MEV2ERG/H), gamma_pi0_decay_luminosity_spectrum[ip]*MEV2ERG );
    }

    //Gtypes.h:enum EColor { kWhite, kBlack, kRed, kGreen, kBlue, kYellow, kMagenta, kCyan };
 
    spectrum->SetLineColor(kRed);
    spectrum->SetLineWidth(2);
    spectrum->SetLineStyle(1);//1=solid 2=dash 3=dot 4=dash-dot 
    spectrum->Draw("L");

   spectrum = new TGraph(galaxy.n_E_gammagrid);

    for (int ip = 0; ip < galaxy.n_E_gammagrid; ip++)
    {
      cout<<" setting E="<< galaxy.E_gamma[ip]<<" luminosity="<<gamma_IC_luminosity_spectrum[ip]<<endl;
      spectrum->SetPoint(ip, log10(galaxy.E_gamma[ip]*MEV2ERG/H), gamma_IC_luminosity_spectrum[ip]*MEV2ERG );
    }

    //Gtypes.h:enum EColor { kWhite, kBlack, kRed, kGreen, kBlue, kYellow, kMagenta, kCyan };
 
    spectrum->SetLineColor(kGreen);
    spectrum->SetLineWidth(2);
    spectrum->SetLineStyle(1);//1=solid 2=dash 3=dot 4=dash-dot 
    spectrum->Draw("L");

   spectrum = new TGraph(galaxy.n_E_gammagrid);

    for (int ip = 0; ip < galaxy.n_E_gammagrid; ip++)
    {
      //      cout<<" setting E="<< galaxy.E_gamma[ip]<<" luminosity="<<gamma_total_luminosity_spectrum[ip]<<endl;
      spectrum->SetPoint(ip, log10(galaxy.E_gamma[ip]*MEV2ERG/H), gamma_total_luminosity_spectrum[ip]*MEV2ERG );
    }

    //Gtypes.h:enum EColor { kWhite, kBlack, kRed, kGreen, kBlue, kYellow, kMagenta, kCyan };
 
    spectrum->SetLineColor(kBlack);//AWS20100514
    spectrum->SetLineWidth(2);
    spectrum->SetLineStyle(1);//1=solid 2=dash 3=dot 4=dash-dot 
    spectrum->Draw("L");


   spectrum = new TGraph(galaxy.n_nu_synchgrid);

    for (int inu = 0; inu < galaxy.n_nu_synchgrid; inu++)
    {
     double factor= 1./MEV2ERG; // conversion  erg  s-1   to MeV  s-1     using constants.h
     double E_nu=  galaxy.nu_synch[inu];                                    

           cout<<" setting synch E="<< E_nu<<" luminosity="<<synchrotron_luminosity_spectrum[inu]*galaxy.nu_synch[inu]<<endl;
           spectrum->SetPoint(inu, log10(E_nu), synchrotron_luminosity_spectrum[inu]*galaxy.nu_synch[inu] );
    }

    //Gtypes.h:enum EColor { kWhite, kBlack, kRed, kGreen, kBlue, kYellow, kMagenta, kCyan };
 
    spectrum->SetLineColor(kBlack);
    spectrum->SetLineWidth(2);
    spectrum->SetLineStyle(1);//1=solid 2=dash 3=dot 4=dash-dot 
    spectrum->Draw("L");


    int alt_luminosity=0;//AWS20100412
    if(alt_luminosity==1)//AWS20100412 
    {
   spectrum = new TGraph(galaxy.ISRF[0].n_pgrid);

    for (int inu = 0; inu < galaxy.ISRF[0].n_pgrid ; inu++)
    {
    
     double E_nu=  galaxy.nu_ISRF[inu];                                    

           cout<<" setting ISRF E="<< E_nu<<" opt +IR luminosity="<< (opt_luminosity_spectrum[inu] + IR_luminosity_spectrum[inu])*galaxy.nu_ISRF[inu]  <<endl;

           spectrum->SetPoint(inu, log10(E_nu), (opt_luminosity_spectrum[inu] + IR_luminosity_spectrum[inu])*galaxy.nu_ISRF[inu]);
    }

    //Gtypes.h:enum EColor { kWhite, kBlack, kRed, kGreen, kBlue, kYellow, kMagenta, kCyan };
 
    spectrum->SetLineColor(kMagenta);
    spectrum->SetLineWidth(2);
    spectrum->SetLineStyle(1);//1=solid 2=dash 3=dot 4=dash-dot 
    spectrum->Draw("L");
    }

    //AWS20100412 plot surface integrated ISRF 
   spectrum = new TGraph(galaxy.ISRF[0].n_pgrid);

    for (int inu = 0; inu < galaxy.ISRF[0].n_pgrid ; inu++)
    {
    
     double E_nu=  galaxy.nu_ISRF[inu];                                    

     cout<<" setting ISRF E="<< E_nu<<" opt +IR luminosity2="<< (opt_luminosity_spectrum2[inu] + IR_luminosity_spectrum2[inu])  <<endl; // already in erg s-1

     spectrum->SetPoint(inu, log10(E_nu), (opt_luminosity_spectrum2[inu] + IR_luminosity_spectrum2[inu]));// already in erg s-1
    }

    //Gtypes.h:enum EColor { kWhite, kBlack, kRed, kGreen, kBlue, kYellow, kMagenta, kCyan };
 
    spectrum->SetLineColor(kMagenta);
    spectrum->SetLineWidth(2);
    spectrum->SetLineStyle(1);//1=solid 2=dash 3=dot 4=dash-dot 
    spectrum->Draw("L");




   // -- cosmic rays
    // protons

    if(cosmic_ray_mode==1 || cosmic_ray_mode==4)
    {
    spectrum = new TGraph(gcr[0].n_pgrid);

    for (int ip = 0; ip < gcr[0].n_pgrid ; ip++)
    {
    
      double E_nu = gcr[0].Ekin[ip] * MEV2ERG / H; // MeV -> Hz (ala photons!)
      spectrum->SetPoint(ip , log10(E_nu), gcr_proton_luminosity_spectrum[ip] * MEV2ERG );
    }

    //Gtypes.h:enum EColor { kWhite, kBlack, kRed, kGreen, kBlue, kYellow, kMagenta, kCyan };
 
    spectrum->SetLineColor(kRed  );
    spectrum->SetLineWidth(2);
    spectrum->SetLineStyle(2);//1=solid 2=dash 3=dot 4=dash-dot 
    spectrum->Draw("L");


    // helium
  spectrum = new TGraph(gcr[0].n_pgrid);

    for (int ip = 0; ip < gcr[0].n_pgrid ; ip++)
    {
    
      double E_nu = gcr[0].Ekin[ip] * MEV2ERG / H; // MeV -> Hz (ala photons!)
      spectrum->SetPoint(ip , log10(E_nu), gcr_helium_luminosity_spectrum[ip]* MEV2ERG );
    }

    //Gtypes.h:enum EColor { kWhite, kBlack, kRed, kGreen, kBlue, kYellow, kMagenta, kCyan };
 
    spectrum->SetLineColor(kBlue );
    spectrum->SetLineWidth(2);
    spectrum->SetLineStyle(2);//1=solid 2=dash 3=dot 4=dash-dot 
    spectrum->Draw("L");


    // electrons
  spectrum = new TGraph(gcr[0].n_pgrid);

    for (int ip = 0; ip < gcr[0].n_pgrid ; ip++)
    {
    
      double E_nu = gcr[0].Ekin[ip] * MEV2ERG / H; // MeV -> Hz (ala photons!)
      spectrum->SetPoint(ip , log10(E_nu), gcr_lepton_luminosity_spectrum[ip] *MEV2ERG );
    }

    //Gtypes.h:enum EColor { kWhite, kBlack, kRed, kGreen, kBlue, kYellow, kMagenta, kCyan };
 
    spectrum->SetLineColor(kGreen);
    spectrum->SetLineWidth(2);
    spectrum->SetLineStyle(2);//1=solid 2=dash 3=dot 4=dash-dot 
    spectrum->Draw("L");

    } // cosmic_ray_mode==1 || 4


   if(cosmic_ray_mode==2 || cosmic_ray_mode==4)
    {
    spectrum = new TGraph(gcr[0].n_pgrid);

    for (int ip = 0; ip < gcr[0].n_pgrid ; ip++)
    {
    
      double E_nu = gcr[0].Ekin[ip] * MEV2ERG / H; // MeV -> Hz (ala photons!)
      spectrum->SetPoint(ip , log10(E_nu), gcr_proton_luminosity_spectrum2[ip] * MEV2ERG );
    }

    //Gtypes.h:enum EColor { kWhite, kBlack, kRed, kGreen, kBlue, kYellow, kMagenta, kCyan };
 
    spectrum->SetLineColor(kRed  );
    spectrum->SetLineWidth(2);
    spectrum->SetLineStyle(2);//1=solid 2=dash 3=dot 4=dash-dot 
    spectrum->Draw("L");


    // helium
  spectrum = new TGraph(gcr[0].n_pgrid);

    for (int ip = 0; ip < gcr[0].n_pgrid ; ip++)
    {
    
      double E_nu = gcr[0].Ekin[ip] * MEV2ERG / H; // MeV -> Hz (ala photons!)
      spectrum->SetPoint(ip , log10(E_nu), gcr_helium_luminosity_spectrum2[ip]* MEV2ERG );
    }

    //Gtypes.h:enum EColor { kWhite, kBlack, kRed, kGreen, kBlue, kYellow, kMagenta, kCyan };
 
    spectrum->SetLineColor(kBlue );
    spectrum->SetLineWidth(2);
    spectrum->SetLineStyle(2);//1=solid 2=dash 3=dot 4=dash-dot 
    spectrum->Draw("L");


    // electrons
  spectrum = new TGraph(gcr[0].n_pgrid);

    for (int ip = 0; ip < gcr[0].n_pgrid ; ip++)
    {
    
      double E_nu = gcr[0].Ekin[ip] * MEV2ERG / H; // MeV -> Hz (ala photons!)
      spectrum->SetPoint(ip , log10(E_nu), gcr_lepton_luminosity_spectrum2[ip] *MEV2ERG );
    }

    //Gtypes.h:enum EColor { kWhite, kBlack, kRed, kGreen, kBlue, kYellow, kMagenta, kCyan };
 
    spectrum->SetLineColor(kGreen);
    spectrum->SetLineWidth(2);
    spectrum->SetLineStyle(2);//1=solid 2=dash 3=dot 4=dash-dot 
    spectrum->Draw("L");

    } // cosmic_ray_mode==2 || 4



   if(cosmic_ray_mode==3 || cosmic_ray_mode==4)
    {
    spectrum = new TGraph(gcr[0].n_pgrid);

    for (int ip = 0; ip < gcr[0].n_pgrid ; ip++)
    {
    
      double E_nu = gcr[0].Ekin[ip] * MEV2ERG / H; // MeV -> Hz (ala photons!)
      spectrum->SetPoint(ip , log10(E_nu), gcr_proton_luminosity_spectrum3[ip] * MEV2ERG );
    }

    //Gtypes.h:enum EColor { kWhite, kBlack, kRed, kGreen, kBlue, kYellow, kMagenta, kCyan };
 
    spectrum->SetLineColor(kRed  );
    spectrum->SetLineWidth(2);
    spectrum->SetLineStyle(3);//1=solid 2=dash 3=dot 4=dash-dot 
    spectrum->Draw("L");


    // helium
  spectrum = new TGraph(gcr[0].n_pgrid);

    for (int ip = 0; ip < gcr[0].n_pgrid ; ip++)
    {
    
      double E_nu = gcr[0].Ekin[ip] * MEV2ERG / H; // MeV -> Hz (ala photons!)
      spectrum->SetPoint(ip , log10(E_nu), gcr_helium_luminosity_spectrum3[ip]* MEV2ERG );
    }

    //Gtypes.h:enum EColor { kWhite, kBlack, kRed, kGreen, kBlue, kYellow, kMagenta, kCyan };
 
    spectrum->SetLineColor(kBlue );
    spectrum->SetLineWidth(2);
    spectrum->SetLineStyle(3);//1=solid 2=dash 3=dot 4=dash-dot 
    spectrum->Draw("L");


    // primary electrons
  spectrum = new TGraph(gcr[0].n_pgrid);

    for (int ip = 0; ip < gcr[0].n_pgrid ; ip++)
    {
    
      double E_nu = gcr[0].Ekin[ip] * MEV2ERG / H; // MeV -> Hz (ala photons!)
      spectrum->SetPoint(ip , log10(E_nu), gcr_primary_electron_luminosity_spectrum3[ip] *MEV2ERG );
    }

    //Gtypes.h:enum EColor { kWhite, kBlack, kRed, kGreen, kBlue, kYellow, kMagenta, kCyan };
 
    spectrum->SetLineColor(kGreen);
    spectrum->SetLineWidth(2);
    spectrum->SetLineStyle(3);//1=solid 2=dash 3=dot 4=dash-dot 
    spectrum->Draw("L");


  // secondary electrons
  spectrum = new TGraph(gcr[0].n_pgrid);

    for (int ip = 0; ip < gcr[0].n_pgrid ; ip++)
    {
    
      double E_nu = gcr[0].Ekin[ip] * MEV2ERG / H; // MeV -> Hz (ala photons!)
      spectrum->SetPoint(ip , log10(E_nu), gcr_secondary_electron_luminosity_spectrum3[ip] *MEV2ERG );
    }

    //Gtypes.h:enum EColor { kWhite, kBlack, kRed, kGreen, kBlue, kYellow, kMagenta, kCyan };
 
    spectrum->SetLineColor(kCyan);
    spectrum->SetLineWidth(2);
    spectrum->SetLineStyle(3);//1=solid 2=dash 3=dot 4=dash-dot 
    spectrum->Draw("L");



  // secondary positrons
  spectrum = new TGraph(gcr[0].n_pgrid);

    for (int ip = 0; ip < gcr[0].n_pgrid ; ip++)
    {
    
      double E_nu = gcr[0].Ekin[ip] * MEV2ERG / H; // MeV -> Hz (ala photons!)
      spectrum->SetPoint(ip , log10(E_nu), gcr_secondary_positron_luminosity_spectrum3[ip] *MEV2ERG );
    }

    //Gtypes.h:enum EColor { kWhite, kBlack, kRed, kGreen, kBlue, kYellow, kMagenta, kCyan };
 
    spectrum->SetLineColor(kMagenta);
    spectrum->SetLineWidth(2);
    spectrum->SetLineStyle(3);//1=solid 2=dash 3=dot 4=dash-dot 
    spectrum->Draw("L");


    } // cosmic_ray_mode==3 || 4












 //============== postscript, gif and txt files
  
  sprintf(workstring1,"l_%.2f_%.2f_%.2f_%.2f",                       
	  galplotdef.long_min1,galplotdef.long_max1,
	  galplotdef.long_min2,galplotdef.long_max2);
  
  sprintf(workstring2,"b_%.2f_%.2f_%.2f_%.2f",                        
	  galplotdef.lat_min1,galplotdef.lat_max1,
	  galplotdef.lat_min2,galplotdef.lat_max2);
  
  strcpy(psfile,"plots/");
  strcat(psfile,"luminosity_Hz_");
  strcat(psfile,galdef.galdef_ID);
  strcat(psfile,"_"        );
  strcat(psfile,workstring1);
  strcat(psfile,"_"        );
  strcat(psfile,workstring2);
  strcat(psfile,"_");    //AWS20100308
  

  strcat(psfile,galplotdef.psfile_tag);

  strcpy(giffile,psfile);
  strcpy(txtfile,psfile);                        
  strcpy(iosfile,psfile);                      







 //============== postscript and gif output


  if(galplotdef.output_format==1 || galplotdef.output_format==3)
   {
   strcat(psfile,".eps");
   cout<<"postscript file="<<psfile<<endl;
   c1->Print(psfile,"eps" );
  }

  if(galplotdef.output_format==2 || galplotdef.output_format==3)
   {
   strcat(giffile,".gif");
   cout<<"       gif file="<<giffile<<endl;
   c1->Print(giffile,"gif" );
  }







  // test new read routine
  //read_bremss_emiss("54_z30G4c5MS");

  //exit(0);



  cout<<" <<<< plot_luminosity   "<<endl;
  return 0;
 }
