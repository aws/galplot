#ifndef Sun_h
#define Sun_h

#include "Model.h"
#include "Exposure.h"
#include "Counts.h"
#include "Psf.h"
#include "Sources.h"
#include "Parameters.h"
#include "Variables.h"


class Sun : public BaseModel {
	public:
		/** \brief The constructor takes the same parameters as the BaseModel constructor.
		 *
		 * The constructor should load and prepare the data used in the model. 
		 */

                                                                                                                           

                Sun(const Exposure &exposure, const CountsMap &counts, const Psf &psf, const Sources &sources, const Parameters &pars, const Skymap<char> &filter, unsigned int configure = 3); //AWS20080717
	    

		void getMap(const Variables &vars, Skymap<double> &map);
		gradMap getComponents(const Variables & vars, const std::string &prefix);
		void getGrads(const Variables &vars, const std::string & varName, Skymap<double> &map);
		gradMap getSecDer(const Variables & vars, const std::string & varName1, const std::string & varName2);
	private:
       
		Skymap<double> fSun_disk_Map, fSun_IC_Map;    
};

#endif
