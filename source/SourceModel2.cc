#include "SourceModel2.h"
#include "Model.h"
#include "Exposure.h"
#include "Counts.h"
#include "Psf.h"
#include "Sources.h"
#include "Parameters.h"
#include "Variables.h"
#include "Skymap.h"
#include "Utils.h"
#include <string>
#include <iostream>
#include <sstream>
#include <iomanip>
#include <algorithm>
#include <valarray>




/******************************************************************************************/
/******************************************************************************************/
//Constructor
/******************************************************************************************/
SourceModel2::SourceModel2(const Exposure &exposure, const CountsMap &counts, const Psf &psf, const Sources &sources, const Parameters &pars, const Skymap<char> & filter, const std::string &scalerVariable, unsigned int configure) : BaseModel(counts, exposure, psf, sources, pars,filter,configure), ffixedScalerVar(scalerVariable){

   //Relative error for the fit parameters initial guess
   double relErr = 1e-2;

   //The containment fraction of the psf
   fpsfFraction = 0.999;

	 //Have a flux limit, or a number limit on fixed sources
	 double fluxLim = 0;
	 int maxFixedSources = -1;
	 try{
		 fparameters.getParameter("minFixedSourceFlux", fluxLim);
	 }catch (Parameters::ParameterError){}
	 try{
		 fparameters.getParameter("maxNumberFixedSources", maxFixedSources);
	 }catch (Parameters::ParameterError){}

	//Loop through all the sources and find out their type
	//Add their indices to the correct vectors
	 for (int i = 0; i < fsources.size(); ++i){  std::cout<<"SourceModel2: i, fsources[i].fitType, flux100, fluxLim flux1000:"<<i<<" "<<fsources[i].fitType<<" "<<fsources[i].flux100<<" "<<fluxLim<<" "<<fsources[i].flux1000<<std::endl;  //AWS20200118  flux100 not in 4FGL
		switch(fsources[i].fitType){
			case Sources::FIXED:
			  //if (fsources[i].flux100 > fluxLim && (maxFixedSources > fFixedSources.size() || maxFixedSources == -1)) //AWS20200118
					fFixedSources.push_back(i);
				break;
			case Sources::PREFACTOR:
				fPrefactorSources.push_back(i);
				break;
			case Sources::INDEX:
				fIndexSources.push_back(i);
				break;
			case Sources::POWERLAW:
				fPowerLawSources.push_back(i);
				break;
			case Sources::POWERLAWEXPCUTOFF:
				fPowerLawExpCutOffSources.push_back(i);
				break;
			case Sources::BROKENPOWERLAW:
				fBrokenPowerLawSources.push_back(i);
				break;
			case Sources::FREE:
				fFreeSources.push_back(i);
				break;
		}
	}
	
	//Debugging output
	std::cout<<"Number of Fixed sources: "<<fFixedSources.size()<<std::endl;
	std::cout<<"Number of Prefactor sources: "<<fPrefactorSources.size()<<std::endl;
	std::cout<<"Number of Index sources: "<<fIndexSources.size()<<std::endl;
	std::cout<<"Number of PowerLaw sources: "<<fPowerLawSources.size()<<std::endl;
	std::cout<<"Number of PowerLawExpCutOff sources: "<<fPowerLawExpCutOffSources.size()<<std::endl;
	std::cout<<"Number of BrokenPowerLaw sources: "<<fBrokenPowerLawSources.size()<<std::endl;
	std::cout<<"Number of Free sources: "<<fFreeSources.size()<<std::endl;

	//The energy binning, taking the psf energy bins into account
	std::valarray<double> eMinArr, eMaxArr;
	getPsfEnergyBoundaries(eMinArr, eMaxArr);

	//The default break energy for broken power laws and exponential cut offs
	const double Eb = 1e4;

	//Read the fixed sources counts map from file if available
	std::string cacheFile;
	cacheFile = "NONE";
	
	
	bool cache = false;


	if (! cache && fFixedSources.size() != 0){
		//Create the fixed sources counts map
		std::valarray<int> fixedSources(&fFixedSources[0], fFixedSources.size());
		SparseSkymap<double> fixedMap(fcounts.getCountsMap().Order(), fcounts.getEMin(), fcounts.getEMax());
		genCountMaps(fixedSources, &fixedMap);
		ffixedSkymap = fixedMap.toSkymap();
		//Store the map if requested
		if ( cacheFile != "NONE" ) ffixedSkymap.write(cacheFile);
	}

	//Set a variable for the fixed map
	//Adding the free source parameters
	if (! ffixedScalerVar.empty() && fFixedSources.size() != 0 ){
	   //Try to read it from the config
	   try {
	      std::vector<std::string> varNames(1);
	      varNames[0] = ffixedScalerVar;
	      fvariables.add(varNames, fparameters);
	   } catch (...) {
	      fvariables.add(ffixedScalerVar, 1.0, relErr);
      	      fvariables.setLowerLimit(ffixedScalerVar, 0);
	   }
	}

	
	SparseSkymap<double> tmpMap; //This will be used as a temporary to create the fitted skymaps

//#pragma omp parallel default(shared) firstprivate(tmpMap)
	{
	
	//First the prefactors
//#pragma omp for schedule(dynamic) nowait
	for (int i = 0; i < fPrefactorSources.size(); ++i){
		int si = fPrefactorSources[i];
		std::string varName = "Pref_" + fsources[si].name;
		fvariables.add(varName, fsources[si].spParameters[0], fsources[si].spParametersUnc[0]);
		fvariables.setLowerLimit(varName,0);
		SparseSkymap<double> *map = new SparseSkymap<double>(fcounts.getCountsMap().Order(), fcounts.getEMin(), fcounts.getEMax());
//#pragma omp critical (addSkymap)
		fvariableSkymaps[si] = map;
		genCountMap(si, map);
		//Store the variable value
		std::vector<double> tmp(1, fsources[si].spParameters[0]);
//#pragma omp critical (addPars)
		flastPars[si] = tmp;
	}
	
	//Then indices
//#pragma omp for schedule(dynamic) nowait
	for (int i = 0; i < fIndexSources.size(); ++i){
		int si = fIndexSources[i];
		std::string varName = "Indx_" + fsources[si].name;
		fvariables.add(varName, fsources[si].spParameters[1], fsources[si].spParametersUnc[1]);
		SparseSkymap<double> *map = new SparseSkymap<double>(fcounts.getCountsMap().Order(), fcounts.getEMin(), fcounts.getEMax());
//#pragma omp critical (addSkymap)
		fvariableSkymaps[si] = map;
		genCountMap(si, map);
		//Store the variable value
		std::vector<double> tmp(1, fsources[si].spParameters[1]);
//#pragma omp critical (addPars)
		flastPars[si] = tmp;
	}
	
	//Then powerlaws
//#pragma omp for schedule(dynamic) nowait
	for (int i = 0; i < fPowerLawSources.size(); ++i){
		int si = fPowerLawSources[i];
		std::string varName = "Pref_" + fsources[si].name;
		fvariables.add(varName, fsources[si].spParameters[0], fsources[si].spParametersUnc[0]);
		fvariables.setLowerLimit(varName,0);
		varName = "Indx_" + fsources[si].name;
		fvariables.add(varName, fsources[si].spParameters[1], fsources[si].spParametersUnc[1]);
		SparseSkymap<double> *map = new SparseSkymap<double>(fcounts.getCountsMap().Order(), fcounts.getEMin(), fcounts.getEMax());
//#pragma omp critical (addSkymap)
		fvariableSkymaps[si] = map;
		genCountMap(si, map);
		//Store the variable value
		std::vector<double> tmp(2, fsources[si].spParameters[0]);
		tmp[1] = fsources[si].spParameters[1];
//#pragma omp critical (addPars)
		flastPars[si] = tmp;
	}
	
	//Power laws with exponential cut off
//#pragma omp for schedule(dynamic) nowait
	for (int i = 0; i < fPowerLawExpCutOffSources.size(); ++i){
		int si = fPowerLawExpCutOffSources[i];
		std::string varName = "Pref_" + fsources[si].name;
		fvariables.add(varName, fsources[si].spParameters[0], fsources[si].spParametersUnc[0]);
		fvariables.setLowerLimit(varName,0);
		varName = "Indx_" + fsources[si].name;
		fvariables.add(varName, fsources[si].spParameters[1], fsources[si].spParametersUnc[1]);
		varName = "Ecut_" + fsources[si].name;
		if ( fsources[si].spType == Sources::POWERLAWEXPCUTOFF) {
			fvariables.add(varName, fsources[si].spParameters[2], fsources[si].spParametersUnc[2]);
		} else {
			fvariables.add(varName, Eb, Eb*1e-2); //Defaults to 10 GeV
		}
		fvariables.setLowerLimit(varName,0);
		SparseSkymap<double> *map = new SparseSkymap<double>(fcounts.getCountsMap().Order(), fcounts.getEMin(), fcounts.getEMax());
//#pragma omp critical (addSkymap)
		fvariableSkymaps[si] = map;
		genCountMap(si, map);
		//Store the variable value
		std::vector<double> tmp(3, fsources[si].spParameters[0]);
		tmp[1] = fsources[si].spParameters[1];
		tmp[2] = fvariables[varName];
//#pragma omp critical (addPars)
		flastPars[si] = tmp;
		if ( fsources[si].spType != Sources::POWERLAWEXPCUTOFF ) {
			//We must make the cut in the map
		   const std::valarray<double> &en(map->getSpectra());
			std::valarray<double> newScale(en.size());
			for (int j = 0; j < newScale.size(); ++j){
				newScale[j] = exp(-en[j]/tmp[2]);
			}
			//Loop over all the pixels in the map
			for (SparseSkymap<double>::iterator it = map->begin(); it != map->end(); ++it){
				(*it).second *= newScale[(*it).first.second];
			}
		}
	}
	
	//Broken power laws 
//#pragma omp for schedule(dynamic) nowait
	for (int i = 0; i < fBrokenPowerLawSources.size(); ++i){
		int si = fBrokenPowerLawSources[i];
		std::string varName = "Ind1_" + fsources[si].name;
		fvariables.add(varName, fsources[si].spParameters[1], fsources[si].spParametersUnc[1]);
		if ( fsources[si].spType == Sources::BROKENPOWERLAW ) {
			varName = "Ind2_" + fsources[si].name;
			fvariables.add(varName, fsources[si].spParameters[2], fsources[si].spParametersUnc[2]);
			varName = "Ebre_" + fsources[si].name;
			fvariables.add(varName, fsources[si].spParameters[3], fsources[si].spParametersUnc[3]);
			fvariables.setLowerLimit(varName,0);
			double pref = fsources[si].spParameters[0];
			std::string varName = "Pref_" + fsources[si].name;
			fvariables.add(varName, pref, fsources[si].prefactor*pow(fsources[si].pivot/Eb, fsources[si].index));
			fvariables.setLowerLimit(varName,0);
		} else {
			varName = "Ind2_" + fsources[si].name;
			fvariables.add(varName, fsources[si].spParameters[1], fabs(fsources[si].spParameters[1])*relErr);
			varName = "Ebre_" + fsources[si].name;
			fvariables.add(varName, Eb, Eb*relErr); //Defaults to 10 GeV
			fvariables.setLowerLimit(varName,0);
			double pref = fsources[si].spParameters[0]*pow(fsources[si].pivot/Eb,fsources[si].spParameters[1]);
			double prefUnc = fsources[si].spParametersUnc[0]*pow(fsources[si].pivot/Eb,fsources[si].spParameters[1]);
			std::string varName = "Pref_" + fsources[si].name;
			fvariables.add(varName, pref, prefUnc);
			fvariables.setLowerLimit(varName,0);
		}
		SparseSkymap<double> *map = new SparseSkymap<double>(fcounts.getCountsMap().Order(), fcounts.getEMin(), fcounts.getEMax());
//#pragma omp critical (addSkymap)
		fvariableSkymaps[si] = map;
		genCountMap(si, map);
		//Store the variable value
		std::vector<double> tmp(4, fsources[si].spParameters[0]);
		tmp[1] = fsources[si].spParameters[1];
		varName = "Ind2_" + fsources[si].name;
		tmp[2] = fvariables[varName];
		varName = "Ebre_" + fsources[si].name;
		tmp[3] = fvariables[varName];
//#pragma omp critical (addPars)
		flastPars[si] = tmp;
	}

	//Free spectra
//#pragma omp for schedule(dynamic) nowait
	for (int i = 0; i < fFreeSources.size(); ++i){
		int si = fFreeSources[i];
		std::vector<double> tmp(fcounts.getEMin().size());
		//Use the same energies as in generating the counts map
		std::valarray<double> energies(&fpsf.getEnergies()[0], fpsf.getEnergies().size());
		std::valarray<double> spectra = fsources.genSpectra(si, energies);
		for (int j = 0; j < fcounts.getEMin().size(); ++j){
			std::ostringstream os;
			os << "ES" << std::setfill('0') << std::setw(2)<< j << "_" << fsources[si].name;
			//Need to calculate the value of the scalers.  They are all compared to the index and prefactor given in
			//the source catalog
			//Integrate the source spectra
			int lind, uind;
			Utils::findIndexRange(energies, fcounts.getEMin()[j], fcounts.getEMax()[j], lind, uind);
			double flux=0;
			for (int l = lind; l < uind; ++l){
			   double Emin, Emax;
			   //We must handle corner cases when extrapolating
			   if (l == 0) {
			      Emin = fcounts.getEMin()[j];
			   } else {
			      Emin = std::max(fcounts.getEMin()[j], energies[l]);
			   }
			   if (l == energies.size()-2) {
			      Emax = fcounts.getEMax()[j];
			   } else {
			      Emax = std::min(fcounts.getEMax()[j], energies[l+1]);
			   }
			   //Integrate using a power law interpolation
			   double index = log(spectra[l+1]/spectra[l])/log(energies[l+1]/energies[l]);
			   double pref = spectra[l]/pow(energies[l],index);
			   if (fabs(index + 1) > 1e-10) {
			      flux += pref/(index + 1)*(pow(Emax,index+1)-pow(Emin,index+1));
			   } else {
			      flux += pref*(log(Emax/Emin));
			   }
			}

			//Find the scaler by dividing the power law flux with
			//the source flux
			double plflux;
			if ( fabs(fsources[si].index + 1) > 1e-10 ) {
			   plflux = fsources[si].prefactor/(fsources[si].index+1)*fsources[si].pivot*(pow(fcounts.getEMax()[j]/fsources[si].pivot,fsources[si].index+1) - pow(fcounts.getEMin()[j]/fsources[si].pivot,fsources[si].index+1));
			} else {
			   plflux = fsources[si].prefactor*fsources[si].pivot*(log(fcounts.getEMax()[j]/fcounts.getEMin()[j]));
			}
			double scaler = flux/plflux;
			if (isnan(scaler) || isinf(scaler)) std::cerr<<"not a number in scaler for free spectra"<<std::endl;

			//The error is found by simple interpolation of the
			//errors defined in the source, if available
			double unc = 1e-2;
			if (fsources[si].spType == Sources::FREE) {
			   std::valarray<double> senergies(fsources[si].spParameters.size()/2);
			   std::valarray<double> uncPref(fsources[si].spParameters.size()/2);
			   for (int i = 0; i < fsources[si].spParameters.size()/2; ++i){
			      senergies[i] = fsources[si].spParameters[2*i];
			      uncPref[i] = fsources[si].spParametersUnc[2*i+1];
			   }
			   int ilow,iupp;
			   double en = sqrt(fcounts.getEMax()[j]*fcounts.getEMin()[j]);
			   Utils::findIndexRange(senergies, en, en, ilow, iupp);
			   double tmpInd = log(uncPref[iupp]/uncPref[ilow])/log(senergies[iupp]/senergies[ilow]);
			   unc = uncPref[ilow]*pow(en/senergies[ilow], tmpInd);
			}
			fvariables.add(os.str(), scaler, unc);
			fvariables.setLowerLimit(os.str(), 0);
			tmp[j] = scaler;
		}
		SparseSkymap<double> *map = new SparseSkymap<double>(fcounts.getCountsMap().Order(), fcounts.getEMin(), fcounts.getEMax());
//#pragma omp critical (addSkymap)
		fvariableSkymaps[si] = map;
		genCountMap(si, map);
//#pragma omp critical (addPars)
		flastPars[si] = tmp;
	}
	} //Parallel region for skymap generation of fitted sources

}	


/******************************************************************************************/
/******************************************************************************************/
//Destructor
/******************************************************************************************/
SourceModel2::~SourceModel2() {
   //Loop over the map and delete the pointers
   for ( std::map<int, SparseSkymap<double>* >::iterator it = fvariableSkymaps.begin(); it != fvariableSkymaps.end(); ++it){
      delete it->second;
   }
}

/******************************************************************************************/
/******************************************************************************************/
//Convert sources to count maps
/******************************************************************************************/
void SourceModel2::genCountMaps(double lowFlux, SparseSkymap<double> *counts) const{
	int i = 0;
	while (i < fsources.size() && fsources[i].flux100 > lowFlux) i++;
	return genCountMaps(i, counts);
}

void SourceModel2::genCountMaps(int numberOfSources, SparseSkymap<double> *counts) const{
	//Keep numbers within bounds
	if (numberOfSources > fsources.size()){
		numberOfSources = fsources.size();
	}
	//Create an array of sources to generate
	std::valarray<int> srcNumbers(numberOfSources);
	for (int i = 0; i < numberOfSources; ++i){
		srcNumbers[i] = i;
	}
	//Call genCountMaps with the array and get the actual map
	genCountMaps(srcNumbers, counts);
}

void SourceModel2::genCountMaps(const std::valarray<int> &sourceNumbers, SparseSkymap<double> *counts) const{
	//Status indicator
	Utils::StatusIndicator status("Source map", sourceNumbers.size());
#pragma omp parallel for default(shared) schedule(static)
	for (int i = 0; i < int(sourceNumbers.size()); ++i){
		if (sourceNumbers[i] >= 0 && sourceNumbers[i] < fsources.size()){
			genCountMap(sourceNumbers[i], counts);
		}
		status.refresh();
	}
}

void SourceModel2::genCountMap(int sourceNumber, SparseSkymap<double> *counts) const{
	//Get the spectra
   std::valarray<double> eLower, eUpper;
   if ( ! counts->getBoundaries(eLower,eUpper) ) {
      std::cerr<<"Cannot create a source counts map unless it is binned"<<std::endl;
      return;
   }

   //Check that the sparse skymap has the correct boundaries for the counts map
   if ( ! (counts->nSpectra() == fcounts.getCountsMap().nSpectra() && counts->Order() == fcounts.getCountsMap().Order()) ) {
      std::cerr<<"Sparse skymap has to have same size as counts when generating source counts map"<<std::endl;
      std::cerr<<counts->nSpectra()<<" != "<<fcounts.getCountsMap().nSpectra()<<" && "<<counts->Order()<<" != "<<fcounts.getCountsMap().Order()<<std::endl;
      return;
   }

   if (! (std::equal(&eLower[0],&eLower[0]+eLower.size(), &fcounts.getEMin()[0]) &&
	    std::equal(&eUpper[0],&eUpper[0]+eUpper.size(), &fcounts.getEMax()[0]))){
      std::cerr<<"Sparse skymap has to have the same spectra as counts when generating source counts map"<<std::endl;
      return;
   }


	//Return if number out of bounds
	if (sourceNumber < 0 || sourceNumber > fsources.size()) return;
	
	//Get the source
	const Sources::Source &source = fsources[sourceNumber];

	//Get the spectra from the sources class and convert to counts
	SM::Coordinate co(source.l, source.b);
	std::valarray<double> eMin, eMax;
	//Use the energies of the psf to use as a basis for the flux spectra
	//calculation
	std::valarray<double> energies(&fpsf.getEnergies()[0], fpsf.getEnergies().size());
	std::valarray<double> spectra = fsources.genSpectra(sourceNumber, energies);
	spectra = spectraToCounts(spectra, energies, co, true, &eMin, &eMax);

	/*
	//Create a power law counts spectra
	SM::Coordinate co(source.l, source.b);
	std::valarray<double> eMin, eMax;
	std::valarray<double> spectra = powerLawCounts(source.prefactor, source.index, source.pivot, co, true, &eMin, &eMax);

	//Apply the corrections to it according to the spectral type
	for ( int k = 0; k < eMin.size(); ++k) {
   	   //Now we need to make corrections depending on the spectral type
	   double E = (eMin[k]+eMax[k])/2.;
	   switch(source.spType) {
	      case POWERLAWEXPCUTOFF:
		 spectra[k] *= exp(-E/source.spParameters[2]);
		 //Pass through for the index and prefactor
	      case POWERLAW:
		 spectra[k] *= source.spParameters[0]/source.prefactor*pow(E/source.pivot, source.spParameters[1]-source.index);
		 break;
	      case BROKENPOWERLAW:
		 spectra[k] *= source.spParameters[0]/source.prefactor;
		 if (E < source.spParameters[3]){
		    spectra[k] *=pow(E/source.pivot, -source.index) * pow(E/source.spParameters[3], source.spParameters[1]); 
		 }else{
		    spectra[k] *=pow(E/source.pivot, -source.index) * pow(E/source.spParameters[3], source.spParameters[2]); 
		 }
		 break;
	      case FREE:
		 //Create two arrays, one for energies and another for prefactors
		 std::valarray<double> energies(source.spParameters.size()/2);
		 std::valarray<double> prefactors(source.spParameters.size()/2);
		 for (int i = 0; i < source.spParameters.size()/2; ++i) {
		    energies[i] = source.spParameters[2*i];
		    //We should have a minimum of 1e-20 for the prefactors
		    prefactors[i] = std::max(1e-20, source.spParameters[2*i+1]);
		 }
		 //If there is only one pair, use that for all energies
		 if (energies.size() == 1) {
		    spectra[k] *= prefactors[0];
		 } else {
		    //Find the right place in the arrays for interpolation
		    int il = Utils::indexBelow(energies, E);
		    int iu = Utils::indexAbove(energies, E);
		    // The indices must be different
		    if ( il == iu ) {
		       if ( il == 0 ) {
			  iu += 1;
		       } else {
			  il -= 1;
		       }
		    }
		    //Calculate the prefactor, using power law interpolation
		    double tmpInd = log(prefactors[iu]/prefactors[il])/log(energies[iu]/energies[il]);
		    double scale = std::max(fscalerMin, pow(E/energies[il], tmpInd));
		    spectra[k] *= prefactors[il]*scale;
		    if (isnan(spectra[k]) || spectra[k] <= 0) {
		       std::cout<<spectra[k]<<", "<<prefactors[iu]<<", "<<prefactors[il]<<std::endl;
		    }
		 }
		 break;
	   }
	}
	*/

	//Loop over the energy bins and apply the psf
	for ( int j = 0; j< eLower.size(); ++j){
	   //Loop over the finer energy bins from powerLawCounts method
	   int lowIndex = Utils::indexBelow(eMin, eLower[j]);
	   int highIndex = Utils::indexAbove(eMax, eUpper[j]);
	   //If the values are not equal, something is wrong
	   if (! (eMin[lowIndex] == eLower[j] && eMax[highIndex] == eUpper[j]) ){
	      std::cerr<<"Energy boundaries do not fit when creating source counts map"<<std::endl;
	      std::cerr<<eMin[lowIndex]<<" != "<<eLower[j]<<" && "<<eMax[highIndex]<<" != "<<eUpper[j]<<std::endl;
	   }
	   //Loop over the finer bins.  Calculate approximate power law indexes
	   //for more accurate psf handling
	   for ( int k = lowIndex; k <= highIndex; ++k) {
	      // Nothing to be done if spectra is 0
	      if (spectra[k] != 0) { 
		 double index = 0;
		 int count = 0;
		 if (k != 0 && spectra[k-1] != 0) {//We can calculate backward index
		    index += log(spectra[k]/spectra[k-1])/log((eMin[k]+eMax[k])/(eMin[k-1]+eMax[k-1]));
		    count++;
		 }
		 if (k != eMin.size()-1 && spectra[k+1] != 0){ //We can calculate forward index
		    index += log(spectra[k]/spectra[k+1])/log((eMin[k]+eMax[k])/(eMin[k+1]+eMax[k+1]));
		    count++;
		 }
		 if (count != 0) index /= float(count);
		 //We need to limit the index to around 35 until we get error
		 //limits in the sources
		 if (fabs(index) > 10) index = index/fabs(index)*10;
		 //We must remember to multiply with the solid angle of the pixels since
		 //the psf is scaled to be 1 if we integrate over solid angle.
		 //If we are not performing exposure correction, we must
		 //calculate the average value.
		 double sa = (eMax[k] - eMin[k])/(eMax[highIndex]-eMin[lowIndex]);
		 //Debugging check
		 if (isnan(sa) || isnan(spectra[k])) {
		    std::cerr<<"NAN in sourceModel2 "<<sa<<", "<<spectra[k]<<std::endl;
		 }
		 //If we do exposure correction, we multiply with the solid
		 //angle
		 if (fconfigure & EXPOSURE_CORRECT)
		    sa = fcounts.getCountsMap().solidAngle();
		 //Create the psf map, multiply it with the integral and add it to the counts map
		 Healpix_Map<double> psfMap;
		 if (fconfigure & CONVOLVE) {
		    psfMap = fpsf.getEffectiveMap(eMin[k], eMax[k], index, counts->Order(), co.healpixAng(), fpsfFraction);
		    for (int l = 0; l<psfMap.Npix(); ++l){
		       psfMap[l] *= spectra[k]*sa;
		    }
		 } else {
		    psfMap.Set(counts->Order(), RING);
		    psfMap.fill(0);
		    psfMap[psfMap.ang2pix(co.healpixAng())] = spectra[k]*sa/counts->solidAngle();
		 }
#pragma omp critical (addToCounts)
		 counts->addHealpixMap(psfMap, j);
	      }
	   }
	}
}



/******************************************************************************************/
/******************************************************************************************/
//Return the total source maps
/******************************************************************************************/
void SourceModel2::getMap(const Variables &variables, Skymap<double> &map){
	//Create the skymaps for the free sources
	//Make everything in a single parallel construct with nowait
//#pragma omp parallel default(shared)
   {

	//First the prefactors
//#pragma omp for schedule(dynamic) nowait
	for (int i = 0; i < fPrefactorSources.size(); ++i){
		int si = fPrefactorSources[i];
		std::string varName = "Pref_" + fsources[si].name;
		double pref = variables[varName];
		//See if it has changed and rescale the map accordingly
		if (pref != flastPars[si][0]){
			double scale = pref/flastPars[si][0];
			//Loop over all the pixels in the map
			for (SparseSkymap<double>::iterator it = fvariableSkymaps[si]->begin(); it != fvariableSkymaps[si]->end(); ++it){
				(*it).second *= scale;
			}
			flastPars[si][0] = pref;
		}
		//Add the skymap to the total
//#pragma omp critical (addToMap)
		for (SparseSkymap<double>::iterator it = fvariableSkymaps[si]->begin(); it != fvariableSkymaps[si]->end(); ++it){
		   map[it->first.first][it->first.second] += it->second;
		}
	}


	//Then indices
//#pragma omp for schedule(dynamic) nowait
	for (int i = 0; i < fIndexSources.size(); ++i){
		int si = fIndexSources[i];
		std::string varName = "Indx_" + fsources[si].name;
		double index = variables[varName];
		//If the index has changed since last time, we need to fix that
		if (index != flastPars[si][0]){
			double diffInd = index - flastPars[si][0];
			double scale = pow(fsources[si].pivot,-diffInd);
			//Pre calculate the index correction
			std::valarray<double> newScale(fvariableSkymaps[si]->getSpectra().size());
			for (int j = 0; j < newScale.size(); ++j){
				newScale[j] = pow(fvariableSkymaps[si]->getSpectra()[j],diffInd)*scale;
			}
			//Loop over all the pixels in the map
			for (SparseSkymap<double>::iterator it = fvariableSkymaps[si]->begin(); it != fvariableSkymaps[si]->end(); ++it){
				(*it).second *= newScale[(*it).first.second];
			}
			flastPars[si][0] = index;
		}
		//Add the skymap to the total
//#pragma omp critical (addToMap)
		for (SparseSkymap<double>::iterator it = fvariableSkymaps[si]->begin(); it != fvariableSkymaps[si]->end(); ++it){
		   map[it->first.first][it->first.second] += it->second;
		}
	}
	
	
	//Then powerlaws
//#pragma omp for schedule(dynamic) nowait
	for (int i = 0; i < fPowerLawSources.size(); ++i){
		int si = fPowerLawSources[i];
		std::string varName = "Pref_" + fsources[si].name;
		double pref = variables[varName];
		varName = "Indx_" + fsources[si].name;
		double index = variables[varName];

		//If the values are different, store the scale in this array
		std::valarray<double> newScale(1.0, fvariableSkymaps[si]->getSpectra().size());
		bool different = false;

		//First the index
		if (index != flastPars[si][1]){
			different = true;
			double diffInd = index - flastPars[si][1];
			double scale = pow(fsources[si].pivot,-diffInd);
			//Pre calculate the index correction
			for (int j = 0; j < newScale.size(); ++j){
				newScale[j] *= pow(fvariableSkymaps[si]->getSpectra()[j],diffInd)*scale;
			}
			flastPars[si][1] = index;
		}
		//Prefactor
		if (pref != flastPars[si][0]) {
			different = true;
			double scale = pref/flastPars[si][0];
			//Adjust the scale
			for (int j = 0; j < newScale.size(); ++j){
				newScale[j] *= scale;
			}
			flastPars[si][0] = pref;
		}

		//Change the map if it is different
		if (different) {
			//Loop over all the pixels in the map
			for (SparseSkymap<double>::iterator it = fvariableSkymaps[si]->begin(); it != fvariableSkymaps[si]->end(); ++it){
				(*it).second *= newScale[(*it).first.second];
			}
		}

		//Add the skymap to the total
//#pragma omp critical (addToMap)
		for (SparseSkymap<double>::iterator it = fvariableSkymaps[si]->begin(); it != fvariableSkymaps[si]->end(); ++it){
		   map[it->first.first][it->first.second] += it->second;
		}
	}


	//Power laws with exponential cut off
//#pragma omp for schedule(dynamic) nowait
	for (int i = 0; i < fPowerLawExpCutOffSources.size(); ++i){
		int si = fPowerLawExpCutOffSources[i];
		std::string varName = "Pref_" + fsources[si].name;
		double pref = variables[varName];
		varName = "Indx_" + fsources[si].name;
		double index = variables[varName];
		varName = "Ecut_" + fsources[si].name;
		double ecut = variables[varName];

		//If the values are different, store the scale in this array
		std::valarray<double> newScale(1.0, fvariableSkymaps[si]->getSpectra().size());
		bool different = false;

		//First the index
		if (index != flastPars[si][1]){
			different = true;
			double diffInd = index - flastPars[si][1];
			double scale = pow(fsources[si].pivot,-diffInd);
			//Pre calculate the index correction
			for (int j = 0; j < newScale.size(); ++j){
				newScale[j] *= pow(fvariableSkymaps[si]->getSpectra()[j],diffInd)*scale;
			}
			flastPars[si][1] = index;
		}
		//Prefactor
		if (pref != flastPars[si][0]) {
			different = true;
			double scale = pref/flastPars[si][0];
			//Adjust the scale
			for (int j = 0; j < newScale.size(); ++j){
				newScale[j] *= scale;
			}
			flastPars[si][0] = pref;
		}
		//Cut off
		if (ecut != flastPars[si][2]) {
			different = true;
			//Adjust the scale
			for (int j = 0; j < newScale.size(); ++j){
				newScale[j] *= exp(-fvariableSkymaps[si]->getSpectra()[j]/ecut + fvariableSkymaps[si]->getSpectra()[j]/flastPars[si][2]);
			}
			flastPars[si][2] = ecut;
		}

		//Change the map if it is different
		if (different) {
			//Loop over all the pixels in the map
			for (SparseSkymap<double>::iterator it = fvariableSkymaps[si]->begin(); it != fvariableSkymaps[si]->end(); ++it){
				(*it).second *= newScale[(*it).first.second];
			}
		}

		//Add the skymap to the total
//#pragma omp critical (addToMap)
		for (SparseSkymap<double>::iterator it = fvariableSkymaps[si]->begin(); it != fvariableSkymaps[si]->end(); ++it){
		   map[it->first.first][it->first.second] += it->second;
		}
	}


	//Broken power laws 
//#pragma omp for schedule(dynamic) nowait
	for (int i = 0; i < fBrokenPowerLawSources.size(); ++i){
		int si = fBrokenPowerLawSources[i];
		std::string varName = "Pref_" + fsources[si].name;
		double pref = variables[varName];
		varName = "Ind1_" + fsources[si].name;
		double ind1 = variables[varName];
		varName = "Ind2_" + fsources[si].name;
		double ind2 = variables[varName];
		varName = "Ebre_" + fsources[si].name;
		double ebreak = variables[varName];

		//If the values are different, store the scale in this array
		std::valarray<double> newScale(1.0, fvariableSkymaps[si]->getSpectra().size());
		bool different = false;

		//Prefactor
		if (pref != flastPars[si][0]) {
			different = true;
			double scale = pref/flastPars[si][0];
			//Adjust the scale
			for (int j = 0; j < newScale.size(); ++j){
				newScale[j] *= scale;
			}
			flastPars[si][0] = pref;
		}
		//Since it is pretty difficult to decouple the rest, we calculate it all at
		//once
		if (ind1 != flastPars[si][1] || ind2 != flastPars[si][2] || ebreak != flastPars[si][3] ) {
			different = true;
			//Loop over the energies
			for (int j = 0; j < newScale.size(); ++j){
				double E = fvariableSkymaps[si]->getSpectra()[j];
				if ( E <= std::min(ebreak, flastPars[si][3]) ) {
					newScale[j] *= pow(flastPars[si][3], flastPars[si][1])*pow(ebreak, -ind1)*pow(E,ind1-flastPars[si][1]);
				} else if ( ebreak < E && E <= flastPars[si][3] ) {
					newScale[j] *= pow(flastPars[si][3], flastPars[si][1])*pow(ebreak, -ind2)*pow(E,ind2-flastPars[si][1]);
				} else if ( ebreak > E && E >= flastPars[si][3] ) {
					newScale[j] *= pow(flastPars[si][3], flastPars[si][2])*pow(ebreak, -ind1)*pow(E,ind1-flastPars[si][2]);
				} else {
					newScale[j] *= pow(flastPars[si][3], flastPars[si][2])*pow(ebreak, -ind2)*pow(E,ind2-flastPars[si][2]);
				}
			}
			flastPars[si][1] = ind1;
			flastPars[si][2] = ind2;
			flastPars[si][3] = ebreak;
		}

		//Add the skymap to the total
//#pragma omp critical (addToMap)
		for (SparseSkymap<double>::iterator it = fvariableSkymaps[si]->begin(); it != fvariableSkymaps[si]->end(); ++it){
		   map[it->first.first][it->first.second] += it->second;
		}
	}

	
	//Free spectra
//#pragma omp for schedule(dynamic) nowait
	for (int i = 0; i < fFreeSources.size(); ++i){
		int si = fFreeSources[i];

		std::valarray<double> newScale(1.0, fvariableSkymaps[si]->getSpectra().size());

		std::ostringstream os;
		for (int j = 0; j < fcounts.getEMin().size(); ++j){
			os.str("");
			os << "ES" << std::setfill('0') << std::setw(2)<< j << "_" << fsources[si].name;
			newScale[j] = variables[os.str()]/flastPars[si][j];
		}

		//Add the skymap to the total
//#pragma omp critical (addToMap)
		for (SparseSkymap<double>::iterator it = fvariableSkymaps[si]->begin(); it != fvariableSkymaps[si]->end(); ++it){
		   map[it->first.first][it->first.second] += it->second*newScale[(*it).first.second];
		}
	}


	//Add the fixed sources
//#pragma omp critical (addToMap)
	if (fFixedSources.size() != 0) {
		if (! ffixedScalerVar.empty()) {
		   double mapScale = variables[ffixedScalerVar];
		   for (int co = 0; co < map.Npix(); ++co){
		      for (int i = 0; i < map.nSpectra(); ++i){
			 map[co][i] += ffixedSkymap[co][i]*mapScale;
		      }
		   }
		} else {
			map += ffixedSkymap;
		}
	}
   } //End parallel 
}


BaseModel::gradMap SourceModel2::getComponents(const Variables &variables, const std::string &prefix){
   gradMap output;
   output["Sources"] = Skymap<double>(fcounts.getCountsMap().Order(), fcounts.getEMin(), fcounts.getEMax(), RING);
   getMap(variables, output["Sources"]);
   return output;
}


/******************************************************************************************/
/******************************************************************************************/
//Calculate the gradient maps
/******************************************************************************************/
void SourceModel2::getGrads(const Variables &variables, const std::string & varName, Skymap<double> &map){
	//Find the source index and type
	varType type;
	int si;
	getIndexAndType(varName, si, type);
	//Nothing to be done if the type is unknown
	if ( type != UNKN ){
		//The scale for the map is stored in this array
		std::valarray<double> newScale(1.0, fcounts.getEMin().size());

		//Switch the type
		switch (type) {
			//The gradient for the prefactor
			case PREF:
				{ //Needed to change the scope of variables
				//Divide by the prefactor value for the derivative
				//We use its last value, since we haven't corrected for the first
				newScale /= flastPars[si][0];
				
				//Depending on the type of the source, we must add corrections if the
				//variables have changed
				switch (fsources[si].fitType) {
				   case Sources::POWERLAWEXPCUTOFF:
						{//Needed for the variables
						//If the cutoff energy has changed, correct for that
						std::string cutVar = "Ecut_" + fsources[si].name;
						double ecut = variables[cutVar];
						if (ecut != flastPars[si][2]) {
							//Adjust the scale
							for (int j = 0; j < newScale.size(); ++j){
								newScale[j] *= exp(-fvariableSkymaps[si]->getSpectra()[j]/ecut + fvariableSkymaps[si]->getSpectra()[j]/flastPars[si][2]);
							}
						}
						}
						//Pass through for the index
					case Sources::POWERLAW:
						//If the index has changed, we must correct for that
						std::string indVar = "Indx_" + fsources[si].name;
						double index = variables[indVar];
						if (index != flastPars[si][1]){
							double diffInd = index - flastPars[si][1];
							double scale = pow(fsources[si].pivot, -diffInd);
							for (int j = 0; j < newScale.size(); ++j){
								newScale[j] *= pow(fvariableSkymaps[si]->getSpectra()[j],diffInd)*scale;
							}
						}
						break;
				}
				}
				break;

			//We are calculating the gradient for the index
			case INDX:
				{//Change the scope of the variables
				//Here we must multiply with the log of energy
				for (int j = 0; j < newScale.size(); ++j) {
					newScale[j] *= log(fvariableSkymaps[si]->getSpectra()[j]/fsources[si].pivot);
				}
				
				//If the index has changed, make corrections
				double lastIndex = flastPars[si][0];
				if (fsources[si].fitType != Sources::INDEX) { //It is different if we only change the index
					lastIndex = flastPars[si][1];
				}
				//If the index has changed, we must correct for that
				std::string indVar = "Indx_" + fsources[si].name;
				double index = variables[indVar];
				if (index != lastIndex){
					double diffInd = index - lastIndex;
					double scale = pow(fsources[si].pivot, -diffInd);
					for (int j = 0; j < newScale.size(); ++j){
						newScale[j] *= pow(fvariableSkymaps[si]->getSpectra()[j],diffInd)*scale;
					}
				}

				//The corrections depend on the spectral type
				switch (fsources[si].fitType) {
					case Sources::POWERLAWEXPCUTOFF:
					{//Change scope
						//If the cutoff energy has changed, correct for that
						std::string cutVar = "Ecut_" + fsources[si].name;
						double ecut = variables[cutVar];
						if (ecut != flastPars[si][2]) {
							//Adjust the scale
							for (int j = 0; j < newScale.size(); ++j){
								newScale[j] *= exp(-fvariableSkymaps[si]->getSpectra()[j]/ecut + fvariableSkymaps[si]->getSpectra()[j]/flastPars[si][2]);
							}
						}
					}
						//Pass through for the prefactor
					case Sources::POWERLAW:
						//If the prefactor has changed, we must correct
						std::string preVar = "Pref_" + fsources[si].name;
						double pref = variables[preVar];
						if (pref != flastPars[si][0]){
							newScale *= pref/flastPars[si][0];
						}
						break;
				}
				}
				break;

			//Gradient for the cut energy
			case ECUT:
				{
				//Multiply by the energy and divide with the cut energy squared
				std::string ecutVar = "Ecut_" + fsources[si].name;
				double ecut = variables[ecutVar];
				for (int j = 0; j < newScale.size(); ++j) {
					newScale[j] *= fvariableSkymaps[si]->getSpectra()[j]/(ecut*ecut);
				}

				//Do the corrections if necessary
				if (ecut != flastPars[si][2]){
					//Adjust the scale
					for (int j = 0; j < newScale.size(); ++j){
						newScale[j] *= exp(-fvariableSkymaps[si]->getSpectra()[j]/ecut + fvariableSkymaps[si]->getSpectra()[j]/flastPars[si][2]);
					}
				}

				//If the prefactor has changed, we must correct
				std::string preVar = "Pref_" + fsources[si].name;
				double pref = variables[preVar];
				if (pref != flastPars[si][0]){
					newScale *= pref/flastPars[si][0];
				}

				//If the index has changed, we must correct for that
				std::string indVar = "Indx_" + fsources[si].name;
				double index = variables[indVar];
				if (index != flastPars[si][1]){
					double diffInd = index - flastPars[si][1];
					double scale = pow(fsources[si].pivot, -diffInd);
					for (int j = 0; j < newScale.size(); ++j){
						newScale[j] *= pow(fvariableSkymaps[si]->getSpectra()[j],diffInd)*scale;
					}
				}

				}
				break;

			//Gradient for the free scaling spectra
			case SCAL:
				{
				//We must find the index of the variable
				std::istringstream iss(varName.substr(2,2));
				int ei;
				iss>>ei;

				//Set all the scaling to 0, unless the corresponding index
				for (int j = 0; j < newScale.size(); ++j){
					if (j != ei) {
						newScale[j] = 0;
					}else {
						newScale[j] /= flastPars[si][j];
					}
				}
				}
				break;

			//Rest is for broken power laws.  Will maybe be implemented later
			//! \todo Implement broken powerlaw derivatives
			case EBRE:
				std::cerr<<"WARNING: ANALYTICAL GRADIENT NOT IMPLEMENTED FOR BROKEN POWER LAWS.\nUSE NUMERICAL GRADIENT OR SIMPLY FREE SPECTRAL FITS."<<std::endl;
				break;
			case IND1:
				break;
			case IND2:
				break;
		}
		//Create the output map by scaling
		for (SparseSkymap<double>::iterator it = fvariableSkymaps[si]->begin(); it != fvariableSkymaps[si]->end(); ++it){
			map[(*it).first.first][(*it).first.second] += (*it).second*newScale[(*it).first.second];
		}
	}else if (varName == ffixedScalerVar) {
		map += ffixedSkymap;
	}

}

void SourceModel2::addToFixedSources(const Skymap<double> &map) {
   //If the fixed skymap is not initialized, do it now.  Clear the ffixedScalerVar since it has not been initialized
   if (ffixedSkymap.Order() == 0) {
      ffixedSkymap.Resize(fcounts.getCountsMap().Order(), fcounts.getEMin(), fcounts.getEMax(), RING);
      fFixedSources.push_back(-1);
      ffixedScalerVar = "";
   }
   ffixedSkymap += map;
}

BaseModel::gradMap SourceModel2::getSecDer(const Variables & vars, const std::string & varName1, const std::string & varName2){
	gradMap output;
	//! \todo Implement the second derivative for SouceModel2
	return output;
}

void SourceModel2::modifySources(const Variables & variables, Sources &sources) const {
	//Loop through all the fitted sources and set the spectral types in the
	//sources.
	//First the prefactors
	for (int i = 0; i < fPrefactorSources.size(); ++i){
		int si = fPrefactorSources[i];
		std::string varName = "Pref_" + fsources[si].name;
		double pref = variables[varName];
		sources[si].spParameters[0] = pref;
		double prefUnc = variables.error(varName);
		sources[si].spParametersUnc[0] = prefUnc;
	}

	//Then indices
	for (int i = 0; i < fIndexSources.size(); ++i){
		int si = fIndexSources[i];
		std::string varName = "Indx_" + fsources[si].name;
		double index = variables[varName];
		sources[si].spParameters[1] = index;
		double indexUnc = variables.error(varName);
		sources[si].spParametersUnc[1] = indexUnc;
	}
	
	//Then powerlaws
	for (int i = 0; i < fPowerLawSources.size(); ++i){
		int si = fPowerLawSources[i];
		std::string varName = "Pref_" + fsources[si].name;
		double pref = variables[varName];
		double prefUnc = variables.error(varName);
		varName = "Indx_" + fsources[si].name;
		double index = variables[varName];
		double indexUnc = variables.error(varName);
		sources[si].spParameters[0] = pref;
		sources[si].spParametersUnc[0] = prefUnc;
		sources[si].spParameters[1] = index;
		sources[si].spParametersUnc[1] = indexUnc;
	}


	//Power laws with exponential cut off
	for (int i = 0; i < fPowerLawExpCutOffSources.size(); ++i){
		int si = fPowerLawExpCutOffSources[i];
		std::string varName = "Pref_" + fsources[si].name;
		double pref = variables[varName];
		double prefUnc = variables.error(varName);
		varName = "Indx_" + fsources[si].name;
		double index = variables[varName];
		double indexUnc = variables.error(varName);
		varName = "Ecut_" + fsources[si].name;
		double ecut = variables[varName];
		double ecutUnc = variables.error(varName);

		//We must change the spectral type to power law with exponential cut off
		sources[si].spType = Sources::POWERLAWEXPCUTOFF;
		sources[si].spParameters.resize(3);
		sources[si].spParametersUnc.resize(3);
		sources[si].spParameters[0] = pref;
		sources[si].spParametersUnc[0] = prefUnc;
		sources[si].spParameters[1] = index;
		sources[si].spParametersUnc[1] = indexUnc;
		sources[si].spParameters[2] = ecut;
		sources[si].spParametersUnc[2] = ecutUnc;
	}

	//Broken power laws 
	for (int i = 0; i < fBrokenPowerLawSources.size(); ++i){
		int si = fBrokenPowerLawSources[i];
		std::string varName = "Pref_" + fsources[si].name;
		double pref = variables[varName];
		double prefUnc = variables.error(varName);
		varName = "Ind1_" + fsources[si].name;
		double ind1 = variables[varName];
		double ind1Unc = variables.error(varName);
		varName = "Ind2_" + fsources[si].name;
		double ind2 = variables[varName];
		double ind2Unc = variables.error(varName);
		varName = "Ebre_" + fsources[si].name;
		double ebreak = variables[varName];
		double ebreakUnc = variables.error(varName);

		//We must change the spectral type to a broken power law
		sources[si].spType = Sources::BROKENPOWERLAW;
		sources[si].spParameters.resize(4);
		sources[si].spParametersUnc.resize(4);
		sources[si].spParameters[0] = pref;
		sources[si].spParametersUnc[0] = prefUnc;
		sources[si].spParameters[1] = ind1;
		sources[si].spParametersUnc[1] = ind1Unc;
		sources[si].spParameters[2] = ind2;
		sources[si].spParametersUnc[2] = ind2Unc;
		sources[si].spParameters[3] = ebreak;
		sources[si].spParametersUnc[3] = ebreakUnc;
	}

	//Free spectra
	for (int i = 0; i < fFreeSources.size(); ++i){
		int si = fFreeSources[i];

		//Change the spectral type and resize the array
		sources[si].spType = Sources::FREE;
		sources[si].spParameters.resize(fcounts.getEMin().size()*2);
		sources[si].spParametersUnc.resize(fcounts.getEMin().size()*2);
		std::valarray<double> flux(fcounts.getEMin().size()), flrelerr(fcounts.getEMin().size());

		std::ostringstream os;
		for (int j = 0; j < fcounts.getEMin().size(); ++j){
			sources[si].spParameters[2*j] = sqrt(fcounts.getEMax()[j]*fcounts.getEMin()[j]);
			sources[si].spParametersUnc[2*j] = 0.5*(fcounts.getEMax()[j]-fcounts.getEMin()[j]);
			os.str("");
			os << "ES" << std::setfill('0') << std::setw(2)<< j << "_" << sources[si].name;
			double value = variables[os.str()];
			double error = variables.error(os.str());
			//Calculate the power law flux, apply the value to it
			//and store in the flux array for later use to
			//calculate the intensities
			double plflux;
			if ( fabs(fsources[si].index + 1) > 1e-10 ) {
			   plflux = fsources[si].prefactor/(fsources[si].index+1)*fsources[si].pivot*(pow(fcounts.getEMax()[j]/fsources[si].pivot,fsources[si].index+1) - pow(fcounts.getEMin()[j]/fsources[si].pivot,fsources[si].index+1));
			} else {
			   plflux = fsources[si].prefactor*fsources[si].pivot*(log(fcounts.getEMax()[j]/fcounts.getEMin()[j]));
			}
			value = (isinf(value) || isnan(value)) ? 1 : value;
			flux[j] = plflux*value;
			if (value < 0 || flux[j] < 0) {
			   std::cout<<"Something is negative in SourceModel: "<<value<<", "<<flux[j]<<std::endl;
			}
			flrelerr[j] = (isinf(error) || isnan(error) || value == 0) ? 1e-2 : error/value;
		}
		//Calculate the intensities
		std::valarray<double> intens = Sources::flux2int(fcounts.getEMin(), fcounts.getEMax(), flux);
		//std::valarray<double> intens = flux/sqrt(fcounts.getEMin()*fcounts.getEMax());
		for (int j = 0; j < intens.size(); ++j){
		   sources[si].spParameters[2*j+1] = intens[j]/fsources[si].prefactor/pow(sources[si].spParameters[2*j]/fsources[si].pivot,fsources[si].index);
		   sources[si].spParametersUnc[2*j+1] = std::max(sources[si].spParameters[2*j+1]*flrelerr[j],1e-4);
		}
	}
}

void SourceModel2::getIndexAndType(const std::string & varName, int &index, varType &type) const {
	//Get the first five letters of the varName
	std::string pre = varName.substr(0,5);

	//Now we can find the type
	type = UNKN;
	if (pre == "Pref_") {
		type = PREF;
	} else if (pre == "Indx_") {
		type = INDX;
	} else if (pre == "Ecut_") {
		type = ECUT;
	} else if (pre == "Ebre_") {
		type = EBRE;
	} else if (pre == "Ind1_") {
		type = IND1;
	} else if (pre == "Ind2_") {
		type = IND2;
	} else if (varName.substr(0,2) == "ES"  && varName[4] == '_') {
		std::string numb = "0123456789";
		if ( numb.find(varName[2]) != std::string::npos && numb.find(varName[3]) != std::string::npos){
			type = SCAL;
		}
	}

	//If type is not unknown, we can get the source name and the id
	index = -1;
	if (type != UNKN){
		index = fsources.sourceNumber(varName.substr(5,varName.size()-5));
		//Now we check that this variable exist in this model
		if (flastPars.find(index) == flastPars.end()) {
			type = UNKN;
			index = -1;
		}
	}
}
