
//**.****|****.****|****.****|****.****|****.****|****.****|****.****|****.****|
// * Galplotdef.h *                                    galprop package
//**"****!****"****!****"****!****"****!****"****!****"****!****"****!****"****|

#ifndef Galplot_h
#define Galplot_h

using namespace std;//AWS20051124
#include<cstdio>    //AWS20051124
#include<string>    //AWS20051124
#include<fstream>   //AWS20051124

class Galplotdef
{
 public:

  // from galprop
   char version[10];    // galprop version number
   char run_no [100];    // identifier of galprop run AWS20100326
   char galdef_ID[100]; // full identifier e.g. 01_123456

   /* --- not needed, uses Galdef.h : to be removed
   int n_spatial_dimensions;             // 1,2 or 3
   double z_min,z_max,dz;                // for 1,2,3D    
   double r_min,r_max,dr;                // for 2D 
   double x_min,x_max,dx,y_min,y_max,dy; // for 3D 
   double p_min,p_max,p_factor;          // momentum start, end, factor
   double Ekin_min,Ekin_max,Ekin_factor; // kinetic energy per nucleon start, end, factor

   char p_Ekin_grid[5];                    // "p"||"Ekin": construct grid in p or Ekin

   ---------- end of not needed */

   char *psfile_tag;                              // tag to be included in postscript file name
   int screen_output;                             // 0=no screen output 1=screen output
   int output_format;                             // 0=none 1=ps 2=gif 3=both


   double E_gamma_min,E_gamma_max,E_gamma_factor; // gamma-ray energy (MeV) start, end, factor
   double long_min,long_max;                      // gamma-ray skymap longitude min,max (degrees)
   double  lat_min, lat_max;                      // gamma-ray skymap latitude  min,max (degrees)
   double d_long,d_lat;                           // gamma-ray skymap longitude,latitude binsize (degrees)       

   double nu_synch_min,nu_synch_max,nu_synch_factor;// synchrotron frequency min,max (Hz), factor

 
   int verbose;                          // verbosity: 0=min 10=max 
   int test_suite;                       // run test suit instead of normal run 

  

   int gamma_spectrum, gamma_long_profile, gamma_lat_profile;
   double long_min1,long_max1;                      // gamma-ray skymap longitude min,max (degrees)
   double long_min2,long_max2;                      // gamma-ray skymap longitude min,max (degrees)
   double long_binsize;                             // gamma-ray skymap longitude binsize (degrees) 
   int    long_log_scale;                           // gamma-ray longitude profile scale 0=linear 1=log  AWS20050722
   double  lat_min1, lat_max1;                      // gamma-ray skymap latitude  min,max (degrees)
   double  lat_min2, lat_max2;                      // gamma-ray skymap latitude  min,max (degrees)
   double  lat_binsize;                             // gamma-ray skymap latitude  binsize (degrees)                          
   int     lat_log_scale;                           // gamma-ray latitude profile scale 0=linear 1=log

   double gamma_spectrum_Emin;                      // minimum energy        on spectrum plot
   double gamma_spectrum_Emax;                      // maximum energy        on spectrum plot
   double gamma_spectrum_Imin;                      // minimum intensity*E^2 on spectrum plot
   double gamma_spectrum_Imax;                      // maximum intensity*E^2 on spectrum plot

   int    gamma_IC_selectcomp;                      // select IC components 0=no selection, 1=optical, 2=FIR, 3=CMB   AWS20060310

   int    sources_EGRET;                            // 0=sources removed, 1=sources in

   int    energies_EGRET;                           // number of energy ranges for EGRET                   AWS20031021
   int    convolve_EGRET;                           // 0=no convolution, 1,2,3=convolve, different normalizations
   int    convolve_EGRET_iEmax;                     // maximum EGRET energy range to convolve (starting 1) AWS20031017
   double convolve_EGRET_thmax;                     // maximum angle for convolution (deg)

   double error_bar_EGRET;                          // EGRET error bar, as fraction
   int    spectrum_style_EGRET;                     // 1=segments, 2=at mean energy 3=both
   double spectrum_index_EGRET;                     // spectral index to plot EGRET data: 0.0=automatic, otherwise value e.g. 2.0 -> E^-2.0  AWS20050916
   int    spectrum_50GeV_EGRET;                     // 1= plot EGRET 10-50 GeV spectrum data points       
                   
   int    spectrum_cut_total;                       // 1= cut total prediction outside EGRET energy range  AWS20040427

   int isotropic_use;       //    1=ignore isotropic, 2=add to prediction, 3=subtract from data
   int isotropic_type;      //    1=power law, 2=explicit EGRET ranges

   double isotropic_const;                          // isotropic background = isotropic_const * E^-isotropic_g
   double isotropic_g    ;                          //                        cm^-2 sr^-1 MeV^-1 s^-1

   int    n_E_EGRET;                                // number of isotropic background values; set in Galplotdef.cc
   double *isotropic_EGRET;                         // isotropic background in EGRET ranges, cm^-2  s^-1

   int isotropic_sree_plot;                         //1=plot Sreekumar et al. isotropic background separately

   int    convolve_GLAST;                           // 0=no convolution, 1=convolve                  AWS20080515
   int    energy_disp_FermiLAT;                     // 0=no energy dispersion, 1=energy dispersion   AWS20140825
   double energy_disp_factor;                       // factor for energy dispersion interpolation    AWS20140829

   int fit_EGRET            ;  //  1=fit EGRET data
   int fit_nrebin_EGRET     ;  //  number of 0.5*0.5 bins for EGRET fitting rebinning
   int fit_counts_min_EGRET ;  //  minimum counts for EGRET fitting after rebinning
   int fit_function_EGRET   ;  //  1=background only, 2=scaling+background 3=both
   int fit_options_EGRET    ;  //  other options
 

   int data_EGRET           ; //   plot EGRET    data

   int data_GLAST           ; //   plot GLAST    data                                       AWS20080507
   char    *GLAST_counts_file;  //      GLAST    counts healpix data file                   AWS20080819
   char    *GLAST_exposure_file;//      GLAST  exposure healpix data file                   AWS20080819
   char    *GLAST_psf_file;     //      GLAST  psf              data file                   AWS20080819

   char    *Fermi_edisp_file;            //    Fermi energy dispersion parameter file              AWS20150226
   char    *Fermi_edisp_file_type;       //    Fermi energy dispersion parameter file type         AWS20150226 
   int      Fermi_edisp_use_Aeff;        //    Fermi energy dispersion use Aeff factor             AWS20150226
   double   Fermi_edisp_E_true_threshold;//    Fermi energy dispersion true     energy threshold   AWS20150226 
   double   Fermi_edisp_E_meas_threshold;//    Fermi energy dispersion measured energy threshold   AWS20150226 
          
   char    *isotropic_bgd_file; //        isotropic background  data file                   AWS20081119
   char    *Fermi_cat_file    ; //        Fermi catalogue FITS file                         AWS20081215
   char    *Fermi_cat_file2   ; //        Fermi catalogue FITS file                         AWS20140114
   char    *Fermi_cat_file3   ; //        Fermi catalogue FITS file                         AWS20140115

   char    *Fermi_sensitivity_file; //    Fermi sensitivity skymap file AWS20170112

   int data_COMPTEL         ; //   plot COMPTEL  data
   int data_OSSE            ; //   plot OSSE     data
   int data_RXTE            ; //   plot RXTE     data
   int data_INTEGRAL        ; //   plot INTEGRAL/SPI data
   int*data_INTEGRAL_comp   ; //        INTEGRAL/SPI range of fitted components to plot
   int    data_INTEGRAL_mode; //        INTEGRAL/SPI 1=plot data 2=fit 3=both
   double data_INTEGRAL_syserr;//       INTEGRAL/SPI systematic error                       AWS20050412   
   double data_INTEGRAL_sysemn;//       INTEGRAL/SPI systematic error min. energy to apply  AWS20050412   
   int data_IBIS            ; //   plot IBIS     data                                       AWS20041018
   int data_MILAGRO         ; //   plot MILAGRO  data
   int data_HEGRA           ; //   plot HEGRA    data
   int data_WHIPPLE         ; //   plot WHIPPLE  data
   int data_GINGA           ; //   plot GINGA    data
   int data_Chandra         ; //   plot Chandra  data                                       AWS20050909

   int   spiskymax_profile ;  //   plot SPI spiskymax profile                               AWS20050722                      
   char *spiskymax_image_ID;  //   spiskymax_image.xxxx.fits: identify spiskymax image      AWS20050722
   int   spiskymax_iteration; //   spiskymax iteration selection                            AWS20050722


   int model_ridge          ; //   plot ridge    model

   int    sourcepop1_verbose ;  //  source population parameters                            AWS20050911
   int    sourcepop1_print_ctl; //  control output of sources to cout                       AWS20200721
   double sourcepop1_density0 ;
   double sourcepop1_oversample;//  source oversampling to avoid statistical fluctuations   AWS20060109
   double sourcepop1_L_min    ;
   double sourcepop1_L_max    ;
   double sourcepop1_alpha_L  ;
   double sourcepop1_fluxlimit;
   double sourcepop1_alpha_R  ;
   double sourcepop1_beta_R   ;   
   double sourcepop1_zscale   ;
   double sourcepop1_alpha_z  ;

   int    sourcepop1_spectral_model;    //                                                  AWS20100824

   double sourcepop1_spectrumg ;
   double sourcepop1_spect_g_0 ;//                                                          AWS20051219
   double sourcepop1_spect_br0 ;
   double sourcepop1_spect_g_1 ;
   double sourcepop1_spect_br1 ;
   double sourcepop1_spect_g_2 ;
   double sourcepop1_E_ref_low ;//                                                          AWS20060109
   double sourcepop1_E_ref_high;//                                                          AWS20060109                                                 

   double sourcepop1_spectrum_g_0_sigma ;//       spectrum with two breaks: Gaussian distribution AWS20100824
   double sourcepop1_spectrum_br0_sigma ;
   double sourcepop1_spectrum_g_1_sigma ;
   double sourcepop1_spectrum_br1_sigma ;
   double sourcepop1_spectrum_g_2_sigma ;

   double sourcepop1_specnormE;     
      
   char  *sourcepop_SPI_cat;   //                                                           AWS20060111

   int    sourcepop_total;     // plot intensity of sources total
   int    sourcepop_sublimit;  // plot intensity of sources below limit
   int    sourcepop_soplimit;  // plot intensity of sources above limit

   int    sourcepop_ext_model; // external model



   int    gcr_spectra          ;
   double gcr_spectra_r_min    ;         
   double gcr_spectra_r_max    ;        
   double gcr_spectra_z_min    ;          
   double gcr_spectra_z_max    ;   

   double gcr_spectra_Emin;                      // minimum energy        on spectrum plot
   double gcr_spectra_Emax;                      // maximum energy        on spectrum plot
   double gcr_spectra_Imin;                      // minimum intensity*E^2 on spectrum plot
   double gcr_spectra_Imax;                      // maximum intensity*E^2 on spectrum plot

   int   gcr_spectra_n_ZA      ;                 // implicit in input string, not read     
   int  *gcr_spectra_Z         ;           
   int  *gcr_spectra_A         ;    
     
   int   gcr_spectra_n_ratio   ;                 // number of sec/prim ratios
   int  *gcr_spectra_n_sec_ZA  ;                 // numbers of Z,A in secondary lists (implicit, not read)
   int  *gcr_spectra_n_pri_ZA  ;                 // numbers of Z,A in primary   lists (implicit, not read)
   int **gcr_spectra_sec_Z     ;                 // lists of sec Z for sec/prim ratios
   int **gcr_spectra_sec_A     ;                 // lists of sec A for sec/prim ratios
   int **gcr_spectra_pri_Z     ;                 // lists of pri Z for sec/prim ratios
   int **gcr_spectra_pri_A     ;                 // lists of pri A for sec/prim ratios
double  *gcr_spectra_ratio_min ;                 // minimum  value for sec/prim ratios
double  *gcr_spectra_ratio_max ;                 // maximum  value for sec/prim ratios

   int   gcr_spectra_n_mod_phi ;                 // number of modulation phi (implicit, not read)
double  *gcr_spectra_mod_phi  ;                  // list   of modulation phi (MV)

 char   *gcr_database_file    ;                  // filename for database file                             AWS20090220

 double *sync_index_nu         ;                 // frequencies  (MHz) in pairs  for synchrotron index     AWS20070523
 int     sync_index_n_nu       ;                 // number of             pairs  for synchrotron index     AWS20070523

 int     sync_data____10MHz    ;                 // plot synchrotron data at this frequency                AWS20071221
 int     sync_data____22MHz    ;                 // plot synchrotron data at this frequency                AWS20071221
 int     sync_data____45MHz    ;                 // plot synchrotron data at this frequency                AWS20071221
 int     sync_data___150MHz    ;                 // plot synchrotron data at this frequency                AWS20071221
 int     sync_data___408MHz    ;                 // plot synchrotron data at this frequency                AWS20071221
 int     sync_data___820MHz    ;                 // plot synchrotron data at this frequency                AWS20071221
 int     sync_data__1420MHz    ;                 // plot synchrotron data at this frequency                AWS20071221
 int     sync_data__2326MHz    ;                 // plot synchrotron data at this frequency                AWS20071221
 int     sync_data_22800MHz    ;                 // plot synchrotron data at this frequency                AWS20071221
 int     sync_data_33000MHz    ;                 // plot synchrotron data at this frequency                AWS20071221
 int     sync_data_41000MHz    ;                 // plot synchrotron data at this frequency                AWS20071221
 int     sync_data_61000MHz    ;                 // plot synchrotron data at this frequency                AWS20071221
 int     sync_data_94000MHz    ;                 // plot synchrotron data at this frequency                AWS20071221

 int     sync_data_WMAP        ;                 // select WMAP data                                       AWS20071221
 int     sync_data_options     ;                 // more  synch data options                               AWS20120110

 int     free_free_options     ;                 // 1=model                        2=WMAP MEM dust template AWS20111222
 int     spin_dust_options     ;                 // 1=WMAP MCMC dust+spinning dust 2=WMAP MEM dust template AWS20120117

 int     galdef_series         ;                 // to plot several runs in plot_luminosity_multiple_galdef AWS20100329

 int     luminosity            ;                 // 1=compute luminosity                                   AWS20100823

 double  ISRF_luminosity_R     ;                 // Galactocentric radius for ISRF luminosity calculation  AWS20100421
 double  ISRF_luminosity_z     ;                 // Galactocentric height for ISRF luminosity calculation  AWS20100421

 int     ISRF_read             ;                 // read ISRF                                              AWS20100818

 
//interface functions prototypes
   int read(char *version_,char  *run_no_,char *galdef_directory);
   int read_galdef_parameter(char *filename,char *parstring,double *value);
   int read_galdef_parameter(char *filename,char *parstring,int    *value);
   int read_galdef_parameter(char *filename,char *parstring,char *value);
   void print();
};

#endif










