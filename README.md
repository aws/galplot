# galplot

Plotting routines for galprop cosmic-ray and gamma-ray output

Moved to gitlab on 2019-12-21 by AWS  from propagate/c/galplot using galplot_20191218bis.tar

Details of usage in README
