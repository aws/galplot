# PAO_WITH_GSL
#   If GSL found, provide substitution variable
#     GSL_HOME
#     GSL_CFLAGS
#     GSL_LIBS
#
AC_DEFUN([PAO_WITH_GSL],
[_PAO_WITH_PACKAGE([gsl],
  [AC_HELP_STRING([--with-gsl],
                  [use gsl (default: ARG or GSL_HOME or search)])],
  [/usr /usr/local $GSL_HOME],
  [include/gsl/gsl_types.h])
if test x$pao_cv_root_gsl != xno
then
  GSL_HOME=$pao_cv_root_gsl
  AC_SUBST(GSL_HOME)
  GSL_CPPFLAGS=-I$GSL_HOME/include
  AC_SUBST(GSL_CPPFLAGS)
  GSL_LIBS="-L$GSL_HOME/lib -lgsl -lgslcblas -lm"
  AC_SUBST(GSL_LIBS)
  AC_DEFINE([HAVE_GSL], 1)
else
  PAO_MISSING([GSL])
fi
AH_TEMPLATE([HAVE_GSL], [Define to 1 if gsl is available on the system])
])

# PAO_REQUIRE_CFITSIO
#   Require that CFITSIO is installed, if not, configuration fails
#   (No version checking, could test features...)
#
AC_DEFUN([PAO_REQUIRE_GSL],
[AC_REQUIRE([PAO_WITH_GSL])
if test x$pao_cv_root_gsl = xno
then
  PAO_MISSING_REQUIRED([GSL])
fi])

# PAO_WITH_CFITSIO
#   If CFITSIO found, provide substitution variable
#     CFITSIO_HOME
#     CFITSIO_CPPFLAGS
#     CFITSIO_LIBS
#
AC_DEFUN([PAO_WITH_CFITSIO],
[_PAO_WITH_PACKAGE([cfitsio],
  [AC_HELP_STRING([--with-cfitsio],
                  [use cfitsio (default: ARG or CFITSIO_HOME or search)])],
  [/usr /usr/local $CFITSIO_HOME],
  [include/fitsio.h])
if test x$pao_cv_root_cfitsio != xno
then
  CFITSIO_HOME=$pao_cv_root_cfitsio
  AC_SUBST(CFITSIO_HOME)
  CFITSIO_CPPFLAGS=-I$CFITSIO_HOME/include
  AC_SUBST(CFITSIO_CPPFLAGS)
  CFITSIO_LIBS="-L$CFITSIO_HOME/lib -lcfitsio"
  AC_SUBST(CFITSIO_LIBS)
  AC_DEFINE([HAVE_CFITSIO], 1)
else
  PAO_MISSING([CFITSIO])
fi
AH_TEMPLATE([HAVE_CFITSIO], [Define to 1 if cfitsio is available on the system])
])

# PAO_REQUIRE_CFITSIO
#   Require that CFITSIO is installed, if not, configuration fails
#   (No version checking, could test features...)
#
AC_DEFUN([PAO_REQUIRE_CFITSIO],
[AC_REQUIRE([PAO_WITH_CFITSIO])
if test x$pao_cv_root_cfitsio = xno
then
  PAO_MISSING_REQUIRED([CFITSIO])
fi])




# PAO_WITH_CCFITS
#   If CCFITS found, provide substitution variable
#     CCFITS_HOME
#     CCFITS_CPPFLAGS
#     CCFITS_LIBS
#
AC_DEFUN([PAO_WITH_CCFITS],
[_PAO_WITH_PACKAGE([ccfits],
  [AC_HELP_STRING([--with-ccfits],
                  [use ccfits (default: ARG or CCFITS_HOME or search)])],
  [/usr /usr/local $CCFITS_HOME],
  [include/CCfits/CCfits])
if test x$pao_cv_root_ccfits != xno
then
  CCFITS_HOME=$pao_cv_root_ccfits
  AC_SUBST(CCFITS_HOME)
  CCFITS_CPPFLAGS=-I$CCFITS_HOME/include
  AC_SUBST(CCFITS_CPPFLAGS)
  CCFITS_LIBS="-L$CCFITS_HOME/lib -lCCfits"
  AC_SUBST(CCFITS_LIBS)
  AC_DEFINE([HAVE_CCFITS], 1)
else
  PAO_MISSING([CCFITS])
fi
AH_TEMPLATE([HAVE_CCFITS], [Define to 1 if ccfits is available on the system])
])

# PAO_REQUIRE_CCFITS
#   Require that CCFITS is installed, if not, configuration fails
#   (No version checking, could test features...)
#
AC_DEFUN([PAO_REQUIRE_CCFITS],
[AC_REQUIRE([PAO_WITH_CCFITS])
if test x$pao_cv_root_ccfits = xno
then
  PAO_MISSING_REQUIRED([CCFITS])
fi])




# PAO_WITH_HEALPIX
#   If HEALPIX found, provide substitution variable
#     HEALPIX_HOME
#     HEALPIX_CPPFLAGS
#     HEALPIX_LIBS
#
AC_DEFUN([PAO_WITH_HEALPIX],
[_PAO_WITH_PACKAGE([healpix],
  [AC_HELP_STRING([--with-healpix],
                  [use healpix (default: ARG or HEALPIX_HOME or search)])],
  [/usr /usr/local $HEALPIX_HOME],
  [include/healpix_base.h])
if test x$pao_cv_root_healpix != xno
then
  HEALPIX_HOME=$pao_cv_root_healpix
  AC_SUBST(HEALPIX_HOME)
  HEALPIX_CPPFLAGS=-I$HEALPIX_HOME/include
  AC_SUBST(HEALPIX_CPPFLAGS)
#  healpix 2.01:
#  HEALPIX_LIBS="-L$HEALPIX_HOME/lib -lcxxsupport -lhealpix_cxx -lfftpack" AWS20110414: this did not link, hence the reordering
#  HEALPIX_LIBS="-L$HEALPIX_HOME/lib -lhealpix_cxx  -lcxxsupport -lfftpack"  
#  healpix 2.20a:
#  HEALPIX_LIBS="-L$HEALPIX_HOME/lib    -lhealpix_cxx  -lcxxsupport -lpsht -lfftpack -lc_utils" 
#  healpix 3.11:
  HEALPIX_LIBS="-L$HEALPIX_HOME/lib    -lhealpix_cxx  -lcxxsupport -lsharp -lfftpack -lc_utils"
  AC_SUBST(HEALPIX_LIBS)
  AC_DEFINE([HAVE_HEALPIX], 1)
else
  PAO_MISSING([HEALPIX])
fi
AH_TEMPLATE([HAVE_HEALPIX], [Define to 1 if healpix is available on the system])
])

# PAO_REQUIRE_HEALPIX
#   Require that HEALPIX is installed, if not, configuration fails
#   (No version checking, could test features...)
#
AC_DEFUN([PAO_REQUIRE_HEALPIX],
[AC_REQUIRE([PAO_WITH_HEALPIX])
if test x$pao_cv_root_healpix = xno
then
  PAO_MISSING_REQUIRED([HEALPIX])
fi])



# PAO_WITH_ROOTMINUIT2
#   If ROOTMINUIT2 found, provide substitution variable
#     ROOTMINUIT2_HOME
#     ROOTMINUIT2_CPPFLAGS
#     ROOTMINUIT2_LIBS
#
AC_DEFUN([PAO_WITH_ROOTMINUIT2],
[_PAO_WITH_PACKAGE([rootminuit2],
  [AC_HELP_STRING([--with-rootminuit2],
                  [use rootminuit2 (default: ARG or ROOTSYS or search)])],
  [/usr /usr/local $ROOTSYS],
  [include/Minuit2/MnMigrad.h])
if test x$pao_cv_root_rootminuit2 != xno
then
  ROOTMINUIT2_HOME=$pao_cv_root_rootminuit2
  AC_SUBST(ROOTMINUIT2_HOME)
  ROOTMINUIT2_CPPFLAGS=-I$ROOTMINUIT2_HOME/include
  AC_SUBST(ROOTMINUIT2_CPPFLAGS)
  ROOTMINUIT2_LIBS="-L$ROOTMINUIT2_HOME/lib -ldl -lCore -lCint -lHist -lMatrix -lGraf -lGpad -lHistPainter -lNew  -lGX11 -lPostscript -lMatrix -lHist -lMinuit -lMinuit2 -lRIO -lMathCore -lThread"
  AC_SUBST(ROOTMINUIT2_LIBS)
  AC_DEFINE([HAVE_ROOTMINUIT2], 1)
else
  PAO_MISSING([ROOTMINUIT2])
fi
AH_TEMPLATE([HAVE_ROOTMINUIT2], [Define to 1 if Minuit2 is available on the system])
])

# PAO_REQUIRE_ROOTMINUIT2
#   Require that ROOTMINUIT2 is installed, if not, configuration fails
#   (No version checking, could test features...)
#
AC_DEFUN([PAO_REQUIRE_ROOTMINUIT2],
[AC_REQUIRE([PAO_WITH_ROOTMINUIT2])
if test x$pao_cv_root_rootminuit2 = xno
then
  PAO_MISSING_REQUIRED([ROOTMINUIT2])
fi])



# PAO_WITH_SLALIB
#   If SLALIB found, provide substitution variable
#     SLALIB_HOME
#     SLALIB_CPPFLAGS
#     SLALIB_LIBS
#
AC_DEFUN([PAO_WITH_SLALIB],
[_PAO_WITH_PACKAGE([slalib],
  [AC_HELP_STRING([--with-slalib],
                  [use slalib (default: ARG or ROOTSYS or search)])],
  [/usr /usr/local $ROOTSYS],
  [include/slalib.h]) 
if test x$pao_cv_root_slalib != xno
then
  SLALIB_HOME=$pao_cv_root_slalib
  AC_SUBST(SLALIB_HOME)
  SLALIB_CPPFLAGS=-I$SLALIB_HOME/include
  AC_SUBST(SLALIB_CPPFLAGS)
  SLALIB_LIBS="-L$SLALIB_HOME/lib -lsla_c"
  AC_SUBST(SLALIB_LIBS)
  AC_DEFINE([HAVE_SLALIB], 1)
else
  PAO_MISSING([SLALIB])
fi
AH_TEMPLATE([HAVE_SLALIB], [Define to 1 if slalib is available on the system])
])

# PAO_REQUIRE_SLALIB
#   Require that SLALIB is installed, if not, configuration fails
#   (No version checking, could test features...)
#
AC_DEFUN([PAO_REQUIRE_SLALIB],
[AC_REQUIRE([PAO_WITH_SLALIB])
if test x$pao_cv_slalib = xno
then
  PAO_MISSING_REQUIRED([SLALIB])
fi])




# PAO_WITH_CLHEP
#   If CLHEP found, provide substitution variable
#     CLHEPHOME
#     CLHEP_CPPFLAGS
#     CLHEP_LDFLAGS
#
AC_DEFUN([PAO_WITH_CLHEP],
[_PAO_WITH_PACKAGE([clhep],
  [AC_HELP_STRING([--with-clhep],
                  [use clhep (default: ARG or CLHEPHOME or search)])],
  [/usr /usr/local /opt/local /usr/local/CLHEP $CLHEPHOME],
  [include/CLHEP/Evaluator/Evaluator.h])
if test x$pao_cv_root_clhep != xno
then
  CLHEPHOME=$pao_cv_root_clhep
  AC_SUBST(CLHEPHOME)
  CLHEP_CPPFLAGS=-I$CLHEPHOME/include
  AC_SUBST(CLHEP_CPPFLAGS)
  CLHEP_LDFLAGS="-L$CLHEPHOME/lib -lCLHEP"
  AC_SUBST(CLHEP_LDFLAGS)
  AC_DEFINE([HAVE_CLHEP], 1)
else
  PAO_MISSING([CLHEP])
fi
AH_TEMPLATE([HAVE_CLHEP], [Define to 1 if CLHEP is available on the system])
])


# PAO_REQUIRE_CLHEP
#   Require that CLHEP is installed, if not, configuration fails
#
# Attempt to distinguish between 1.8, 1.9 and 2.0 series of CLHEP :
#  1.9 : only 1.9 has $CLHEPHOME/include/CLHEP/config/defs.h.
#        Geant4.8.1 is tested against 1.9.2.3.
#  2.0 : no $CLHEPHOME/include/CLHEP/config/defs.h file, BUT there is
#        a $CLHEPHOME/include/CLHEP/Evaluator/defs.h file.
#  1.8 : has no defs.h files at all
#
AC_DEFUN([PAO_REQUIRE_CLHEP],
[AC_REQUIRE([PAO_WITH_CLHEP])
if test x$gal_cv_root_clhep = xno
then
  PAO_MISSING_REQUIRED([CLHEP])
fi
AC_CHECK_FILE([$CLHEPHOME/include/CLHEP/config/defs.h],
              [AC_MSG_NOTICE([CLHEP 1.9.x series found as default.  This version does not work with Offline. Please install CLHEP 1.8.x or 2.x series.])]
              [PAO_MISSING_REQUIRED([CLHEP])],
              [AC_CHECK_FILE([$CLHEPHOME/include/CLHEP/Evaluator/defs.h],
                             [AC_MSG_NOTICE([Using CLHEP 2.0 series])]
                             [AC_DEFINE([CLHEP_V2_0], 1)],
                               [AC_MSG_NOTICE([Using CLHEP 1.8 series])]
                               [AC_DEFINE([CLHEP_V1_8], 1)]  )])
AH_TEMPLATE([CLHEP_V1_8], [Define to 1 if CLHEP version =  1.8])
AH_TEMPLATE([CLHEP_V2_0], [Define to 1 if CLHEP version >= 2])
])



dnl Check whether the target supports __sync_fetch_and_add.
AC_DEFUN([LIBGFOR_CHECK_SYNC_FETCH_AND_ADD], [
  AC_CACHE_CHECK([whether the target supports __sync_fetch_and_add],
		 have_sync_fetch_and_add, [
  AC_TRY_LINK([int foovar = 0;], [
if (foovar <= 0) return __sync_fetch_and_add (&foovar, 1);
if (foovar > 10) return __sync_add_and_fetch (&foovar, -1);],
	      have_sync_fetch_and_add=yes, have_sync_fetch_and_add=no)])
  if test $have_sync_fetch_and_add = yes; then
    AC_DEFINE(HAVE_SYNC_FETCH_AND_ADD, 1,
	      [Define to 1 if the target supports __sync_fetch_and_add])
  fi])

# PAO_INIT
#   Initial code for extra scripts to generate
#
AC_DEFUN([PAO_INIT],
[pao_missing=
pao_missing_required=
])


# PAO_MISSING
#   Deal with missing pacakges
#
AC_DEFUN([PAO_MISSING],
[pao_missing="$pao_missing $1"
])


# PAO_MISSING_REQUIRED
#   Deal with missing pacakges
#
AC_DEFUN([PAO_MISSING_REQUIRED],
[pao_missing_required="$pao_missing_required $1"
])


# PAO_CHECK_MISSING_REQUIRED_PACKAGES
#   Code to test if we are missing any required packages.
#   Calls PAO_FINISH if this is the case.
#
AC_DEFUN([PAO_CHECK_MISSING_REQUIRED_PACKAGES],
[
if test "x$pao_missing_required" != x
then
  PAO_FINISH
fi  
])

# PAO_FINISH
#   Final code after all tests for external packages
#   Aborts execution if we are missing any required packages.
#
AC_DEFUN([PAO_FINISH],
[cat >galprop-missing-packages <<EOF
#! /bin/sh
if test x\$[]1 = x--required
then
  echo $pao_missing_required
else
  echo $pao_missing
fi
EOF
chmod a+x galprop-missing-packages
if test "x$pao_missing" != x
then
  AC_MSG_NOTICE([Packages not found or deactivated on this system:])
  AC_MSG_NOTICE([    $pao_missing])
fi
if test "x$pao_missing_required" != x
then
  AC_MSG_NOTICE([Missing required packages: $pao_missing_required])
  AC_MSG_ERROR([Aborting configuration])
fi
])


# _PAO_WITH_PACKAGE(name, help, path, file)
#   Wrapper for AC_ARG_WITH and AC_CACHE_CHECK
#   Provides a --with-name option with help.
#   If package location not given, try to find `file' in `path' and
#   keep the *last* match
#
AC_DEFUN([_PAO_WITH_PACKAGE], 
[AC_ARG_WITH([$1], [$2],
             [_pao_with_$1=$withval])
AC_CACHE_CHECK([for $1],
               [pao_cv_root_$1],
               [if test x$_pao_with_$1 = xno
                then
                  pao_cv_root_$1=no
                fi
                if test x$_pao_with_$1 = x || test x$_pao_with_$1 = xyes
                then
                  for p in $3
                  do
                    test -f $p/$4 && pao_cv_root_$1=$p
                  done
                  test x$pao_cv_root_$1 = x && pao_cv_root_$1=no
                else
                  pao_cv_root_$1=$_pao_with_$1
                fi
               ])
])

# _PAO_BASENAME
#  M4 analogue to the basename shell utility
#
m4_define([_PAO_BASENAME], [m4_bregexp($1, [\(.*/\|\)\(.+\)], [\2])])

# _PAO_NORMALISE_VERSION_3
#   Convert dot-separated version number into a 
#   comma-separated list for individual processing
#   Up to 3 components accepted, missing componented
#   replaced by 0.
#
m4_define([_PAO_NORMALISE_VERSION_3], 
          [_PAO_NORMALISE_VERSION_AUX_3(m4_bpatsubst($1, [\.], [,]))])

m4_define([_PAO_NORMALISE_VERSION_AUX_3], 
  [ifelse($#, 3, [$1, $2, $3], $#, 2, [$1, $2, 0], $#, 1, [$1, 0, 0], [AC_FATAL([Maximal 3 components in version expected - got $*])])])

# _PAO_SAVE_COMPILE_FLAGS
#   Save flags for compilation so we can modify them for test compilation
#   NB: This macro cannot be nested
#
m4_define([_PAO_SAVE_COMPILE_FLAGS],
[_pao_save_CXXFLAGS="$CXXFLAGS"
_pao_save_CPPFLAGS="$CPPFLAGS"
_pao_save_LDFLAGS="$LDFLAGS"
_pao_save_LD_LIBRARY_PATH="$LD_LIBRARY_PATH"
])

# _PAO_SAVE_RESTORE_FLAGS
#   Restore flags after compilation. so we can modify them for test compilation
#
m4_define([_PAO_RESTORE_COMPILE_FLAGS],
[CXXFLAGS="$_pao_save_CXXFLAGS"
CPPFLAGS="$_pao_save_CPPFLAGS"
LDFLAGS="$_pao_save_LDFLAGS"
LD_LIBRARY_PATH="$_pao_save_LD_LIBRARY_PATH"
])

